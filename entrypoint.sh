#!/bin/sh

chown -R www-data:www-data /var/www
env >> /etc/environment
cron start
crontab /var/www/crons/*

# execute CMD
echo "$@"
exec "$@"