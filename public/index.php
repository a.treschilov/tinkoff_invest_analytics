<?php

declare(strict_types=1);

use App\Handlers\AppErrorHandler;
use App\Handlers\ShutdownHandler;
use DI\Container;
use Monolog\Logger;
use Ramsey\Uuid\Uuid;
use Slim\Factory\AppFactory;
use Slim\Factory\ServerRequestCreatorFactory;

require __DIR__ . '/../vendor/autoload.php';

// TODO: Remove after sendgrid will support php 8.4
set_error_handler(
    function ($code, $error, $file) {
        return str_contains($file, '/vendor/sendgrid/');
    },
    E_DEPRECATED
);

// Create Container using PHP-DI
$container = new Container();

require __DIR__ . '/../app/Common/dependencies.php';

// Configure the application via container
$app = AppFactory::createFromContainer($container);

require_once __DIR__ . '/../app/route.php';

$displayErrorDetails = true;

$logger = $container->get(Logger::class);
$callableResolver = $app->getCallableResolver();
$responseFactory = $app->getResponseFactory();

$serverRequestCreator = ServerRequestCreatorFactory::create();
$request = $serverRequestCreator->createServerRequestFromGlobals();

$errorHandler = new AppErrorHandler($callableResolver, $responseFactory, $logger);
$shutdownHandler = new ShutdownHandler($request, $errorHandler, $displayErrorDetails);
register_shutdown_function($shutdownHandler);

$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, true, true, $logger);
$errorMiddleware->setDefaultErrorHandler($errorHandler);

$requestID = Uuid::uuid4()->toString();
if (getenv('LOG_API_REQUESTS') === "1") {
    $logger->info(
        "Api call start",
        [
            "requestId" => $requestID,
            "uri" => $_SERVER['REQUEST_URI'],
            "method" => $_SERVER['REQUEST_METHOD'],
            "body" => json_decode(file_get_contents('php://input'), true)
        ]
    );
}
$app->run();
if (getenv('LOG_API_REQUESTS') === "1") {
    $logger->info("Api call processed", ['requestId' => $requestID]);
}
