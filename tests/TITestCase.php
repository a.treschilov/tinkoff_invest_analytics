<?php

declare(strict_types=1);

namespace Tests;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Models\BrokerShareModel;
use App\Entities\MarketSectorEntity;
use App\Intl\Entities\CountryEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Market\Types\CandleIntervalType;
use App\Models\OperationModel;

class TITestCase
{
    public static function createBrokerPortfolioPosition(array $data): BrokerPortfolioPositionModel
    {
        $averagePositionPrice = new BrokerMoneyModel([
            'amount' => $data['averagePositionPrice']['amount'] ?? 70,
            'currency' => $data['averagePositionPrice']['currency'] ?? 'RUB'
        ]);

        $expectedYield = new BrokerMoneyModel([
            'amount' => $data['expectedYield']['amount'] ?? 10,
            'currency' => $data['expectedYield']['currency'] ?? 'RUB'
        ]);

        $currentNkd = new BrokerMoneyModel([
            'amount' => $data['currentNkd']['amount'] ?? 75,
            'currency' => $data['currentNkd']['currency'] ?? 'RUB'
        ]);

        $currentPrice = new BrokerMoneyModel([
            'amount' => $data['currentPrice']['amount'] ?? 60,
            'currency' => $data['currentPrice']['currency'] ?? 'RUB'
        ]);

        $portfolioPosition = new BrokerPortfolioPositionModel();
        $portfolioPosition->setIsin($data['isin'] ?? 'isin');
        $portfolioPosition->setInstrumentType($data['instrumentType'] ?? 'stock');
        $portfolioPosition->setQuantity($data['quantity'] ?? 13);
        $portfolioPosition->setAveragePositionPrice($averagePositionPrice);
        $portfolioPosition->setExpectedYield($expectedYield);
        $portfolioPosition->setCurrentNkd($currentNkd);
        $portfolioPosition->setCurrentPrice($currentPrice);

        return $portfolioPosition;
    }

    public static function createOperation(array $data): OperationModel
    {
        $defaultData = [
            'id' => '1',
            'parentOperationId' => '2',
            'status' => 'Done',
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel(['amount' => 100, 'currency' => 'RUB']),
            'price' => new BrokerMoneyModel(['amount' => 5.15, 'currency' => 'RUB']),
            'quantity' => 20,
            'type' => 'Stock',
            'isMarginCall' => false,
            'date' => new \DateTime('2021-04-01 18:00:13'),
            'operationType' => 'Buy',
            'instrument' => null
        ];

        $data = array_merge($defaultData, $data);

        return new OperationModel($data);
    }

    public static function createBrokerOperation(array $data): BrokerOperationModel
    {
        $defaultData = [
            'id' => '1',
            'parentOperationId' => '2',
            'status' => 'Done',
            'currency' => 'RUB',
            'payment' => new BrokerMoneyModel(['amount' => 100, 'currency' => 'RUB']),
            'price' => new BrokerMoneyModel(['amount' => 5.15, 'currency' => 'RUB']),
            'quantity' => 20,
            'figi' => 'LKOH',
            'type' => 'Stock',
            'isMarginCall' => false,
            'date' => new \DateTime('2021-04-01 18:00:13'),
            'operationType' => 'Buy'
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerOperationModel($data);
    }

    public static function createBrokerShare(array $data): BrokerShareModel
    {
        $defaultData = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond',
            'country' => 'RU',
            'sector' => 'Consumer Cyclical',
            'industry' => 'Footwear & Accessories'
        ];

        $data = array_merge($defaultData, $data);

        $instrument = new BrokerInstrumentModel([
            'figi' => $data['figi'],
            'ticker' => $data['ticker'],
            'isin' => $data['isin'],
            'minPriceIncrement' => $data['minPriceIncrement'],
            'lot' => $data['lot'],
            'currency' => $data['currency'],
            'name' => $data['name'],
            'type' => $data['type']
        ]);

        return new BrokerShareModel([
            'instrument' => $instrument,
            'country' => $data['country'],
            'sector' => $data['sector'],
            'industry' => $data['industry'],
        ]);
    }

    public static function createBrokerInstrument(array $data): BrokerInstrumentModel
    {
        $defaultData = [
            'figi' => 'BBG00N8HZBK8',
            'ticker' => 'RU000A0JXE06',
            'isin' => 'RU000A0JXE06i',
            'minPriceIncrement' => 0.1,
            'lot' => 1,
            'currency' => 'RUB',
            'name' => 'ГТЛК выпуск 3',
            'type' => 'Bond'
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerInstrumentModel($data);
    }

    public static function createBrokerCandle(array $data): BrokerCandleModel
    {
        $defaultData = [
            'open' => 10,
            'close' => 15,
            'high' => 18,
            'low' => 8,
            'volume' => 1000,
            'time' => new \DateTime('2022-01-01 00:00:00'),
            'interval' => CandleIntervalType::DAY_1->value,
            'isComplete' => true,
            'currency' => 'USD'
        ];

        $data = array_merge($defaultData, $data);

        return new BrokerCandleModel($data);
    }

    public static function createCountryEntity(array $data = []): CountryEntity
    {
        $country = new CountryEntity();
        $country->setIso($data['iso'] ?? 'RU');
        $country->setName($data['name'] ?? 'Russian Federation');
        $country->setContinent($data['continent'] ?? 'Europe');

        return $country;
    }

    public static function createCurrencyEntity(array $data = []): CurrencyEntity
    {
        $currency = new CurrencyEntity();
        $currency->setIso($data['iso'] ?? 'RUB');
        $currency->setName($data['name'] ?? 'Российский рубль');
        $currency->setSymbol($data['symbol'] ?? '₽');
        $currency->setId($data['id'] ?? 1);

        return $currency;
    }

    public static function createMarketSectorEntity(array $data = []): MarketSectorEntity
    {
        $sector = new MarketSectorEntity();
        $sector->setId($data['id'] ?? 1);
        $sector->setName($data['name'] ?? 'Utilities');

        return $sector;
    }
}
