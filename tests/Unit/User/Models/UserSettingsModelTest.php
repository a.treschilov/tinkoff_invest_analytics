<?php

namespace Tests\Unit\User\Models;

use App\Models\PriceModel;
use App\User\Models\UserSettingsModel;
use Tests\ModelTestCase;

class UserSettingsModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserSettingsModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setUserSettingsId' => 1,
                    'setUserId' => 2,
                    'setDesiredPension' => new PriceModel([
                        'amount' => 1.5,
                        'currency' => 'USD'
                    ]),
                    'setRetirementAge' => 50
                ],
                [
                    'getUserSettingsId' => 1,
                    'getUserId' => 2,
                    'getDesiredPension' => new PriceModel([
                        'amount' => 1.5,
                        'currency' => 'USD'
                    ]),
                    'getRetirementAge' => 50
                ]
            ],
            'Empty Field Case' => [
                [
                    'setUserId' => 2
                ],
                [
                    'getUserSettingsId' => null,
                    'getUserId' => 2,
                    'getDesiredPension' => null,
                    'getRetirementAge' => null
                ]
            ]
        ];
    }
}
