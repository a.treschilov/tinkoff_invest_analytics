<?php

declare(strict_types=1);

namespace Tests\Unit\User\Models;

use App\User\Models\UserCredentialModel;
use Tests\ModelTestCase;

class UserCredentialModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserCredentialModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setUserCredentialId' => 1,
                    'setUserId' => 2,
                    'setBrokerId' => 'tinkoff',
                    'setApiKey' => 'AAAA-BBBB',
                    'setIsActive' => 0
                ],
                [
                    'getUserCredentialId' => 1,
                    'getUserId' => 2,
                    'getBrokerId' => 'tinkoff',
                    'getApiKey' => 'AAAA-BBBB',
                    'getIsActive' => 0
                ]
            ],
            'Empty Case' => [
                [
                    'setUserCredentialId' => 1,
                    'setUserId' => 2,
                    'setBrokerId' => 'tinkoff',
                    'setApiKey' => 'AAAA-BBBB',
                ],
                [
                    'getUserCredentialId' => 1,
                    'getUserId' => 2,
                    'getBrokerId' => 'tinkoff',
                    'getApiKey' => 'AAAA-BBBB',
                    'getIsActive' => null
                ]
            ]
        ];
    }
}
