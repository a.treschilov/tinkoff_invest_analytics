<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Broker\Collections\AccountCollection;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Models\BrokerAccountModel;
use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use App\Services\StockBrokerFactory;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Storages\UserBrokerAccountStorage;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class UserBrokerAccountServiceTest extends TestCase
{
    private StockBrokerFactory $stockBrokerFactory;
    private UserCredentialService $userCredentialService;
    private UserBrokerAccountStorage $userBrokerAccountStorage;
    private UserBrokerAccountService $userBrokerAccountService;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->userBrokerAccountStorage = $this->createMock(UserBrokerAccountStorage::class);
        $this->userBrokerAccountService = new UserBrokerAccountService(
            $this->stockBrokerFactory,
            $this->userCredentialService,
            $this->userBrokerAccountStorage
        );
    }

    public function testGetAccountListByCredential()
    {
        $credentialId = 1;
        $credentialEntity = new UserCredentialEntity();
        $credentialEntity->setId(1);
        $brokerAccount01 = new BrokerAccountModel([
            'id' => '1',
            'type' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
            'name' => 'Primary Account',
            'status' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW,
            'openedDate' => new \DateTime('2022-03-01 12:15:18'),
            'closedDate' => null
        ]);
        $brokerAccount02 = new BrokerAccountModel([
            'id' => '2',
            'type' => BrokerAccountTypeType::ACCOUNT_TYPE_IIS,
            'name' => 'Secondary Account',
            'status' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW,
            'openedDate' => new \DateTime('2022-02-04 13:13:01'),
            'closedDate' => new \DateTime('2022-03-04 11:43:22'),
        ]);
        $accountCollection = new AccountCollection([$brokerAccount01, $brokerAccount02]);

        $userBrokerAccountEntity01 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity01->setId(1);
        $userBrokerAccountEntity01->setUserCredentialId(1);
        $userBrokerAccountEntity01->setIsActive(1);
        $userBrokerAccountEntity01->setIsDeleted(0);
        $userBrokerAccountEntity01->setExternalId('1');
        $userBrokerAccountEntity01->setName('Primary DB Account');
        $userBrokerAccountEntity01->setType(3);
        $userBrokerAccountEntity01->setCreatedDate(new \DateTime('2022-03-07 11:24:02'));
        $userBrokerAccountEntity01->setUpdatedDate(new \DateTime('2022-03-07 11:24:02'));

        $resultUserBrokerAccountEntity01 = new UserBrokerAccountEntity();
        $resultUserBrokerAccountEntity01->setId(1);
        $resultUserBrokerAccountEntity01->setUserCredentialId(1);
        $resultUserBrokerAccountEntity01->setIsActive(1);
        $resultUserBrokerAccountEntity01->setType(1);
        $resultUserBrokerAccountEntity01->setIsDeleted(0);
        $resultUserBrokerAccountEntity01->setExternalId('1');
        $resultUserBrokerAccountEntity01->setName('Primary Account');
        $resultUserBrokerAccountEntity01->setType(1);
        $resultUserBrokerAccountEntity01->setCreatedDate(new \DateTime('2022-03-07 11:24:02'));
        $resultUserBrokerAccountEntity01->setUpdatedDate(new \DateTime('2022-03-07 11:24:02'));

        $resultUserBrokerAccountEntity02 = new UserBrokerAccountEntity();
        $resultUserBrokerAccountEntity02->setIsActive(0);
        $resultUserBrokerAccountEntity02->setType(2);
        $resultUserBrokerAccountEntity02->setExternalId('2');
        $resultUserBrokerAccountEntity02->setName('Secondary Account');
        $resultUserBrokerAccountEntity02->setType(2);

        $userBrokerAccountCollection = new UserBrokerAccountCollection([
            $resultUserBrokerAccountEntity01,
            $resultUserBrokerAccountEntity02
        ]);

        $client = $this->createMock(UserBrokerInterface::class);

        $this->userCredentialService->expects($this->once())
            ->method('getUserCredentialById')
            ->with($credentialId)
            ->willReturn($credentialEntity);
        $this->stockBrokerFactory->expects($this->once())
            ->method('getBroker')
            ->with($credentialEntity)
            ->willReturn($client);
        $client->expects($this->once())->method('getAccounts')->willReturn($accountCollection);

        $this->userBrokerAccountStorage->expects($this->once())->method('findByCredentialId')
            ->with($credentialId)
            ->willReturn([$userBrokerAccountEntity01]);

        $this->assertEquals(
            $userBrokerAccountCollection,
            $this->userBrokerAccountService->getUserAccountListByCredential($credentialId)
        );
    }

    public function testGetUserAccountByUserId()
    {
        $userId = 1;

        $credentialO1 = new UserCredentialModel([
            'userCredentialId' => 1
        ]);
        $credentialO2 = new UserCredentialModel([
            'userCredentialId' => 2
        ]);

        $credentialCollection = new UserCredentialCollection([$credentialO1, $credentialO2]);

        $brokerAccount01 = new UserBrokerAccountEntity();
        $brokerAccount01->setId(1);
        $brokerAccount01->setType(1);
        $brokerAccount02 = new UserBrokerAccountEntity();
        $brokerAccount02->setId(2);
        $brokerAccount02->setType(1);
        $brokerAccount03 = new UserBrokerAccountEntity();
        $brokerAccount03->setId(3);
        $brokerAccount03->setType(1);
        $brokerAccount04 = new UserBrokerAccountEntity();
        $brokerAccount04->setId(4);
        $brokerAccount04->setType(1);
        $brokerAccountCollection03 = new UserBrokerAccountCollection([
            $brokerAccount01,
            $brokerAccount02,
            $brokerAccount03,
            $brokerAccount04
        ]);



        $this->userCredentialService->expects($this->once())->method('getActiveCredentialList')
            ->with($userId)->willReturn($credentialCollection);

        $this->userBrokerAccountStorage->expects($this->exactly(2))->method('findActiveByCredentialId')
            ->with(...WithConsecutive::create([1], [2]))
            ->willReturnOnConsecutiveCalls([$brokerAccount01, $brokerAccount02], [$brokerAccount03, $brokerAccount04]);

        $this->assertEquals(
            $brokerAccountCollection03,
            $this->userBrokerAccountService->getUserAccountByUserId($userId)
        );
    }
}
