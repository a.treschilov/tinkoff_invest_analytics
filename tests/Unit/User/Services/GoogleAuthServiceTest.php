<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Common\HttpClient;
use App\User\Models\UserAuthModel;
use App\User\Services\GoogleAuthService;
use App\User\Types\AuthType;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;

class GoogleAuthServiceTest extends TestCase
{
    private HttpClient $httpClient;
    private GoogleAuthService $googleAuthService;

    public function setUp(): void
    {
        parent::setUp();

        $this->httpClient = $this->createMock(HttpClient::class);
        $this->googleAuthService = new GoogleAuthService($this->httpClient);
    }

    #[DataProvider('dataGetUserInfo')]
    public function testGetUserInfo(
        ?UserAuthModel $expected,
        string $accessToken,
        ?string $content,
        int $statusCode
    ) {
        $response = $this->createMock(ResponseInterface::class);

        if ($content !== null) {
            $body = $this->createMock(StreamInterface::class);
            $body->expects($this->once())->method('getContents')->willReturn($content);
            $response->expects($this->once())->method('getBody')->willReturn($body);
        }

        $response->expects($this->once())->method('getStatusCode')->willReturn($statusCode);

        $this->httpClient->expects($this->once())->method('doRequest')
            ->with(
                'GET',
                'people/me',
                ['personFields' => 'emailAddresses,names,birthdays'],
                ['Authorization' => 'Bearer ' . 'AAAA-BBBB-CCCC']
            )->willReturn($response);

        $this->assertEquals($expected, $this->googleAuthService->getUserInfo($accessToken));
    }

    public static function dataGetUserInfo(): array
    {
        $accessToken01 = 'AAAA-BBBB-CCCC';

        $content01 = json_encode([
            'names' => [
                ['displayName' => 'a.treschilov']
            ],
            'emailAddresses' => [
                ['value' => 'a.treschilov@email.com']
            ],
            'birthdays' => [
                [
                    'date' => [
                        'year' => 1985,
                        'month' => 4,
                        'day' => 6
                    ]
                ]
            ],
            'resourceName' => '1111-2222'
        ]);
        $statusCode01 = 200;
        $expected01 = new UserAuthModel([
            'login' => 'a.treschilov@email.com',
            'access_token' => 'AAAA-BBBB-CCCC',
            'auth_type' => AuthType::GOOGLE,
            'name' => 'a.treschilov',
            'email' => 'a.treschilov@email.com',
            'birthday' => new \DateTime('1985-04-06'),
            'client_id' => '1111-2222'
        ]);

        $content02 = null;
        $statusCode02 = 403;
        $expected02 = null;

        return [
            'Common Case' => [
                $expected01,
                $accessToken01,
                $content01,
                $statusCode01,
            ],
            'Unsuccessful response' => [
                $expected02,
                $accessToken01,
                $content02,
                $statusCode02
            ]
        ];
    }
}
