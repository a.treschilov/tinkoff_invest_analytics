<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Broker\Services\BrokerHelper;
use App\Broker\Services\BrokerService;
use App\Broker\Entities\BrokerEntity;
use App\Broker\Models\BrokerModel;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Services\EncryptService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use App\User\Storages\UserCredentialStorage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\PersistentCollection;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\WithConsecutive;

class UserCredentialServiceTest extends TestCase
{
    private UserCredentialService $userCredentialService;
    private UserService $userServiceMock;
    private UserCredentialStorage $userCredentialStorageMock;
    private EncryptService $encryptService;
    private BrokerService $brokerService;
    private BrokerHelper $brokerHelper;

    public function setUp(): void
    {
        parent::setUp();

        $this->userServiceMock = $this->createMock(UserService::class);
        $this->userCredentialStorageMock = $this->createMock(UserCredentialStorage::class);
        $this->encryptService = $this->createMock(EncryptService::class);
        $this->brokerService = $this->createMock(BrokerService::class);
        $this->brokerHelper = $this->createMock(BrokerHelper::class);
        $this->userCredentialService = new UserCredentialService(
            $this->encryptService,
            $this->userCredentialStorageMock,
            $this->userServiceMock,
            $this->brokerService,
            $this->brokerHelper
        );
    }

    private static function getDefaultUserId(): int
    {
        return 1;
    }

    /**
     * @return UserCredentialEntity[]
     */
    private static function getDefaultCredentials(): array
    {
        $userId01 = 1;
        $userEntity01 = new UserEntity();
        $userEntity01->setId(1);

        $userId02 = 2;
        $userEntity02 = new UserEntity();
        $userEntity02->setId(2);

        $brokerEntity01 = new BrokerEntity();
        $brokerEntity01->setId('tinkoff');
        $brokerEntity01->setName('Tinkoff');
        $brokerEntity01->setIsActive(1);

        $brokerEntity02 = new BrokerEntity();
        $brokerEntity02->setId('bcs');
        $brokerEntity02->setName('BCS');
        $brokerEntity02->setIsActive(1);

        $credential01 = new UserCredentialEntity();
        $credential01->setId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBrokerId('tinkoff');
        $credential01->setUser($userEntity01);
        $credential01->setActive(1);
        $credential01->setUserId($userId01);
        $credential01->setUserBrokerAccount(new PersistentCollection(null, null, new ArrayCollection()));
        $credential01->setBroker($brokerEntity01);

        $credential02 = new UserCredentialEntity();
        $credential02->setId(2);
        $credential02->setApiKey('DDDD-EEEE-FFFF');
        $credential02->setBrokerId('bcs');
        $credential02->setUser($userEntity02);
        $credential02->setActive(0);
        $credential02->setUserId($userId02);
        $credential02->setUserBrokerAccount(new PersistentCollection(null, null, new ArrayCollection()));
        $credential02->setBroker($brokerEntity02);

        return [$credential01, $credential02];
    }

    /**
     * @return UserCredentialModel[]
     */
    private static function getDefaultCredentialModels(): array
    {
        $userId01 = 1;
        $userId02 = 2;

        $credential01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'userId' => $userId01,
            'brokerId' => 'tinkoff',
            'broker' => new BrokerModel([
                'brokerId' => 'tinkoff',
                'name' => 'Tinkoff',
                'isActive' => 1
            ]),
            'apiKey' => 'AAAA-BBBB-CCCC',
            'isActive' => 1,
            'brokerAccounts' => new UserBrokerAccountCollection()
        ]);

        $credential02 = new UserCredentialModel([
            'userCredentialId' => 2,
            'userId' => $userId02,
            'brokerId' => 'bcs',
            'broker' => new BrokerModel([
                'brokerId' => 'bcs',
                'name' => 'BCS',
                'isActive' => 1
            ]),
            'apiKey' => 'DDDD-EEEE-FFFF',
            'isActive' => 0,
            'brokerAccounts' => new UserBrokerAccountCollection()
        ]);

        return [$credential01, $credential02];
    }

    public function testGetCredentialList()
    {
        $em = $this->createMock(EntityManager::class);
        $metadata = $this->createMock(ClassMetadata::class);
        $userId01 = 1;
        $userEntity01 = new UserEntity();
        $userEntity01->setId(1);

        $brokerEntity01 = new BrokerEntity();
        $brokerEntity01->setId('tinkoff');
        $brokerEntity01->setName('Tinkoff');
        $brokerEntity01->setIsActive(1);
        $brokerEntity01->setIsApi(0);
        $brokerEntity01->setIsManual(0);

        $brokerEntity02 = new BrokerEntity();
        $brokerEntity02->setId('bcs');
        $brokerEntity02->setName('БКС');
        $brokerEntity02->setIsActive(1);
        $brokerEntity02->setIsApi(0);
        $brokerEntity02->setIsManual(0);

        $userId02 = 2;
        $userEntity02 = new UserEntity();
        $userEntity02->setId(2);

        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);

        $credential01 = new UserCredentialEntity();
        $credential01->setId(1);
        $credential01->setApiKey('ENCRYPT1');
        $credential01->setBrokerId('tinkoff');
        $credential01->setUser($userEntity01);
        $credential01->setBroker($brokerEntity01);
        $credential01->setActive(1);
        $credential01->setUserId($userId01);
        $credential01->setUserBrokerAccount(
            new PersistentCollection($em, $metadata, new ArrayCollection([$account01, $account02]))
        );

        $credential02 = new UserCredentialEntity();
        $credential02->setId(2);
        $credential02->setApiKey('ENCRYPT2');
        $credential02->setBrokerId('bcs');
        $credential02->setUser($userEntity02);
        $credential02->setBroker($brokerEntity02);
        $credential02->setActive(0);
        $credential02->setUserId($userId02);
        $credential02->setUserBrokerAccount(
            new PersistentCollection($em, $metadata, new ArrayCollection([$account02]))
        );

        $credentialModel01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'AAAA-BBBB',
            'brokerId' => 'tinkoff',
            'brokerName' => 'Tinkoff',
            'isActive' => 1,
            'userId' => $userId01,
            'brokerAccounts' => new UserBrokerAccountCollection([$account01, $account02]),
            'broker' => new BrokerModel(['brokerId' => 'tinkoff', 'isActive' => 1, 'name' => 'Tinkoff'])
        ]);
        $credentialModel02 = new UserCredentialModel([
            'userCredentialId' => 2,
            'apiKey' => 'CCCC-DDDD',
            'brokerId' => 'bcs',
            'brokerName' => 'БКС',
            'isActive' => 0,
            'userId' => $userId02,
            'accountNumber' => 1,
            'activeAccountNumber' => 0,
            'brokerAccounts' => new UserBrokerAccountCollection([$account02]),
            'broker' => new BrokerModel(['brokerId' => 'bcs', 'isActive' => 1, 'name' => 'БКС'])
        ]);

        $userId01 = self::getDefaultUserId();

        $expectedCollection = new UserCredentialCollection([$credentialModel01, $credentialModel02]);

        $this->userCredentialStorageMock->expects($this->once())->method('findByUserId')
            ->with($userId01)->willReturn([$credential01, $credential02]);
        $this->brokerHelper->expects($this->exactly(2))->method('convertBrokerEntityToModel')
            ->with(...WithConsecutive::create([$brokerEntity01], [$brokerEntity02]))
            ->willReturnOnConsecutiveCalls($credentialModel01->getBroker(), $credentialModel02->getBroker());
        $this->encryptService->expects($this->exactly(2))->method('decrypt')
            ->with(...WithConsecutive::create(['ENCRYPT1'], ['ENCRYPT2']))
            ->willReturnOnConsecutiveCalls('AAAA-BBBB', 'CCCC-DDDD');

        $this->assertEquals($expectedCollection, $this->userCredentialService->getCredentialList($userId01));
    }

    public function testGetUserActiveCredential()
    {
        $credentials = self::getDefaultCredentials();
        $models = self::getDefaultCredentialModels();
        $userId01 = self::getDefaultUserId();
        $broker01 = 'tinkoff';

        $expected = $models[0];

        $this->userCredentialStorageMock->expects($this->once())->method('findActiveByUserIdAndBroker')
            ->with($userId01)->willReturn($credentials[0]);
        $this->brokerHelper->expects($this->exactly(1))
            ->method('convertBrokerEntityToModel')
            ->with($credentials[0]->getBroker())
            ->willReturn($models[0]->getBroker());
        $this->encryptService->expects($this->once())->method('decrypt')
            ->with('AAAA-BBBB-CCCC')->willReturn('AAAA-BBBB-CCCC');

        $this->assertEquals($expected, $this->userCredentialService->getUserActiveCredential($userId01, $broker01));
    }

    public function testGetActiveCredentialList()
    {
        $credentials01 = self::getDefaultCredentials();
        $credentialModels01 = self::getDefaultCredentialModels();
        $userId01 = self::getDefaultUserId();

        $collection01 = new UserCredentialCollection($credentialModels01);

        $this->userCredentialStorageMock->expects($this->once())->method('findActiveByUserId')
            ->with($userId01)->willReturn($credentials01);
        $this->brokerHelper->expects($this->exactly(2))
            ->method('convertBrokerEntityToModel')
            ->with(...WithConsecutive::create([$credentials01[0]->getBroker()], [$credentials01[1]->getBroker()]))
            ->willReturnOnConsecutiveCalls($credentialModels01[0]->getBroker(), $credentialModels01[1]->getBroker());
        $this->encryptService->expects($this->exactly(2))
            ->method('decrypt')
            ->with(...WithConsecutive::create(['AAAA-BBBB-CCCC'], ['DDDD-EEEE-FFFF']))
            ->willReturnOnConsecutiveCalls('AAAA-BBBB-CCCC', 'DDDD-EEEE-FFFF');

        $this->assertEquals($collection01, $this->userCredentialService->getActiveCredentialList($userId01));
    }

    public function testGetAllActiveCredentialList()
    {
        $credentials01 = self::getDefaultCredentials();
        $credentialModels01 = self::getDefaultCredentialModels();
        $collection01 = new UserCredentialCollection($credentialModels01);

        $this->userCredentialStorageMock->expects($this->once())->method('findActive')
            ->willReturn($credentials01);
        $this->brokerHelper->expects($this->exactly(2))
            ->method('convertBrokerEntityToModel')
            ->with(...WithConsecutive::create([$credentials01[0]->getBroker()], [$credentials01[1]->getBroker()]))
            ->willReturnOnConsecutiveCalls($credentialModels01[0]->getBroker(), $credentialModels01[1]->getBroker());
        $this->encryptService->expects($this->exactly(2))
            ->method('decrypt')
            ->with(...WithConsecutive::create(['AAAA-BBBB-CCCC'], ['DDDD-EEEE-FFFF']))
            ->willReturnOnConsecutiveCalls('AAAA-BBBB-CCCC', 'DDDD-EEEE-FFFF');

        $this->assertEquals($collection01, $this->userCredentialService->getAllActiveCredentialList());
    }

    #[DataProvider('dataGetUserCredentialById')]
    public function testGetUserCredentialById(
        ?UserCredentialEntity $credentialEntity,
        ?array $exception,
        int $userId
    ) {
        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }
        $credentialId = 1;

        if (null === $exception || $exception['code'] !== 2020) {
            $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn($userId);
        }
        $this->userCredentialStorageMock->expects($this->once())->method('findOneById')
            ->with($credentialId)->willReturn($credentialEntity);

        $this->assertEquals($credentialEntity, $this->userCredentialService->getUserCredentialById($credentialId));
    }

    public static function dataGetUserCredentialById(): array
    {
        $credentials01 = self::getDefaultCredentials();
        $userId01 = self::getDefaultUserId();

        $exception01 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2021
        ];
        $exception02 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2020
        ];
        return [
            'Common Case' => [
                $credentials01[0],
                null,
                $userId01
            ],
            'Access Denied Case' => [
                $credentials01[1],
                $exception01,
                $userId01
            ],
            'Not found Case' => [
                null,
                $exception02,
                $userId01
            ]
        ];
    }

    public function testAddUserCredential()
    {
        $userEntity = new UserEntity();
        $userEntity->setId(1);

        $brokerEntity = new BrokerEntity();
        $brokerEntity->setId('tinkoff');

        $credentialEntity = new UserCredentialEntity();
        $credentialEntity->setBrokerId('tinkoff');
        $credentialEntity->setApiKey('ENCRYPT1');
        $credentialEntity->setActive(0);
        $credentialEntity->setUserId(1);
        $credentialEntity->setUser($userEntity);
        $credentialEntity->setBroker($brokerEntity);

        $credentialModel = new UserCredentialModel();
        $credentialModel->setBrokerId('tinkoff');
        $credentialModel->setApiKey('AAAA-BBBB');
        $credentialModel->setIsActive(0);

        $this->userServiceMock->expects($this->once())->method('getUser')->willReturn($userEntity);
        $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn(1);
        $this->encryptService->expects($this->once())->method('encrypt')->with('AAAA-BBBB')
            ->willReturn('ENCRYPT1');
        $this->brokerService->expects($this->once())->method('getBrokerEntityById')->with('tinkoff')
            ->willReturn($brokerEntity);

        $this->userCredentialStorageMock->expects($this->once())->method('addEntity')
            ->with($credentialEntity);

        $this->assertEquals($credentialEntity, $this->userCredentialService->addUserCredential($credentialModel));
    }

    #[DataProvider('dataUpdateUserCredential')]
    public function testUpdateUserCredential(
        UserCredentialEntity $expected,
        ?array $exception,
        UserCredentialModel $userCredentialModel,
        UserCredentialEntity $userCredentialEntity,
        int $userId,
        int $credentialId,
        ?array $key
    ): void {
        $this->userServiceMock->expects($this->once())
            ->method('getUserId')->willReturn($userId);

        $this->userCredentialStorageMock->expects($this->once())
            ->method('findOneById')->with($credentialId)
            ->willReturn($userCredentialEntity);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        } else {
            $this->encryptService->expects($this->once())->method('encrypt')
                ->with($key['encrypt'])
                ->willReturn($key['decrypt']);
            $this->userCredentialStorageMock->expects($this->once())
                ->method('addEntity')->with($expected);
        }

        $this->userCredentialService->updateUserCredential($userCredentialModel);
    }

    public static function dataUpdateUserCredential(): array
    {
        $userId01 = 2;
        $userId02 = 3;
        $credentialId01 = 1;
        $userCredential01 = [
            'user_credential_id' => $credentialId01,
            'brokerId' => 'tinkoff',
            'api_key' => 'ENCRYPT1',
            'is_active' => 0
        ];
        $userCredentialModel01 = new UserCredentialModel($userCredential01);
        $encrypt01 = 'ENCRYPT1';
        $key01 = 'AAAA-BBBB-CCCC';

        $userCredential02 = [
            'user_credential_id' => $credentialId01,
            'brokerId' => 'tinkoff',
            'api_key' => 'ENCRYPT2',
            'is_active' => 0
        ];
        $encrypt02 = 'ENCRYPT2';
        $key02 = 'AAAA-BBBB-CCCC';

        $userCredentialModel02 = new UserCredentialModel($userCredential02);

        $userCredentialEntityResult01 = new UserCredentialEntity();
        $userCredentialEntityResult01->setActive(0);
        $userCredentialEntityResult01->setUserId($userId01);
        $userCredentialEntityResult01->setBrokerId('tinkoff');
        $userCredentialEntityResult01->setApiKey('AAAA-BBBB-CCCC');
        $userCredentialEntityResult01->setId(1);

        $userCredentialEntity01 = new UserCredentialEntity();
        $userCredentialEntity01->setActive(1);
        $userCredentialEntity01->setUserId($userId01);
        $userCredentialEntity01->setBrokerId('bcs');
        $userCredentialEntity01->setApiKey('DDDD-EEEE-FFFF');
        $userCredentialEntity01->setId(1);

        $exception01 = null;
        $exception02 = [
            'class' => UserAccessDeniedException::class,
            'code' => 2022
        ];

        return [
            'Common Case' => [
                $userCredentialEntityResult01,
                $exception01,
                $userCredentialModel01,
                $userCredentialEntity01,
                $userId01,
                $credentialId01,
                ['encrypt' => $encrypt01, 'decrypt' => $key01]
            ],
            'Default is Active Case' => [
                $userCredentialEntityResult01,
                $exception01,
                $userCredentialModel02,
                $userCredentialEntity01,
                $userId01,
                $credentialId01,
                ['encrypt' => $encrypt02, 'decrypt' => $key02]
            ],
            'Credential Access Denied for User' => [
                $userCredentialEntityResult01,
                $exception02,
                $userCredentialModel02,
                $userCredentialEntity01,
                $userId02,
                $credentialId01,
                null
            ]
        ];
    }

    public function testGetUserCredentialDecryptedById()
    {
        $em = $this->createMock(EntityManager::class);
        $metadata = $this->createMock(ClassMetadata::class);

        $userCredentialId = 101;
        $userId01 = 1;

        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);

        $brokerEntity01 = new BrokerEntity();
        $brokerEntity01->setId('bcs');
        $brokerEntity01->setName('БКС');
        $brokerEntity01->setIsActive(1);

        $brokerModel01 = new BrokerModel([
            'brokerId' => 'bcs',
            'name' => 'БКС',
            'isActive' => 1
        ]);

        $userCredentialEntity01 = new UserCredentialEntity();
        $userCredentialEntity01->setActive(1);
        $userCredentialEntity01->setUserId($userId01);
        $userCredentialEntity01->setBrokerId('bcs');
        $userCredentialEntity01->setBroker($brokerEntity01);
        $userCredentialEntity01->setApiKey('ENCRYPT01');
        $userCredentialEntity01->setId(101);
        $userCredentialEntity01->setUserBrokerAccount(
            new PersistentCollection($em, $metadata, new ArrayCollection([$account01, $account02]))
        );

        $expected = new UserCredentialModel([
            'userCredentialId' => 101,
            'isActive' => 1,
            'userId' => 1,
            'brokerId' => 'bcs',
            'broker' => $brokerModel01,
            'apiKey' => 'DDDD-EEEE-FFFF',
            'brokerAccounts' => new UserBrokerAccountCollection([$account01, $account02])
        ]);

        $this->userCredentialStorageMock->expects($this->once())->method('findOneById')
            ->with($userCredentialId)->willReturn($userCredentialEntity01);
        $this->brokerHelper->expects($this->once())->method('convertBrokerEntityToModel')
            ->with($brokerEntity01)
            ->willReturn($brokerModel01);
        $this->encryptService->expects($this->once())->method('decrypt')
            ->with('ENCRYPT01')
            ->willReturn('DDDD-EEEE-FFFF');
        $this->userServiceMock->expects($this->once())->method('getUserId')->willReturn(1);

        $this->assertEquals($expected, $this->userCredentialService->getUserCredentialDecryptedById(101));
    }
}
