<?php

declare(strict_types=1);

namespace Tests\Unit\User\Services;

use App\Common\HttpClient;
use App\User\Models\UserAuthModel;
use App\User\Services\YandexAuthService;
use App\User\Types\AuthType;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class YandexAuthServiceTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    #[DataProvider('dataGetUserInfo')]
    public function testGetUserInfo(
        UserAuthModel $expected,
        string $accessToken,
        string $content
    ): void {
        $body = $this->createMock(StreamInterface::class);
        $body->expects($this->once())->method('getContents')->willReturn($content);
        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())->method('getBody')->willReturn($body);

        $httpClient = $this->getMockBuilder(HttpClient::class)->disableOriginalConstructor()
            ->getMock();
        $httpClient->expects($this->once())->method('doRequest')
            ->with('GET', 'info', ['format' => 'json'], ['Authorization' => 'OAuth ' . $accessToken])
            ->willReturn($response);

        $authService = new YandexAuthService($httpClient);

        $this->assertEquals($expected, $authService->getUserInfo($accessToken));
    }

    public static function dataGetUserInfo(): array
    {
        $user01 = new UserAuthModel([
            'login' => 'a.treschilov@email.ru',
            'access_token' => 'AAAA-BBBB',
            'auth_type' => AuthType::YANDEX,
            'email' => 'a.treschilov@email.ru',
            'client_id' => '1111-2222',
            'birthday' => new \DateTime('1987-09-03')
        ]);
        $token01 = 'AAAA-BBBB';

        $content01 = json_encode([
            'login' => 'a.treschilov',
            'default_email' => 'a.treschilov@email.ru',
            'id' => '1111-2222',
            'birthday' => '1987-09-03'
        ]);

        return [
            'Common Case' => [
                $user01,
                $token01,
                $content01
            ]
        ];
    }
}
