<?php

declare(strict_types=1);

namespace Tests\Unit\User\Storages;

use App\User\Entities\UserBrokerAccountEntity;
use App\User\Storages\UserBrokerAccountStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UserBrokerAccountStorageTest extends TestCase
{
    private MockObject $objectRepositoryMock;
    private MockObject $entityManagerMock;
    private UserBrokerAccountStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepositoryMock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository', 'persist', 'flush'])
            ->getMock();

        $this->storage = new UserBrokerAccountStorage($this->entityManagerMock);
    }

    public function testAddEntity()
    {
        $userBrokerAccountEntity = new UserBrokerAccountEntity();
        $userBrokerAccountEntity->setId(1);

        $this->entityManagerMock->expects($this->once())->method('persist')->with($userBrokerAccountEntity);
        $this->entityManagerMock->expects($this->once())->method('flush');

        $this->storage->addEntity($userBrokerAccountEntity);
    }

    public function testFindByCredentialId()
    {
        $userBrokerAccountEntity1 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity1->setId(1);

        $userBrokerAccountEntity2 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity2->setId(2);

        $userCredentialId = 2;

        $this->objectRepositoryMock->expects($this->once())->method('findBy')
            ->with(['userCredentialId' => $userCredentialId])
            ->willReturn([$userBrokerAccountEntity1, $userBrokerAccountEntity2]);

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserBrokerAccountEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals(
            [$userBrokerAccountEntity1, $userBrokerAccountEntity2],
            $this->storage->findByCredentialId($userCredentialId)
        );
    }

    public function testFindActiveByCredentialId()
    {
        $userBrokerAccountEntity1 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity1->setId(1);

        $userBrokerAccountEntity2 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity2->setId(2);

        $userCredentialId = 2;

        $this->objectRepositoryMock->expects($this->once())->method('findBy')
            ->with(['userCredentialId' => $userCredentialId, 'isActive' => 1, 'isDeleted' => 0])
            ->willReturn([$userBrokerAccountEntity1, $userBrokerAccountEntity2]);

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserBrokerAccountEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals(
            [$userBrokerAccountEntity1, $userBrokerAccountEntity2],
            $this->storage->findActiveByCredentialId($userCredentialId)
        );
    }
}
