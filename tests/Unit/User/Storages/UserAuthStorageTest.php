<?php

declare(strict_types=1);

namespace Tests\Unit\User\Storages;

use App\User\Entities\UserAuthEntity;
use App\User\Storages\UserAuthStorage;
use App\User\Types\AuthType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class UserAuthStorageTest extends TestCase
{
    public function testAddEntity()
    {
        $entity = new UserAuthEntity();

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['persist', 'flush'])
            ->getMock();
        $entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $entityManager->expects($this->once())->method('flush');

        $userAuthStorage = new UserAuthStorage($entityManager);
        $userAuthStorage->addEntity($entity);
    }

    public function testFindActiveByAuthTypeToken()
    {
        $user = new UserAuthEntity();
        $user->setId(1);

        $token = '1111-2222';
        $authType = AuthType::YANDEX;

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findOneBy')
            ->with(['authType' => $authType->value, 'accessToken' => $token, 'isActive' => 1])
            ->willReturn($user);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(UserAuthEntity::class)
            ->willReturn($objectRepository);

        $userAuthStorage = new UserAuthStorage($entityManager);
        $this->assertEquals($user, $userAuthStorage->findActiveByAuthTypeAndToken($authType, $token));
    }

    public function testFindByAuthTypeAndClientId()
    {
        $authType = 'YANDEX';
        $clientId = 'AAAA-BBBB';

        $user = new UserAuthEntity();
        $user->setId(1);

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findOneBy')
            ->with(['authType' => $authType, 'clientId' => $clientId])->willReturn($user);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(UserAuthEntity::class)
            ->willReturn($objectRepository);

        $userAuthStorage = new UserAuthStorage($entityManager);
        $this->assertEquals($user, $userAuthStorage->findByAuthTypeAndClientId($authType, $clientId));
    }
}
