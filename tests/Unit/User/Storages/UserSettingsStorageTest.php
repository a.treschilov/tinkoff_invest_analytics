<?php

namespace Tests\Unit\User\Storages;

use App\User\Entities\UserSettingsEntity;
use App\User\Storages\UserSettingsStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class UserSettingsStorageTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new UserSettingsStorage($this->entityManager);
    }

    public function testFindByUserId()
    {
        $userId01 = 1;

        $userSettingsEntity01 = new UserSettingsEntity();
        $userSettingsEntity01->setId(1);

        $expected = $userSettingsEntity01;

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(UserSettingsEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['userId' => $userId01])
            ->willReturn($userSettingsEntity01);

        $this->assertEquals($expected, $this->storage->findByUserId($userId01));
    }

    public function testAddEntity()
    {
        $userCredential = new UserSettingsEntity();
        $userCredential->setId(1);

        $this->entityManager->expects($this->once())->method('persist')->with($userCredential);
        $this->entityManager->expects($this->once())->method('flush');

        $this->storage->addEntity($userCredential);
    }
}
