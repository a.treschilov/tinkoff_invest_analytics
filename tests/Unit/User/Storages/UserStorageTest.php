<?php

declare(strict_types=1);

namespace Tests\Unit\User\Storages;

use App\User\Entities\UserEntity;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class UserStorageTest extends TestCase
{
    private EntityRepository $objectRepository;
    private EntityManager $entityManager;
    private UserStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new UserStorage($this->entityManager);
    }

    public function testAddEntity()
    {
        $entity = new UserEntity();

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['persist', 'flush'])
            ->getMock();
        $entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $entityManager->expects($this->once())->method('flush');

        $userStorage = new UserStorage($entityManager);
        $userStorage->addEntity($entity);
    }

    public function testFindByLogin()
    {
        $user = new UserEntity();
        $user->setId(1);
        $login = 'a.treschilov';

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findOneBy')
            ->with(['login' => $login])->willReturn($user);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(UserEntity::class)
            ->willReturn($objectRepository);

        $userAuthStorage = new UserStorage($entityManager);
        $this->assertEquals($user, $userAuthStorage->findByLogin($login));
    }

    public function testFindById()
    {
        $userId = 1;
        $user = new UserEntity();
        $user->setId($userId);

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findOneBy')
            ->with(['id' => $userId])->willReturn($user);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(UserEntity::class)
            ->willReturn($objectRepository);

        $userAuthStorage = new UserStorage($entityManager);
        $this->assertEquals($user, $userAuthStorage->findById($userId));
    }

    public function testFindAll()
    {
        $user01 = new UserEntity();
        $user01->setId(1);
        $user02 = new UserEntity();
        $user02->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(UserEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())->method('findAll')
            ->willReturn([$user01, $user02]);

        $this->assertEquals([$user01, $user02], $this->storage->findAll());
    }
}
