<?php

declare(strict_types=1);

namespace Tests\Unit\User\Storages;

use App\User\Entities\UserCredentialEntity;
use App\User\Storages\UserCredentialStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UserCredentialStorageTest extends TestCase
{
    private MockObject $objectRepositoryMock;
    private MockObject $entityManagerMock;
    private UserCredentialStorage $userCredentialStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepositoryMock = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository', 'persist', 'flush'])
            ->getMock();

        $this->userCredentialStorage = new UserCredentialStorage($this->entityManagerMock);
    }

    public function testFindActiveByUserIdAndBroker()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);
        $userId = 2;
        $brokerId = 'tinkoff';

        $this->objectRepositoryMock->expects($this->once())->method('findOneBy')
            ->with(['userId' => $userId, 'brokerId' => $brokerId, 'isActive' => 1])->willReturn($userCredential);
        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals(
            $userCredential,
            $this->userCredentialStorage->findActiveByUserIdAndBroker($userId, $brokerId)
        );
    }

    public function testFindByUserIdAndBroker()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);
        $userId = 2;
        $brokerId = 'tinkoff';

        $this->objectRepositoryMock->expects($this->once())->method('findOneBy')
            ->with(['userId' => $userId, 'brokerId' => $brokerId])->willReturn($userCredential);
        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals($userCredential, $this->userCredentialStorage->findByUserIdAndBroker($userId, $brokerId));
    }

    public function testFindActiveByUserId()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);
        $userId = 2;

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->objectRepositoryMock->expects($this->once())->method('findBy')
            ->with(['userId' => $userId, 'isActive' => 1])->willReturn([$userCredential]);


        $this->assertEquals([$userCredential], $this->userCredentialStorage->findActiveByUserId($userId));
    }

    public function testFindByUser()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);
        $userId = 2;

        $this->objectRepositoryMock->expects($this->once())->method('findBy')
            ->with(['userId' => $userId])->willReturn([$userCredential]);

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals([$userCredential], $this->userCredentialStorage->findByUserId($userId));
    }

    public function testFindActive()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);

        $this->objectRepositoryMock->expects($this->once())->method('findBy')
            ->with(['isActive' => 1])->willReturn([$userCredential]);

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals([$userCredential], $this->userCredentialStorage->findActive());
    }

    public function testFindOneById()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);
        $userCredentialId = 2;

        $this->objectRepositoryMock->expects($this->once())->method('find')
            ->with($userCredentialId)->willReturn($userCredential);

        $this->entityManagerMock->expects($this->once())->method('getRepository')
            ->with(UserCredentialEntity::class)
            ->willReturn($this->objectRepositoryMock);

        $this->assertEquals($userCredential, $this->userCredentialStorage->findOneById($userCredentialId));
    }

    public function testAddEntity()
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(1);

        $this->entityManagerMock->expects($this->once())->method('persist')->with($userCredential);
        $this->entityManagerMock->expects($this->once())->method('flush');

        $this->userCredentialStorage->addEntity($userCredential);
    }
}
