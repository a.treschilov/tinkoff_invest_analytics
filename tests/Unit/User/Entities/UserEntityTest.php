<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserAuthEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\ModelTestCase;

class UserEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserEntity();
    }

    public static function dataTestGetSet(): array
    {
        $userAuth = new UserAuthEntity();
        $userAuth->setId(1);

        $userCredential01 = new UserCredentialEntity();
        $userCredential01->setId(2);

        $userCredential02 = new UserCredentialEntity();
        $userCredential02->setId(3);

        $userCredentialArray = new ArrayCollection([
            $userCredential01, $userCredential02
        ]);

        $birthday = new \DateTime('2022-01-23 00:15:54');

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setLogin' => 'i.ivanov',
                    'setEmail' => 'i.ivanov@yandex.ru',
                    'setUserAuth' => new ArrayCollection([$userAuth]),
                    'setUserCredential' => $userCredentialArray,
                    'setBirthday' => $birthday,
                    'setIsNewUser' => 1
                ],
                [
                    'getId' => 1,
                    'getLogin' => 'i.ivanov',
                    'getEmail' => 'i.ivanov@yandex.ru',
                    'getUserAuth' => new ArrayCollection([$userAuth]),
                    'getUserCredential' => $userCredentialArray,
                    'getBirthday' => $birthday,
                    'getIsNewUser' => 1
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                ],
                [
                    'getId' => 1,
                    'getLogin' => null,
                    'getEmail' => null,
                    'getUserAuth' => null,
                    'getUserCredential' => new ArrayCollection(),
                    'getBirthday' => null,
                    'getIsNewUser' => 1
                ]
            ]
        ];
    }
}
