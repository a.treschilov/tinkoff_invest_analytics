<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use Tests\ModelTestCase;

class UserCredentialTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserCredentialEntity();
    }

    public static function dataTestGetSet(): array
    {
        $user = new UserEntity();
        $user->setId(2);
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUser' => $user,
                    'setUserId' => 2,
                    'setBrokerId' => 'tinkoff',
                    'setApiKey' => 'AAAA-BBBB',
                    'setActive' => 0
                ],
                [
                    'getId' => 1,
                    'getUser' => $user,
                    'getUserId' => 2,
                    'getBrokerId' => 'tinkoff',
                    'getApiKey' => 'AAAA-BBBB',
                    'getActive' => 0
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                    'setUser' => $user,
                    'setUserId' => 2,
                    'setBrokerId' => 'tinkoff',
                    'setApiKey' => 'AAAA-BBBB'
                ],
                [
                    'getId' => 1,
                    'getUser' => $user,
                    'getUserId' => 2,
                    'getBrokerId' => 'tinkoff',
                    'getApiKey' => 'AAAA-BBBB',
                    'getActive' => '1'
                ]
            ]
        ];
    }
}
