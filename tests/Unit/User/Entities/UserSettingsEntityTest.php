<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserSettingsEntity;
use Tests\ModelTestCase;

class UserSettingsEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserSettingsEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setDesiredPensionAmount' => 100.9,
                    'setDesiredPensionCurrency' => 'RUB',
                    'setRetirementAge' => 45,
                    'setExpensesAmount' => 10000,
                    'setExpensesCurrency' => 'RUB'
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getDesiredPensionAmount' => 100.9,
                    'getDesiredPensionCurrency' => 'RUB',
                    'getRetirementAge' => 45,
                    'getExpensesAmount' => 10000,
                    'getExpensesCurrency' => 'RUB'
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setDesiredPensionCurrency' => 'RUB',
                    'setExpensesCurrency' => 'RUB'
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getDesiredPensionAmount' => null,
                    'getDesiredPensionCurrency' => 'RUB',
                    'getRetirementAge' => null,
                    'getExpensesAmount' => null,
                    'getExpensesCurrency' => 'RUB'
                ]
            ]
        ];
    }
}
