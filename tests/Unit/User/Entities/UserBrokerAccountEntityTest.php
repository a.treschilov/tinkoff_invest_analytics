<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use Tests\ModelTestCase;

class UserBrokerAccountEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new UserBrokerAccountEntity();
    }

    public static function dataTestGetSet(): array
    {
        $userCredential = new UserCredentialEntity();
        $userCredential->setId(3);

        $createdDate = new \DateTime('2022-03-01 12:14:18');
        $updatedDate = new \DateTime('2022-03-28 18:12:21');

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserCredentialId' => 2,
                    'setUserCredential' => $userCredential,
                    'setExternalId' => 'AAAA-BBBB',
                    'setName' => 'Primary Account',
                    'setType' => 1,
                    'setIsActive' => 1,
                    'setIsDeleted' => 1,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserCredentialId' => 2,
                    'getUserCredential' => $userCredential,
                    'getExternalId' => 'AAAA-BBBB',
                    'getName' => 'Primary Account',
                    'getType' => 1,
                    'getIsActive' => 1,
                    'getIsDeleted' => 1,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setUserCredentialId' => 2,
                    'setUserCredential' => $userCredential,
                    'setExternalId' => 'AAAA-BBBB',
                    'setType' => 1,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => null,
                    'getUserCredentialId' => 2,
                    'getUserCredential' => $userCredential,
                    'getExternalId' => 'AAAA-BBBB',
                    'getName' => null,
                    'getType' => 1,
                    'getIsActive' => 0,
                    'getIsDeleted' => 0,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
