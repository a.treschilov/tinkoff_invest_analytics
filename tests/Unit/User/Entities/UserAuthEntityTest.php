<?php

declare(strict_types=1);

namespace Tests\Unit\User\Entities;

use App\User\Entities\UserAuthEntity;
use App\User\Entities\UserEntity;
use Tests\ModelTestCase;

class UserAuthEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new UserAuthEntity();
    }

    public static function dataTestGetSet(): array
    {
        $user = new UserEntity();
        $user->setId(1);
        $authDate = new \DateTime();
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUser' => $user,
                    'setUserId' => 1,
                    'setIsActive' => 1,
                    'setAuthDate' => $authDate,
                    'setAuthType' => 'YANDEX',
                    'setAccessToken' => 'AAAA-BBBB',
                    'setClientId' => '1111-2222'
                ],
                [
                    'getId' => 1,
                    'getUser' => $user,
                    'getUserId' => 1,
                    'isActive' => 1,
                    'getAuthDate' => $authDate,
                    'getAuthType' => 'YANDEX',
                    'getAccessToken' => 'AAAA-BBBB',
                    'getClientId' => '1111-2222'
                ]
            ],
            'Empty Fields Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 1,
                    'setIsActive' => 1,
                    'setAuthDate' => $authDate,
                    'setAuthType' => 'YANDEX',
                    'setAccessToken' => 'AAAA-BBBB',
                    'setClientId' => '1111-2222'
                ],
                [
                    'getId' => 1,
                    'getUser' => null,
                    'getUserId' => 1,
                    'isActive' => 1,
                    'getAuthDate' => $authDate,
                    'getAuthType' => 'YANDEX',
                    'getAccessToken' => 'AAAA-BBBB',
                    'getClientId' => '1111-2222'
                ]
            ]
        ];
    }
}
