<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Item\Collections\OperationCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Formatters\OperationListFormatter;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class OperationListFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    #[DataProvider('dataFormat')]
    public function testFormat(array $expected, OperationCollection $operationCollection): void
    {
        $formatter = new OperationListFormatter($operationCollection);
        $this->assertEquals($expected, $formatter->format());
    }

    public static function dataFormat(): array
    {
        $date01 = new \DateTime('2021-12-11 15:05:28');
        $date02 = new \DateTime('2021-12-11 15:05:29');
        $instrumentEntity01 = new MarketInstrumentEntity();
        $instrumentEntity01->setName('Лукойл');
        $instrumentEntity01->setType('Stock');

        $operation01 = new OperationModel([
            'itemOperationId' => 1,
            'externalId' => 'ex01',
            'brokerId' => 'tinkoff',
            'status' => 'Done',
            'itemId' => 1,
            'currencyIso' => 'RUB',
            'amount' => 50,
            'quantity' => 5,
            'item' => new ItemModel([
                'itemId' => 1,
                'name' => 'Лукойл',
                'type' => 'market',
                'externalId' => 'RU12345',
                "logo" => "https://cdn.hakkes.com/tinkoff.png",
            ]),
            'date' => $date01,
            'operationType' => 'Buy'
        ]);
        $operation02 = new OperationModel([
            'itemOperationId' => 2,
            'externalId' => 'ex02',
            'brokerId' => 'tinkoff',
            'status' => 'Done',
            'currencyIso' => 'RUB',
            'amount' => 50,
            'quantity' => null,
            'date' => $date02,
            'operationType' => 'Fee',
            'item' => null,
            'itemId' => null,
        ]);

        $operationCollection01 = new OperationCollection([$operation01, $operation02]);

        $expected01 = [
            [
                'id' => 1,
                'externalId' => 'ex01',
                'type' => 'Buy',
                'itemId' => 1,
                'itemName' => 'Лукойл',
                'itemType' => 'market',
                "itemLogo" => "https://cdn.hakkes.com/tinkoff.png",
                'itemExternalId' => 'RU12345',
                'broker' => 'tinkoff',
                'quantity' => 5,
                'amount' => 50,
                'currency' => 'RUB',
                'date' => $date01,
                'status' => 'Done'
            ],
            [
                'id' => 2,
                'externalId' => 'ex02',
                'type' => 'Fee',
                'itemId' => null,
                'itemName' => null,
                'itemLogo' => null,
                'itemType' => null,
                'itemExternalId' => null,
                'broker' => 'tinkoff',
                'quantity' => null,
                'amount' => 50,
                'currency' => 'RUB',
                'date' => $date02,
                'status' => 'Done'
            ]
        ];

        return [
            'Common Case' => [
                $expected01, $operationCollection01
            ]
        ];
    }
}
