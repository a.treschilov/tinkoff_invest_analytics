<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\CountryFormatter;
use App\Intl\Collections\CountryCollection;
use Monolog\Test\TestCase;
use Tests\TITestCase;

class CountryFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat(): void
    {
        $country01 = TITestCase::createCountryEntity(['iso' => 'RU', 'name' => 'Russia', 'continent' => 'Europe']);
        $country02 = TITestCase::createCountryEntity(['iso' => 'JP', 'name' => 'Japan', 'continent' => 'Asia']);
        $country03 = TITestCase::createCountryEntity([
            'iso' => 'CA',
            'name' => 'Canada',
            'continent' => 'South America'
        ]);
        $countryCollection = new CountryCollection([$country01, $country02, $country03]);

        $expected = [
            ['iso' => 'RU', 'name' => 'Russia', 'continent' => 'Europe'],
            ['iso' => 'JP', 'name' => 'Japan', 'continent' => 'Asia'],
            ['iso' => 'CA', 'name' => 'Canada', 'continent' => 'South America']
        ];

        $countryFormatter = new CountryFormatter($countryCollection);
        $this->assertEquals($expected, $countryFormatter->format());
    }
}
