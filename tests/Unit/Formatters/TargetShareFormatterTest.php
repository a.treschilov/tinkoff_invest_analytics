<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Collections\ShareCollection;
use App\Formatters\TargetShareFormatter;
use App\Models\TargetShareModel;
use PHPUnit\Framework\TestCase;

class TargetShareFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $shareCollection = new ShareCollection([
            new TargetShareModel([
                'name' => 'stock',
                'value' => 0.3,
                'externalId' => 'Stock'
            ]),
            new TargetShareModel([
                'name' => 'gold',
                'value' => 0.45,
                'externalId' => 'Gold'
            ]),
            new TargetShareModel([
                'name' => 'etf',
                'value' => 0,
                'externalId' => 'Etf'
            ])
        ]);
        $expected = [
            [
                'name' => 'stock',
                'value' => 0.3,
                'externalId' => 'Stock'
            ],
            [
                'name' => 'gold',
                'value' => 0.45,
                'externalId' => 'Gold'
            ],
            [
                'name' => 'etf',
                'value' => 0,
                'externalId' => 'Etf'
            ],
        ];
        $formatter = new TargetShareFormatter($shareCollection);
        $this->assertEquals($expected, $formatter->format());
    }
}
