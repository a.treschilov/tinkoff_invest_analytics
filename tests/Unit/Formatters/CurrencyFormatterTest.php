<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\CurrencyFormatter;
use App\Intl\Collections\CurrencyCollection;
use Monolog\Test\TestCase;
use Tests\TITestCase;

class CurrencyFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $currency01 = TITestCase::createCurrencyEntity([
            'id' => 1,
            'iso' => 'RUB',
            'name' => 'Российский рубль',
            'symbol' => '₽'
        ]);
        $currency02 = TITestCase::createCurrencyEntity([
            'id' => 2,
            'iso' => 'USD',
            'name' => 'Доллар США',
            'symbol' => '$'
        ]);
        $currency03 = TITestCase::createCurrencyEntity([
            'id' => 3,
            'iso' => 'EUR',
            'name' => 'Евро',
            'symbol' => '€'
        ]);
        $collection = new CurrencyCollection([$currency01, $currency02, $currency03]);

        $expected = [
            [
                'id' => 1,
                'iso' => 'RUB',
                'name' => 'Российский рубль',
                'symbol' => '₽'
            ],
            [
                'id' => 2,
                'iso' => 'USD',
                'name' => 'Доллар США',
                'symbol' => '$'
            ],
            [
                'id' => 3,
                'iso' => 'EUR',
                'name' => 'Евро',
                'symbol' => '€'
            ]
        ];

        $formatter = new CurrencyFormatter($collection);
        $this->assertEquals($expected, $formatter->format());
    }
}
