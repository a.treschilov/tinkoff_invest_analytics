<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\PriceFormatter;
use App\Models\PriceModel;
use PHPUnit\Framework\TestCase;

class PriceFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $priceModel = new PriceModel(['amount' => 10, 'currency' => 'USD']);
        $expected = ['amount' => 10, 'currency' => 'USD'];

        $formatter = new PriceFormatter($priceModel);
        $this->assertEquals($expected, $formatter->format());
    }
}
