<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\UserCredentialFormatter;
use App\Broker\Models\BrokerModel;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Models\UserCredentialModel;
use Monolog\Test\TestCase;

class UserCredentialFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);
        $credential01 = new UserCredentialModel();
        $credential01->setUserCredentialId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBrokerId('tinkoff');
        $credential01->setIsActive(1);
        $credential01->setBrokerAccounts(new UserBrokerAccountCollection([$account01, $account02]));
        $credential01->setBroker(new BrokerModel(['name' => 'Tinkoff']));

        $expected01 = [
                'userCredentialId' => 1,
                'apiKey' => 'AAAA-BBBB-CCCC',
                'isActive' => 1,
                'brokerId' => 'tinkoff',
                'brokerName' => 'Tinkoff',
                'activeAccountNumber' => 1,
                'accountNumber' => 2
            ];

        $formatter = new UserCredentialFormatter($credential01);
        $this->assertEquals($expected01, $formatter->format());
    }
}
