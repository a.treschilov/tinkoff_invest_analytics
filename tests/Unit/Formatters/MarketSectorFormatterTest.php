<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\CountryFormatter;
use App\Formatters\CurrencyFormatter;
use App\Formatters\MarketSectorFormatter;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;
use Monolog\Test\TestCase;
use Tests\TITestCase;

class MarketSectorFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $sector01 = TITestCase::createMarketSectorEntity([
            'id' => 1,
            'name' => 'Utilities'
        ]);
        $sector02 = TITestCase::createMarketSectorEntity([
            'id' => 2,
            'name' => 'Technology'
        ]);
        $sector03 = TITestCase::createMarketSectorEntity([
            'id' => 3,
            'name' => 'Consumer Defensive'
        ]);
        $collection = new MarketSectorCollection([$sector01, $sector02, $sector03]);

        $expected = [
            [
                'id' => 1,
                'name' => 'Utilities'
            ],
            [
                'id' => 2,
                'name' => 'Technology'
            ],
            [
                'id' => 3,
                'name' => 'Consumer Defensive'
            ]
        ];

        $formatter = new MarketSectorFormatter($collection);
        $this->assertEquals($expected, $formatter->format());
    }
}
