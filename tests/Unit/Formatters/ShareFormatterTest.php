<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Collections\CompareCollection;
use App\Formatters\ShareFormatter;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ShareFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    #[DataProvider('dataFormat')]
    public function testFormat($expected, $shareCollection): void
    {
        $formatter = new ShareFormatter($shareCollection);
        $this->assertEquals($expected, $formatter->format());
    }

    public static function dataFormat(): array
    {
        $share01 = [
            "targetShare" => 0.2,
            "diffPercent" => -0.01,
            "diffAmount" => 500
        ];
        $share02 = [
            "targetShare" => 0.6,
            "diffPercent" => 0.05,
            "diffAmount" => 2500
        ];

        $compareCollection01 = new CompareCollection([
            'stock' => $share01,
            'bond' => $share02
        ]);
        $compareCollection02 = new CompareCollection();

        $expected01 = [
            [
                "targetShare" => 0.2,
                "diffPercent" => -0.01,
                "diffAmount" => 500,
                "name" => 'stock'
            ],
            [
                "targetShare" => 0.6,
                "diffPercent" => 0.05,
                "diffAmount" => 2500,
                "name" => 'bond'
            ]
        ];
        $expected02 = [];

        return [
            'Common Case' => [
                $expected01,
                $compareCollection01
            ],
            'Empty Case' => [
                $expected02,
                $compareCollection02
            ]
        ];
    }
}
