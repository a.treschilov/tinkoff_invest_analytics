<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\UserCredentialListFormatter;
use App\Broker\Models\BrokerModel;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Models\UserCredentialModel;
use Monolog\Test\TestCase;

class UserCredentialFormatterListTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);

        $credential01 = new UserCredentialModel();
        $credential01->setUserCredentialId(1);
        $credential01->setApiKey('AAAA-BBBB-CCCC');
        $credential01->setBrokerId('tinkoff');
        $credential01->setIsActive(1);
        $credential01->setBrokerAccounts(new UserBrokerAccountCollection([$account01]));
        $credential01->setBroker(new BrokerModel(['name' => 'Tinkoff']));

        $credential02 = new UserCredentialModel();
        $credential02->setUserCredentialId(2);
        $credential02->setApiKey('DDDD-EEEE-FFFF');
        $credential02->setBrokerId('bcs');
        $credential02->setIsActive(0);
        $credential02->setBrokerAccounts(new UserBrokerAccountCollection([$account01, $account02]));
        $credential02->setBroker(new BrokerModel(['name' => 'БКС']));

        $expected01 = [
            [
                'userCredentialId' => 1,
                'apiKey' => 'AAAA-BBBB-CCCC',
                'isActive' => 1,
                'brokerId' => 'tinkoff',
                'brokerName' => 'Tinkoff',
                'activeAccountNumber' => 1,
                'accountNumber' => 1
            ],
            [
                'userCredentialId' => 2,
                'apiKey' => 'DDDD-EEEE-FFFF',
                'isActive' => 0,
                'brokerId' => 'bcs',
                'brokerName' => 'БКС',
                'activeAccountNumber' => 1,
                'accountNumber' => 2
            ]
        ];

        $collection = new UserCredentialCollection([$credential01, $credential02]);
        $formatter = new UserCredentialListFormatter($collection);
        $this->assertEquals($expected01, $formatter->format());
    }
}
