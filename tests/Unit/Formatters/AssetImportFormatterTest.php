<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Assets\Models\ImportStatisticModel;
use App\Formatters\AssetImportFormatter;
use PHPUnit\Framework\TestCase;

class AssetImportFormatterTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testFormat()
    {
        $model = new ImportStatisticModel([
            'added' => 10,
            'updated' => 20,
            'skipped' => 30
        ]);

        $expected = [
            'added' => 10,
            'updated' => 20,
            'skipped' => 30
        ];

        $formatter = new AssetImportFormatter($model);

        $this->assertEquals($expected, $formatter->format());
    }
}
