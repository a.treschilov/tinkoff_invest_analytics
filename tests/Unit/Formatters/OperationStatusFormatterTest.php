<?php

declare(strict_types=1);

namespace Tests\Unit\Formatters;

use App\Formatters\OperationStatusFormatter;
use App\Item\Collections\OperationStatusCollection;
use App\Item\Models\OperationStatusModel;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class OperationStatusFormatterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    #[DataProvider('dataFormat')]
    public function testFormat($expected, $operationStatusCollection)
    {
        $formatter = new OperationStatusFormatter($operationStatusCollection);
        $this->assertEquals($expected, $formatter->format());
    }

    public static function dataFormat(): array
    {
        $operationStatus01 = new OperationStatusModel([
            'operationStatusId' => 1,
            'name' => 'Выполнена',
            'externalId' => 'Done'
        ]);
        $operationStatus02 = new OperationStatusModel([
            'operationStatusId' => 2,
            'name' => 'Отклонена',
            'externalId' => 'Decline'
        ]);

        $operationStatusCollection01 = new OperationStatusCollection([$operationStatus01, $operationStatus02]);
        $operationStatusCollection02 = new OperationStatusCollection();

        $expected01 = [
            [
                'id' => 'Done',
                'name' => 'Выполнена'
            ],
            [
                'id' => 'Decline',
                'name' => 'Отклонена'
            ]
        ];
        $expected02 = [];

        return [
            'Common Case' => [
                $expected01,
                $operationStatusCollection01
            ],
            'Empty Case' => [
                $expected02,
                $operationStatusCollection02
            ]
        ];
    }
}
