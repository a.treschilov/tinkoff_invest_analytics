<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketSectorEntity;
use App\Entities\StrategySectorEntity;
use Tests\ModelTestCase;

class StrategySectorEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new StrategySectorEntity();
    }

    public static function dataTestGetSet(): array
    {
        $sector = new MarketSectorEntity();
        $sector->setId(1);
        $createdDate = new \DateTime('2022-06-06 12:49:12');
        $updatedDate = new \DateTime('2022-06-27 14:21:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketSectorId' => 3,
                    'setIsActive' => 0,
                    'setMarketSector' => $sector,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketSectorId' => 3,
                    'getIsActive' => 0,
                    'getMarketSector' => $sector,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketSectorId' => 3,
                    'setMarketSector' => $sector,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketSectorId' => 3,
                    'getIsActive' => 1,
                    'getMarketSector' => $sector,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
