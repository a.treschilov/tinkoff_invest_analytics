<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\InstrumentTypeEntity;
use App\Entities\StrategyInstrumentTypeEntity;
use Tests\ModelTestCase;

class StrategyInstrumentTypeEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new StrategyInstrumentTypeEntity();
    }

    public static function dataTestGetSet(): array
    {
        $instrumentType = new InstrumentTypeEntity();
        $instrumentType->setId(2);

        $createdDate = new \DateTime('2022-05-10 00:15:21');
        $updatedDate = new \DateTime('2022-05-11 12:15:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setInstrumentType' => $instrumentType,
                    'setInstrumentTypeId' => 1,
                    'setIsActive' => 0,
                    'setShare' => 0.2,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getInstrumentType' => $instrumentType,
                    'getInstrumentTypeId' => 1,
                    'getIsActive' => 0,
                    'getShare' => 0.2,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Valued Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setInstrumentType' => $instrumentType,
                    'setInstrumentTypeId' => 1,
                    'setShare' => 0.2,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getInstrumentType' => $instrumentType,
                    'getInstrumentTypeId' => 1,
                    'getIsActive' => 1,
                    'getShare' => 0.2,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
