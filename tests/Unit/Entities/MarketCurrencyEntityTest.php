<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCurrencyEntity;
use App\Market\Entities\MarketInstrumentEntity;
use Tests\ModelTestCase;

class MarketCurrencyEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new MarketCurrencyEntity();
    }

    public static function dataTestGetSet(): array
    {
        $marketInstrument = new MarketInstrumentEntity();
        $marketInstrument->setId(1);
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setIso' => 'EUR',
                    'setMarketInstrumentId' => 1,
                    'setMarketInstrument' => $marketInstrument
                ],
                [
                    'getId' => 1,
                    'getIso' => 'EUR',
                    'getMarketInstrumentId' => 1,
                    'getMarketInstrument' => $marketInstrument
                ]
            ],
            'Null Case' => [
                [
                    'setId' => 2,
                    'setIso' => 'RUB'
                ],
                [
                    'getId' => 2,
                    'getIso' => 'RUB',
                    'getMarketInstrumentId' => null,
                    'getMarketInstrument' => null
                ]
            ]
        ];
    }
}
