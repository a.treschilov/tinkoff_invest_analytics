<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\OperationStatusEntity;
use Tests\ModelTestCase;

class OperationStatusEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new OperationStatusEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setExternalId' => 'Buy',
                    'setName' => 'Покупка'
                ],
                [
                    'getId' => 1,
                    'getExternalId' => 'Buy',
                    'getName' => 'Покупка'
                ]
            ],
            'Empty Name Case' => [
                [
                    'setExternalId' => 'Buy'
                ],
                [
                    'getExternalId' => 'Buy',
                    'getName' => ''
                ]
            ]
        ];
    }
}
