<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCurrencyEntity;
use App\Entities\StrategyCurrencyBalanceEntity;
use Tests\ModelTestCase;

class StrategyCurrencyBalanceEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new StrategyCurrencyBalanceEntity();
    }

    public static function dataTestGetSet(): array
    {
        $createdDate = new \DateTime('2022-05-21 12:49:12');
        $updatedDate = new \DateTime('2022-05-23 14:21:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setCurrencyIso' => 'RUB',
                    'setIsActive' => 0,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getCurrencyIso' => 'RUB',
                    'getIsActive' => 0,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setCurrencyIso' => 'RUB',
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getCurrencyIso' => 'RUB',
                    'getIsActive' => 1,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
