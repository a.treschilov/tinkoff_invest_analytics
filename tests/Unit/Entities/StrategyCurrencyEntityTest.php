<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\StrategyCurrencyEntity;
use App\Intl\Entities\CurrencyEntity;
use Tests\ModelTestCase;

class StrategyCurrencyEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new StrategyCurrencyEntity();
    }

    public static function dataTestGetSet(): array
    {
        $currency = new CurrencyEntity();
        $currency->setId(1);
        $createdDate = new \DateTime('2022-05-21 12:49:12');
        $updatedDate = new \DateTime('2022-05-23 14:21:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setCurrencyId' => 3,
                    'setIsActive' => 0,
                    'setCurrency' => $currency,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getCurrencyId' => 3,
                    'getIsActive' => 0,
                    'getCurrency' => $currency,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setCurrencyId' => 3,
                    'setCurrency' => $currency,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getCurrencyId' => 3,
                    'getIsActive' => 1,
                    'getCurrency' => $currency,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
