<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCountryEntity;
use Tests\ModelTestCase;

class MarketCountryEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new MarketCountryEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setIso' => 'RU',
                ],
                [
                    'getId' => 1,
                    'getIso' => 'RU'
                ]
            ]
        ];
    }
}
