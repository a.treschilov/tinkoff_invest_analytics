<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Intl\Entities\CountryEntity;
use Tests\ModelTestCase;

class MarketStockEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new MarketStockEntity();
    }

    public static function dataTestGetSet(): array
    {
        $marketInstrument = new MarketInstrumentEntity();
        $marketInstrument->setIsin('123');

        $country = new CountryEntity();
        $country->setName('Russia');
        $country->setIso('RU');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setMarketInstrumentId' => 10,
                    'setCountryIso' => 'country',
                    'setSector' => 'sector',
                    'setIndustry' => 'industry',
                    'setMarketInstrument' => $marketInstrument,
                    'setCountry' => $country
                ],
                [
                    'getId' => 1,
                    'getMarketInstrumentId' => 10,
                    'getCountryIso' => 'country',
                    'getSector' => 'sector',
                    'getIndustry' => 'industry',
                    'getMarketInstrument' => $marketInstrument,
                    'getCountry' => $country
                ]
            ],
            'Empty fields Test' => [
                [
                    'setId' => 1,
                    'setMarketInstrumentId' => 10,
                    'setMarketInstrument' => $marketInstrument,
                ],
                [
                    'getId' => 1,
                    'getMarketInstrumentId' => 10,
                    'getCountry' => null,
                    'getSector' => null,
                    'getIndustry' => null,
                    'getMarketInstrument' => $marketInstrument,
                ]
            ]
        ];
    }
}
