<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\InstrumentTypeEntity;
use Tests\ModelTestCase;

class InstrumentTypeEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new InstrumentTypeEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setExternalId' => 'Stock',
                    'setIsActive' => 0,
                    'setName' => 'Stock'
                ],
                [
                    'getId' => 1,
                    'getExternalId' => 'Stock',
                    'getIsActive' => 0,
                    'getName' => 'Stock'
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setExternalId' => 'Gold',
                    'setName' => 'Gold'
                ],
                [
                    'getId' => 1,
                    'getExternalId' => 'Gold',
                    'getIsActive' => 1,
                    'getName' => 'Gold'
                ]
            ]
        ];
    }
}
