<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use Tests\ModelTestCase;

class MarketSectorEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new \App\Entities\MarketSectorEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setName' => 'Technology'
                ],
                [
                    'getId' => 1,
                    'getName' => 'Technology'
                ]
            ]
        ];
    }
}
