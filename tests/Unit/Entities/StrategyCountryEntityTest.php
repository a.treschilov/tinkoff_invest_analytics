<?php

declare(strict_types=1);

namespace Tests\Unit\Entities;

use App\Entities\MarketCountryEntity;
use App\Entities\StrategyCountryEntity;
use Tests\ModelTestCase;

class StrategyCountryEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new StrategyCountryEntity();
    }

    public static function dataTestGetSet(): array
    {
        $country = new MarketCountryEntity();
        $country->setId(1);
        $createdDate = new \DateTime('2022-05-21 12:49:12');
        $updatedDate = new \DateTime('2022-05-23 14:21:54');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketCountryId' => 3,
                    'setIsActive' => 0,
                    'setMarketCountry' => $country,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketCountryId' => 3,
                    'getIsActive' => 0,
                    'getMarketCountry' => $country,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setShare' => 0.4,
                    'setMarketCountryId' => 3,
                    'setMarketCountry' => $country,
                    'setCreatedDate' => $createdDate,
                    'setUpdatedDate' => $updatedDate
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getShare' => 0.4,
                    'getMarketCountryId' => 3,
                    'getIsActive' => 1,
                    'getMarketCountry' => $country,
                    'getCreatedDate' => $createdDate,
                    'getUpdatedDate' => $updatedDate
                ]
            ]
        ];
    }
}
