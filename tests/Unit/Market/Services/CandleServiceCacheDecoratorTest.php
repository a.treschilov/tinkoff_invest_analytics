<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Market\Entities\CandleEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Interfaces\CandleServiceInterface;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Services\CandleBrokerService;
use App\Market\Services\CandleDbService;
use App\Market\Services\CandleDbStoreServiceDecorator;
use App\Market\Storages\CandleStorage;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TITestCase;

class CandleServiceCacheDecoratorTest extends TestCase
{
    private CandleServiceInterface $candleService;
    private CandleDbService $candleDbService;
    private CandleStorage $candleStorage;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $now;

    private CandleDbStoreServiceDecorator $candleServiceCacheDecorator;

    public function setUp(): void
    {
        parent::setUp();

        $this->now = new \DateTime('2022-06-17 19:52:55');

        $this->candleService = $this->createMock(CandleBrokerService::class);
        $this->candleDbService = $this->createMock(CandleDbService::class);
        $this->candleStorage = $this->createMock(CandleStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);

        $this->candleServiceCacheDecorator = new CandleDbStoreServiceDecorator(
            $this->candleService,
            $this->candleDbService,
            $this->candleStorage,
            $this->marketInstrumentService,
            $this->now
        );
    }

    #[DataProvider('dataGetDayCandleByMarketInstrument')]
    public function testGetDayCandleByMarketInstrument(
        BrokerCandleModel $expected,
        int $marketInstrumentId,
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date,
        ?CandleEntity $candle,
        ?BrokerCandleModel $candleModel,
        ?CandleEntity $addedCandle
    ): void {
        $this->candleStorage->expects($this->once())->method('findOneFinalByInstrumentIdAndDate')
            ->with($marketInstrumentId, '1day', $date)
            ->willReturn($candle);

        if (null === $candle) {
            $this->candleService->expects($this->once())->method('getDayCandleByMarketInstrument')
                ->with($marketInstrumentEntity, $date)
                ->willReturn($candleModel);

            $this->candleStorage->expects($this->once())->method('addEntity')
                ->with($addedCandle);
        }

        $this->assertEquals(
            $expected,
            $this->candleServiceCacheDecorator->getDayCandleByMarketInstrument($marketInstrumentEntity, $date)
        );
    }

    public static function dataGetDayCandleByMarketInstrument(): array
    {
        $now = new \DateTime('2022-06-17 19:52:55');
        $marketInstrumentId01 = 1;

        $marketInstrumentEntity01 = new MarketInstrumentEntity();
        $marketInstrumentEntity01->setId(1);

        $date01 = new \DateTime('2022-06-17 19:16:00');

        $candle01 = new CandleEntity();
        $candle01->setOpen(8);
        $candle01->setClose(9);
        $candle01->setHigh(10);
        $candle01->setLow(7);
        $candle01->setTime(new \DateTime('2022-06-17 17:00:00'));
        $candle01->setVolume(145);
        $candle01->setCurrency('RUB');
        $candle02 = null;

        $candleModel01 = null;
        $candleModel02 = new BrokerCandleModel([
            'open' => 10,
            'close' => 14,
            'high' => 15,
            'low' => 9,
            'time' => new \DateTime('2022-06-17 20:00:00'),
            'volume' => 184,
            'currency' => 'USD',
            'isComplete' => true
        ]);
        $addedCandle01 = null;
        $addedCandle02 = new CandleEntity();
        $addedCandle02->setMarketInstrumentId(1);
        $addedCandle02->setFinal(1);
        $addedCandle02->setOpen(10);
        $addedCandle02->setClose(14);
        $addedCandle02->setLow(9);
        $addedCandle02->setHigh(15);
        $addedCandle02->setCurrency('USD');
        $addedCandle02->setVolume(184);
        $addedCandle02->setTime(new \DateTime('2022-06-17 20:00:00'));
        $addedCandle02->setCandleInterval('1day');
        $addedCandle02->setCreatedDate($now);

        $expected01 = new BrokerCandleModel([
            'open' => 8,
            'close' => 9,
            'high' => 10,
            'low' => 7,
            'time' => new \DateTime('2022-06-17 17:00:00'),
            'currency' => 'RUB',
            'volume' => 145
        ]);
        $expected02 = new BrokerCandleModel([
            'open' => 10,
            'close' => 14,
            'high' => 15,
            'low' => 9,
            'time' => new \DateTime('2022-06-17 20:00:00'),
            'currency' => 'USD',
            'volume' => 184,
            'isComplete' => true
        ]);

        return [
            'Common Case' => [
                $expected01,
                $marketInstrumentId01,
                $marketInstrumentEntity01,
                $date01,
                $candle01,
                $candleModel01,
                $addedCandle01
            ],
            'New Candle Case' => [
                $expected02,
                $marketInstrumentId01,
                $marketInstrumentEntity01,
                $date01,
                $candle02,
                $candleModel02,
                $addedCandle02
            ]
        ];
    }
}
