<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Services;

use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\CandleCollection;
use App\Common\Amqp\AmqpClient;
use App\Market\Services\CandleBrokerService;
use App\Market\Services\MarketInstrumentHelperService;
use App\Market\Storages\CandleImportStorage;
use App\Market\Types\CandleIntervalType;
use App\Services\StockBrokerFactory;
use Monolog\Logger;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Stub\Exception;
use Tests\TITestCase;

class CandleServiceTest extends TestCase
{
    private CandleBrokerService $candleService;
    private CandleImportStorage $candleImportStorage;
    private AmqpClient $amqpClient;
    private MarketInstrumentHelperService $marketInstrumentHelperService;
    private Logger $logger;
    private SourceBrokerInterface $sourceBroker;


    public function setUp(): void
    {
        parent::setUp();

        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->candleImportStorage = $this->createMock(CandleImportStorage::class);
        $this->amqpClient = $this->createMock(AmqpClient::class);
        $this->marketInstrumentHelperService = $this->createMock(MarketInstrumentHelperService::class);
        $this->logger = $this->createMock(Logger::class);
        $this->sourceBroker = $this->createMock(SourceBrokerInterface::class);


        $date = new \DateTime('2023-04-09 00:34:20');
        $this->candleService = new CandleBrokerService(
            $this->sourceBroker,
            $date,
            $this->candleImportStorage,
            $this->amqpClient,
            $this->marketInstrumentHelperService,
            $this->logger
        );
    }

    /**
     * @param BrokerCandleModel|null $expected
     * @param $exception
     * @param CandleCollection|Exception $candles
     * @param BrokerPortfolioPositionModel $portfolioPosition
     * @param string $figi
     * @throws \App\Exceptions\AccountException
     * @throws \App\Exceptions\ManyRequestException
     */
    #[DataProvider('dataGetTodayCandle')]
    public function testGetTodayCandle(
        ?BrokerCandleModel $expected,
        ?array $exception,
        CandleCollection|Exception $candles,
        BrokerPortfolioPositionModel $portfolioPosition,
        string $isin
    ): void {
        $from = new \DateTime();
        $from->setTime(0, 0, 0);
        $to = new \DateTime();
        $to->setTime(23, 59, 59);

        $this->sourceBroker->expects($this->once())->method('getHistoryCandles')
            ->with($isin, $from, $to, CandleIntervalType::HOUR_1)
            ->willReturn($candles);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }
        $this->assertEquals($expected, $this->candleService->getTodayCandle($portfolioPosition->getIsin()));
    }

    public static function dataGetTodayCandle(): array
    {
        $instrument01 = TITestCase::createBrokerPortfolioPosition(['isin' => 'RU02342F333']);
        $isin01 = 'RU02342F333';
        $candle01 = TITestCase::createBrokerCandle([
            'open' => 8,
            'close' => 9,
            'time' => new \DateTime('2022-01-01 04:00:00')
        ]);
        $candle02 = TITestCase::createBrokerCandle([
            'open' => 9,
            'close' => 10,
            'time' => new \DateTime('2022-01-01 04:05:00')
        ]);
        return [
            'Common Case' => [
                $candle02,
                null,
                new CandleCollection([$candle01, $candle02]),
                $instrument01,
                $isin01
            ]
        ];
    }
}
