<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Storages;

use App\Market\Entities\CandleEntity;
use App\Market\Storages\CandleStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\LazyCriteriaCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CandleStorageTest extends TestCase
{
    private MockObject $entityManager;
    private CandleStorage $candleStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->candleStorage = new CandleStorage($this->entityManager);
    }

    public function testAddEntity()
    {
        $entity = new CandleEntity();

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['persist', 'flush'])
            ->getMock();
        $entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $entityManager->expects($this->once())->method('flush');

        $candleStorage = new CandleStorage($entityManager);
        $candleStorage->addEntity($entity);
    }

    public function testFindOneByInstrumentIdAndIntervalAndAfterDate()
    {
        $createdDate = new \DateTime('2022-01-06 10:00');
        $dateTime = new \DateTime('2022-01-06 22:45');
        $marketInstrumentId = 1;
        $candleInterval = 'day';

        $candleEntity01 = new CandleEntity();
        $candleEntity01->setId(1);

        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('createdDate', $createdDate))
            ->andWhere(Criteria::expr()->gte('time', $dateTime))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->orderBy(['createdDate' => Order::Descending])
            ->setMaxResults(1);


        $collection = $this->createMock(LazyCriteriaCollection::class);
        $collection->expects($this->once())->method('get')->with(0)->willReturn($candleEntity01);

        $objectRepository = $this->createMock(EntityRepository::class);
        $objectRepository->expects($this->once())->method('matching')
            ->with($criteria)->willReturn($collection);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CandleEntity::class)->willReturn($objectRepository);

        $this->assertEquals(
            $candleEntity01,
            $this->candleStorage->findOneByInstrumentIdAndIntervalAndAfterDate(
                $marketInstrumentId,
                $candleInterval,
                $dateTime,
                $createdDate
            )
        );
    }

    public function testFindOneFinalByInstrumentIdAndDate()
    {
        $dateTime = new \DateTime('2022-06-17 22:45');
        $marketInstrumentId = 1;
        $candleInterval = 'day';

        $candleEntity01 = new CandleEntity();
        $candleEntity01->setId(1);

        $startCandleDate = new \DateTime('2022-06-17 00:00:00');
        $endCandleDate = new \DateTime('2022-06-17 23:59:59');
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isFinal', 1))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('time', $startCandleDate))
            ->andWhere(Criteria::expr()->lte('time', $endCandleDate))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->setMaxResults(1);

        $collection = $this->createMock(LazyCriteriaCollection::class);
        $collection->expects($this->once())->method('get')->with(0)->willReturn($candleEntity01);

        $objectRepository = $this->createMock(EntityRepository::class);
        $objectRepository->expects($this->once())->method('matching')
            ->with($criteria)->willReturn($collection);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CandleEntity::class)->willReturn($objectRepository);

        $this->assertEquals(
            $candleEntity01,
            $this->candleStorage->findOneFinalByInstrumentIdAndDate(
                $marketInstrumentId,
                $candleInterval,
                $dateTime
            )
        );
    }
}
