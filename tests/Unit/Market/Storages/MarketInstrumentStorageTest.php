<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Storages;

use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Storages\MarketInstrumentStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketInstrumentStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private EntityRepository $repository;
    private MarketInstrumentStorage $marketInstrumentStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->marketInstrumentStorage = new MarketInstrumentStorage($this->entityManager);
    }

    #[DataProvider('dataFindStocks')]
    public function testFindStocks(
        array $expected,
        array $foundInstruments,
        string|null $currency,
        array $searchCriteria
    ): void {
        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketInstrumentEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findBy')
            ->with($searchCriteria)
            ->willReturn($foundInstruments);

        $this->assertEquals($expected, $this->marketInstrumentStorage->findStocks($currency));
    }

    public static function dataFindStocks(): array
    {
        $instrument01 = new MarketInstrumentEntity();
        $instrument01->setIsin('isin01');
        $instrument02 = new MarketInstrumentEntity();
        $instrument02->setIsin('isin02');

        $expected01 = [$instrument01, $instrument02];
        $foundInstruments01 = [$instrument01, $instrument02];

        $currency01 = 'RUB';
        $currency02 = null;

        $searchCriteria01 = ['type' => 'Stock', 'currency' => 'RUB'];
        $searchCriteria02 = ['type' => 'Stock'];

        return [
            'Common Case' => [
                $expected01,
                $foundInstruments01,
                $currency01,
                $searchCriteria01
            ],
            'Find without Currency Case' => [
                $expected01,
                $foundInstruments01,
                $currency02,
                $searchCriteria02
            ]
        ];
    }
}
