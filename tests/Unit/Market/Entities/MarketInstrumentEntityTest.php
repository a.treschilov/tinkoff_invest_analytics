<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Entities;

use App\Entities\MarketCurrencyEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Item\Entities\ItemEntity;
use Tests\ModelTestCase;

class MarketInstrumentEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new MarketInstrumentEntity();
    }

    public static function dataTestGetSet(): array
    {
        $marketStock = new MarketStockEntity();
        $marketStock->setId(123);
        $marketCurrency = new MarketCurrencyEntity();
        $marketCurrency->setId(2);
        $item = new ItemEntity();
        $item->setId(3);

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setItemId' => 3,
                    'setItem' => $item,
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setMinPriceIncrement' => 0.5,
                    'setLot' => 10,
                    'setCurrency' => 'USD',
                    'setName' => 'Лукойл',
                    'setType' => 'Etf',
                    'setMarketStock' => $marketStock,
                    'setMarketCurrency' => $marketCurrency
                ],
                [
                    'getId' => 1,
                    'getItemId' => 3,
                    'getItem' => $item,
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => 0.5,
                    'getLot' => 10,
                    'getCurrency' => 'USD',
                    'getName' => 'Лукойл',
                    'getType' => 'Etf',
                    'getMarketStock' => $marketStock,
                    'getMarketCurrency' => $marketCurrency
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setItemId' => 3,
                    'setItem' => $item,
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setLot' => 10,
                    'setCurrency' => 'USD',
                    'setName' => 'Лукойл',
                    'setType' => 'Etf'
                ],
                [
                    'getId' => 1,
                    'getItemId' => 3,
                    'getItem' => $item,
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => null,
                    'getLot' => 10,
                    'getCurrency' => 'USD',
                    'getName' => 'Лукойл',
                    'getType' => 'Etf',
                    'getMarketStock' => null,
                    'getMarketCurrency' => null,
                ]
            ]
        ];
    }
}
