<?php

declare(strict_types=1);

namespace Tests\Unit\Market\Entities;

use App\Market\Entities\CandleEntity;
use App\Market\Entities\MarketInstrumentEntity;
use Tests\ModelTestCase;

class CandleEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new CandleEntity();
    }

    public static function dataTestGetSet(): array
    {
        $createdDate = new \DateTime();
        $createdDate->setDate(2022, 1, 6);
        $createdDate->setTime(12, 30, 0);

        $time = new \DateTime();
        $time->setTime(2022, 1, 5);
        $time->setTime(10, 0, 0);

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setMarketInstrumentId' => 2,
                    'setFinal' => 1,
                    'setOpen' => 10,
                    'setClose' => 11,
                    'setLow' => 8,
                    'setHigh' => 12,
                    'setVolume' => 100,
                    'setCandleInterval' => 'day',
                    'setTime' => $time,
                    'setCreatedDate' => $createdDate
                ],
                [
                    'getId' => 1,
                    'getMarketInstrumentId' => 2,
                    'getIsFinal' => 1,
                    'getOpen' => 10,
                    'getClose' => 11,
                    'getLow' => 8,
                    'getHigh' => 12,
                    'getVolume' => 100,
                    'getCandleInterval' => 'day',
                    'getTime' => $time,
                    'getCreatedDate' => $createdDate,
                    'getIsActual' => 1,
                ]
            ]
        ];
    }
}
