<?php

declare(strict_types=1);

namespace Tests\Unit\Middlewares;

use App\Middlewares\JwtMiddleware;
use App\Services\AuthService;
use App\User\Entities\UserEntity;
use App\User\Exceptions\AuthTypeException;
use App\User\Services\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JwtMiddlewareTest extends TestCase
{
    private MockObject $request;
    private MockObject $handler;
    private MockObject $authService;
    private MockObject $userService;

    public function setUp(): void
    {
        parent::setUp();
        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->handler = $this->createMock(RequestHandlerInterface::class);
        $this->userService = $this->createMock(UserService::class);
        $this->authService = $this->createMock(AuthService::class);
    }

    #[DataProvider('dataInvoke')]
    public function testInvoke(
        array|null $exception,
        array|null $header,
        string|null $jwt,
        array|null $userData,
        bool|null $isValidJwt,
        UserEntity|null $user
    ): void {
        $this->request->expects($this->once())->method('getHeader')
            ->with('Authorization')->willReturn($header);

        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }

        if (null !== $jwt) {
            $this->authService->expects($this->once())->method('validateJWT')
                ->with($jwt)->willReturn($isValidJwt);
        }
        if (null !== $jwt && $isValidJwt) {
            $this->authService->expects($this->once())->method('decodeJWT')
                ->with($jwt)->willReturn($userData);
        }

        if (null !== $userData) {
            $this->userService->expects($this->once())->method('getActiveUserById')
                ->with($userData['userId'])
                ->willReturn($user);
            $this->userService->expects($this->once())->method('setUser')->with($user);
            $this->handler->expects($this->once())->method('handle')->with($this->request);
        }

        $jwtMiddleware = new JwtMiddleware($this->authService, $this->userService);
        $jwtMiddleware($this->request, $this->handler);
    }

    public static function dataInvoke(): array
    {
        $exception01 = null;
        $exception02 = [
            'class' => AuthTypeException::class,
            'code' => 2012
        ];
        $exception03 = [
            'class' => AuthTypeException::class,
            'code' => 2011
        ];

        $jwt01 = 'XXXX-YYYY-ZZZZ';
        $jwt02 = null;

        $header01 = ['Bearer ' . $jwt01];
        $header02 = [];
        $header03 = ['X-Token: AAAA-BBBB-CCCC'];

        $userData01 = ['userId' => 1];
        $userData02 = null;

        $isValidJwt01 = true;
        $isValidJwt02 = null;
        $isValidJwt03 = false;

        $user01 = new UserEntity();
        $user01->setId(1);
        $user02 = null;

        return [
            'Common Case' => [
                $exception01,
                $header01,
                $jwt01,
                $userData01,
                $isValidJwt01,
                $user01
            ],
            'Empty Auth Header Case' => [
                $exception02,
                $header02,
                $jwt02,
                $userData02,
                $isValidJwt02,
                $user02
            ],
            'Without Bearer In Header Case' => [
                $exception03,
                $header03,
                $jwt02,
                $userData02,
                $isValidJwt02,
                $user02
            ],
            'Not Valit JWT Case' => [
                $exception03,
                $header01,
                $jwt01,
                $userData02,
                $isValidJwt03,
                $user02
            ]
        ];
    }
}
