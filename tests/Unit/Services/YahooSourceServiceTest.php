<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Common\HttpClient;
use App\Entities\MarketStockEntity;
use App\Intl\Entities\CountryEntity;
use App\Intl\Services\CountryService;
use App\Services\Sources\YahooSourceService;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class YahooSourceServiceTest extends TestCase
{
    #[DataProvider('dataGetCompanyProfile')]
    public function testGetCompanyProfile(
        $marketStock,
        $countRequests,
        $ticker,
        $yahooTicker,
        $responseJson,
        $countryName,
        $convertCountry
    ) {
        $countryService = $this->createMock(CountryService::class);
        $countryService->expects($this->exactly(count($convertCountry)))
            ->method('getCountryByName')
            ->with(...WithConsecutive::create(...$countryName))
            ->willReturnOnConsecutiveCalls(...$convertCountry);

        $requestBody = [
            'modules' => 'assetProfile'
        ];

        $responseBody = $this->getMockBuilder(Stream::class)
            ->disableOriginalConstructor()
            ->getMock();
        $responseBody->expects($this->exactly($countRequests))
            ->method('getContents')
            ->withAnyParameters()
            ->willReturnOnConsecutiveCalls(...$responseJson);


        $response = $this->createMock(Response::class);
        $response->method('getBody')->willReturn($responseBody);

        $httpClient = $this->getMockBuilder(HttpClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $httpClient->expects($this->exactly($countRequests))
            ->method('doRequest')
            ->with(...WithConsecutive::create(
                ['GET', 'v10/finance/quoteSummary/' . $yahooTicker[0], $requestBody],
                ['GET', 'v10/finance/quoteSummary/' . $yahooTicker[1], $requestBody],
            ))
            ->willReturn($response);


        $yahooService = new YahooSourceService($httpClient, $countryService);

        $this->assertEquals($marketStock, $yahooService->getCompanyProfile($ticker));
    }

    public static function dataGetCompanyProfile()
    {
        $country01 = new CountryEntity();
        $country01->setIso('RU');

        $marketStock01 = new MarketStockEntity();
        $marketStock01->setIndustry('Credit Services');
        $marketStock01->setSector('Financial Services');
        $marketStock01->setCountryIso('RU');
        $marketStock01->setCountry($country01);

        $ticker01 = 'LKOH';
        $ticker02 = 'LKOH.ME';
        $ticker03 = 'LKOH@DE';
        $ticker04 = 'LKOH.DE';

        $countRequests01 = 1;
        $countRequests02 = 2;

        $countryName01 = [['Russia']];
        $countryName02 = [];
        $convertCountry01 = [$country01];
        $convertCountry02 = [];
        $responseJson01 = json_encode(
            [
                'quoteSummary' => [
                    'result' => [
                        0 => [
                            'assetProfile' => [
                                'country' => 'Russia',
                                'industry' => 'Credit Services',
                                'sector' => 'Financial Services'
                            ]
                        ]
                    ]
                ]
            ]
        );

        $responseJson02 = json_encode(
            [
                'quoteSummary' => [
                    'result' => null
                ]
            ]
        );

        $responseJson03 = json_encode(
            [
                'quoteSummary' => [
                    'result' => [
                        [
                            'assetProfile' => [
                                'name' => 'Lukoil'
                            ]
                        ]
                    ]
                ]
            ]
        );

        return [
            'Common Case' => [
                $marketStock01,
                $countRequests01,
                $ticker01,
                [$ticker01, null],
                [$responseJson01, null],
                $countryName01,
                $convertCountry01
            ],
            'Twiсe Requests Test' =>
            [
                $marketStock01,
                $countRequests02,
                $ticker01,
                [$ticker01, $ticker02],
                [$responseJson02, $responseJson01],
                $countryName01,
                $convertCountry01
            ],
            'Germany Ticker Test' =>
                [
                    $marketStock01,
                    $countRequests01,
                    $ticker03,
                    [$ticker04, null],
                    [$responseJson01, null],
                    $countryName01,
                    $convertCountry01
                ],
            'Null Response Test' =>
                [
                    null,
                    $countRequests02,
                    $ticker01,
                    [$ticker01, $ticker02],
                    [$responseJson02, $responseJson03],
                    $countryName02,
                    $convertCountry02
                ]
        ];
    }
}
