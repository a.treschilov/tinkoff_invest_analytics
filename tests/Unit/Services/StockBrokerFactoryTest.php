<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Services\Adapters\TinkoffHelper;
use App\Broker\Services\Adapters\TinkoffInvest;
use App\Exceptions\AccountException;
use App\Services\MarketInstrumentService;
use App\Services\StockBrokerFactory;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserCredentialService;
use ATreschilov\TinkoffInvestApiSdk\TIClient as TIClientV2;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class StockBrokerFactoryTest extends TestCase
{
    private StockBrokerFactory $stockBrokerFactory;
    private UserCredentialService $userCredentialService;
    private SourceBrokerInterface $sourceBroker;
    private SourceBrokerInterface $moexBroker;
    private MarketInstrumentService $marketInstrumentService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->sourceBroker = $this->createMock(SourceBrokerInterface::class);
        $this->moexBroker = $this->createMock(SourceBrokerInterface::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentService::class);

        $this->stockBrokerFactory = new StockBrokerFactory(
            $this->userCredentialService,
            $this->sourceBroker,
            $this->moexBroker,
            $this->marketInstrumentService
        );
    }

    #[DataProvider('dataGetSourceService')]
    public function testGetSourceService(
        ?array $exception,
        string $brokerCode,
    ): void {
        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
            $result = null;
        } else {
            $result = $this->sourceBroker;
        }

        $this->assertEquals(
            $result,
            $this->stockBrokerFactory->getSourceService($brokerCode)
        );
    }

    public static function dataGetSourceService(): array
    {
        $brokerCode01 = 'tinkoff2';
        $brokerCode02 = 'unknown_broker';

        $exception02 = [
            'class' => AccountException::class,
            'code' => 1032
        ];
        return [
            'Tinkoff V2 Case' => [
                null,
                $brokerCode01,
            ],
            'Unknown Broker Case' => [
                $exception02,
                $brokerCode02
            ]
        ];
    }

    #[DataProvider('dataGetBroker')]
    public function testGetBroker(
        ?TIClientV2 $broker,
        ?array $exception,
        UserCredentialEntity $credentialEntity,
        UserCredentialModel $credentialModel
    ): void {
        if (null !== $exception) {
            $this->expectException($exception['class']);
            $this->expectExceptionCode($exception['code']);
        }

        $this->userCredentialService->expects($this->once())->method('convertEntityToModel')
            ->with($credentialEntity)->willReturn($credentialModel);

        $result = $broker === null
            ? null
            : new TinkoffInvest(
                $broker,
                $this->sourceBroker,
                new TinkoffHelper()
            );
        $this->assertEquals($result, $this->stockBrokerFactory->getBroker($credentialEntity));
        $this->assertEquals($result, $this->stockBrokerFactory->getBroker($credentialEntity));
    }

    public static function dataGetBroker(): array
    {
        $apikey01 = 'AAAA-BBBB-CCCC';

        $credentialEntity01 = new UserCredentialEntity();
        $credentialEntity01->setId(1);
        $credentialEntity01->setApiKey('AAAA-BBBB-CCCC');
        $credentialEntity01->setBrokerId('tinkoff2');

        $credentialModel01 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'AAAA-BBBB-CCCC',
            'brokerId' => 'tinkoff2'
        ]);

        $credentialEntity02 = new UserCredentialEntity();
        $credentialEntity02->setId(1);
        $credentialEntity02->setApiKey('CCCC-DDDD-EEEE');
        $credentialEntity02->setBrokerId('tinkoff');

        $credentialEntity03 = new UserCredentialEntity();
        $credentialEntity03->setId(1);
        $credentialEntity03->setApiKey('FFFF-GGGG-HHHH');
        $credentialEntity03->setBrokerId('unknown_broker');
        $credentialModel03 = new UserCredentialModel([
            'userCredentialId' => 1,
            'apiKey' => 'FFFF-GGGG-HHHH',
            'brokerId' => 'unknown_broker'
        ]);


        $broker01 = new TIClientV2($apikey01, ['isRateLimitRetry' => true]);
        $broker03 = null;

        $exception01 = null;
        $exception02 = [
            'class' => AccountException::class,
            'code' => 1032
        ];

        return [
            'Common Case' => [
                $broker01,
                $exception01,
                $credentialEntity01,
                $credentialModel01
            ],
            'Unknown Adapter' => [
                $broker03,
                $exception02,
                $credentialEntity03,
                $credentialModel03
            ]
        ];
    }
}
