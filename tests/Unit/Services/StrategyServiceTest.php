<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\CompareCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Interfaces\ShareStrategyInterface;
use App\Models\TargetShareModel;
use App\Services\ShareStrategy\CurrencyShare;
use App\Services\ShareStrategyFactory;
use App\Services\StrategyService;
use App\User\Services\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class StrategyServiceTest extends TestCase
{
    private ShareStrategyFactory $shareStrategyFactory;
    private UserService $userService;
    private StrategyService $strategyService;

    public function setUp(): void
    {
        parent::setUp();

        $this->shareStrategyFactory = $this->createMock(ShareStrategyFactory::class);
        $this->userService = $this->createMock(UserService::class);
        $this->strategyService = new StrategyService($this->shareStrategyFactory, $this->userService);
    }

    #[DataProvider('dataCalculateShare')]
    public function testCalculateShare($share, $target, $amount, $expected): void
    {
        $balance = new PortfolioBalanceCollection();
        $instruments = $this->createMock(InstrumentCollection::class);

        $this->userService->expects($this->once())->method('getUserId')->willReturn(1);

        $strategyCalculator = $this->createMock(CurrencyShare::class);
        $strategyCalculator->method('calculateShare')->with($instruments, $balance)->willReturn($share);
        $strategyCalculator->method('getTargetShare')->with(1)->willReturn($target);
        $strategyCalculator->method('calculateTotalAmount')->with($instruments, $balance)->willReturn((float)$amount);

        $this->shareStrategyFactory->method('getShareCalculator')
            ->with('currency')->willReturn($strategyCalculator);

        $this->assertEqualsWithDelta(
            $expected,
            $this->strategyService->calculateShare('currency', $instruments, $balance),
            0.001
        );
    }

    public static function dataCalculateShare(): array
    {
        $shareTargetCollection01 = new ShareCollection(array(
            new TargetShareModel(['name' => 'RUB', 'value' => 0.40]),
            new TargetShareModel(['name' => 'USD', 'value'  => 0.40]),
            new TargetShareModel(['name' => 'EUR', 'value'  => 0.20]),
        ));
        $shareCollection01 = new ShareCollection(array(
            'RUB' => 0.20,
            'USD' => 0.50,
            'EUR' => 0.30,
        ));
        $compare01 = [
            'RUB' => [
                'share' => 0.2,
                'targetShare' => 0.4,
                'diffPercent' => -0.2,
                'diffAmount' => -200
            ],
            'USD' => [
                'share' => 0.5,
                'targetShare' => 0.4,
                'diffPercent' => 0.1,
                'diffAmount' => 100
            ],
            'EUR' => [
                'share' => 0.3,
                'targetShare' => 0.2,
                'diffPercent' => 0.1,
                'diffAmount' => 100
            ],
        ];
        $amount01 = 1000;
        $expected01 = new CompareCollection($compare01);

        $shareCollection02 = new ShareCollection(array(
            'RUB' => 0.60,
            'USD' => 0.40
        ));
        $compare02 = [
            'RUB' => [
                'share' => 0.6,
                'targetShare' => 0.4,
                'diffPercent' => 0.2,
                'diffAmount' => 200
            ],
            'USD' => [
                'share' => 0.4,
                'targetShare' => 0.4,
                'diffPercent' => 0,
                'diffAmount' => 0
            ],
            'EUR' => [
                'share' => 0,
                'targetShare' => 0.2,
                'diffPercent' => -0.2,
                'diffAmount' => -200
            ],
        ];
        $expected02 = new CompareCollection($compare02);

        $shareTargetCollection03 = new ShareCollection(array(
            new TargetShareModel(['name' => 'RUB', 'value' => 0.60]),
            new TargetShareModel(['name' => 'EUR', 'value' => 0.40]),
        ));
        $compare03 = [
            'RUB' => [
                'share' => 0.2,
                'targetShare' => 0.6,
                'diffPercent' => -0.4,
                'diffAmount' => -400
            ],
            'EUR' => [
                'share' => 0.3,
                'targetShare' => 0.4,
                'diffPercent' => -0.1,
                'diffAmount' => -100
            ],
            'USD' => [
                'share' => 0.5,
                'targetShare' => 0,
                'diffPercent' => 0.5,
                'diffAmount' => 500
            ]
        ];
        $expected03 = new CompareCollection($compare03);

        return [
            'General Case' => [$shareCollection01, $shareTargetCollection01, $amount01, $expected01],
            'Not full portfolio' => [$shareCollection02, $shareTargetCollection01, $amount01, $expected02],
            'Not full strategy' => [$shareCollection01, $shareTargetCollection03, $amount01, $expected03]
        ];
    }

    public function testGetTargetShare()
    {
        $type01 = 'stock';
        $userId01 = 1;


        $strategyCalculator01 = $this->createMock(ShareStrategyInterface::class);
        $this->userService->expects($this->once())->method('getUserId')->willReturn($userId01);
        $this->shareStrategyFactory->expects($this->once())
            ->method('getShareCalculator')
            ->with($type01)
            ->willReturn($strategyCalculator01);
        $strategyCalculator01->expects($this->once())->method('getTargetShare')
            ->with($userId01);

        $this->strategyService->getTargetShare($type01);
    }

    public function testUpdateTargetShare()
    {
        $type01 = 'stock';
        $userId01 = 1;
        $shareCollection01 = new ShareCollection();

        $strategyCalculator01 = $this->createMock(ShareStrategyInterface::class);
        $this->userService->expects($this->once())->method('getUserId')->willReturn($userId01);
        $this->shareStrategyFactory->expects($this->once())
            ->method('getShareCalculator')
            ->with($type01)
            ->willReturn($strategyCalculator01);
        $strategyCalculator01->expects($this->once())->method('updateTargetShare')
            ->with($userId01, $shareCollection01);

        $this->strategyService->updateTargetShare($type01, $shareCollection01);
    }
}
