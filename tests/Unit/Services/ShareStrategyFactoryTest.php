<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Exceptions\ShareStrategyException;
use App\Intl\Services\CountryService;
use App\Intl\Storages\CurrencyStorage;
use App\Services\MarketInstrumentService;
use App\Services\ShareStrategy\BalanceShare;
use App\Services\ShareStrategy\CountryShare;
use App\Services\ShareStrategy\CurrencyShare;
use App\Services\ShareStrategy\SectorShare;
use App\Services\ShareStrategy\StockShare;
use App\Services\ShareStrategyFactory;
use App\Storages\InstrumentTypeStorage;
use App\Storages\MarketCountryStorage;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarketSectorStorage;
use App\Storages\StrategyCountryStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use App\Storages\StrategyCurrencyStorage;
use App\Storages\StrategyInstrumentTypeStorage;
use App\Storages\StrategySectorStorage;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ShareStrategyFactoryTest extends TestCase
{
    private ShareStrategyFactory $strategyFactory;
    private StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage;
    private StrategyCurrencyStorage $strategyCurrencyStorage;
    private StrategyCurrencyBalanceStorage $strategyCurrencyBalanceStorage;
    private StrategyCountryStorage $strategyCountryStorage;
    private StrategySectorStorage $strategySectorStorage;
    private MarketCountryStorage $marketCountryStorage;
    private MarketSectorStorage $marketSectorStorage;
    private CurrencyStorage $currencyStorage;
    private InstrumentTypeStorage $instrumentTypeStorage;
    private \DateTime $dateTime;
    private CountryService $countryService;

    public function setUp(): void
    {
        parent::setUp();
        $marketInstrumentService = $this->createMock(MarketInstrumentService::class);
        $this->strategyInstrumentTypeStorage = $this->createMock(StrategyInstrumentTypeStorage::class);
        $this->strategyCurrencyStorage = $this->createMock(StrategyCurrencyStorage::class);
        $this->instrumentTypeStorage = $this->createMock(InstrumentTypeStorage::class);
        $this->currencyStorage = $this->createMock(CurrencyStorage::class);
        $this->strategyCurrencyBalanceStorage = $this->createMock(StrategyCurrencyBalanceStorage::class);
        $this->strategyCountryStorage = $this->createMock(StrategyCountryStorage::class);
        $this->marketCountryStorage = $this->createMock(MarketCountryStorage::class);
        $this->countryService = $this->createMock(CountryService::class);
        $this->strategySectorStorage = $this->createMock(StrategySectorStorage::class);
        $this->marketSectorStorage = $this->createMock(MarketSectorStorage::class);

        $this->dateTime = new \DateTime('2022-05-13 18:29:32');

        $this->strategyFactory = new ShareStrategyFactory(
            $this->dateTime,
            $marketInstrumentService,
            $this->countryService,
            $this->strategyInstrumentTypeStorage,
            $this->strategyCurrencyStorage,
            $this->strategyCurrencyBalanceStorage,
            $this->strategyCountryStorage,
            $this->strategySectorStorage,
            $this->currencyStorage,
            $this->instrumentTypeStorage,
            $this->marketCountryStorage,
            $this->marketSectorStorage
        );
    }

    #[DataProvider('dataCreateShareCalculator')]
    public function testCreateShareCalculator($type, $strategyClass, $exception = null): void
    {
        if (null !== $exception) {
            $this->expectException($exception);
        }

        $strategy = $this->strategyFactory->getShareCalculator($type);
        $this->assertTrue(get_class($strategy) === $strategyClass);
    }

    public static function dataCreateShareCalculator(): array
    {
        return [
            'Currency Strategy' => ['currency', CurrencyShare::class],
            'Stock Strategy' => ['stock', StockShare::class],
            'Sector Strategy' => ['sector', SectorShare::class],
            'Country Strategy' => ['country', CountryShare::class],
            'Balance Strategy' => ['currencyBalance', BalanceShare::class],
            'Unknown Strategy' => ['marketplace', '', ShareStrategyException::class]
        ];
    }
}
