<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Services\BrokerReportUploadService;
use App\Broker\Services\BrokerService;
use App\Common\Amqp\AmqpClient;
use App\Entities\OperationTypeEntity;
use App\Intl\Services\CurrencyService;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Services\AccountService;
use App\Services\MarketInstrumentService;
use App\Services\MarketOperationsService;
use App\Services\StockBrokerFactory;
use App\Storages\OperationTypeStorage;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use Monolog\Test\TestCase;

class MarketOperationsServiceTest extends TestCase
{
    private StockBrokerFactory $stockBrokerFactory;
    private OperationTypeStorage $operationTypeStorage;
    private MarketInstrumentService $marketInstrumentService;
    private AccountService $accountService;
    private MarketOperationsService $operationsService;
    private UserService $userService;
    private BrokerService $brokerService;
    private ItemOperationStorage $itemOperationStorage;
    private CurrencyService $currencyService;
    private \DateTime $currentDateTime;
    private UserCredentialService $userCredentialService;
    private AmqpClient $amqpClient;
    private ItemOperationImportStorage $itemOperationImportStorage;
    private UserBrokerAccountService $userBrokerAccountService;
    private BrokerReportUploadService $brokerReportUploadService;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->operationTypeStorage = $this->createMock(OperationTypeStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentService::class);
        $this->accountService = $this->createMock(AccountService::class);
        $this->userService = $this->createMock(UserService::class);
        $this->brokerService = $this->createMock(BrokerService::class);
        $this->itemOperationStorage = $this->createMock(ItemOperationStorage::class);
        $this->currencyService = $this->createMock(CurrencyService::class);
        $this->currentDateTime = new \DateTime('2023-01-08 23:07:22');
        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->amqpClient = $this->createMock(AmqpClient::class);
        $this->itemOperationImportStorage = $this->createMock(ItemOperationImportStorage::class);
        $this->userBrokerAccountService = $this->createMock(UserBrokerAccountService::class);
        $this->brokerReportUploadService = $this->createMock(BrokerReportUploadService::class);

        $this->operationsService = new MarketOperationsService(
            $this->stockBrokerFactory,
            $this->operationTypeStorage,
            $this->marketInstrumentService,
            $this->accountService,
            $this->userService,
            $this->brokerService,
            $this->itemOperationStorage,
            $this->currencyService,
            $this->currentDateTime,
            $this->userCredentialService,
            $this->amqpClient,
            $this->itemOperationImportStorage,
            $this->userBrokerAccountService,
            $this->brokerReportUploadService
        );
    }

    public function testGetOperationTypeByExternalId()
    {
        $expected = new OperationTypeEntity();
        $expected->setId(1);

        $externalId = 'PayIn';

        $this->operationTypeStorage->expects($this->once())->method('findByExternalId')
            ->with($externalId)
            ->willReturn($expected);

        $this->assertEquals($expected, $this->operationsService->getOperationTypeByExternalId($externalId));
    }
}
