<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Services\AccountService;
use App\Services\PortfolioService;
use App\Services\StockBrokerFactory;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserService;
use Monolog\Test\TestCase;
use Tests\WithConsecutive;

class AccountServiceTest extends TestCase
{
    private AccountService $accountService;
    private PortfolioService $portfolioService;
    private StockBrokerFactory $stockBrokerFactory;
    private UserBrokerAccountService $userBrokerAccountService;
    private UserService $userService;

    public function setUp(): void
    {
        parent::setUp();

        $this->portfolioService = $this->createMock(PortfolioService::class);
        $this->stockBrokerFactory = $this->createMock(StockBrokerFactory::class);
        $this->userBrokerAccountService = $this->createMock(UserBrokerAccountService::class);
        $this->userService = $this->createMock(UserService::class);
        $this->accountService = new AccountService(
            $this->stockBrokerFactory,
            $this->portfolioService,
            $this->userBrokerAccountService,
            $this->userService
        );
    }

    public function testGetPortfolio(): void
    {
        $userCredential01 = new UserCredentialEntity();
        $userCredential01->setId(1);
        $userBrokerAccount01 = new UserBrokerAccountEntity();
        $userBrokerAccount01->setId(1);
        $userBrokerAccount01->setExternalId('account-01');
        $userBrokerAccount01->setUserCredential($userCredential01);

        $userCredential02 = new UserCredentialEntity();
        $userCredential02->setId(2);
        $userBrokerAccount02 = new UserBrokerAccountEntity();
        $userBrokerAccount02->setId(2);
        $userBrokerAccount02->setExternalId('account-02');
        $userBrokerAccount02->setUserCredential($userCredential02);

        $userBrokerAccountCollection01 = new UserBrokerAccountCollection([$userBrokerAccount01, $userBrokerAccount02]);

        $broker01 = $this->createMock(UserBrokerInterface::class);
        $broker02 = $this->createMock(UserBrokerInterface::class);

        $this->userService->expects($this->once())->method('getUserId')->willReturn(1);
        $this->userBrokerAccountService->expects($this->once())->method('getUserAccountByUserId')
            ->with(1)->willReturn($userBrokerAccountCollection01);
        $this->stockBrokerFactory->expects($this->exactly(2))->method('getBroker')
            ->with(...WithConsecutive::create([$userCredential01], [$userCredential02]))
            ->willReturnOnConsecutiveCalls($broker01, $broker02);
        $broker01->expects($this->once())->method('getPortfolio')->with('account-01');
        $broker02->expects($this->once())->method('getPortfolio')->with('account-02');

        $this->accountService->getPortfolio();
    }

    public function testGetPortfolioBalance(): void
    {
        $userCredential01 = new UserCredentialEntity();
        $userCredential01->setId(1);
        $userBrokerAccount01 = new UserBrokerAccountEntity();
        $userBrokerAccount01->setId(1);
        $userBrokerAccount01->setExternalId('account-01');
        $userBrokerAccount01->setUserCredential($userCredential01);

        $userCredential02 = new UserCredentialEntity();
        $userCredential02->setId(2);
        $userBrokerAccount02 = new UserBrokerAccountEntity();
        $userBrokerAccount02->setId(2);
        $userBrokerAccount02->setExternalId('account-02');
        $userBrokerAccount02->setUserCredential($userCredential02);

        $userBrokerAccountCollection01 = new UserBrokerAccountCollection([$userBrokerAccount01, $userBrokerAccount02]);

        $balance01 = new BrokerPortfolioBalanceModel(['amount' => 10, 'currency' => 'RUB', 'blockerAmount' => 4.2]);
        $balance02 = new BrokerPortfolioBalanceModel(['amount' => 4, 'currency' => 'USD', 'blockerAmount' => 3.1]);
        $balance03 = new BrokerPortfolioBalanceModel(['amount' => 5, 'currency' => 'RUB', 'blockerAmount' => 0]);
        $balance04 = new BrokerPortfolioBalanceModel(['amount' => 15, 'currency' => 'CNY', 'blockerAmount' => 10.3]);
        $balanceCollection01 = new PortfolioBalanceCollection([$balance01, $balance02, $balance04]);
        $balanceCollection02 = new PortfolioBalanceCollection([$balance03]);

        $expected01 = new PortfolioBalanceCollection([$balance01, $balance02, $balance04, $balance03]);

        $broker01 = $this->createMock(UserBrokerInterface::class);
        $broker02 = $this->createMock(UserBrokerInterface::class);

        $this->userService->expects($this->once())->method('getUserId')->willReturn(1);
        $this->userBrokerAccountService->expects($this->once())->method('getUserAccountByUserId')
            ->with(1)->willReturn($userBrokerAccountCollection01);
        $this->stockBrokerFactory->expects($this->exactly(2))->method('getBroker')
            ->with(...WithConsecutive::create([$userCredential01], [$userCredential02]))
            ->willReturnOnConsecutiveCalls($broker01, $broker02);
        $broker01->expects($this->once())->method('getPortfolioBalance')->with('account-01')
            ->willReturn($balanceCollection01);
        $broker02->expects($this->once())->method('getPortfolioBalance')->with('account-02')
            ->willReturn($balanceCollection02);

        $this->assertEquals($expected01, $this->accountService->getPortfolioBalance());
    }

    public function testGetCredentialAccounts(): void
    {
        $credential01 = new UserCredentialModel();
        $credential01->setUserCredentialId(1);

        $userBrokerAccountEntity01 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity01->setId(2);
        $expected = new UserBrokerAccountCollection([$userBrokerAccountEntity01]);

        $this->userBrokerAccountService->expects($this->once())->method('getActiveUserAccountListByCredential')
            ->with(1)->willReturn($expected);

        $this->assertEquals($expected, $this->accountService->getCredentialAccounts($credential01));
    }
}
