<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\InstrumentTypeEntity;
use App\Entities\StrategyInstrumentTypeEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Models\TargetShareModel;
use App\Services\ShareStrategy\StockShare;
use App\Storages\InstrumentTypeStorage;
use App\Storages\StrategyInstrumentTypeStorage;
use Monolog\Test\TestCase;
use Tests\WithConsecutive;

class StockShareTest extends TestCase
{
    private StockShare $stockShare;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage;
    private InstrumentTypeStorage $instrumentTypeStorage;
    private \DateTime $currentDateTime;

    public function setUp(): void
    {
        parent::setUp();

        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);
        $this->strategyInstrumentTypeStorage = $this->createMock(StrategyInstrumentTypeStorage::class);
        $this->instrumentTypeStorage = $this->createMock(InstrumentTypeStorage::class);
        $this->currentDateTime = new \DateTime('2022-05-13 18:32:51');
        $this->stockShare = new StockShare(
            $this->currentDateTime,
            $this->marketInstrumentService,
            $this->strategyInstrumentTypeStorage,
            $this->instrumentTypeStorage
        );
    }

    public function testGetTargetShare()
    {
        $instrumentTypeStock = new InstrumentTypeEntity();
        $instrumentTypeStock->setId(1);
        $instrumentTypeStock->setName('Stock');
        $instrumentTypeStock->setExternalId('Stock');
        $instrumentTypeGold = new InstrumentTypeEntity();
        $instrumentTypeGold->setId(2);
        $instrumentTypeGold->setName('Gold');
        $instrumentTypeGold->setExternalId('Gold');
        $instrumentTypeFederalBond = new InstrumentTypeEntity();
        $instrumentTypeFederalBond->setId(4);
        $instrumentTypeFederalBond->setName('Federal Bond');
        $instrumentTypeFederalBond->setExternalId('FederalBond');
        $instrumentTypeBond = new InstrumentTypeEntity();
        $instrumentTypeBond->setId(5);
        $instrumentTypeBond->setName('Bond');
        $instrumentTypeBond->setExternalId('Bond');
        $instrumentTypeEtf = new InstrumentTypeEntity();
        $instrumentTypeEtf->setId(3);
        $instrumentTypeEtf->setName('Etf');
        $instrumentTypeEtf->setExternalId('Etf');
        $instrumentTypeCurrency = new InstrumentTypeEntity();
        $instrumentTypeCurrency->setId(6);
        $instrumentTypeCurrency->setName('Currency');
        $instrumentTypeCurrency->setExternalId('Currency');
        $userId = 1;
        $strategyInstrumentTypeEntity01 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity01->setInstrumentType($instrumentTypeStock);
        $strategyInstrumentTypeEntity01->setShare(0.35);
        $strategyInstrumentTypeEntity02 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity02->setInstrumentType($instrumentTypeGold);
        $strategyInstrumentTypeEntity02->setShare(0.2);
        $strategyInstrumentTypeEntity03 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity03->setInstrumentType($instrumentTypeFederalBond);
        $strategyInstrumentTypeEntity03->setShare(0.1);
        $strategyInstrumentTypeEntity04 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity04->setInstrumentType($instrumentTypeBond);
        $strategyInstrumentTypeEntity04->setShare(0.25);
        $strategyInstrumentTypeEntity05 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity05->setInstrumentType($instrumentTypeEtf);
        $strategyInstrumentTypeEntity05->setShare(0.1);
        $strategyInstrumentTypeEntity06 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity06->setInstrumentType($instrumentTypeCurrency);
        $strategyInstrumentTypeEntity06->setShare(0);
        $entityArray = [
            $strategyInstrumentTypeEntity01,
            $strategyInstrumentTypeEntity02,
            $strategyInstrumentTypeEntity03,
            $strategyInstrumentTypeEntity04,
            $strategyInstrumentTypeEntity05,
            $strategyInstrumentTypeEntity06
        ];
        $instrumentTypeEntity01 = new InstrumentTypeEntity();
        $instrumentTypeEntity01->setId(1);
        $instrumentTypeEntity01->setName('Stock');
        $instrumentTypeEntity01->setExternalId('Stock');
        $instrumentTypeEntity02 = new InstrumentTypeEntity();
        $instrumentTypeEntity02->setId(2);
        $instrumentTypeEntity02->setName('Gold');
        $instrumentTypeEntity02->setExternalId('Gold');
        $instrumentTypeEntity03 = new InstrumentTypeEntity();
        $instrumentTypeEntity03->setId(3);
        $instrumentTypeEntity03->setName('Etf');
        $instrumentTypeEntity03->setExternalId('Etf');
        $instrumentTypeEntity04 = new InstrumentTypeEntity();
        $instrumentTypeEntity04->setId(4);
        $instrumentTypeEntity04->setName('Federal Bond');
        $instrumentTypeEntity04->setExternalId('FederalBond');
        $instrumentTypeEntity05 = new InstrumentTypeEntity();
        $instrumentTypeEntity05->setId(5);
        $instrumentTypeEntity05->setName('Bond');
        $instrumentTypeEntity05->setExternalId('Bond');
        $instrumentTypeEntity06 = new InstrumentTypeEntity();
        $instrumentTypeEntity06->setId(6);
        $instrumentTypeEntity06->setName('Currency');
        $instrumentTypeEntity06->setExternalId('Currency');
        $instrumentTypeEntity07 = new InstrumentTypeEntity();
        $instrumentTypeEntity07->setId(7);
        $instrumentTypeEntity07->setName('Futures');
        $instrumentTypeEntity07->setExternalId('Futures');
        $instrumentTypes01 = [
            $instrumentTypeEntity01,
            $instrumentTypeEntity02,
            $instrumentTypeEntity03,
            $instrumentTypeEntity04,
            $instrumentTypeEntity05,
            $instrumentTypeEntity06,
            $instrumentTypeEntity07
        ];

        $instrumentStrategy = new ShareCollection([
            new TargetShareModel(['external_id' => 'Stock', 'name' => 'Stock', 'value' => 0.35]),
            new TargetShareModel(['external_id' => 'Gold', 'name' => 'Gold', 'value'  => 0.20]),
            new TargetShareModel(['external_id' => 'Etf', 'name' => 'Etf', 'value'  => 0.10]),
            new TargetShareModel(['external_id' => 'FederalBond', 'name' => 'Federal Bond', 'value'  => 0.10]),
            new TargetShareModel(['external_id' => 'Bond', 'name' => 'Bond', 'value'  => 0.25]),
            new TargetShareModel(['external_id' => 'Currency', 'name' => 'Currency', 'value'  => 0]),
            new TargetShareModel(['external_id' => 'Futures', 'name' => 'Futures', 'value'  => 0])
        ]);
        $this->instrumentTypeStorage->expects($this->once())
            ->method('findActive')
            ->willReturn($instrumentTypes01);
        $this->strategyInstrumentTypeStorage->expects($this->once())
            ->method('findActiveByUserId')->with($userId)
            ->willReturn($entityArray);

        $this->assertEquals($instrumentStrategy, $this->stockShare->getTargetShare($userId));
    }

    public function testUpdateTargetShare()
    {
        $userId01 = 1;
        $currentShares01 = [
            new TargetShareModel(['externalId' => 'Stock', 'value' => 0.35]),
            new TargetShareModel(['externalId' => 'Etf', 'value' => 0.1])
        ];
        $currentSharesCollection01 = new ShareCollection($currentShares01);

        $instrumentTypeEntity01 = new InstrumentTypeEntity();
        $instrumentTypeEntity01->setId(1);
        $instrumentTypeEntity02 = new InstrumentTypeEntity();
        $instrumentTypeEntity02->setId(2);

        $searchingParams01 = [['Stock'], ['Etf']];
        $returningParams01 = [$instrumentTypeEntity01, $instrumentTypeEntity02];

        $currentInstrumentTypeEntity01 = new StrategyInstrumentTypeEntity();
        $currentInstrumentTypeEntity01->setId(1);
        $currentInstrumentTypeEntity01->setIsActive(1);
        $currentInstrumentTypeEntity01->setUpdatedDate(new \DateTime());
        $currentInstrumentTypeEntity02 = new StrategyInstrumentTypeEntity();
        $currentInstrumentTypeEntity02->setId(2);
        $currentInstrumentTypeEntity02->setIsActive(0);
        $currentInstrumentTypeEntity02->setUpdatedDate(new \DateTime());

        $toUpdateEntity01 = new StrategyInstrumentTypeEntity();
        $toUpdateEntity01->setId(1);
        $toUpdateEntity01->setIsActive(0);
        $toUpdateEntity01->setUpdatedDate($this->currentDateTime);
        $toUpdateEntity02 = new StrategyInstrumentTypeEntity();
        $toUpdateEntity02->setId(2);
        $toUpdateEntity02->setIsActive(0);
        $toUpdateEntity02->setUpdatedDate($this->currentDateTime);

        $toUpdateEntity03 = new StrategyInstrumentTypeEntity();
        $toUpdateEntity03->setUserId($userId01);
        $toUpdateEntity03->setIsActive(1);
        $toUpdateEntity03->setShare(0.35);
        $toUpdateEntity03->setInstrumentTypeId(1);
        $toUpdateEntity03->setInstrumentType($returningParams01[0]);
        $toUpdateEntity03->setCreatedDate($this->currentDateTime);
        $toUpdateEntity03->setUpdatedDate($this->currentDateTime);

        $toUpdateEntity04 = new StrategyInstrumentTypeEntity();
        $toUpdateEntity04->setUserId($userId01);
        $toUpdateEntity04->setIsActive(1);
        $toUpdateEntity04->setShare(0.1);
        $toUpdateEntity04->setInstrumentTypeId(2);
        $toUpdateEntity04->setInstrumentType($returningParams01[1]);
        $toUpdateEntity04->setCreatedDate($this->currentDateTime);
        $toUpdateEntity04->setUpdatedDate($this->currentDateTime);

        $toUpdateShares01 = [
            $toUpdateEntity01, $toUpdateEntity02, $toUpdateEntity03, $toUpdateEntity04
        ];

        $this->strategyInstrumentTypeStorage->expects($this->once())
            ->method('findActiveByUserId')->with($userId01)
            ->willReturn([$currentInstrumentTypeEntity01, $currentInstrumentTypeEntity02]);
        $this->instrumentTypeStorage->expects($this->exactly(count($currentShares01)))
            ->method('findByExternalId')
            ->with(...WithConsecutive::create(...$searchingParams01))
            ->willReturnOnConsecutiveCalls(...$returningParams01);
        $this->strategyInstrumentTypeStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdateShares01);

        $this->stockShare->updateTargetShare($userId01, $currentSharesCollection01);
    }

    public function testCalculateTotalAmount()
    {
        $balance01 = new BrokerPortfolioBalanceModel([
            'currency' => 'RUB',
            'amount' => 10,
            'blockedAmount' => 4
        ]);
        $balance02 = new BrokerPortfolioBalanceModel([
            'currency' => 'USD',
            'amount' => 7,
            'blockedAmount' => 0
        ]);
        $balanceCollection = new PortfolioBalanceCollection([$balance01, $balance02]);
        $instruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $instruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);

        $this->assertEquals(106, $this->stockShare->calculateTotalAmount($instruments, $balanceCollection));
    }
}
