<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\MarketCurrencyEntity;
use App\Entities\StrategyCurrencyBalanceEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Intl\Entities\CurrencyEntity;
use App\Intl\Storages\CurrencyStorage;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Services\ShareStrategy\BalanceShare;
use App\Storages\MarketCurrencyStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class BalanceShareTest extends TestCase
{
    private BalanceShare $balanceShare;
    private StrategyCurrencyBalanceStorage $strategyCurrencyBalanceStorage;
    private CurrencyStorage $currencyStorage;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $currentDateTime;

    public function setUp(): void
    {
        parent::setUp();

        $this->currentDateTime = new \DateTime('2022-06-02 20:04:43');
        $this->strategyCurrencyBalanceStorage = $this->createMock(StrategyCurrencyBalanceStorage::class);
        $this->currencyStorage = $this->createMock(CurrencyStorage::class);
        $this->marketInstrumentService = $this->createMock(MarketInstrumentServiceInterface::class);
        $this->balanceShare = new BalanceShare(
            $this->currentDateTime,
            $this->strategyCurrencyBalanceStorage,
            $this->currencyStorage,
            $this->marketInstrumentService
        );
    }

    public function testGetTargetShare()
    {
        $userId01 = 1;
        $userStrategyEntity01 = new StrategyCurrencyBalanceEntity();
        $userStrategyEntity01->setId(10);
        $userStrategyEntity01->setCurrencyIso('USD');
        $userStrategyEntity01->setShare(0.35);

        $userStrategyEntity02 = new StrategyCurrencyBalanceEntity();
        $userStrategyEntity02->setId(11);
        $userStrategyEntity02->setCurrencyIso('RUB');
        $userStrategyEntity02->setShare(0.1);

        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('USD');

        $currencyEntity02 = new CurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('RUB');

        $currencyEntity03 = new CurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('CNY');

        $userStrategy01 = [$userStrategyEntity01, $userStrategyEntity02];
        $currency01 = [$currencyEntity01, $currencyEntity02, $currencyEntity03];

        $this->strategyCurrencyBalanceStorage->expects($this->once())->method('findActiveByUserId')
            ->with(1)->willReturn($userStrategy01);
        $this->currencyStorage->expects($this->once())->method('findAll')
            ->willReturn($currency01);

        $expected = new ShareCollection(
            [
                new TargetShareModel(['externalId' => 'USD', 'name' => 'USD', 'value' => 0.35]),
                new TargetShareModel(['externalId' => 'RUB', 'name' => 'RUB', 'value' => 0.1]),
                new TargetShareModel(['externalId' => 'CNY', 'name' => 'CNY', 'value' => 0])
            ]
        );
        $this->assertEquals($expected, $this->balanceShare->getTargetShare($userId01));
    }

    public function testUpdateTargetShares()
    {
        $dateTime01 = new \DateTime('2021-01-02 12:00:00');
        $userId01 = 1;

        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('RUB');
        $currencyEntity02 = new CurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('USD');
        $currencyEntity03 = new CurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('EUR');

        $strategyEntity01 = new StrategyCurrencyBalanceEntity();
        $strategyEntity01->setId(1);
        $strategyEntity01->setIsActive(1);
        $strategyEntity01->setUpdatedDate($dateTime01);

        $targetShare01 = new TargetShareModel(['externalId' => 'RUB', 'value' => 0.3]);
        $targetShare02 = new TargetShareModel(['externalId' => 'USD', 'value' => 0.2]);
        $targetShare03 = new TargetShareModel(['externalId' => 'EUR', 'value' => 0.15]);
        $shareCollection = new ShareCollection([$targetShare01, $targetShare02, $targetShare03]);

        $toUpdateStrategyEntity01 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity01->setId(1);
        $toUpdateStrategyEntity01->setIsActive(0);
        $toUpdateStrategyEntity01->setUpdatedDate($this->currentDateTime);

        $toUpdateStrategyEntity02 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setShare(0.3);
        $toUpdateStrategyEntity02->setUserId($userId01);
        $toUpdateStrategyEntity02->setCurrencyIso('RUB');

        $toUpdateStrategyEntity03 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setShare(0.2);
        $toUpdateStrategyEntity03->setUserId($userId01);
        $toUpdateStrategyEntity03->setCurrencyIso('USD');

        $toUpdateStrategyEntity04 = new StrategyCurrencyBalanceEntity();
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setCreatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setUpdatedDate($this->currentDateTime);
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setShare(0.15);
        $toUpdateStrategyEntity04->setUserId($userId01);
        $toUpdateStrategyEntity04->setCurrencyIso('EUR');

        $toUpdated01 = [
            $toUpdateStrategyEntity01,
            $toUpdateStrategyEntity02,
            $toUpdateStrategyEntity03,
            $toUpdateStrategyEntity04
        ];

        $this->strategyCurrencyBalanceStorage->expects($this->once())
            ->method('findActiveByUserId')
            ->with($userId01)
            ->willReturn([$strategyEntity01]);

        $this->currencyStorage->expects($this->exactly($shareCollection->count()))
            ->method('findOneByIso')
            ->with(...WithConsecutive::create(
                [$targetShare01->getExternalId()],
                [$targetShare02->getExternalId()],
                [$targetShare03->getExternalId()]
            ))->willReturnOnConsecutiveCalls($currencyEntity01, $currencyEntity02, $currencyEntity03);

        $this->strategyCurrencyBalanceStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdated01);

        $this->balanceShare->updateTargetShare($userId01, $shareCollection);
    }
}
