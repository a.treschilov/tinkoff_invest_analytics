<?php

declare(strict_types=1);

namespace Tests\Unit\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategyCurrencyEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Intl\Storages\CurrencyStorage;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Services\ShareStrategy\CurrencyShare;
use App\Storages\StrategyCurrencyStorage;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;
use Tests\WithConsecutive;

class CurrencyShareTest extends TestCase
{
    private CurrencyShare $currencyShare;
    private MockObject $strategyCurrencyShareStorage;
    private MockObject $currencyStorage;
    private \DateTime $dateTime;

    protected function setUp(): void
    {
        parent::setUp();

        $this->strategyCurrencyShareStorage = $this->createMock(StrategyCurrencyStorage::class);
        $this->currencyStorage = $this->createMock(CurrencyStorage::class);
        $this->dateTime = new \DateTime('2022-05-21 13:26:43');
        $this->currencyShare = new CurrencyShare(
            $this->dateTime,
            $this->strategyCurrencyShareStorage,
            $this->currencyStorage
        );
    }

    public function testGetTargetShare()
    {
        $userId = 1;

        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('RUB');
        $strategyCurrencyEntity01 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity01->setCurrencyId(1);
        $strategyCurrencyEntity01->setShare(0.4);
        $strategyCurrencyEntity01->setCurrency($currencyEntity01);

        $currencyEntity02 = new CurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('USD');
        $strategyCurrencyEntity02 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity02->setCurrencyId(2);
        $strategyCurrencyEntity02->setShare(0.4);
        $strategyCurrencyEntity02->setCurrency($currencyEntity02);

        $currencyEntity03 = new CurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('EUR');

        $this->strategyCurrencyShareStorage->expects($this->once())->method('findActiveByUserId')
            ->with($userId)
            ->willReturn([$strategyCurrencyEntity01, $strategyCurrencyEntity02]);
        $this->currencyStorage->expects($this->once())->method('findAll')
            ->willReturn([$currencyEntity01, $currencyEntity02, $currencyEntity03]);

        $currencyStrategy = new ShareCollection([
            new TargetShareModel(['externalId' => 'RUB', 'name' => 'RUB', 'value' => 0.4]),
            new TargetShareModel(['externalId' => 'USD', 'name' => 'USD', 'value' => 0.4]),
            new TargetShareModel(['externalId' => 'EUR', 'name' => 'EUR', 'value' => 0])
        ]);

        $this->assertEquals($currencyStrategy, $this->currencyShare->getTargetShare($userId));
    }

    /**
     * @param $instrumentCollection
     * @param ShareCollection $expected
     */
    #[DataProvider('dataCalculateShare')]
    public function testCalculateShare($instrumentCollection, ShareCollection $expected): void
    {
        $balance = new PortfolioBalanceCollection();
        $result = $this->currencyShare->calculateShare($instrumentCollection, $balance);
        $this->assertEquals($expected, $result);
    }

    public static function dataCalculateShare(): array
    {
        $instr01 = TITestCase::createBrokerPortfolioPosition([
            'instrumentType' => 'currency',
            'averagePositionPrice' => [
                'amount' => 70,
                'currency' => 'RUB'
            ]
        ]);

        $instr02 = TITestCase::createBrokerPortfolioPosition([
            'instrumentType' => 'stock',
            'averagePositionPrice' => [
                'amount' => 2,
                'currency' => 'USD'
            ]
        ]);

        $instr03 = TITestCase::createBrokerPortfolioPosition([
            'instrumentType' => 'stock',
            'averagePositionPrice' => [
                'amount' => null,
                'currency' => 'USD'
            ]
        ]);

        $amount01 = 700;
        $amount02 = 6300;
        $amount03 = 7000;

        $instrumentModel01 = new InstrumentModel(array(
            'instrument' => $instr01,
            'amount' => $amount01
        ));
        $instrumentModel02 = new InstrumentModel(array(
            'instrument' => $instr02,
            'amount' => $amount02
        ));
        $instrumentModel03 = new InstrumentModel(array(
            'instrument' => $instr03,
            'amount' => $amount03
        ));

        $instrumentCollection01 = new InstrumentCollection(array(
            $instrumentModel01, $instrumentModel02, $instrumentModel03
        ));
        $instrumentCollection02 = new InstrumentCollection(array(
            $instrumentModel01
        ));
        $expected01 = new ShareCollection([
            'RUB' => '0.05',
            'USD' => '0.95'
        ]);
        $expected02 = new ShareCollection([
            'RUB' => '1'
        ]);

        return [
            'Success Case' => [$instrumentCollection01, $expected01],
            'One Currency Instruments' => [$instrumentCollection02, $expected02]
        ];
    }

    public function testCalculateTotalAmount()
    {
        $balance = new PortfolioBalanceCollection();
        $instruments = $this->getMockBuilder(InstrumentCollection::class)->disableOriginalConstructor()
            ->getMock();
        $instruments->expects($this->once())->method('calculateTotalAmount')
            ->with()->willReturn(100);

        $this->assertEquals(100, $this->currencyShare->calculateTotalAmount($instruments, $balance));
    }

    public function testUpdateTargetShares()
    {
        $dateTime01 = new \DateTime('2021-01-02 12:00:00');
        $userId01 = 1;

        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setId(1);
        $currencyEntity01->setIso('RUB');
        $currencyEntity02 = new CurrencyEntity();
        $currencyEntity02->setId(2);
        $currencyEntity02->setIso('EUR');
        $currencyEntity03 = new CurrencyEntity();
        $currencyEntity03->setId(3);
        $currencyEntity03->setIso('USD');

        $strategyEntity01 = new StrategyCurrencyEntity();
        $strategyEntity01->setId(1);
        $strategyEntity01->setIsActive(1);
        $strategyEntity01->setUpdatedDate($dateTime01);

        $targetShare01 = new TargetShareModel(['externalId' => 'RUB', 'value' => 0.3]);
        $targetShare02 = new TargetShareModel(['externalId' => 'USD', 'value' => 0.2]);
        $targetShare03 = new TargetShareModel(['externalId' => 'EUR', 'value' => 0.15]);
        $shareCollection = new ShareCollection([$targetShare01, $targetShare02, $targetShare03]);

        $toUpdateStrategyEntity01 = new StrategyCurrencyEntity();
        $toUpdateStrategyEntity01->setId(1);
        $toUpdateStrategyEntity01->setIsActive(0);
        $toUpdateStrategyEntity01->setUpdatedDate($this->dateTime);

        $toUpdateStrategyEntity02 = new StrategyCurrencyEntity();
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setCreatedDate($this->dateTime);
        $toUpdateStrategyEntity02->setUpdatedDate($this->dateTime);
        $toUpdateStrategyEntity02->setIsActive(1);
        $toUpdateStrategyEntity02->setShare(0.3);
        $toUpdateStrategyEntity02->setUserId($userId01);
        $toUpdateStrategyEntity02->setCurrencyId(1);
        $toUpdateStrategyEntity02->setCurrency($currencyEntity01);

        $toUpdateStrategyEntity03 = new StrategyCurrencyEntity();
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setCreatedDate($this->dateTime);
        $toUpdateStrategyEntity03->setUpdatedDate($this->dateTime);
        $toUpdateStrategyEntity03->setIsActive(1);
        $toUpdateStrategyEntity03->setShare(0.2);
        $toUpdateStrategyEntity03->setUserId($userId01);
        $toUpdateStrategyEntity03->setCurrencyId(2);
        $toUpdateStrategyEntity03->setCurrency($currencyEntity02);

        $toUpdateStrategyEntity04 = new StrategyCurrencyEntity();
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setCreatedDate($this->dateTime);
        $toUpdateStrategyEntity04->setUpdatedDate($this->dateTime);
        $toUpdateStrategyEntity04->setIsActive(1);
        $toUpdateStrategyEntity04->setShare(0.15);
        $toUpdateStrategyEntity04->setUserId($userId01);
        $toUpdateStrategyEntity04->setCurrencyId(3);
        $toUpdateStrategyEntity04->setCurrency($currencyEntity03);

        $toUpdated01 = [
            $toUpdateStrategyEntity01,
            $toUpdateStrategyEntity02,
            $toUpdateStrategyEntity03,
            $toUpdateStrategyEntity04
        ];

        $this->strategyCurrencyShareStorage->expects($this->once())
            ->method('findActiveByUserId')
            ->with($userId01)
            ->willReturn([$strategyEntity01]);

        $this->currencyStorage->expects($this->exactly($shareCollection->count()))
            ->method('findOneByIso')
            ->with(...WithConsecutive::create(
                [$targetShare01->getExternalId()],
                [$targetShare02->getExternalId()],
                [$targetShare03->getExternalId()]
            ))->willReturnOnConsecutiveCalls($currencyEntity01, $currencyEntity02, $currencyEntity03);

        $this->strategyCurrencyShareStorage->expects($this->once())->method('updateArrayEntities')
            ->with($toUpdated01);

        $this->currencyShare->updateTargetShare($userId01, $shareCollection);
    }
}
