<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Item\Entities\ItemOperationsImportEntity;
use App\Item\Storages\ItemOperationImportStorage;
use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;
use App\Services\MarketOperationServiceFillDecorator;
use App\Services\MarketOperationsService;
use App\User\Services\UserService;
use PHPUnit\Framework\TestCase;

class MarketOperationsServiceFillDecoratorTest extends TestCase
{
    private MarketOperationsService $operationService;
    private ItemOperationImportStorage $itemOperationImportStorage;
    private MarketOperationServiceFillDecorator $operationServiceFillDecorator;
    private UserService $userService;
    private \DateTime $dateTime;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dateTime = new \DateTime('2022-11-27 12:15:11');
        $this->operationService = $this->createMock(MarketOperationsService::class);
        $this->itemOperationImportStorage = $this->createMock(ItemOperationImportStorage::class);
        $this->userService = $this->createMock(UserService::class);
        $this->operationServiceFillDecorator = new MarketOperationServiceFillDecorator(
            $this->dateTime,
            $this->userService,
            $this->operationService,
            $this->itemOperationImportStorage
        );
    }

    public function testImportBrokerOperations()
    {
        $this->userService->expects($this->once())->method('getUserId')->willReturn(20);

        $dateFrom = new \DateTime('2022-11-01');
        $dateTo = new \DateTime('2022-11-26');
        $filter = new OperationFiltersModel([
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo
        ]);
        $userBrokerAccountId = 2;

        $statistic = new ImportStatisticModel([
            'added' => 10,
            'updated' => 5,
            'skipped' => 2
        ]);

        $importEntity = new ItemOperationsImportEntity();
        $importEntity->setImportDateStart($dateFrom);
        $importEntity->setImportDateEnd($dateTo);
        $importEntity->setUserBrokerAccountId($userBrokerAccountId);
        $importEntity->setUserId(20);
        $importEntity->setCreatedDate($this->dateTime);
        $importEntity->setAdded(10);
        $importEntity->setUpdated(5);
        $importEntity->setSkipped(2);

        $this->operationService->expects($this->once())->method('importBrokerOperations')
            ->with($filter, $userBrokerAccountId)
            ->willReturn($statistic);
        $this->itemOperationImportStorage->expects($this->once())->method('addEntity')
            ->with($importEntity);

        $this->assertEquals(
            $statistic,
            $this->operationServiceFillDecorator->importBrokerOperations($filter, $userBrokerAccountId)
        );
    }
}
