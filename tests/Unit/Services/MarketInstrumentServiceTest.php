<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Market\Storages\MarketInstrumentStorage;
use App\Services\MarketInstrumentService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MarketInstrumentServiceTest extends TestCase
{
    private MarketInstrumentStorage $marketInstrumentStorage;
    private MarketInstrumentService $marketInstrumentService;

    public function setUp(): void
    {
        parent::setUp();

        $this->marketInstrumentStorage = $this->createMock(MarketInstrumentStorage::class);
        $this->marketInstrumentService = new MarketInstrumentService(
            $this->marketInstrumentStorage
        );
    }

    #[DataProvider('dataGetStocks')]
    public function testGetStocks(
        MarketInstrumentCollection $expected,
        string|null $country,
        string $currency,
        string|null $sector,
        array $stocks
    ): void {
        $this->marketInstrumentStorage->expects($this->once())->method('findStocks')
            ->with($currency)
            ->willReturn($stocks);

        $this->assertEquals($expected, $this->marketInstrumentService->getStocks($country, $currency, $sector));
    }

    public static function dataGetStocks(): array
    {
        $currency01 = 'RUB';
        $country01 = 'RU';
        $country02 = null;
        $sector01 = 'Technology';
        $sector02 = null;

        $marketStockEntity01 = new MarketStockEntity();
        $marketStockEntity01->setCountryIso('RU');
        $marketStockEntity01->setSector('Technology');

        $marketStockEntity02 = new MarketStockEntity();
        $marketStockEntity02->setCountryIso('CN');
        $marketStockEntity02->setSector('Technology');

        $marketStockEntity03 = new MarketStockEntity();
        $marketStockEntity03->setCountryIso('RU');
        $marketStockEntity03->setSector('Utilities');

        $marketInstrumentEntity01 = new MarketInstrumentEntity();
        $marketInstrumentEntity01->setId(1);
        $marketInstrumentEntity01->setMarketStock($marketStockEntity01);

        $marketInstrumentEntity02 = new MarketInstrumentEntity();
        $marketInstrumentEntity02->setId(2);
        $marketInstrumentEntity02->setMarketStock($marketStockEntity02);

        $marketInstrumentEntity03 = new MarketInstrumentEntity();
        $marketInstrumentEntity03->setId(3);
        $marketInstrumentEntity03->setMarketStock($marketStockEntity03);

        $stocks01 = [$marketInstrumentEntity01, $marketInstrumentEntity02, $marketInstrumentEntity03];

        $expected01 = new MarketInstrumentCollection([$marketInstrumentEntity01]);
        $expected02 = new MarketInstrumentCollection([$marketInstrumentEntity01, $marketInstrumentEntity02]);
        $expected03 = new MarketInstrumentCollection([$marketInstrumentEntity01, $marketInstrumentEntity03]);

        return [
            'Common Case' => [
                $expected01,
                $country01,
                $currency01,
                $sector01,
                $stocks01
            ],
            'Null Country Case' => [
                $expected02,
                $country02,
                $currency01,
                $sector01,
                $stocks01
            ],
            'Null Sector Case' => [
                $expected03,
                $country01,
                $currency01,
                $sector02,
                $stocks01
            ]
        ];
    }
}
