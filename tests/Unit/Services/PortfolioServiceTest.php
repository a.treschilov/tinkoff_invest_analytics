<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Collections\InstrumentCollection;
use App\Collections\PortfolioCollection;
use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Models\InstrumentModel;
use App\Services\ExchangeCurrencyService;
use App\Services\PortfolioService;
use App\Services\StrategyService;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TITestCase;

class PortfolioServiceTest extends TestCase
{
    private ExchangeCurrencyService $exchangeStub;
    private $portfolio;
    private \DateTime $currentDateTime;

    protected function setUp(): void
    {
        parent::setUp();

        $this->exchangeStub = $this->createMock(ExchangeCurrencyService::class);
        $this->exchangeStub->method('getExchangeRate')->withAnyParameters()->willReturn(70.0);

        $this->currentDateTime = new \DateTime('2023-01-22 16:26:14');
        $strategy = $this->createMock(StrategyService::class);
        $this->portfolio = new PortfolioService($this->currentDateTime, $strategy, $this->exchangeStub);
    }

    /**
     * @param PortfolioPositionCollection $instruments
     * @param $excepted
     * @param null $exception
     * @throws ExchangeUnsupportedCurrencyException
     */
    #[DataProvider('dataConvertToRub')]
    public function testConvertToRub(PortfolioPositionCollection $instruments, $excepted, $exception = null): void
    {
        if (null !== $exception) {
            $this->expectException($exception);
        }

        $this->assertEquals($excepted, $this->portfolio->convertToRub($instruments));
    }

    public static function dataConvertToRub(): array
    {
        $averagePrice01 = [
            'amount' => 70,
            'currency' => 'RUB'
        ];
        $expectedYield01 = [
            'amount' => -2,
            'currency' => 'RUB'
        ];
        $currentPrice01 = [
            'amount' => 69.8,
            'currency' => 'RUB'
        ];
        $instr01 = TITestCase::createBrokerPortfolioPosition([
            'quantity' => 10,
            'expectedYield' => $expectedYield01,
            'instrumentType' => 'Currency',
            'averagePositionPrice' => $averagePrice01,
            'currentPrice' => $currentPrice01
        ]);

        $averagePrice02 = [
            'amount' => 2,
            'currency' => 'USD'
        ];
        $expectedYield02 = [
            'amount' => 10,
            'currency' => 'USD'
        ];
        $currentPrice02 = [
            'amount' => 3,
            'currency' => 'USD'
        ];
        $instr02 = TITestCase::createBrokerPortfolioPosition([
            'quantity' => 10,
            'expectedYield' => $expectedYield02,
            'instrumentType' => 'Currency',
            'averagePositionPrice' => $averagePrice02,
            'currentPrice' => $currentPrice02
        ]);

        $instr01Model = new InstrumentModel(array('instrument' => $instr01, 'amount' => 698));
        $instr02Model = new InstrumentModel(array('instrument' => $instr02, 'amount' => 2100));

        $instruments01 = new PortfolioPositionCollection([$instr01, $instr02]);
        $instrumentCollection01 = new InstrumentCollection([$instr01Model, $instr02Model]);

        return [
            'Success Case' => [$instruments01, $instrumentCollection01],
        ];
    }

    #[DataProvider('dataCalculateTotalAmount')]
    public function testCalculateTotalAmount(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balanceCollection,
        float $excepted
    ): void {
        $this->assertEquals($excepted, $this->portfolio->calculateTotalAmount($portfolio, $balanceCollection));
    }

    public static function dataCalculateTotalAmount(): array
    {
        $averagePrice01 = [
            'amount' => 70,
            'currency' => 'RUB'
        ];
        $expectedYield01 = [
            'amount' => -2,
            'currency' => 'RUB'
        ];
        $currentPrice01 = [
            'amount' => 69.8,
            'currency' => 'RUB'
        ];
        $instr01 = TITestCase::createBrokerPortfolioPosition([
            'quantity' => 10,
            'expectedYield' => $expectedYield01,
            'instrumentType' => 'currency',
            'averagePositionPrice' => $averagePrice01,
            'currentPrice' => $currentPrice01
        ]);

        $averagePrice02 = [
            'amount' => 2,
            'currency' => 'USD'
        ];
        $expectedYield02 = [
            'amount' => 10,
            'currency' => 'USD'
        ];
        $currentPrice02 = [
            'amount' => 3,
            'currency' => 'USD'
        ];
        $instr02 = TITestCase::createBrokerPortfolioPosition([
            'quantity' => 10,
            'expectedYield' => $expectedYield02,
            'instrumentType' => 'currency',
            'averagePositionPrice' => $averagePrice02,
            'currentPrice' => $currentPrice02
        ]);

        $balance01 = new BrokerPortfolioBalanceModel([
            'currency' => 'RUB',
            'amount' => 10,
            'blockedAmount' => 4
        ]);
        $balance02 = new BrokerPortfolioBalanceModel([
            'currency' => 'USD',
            'amount' => 7,
            'blockedAmount' => 2
        ]);
        $balanceCollection01 = new PortfolioBalanceCollection([$balance01, $balance02]);

        $instruments01 = new PortfolioPositionCollection([$instr01, $instr02]);
        $portfolio01 = new BrokerPortfolioModel(['positions' => $instruments01]);

        $portfolio02 = new BrokerPortfolioModel(['positions' => new PortfolioPositionCollection([])]);

        return [
            'Success Case' => [$portfolio01, $balanceCollection01, 2804],
            'Empty Portfolio Case' => [$portfolio02, $balanceCollection01, 0]
        ];
    }

    /** @dataProvider dataMergePortfolios */
    #[DataProvider('dataMergePortfolios')]
    public function testMergePortfolios($portfolioCollection, $expected): void
    {
        $this->assertEquals($expected, $this->portfolio->mergePortfolios($portfolioCollection));
    }

    public static function dataMergePortfolios(): array
    {
        $instr1 = TITestCase::createBrokerPortfolioPosition(['figi' => 't1']);
        $instr2 = TITestCase::createBrokerPortfolioPosition(['figi' => 't2']);
        $instr3 = TITestCase::createBrokerPortfolioPosition(['figi' => 't3']);
        $instr4 = TITestCase::createBrokerPortfolioPosition(['figi' => 't4']);
        $instr5 = TITestCase::createBrokerPortfolioPosition(['figi' => 't5']);
        $instr6 = TITestCase::createBrokerPortfolioPosition(['figi' => 't6']);

        $portfolio1 = new BrokerPortfolioModel([
            'positions' => new PortfolioPositionCollection([$instr1, $instr2, $instr3])
        ]);

        $portfolio2 = new BrokerPortfolioModel([
            'positions' => new PortfolioPositionCollection([$instr4, $instr5])
        ]);

        $portfolio3 = new BrokerPortfolioModel([
            'positions' => new PortfolioPositionCollection([$instr6])
        ]);

        $collection02 = new PortfolioCollection([$portfolio1, $portfolio2, $portfolio3]);
        $expected02 = new BrokerPortfolioModel([
            'positions' => new PortfolioPositionCollection([$instr1, $instr2, $instr3, $instr4, $instr5, $instr6])
        ]);

        return [
            'Merge 1 Portfolio' => [new PortfolioCollection([$portfolio1]), $portfolio1],
            'Merge 3 Portfolios' => [$collection02, $expected02]
        ];
    }

    public function testCalculateInstrumentShare()
    {
        $balance = new PortfolioBalanceCollection();
        $strategyService = $this->getMockBuilder(StrategyService::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['calculateShare'])
            ->getMock();
        $exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);

        $portfolioService = $this->getMockBuilder(PortfolioService::class)
            ->setConstructorArgs([$this->currentDateTime, $strategyService, $exchangeCurrencyService])
            ->onlyMethods(['convertToRub'])
            ->getMock();

        $instr01 = TITestCase::createBrokerPortfolioPosition([]);
        $instrCollection01 = new InstrumentCollection([$instr01]);
        $positionCollection01 = new PortfolioPositionCollection([$instr01]);
        $portfolioService->expects($this->once())->method('convertToRub')
            ->with($positionCollection01)
            ->willReturn($instrCollection01);
        $strategyService->expects($this->once())->method('calculateShare')
            ->with('stock', $instrCollection01);

        $portfolio = new BrokerPortfolioModel(['positions' => $positionCollection01]);
        $portfolioService->calculateInstrumentShare($portfolio, $balance);
    }

    public function testCalculateSectorShare()
    {
        $balance = new PortfolioBalanceCollection();
        $strategyService = $this->getMockBuilder(StrategyService::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['calculateShare'])
            ->getMock();
        $exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);

        $portfolioService = $this->getMockBuilder(PortfolioService::class)
            ->setConstructorArgs([$this->currentDateTime, $strategyService, $exchangeCurrencyService])
            ->onlyMethods(['convertToRub'])
            ->getMock();

        $instr01 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Stock']);
        $instr02 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Stock']);
        $instr03 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Etf']);

        $instrumentModel01 = new InstrumentModel(['instrument' => $instr01]);
        $instrumentModel02 = new InstrumentModel(['instrument' => $instr02]);
        $instrumentModel03 = new InstrumentModel(['instrument' => $instr03]);

        $instrCollection01 =  new InstrumentCollection([
            $instrumentModel01,
            $instrumentModel02,
            $instrumentModel03
        ]);
        $calculatedCollection01 =  new InstrumentCollection([
            $instrumentModel01,
            $instrumentModel02
        ]);
        $positionCollection01 = new PortfolioPositionCollection([$instr01, $instr02, $instr03]);
        $portfolioService->expects($this->once())->method('convertToRub')
            ->with($positionCollection01)
            ->willReturn($instrCollection01);
        $strategyService->expects($this->once())->method('calculateShare')
            ->with('sector', $calculatedCollection01);

        $tiPortfolio = new BrokerPortfolioModel(['positions' => $positionCollection01]);
        $portfolioService->calculateSectorShare($tiPortfolio, $balance);
    }

    public function testCalculateCountryShare()
    {
        $balance = new PortfolioBalanceCollection();
        $strategyService = $this->getMockBuilder(StrategyService::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['calculateShare'])
            ->getMock();
        $exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);

        $portfolioService = $this->getMockBuilder(PortfolioService::class)
            ->setConstructorArgs([$this->currentDateTime, $strategyService, $exchangeCurrencyService])
            ->onlyMethods(['convertToRub'])
            ->getMock();

        $instr01 = TITestCase::createBrokerPortfolioPosition([]);
        $instrCollection01 = new InstrumentCollection([$instr01]);
        $positionColldection01 = new PortfolioPositionCollection([$instr01]);
        $portfolioService->expects($this->once())->method('convertToRub')
            ->with($positionColldection01)
            ->willReturn($instrCollection01);
        $strategyService->expects($this->once())->method('calculateShare')
            ->with('country', $instrCollection01);

        $tiPortfolio = new BrokerPortfolioModel(['positions' => $positionColldection01]);
        $portfolioService->calculateCountryShare($tiPortfolio, $balance);
    }

    public function testCalculateCurrencyShare()
    {
        $balance = new PortfolioBalanceCollection();
        $strategyService = $this->getMockBuilder(StrategyService::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['calculateShare'])
            ->getMock();
        $exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);

        $portfolioService = $this->getMockBuilder(PortfolioService::class)
            ->setConstructorArgs([$this->currentDateTime, $strategyService, $exchangeCurrencyService])
            ->onlyMethods(['convertToRub'])
            ->getMock();

        $instr01 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Stock']);
        $instr02 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Currency']);
        $instr03 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Bond']);
        $instr04 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Etf']);
        $instr05 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Futures']);

        $instrumentModel01 = new InstrumentModel(['instrument' => $instr01]);
        $instrumentModel02 = new InstrumentModel(['instrument' => $instr02]);
        $instrumentModel03 = new InstrumentModel(['instrument' => $instr03]);
        $instrumentModel04 = new InstrumentModel(['instrument' => $instr04]);
        $instrumentModel05 = new InstrumentModel(['instrument' => $instr05]);

        $instrCollection01 = new InstrumentCollection([
            $instrumentModel01,
            $instrumentModel02,
            $instrumentModel03,
            $instrumentModel04,
            $instrumentModel05
        ]);
        $filteredInstrumentCollection01 = new InstrumentCollection([
            $instrumentModel01,
            $instrumentModel04,
            $instrumentModel03,
            $instrumentModel05
        ]);

        $positionCollection01 = new PortfolioPositionCollection([$instr01, $instr02, $instr03, $instr04, $instr05]);
        $tiPortfolio = new BrokerPortfolioModel(['positions' => $positionCollection01]);

        $portfolioService->expects($this->once())->method('convertToRub')
            ->with($positionCollection01)
            ->willReturn($instrCollection01);
        $strategyService->expects($this->once())->method('calculateShare')
            ->with('currency', $filteredInstrumentCollection01);

        $portfolioService->calculateCurrencyShare($tiPortfolio, $balance);
    }
}
