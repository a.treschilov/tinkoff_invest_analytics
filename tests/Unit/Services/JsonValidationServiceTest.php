<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Exceptions\JsonSchemeException;
use App\Services\JsonValidatorService;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Errors\ValidationError;
use Opis\JsonSchema\Resolvers\SchemaResolver;
use Opis\JsonSchema\ValidationResult;
use Opis\JsonSchema\Validator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class JsonValidationServiceTest extends TestCase
{
    private Validator $validator;
    private ErrorFormatter $errorFormatter;
    private JsonValidatorService $jsonValidatorService;

    public function setUp(): void
    {
        parent::setUp();

        $this->validator = $this->createMock(Validator::class);
        $this->errorFormatter = $this->createMock(ErrorFormatter::class);
        $this->jsonValidatorService = new JsonValidatorService($this->validator, $this->errorFormatter);
    }

    /** @dataProvider dataValidate */
    #[DataProvider('dataValidate')]
    public function testValidate(array|null $exception, bool $isValid): void
    {
        $scheme = 'validator.json';
        $requestBody = [
            'user' => 'a.treschilov'
        ];
        $data = json_decode(json_encode($requestBody));

        $url = 'https://hakkes.com/schemes/';
        $path = JsonValidatorService::SCHEME_PATH;
        $schemaResolver = $this->createMock(SchemaResolver::class);
        $validationResult = $this->createMock(ValidationResult::class);
        $validationError = $this->createMock(ValidationError::class);

        $this->validator->expects($this->once())->method('resolver')->willReturn($schemaResolver);
        $this->validator->expects($this->once())->method('validate')
            ->with($data, $url . $scheme)
            ->willReturn($validationResult);
        $schemaResolver->expects($this->once())->method('registerPrefix')
            ->with($url, $path);
        $validationResult->expects($this->once())->method('isValid')->willReturn($isValid);

        if (null !== $exception) {
            $errorMessage = ['Json Scheme validation error'];
            $validationResult->expects($this->once())->method('error')->willReturn($validationError);
            $this->errorFormatter->expects($this->once())->method('format')
                ->with($validationError, false)
                ->willReturn($errorMessage);

            $this->expectException($exception['class']);
        }

        $this->assertEquals(true, $this->jsonValidatorService->validate($scheme, $requestBody));
    }

    public static function dataValidate(): array
    {
        $exception01 = null;
        $isValid01 = true;

        $exception02 = [
            'class' => JsonSchemeException::class
        ];
        $isValid02 = false;

        return [
            'Common Case' => [
                $exception01,
                $isValid01
            ],
            'No Valid Scheme' => [
                $exception02,
                $isValid02
            ]
        ];
    }
}
