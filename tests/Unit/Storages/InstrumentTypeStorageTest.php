<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\InstrumentTypeEntity;
use App\Storages\InstrumentTypeStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class InstrumentTypeStorageTest extends TestCase
{
    private MockObject $entityManager;
    private MockObject $repository;
    private InstrumentTypeStorage $instrumentTypeStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->instrumentTypeStorage = new InstrumentTypeStorage($this->entityManager);
    }

    public function testFindActive()
    {
        $instrumentTypeEntity01 = new InstrumentTypeEntity();
        $instrumentTypeEntity01->setId(1);
        $instrumentTypeEntity02 = new InstrumentTypeEntity();
        $instrumentTypeEntity02->setId(2);
        $instrumentTypeEntity03 = new InstrumentTypeEntity();
        $instrumentTypeEntity03->setId(3);
        $instrumentTypes01 = [$instrumentTypeEntity01, $instrumentTypeEntity02, $instrumentTypeEntity03];

        $this->repository->expects($this->once())->method('findBy')
            ->with(['isActive' => 1])->willReturn($instrumentTypes01);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(InstrumentTypeEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($instrumentTypes01, $this->instrumentTypeStorage->findActive());
    }

    public function testFindByExternalId()
    {
        $externalId01 = 'stock';

        $instrumentTypeEntity01 = new InstrumentTypeEntity();
        $instrumentTypeEntity01->setId(1);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['externalId' => $externalId01])->willReturn($instrumentTypeEntity01);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(InstrumentTypeEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($instrumentTypeEntity01, $this->instrumentTypeStorage->findByExternalId($externalId01));
    }
}
