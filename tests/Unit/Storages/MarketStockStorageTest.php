<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\MarketStockEntity;
use App\Storages\MarketStockStorage;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class MarketStockStorageTest extends TestCase
{
    public function testAddEntity()
    {
        $entity = new MarketStockEntity();

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['persist', 'flush'])
            ->getMock();
        $entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $entityManager->expects($this->once())->method('flush');

        $marketStockStorage = new MarketStockStorage($entityManager);
        $marketStockStorage->addEntity($entity);
    }
}
