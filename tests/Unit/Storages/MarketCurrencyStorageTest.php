<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\MarketCurrencyEntity;
use App\Storages\MarketCurrencyStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketCurrencyStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private MarketCurrencyStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new MarketCurrencyStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $entity01 = new MarketCurrencyEntity();
        $entity01->setId(1);
        $entity02 = new MarketCurrencyEntity();
        $entity02->setId(1);

        $expected = [$entity01, $entity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketCurrencyEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findAll')
            ->willReturn([$entity01, $entity02]);

        $this->assertEquals($expected, $this->storage->findAll());
    }

    public function testAddEntity()
    {
        $entity = new MarketCurrencyEntity();

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->storage->addEntity($entity);
    }

    public function testFindOneByIso()
    {
        $iso = 'USD';

        $entity01 = new MarketCurrencyEntity();
        $entity01->setId(1);

        $expected = $entity01;

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketCurrencyEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['iso' => $iso])
            ->willReturn($entity01);

        $this->assertEquals($expected, $this->storage->findOneByIso($iso));
    }
}
