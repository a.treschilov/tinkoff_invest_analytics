<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\StrategyCountryEntity;
use App\Storages\StrategyCountryStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class StrategyCountryStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private StrategyCountryStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new StrategyCountryStorage($this->entityManager);
    }

    public function testFindActiveByUserId()
    {
        $userId01 = 1;

        $strategyCountryEntity01 = new StrategyCountryEntity();
        $strategyCountryEntity01->setId(1);

        $strategyCountryEntity02 = new StrategyCountryEntity();
        $strategyCountryEntity02->setId(1);

        $expected = [$strategyCountryEntity02, $strategyCountryEntity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(StrategyCountryEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findBy')
            ->with(['userId' => $userId01, 'isActive' => 1])
            ->willReturn([$strategyCountryEntity01, $strategyCountryEntity02]);

        $this->assertEquals($expected, $this->storage->findActiveByUserId($userId01));
    }

    public function testUpdateArrayEntities()
    {
        $strategyCountryEntity01 = new StrategyCountryEntity();
        $strategyCountryEntity01->setId(1);

        $strategyCountryEntity02 = new StrategyCountryEntity();
        $strategyCountryEntity02->setId(2);

        $consecutiveCallsParams = [[$strategyCountryEntity01], [$strategyCountryEntity02]];
        $toUpdateEntities01 = [$strategyCountryEntity01, $strategyCountryEntity02];

        $this->entityManager->expects($this->exactly(count($toUpdateEntities01)))
            ->method('persist')
            ->with(...WithConsecutive::create(...$consecutiveCallsParams));
        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->storage->updateArrayEntities($toUpdateEntities01);
    }
}
