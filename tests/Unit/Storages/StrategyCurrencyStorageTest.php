<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\StrategyCurrencyEntity;
use App\Storages\StrategyCurrencyStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class StrategyCurrencyStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private StrategyCurrencyStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new StrategyCurrencyStorage($this->entityManager);
    }

    public function testFindActiveByUserId()
    {
        $userId01 = 1;

        $strategyCurrencyEntity01 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity01->setId(1);

        $strategyCurrencyEntity02 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity02->setId(1);

        $expected = [$strategyCurrencyEntity02, $strategyCurrencyEntity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(StrategyCurrencyEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findBy')
            ->with(['userId' => $userId01, 'isActive' => 1])
            ->willReturn([$strategyCurrencyEntity01, $strategyCurrencyEntity02]);

        $this->assertEquals($expected, $this->storage->findActiveByUserId($userId01));
    }

    public function testUpdateArrayEntities()
    {
        $strategyCurrencyEntity01 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity01->setId(1);

        $strategyCurrencyEntity02 = new StrategyCurrencyEntity();
        $strategyCurrencyEntity02->setId(2);

        $consecutiveCallsParams = [[$strategyCurrencyEntity01], [$strategyCurrencyEntity02]];
        $toUpdateEntities01 = [$strategyCurrencyEntity01, $strategyCurrencyEntity02];

        $this->entityManager->expects($this->exactly(count($toUpdateEntities01)))
            ->method('persist')
            ->with(...WithConsecutive::create(...$consecutiveCallsParams));
        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->storage->updateArrayEntities($toUpdateEntities01);
    }
}
