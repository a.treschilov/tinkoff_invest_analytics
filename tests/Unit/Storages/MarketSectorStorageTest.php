<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\MarketSectorEntity;
use App\Storages\MarketSectorStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketSectorStorageTest extends TestCase
{
    private MockObject $entityManager;
    private MockObject $repository;
    private MarketSectorStorage $marketSectorStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->marketSectorStorage = new MarketSectorStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $sector01 = new MarketSectorEntity();
        $sector01->setId(1);
        $sector02 = new MarketSectorEntity();
        $sector02->setId(2);

        $expected = [$sector01, $sector02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketSectorEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findBy')
            ->with([], ['name' => 'ASC'])
            ->willReturn([$sector01, $sector02]);

        $this->assertEquals($expected, $this->marketSectorStorage->findAll());
    }

    public function testFindById()
    {
        $id = 1;
        $sector01 = new MarketSectorEntity();
        $sector01->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketSectorEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['id' => $id])
            ->willReturn($sector01);

        $this->assertEquals($sector01, $this->marketSectorStorage->findById($id));
    }

    public function testFindByExternalId()
    {
        $externalId = 'technology';
        $sector01 = new MarketSectorEntity();
        $sector01->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketSectorEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['externalId' => $externalId])
            ->willReturn($sector01);

        $this->assertEquals($sector01, $this->marketSectorStorage->findOneByExternalId($externalId));
    }
}
