<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\MarketCountryEntity;
use App\Storages\MarketCountryStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketCountryStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private MarketCountryStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new MarketCountryStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $entity01 = new MarketCountryEntity();
        $entity01->setId(1);
        $entity02 = new MarketCountryEntity();
        $entity02->setId(1);

        $expected = [$entity01, $entity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketCountryEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findAll')
            ->willReturn([$entity01, $entity02]);

        $this->assertEquals($expected, $this->storage->findAll());
    }

    public function testFindOneByIso()
    {
        $iso = 'US';

        $entity01 = new MarketCountryEntity();
        $entity01->setId(1);

        $expected = $entity01;

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(MarketCountryEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['iso' => $iso])
            ->willReturn($entity01);

        $this->assertEquals($expected, $this->storage->findOneByIso($iso));
    }
}
