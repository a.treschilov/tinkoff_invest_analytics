<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\StrategyInstrumentTypeEntity;
use App\Storages\StrategyInstrumentTypeStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class StrategyInstrumentTypeStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private StrategyInstrumentTypeStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new StrategyInstrumentTypeStorage($this->entityManager);
    }

    public function testFindActiveByUserId()
    {
        $userId01 = 1;

        $strategyInstrumentTypeEntity01 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity01->setId(1);

        $strategyInstrumentTypeEntity02 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity02->setId(2);

        $expected = [$strategyInstrumentTypeEntity01, $strategyInstrumentTypeEntity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(StrategyInstrumentTypeEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findBy')
            ->with(['userId' => $userId01, 'isActive' => 1])
            ->willReturn([$strategyInstrumentTypeEntity01, $strategyInstrumentTypeEntity02]);

        $this->assertEquals($expected, $this->storage->findActiveByUserId($userId01));
    }

    public function testUpdateArrayEntities()
    {
        $strategyInstrumentTypeEntity01 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity01->setId(1);

        $strategyInstrumentTypeEntity02 = new StrategyInstrumentTypeEntity();
        $strategyInstrumentTypeEntity02->setId(2);

        $consecutiveCallsParams = [[$strategyInstrumentTypeEntity01], [$strategyInstrumentTypeEntity02]];
        $toUpdateEntities01 = [$strategyInstrumentTypeEntity01, $strategyInstrumentTypeEntity02];

        $this->entityManager->expects($this->exactly(count($toUpdateEntities01)))
            ->method('persist')
            ->with(...WithConsecutive::create(...$consecutiveCallsParams));
        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->storage->updateArrayEntities($toUpdateEntities01);
    }
}
