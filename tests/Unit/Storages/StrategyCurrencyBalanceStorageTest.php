<?php

declare(strict_types=1);

namespace Tests\Unit\Storages;

use App\Entities\StrategyCurrencyBalanceEntity;
use App\Storages\StrategyCurrencyBalanceStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class StrategyCurrencyBalanceStorageTest extends TestCase
{
    private MockObject $objectRepository;
    private MockObject $entityManager;
    private StrategyCurrencyBalanceStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new StrategyCurrencyBalanceStorage($this->entityManager);
    }

    public function testFindActiveByUserId()
    {
        $userId01 = 1;

        $entity01 = new StrategyCurrencyBalanceEntity();
        $entity01->setId(1);

        $entity02 = new StrategyCurrencyBalanceEntity();
        $entity02->setId(1);

        $expected = [$entity02, $entity02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(StrategyCurrencyBalanceEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findBy')
            ->with(['userId' => $userId01, 'isActive' => 1])
            ->willReturn([$entity01, $entity02]);

        $this->assertEquals($expected, $this->storage->findActiveByUserId($userId01));
    }

    public function testUpdateArrayEntities()
    {
        $strategyCurrencyBalanceEntity01 = new StrategyCurrencyBalanceEntity();
        $strategyCurrencyBalanceEntity01->setId(1);

        $strategyCurrencyBalanceEntity02 = new StrategyCurrencyBalanceEntity();
        $strategyCurrencyBalanceEntity02->setId(2);

        $consecutiveCallsParams = [[$strategyCurrencyBalanceEntity01], [$strategyCurrencyBalanceEntity02]];
        $toUpdateEntities01 = [$strategyCurrencyBalanceEntity01, $strategyCurrencyBalanceEntity02];

        $this->entityManager->expects($this->exactly(count($toUpdateEntities01)))
            ->method('persist')
            ->with(...WithConsecutive::create(...$consecutiveCallsParams));
        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->storage->updateArrayEntities($toUpdateEntities01);
    }
}
