<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Services;

use App\Intl\Collections\CountryCollection;
use App\Intl\Entities\CountryEntity;
use App\Intl\Services\CountryService;
use App\Intl\Storages\CountryStorage;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TITestCase;

class CountryServiceTest extends TestCase
{
    private CountryStorage $countryStorage;
    private CountryService $countryService;

    public function setUp(): void
    {
        parent::setUp();

        $this->countryStorage = $this->createMock(CountryStorage::class);

        $this->countryService = new CountryService($this->countryStorage);
    }

    public function testGetList(): void
    {
        $country01 = new CountryEntity();
        $country01->setIso('RU');
        $country02 = new CountryEntity();
        $country02->setIso('CN');
        $country03 = new CountryEntity();
        $country03->setIso('DE');
        $countryCollection01 = new CountryCollection([$country01, $country02, $country03]);

        $this->countryStorage->expects($this->once())->method('findAll')
            ->willReturn([$country01, $country02, $country03]);

        $this->assertEquals($countryCollection01, $this->countryService->getList());
    }

    #[DataProvider('dataGetIsoByName')]
    public function testGetCountryByName(
        CountryEntity|null $expected,
        string $countryName,
        string $countryFindName,
        CountryEntity|null $countryEntity
    ) {
        $this->countryStorage->expects($this->once())->method('findOneByName')
            ->with($countryFindName)
            ->willReturn($countryEntity);

        $this->assertEquals($expected, $this->countryService->getCountryByName($countryName));
    }

    public static function dataGetIsoByName(): array
    {
        $countryName01 = 'China';
        $countryName02 = 'Russia';
        $countryName03 = 'Taiwan';
        $countryName04 = 'South Korea';

        $countryFindName01 = 'China';
        $countryFindName02 = 'Russian Federation';
        $countryFindName03 = 'Taiwan, Province of China';
        $countryFindName04 = 'Korea, Republic of';

        $countryEntity01 = new CountryEntity();
        $countryEntity01->setIso('CN');
        $countryEntity02 = new CountryEntity();
        $countryEntity02->setIso('RU');
        $countryEntity03 = null;
        $countryEntity04 = new CountryEntity();
        $countryEntity04->setIso('KR');

        $expected01 = $countryEntity01;
        $expected02 = $countryEntity02;
        $expected03 = $countryEntity03;
        $expected04 = $countryEntity04;

        return [
            'Common Case' => [
                $expected01,
                $countryName01,
                $countryFindName01,
                $countryEntity01
            ],
            'Convert Name Case' => [
                $expected02,
                $countryName02,
                $countryFindName02,
                $countryEntity02
            ],
            'Not found country Case' => [
                $expected03,
                $countryName03,
                $countryFindName03,
                $countryEntity03
            ],
            'South Korea Case' => [
                $expected04,
                $countryName04,
                $countryFindName04,
                $countryEntity04
            ]
        ];
    }

    public function testGetCountryByIso(): void
    {
        $expected = new CountryEntity();
        $expected->setIso('RU');

        $countryEntity = new CountryEntity();
        $countryEntity->setIso('RU');

        $iso = 'RU';

        $this->countryStorage->expects($this->once())->method('findOneByIso')
            ->with($iso)
            ->willReturn($countryEntity);

        $this->assertEquals($expected, $this->countryService->getCountryByIso($iso));
    }
}
