<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Services;

use App\Intl\Collections\CurrencyCollection;
use App\Intl\Entities\CurrencyEntity;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\ThirdParty\ExchangeRateApiInterface;
use App\Intl\Services\ThirdParty\ExchangeRatesApiService;
use App\Intl\Storages\CurrencyStorage;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CurrencyServiceTest extends TestCase
{
    private CurrencyStorage $currencyStorage;
    private ExchangeRateApiInterface $exchangeRateApi;
    private CurrencyService $currencyService;

    public function setUp(): void
    {
        parent::setUp();

        $this->currencyStorage = $this->createMock(CurrencyStorage::class);
        $this->exchangeRateApi = $this->createMock(ExchangeRateApiInterface::class);
        $this->currencyService = new CurrencyService($this->currencyStorage, $this->exchangeRateApi);
    }

    public function testGetList()
    {
        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setIso('RUB');
        $currencyEntity02 = new CurrencyEntity();
        $currencyEntity02->setIso('USD');
        $currencyEntity03 = new CurrencyEntity();
        $currencyEntity03->setIso('EUR');
        $currencyCollection = new CurrencyCollection([$currencyEntity01, $currencyEntity02, $currencyEntity03]);


        $this->currencyStorage->expects($this->once())->method('findAll')
            ->willReturn([$currencyEntity01, $currencyEntity02, $currencyEntity03]);

        $this->assertEquals($currencyCollection, $this->currencyService->getList());
    }

    public function testGetCurrencyByIso()
    {
        $iso01 = 'RUB';

        $currencyEntity01 = new CurrencyEntity();
        $currencyEntity01->setIso('RUB');

        $this->currencyStorage->expects($this->once())->method('findOneByIso')
            ->with($iso01)
            ->willReturn($currencyEntity01);

        $this->assertEquals($currencyEntity01, $this->currencyService->getCurrencyByIso($iso01));
    }
}
