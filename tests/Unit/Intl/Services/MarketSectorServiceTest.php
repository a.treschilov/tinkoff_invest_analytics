<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Services;

use App\Entities\MarketSectorEntity;
use App\Intl\Collections\MarketSectorCollection;
use App\Intl\Services\MarketSectorService;
use App\Storages\MarketSectorStorage;
use JetBrains\PhpStorm\ArrayShape;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MarketSectorServiceTest extends TestCase
{
    private MockObject $sectorStorage;
    private MarketSectorService $marketSectorService;

    public function setUp(): void
    {
        parent::setUp();

        $this->sectorStorage = $this->createMock(MarketSectorStorage::class);
        $this->marketSectorService = new MarketSectorService($this->sectorStorage);
    }

    public function testGetList()
    {
        $sectorEntity01 = new MarketSectorEntity();
        $sectorEntity01->setId(1);
        $sectorEntity02 = new MarketSectorEntity();
        $sectorEntity02->setId(2);
        $sectorEntity03 = new MarketSectorEntity();
        $sectorEntity03->setId(3);
        $sectorCollection = new MarketSectorCollection([$sectorEntity01, $sectorEntity02, $sectorEntity03]);

        $this->sectorStorage->expects($this->once())->method('findAll')
            ->willReturn([$sectorEntity01, $sectorEntity02, $sectorEntity03]);

        $this->assertEquals($sectorCollection, $this->marketSectorService->getList());
    }

    #[DataProvider('dataGetNameById')]
    public function testGetNameById(
        string|null $expected,
        int $id,
        MarketSectorEntity|null $marketSectorEntity
    ): void {
        $this->sectorStorage->expects($this->once())->method('findById')
            ->with($id)
            ->willReturn($marketSectorEntity);

        $this->assertEquals($expected, $this->marketSectorService->getNameById($id));
    }

    #[ArrayShape(['Common Case' => "array", 'Not Found Case' => "array"])]
    public static function dataGetNameById(): array
    {
        $id01 = 1;
        $expected01 = 'Technology';
        $expected02 = null;
        $marketSectorEntity01 = new MarketSectorEntity();
        $marketSectorEntity01->setName('Technology');
        $marketSectorEntity02 = null;

        return [
            'Common Case' => [
                $expected01,
                $id01,
                $marketSectorEntity01
            ],
            'Not Found Case' => [
                $expected02,
                $id01,
                $marketSectorEntity02
            ]
        ];
    }
}
