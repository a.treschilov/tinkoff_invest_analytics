<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Models;

use App\Intl\Models\CountryModel;
use Tests\ModelTestCase;

class CountryModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new CountryModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setIso' => 'RU',
                    'setName' => 'Russia',
                    'setContinent' => 'Europe'
                ],
                [
                    'getIso' => 'RU',
                    'getName' => 'Russia',
                    'getContinent' => 'Europe'
                ]
            ]
        ];
    }
}
