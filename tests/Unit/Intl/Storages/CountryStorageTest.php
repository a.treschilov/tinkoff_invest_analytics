<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Storages;

use App\Intl\Entities\CountryEntity;
use App\Intl\Storages\CountryStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CountryStorageTest extends TestCase
{
    private MockObject $entityManager;
    private EntityRepository $repository;
    private CountryStorage $countryStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->countryStorage = new CountryStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $country01 = new CountryEntity();
        $country01->setIso('RU');
        $country02 = new CountryEntity();
        $country02->setIso('CN');

        $expected = [$country01, $country02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CountryEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findBy')
            ->with([], ['name' => 'ASC'])
            ->willReturn([$country01, $country02]);

        $this->assertEquals($expected, $this->countryStorage->findAll());
    }

    public function testFindOneByName()
    {
        $name = 'Russian Federation';

        $country01 = new CountryEntity();
        $country01->setIso('RU');

        $expected = new CountryEntity();
        $expected->setIso('RU');

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CountryEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['name' => $name])
            ->willReturn($country01);

        $this->assertEquals($expected, $this->countryStorage->findOneByName($name));
    }

    public function testFindOneByIso()
    {
        $iso = 'RU';

        $country01 = new CountryEntity();
        $country01->setIso('RU');

        $expected = new CountryEntity();
        $expected->setIso('RU');

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CountryEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['iso' => $iso])
            ->willReturn($country01);

        $this->assertEquals($expected, $this->countryStorage->findOneByIso($iso));
    }
}
