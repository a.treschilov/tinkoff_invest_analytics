<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Storages;

use App\Intl\Entities\CurrencyEntity;
use App\Intl\Storages\CurrencyStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Monolog\Test\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class CurrencyStorageTest extends TestCase
{
    private MockObject $entityManager;
    private EntityRepository $repository;
    private CurrencyStorage $currencyStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->currencyStorage = new CurrencyStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $currency01 = new CurrencyEntity();
        $currency01->setIso('RUB');
        $currency02 = new CurrencyEntity();
        $currency02->setIso('USD');

        $expected = [$currency01, $currency02];

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CurrencyEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findAll')
            ->willReturn([$currency01, $currency02]);

        $this->assertEquals($expected, $this->currencyStorage->findAll());
    }

    public function testFindOneByIso()
    {
        $iso01 = 'RUB';
        $currency01 = new CurrencyEntity();
        $currency01->setId(1);
        $currency01->setIso('RUB');

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(CurrencyEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['iso' => $iso01])
            ->willReturn($currency01);

        $this->assertEquals($currency01, $this->currencyStorage->findOneByIso($iso01));
    }
}
