<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Entities;

use App\Intl\Entities\CountryEntity;
use Tests\ModelTestCase;

class CountryEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        parent::setUp();
        $this->model = new CountryEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setIso' => 'RU',
                    'setName' => 'Russia',
                    'setContinent' => 'Europe'
                ],
                [
                    'getIso' => 'RU',
                    'getName' => 'Russia',
                    'getContinent' => 'Europe'
                ]
            ]
        ];
    }
}
