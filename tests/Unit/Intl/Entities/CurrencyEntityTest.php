<?php

declare(strict_types=1);

namespace Tests\Unit\Intl\Entities;

use App\Intl\Entities\CurrencyEntity;
use Tests\ModelTestCase;

class CurrencyEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new CurrencyEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setIso' => 'USD',
                    'setSymbol' => '$',
                    'setName' => 'Доллар США'
                ],
                [
                    'getId' => 1,
                    'getIso' => 'USD',
                    'getSymbol' => '$',
                    'getName' => 'Доллар США'
                ]
            ]
        ];
    }
}
