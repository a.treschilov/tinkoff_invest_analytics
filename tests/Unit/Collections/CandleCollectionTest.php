<?php

declare(strict_types=1);

namespace Tests\Unit\Collections;

use App\Broker\Models\BrokerCandleModel;
use App\Collections\CandleCollection;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TITestCase;

class CandleCollectionTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param BrokerCandleModel|null $expected
     * @param TICandle[] $candles
     * @return void
     */
    #[DataProvider('dataGetLastCandle')]
    public function testGetLastCandle(?BrokerCandleModel $expected, array $candles): void
    {
        $candleCollection = new CandleCollection($candles);
        $this->assertEquals($expected, $candleCollection->getLastCandle());
    }

    public static function dataGetLastCandle(): array
    {
        $candle01 = TITestCase::createBrokerCandle([
            'time' => new \DateTime('2022-01-01 04:00:00'),
            'close' => 70
        ]);
        $candle02 = TITestCase::createBrokerCandle([
            'time' => new \DateTime('2022-01-03 04:00:00'),
            'close' => 65
        ]);
        $candle03 = TITestCase::createBrokerCandle([
            'time' => new \DateTime('2022-01-02 04:00:00'),
            'close' => 68
        ]);
        $candle04 = TITestCase::createBrokerCandle([
            'time' => new \DateTime('2022-01-03 04:00:00'),
            'close' => 69
        ]);
        $candles01 = [$candle01, $candle02, $candle03, $candle04];
        $candles02 = [];


        return [
            'Common Case' => [
                $candle04,
                $candles01
            ],
            'Empty Collection Case' => [
                null,
                $candles02
            ]
        ];
    }
}
