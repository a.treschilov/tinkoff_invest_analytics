<?php

declare(strict_types=1);

namespace Tests\Unit\Collections;

use App\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use PHPUnit\Framework\TestCase;

class MarketInstrumentCollectionTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFilterByCountry()
    {
        $marketStock01 = new MarketStockEntity();
        $marketStock01->setId(1);
        $marketStock01->setCountryIso('RU');
        $marketStock02 = new MarketStockEntity();
        $marketStock02->setId(2);
        $marketStock02->setCountryIso('US');
        $marketStock03 = new MarketStockEntity();
        $marketStock03->setId(3);
        $marketStock03->setCountryIso('RU');

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setId(1);
        $marketInstrument01->setMarketStock($marketStock01);

        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setId(2);
        $marketInstrument02->setMarketStock($marketStock02);

        $marketInstrument03 = new MarketInstrumentEntity();
        $marketInstrument03->setId(3);
        $marketInstrument03->setMarketStock($marketStock03);

        $marketInstrument04 = new MarketInstrumentEntity();
        $marketInstrument04->setId(4);

        $collection = new MarketInstrumentCollection([
            $marketInstrument01,
            $marketInstrument02,
            $marketInstrument03,
            $marketInstrument04
        ]);

        $expected = new MarketInstrumentCollection([$marketInstrument01, $marketInstrument03]);

        $this->assertEquals($expected, $collection->filterByCountry('RU'));
    }

    public function testFilterBySector()
    {
        $marketStock01 = new MarketStockEntity();
        $marketStock01->setId(1);
        $marketStock01->setSector('Technology');
        $marketStock02 = new MarketStockEntity();
        $marketStock02->setId(2);
        $marketStock02->setSector('Technology');
        $marketStock03 = new MarketStockEntity();
        $marketStock03->setId(3);
        $marketStock03->setSector('Basic Materials');

        $marketInstrument01 = new MarketInstrumentEntity();
        $marketInstrument01->setId(1);
        $marketInstrument01->setMarketStock($marketStock01);

        $marketInstrument02 = new MarketInstrumentEntity();
        $marketInstrument02->setId(2);
        $marketInstrument02->setMarketStock($marketStock02);

        $marketInstrument03 = new MarketInstrumentEntity();
        $marketInstrument03->setId(3);
        $marketInstrument03->setMarketStock($marketStock03);

        $marketInstrument04 = new MarketInstrumentEntity();
        $marketInstrument04->setId(4);

        $collection = new MarketInstrumentCollection([
            $marketInstrument01,
            $marketInstrument02,
            $marketInstrument03,
            $marketInstrument04
        ]);

        $expected = new MarketInstrumentCollection([$marketInstrument01, $marketInstrument02]);

        $this->assertEquals($expected, $collection->filterBySector('Technology'));
    }
}
