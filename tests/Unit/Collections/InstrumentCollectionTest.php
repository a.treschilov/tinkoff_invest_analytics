<?php

declare(strict_types=1);

namespace Tests\Unit\Collections;

use App\Collections\InstrumentCollection;
use App\Models\InstrumentModel;
use Monolog\Test\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TITestCase;

class InstrumentCollectionTest extends TestCase
{
    #[DataProvider('dataFilteredByInstrumentType')]
    public function testFilteredByInstrumentType(
        InstrumentCollection $instrumentCollection,
        InstrumentCollection $expected,
        string $type
    ): void {
        $this->assertEquals($expected, $instrumentCollection->filteredByInstrumentType($type));
    }

    public static function dataFilteredByInstrumentType(): array
    {
        $instrumentO1 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Stock']);
        $instrumentModel01 = new InstrumentModel();
        $instrumentModel01->setInstrument($instrumentO1);
        $instrumentModel01->setAmount(70);
        $instrumentO2 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Bond']);
        $instrumentModel02 = new InstrumentModel();
        $instrumentModel02->setInstrument($instrumentO2);
        $instrumentModel02->setAmount(20);
        $instrumentO3 = TITestCase::createBrokerPortfolioPosition(['instrumentType' => 'Stock']);
        $instrumentModel03 = new InstrumentModel();
        $instrumentModel03->setInstrument($instrumentO3);
        $instrumentModel03->setAmount(10);

        $instrumentArray01 = [$instrumentModel01, $instrumentModel02, $instrumentModel03];
        $instrumentArray02 = [$instrumentModel01, $instrumentModel03];

        $instrumentCollection01 = new InstrumentCollection($instrumentArray01);
        $instrumentCollection02 = new InstrumentCollection($instrumentArray02);
        $instrumentCollection03 = new InstrumentCollection();

        return [
            'Common Case' => [
                $instrumentCollection01,
                $instrumentCollection02,
                'Stock'
            ],
            'Empty Case' => [
                $instrumentCollection01,
                $instrumentCollection03,
                'Gold'
            ]
        ];
    }
}
