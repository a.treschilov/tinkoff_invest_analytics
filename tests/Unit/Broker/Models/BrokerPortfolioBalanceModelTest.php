<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerPortfolioBalanceModel;
use Tests\ModelTestCase;

class BrokerPortfolioBalanceModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new BrokerPortfolioBalanceModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setCurrency' => 'RUB',
                    'setAmount' => 10.1,
                    'setBlockedAmount' => 3.2
                ],
                [
                    'getCurrency' => 'RUB',
                    'getAmount' => 10.1,
                    'getBlockedAmount' => 3.2
                ]
            ]
        ];
    }
}
