<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use Tests\ModelTestCase;

class BrokerPortfolioModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerPortfolioModel();
    }

    public static function dataTestGetSet(): array
    {
        $instrument = new BrokerPortfolioPositionModel([]);
        $position = new PortfolioPositionCollection([$instrument]);
        return [
            'Common Case' => [
                [
                    'setPositions' => $position
                ],
                [
                    'getPositions' => $position
                ]
            ]
        ];
    }
}
