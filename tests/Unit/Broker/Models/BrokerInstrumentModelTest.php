<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerInstrumentModel;
use Tests\ModelTestCase;

class BrokerInstrumentModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerInstrumentModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setMinPriceIncrement' => 0.1,
                    'setLot' => 1,
                    'setCurrency' => 'USD',
                    'setName' => 'Apple',
                    'setType' => 'Stock'
                ],
                [
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => 0.1,
                    'getLot' => 1,
                    'getCurrency' => 'USD',
                    'getName' => 'Apple',
                    'getType' => 'Stock'
                ]
            ],
            'Default Values Case' => [
                [
                    'setTicker' => 'ticker',
                    'setIsin' => 'isin',
                    'setLot' => 1,
                    'setCurrency' => 'USD',
                    'setName' => 'Apple',
                    'setType' => 'Stock'
                ],
                [
                    'getTicker' => 'ticker',
                    'getIsin' => 'isin',
                    'getMinPriceIncrement' => null,
                    'getLot' => 1,
                    'getCurrency' => 'USD',
                    'getName' => 'Apple',
                    'getType' => 'Stock'
                ]
            ]
        ];
    }
}
