<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerMoneyModel;
use Tests\ModelTestCase;

class BrokerMoneyModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerMoneyModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setAmount' => 0.5,
                    'setCurrency' => 'USD'
                ],
                [
                    'getAmount' => 0.5,
                    'getCurrency' => 'USD'
                ]
            ]
        ];
    }
}
