<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerShareModel;
use Tests\ModelTestCase;

class BrokerShareModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerShareModel();
    }

    public static function dataTestGetSet(): array
    {
        $instrument = new BrokerInstrumentModel([
            'figi' => 'figi01'
        ]);
        return [
            'Common Case' => [
                [
                    'setSector' => 'sector_share',
                    'setCountry' => 'country_share',
                    'setIndustry' => 'industry_share',
                    'setInstrument' => $instrument
                ],
                [
                    'getSector' => 'sector_share',
                    'getCountry' => 'country_share',
                    'getIndustry' => 'industry_share',
                    'getInstrument' => $instrument
                ]
            ],
            'Null Case' => [
                [
                    'setInstrument' => $instrument
                ],
                [
                    'getSector' => null,
                    'getCountry' => null,
                    'getIndustry' => null,
                    'getInstrument' => $instrument
                ]
            ]
        ];
    }
}
