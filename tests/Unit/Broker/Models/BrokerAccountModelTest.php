<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Models;

use App\Broker\Models\BrokerAccountModel;
use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use Tests\ModelTestCase;

class BrokerAccountModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new BrokerAccountModel();
    }

    public static function dataTestGetSet(): array
    {
        $openedDate = new \DateTime('2022-01-05 16:39:00');
        $closedDate = new \DateTime('2022-02-06 16:39:00');
        return [
            'Common Case' => [
                [
                    'setId' => '1',
                    'setType' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
                    'setName' => 'Primary Account',
                    'setStatus' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW,
                    'setOpenedDate' => $openedDate,
                    'setClosedDate' => $closedDate
                ],
                [
                    'getId' => '1',
                    'getType' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
                    'getName' => 'Primary Account',
                    'getStatus' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW,
                    'getOpenedDate' => $openedDate,
                    'getClosedDate' => $closedDate
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => '1',
                    'setType' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
                    'setStatus' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW
                ],
                [
                    'getId' => '1',
                    'getType' => BrokerAccountTypeType::ACCOUNT_TYPE_GENERAL,
                    'getName' => null,
                    'getStatus' => BrokerAccountStatusType::ACCOUNT_STATUS_NEW,
                    'getOpenedDate' => null,
                    'getClosedDate' => null
                ]
            ]
        ];
    }
}
