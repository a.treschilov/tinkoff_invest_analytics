<?php

declare(strict_types=1);

namespace Tests\Unit\Broker\Entities;

use App\Broker\Entities\BrokerEntity;
use Tests\ModelTestCase;

class BrokerEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new BrokerEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 'tinkoff',
                    'setName' => 'Тинькофф',
                    'setIsActive' => 1
                ],
                [
                    'getId' => 'tinkoff',
                    'getName' => 'Тинькофф',
                    'getIsActive' => 1
                ]
            ]
        ];
    }
}
