<?php

namespace Tests\Unit\Assets\Entities;

use App\Assets\Entities\AssetsEntity;
use App\Assets\Entities\PayoutTypeEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Item\Entities\ItemEntity;
use Tests\ModelTestCase;

class AssetsEntityTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new AssetsEntity();
    }

    public static function dataTestGetSet(): array
    {
        $item = new ItemEntity();
        $item->setId(101);

        $date = new \DateTime('2022-09-28 22:28:24');

        $payoutType = new PayoutTypeEntity();
        $payoutType->setId(100);

        $currencyEntity = new CurrencyEntity();
        $currencyEntity->setId(3);

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setItemId' => 2,
                    'setItem' => $item,
                    'setExternalId' => 'external id',
                    'setPlatform' => 'other',
                    'setName' => 'Sega',
                    'setType' => 'real_estate',
                    'setPayoutTypeId' => 200,
                    'setPayoutType' => $payoutType,
                    'setDealDate' => $date,
                    'setDuration' => 365,
                    'setDurationPeriod' => 'day',
                    'setInterestPercent' => 15.6,
                    'setInterestAmount' => 15000,
                    'setPayoutFrequency' => 30,
                    'setPayoutFrequencyPeriod' => 'month',
                    'setNominalAmount' => 1000,
                    'setCurrencyId' => 5,
                    'setCurrency' => $currencyEntity
                ],
                [
                    'getId' => 1,
                    'getItemId' => 2,
                    'getItem' => $item,
                    'getExternalId' => 'external id',
                    'getPlatform' => 'other',
                    'getName' => 'Sega',
                    'getType' => 'real_estate',
                    'getPayoutTypeId' => 200,
                    'getPayoutType' => $payoutType,
                    'getDealDate' => $date,
                    'getDuration' => 365,
                    'getDurationPeriod' => 'day',
                    'getInterestPercent' => 15.6,
                    'getInterestAmount' => 15000,
                    'getPayoutFrequency' => 30,
                    'getPayoutFrequencyPeriod' => 'month',
                    'getNominalAmount' => 1000,
                    'getCurrencyId' => 5,
                    'getCurrency' => $currencyEntity
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setItemId' => 2,
                    'setItem' => $item,
                    'setPlatform' => 'other',
                    'setName' => 'Sega',
                    'setType' => 'real_estate'
                ],
                [
                    'getId' => 1,
                    'getItemId' => 2,
                    'getItem' => $item,
                    'getExternalId' => null,
                    'getPlatform' => 'other',
                    'getName' => 'Sega',
                    'getType' => 'real_estate',
                    'getPayoutTypeId' => null,
                    'getPayoutType' => null,
                    'getDealDate' => null,
                    'getDuration' => null,
                    'getDurationPeriod' => null,
                    'getInterestPercent' => null,
                    'getInterestAmount' => null,
                    'getPayoutFrequency' => null,
                    'getPayoutFrequencyPeriod' => null,
                    'getNominalAmount' => null,
                    'getCurrencyId' => null,
                    'getCurrency' => null
                ]
            ]
        ];
    }
}
