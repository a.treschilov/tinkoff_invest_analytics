<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Entities;

use App\Assets\Entities\PayoutTypeEntity;
use Tests\ModelTestCase;

class PayoutTypeEntityTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->model = new PayoutTypeEntity();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setExternalId' => 'differentiated',
                    'setName' => 'Differentiated',
                    'setDescription' => 'Дифференцированный платеж'
                ],
                [
                    'getId' => 1,
                    'getExternalId' => 'differentiated',
                    'getName' => 'Differentiated',
                    'getDescription' => 'Дифференцированный платеж'
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => 1,
                    'setExternalId' => 'differentiated',
                    'setName' => 'Differentiated'
                ],
                [
                    'getId' => 1,
                    'getExternalId' => 'differentiated',
                    'getName' => 'Differentiated',
                    'getDescription' => null
                ]
            ]
        ];
    }
}
