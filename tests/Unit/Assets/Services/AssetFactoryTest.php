<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Services;

use App\Assets\Interfaces\AssetCalculatorInterface;
use App\Assets\Services\AssetFactory;
use App\Assets\Services\Calculators\DifferentiatedScheduleCalculator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class AssetFactoryTest extends TestCase
{
    private AssetFactory $assetFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->assetFactory = new AssetFactory();
    }

    #[DataProvider('dataGetCalculator')]
    public function testGetCalculator(
        AssetCalculatorInterface $expected,
        string $type
    ): void {
        $this->assertEquals($expected, $this->assetFactory->getCalculator($type));
    }

    public static function dataGetCalculator(): array
    {
        return [
            'Crowdfunding Case' => [
                'expected' => new DifferentiatedScheduleCalculator(),
                'type' => 'differentiated'
            ]
        ];
    }
}
