<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Storages;

use App\Assets\Entities\PayoutTypeEntity;
use App\Assets\Storages\PayoutTypeStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class PayoutStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ObjectRepository $repository;
    private PayoutTypeStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->storage = new PayoutTypeStorage($this->entityManager);
    }

    public function testFindByExternalId()
    {
        $externalId = 'differentiated';
        $entity01 = new PayoutTypeEntity();
        $entity01->setId(1);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['externalId' => $externalId])
            ->willReturn($entity01);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(PayoutTypeEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($entity01, $this->storage->findByExternalId($externalId));
    }

    public function testFindById()
    {
        $id = 1;
        $entity01 = new PayoutTypeEntity();
        $entity01->setId(1);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['id' => $id])
            ->willReturn($entity01);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(PayoutTypeEntity::class)
            ->willReturn($this->repository);

        $this->assertEquals($entity01, $this->storage->findById($id));
    }
}
