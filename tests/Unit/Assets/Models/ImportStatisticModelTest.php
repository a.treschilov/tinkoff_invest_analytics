<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\ImportStatisticModel;
use Tests\ModelTestCase;

class ImportStatisticModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ImportStatisticModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setAdded' => 10,
                    'setUpdated' => 15,
                    'setSkipped' => 20
                ],
                [
                    'getAdded' => 10,
                    'getUpdated' => 15,
                    'getSkipped' => 20
                ]
            ],
            'Default Values Case' => [
                [

                ],
                [
                    'getAdded' => 0,
                    'getUpdated' => 0,
                    'getSkipped' => 0
                ]
            ]
        ];
    }
}
