<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\AssetModel;
use Tests\ModelTestCase;

class AssetModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new AssetModel();
    }

    public static function dataTestGetSet(): array
    {
        $dealDate = new \DateTime('2022-10-31 23:46:15');
        return [
            'Common Case' => [
                [
                    'setAssetId' => 1,
                    'setExternalId' => 'external_id_1',
                    'setPlatform' => 'money_friends',
                    'setName' => 'OOO Моторинвест',
                    'setType' => 'crowdfunding',
                    'setDealDate' => $dealDate,
                    'setDuration' => 100,
                    'setPayoutType' => 'annuitant',
                    'setDurationPeriod' => 'month',
                    'setInterestPercent' => 0.25,
                    'setInterestAmount' => 1000,
                    'setPayoutFrequency' => 30,
                    'setPayoutFrequencyPeriod' => 'day',
                    'setNominalAmount' => 1500,
                    'setCurrencyIso' => 'RUB'
                ],
                [
                    'getAssetId' => 1,
                    'getExternalId' => 'external_id_1',
                    'getPlatform' => 'money_friends',
                    'getName' => 'OOO Моторинвест',
                    'getType' => 'crowdfunding',
                    'getDealDate' => $dealDate,
                    'getDuration' => 100,
                    'getPayoutType' => 'annuitant',
                    'getDurationPeriod' => 'month',
                    'getInterestPercent' => 0.25,
                    'getInterestAmount' => 1000,
                    'getPayoutFrequency' => 30,
                    'getPayoutFrequencyPeriod' => 'day',
                    'getNominalAmount' => 1500,
                    'getCurrencyIso' => 'RUB'
                ]
            ],
            'Default Values Case' => [
                [
                    'setPlatform' => 'money_friends',
                    'setName' => 'OOO Моторинвест',
                    'setType' => 'crowdfunding'
                ],
                [
                    'getAssetId' => null,
                    'getExternalId' => null,
                    'getPlatform' => 'money_friends',
                    'getName' => 'OOO Моторинвест',
                    'getType' => 'crowdfunding',
                    'getDealDate' => null,
                    'getDuration' => null,
                    'getPayoutType' => null,
                    'getDurationPeriod' => null,
                    'getInterestPercent' => null,
                    'getInterestAmount' => null,
                    'getPayoutFrequency' => null,
                    'getPayoutFrequencyPeriod' => null,
                    'getNominalAmount' => null,
                    'getCurrencyIso' => null,
                ]
            ]
        ];
    }
}
