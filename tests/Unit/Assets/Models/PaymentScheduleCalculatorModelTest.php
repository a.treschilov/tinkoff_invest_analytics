<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\PaymentScheduleCalculatorModel;
use Tests\ModelTestCase;

class PaymentScheduleCalculatorModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new PaymentScheduleCalculatorModel();
    }

    public static function dataTestGetSet(): array
    {
        $date01 = new \DateTime('2023-01-08 02:37:12');
        return [
            'Common Case' => [
                [
                    'setDate' => $date01,
                    'setInterestPercent' => 0.18,
                    'setInterestAmount' => 10000,
                    'setPayoutFrequency' => 10,
                    'setPayoutFrequencyPeriod' => 'day',
                    'setDuration' => 52,
                    'setDurationPeriod' => 'month',
                    'setNominalAmount' => 1000,
                    'setCurrencyId' => 1
                ],
                [
                    'getDate' => $date01,
                    'getInterestPercent' => 0.18,
                    'getInterestAmount' => 10000,
                    'getPayoutFrequency' => 10,
                    'getPayoutFrequencyPeriod' => 'day',
                    'getDuration' => 52,
                    'getDurationPeriod' => 'month',
                    'getNominalAmount' => 1000,
                    'getCurrencyId' => 1
                ]
            ],
            'Default Values Case' => [
                [
                    'setDate' => $date01,
                    'setPayoutFrequency' => 10,
                    'setPayoutFrequencyPeriod' => 'day',
                    'setDuration' => 52,
                    'setDurationPeriod' => 'month',
                    'setNominalAmount' => 1000,
                    'setCurrencyId' => 1
                ],
                [
                    'getDate' => $date01,
                    'getInterestPercent' => null,
                    'getInterestAmount' => null,
                    'getPayoutFrequency' => 10,
                    'getPayoutFrequencyPeriod' => 'day',
                    'getDuration' => 52,
                    'getDurationPeriod' => 'month',
                    'getNominalAmount' => 1000,
                    'getCurrencyId' => 1
                ]
            ]
        ];
    }
}
