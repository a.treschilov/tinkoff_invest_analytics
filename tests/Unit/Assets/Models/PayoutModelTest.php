<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\PayoutModel;
use Tests\ModelTestCase;

class PayoutModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->model = new PayoutModel();
    }

    public static function dataTestGetSet(): array
    {
        $date = new \DateTime('2022-10-02');
        return [
            'Common Case' => [
                [
                    'setDate' => $date,
                    'setInterest' => 10.54,
                    'setDebtPayout' => 150.32
                ],
                [
                    'getDate' => $date,
                    'getInterest' => 10.54,
                    'getDebtPayout' => 150.32
                ]
            ]
        ];
    }
}
