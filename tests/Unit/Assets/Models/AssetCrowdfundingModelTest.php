<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\AssetCrowdfundingModel;
use Tests\ModelTestCase;

class AssetCrowdfundingModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new AssetCrowdfundingModel();
    }

    public static function dataTestGetSet(): array
    {
        $date = new \DateTime('2022-10-26 10:57:32');
        return [
            'Common Case' => [
                [
                    'setAssetCrowdfundingId' => 1,
                    'setAssetId' => 2,
                    'setPayoutTypeId' => 3,
                    'setDealDate' => $date,
                    'setDuration' => 300,
                    'setDurationPeriod' => 'month',
                    'setAmount' => 400,
                    'setInterest' => 0.25,
                    'setPayoutFrequency' => 30,
                    'setPayoutFrequencyPeriod' => 'day',
                    'setTaxes' => 0.13
                ],
                [
                    'getAssetCrowdfundingId' => 1,
                    'getAssetId' => 2,
                    'getPayoutTypeId' => 3,
                    'getDealDate' => $date,
                    'getDuration' => 300,
                    'getDurationPeriod' => 'month',
                    'getAmount' => 400,
                    'getInterest' => 0.25,
                    'getPayoutFrequency' => 30,
                    'getPayoutFrequencyPeriod' => 'day',
                    'getTaxes' => 0.13
                ]
            ]
        ];
    }
}
