<?php

declare(strict_types=1);

namespace Tests\Unit\Assets\Models;

use App\Assets\Models\AssetModel;
use App\Assets\Models\OperationModel;
use Tests\ModelTestCase;

class OperationModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->model = new OperationModel();
    }

    public static function dataTestGetSet(): array
    {
        $date01 = new \DateTime('2022-12-31 00:01:15');
        $asset01 = new AssetModel(['assetId' => 100]);
        return [
            'Common Case' => [
                [
                    'setExternalId' => 'external_id',
                    'setDate' => $date01,
                    'setAmount' => 100,
                    'setCurrencyIso' => 'RUB',
                    'setOperationType' => 'Input',
                    'setComment' => 'Внесение средства на счет №1500',
                    'setBrokerId' => 'money_friend',
                    'setStatus' => 'Done',
                    'setAsset' => $asset01,
                    'setQuantity' => 20
                ],
                [
                    'getExternalId' => 'external_id',
                    'getDate' => $date01,
                    'getAmount' => 100,
                    'getCurrencyIso' => 'RUB',
                    'getOperationType' => 'Input',
                    'getComment' => 'Внесение средства на счет №1500',
                    'getBrokerId' => 'money_friend',
                    'getStatus' => 'Done',
                    'getAsset' => $asset01,
                    'getQuantity' => 20
                ]
            ],
            'Default Values Case' => [
                [
                    'setExternalId' => 'external_id',
                    'setDate' => $date01,
                    'setAmount' => 100,
                    'setCurrencyIso' => 'RUB',
                    'setOperationType' => 'Input',
                    'setBrokerId' => 'money_friend',
                    'setStatus' => 'Done',
                    'setComment' => 'Внесение средства на счет №1500',
                ],
                [
                    'getExternalId' => 'external_id',
                    'getDate' => $date01,
                    'getAmount' => 100,
                    'getCurrencyIso' => 'RUB',
                    'getOperationType' => 'Input',
                    'getComment' => 'Внесение средства на счет №1500',
                    'getBrokerId' => 'money_friend',
                    'getStatus' => 'Done',
                    'getAsset' => null,
                    'getQuantity' => null
                ]
            ]
        ];
    }
}
