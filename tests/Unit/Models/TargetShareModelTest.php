<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Models\TargetShareModel;
use Tests\ModelTestCase;

class TargetShareModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new TargetShareModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setName' => 'Stock',
                    'setValue' => 0.35,
                    'setExternalId' => 'Stock'
                ],
                [
                    'getName' => 'Stock',
                    'getValue' => 0.35,
                    'getExternalId' => 'Stock'
                ]
            ]
        ];
    }
}
