<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Models\InstrumentModel;
use jamesRUS52\TinkoffInvest\TIPortfolioInstrument;
use Tests\ModelTestCase;

class InstrumentModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new InstrumentModel();
    }

    public static function dataTestGetSet(): array
    {
        $instrument = new BrokerPortfolioPositionModel(
            [
                'figi' => 'figi01',
                'instrumentType' => 'Stock'
            ]
        );
        return [
            'Common Case' => [
                [
                    'setAmount' => 100,
                    'setInstrument' => $instrument,
                ],
                [
                    'getAmount' => 100,
                    'getInstrument' => $instrument,
                ]
            ]
        ];
    }
}
