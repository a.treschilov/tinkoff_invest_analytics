<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Models\OperationFiltersModel;
use Tests\ModelTestCase;

class OperationFilterModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new OperationFiltersModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setOperationType' => 'Buy',
                    'setOperationStatus' => 'Done',
                    'setDateFrom' => new \DateTime('01.01.2021'),
                    'setDateTo' => new \DateTime('01.05.2021')
                ],
                [
                    'getOperationType' => 'Buy',
                    'getOperationStatus' => 'Done',
                    'getDateFrom' => new \DateTime('01.01.2021'),
                    'getDateTo' => new \DateTime('01.05.2021')
                ]
            ],
            'Empty Properties Case' => [
                [],
                [
                    'getOperationType' => null,
                    'getOperationStatus' => null,
                    'getDateFrom' => new \DateTime('2018-01-01'),
                    'getDateTo' => new \DateTime('today')
                ]
            ],
            'String Date Case' => [
                [
                    'setDateFrom' => '02.01.2021',
                    'setDateTo' => '02.05.2021'
                ],
                [
                    'getDateFrom' => new \DateTime('02.01.2021'),
                    'getDateTo' => new \DateTime('02.05.2021')
                ]
            ]
        ];
    }
}
