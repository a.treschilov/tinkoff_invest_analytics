<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Models\ImportStatisticModel;
use Tests\ModelTestCase;

class ImportStatisticModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new ImportStatisticModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setCountImportedItems' => 10,
                    'setCountToImportItems' => 5,
                    'setAdded' => 1,
                    'setUpdated' => 2,
                    'setSkipped' => 3
                ],
                [
                    'getCountImportedItems' => 10,
                    'getCountToImportItems' => 5,
                    'getAdded' => 1,
                    'getUpdated' => 2,
                    'getSkipped' => 3
                ]
            ],
            'Default Values Case' => [
                [
                ],
                [
                    'getCountImportedItems' => 0,
                    'getCountToImportItems' => 0,
                    'getAdded' => 0,
                    'getUpdated' => 0,
                    'getSkipped' => 0
                ]
            ]
        ];
    }
}
