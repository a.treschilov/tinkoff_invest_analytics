<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Broker\Models\BrokerMoneyModel;
use App\Market\Entities\MarketInstrumentEntity;
use App\Models\OperationModel;
use Tests\ModelTestCase;

class OperationModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new OperationModel();
    }

    public static function dataTestGetSet(): array
    {
        $instrument = new MarketInstrumentEntity();
        $instrument->setId(100);
        $payment01 = new BrokerMoneyModel([
            'amount' => 10,
            'currency' => 'USD'
        ]);
        $price01 = new BrokerMoneyModel([
            'amount' => 1,
            'currency' => 'EUR'
        ]);
        $date01 = new \DateTime('2022-02-13 07:05:54');
        return [
            'Common Case' => [
                [
                    'setId' => '1234',
                    'setParentOperationId' => '5678',
                    'setCurrency' => 'RUB',
                    'setPayment' => $payment01,
                    'setPrice' => $price01,
                    'setQuantity' => 5,
                    'setStatus' => 'Done',
                    'setIsin' => 'APPL',
                    'setType' => 'Buy',
                    'setDate' => $date01,
                    'setInstrumentType' => 'Bond',
                    'setInstrument' => $instrument,
                    'setPlatform' => 'tinkoff_invest',
                    'setBroker' => 'tinkoff2'
                ],
                [
                    'getId' => '1234',
                    'getParentOperationId' => '5678',
                    'getCurrency' => 'RUB',
                    'getPayment' => $payment01,
                    'getPrice' => $price01,
                    'getQuantity' => 5,
                    'getStatus' => 'Done',
                    'getIsin' => 'APPL',
                    'getType' => 'Buy',
                    'getDate' => $date01,
                    'getInstrumentType' => 'Bond',
                    'getInstrument' => $instrument,
                    'getPlatform' => 'tinkoff_invest',
                    'getBroker' => 'tinkoff2'
                ]
            ],
            'Default Values Case' => [
                [
                    'setId' => '1234',
                    'setCurrency' => 'RUB',
                    'setPayment' => $payment01,
                    'setStatus' => 'Done',
                    'setType' => 'Buy',
                    'setDate' => $date01,
                    'setPlatform' => 'tinkoff_invest'
                ],
                [
                    'getId' => '1234',
                    'getParentOperationId' => null,
                    'getCurrency' => 'RUB',
                    'getPayment' => $payment01,
                    'getPrice' => null,
                    'getQuantity' => null,
                    'getStatus' => 'Done',
                    'getIsin' => null,
                    'getType' => 'Buy',
                    'getDate' => $date01,
                    'getInstrumentType' => null,
                    'getInstrument' => null,
                    'getPlatform' => 'tinkoff_invest',
                    'getBroker' => null
                ]
            ]
        ];
    }
}
