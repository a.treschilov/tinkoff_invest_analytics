<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Models\PriceModel;
use Tests\ModelTestCase;

class PriceModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new PriceModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setAmount' => 10,
                    'setCurrency' => 'RUB'
                ],
                [
                    'getAmount' => 10,
                    'getCurrency' => 'RUB'
                ]
            ]
        ];
    }
}
