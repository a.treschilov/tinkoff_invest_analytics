<?php

declare(strict_types=1);

namespace Tests\Unit\Models;

use App\Models\OperationAggregateModel;
use Tests\ModelTestCase;

class OperationAggregateModelTest extends ModelTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->model = new OperationAggregateModel();
    }

    public static function dataTestGetSet(): array
    {
        return [
            'Common Case' => [
                [
                    'setDate' => '2022-01-01',
                    'setAmount' => 2044.55
                ],
                [
                    'getDate' => '2022-01-01',
                    'getAmount' => 2044.55
                ]
            ]
        ];
    }
}
