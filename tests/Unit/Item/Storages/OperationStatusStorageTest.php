<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Entities\OperationStatusEntity;
use App\Item\Storages\OperationStatusStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class OperationStatusStorageTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testFindAll()
    {
        $op1 = new OperationStatusEntity();
        $op1->setId(1);
        $op2 = new OperationStatusEntity();
        $op2->setId(2);

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findAll')->willReturn([$op1, $op2]);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(OperationStatusEntity::class)
            ->willReturn($objectRepository);

        $operationStatusStorage = new OperationStatusStorage($entityManager);
        $this->assertEquals([$op1, $op2], $operationStatusStorage->findAll());
    }
}
