<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Entities\OperationTypeEntity;
use App\Item\Storages\OperationTypeStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class OperationTypeStorageTest extends TestCase
{
    private EntityRepository $objectRepository;
    private EntityManager $entityManager;
    private OperationTypeStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->objectRepository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);

        $this->storage = new OperationTypeStorage($this->entityManager);
    }

    public function testFindAll()
    {
        $op1 = new OperationTypeEntity();
        $op1->setId(1);
        $op2 = new OperationTypeEntity();
        $op2->setId(2);

        $objectRepository = $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $objectRepository->expects($this->once())->method('findAll')->willReturn([$op1, $op2]);

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()->onlyMethods(['getRepository'])
            ->getMock();
        $entityManager->expects($this->once())->method('getRepository')
            ->with(OperationTypeEntity::class)
            ->willReturn($objectRepository);

        $operationTypeStorage = new OperationTypeStorage($entityManager);
        $this->assertEquals([$op1, $op2], $operationTypeStorage->findAll());
    }

    public function testFindByExternalId()
    {
        $externalId01 = 'PayIn';
        $operationTypeEntity01 = new OperationTypeEntity();
        $operationTypeEntity01->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(OperationTypeEntity::class)
            ->willReturn($this->objectRepository);

        $this->objectRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['externalId' => $externalId01])
            ->willReturn($operationTypeEntity01);

        $this->assertEquals($operationTypeEntity01, $this->storage->findByExternalId($externalId01));
    }
}
