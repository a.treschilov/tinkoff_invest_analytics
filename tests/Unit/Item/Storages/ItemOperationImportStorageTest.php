<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Item\Entities\ItemOperationsImportEntity;
use App\Item\Storages\BrokerStorage;
use App\Item\Storages\ItemOperationImportStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class ItemOperationImportStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private EntityRepository $repository;
    private ItemOperationImportStorage $storage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->storage = new ItemOperationImportStorage($this->entityManager);
    }

    public function testFindByUserAndBroker()
    {
        $userId = 1;
        $brokerId = 'tinkoff';
        $entity01 = new ItemOperationsImportEntity();
        $entity01->setId(1);

        $this->entityManager->expects($this->once())->method('getRepository')
            ->with(ItemOperationsImportEntity::class)
            ->willReturn($this->repository);

        $this->repository->expects($this->once())->method('findOneBy')
            ->with(['userId' => $userId, 'brokerId' => $brokerId], ['importDateEnd' => 'DESC'])
            ->willReturn($entity01);

        $this->assertEquals($entity01, $this->storage->findByUserAndBroker($userId, $brokerId));
    }

    public function testAddEntity()
    {
        $entity = new ItemOperationsImportEntity();

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->storage->addEntity($entity);
    }
}
