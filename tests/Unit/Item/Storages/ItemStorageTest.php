<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Storages;

use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class ItemStorageTest extends TestCase
{
    private EntityManager $entityManager;
    private ItemStorage $itemStorage;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->itemStorage = new ItemStorage($this->entityManager);
    }

    public function testAddEntity()
    {
        $entity = new ItemEntity();
        $entity->setId(1);

        $this->entityManager->expects($this->once())->method('persist')
            ->with($entity);
        $this->entityManager->expects($this->once())->method('flush');

        $this->assertEquals(1, $this->itemStorage->addEntity($entity));
    }
}
