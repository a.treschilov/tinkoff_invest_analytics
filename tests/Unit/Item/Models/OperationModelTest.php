<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Models;

use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use Tests\ModelTestCase;

class OperationModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->model = new OperationModel();
    }

    public static function dataTestGetSet(): array
    {
        $item = new ItemModel(['itemId' => 100]);
        $createdDate = new \DateTime('2023-01-01 15:52:22');
        $date = new \DateTime('2023-01-01 15:53:51');
        return [
            'Common Case' => [
                [
                    'setItemOperationId' => 1,
                    'setUserId' => 2,
                    'setBrokerId' => 'money_friends',
                    'setItemId' => 3,
                    'setItem' => $item,
                    'setOperationType' => 'Hold',
                    'setExternalId' => 'external_id_1',
                    'setCreatedDate' => $createdDate,
                    'setQuantity' => 10,
                    'setDate' => $date,
                    'setAmount' => 5,
                    'setCurrencyIso' => 'RUB',
                    'setStatus' => 'Done'
                ],
                [
                    'getItemOperationId' => 1,
                    'getUserId' => 2,
                    'getBrokerId' => 'money_friends',
                    'getItemId' => 3,
                    'getItem' => $item,
                    'getOperationType' => 'Hold',
                    'getExternalId' => 'external_id_1',
                    'getCreatedDate' => $createdDate,
                    'getQuantity' => 10,
                    'getDate' => $date,
                    'getAmount' => 5,
                    'getCurrencyIso' => 'RUB',
                    'getStatus' => 'Done'
                ]
            ],
            'Default Values Case' => [
                [
                    'setUserId' => 2,
                    'setBrokerId' => 'money_friends',
                    'setOperationType' => 'Hold',
                    'setExternalId' => 'external_id_1',
                    'setCreatedDate' => $createdDate,
                    'setDate' => $date,
                    'setAmount' => 5,
                    'setCurrencyIso' => 'RUB',
                    'setStatus' => 'Done'
                ],
                [
                    'getItemOperationId' => null,
                    'getUserId' => 2,
                    'getBrokerId' => 'money_friends',
                    'getItemId' => null,
                    'getItem' => null,
                    'getOperationType' => 'Hold',
                    'getExternalId' => 'external_id_1',
                    'getCreatedDate' => $createdDate,
                    'getQuantity' => null,
                    'getDate' => $date,
                    'getAmount' => 5,
                    'getCurrencyIso' => 'RUB',
                    'getStatus' => 'Done'
                ]
            ]
        ];
    }
}
