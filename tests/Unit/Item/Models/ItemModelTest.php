<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Models;

use App\Assets\Models\AssetModel;
use App\Item\Models\ItemModel;
use Tests\ModelTestCase;

class ItemModelTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ItemModel();
    }

    public static function dataTestGetSet(): array
    {
        $asset = new AssetModel([
            'assetId' => 1
        ]);
        return [
            'Common Case' => [
                [
                    'setItemId' => 1,
                    'setType' => 'crowdfunding',
                    'setSource' => 'potok',
                    'setExternalId' => 'item ex id',
                    'setName' => 'Промавтоматика',
                    'setAsset' => $asset
                ],
                [
                    'getItemId' => 1,
                    'getType' => 'crowdfunding',
                    'getSource' => 'potok',
                    'getExternalId' => 'item ex id',
                    'getName' => 'Промавтоматика',
                    'getAsset' => $asset
                ]
            ],
            'Default Values Case' => [
                [
                    'setType' => 'crowdfunding',
                    'setSource' => 'potok',
                    'setExternalId' => 'item ex id',
                    'setName' => 'Промавтоматика',
                ],
                [
                    'getItemId' => null,
                    'getType' => 'crowdfunding',
                    'getSource' => 'potok',
                    'getExternalId' => 'item ex id',
                    'getName' => 'Промавтоматика',
                    'getAsset' => null
                ]
            ]
        ];
    }
}
