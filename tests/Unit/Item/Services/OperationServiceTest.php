<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Services;

use App\Broker\Services\BrokerService;
use App\Common\Amqp\AmqpClient;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Models\OperationModel;
use App\Item\Services\ItemConverterService;
use App\Item\Services\OperationService;
use App\Item\Services\Sources\SourceFactory;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use App\Services\MarketOperationsService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tests\WithConsecutive;

class OperationServiceTest extends TestCase
{
    private ItemOperationStorage $itemOperationStorage;
    private ItemStorage $itemStorage;
    private CurrencyService $currencyService;
    private OperationTypeStorage $operationTypeStorage;
    private OperationStatusStorage $operationStatusStorage;
    private ExchangeCurrencyService $exchangeCurrencyService;
    private OperationService $operationService;
    private ItemConverterService $itemConverterService;
    private AmqpClient $amqpClient;
    private MarketOperationsService $marketOperationsService;
    private BrokerService $brokerService;

    public function setUp(): void
    {
        parent::setUp();

        $this->itemOperationStorage = $this->createMock(ItemOperationStorage::class);
        $this->itemStorage = $this->createMock(ItemStorage::class);
        $this->currencyService = $this->createMock(CurrencyService::class);
        $this->operationTypeStorage = $this->createMock(OperationTypeStorage::class);
        $this->operationStatusStorage = $this->createMock(OperationStatusStorage::class);
        $this->exchangeCurrencyService = $this->createMock(ExchangeCurrencyService::class);
        $this->itemConverterService = $this->createMock(ItemConverterService::class);
        $this->amqpClient = $this->createMock(AmqpClient::class);
        $this->marketOperationsService = $this->createMock(MarketOperationsService::class);
        $this->brokerService = $this->createMock(BrokerService::class);
        $sourceFactory = $this->createMock(SourceFactory::class);

        $this->operationService = new OperationService(
            $this->itemOperationStorage,
            $this->itemStorage,
            $this->currencyService,
            $this->operationTypeStorage,
            $this->operationStatusStorage,
            $this->exchangeCurrencyService,
            $this->itemConverterService,
            $this->amqpClient,
            $this->marketOperationsService,
            $this->brokerService,
            $sourceFactory
        );
    }

    #[DataProvider('dataCalculateOperationSummary')]
    public function testCalculateOperationSummary(
        PriceModel $expected,
        string $currency,
        OperationCollection $operations,
        array $exchange
    ): void {
        $this->exchangeCurrencyService->expects($this->exactly(count($exchange['with'])))
            ->method('getExchangeRate')
            ->with(...WithConsecutive::create(...$exchange['with']))
            ->willReturnOnConsecutiveCalls(...$exchange['return']);
        $this->assertEquals($expected, $this->operationService->calculateOperationSummary($operations, $currency));
    }

    public static function dataCalculateOperationSummary(): array
    {
        $op01 = new OperationModel([
            'id' => '2',
            'amount' => 100,
            'currencyIso' => 'RUB',
            'date' => new \DateTime('2022-04-12 04:43:13')
        ]);
        $op02 = new OperationModel([
            'id' => '3',
            'amount' => 5,
            'currencyIso' => 'USD',
            'date' => new \DateTime('2022-05-04 06:41:23')
        ]);
        $op03 = new OperationModel([
            'id' => '4',
            'amount' => -2,
            'currencyIso' => 'EUR',
            'date' => new \DateTime('2022-02-11 14:11:04')
        ]);

        $currency01 = 'RUB';
        $expected01 = new PriceModel(['amount' => 290, 'currency' => 'RUB']);

        $expected02 = new PriceModel(['amount' => 0, 'currency' => 'RUB']);

        $operationCollection01 = new OperationCollection([$op01, $op02, $op03]);
        $operationCollection02 = new OperationCollection([]);

        $exchangeCalls01 = [
            'with' => [
                ['RUB', 'RUB', new \DateTime('2022-04-12 04:43:13')],
                ['USD', 'RUB', new \DateTime('2022-05-04 06:41:23')],
                ['EUR', 'RUB', new \DateTime('2022-02-11 14:11:04')]
            ],
            'return' => [1.0, 70.0, 80.0]
        ];
        $exchangeCalls02 = [
            'with' => [],
            'return' => []
        ];

        return [
            'Common Case' => [
                $expected01,
                $currency01,
                $operationCollection01,
                $exchangeCalls01
            ],
            'Empty Operations Case' => [
                $expected02,
                $currency01,
                $operationCollection02,
                $exchangeCalls02
            ]
        ];
    }
}
