<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Entities;

use App\Entities\OperationTypeEntity;
use App\Intl\Entities\CurrencyEntity;
use App\Broker\Entities\BrokerEntity;
use App\Item\Entities\ItemEntity;
use App\Item\Entities\ItemOperationEntity;
use Tests\ModelTestCase;

class ItemOperationEntityTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ItemOperationEntity();
    }

    public static function dataTestGetSet(): array
    {
        $item01 = new ItemEntity();
        $item01->setId(11);

        $createdDate01 = new \DateTime('2022-11-11 00:27:15');
        $date01 = new \DateTime('2022-10-16 15:15:12');

        $operationType01 = new OperationTypeEntity();
        $operationType01->setId(12);

        $currency01 = new CurrencyEntity();
        $currency01->setId(13);

        $broker01 = new BrokerEntity();
        $broker01->setId('tinkoff');

        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setBrokerId' => 'tinkoff',
                    'setBroker' => $broker01,
                    'setItemId' => 4,
                    'setItem' => $item01,
                    'setOperationTypeId' => 6,
                    'setOperationType' => $operationType01,
                    'setExternalId' => 'external id tinkoff',
                    'setCreatedDate' => $createdDate01,
                    'setQuantity' => 5.5,
                    'setDate' => $date01,
                    'setAmount' => 99.5,
                    'setCurrencyId' => 5,
                    'setCurrency' => $currency01,
                    'setStatus' => 'Done'
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getBrokerId' => 'tinkoff',
                    'getBroker' => $broker01,
                    'getItemId' => 4,
                    'getItem' => $item01,
                    'getOperationTypeId' => 6,
                    'getOperationType' => $operationType01,
                    'getExternalId' => 'external id tinkoff',
                    'getCreatedDate' => $createdDate01,
                    'getQuantity' => 5.5,
                    'getDate' => $date01,
                    'getAmount' => 99.5,
                    'getCurrencyId' => 5,
                    'getCurrency' => $currency01,
                    'getStatus' => 'Done'
                ]
            ],
            'Default Values Case' => [
                [
                    'setUserId' => 2,
                    'setOperationTypeId' => 6,
                    'setOperationType' => $operationType01,
                    'setExternalId' => 'external id tinkoff',
                    'setCreatedDate' => $createdDate01,
                    'setDate' => $date01,
                    'setStatus' => 'Done'
                ],
                [
                    'getId' => null,
                    'getUserId' => 2,
                    'getBrokerId' => null,
                    'getBroker' => null,
                    'getItemId' => null,
                    'getItem' => null,
                    'getOperationTypeId' => 6,
                    'getOperationType' => $operationType01,
                    'getExternalId' => 'external id tinkoff',
                    'getCreatedDate' => $createdDate01,
                    'getQuantity' => null,
                    'getDate' => $date01,
                    'getAmount' => null,
                    'getCurrencyId' => null,
                    'getCurrency' => null,
                    'getStatus' => 'Done'
                ]
            ]
        ];
    }
}
