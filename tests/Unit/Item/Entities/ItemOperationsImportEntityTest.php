<?php

declare(strict_types=1);

namespace Tests\Unit\Item\Entities;

use App\Item\Entities\ItemOperationsImportEntity;
use Tests\ModelTestCase;

class ItemOperationsImportEntityTest extends ModelTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->model = new ItemOperationsImportEntity();
    }

    public static function dataTestGetSet(): array
    {
        $start = new \DateTime('2022-11-01 00:00:00');
        $end = new \DateTime('2022-11-15 00:00:00');
        $current = new \DateTime('2022-11-16 14:22:21');
        return [
            'Common Case' => [
                [
                    'setId' => 1,
                    'setUserId' => 2,
                    'setUserBrokerAccountId' => 3,
                    'setImportDateStart' => $start,
                    'setImportDateEnd' => $end,
                    'setCreatedDate' => $current,
                    'setAdded' => 10,
                    'setUpdated' => 12,
                    'setSkipped' => 15
                ],
                [
                    'getId' => 1,
                    'getUserId' => 2,
                    'getUserBrokerAccountId' => 3,
                    'getImportDateStart' => $start,
                    'getImportDateEnd' => $end,
                    'getCreatedDate' => $current,
                    'getAdded' => 10,
                    'getUpdated' => 12,
                    'getSkipped' => 15
                ]
            ]
        ];
    }
}
