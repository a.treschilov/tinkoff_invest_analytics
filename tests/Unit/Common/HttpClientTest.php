<?php

declare(strict_types=1);

namespace Tests\Unit\Common;

use App\Common\HttpClient;
use GuzzleHttp\Client;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class HttpClientTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    #[DataProvider('dataDoRequest')]
    public function testDoRequest($method, $url, $body, $header, $options, $expect): void
    {
        $client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $httpClient = new HttpClient($client);

        $client->expects($this->once())->method('request')->with($method, $url, $expect);

        $httpClient->doRequest($method, $url, $body, $header, $options);
    }

    public static function dataDoRequest(): array
    {
        $url01 = 'https://yandex.ru';
        $method01 = 'GET';
        $body01 = [
            'param1' => 'value1'
        ];
        $body02 = [];
        $header01 = [
            'headerParam1' => 'headerValue1',
            'Content-Type' => 'application/text'
        ];
        $header02 = [];
        $expectHeader01 = [
            'headerParam1' => 'headerValue1',
            'Content-Type' => 'application/text'
        ];
        $expectHeader02 = [
            'Content-Type' => 'application/json'
        ];
        $options01 = [
          'timeout' => 2,
          'http_errors' => true
        ];
        $options02 = [];
        $expect01 = [
            'query' => $body01,
            'headers' => $expectHeader01,
            'timeout' => 2,
            'http_errors' => true
        ];
        $expect02 = [
            'query' => $body02,
            'headers' => $expectHeader02,
            'timeout' => 15,
            'http_errors' => false
        ];

        return [
            'Common Case' => [
                $method01, $url01, $body01, $header01, $options01, $expect01
            ],
            'Default Values Case' => [
                $method01, $url01, $body02, $header02, $options02, $expect02
            ]
        ];
    }
}
