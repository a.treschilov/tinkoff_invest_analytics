<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Broker\Services\BrokerService;
use App\Collections\OperationAggregateCollection;
use App\Item\Collections\OperationCollection;
use App\Controllers\OperationController;
use App\Market\Entities\MarketInstrumentEntity;
use App\Exceptions\InvalidRequestDataException;
use App\Item\Models\ItemModel;
use App\Item\Services\OperationService;
use App\Models\OperationAggregateModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Models\PriceModel;
use App\Services\JsonValidatorService;
use App\User\Services\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class OperationControllerTest extends TestCase
{
    private OperationService $operationService;
    private BrokerService $brokerService;
    private UserService $userService;
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private OperationController $controller;
    private JsonValidatorService $jsonValidatorService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->createMock(UserService::class);
        $this->operationService = $this->createMock(OperationService::class);
        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->brokerService = $this->createMock(BrokerService::class);
        $this->jsonValidatorService = $this->createMock(JsonValidatorService::class);
        $this->controller = new OperationController(
            $this->jsonValidatorService,
            $this->userService,
            $this->operationService,
            $this->brokerService
        );
    }

    #[DataProvider('dataGetPayIn')]
    public function testGetPayIn(
        ?string $dateFrom,
        ?string $dateTo,
        int $statusCode,
        ?array $exception,
        string $responseJson,
        OperationAggregateCollection $collection,
        ?string $aggregatePeriod
    ): void {
        $userId01 = 1;

        $this->request->expects($this->once())->method('getQueryParams')
            ->willReturn(['dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'aggregatePeriod' => $aggregatePeriod]);

        if (null !== $exception) {
            $this->expectException($exception['className']);
            $this->expectExceptionCode($exception['code']);
        } else {
            $this->userService->expects($this->once())->method('getUserId')
                ->willReturn($userId01);
            $this->operationService->expects($this->once())->method('getPayIn')
                ->with($userId01, new \DateTime($dateFrom), new \DateTime($dateTo), $aggregatePeriod)
                ->willReturn($collection);

            $this->response->expects($this->once())->method('withHeader')
                ->with('Content-Type', 'application/json')->willReturn($this->response);
            $this->response->expects($this->once())->method('withStatus')->with($statusCode)
                ->willReturn($this->response);
            $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
            $this->stream->expects($this->once())->method('write')->with($responseJson);
        }

        $this->controller->getPayIn($this->request, $this->response, []);
    }

    public static function dataGetPayIn(): array
    {
        $aggregatePeriod01 = 'month';
        $aggregatePeriod02 = null;
        $dateFrom01 = '2022-03-21';
        $dateFrom02 = null;
        $dateTo01 = '2022-05-12 23:59:59';
        $dateTo02 = null;
        $statusCode01 = 200;

        $responseJson01 = json_encode([
            'summary' => 1340,
            'data' => [
                [
                    "date" => "2022-05-01",
                    "amount" => 350
                ],
                [
                    "date" => "2022-06-01",
                    "amount" => 990
                ]
            ]
        ]);

        $aggregateModel01 = new OperationAggregateModel(['date' => '2022-05-01', 'amount' => 350]);
        $aggregateModel02 = new OperationAggregateModel(['date' => '2022-06-01', 'amount' => 990]);
        $collection = new OperationAggregateCollection([$aggregateModel01, $aggregateModel02]);

        $exception01 = null;
        $exception02 = [
            'className' => InvalidRequestDataException::class,
            'code' => 1080
        ];

        return [
            'Common Case' => [
                $dateFrom01,
                $dateTo01,
                $statusCode01,
                $exception01,
                $responseJson01,
                $collection,
                $aggregatePeriod01
            ],
            'Date From empty Case' => [
                $dateFrom02,
                $dateTo01,
                $statusCode01,
                $exception02,
                $responseJson01,
                $collection,
                $aggregatePeriod01
            ],
            'Date To empty Case' => [
                $dateFrom01,
                $dateTo02,
                $statusCode01,
                $exception02,
                $responseJson01,
                $collection,
                $aggregatePeriod01
            ],
            'Aggregate Period Empty Case' => [
                $dateFrom01,
                $dateTo01,
                $statusCode01,
                $exception02,
                $responseJson01,
                $collection,
                $aggregatePeriod02
            ]
        ];
    }

    #[DataProvider('dataGetPayIn')]
    public function testGetEarning(
        ?string $dateFrom,
        ?string $dateTo,
        int $statusCode,
        ?array $exception,
        string $responseJson,
        OperationAggregateCollection $collection,
        ?string $aggregatePeriod
    ) {
        $user01 = 1;
        $this->request->expects($this->once())->method('getQueryParams')
            ->willReturn(['dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'aggregatePeriod' => $aggregatePeriod]);

        if (null !== $exception) {
            $this->expectException($exception['className']);
            $this->expectExceptionCode($exception['code']);
        } else {
            $this->userService->expects($this->once())->method('getUserId')
                ->willReturn($user01);
            $this->operationService->expects($this->once())->method('getEarning')
                ->with($user01, new \DateTime($dateFrom), new \DateTime($dateTo), $aggregatePeriod)
                ->willReturn($collection);

            $this->response->expects($this->once())->method('withHeader')
                ->with('Content-Type', 'application/json')->willReturn($this->response);
            $this->response->expects($this->once())->method('withStatus')->with($statusCode)
                ->willReturn($this->response);
            $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
            $this->stream->expects($this->once())->method('write')->with($responseJson);
        }

        $this->controller->getEarning($this->request, $this->response, []);
    }

    public function testApiOperation(): void
    {
        $userId = 1;
        $queryParams = [
            'dateFrom' => '2022-01-12',
            'dateTo' => '2022-05-21',
            'operationStatus' => 'Done',
        ];
        $statusCode = 200;

        $operationFilterModel = new OperationFiltersModel([
            'dateFrom' => '2022-01-12',
            'dateTo' => '2022-05-21 23:59:59',
            'operationStatus' => ['Done']
        ]);
        $operationsArray = [
            [
                "id" => 243,
                "externalId" => 'externalId243',
                "type" => "Buy",
                "itemId" => 10,
                "itemName" => "FinEx Золото",
                "itemType" => "market",
                "itemLogo" => "https://cdn.hakkes.com/tinkoff.png",
                "itemExternalId" => "RU12345",
                "broker" => 'tinkoff',
                "amount" => -1830.8,
                "quantity" => 2,
                "currency" => "RUB",
                "date" => [
                    "date" => "2021-06-10 07:45:45.000000",
                    "timezone_type" => 3,
                    "timezone" => "UTC"
                ],
                "status" => "Done"
            ]
        ];
        $operationTypeArray = [
            [
                "id" => 'PayIn',
                "name" => "Покупка"
            ],
            [
                "id" => 'PayOut',
                "name" => "Продажа"
            ]
        ];
        $operationStatusArray = [
            [
                "id" => 'Done',
                "name" => "Выполнена"
            ],
            [
                "id" => 'Decline',
                "name" => "Отклонена"
            ]
        ];
        $brokersArray = [
            [
                'id' => 'potok',
                'name' => 'Potok Digital'
            ],
            [
                'id' => 'tinkoff2',
                'name' => 'Tinkoff V2'
            ]
        ];
        $instrumentEntity = new MarketInstrumentEntity();
        $instrumentEntity->setName("FinEx Золото");
        $instrumentEntity->setType('Etf');
        $operationModel = new OperationModel([
            "itemOperationId" => 243,
            "externalId" => 'externalId243',
            "brokerId" => 'tinkoff',
            "userId" => 1,
            "itemId" => 10,
            "operationType" => "Buy",
            "item" => new ItemModel([
                'name' => "FinEx Золото",
                'type' => "market",
                'externalId' => 'RU12345',
                "logo" => "https://cdn.hakkes.com/tinkoff.png",
            ]),
            "amount" => -1830.8,
            "quantity" => 2,
            "currencyIso" => "RUB",
            "date" => new \DateTime("2021-06-10 07:45:45.000000"),
            "status" => "Done",
            "instrument" => $instrumentEntity
        ]);
        $responseJson = json_encode([
            'dateFrom' => $queryParams['dateFrom'],
            'dateTo' => $queryParams['dateTo'],
            'filters' => [
                'dateFrom' => [
                    "date" => "2022-01-12 00:00:00.000000",
                    "timezone_type" => 3,
                    "timezone" => "UTC"
                ],
                'dateTo' => [
                    "date" => "2022-05-21 23:59:59.000000",
                    "timezone_type" => 3,
                    "timezone" => "UTC"
                ],
                'operationStatus' => ['Done'],
                'operationType' => null,
                'brokerId' => null
            ],
            'operations' => $operationsArray,
            'summary' => ['amount' => -1830.8, 'currency' => 'RUB'],
        ]);
        $operationCollection = new OperationCollection([$operationModel]);

        $this->userService->expects($this->once())->method('getUserId')
            ->willReturn($userId);
        $this->operationService->expects($this->once())->method('getFilteredUserOperation')
            ->with($userId, $operationFilterModel)
            ->willReturn($operationCollection);
        $this->operationService->expects($this->once())->method('prepareAsSafetyOperations')
            ->with($operationCollection)
            ->willReturn($operationCollection);
        $this->operationService->expects($this->once())->method('calculateOperationSummary')
            ->with($operationCollection, 'RUB')
            ->willReturn(new PriceModel(['amount' => -1830.8, 'currency' => 'RUB']));

        $this->request->expects($this->once())->method('getQueryParams')
            ->willReturn($queryParams);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($statusCode)
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')->with($responseJson);

        $this->controller->apiOperations($this->request, $this->response, []);
    }
}
