<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Collections\ShareCollection;
use App\Controllers\UserStrategyShareController;
use App\Exceptions\JsonSchemeException;
use App\Models\TargetShareModel;
use App\Services\JsonValidatorService;
use App\Services\StrategyService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class UserStrategyShareControllerTest extends TestCase
{
    private MockObject $request;
    private MockObject $response;
    private MockObject $stream;
    private MockObject $strategyService;
    private MockObject $jsonValidatorService;
    private UserStrategyShareController $controller;

    public function setUp(): void
    {
        parent::setUp();

        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->jsonValidatorService = $this->createMock(JsonValidatorService::class);
        $this->strategyService = $this->createMock(StrategyService::class);

        $this->controller = new UserStrategyShareController(
            $this->jsonValidatorService,
            $this->strategyService
        );
    }

    public function testGetTargetShare()
    {
        $args = ['type' => 'Stock'];

        $shareCollection01 = new ShareCollection([
            new TargetShareModel([
                'name' => 'Stock',
                'value' => 0.1,
                'externalId' => 'Stock'
            ]),
            new TargetShareModel([
                'name' => 'Etf',
                'value' => 0.3,
                'externalId' => 'Etf'
            ]),
            new TargetShareModel([
                'name' => 'Bond',
                'value' => 0.6,
                'externalId' => 'Bond'
            ])
        ]);
        $responseJson = json_encode([
            [
                'name' => 'Stock',
                'value' => 0.1,
                'externalId' => 'Stock'
            ],
            [
                'name' => 'Etf',
                'value' => 0.3,
                'externalId' => 'Etf'
            ],
            [
                'name' => 'Bond',
                'value' => 0.6,
                'externalId' => 'Bond'
            ],
        ]);

        $this->strategyService->expects($this->once())->method('getTargetShare')
            ->with('Stock')->willReturn($shareCollection01);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->controller->getTargetShare($this->request, $this->response, $args);
    }

    #[DataProvider('dataPostTargetShare')]
    public function testPostInstrumentShare(
        string $type,
        array $body,
        array $args,
        ShareCollection $shares,
        ?array $exception = null
    ): void {
        $this->request->expects($this->once())->method('getParsedBody')
            ->willReturn($body);


        if ($exception === null) {
            $this->jsonValidatorService->expects($this->once())->method('validate')
                ->with('user_strategy_share_post.json', $body);

            $this->strategyService->expects($this->once())->method('updateTargetShare')
                ->with($type, $shares);

            $responseStatus = 204;
            $responseBody = '';
        } else {
            $this->jsonValidatorService->expects($this->once())->method('validate')
                ->with('user_strategy_share_post.json', $body)
                ->willThrowException(new JsonSchemeException(
                    'Additional object properties are not allowed: name',
                    1050
                ));
            $responseStatus = 422;
            $responseBody = [
                "error_code" => 1050,
                "error_message" => "Additional object properties are not allowed: name"
            ];
        }



        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($responseStatus)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with(json_encode($responseBody));

        $this->controller->postTargetShare($this->request, $this->response, $args);
    }

    public static function dataPostTargetShare(): array
    {
        $type01 = 'stock';
        $body01 = [
            ['name' => 'Stock', 'value' => 0.35],
            ['name' => 'Etf', 'value' => 0.1]
        ];
        $args01 = ['type' => $type01];
        $shares01 = new ShareCollection([
            new TargetShareModel(['name' => 'Stock', 'value' => 0.35]),
            new TargetShareModel(['name' => 'Etf', 'value' => 0.1])
        ]);
        $exception01 = null;
        $exception02 = [
            'className' => JsonSchemeException::class,
            'errorCode' => 1050
        ];

        return [
            'Common Case' => [
                $type01,
                $body01,
                $args01,
                $shares01,
                $exception01
            ],
            'Validation Request Data Case' => [
                $type01,
                $body01,
                $args01,
                $shares01,
                $exception02
            ]
        ];
    }
}
