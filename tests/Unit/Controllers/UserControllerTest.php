<?php

namespace Tests\Unit\Controllers;

use App\Controllers\UserController;
use App\Models\PriceModel;
use App\Services\JsonValidatorService;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Models\UserModel;
use App\User\Models\UserSettingsModel;
use App\User\Services\UserAccountService;
use App\User\Services\UserCodeService;
use App\User\Services\UserService;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Validator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class UserControllerTest extends TestCase
{
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private UserService $userService;
    private UserCodeService $userCodeService;
    private JsonValidatorService $jsonValidatorService;
    private UserAccountService $userAccountService;
    private UserController $controller;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->createMock(UserService::class);

        $validator = new Validator();
        $errorFormatter = new ErrorFormatter();
        $this->jsonValidatorService = new JsonValidatorService(
            $validator,
            $errorFormatter
        );
        $this->userCodeService = $this->createMock(UserCodeService::class);
        $this->userAccountService = $this->createMock(UserAccountService::class);

        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);

        $this->controller = new UserController(
            $this->jsonValidatorService,
            $this->userService,
            $this->userCodeService,
            $this->userAccountService
        );
    }

    #[DataProvider('dataGetUserSettings')]
    public function testGetUserSettings(
        ?UserSettingsModel $userSettingsEntity,
        array $response
    ): void {
        $userId = 1;
        $userEntity = new UserEntity();
        $userEntity->setId($userId);

        $responseJson = json_encode($response);

        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($userEntity);
        $this->userService->expects($this->once())->method('getUserSettings')
            ->with($userId)
            ->willReturn($userSettingsEntity);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->controller->getSettings($this->request, $this->response);
    }

    public static function dataGetUserSettings(): array
    {
        $userSettings01 = new UserSettingsModel([
            'desiredPension' => new PriceModel([
                'amount' => 500,
                'currency' => 'RUB'
            ]),
            'userSettingsId' => 1,
            'user' => new UserModel([
                'userId' => 1,
                'birthday' => new \DateTime("1987-03-09 00:00:00")
            ]),
            'userId' => 1,
            'retirementAge' => 50,
            'expenses' => new PriceModel([
                'amount' => 100,
                'currency' => 'USD'
            ])
        ]);

        $userSettings02 = null;

        $response01 = [
            'userSettingsId' => 1,
            'desiredPension' => [
                'amount' => 500,
                'currency' => 'RUB'
            ],
            'retirementAge' => 50,
            'expenses' => [
                'amount' => 100,
                'currency' => 'USD'
            ],
            'birthday' => [
                'date' => "1987-03-09 00:00:00.000000",
                'timezone_type' => 3,
                'timezone' => "UTC"
            ]
        ];
        $response02 = [
            'userSettingsId' => null,
            'desiredPension' => [
                'amount' => null,
                'currency' => null
            ],
            'retirementAge' => null,
            'expenses' => [
                'amount' => null,
                'currency' => null
            ],
            'birthday' => null
        ];

        return [
            'Common Case' => [
                $userSettings01,
                $response01
            ],
            'Empty Settings Case' => [
                $userSettings02,
                $response02
            ]
        ];
    }

    #[DataProvider('dataSetUserSettings')]
    public function testSetUserSettings(
        array $body,
        UserSettingsModel $userSettingsModel,
        int $statusCode,
        string $responseJson,
        bool $isException
    ): void {
        $user = new UserEntity();
        $user->setId(2);

        $userSettingsEntity = new UserSettingsEntity();
        $userSettingsEntity->setId(3);

        $this->request->expects($this->once())->method('getParsedBody')->willReturn($body);

        if ($isException === false) {
            $this->userService->expects($this->once())->method('getUser')->willReturn($user);
            $this->userService->expects($this->once())->method('setUserSettings')->with($userSettingsModel);
            $this->userService->expects($this->once())->method('getUserSettings')->with(2)
                ->willReturn($userSettingsModel);
        }

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($statusCode)
            ->willReturn($this->response);

        $this->controller->setSettings($this->request, $this->response);
    }

    public static function dataSetUserSettings(): array
    {
        $body01 = [
            'desiredPension' => [
                'currency' => 'USD',
                'amount' => 100
            ],
            'retirementAge' => 50,
            'userSettingsId' => 1,
            'expenses' => [
                'currency' => 'RUB',
                'amount' => 100000
            ],
            'birthday' => [
                'date' => '1987-03-09',
                'timezone' => 'UTC'
            ]
        ];
        $userSettingsModel01 = new UserSettingsModel([
            'desiredPension' => new PriceModel([
                'currency' => 'USD',
                'amount' => 100
            ]),
            'retirementAge' => 50,
            'userSettingsId' => 1,
            'currencyIso' => 'RUB',
            'expenses' => new PriceModel([
                'currency' => 'RUB',
                'amount' => 100000
            ]),
            'birthday' => new \DateTime('1987-03-09')
        ]);

        $statusCode01 = 200;
        $responseJson01 = json_encode([
            'userSettingsId' => 1,
        ]);
        $isException01 = false;

        $body02 = [
            'userSettingsId' => 1
        ];
        $statusCode02 = 422;
        $responseJson02 = json_encode([
            'error_code' => 1050,
            'error_message' => 'The required properties (desiredPension) are missing'
        ]);
        $isException02 = true;

        return [
            'Common Case' => [
                $body01,
                $userSettingsModel01,
                $statusCode01,
                $responseJson01,
                $isException01
            ],
            'Invalid Request Params' => [
                $body02,
                $userSettingsModel01,
                $statusCode02,
                $responseJson02,
                $isException02
            ]
        ];
    }

    public function testData()
    {
        $userId = 1;
        $login = 'IvanIvanov';
        $isNewUser = 0;
        $language = 'ru';

        $response = [
            'user_id' => $userId,
            'login' => $login,
            'name' => null,
            'language' => $language,
            'isNewUser' => $isNewUser
        ];
        $responseJson = json_encode($response);

        $user = new UserEntity();
        $user->setId($userId);
        $user->setLogin($login);
        $user->setIsNewUser($isNewUser);
        $user->setLanguage($language);

        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($user);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->controller->data($this->request, $this->response);
    }
}
