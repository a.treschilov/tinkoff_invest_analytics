<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Controllers\AnalyticsController;
use App\Models\PriceModel;
use App\Services\AnalyticsRealTimeService;
use App\Services\AnalyticsService;
use App\Services\WealthService;
use App\User\Services\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class AnalyticsControllerTest extends TestCase
{
    private AnalyticsService $analyticsService;
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private WealthService $wealthService;
    private UserService $userService;
    private AnalyticsRealTimeService $analyticsRealTimeService;

    private AnalyticsController $analyticsController;

    public function setUp(): void
    {
        parent::setUp();

        $this->analyticsService = $this->createMock(AnalyticsService::class);
        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->wealthService = $this->createMock(WealthService::class);
        $this->userService = $this->createMock(UserService::class);
        $this->analyticsRealTimeService = $this->createMock(AnalyticsRealTimeService::class);

        $this->analyticsController = new AnalyticsController(
            $this->analyticsService,
            $this->analyticsRealTimeService,
            $this->wealthService,
            $this->userService
        );
    }

    #[DataProvider('dataSummaryToday')]
    public function testSummaryToday(
        array $queryParams,
        ?string $currency,
        array $calculatedRevenue,
        string $responseJson
    ): void {
        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->request->expects($this->once())->method('getQueryParams')->willReturn($queryParams);

        $this->analyticsRealTimeService->expects($this->once())->method('calculateProfitToday')
            ->with($currency)->willReturn($calculatedRevenue);

        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->analyticsController->summaryToday($this->request, $this->response, []);
    }

    public static function dataSummaryToday(): array
    {
        $currency01 = 'RUB';
        $currency02 = null;

        $queryParams01 = [
            'currency' => 'RUB'
        ];
        $queryParams02 = [];

        $priceModel01 = new PriceModel(['amount' => 10, 'currency' => 'USD']);
        $calculatedRevenue01 = [
            'revenueToday' => $priceModel01
        ];
        $responseJson01 = json_encode([
            'revenueToday' => [
                'amount' => 10,
                'currency' => 'USD'
            ]
        ]);

        return [
            'Common Case' => [
                $queryParams01,
                $currency01,
                $calculatedRevenue01,
                $responseJson01
            ],
            'Default Currency Case' => [
                $queryParams02,
                $currency02,
                $calculatedRevenue01,
                $responseJson01
            ]
        ];
    }

    public function testDashboard()
    {
        $response = [
            'targetCapital' => 1000,
            "actives" => null,
            "passives" => null,
            "shouldIncrease" => null,
            "saveSpeed" => null,
            "pension" => null,
            "futurePension" => null,
            "capital" => null,
            "predictInterest" => null,
            "lastYearInterest" => null,
            "wealthRate" => null,
            "safeRatio" => null,
            'capitalBefore' => [
                'day' => null,
                'month' => null,
                'year' => null
            ]
        ];
        $responseJson = json_encode($response);

        $this->analyticsService->expects($this->once())->method('getSummary')
            ->willReturn($response);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->analyticsController->dashboard($this->request, $this->response);
    }
}
