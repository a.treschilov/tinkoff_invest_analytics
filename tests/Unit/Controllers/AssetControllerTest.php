<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Assets\Models\AssetModel;
use App\Controllers\AssetController;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Models\PortfolioModel;
use App\Item\Services\ItemService;
use App\Item\Services\PortfolioService;
use App\Services\JsonValidatorService;
use App\User\Entities\UserEntity;
use App\User\Services\UserService;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class AssetControllerTest extends TestCase
{
    private PortfolioService $portfolioService;
    private ItemService $itemService;
    private UserService $userService;
    private AssetController $controller;
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private \DateTime $dateTime;

    protected function setUp(): void
    {
        parent::setUp();

        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);

        $this->userService = $this->createMock(UserService::class);
        $this->portfolioService = $this->createMock(PortfolioService::class);
        $this->itemService =  $this->createMock(ItemService::class);
        $this->dateTime = new \DateTime('2023-02-24 14:12:03');

        $this->controller = new AssetController(
            $this->dateTime,
            $this->userService,
            $this->portfolioService,
            $this->itemService
        );
    }

    public function testGetAssetTypedList()
    {
        $type01 = 'crowdfunding';
        $userId01 = 1;
        $userEntity01 = new UserEntity();
        $userEntity01->setId($userId01);

        $asset01 = new AssetModel([
            "assetId" => 110,
            "name" => "1636 - ООО ПРОМАВТОМАТИКА",
            "nominalAmount" => 1,
            "duration" => 273,
            "durationPeriod" => "day",
            "interestPercent" => 0.26,
            "dealDate" => new \DateTime('2022-08-04'),
            "payoutFrequency" => 1,
            "payoutFrequencyPeriod" => "month"
        ]);
        $item01 = new ItemModel([
            'itemId' => 1,
            'asset' => $asset01,
            'name' => "1636 - ООО ПРОМАВТОМАТИКА",
            "logo" => "https://cdn.hakkes.com/tinkoff.png",
            'type' => 'crowdfunding'
        ]);
        $portfolioItem01 = new PortfolioItemModel([
            'itemId' => 1,
            'quantity' => 100,
            'amount' => 10,
            'currencyIso' => 'RUB',
            'item' => $item01
        ]);
        $balance01 = new PortfolioBalanceCollection();
        $portfolioItems01 = new PortfolioItemCollection([$portfolioItem01]);
        $portfolio01 = new PortfolioModel([
            'balance' => $balance01,
            'items' => $portfolioItems01
        ]);

        $items01 = new ItemCollection([$item01]);

        $response = [
            'items' => [
                [
                    "assetId" => 110,
                    "itemId" => 1,
                    "name" => "1636 - ООО ПРОМАВТОМАТИКА",
                    "amount" => 1,
                    "duration" => 273,
                    "durationPeriod" => "day",
                    "interestPercent" => 0.26,
                    "dealDate" => new \DateTime('2022-08-04'),
                    "payoutFrequency" => 1,
                    "payoutFrequencyPeriod" => "month"
                ]
            ],
            'portfolio' => [
                [
                    'itemId' => 1,
                    'quantity' => 100,
                    'amount' => 10,
                    'currencyIso' => 'RUB',
                    "item" => [
                        "itemId" => 1,
                        "externalId" => null,
                        "type" => "crowdfunding",
                        "name" => "1636 - ООО ПРОМАВТОМАТИКА",
                        "logo" => "https://cdn.hakkes.com/tinkoff.png",
                        "isOutdated" => false,
                        "stock" => null,
                        "loan" => null,
                        "deposit" => null,
                        "realEstate" => null
                    ]
                ]
            ],
        ];

        $responseJson = json_encode($response);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->response);
        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->response->expects($this->once())->method('withStatus')
            ->with(200)
            ->willReturn($this->response);

        $this->userService->expects($this->once())->method('getUser')
            ->willReturn($userEntity01);
        $this->portfolioService->expects($this->once())->method('buildPortfolio')
            ->with($userId01, $this->dateTime)
            ->willReturn($portfolio01);
        $this->itemService->expects($this->once())->method('getPortfolioItemsByType')
            ->with($portfolioItems01, $type01)
            ->willReturn($items01);

        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->controller->getAssetTypedList($this->request, $this->response, ['type' => 'crowdfunding']);
    }
}
