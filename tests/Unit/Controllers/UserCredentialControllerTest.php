<?php

declare(strict_types=1);

namespace Tests\Unit\Controllers;

use App\Common\BaseException;
use App\Controllers\UserCredentialController;
use App\Broker\Entities\BrokerEntity;
use App\Broker\Models\BrokerModel;
use App\Services\JsonValidatorService;
use App\Services\MarketOperationsService;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Entities\UserEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Stub\Exception;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

class UserCredentialControllerTest extends TestCase
{
    private UserService $userService;
    private UserCredentialService $userCredentialService;
    private UserBrokerAccountService $userBrokerAccountService;
    private JsonValidatorService $jwtValidatorService;
    private ServerRequestInterface $request;
    private ResponseInterface $response;
    private StreamInterface $stream;
    private UserCredentialController $userCredentialController;
    private MarketOperationsService $marketOperationsService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->createMock(UserService::class);
        $this->userCredentialService = $this->createMock(UserCredentialService::class);
        $this->userBrokerAccountService = $this->createMock(UserBrokerAccountService::class);
        $this->marketOperationsService = $this->createMock(MarketOperationsService::class);
        $this->jwtValidatorService = $this->createMock(JsonValidatorService::class);
        $this->request = $this->createMock(ServerRequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->userCredentialController = new UserCredentialController(
            $this->userService,
            $this->userCredentialService,
            $this->userBrokerAccountService,
            $this->marketOperationsService,
            $this->jwtValidatorService
        );
    }

    public function testGetUserCredentialList()
    {
        $user = new UserEntity();
        $user->setId(1);

        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);

        $brokerModel01 = new BrokerModel();
        $brokerModel01->setBrokerId('tinkoff');
        $brokerModel01->setName('Tinkoff');
        $brokerModel01->setIsActive(1);

        $brokerModel02 = new BrokerModel();
        $brokerModel02->setBrokerId('BCS');
        $brokerModel02->setName('БКС');
        $brokerModel02->setIsActive(1);

        $userCredentialModel01 = new UserCredentialModel();
        $userCredentialModel01->setUserCredentialId(1);
        $userCredentialModel01->setIsActive(1);
        $userCredentialModel01->setApiKey('AAAA-BBBB-CCCC');
        $userCredentialModel01->setUserId(1);
        $userCredentialModel01->setBrokerId('tinkoff');
        $userCredentialModel01->setBroker($brokerModel01);
        $userCredentialModel01->setBrokerAccounts(new UserBrokerAccountCollection([$account01, $account02]));

        $userCredentialModel02 = new UserCredentialModel();
        $userCredentialModel02->setUserCredentialId(2);
        $userCredentialModel02->setIsActive(0);
        $userCredentialModel02->setApiKey('DDDD-EEEE-FFFF');
        $userCredentialModel02->setUserId(1);
        $userCredentialModel02->setBrokerId('BCS');
        $userCredentialModel02->setBroker($brokerModel02);
        $userCredentialModel02->setBrokerAccounts(new UserBrokerAccountCollection([$account02]));

        $responseJson = json_encode([
            [
                'userCredentialId' => 1,
                'brokerId' => 'tinkoff',
                'brokerName' => 'Tinkoff',
                'apiKey' => 'AAAA-BBBB-CCCC',
                'isActive' => 1,
                'activeAccountNumber' => 1,
                'accountNumber' => 2
            ],
            [
                'userCredentialId' => 2,
                'brokerId' => 'BCS',
                'brokerName' => 'БКС',
                'apiKey' => 'DDDD-EEEE-FFFF',
                'isActive' => 0,
                'activeAccountNumber' => 0,
                'accountNumber' => 1
            ]
        ]);

        $userCredentialCollection = new UserCredentialCollection([$userCredentialModel01, $userCredentialModel02]);

        $this->userService->expects($this->once())->method('getUser')->willReturn($user);
        $this->userCredentialService->expects($this->once())->method('getCredentialList')
            ->with(1)
            ->willReturn($userCredentialCollection);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userCredentialController->getUserCredentialList($this->request, $this->response);
    }

    #[DataProvider('dataGetUserCredential')]
    public function testGetUserCredential(
        array $args,
        string $responseJson,
        int $credentialId,
        ?UserCredentialModel $userCredential
    ): void {
        $this->userCredentialService->expects($this->once())->method('getUserCredentialDecryptedById')
            ->with($credentialId)
            ->willReturn($userCredential);

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userCredentialController->getUserCredential($this->request, $this->response, $args);
    }

    public static function dataGetUserCredential(): array
    {
        $credentialId01 = 10;
        $args01['id'] = '10';

        $account01 = new UserBrokerAccountEntity();
        $account01->setIsActive(1);
        $account02 = new UserBrokerAccountEntity();
        $account02->setIsActive(0);

        $brokerModel01 = new BrokerModel();
        $brokerModel01->setBrokerId('tinkoff');
        $brokerModel01->setName('Tinkoff');
        $brokerModel01->setIsActive(1);

        $userCredential01 = new UserCredentialModel();
        $userCredential01->setUserCredentialId(1);
        $userCredential01->setIsActive(1);
        $userCredential01->setApiKey('AAAA-BBBB-CCCC');
        $userCredential01->setUserId(1);
        $userCredential01->setBrokerId('tinkoff');
        $userCredential01->setBroker($brokerModel01);
        $userCredential01->setBrokerAccounts(new UserBrokerAccountCollection([$account01, $account02]));

        $userCredentialEntity02 = null;

        $responseJson01 = json_encode([
            'userCredentialId' => 1,
            'brokerId' => 'tinkoff',
            'brokerName' => 'Tinkoff',
            'apiKey' => 'AAAA-BBBB-CCCC',
            'isActive' => 1,
            'activeAccountNumber' => 1,
            'accountNumber' => 2
        ]);
        $responseJson02 = json_encode([]);

        return [
            'Common Case' => [
                $args01,
                $responseJson01,
                $credentialId01,
                $userCredential01
            ],
            'Empty Credential Case' => [
                $args01,
                $responseJson02,
                $credentialId01,
                $userCredentialEntity02
            ]
        ];
    }

    public function testAddUserCredential()
    {
        $body = [
            'userId' => 2,
            'brokerId' => 'tinkoff',
            'apiKey' => 'AAAA-BBBB-CCCC',
            'isActive' => 1
        ];
        $userCredentialEntity = new UserCredentialEntity();
        $userCredentialEntity->setId(1);
        $userCredentialEntity->setUserId(2);
        $userCredentialEntity->setBrokerId('tinkoff');
        $userCredentialEntity->setApiKey('AAAA-BBBB-CCCC');
        $userCredentialEntity->setIsActive(1);

        $responseJson = json_encode(['userCredentialId' => 1]);

        $this->userCredentialService->expects($this->once())->method('addUserCredential')
            ->with(new UserCredentialModel($body))
            ->willReturn($userCredentialEntity);

        $this->request->expects($this->once())->method('getParsedBody')
            ->willReturn($body);
        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(200)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userCredentialController->addUserCredential($this->request, $this->response, []);
    }

    public function testUpdateUserCredential()
    {
        $args = ['id' => 1];
        $body = [
            'userId' => 2,
            'brokerId' => 'tinkoff',
            'apiKey' => 'AAAA-BBBB-CCCC',
            'isActive' => 1
        ];
        $brokerEntity = new BrokerEntity();
        $brokerEntity->setId('tinkoff');
        $brokerEntity->setName('Tinkoff');
        $brokerEntity->setIsActive(1);

        $userCredentialEntity = new UserCredentialEntity();
        $userCredentialEntity->setId(1);
        $userCredentialEntity->setUserId(2);
        $userCredentialEntity->setBrokerId('tinkoff');
        $userCredentialEntity->setApiKey('AAAA-BBBB-CCCC');
        $userCredentialEntity->setIsActive(1);
        $userCredentialEntity->setBroker($brokerEntity);

        $userCredentialModel = new UserCredentialModel($body);
        $userCredentialModel->setUserCredentialId(1);

        $responseJson = json_encode([]);

        $this->userCredentialService->expects($this->once())->method('updateUserCredential')
            ->with($userCredentialModel);

        $this->request->expects($this->once())->method('getParsedBody')
            ->willReturn($body);
        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(204)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userCredentialController->updateUserCredential($this->request, $this->response, $args);
    }

    #[DataProvider('dataGetBrokerAccounts')]
    public function testGetBrokerAccounts(
        string $responseJson,
        int $statusCode,
        array $args,
        int $credentialId,
        ?UserBrokerAccountCollection $brokerAccountCollection,
        ?BaseException $exception
    ) {
        if (null === $exception) {
            $this->userBrokerAccountService->expects($this->once())
                ->method('getUserAccountListByCredential')
                ->with($credentialId)
                ->willReturn($brokerAccountCollection);
        } else {
            $this->userBrokerAccountService->method('getUserAccountListByCredential')
                ->will($this->throwException($exception));
        }

        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with($statusCode)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userCredentialController->getBrokerAccounts($this->request, $this->response, $args);
    }

    public static function dataGetBrokerAccounts(): array
    {
        $args01 = ['id' => 1];
        $credentialId01 = 1;
        $brokerAccount01 = new UserBrokerAccountEntity();
        $brokerAccount01->setId(1);
        $brokerAccount01->setUserCredentialId(2);
        $brokerAccount01->setCreatedDate(new \DateTime('2022-01-15 18:05:25'));
        $brokerAccount01->setUpdatedDate(new \DateTime('2022-02-20 12:00:15'));
        $brokerAccount01->setIsDeleted(0);
        $brokerAccount01->setIsActive(1);
        $brokerAccount01->setType(1);
        $brokerAccount01->setName('Primary Broker Account');
        $brokerAccount01->setExternalId('external_id_01');

        $brokerAccount02 = new UserBrokerAccountEntity();
        $brokerAccount02->setId(2);
        $brokerAccount02->setUserCredentialId(4);
        $brokerAccount02->setCreatedDate(new \DateTime('2021-12-02 15:05:25'));
        $brokerAccount02->setUpdatedDate(new \DateTime('2022-01-20 13:00:15'));
        $brokerAccount02->setIsDeleted(0);
        $brokerAccount02->setIsActive(0);
        $brokerAccount02->setType(2);
        $brokerAccount02->setName('Secondary Broker Account');
        $brokerAccount02->setExternalId('external_id_02');

        $brokerAccountCollection01 = new UserBrokerAccountCollection([$brokerAccount01, $brokerAccount02]);
        $brokerAccountCollection02 = null;

        $exception01 = null;

        $exception02 = new UserAccessDeniedException('Access denied for user', 2020);

        $statusCode01 = 200;
        $statusCode02 = 422;
        $responseJson01 = json_encode([
            [
                'id' => 1,
                'name' => 'Primary Broker Account',
                'externalId' => 'external_id_01',
                'isActive' => 1,
                'type' => 1
            ],
            [
                'id' => 2,
                'name' => 'Secondary Broker Account',
                'externalId' => 'external_id_02',
                'isActive' => 0,
                'type' => 2
            ]
        ]);
        $responseJson02 = json_encode([
            'error_code' => 2020,
            'error_message' => 'Access denied for user'
        ]);

        return [
            'Common Case' => [
                $responseJson01,
                $statusCode01,
                $args01,
                $credentialId01,
                $brokerAccountCollection01,
                $exception01
            ],
            'Exception Case' => [
                $responseJson02,
                $statusCode02,
                $args01,
                $credentialId01,
                $brokerAccountCollection02,
                $exception02
            ]
        ];
    }

    public function testSetBrokerAccounts()
    {
        $args = ['id' => 1];
        $credentialId = 1;
        $body = [
            'userAccount' => [
                [
                    'externalId' => 'external_id_01',
                    'isActive' => 1
                ],
                [
                    'externalId' => 'external_id_02',
                    'isActive' => 0
                ]
            ]
        ];

        $userBrokerAccountEntity01 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity01->setExternalId('external_id_01');
        $userBrokerAccountEntity01->setIsActive(1);
        $userBrokerAccountEntity02 = new UserBrokerAccountEntity();
        $userBrokerAccountEntity02->setExternalId('external_id_02');
        $userBrokerAccountEntity02->setIsActive(0);

        $accounts = new UserBrokerAccountCollection();
        $accounts->add($userBrokerAccountEntity01);
        $accounts->add($userBrokerAccountEntity02);

        $responseJson = json_encode([]);

        $this->request->expects($this->once())->method('getParsedBody')->willReturn($body);
        $this->response->expects($this->once())->method('withHeader')
            ->with('Content-Type', 'application/json')->willReturn($this->response);
        $this->response->expects($this->once())->method('withStatus')->with(204)
            ->willReturn($this->response);

        $this->response->expects($this->once())->method('getBody')->willReturn($this->stream);
        $this->stream->expects($this->once())->method('write')
            ->with($responseJson);

        $this->userBrokerAccountService->expects($this->once())->method('updateUserAccountList')
            ->with($credentialId, $accounts);

        $this->userCredentialController->setBrokerAccounts($this->request, $this->response, $args);
    }
}
