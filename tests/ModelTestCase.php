<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

abstract class ModelTestCase extends TestCase
{
    protected $model;

    abstract public static function dataTestGetSet(): array;

    /**
     * @param array $setValues
     * @param array $getValues
     * @param array $initialValues
     * @param string $expectedException
     * @param array $additionalValues
     * @throws \ReflectionException
     */
    #[DataProvider('dataTestGetSet')]
    public function testGetSet(
        array $setValues,
        array $getValues,
        array $initialValues = [],
        ?string $expectedException = null
    ): void {
        if ($expectedException) {
            $this->expectException($expectedException);
        }

        foreach ($initialValues as $property => $value) {
            $this->setAttribute($this->model, $property, $value);
        }

        foreach ($setValues as $method => $value) {
            $this->model->$method($value);
        }

        foreach ($getValues as $method => $value) {
            static::assertEquals($value, $this->model->$method());
        }
    }
}
