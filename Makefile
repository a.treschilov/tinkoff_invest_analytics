start: setup-dev-config docker-compose-services-up docker-compose-up

stop: docker-compose-stop

start-prod: setup-prod-config docker-compose-up

start-with-composer: start composer-install

start-consumers:
	docker exec -it tinkoff_invest_php /bin/bash -c "php app/Consumers/run.php"

code-sniffer:
	docker exec -it tinkoff_invest_php /bin/bash -c "vendor/bin/phpcs --standard=PSR12 app/ tests/"

migrate:
	for eachfile in migrations/*.sql; \
	do \
	  docker exec -i tinkoff_invest_mysql sh -c 'exec mysql -uroot -p"$$MYSQL_ROOT_PASSWORD"' < $$eachfile; \
	  echo "Done $${eachfile}"; \
	done

composer-install:
	echo "Composer install start"
	docker exec -it tinkoff_invest_php /bin/bash -c "composer install"
	echo "Composer install end"

composer-update:
	echo "Composer update start"
	docker exec -it tinkoff_invest_php /bin/bash -c "composer update"
	echo "Composer install end"

composer-outdated:
	docker exec -it tinkoff_invest_php /bin/bash -c "composer outdated --direct"

docker-compose-up:
	docker-compose up -d --build

docker-compose-services-up:
	docker-compose -f docker-compose.services.yml up -d --build

docker-compose-stop:
	docker-compose stop

setup-dev-config:
	rm config/.env | true
	cp config/dev.env config/.env

setup-prod-config:
	rm config/.env | true
	cp config/prod.env config/.env
