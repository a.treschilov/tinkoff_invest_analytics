# Аналитика портфеля на базе API Tinkoff Инвестиции

[![pipeline status](https://gitlab.com/a.treschilov/tinkoff_invest_analytics/badges/master/pipeline.svg)](https://gitlab.com/a.treschilov/tinkoff_invest_analytics/-/commits/master)
[![coverage report](https://gitlab.com/a.treschilov/tinkoff_invest_analytics/badges/master/coverage.svg)](https://gitlab.com/a.treschilov/tinkoff_invest_analytics/-/commits/master)

## How to use
### Before start
Paste your credentials to `config/dev.env`

### Run locally
1. Create file `config/dev.env`
2. `make start`
3. `make migrate`
4. Open `http://localhost:4091/api/v1/health_check/up` in browser

### Run with install composer dependencies
`make start-with-composer`

### App Structure
`app/` - source code

`config/` - config variables

`tests/` - autotests

`images/` - docker images
