SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.deposit
(
    deposit_id INT AUTO_INCREMENT,
    item_id INT NOT NULL,
    user_id INT NOT NULL,
    deal_date DATETIME NOT NULL,
    closed_date DATETIME,
    interest_percent DECIMAL (16,4) NOT NULL,
    duration INT NOT NULL,
    duration_period ENUM('day', 'month', 'year') NOT NULL,
    payout_frequency INT NOT NULL,
    payout_frequency_period ENUM('day', 'month', 'year') NOT NULL,
    initial_operation_id INT NOT NULL,
    PRIMARY KEY (deposit_id),
    INDEX deposit_user_id_index (user_id),
    CONSTRAINT deposit_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT deposit_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT deposit_item_operations__item_operation_id_fk
        FOREIGN KEY (initial_operation_id) REFERENCES tinkoff_invest.item_operations (item_operation_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Deposit\'s list';