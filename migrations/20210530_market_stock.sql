SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_stock
(
    market_stock_id INT AUTO_INCREMENT,
    market_instrument_id INT NOT NULL,
    country VARCHAR(255),
    sector VARCHAR(255) NULL,
    industry VARCHAR(255) NULL,
    PRIMARY KEY (market_stock_id),
    INDEX inx_market_stock_market_instrument_id(market_instrument_id),
    CONSTRAINT market_stock_market_instrument_market_instrument_id_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Информация о компаниях';