SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_volume_anomalies
(
    market_volume_anomaly_id INT AUTO_INCREMENT,
    start_date DATETIME NOT NULL,
    end_date DATETIME,
    market_instrument_id INT NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (market_volume_anomaly_id),
    CONSTRAINT market_volume_anomaly_market_instrument_market_instrument_id_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Volume anomalies';