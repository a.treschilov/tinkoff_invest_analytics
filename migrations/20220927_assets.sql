SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.payout_type
(
    payout_type_id INT AUTO_INCREMENT,
    external_id VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    PRIMARY KEY (payout_type_id),
    UNIQUE KEY unq_payout_type_external_id(external_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Dictionary of types of payout';

INSERT IGNORE INTO tinkoff_invest.payout_type (external_id, name, description) VALUES
   ('differentiated', 'Дифференцированный', 'Проценты начисляются на оставшуюся сумму долга'),
   ('differentiatedConstantBase', 'Дифференцированный с постоянной базой', 'Проценты начисляются на сумму займа'),
   ('annuitant', 'Аннуитентный', 'Равные ежемесячные платежи'),
   ('rent', 'Аренда', 'Фиксированные платежи по модели аренды');

CREATE TABLE IF NOT EXISTS tinkoff_invest.assets
(
    asset_id INT AUTO_INCREMENT,
    item_id INT,
    external_id VARCHAR(255),
    nominal_amount DECIMAL (16, 4),
    currency_id INT,
    platform ENUM('potok', 'money_friends', 'jetlend', 'other') NOT NULL,
    name VARCHAR(255) NOT NULL,
    type ENUM('loan', 'deposit', 'crowdfunding') NOT NULL,
    payout_type_id INT,
    deal_date DATETIME,
    duration INT,
    duration_period ENUM('day', 'month', 'year'),
    interest_percent FLOAT,
    interest_amount FLOAT,
    payout_frequency INT,
    payout_frequency_period ENUM('day', 'month', 'year'),
    PRIMARY KEY (asset_id),
    CONSTRAINT assets_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT assets_payout_type_payout_type_id_fk
        FOREIGN KEY (payout_type_id) REFERENCES tinkoff_invest.payout_type (payout_type_id),
    CONSTRAINT assets_currency_currency_id_fk
        FOREIGN KEY (currency_id) REFERENCES tinkoff_invest.currency (currency_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'List of assets';
