SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_code
(
    user_code_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    code VARCHAR(15) NOT NULL,
    expire_date DATETIME NOT NULL,
    PRIMARY KEY (user_code_id),
    CONSTRAINT user_code_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Temporary user codes';