SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_bond
(
    market_bond_id INT AUTO_INCREMENT,
    market_instrument_id INT NOT NULL,
    nominal DECIMAL(16,4) NOT NULL,
    initial_nominal DECIMAL (16,4) NOT NULL,
    currency_iso VARCHAR(7) NOT NULL,
    maturity_date DATETIME,
    updated_date DATETIME NOT NULL,
    PRIMARY KEY (market_bond_id),
    CONSTRAINT market_bond_market_instrument_market_instrument_id_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Market bond data';