SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_instrument
(
    market_instrument_id INT AUTO_INCREMENT,
    ticker VARCHAR(255) NOT NULL,
    isin VARCHAR(255) NULL,
    min_price_increment FLOAT,
    lot INT NOT NULL,
    currency VARCHAR(255) NOT NULL,
    name VARCHAR (255) NOT NULL,
    type VARCHAR (255) NOT NULL,
    reg_number VARCHAR(255),
    PRIMARY KEY (market_instrument_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник инструментов рынка';