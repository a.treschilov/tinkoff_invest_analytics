SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_tinkoff
(
    market_tinkoff_id INT AUTO_INCREMENT,
    ticker VARCHAR(255) NOT NULL,
    isin VARCHAR(255) NULL,
    class_code VARCHAR(255) NULL,
    exchange VARCHAR(255) NOT NULL,
    instrument_id VARCHAR(255) NULL,
    position_id VARCHAR(255) NULL,
    min_price_increment FLOAT,
    lot INT NOT NULL,
    currency VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    logo VARCHAR(255),
    first_candle_date DATETIME NULL,
    instrument_type VARCHAR(255) NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    not_found_attempts INT NOT NULL DEFAULT 0,
    PRIMARY KEY (market_tinkoff_id),
    INDEX market_tinkoff_isin_index (isin),
    INDEX market_tinkoff_isin_instrument_id (instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник инструментов Tinkoff';
