SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.real_estate
(
    real_estate_id INT AUTO_INCREMENT,
    item_id INT NOT NULL,
    user_id INT NOT NULL,
    interest_amount DECIMAL(16,4),
    interest_currency VARCHAR(7),
    payout_frequency INT,
    payout_frequency_period ENUM('day', 'month', 'year'),
    currency VARCHAR(16),
    purchase_operation_id INT NOT NULL,
    purchase_price_id INT,
    sale_operation_id INT,
    PRIMARY KEY (real_estate_id),
    INDEX real_estate_user_id_index (user_id),
    CONSTRAINT real_estate_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT real_estate__item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT real_estate_item_operations__item_operation_id_fk
        FOREIGN KEY (purchase_operation_id) REFERENCES tinkoff_invest.item_operations (item_operation_id),
    CONSTRAINT real_estate_item_operations__sale_operation_id_fk
        FOREIGN KEY (sale_operation_id) REFERENCES tinkoff_invest.item_operations (item_operation_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Real estate list';

CREATE TABLE IF NOT EXISTS tinkoff_invest.real_estate_price
(
    real_estate_price_id INT AUTO_INCREMENT,
    real_estate_id INT NOT NULL,
    amount DECIMAL(16,4) NOT NULL,
    currency VARCHAR(16) NOT NULL,
    date DATETIME NOT NULL,
    PRIMARY KEY (real_estate_price_id),
    INDEX real_estate_user_id_index (real_estate_id),
    CONSTRAINT real_estate_price_real_estate_real_estate_id_fk
        FOREIGN KEY (real_estate_id) REFERENCES tinkoff_invest.real_estate (real_estate_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Price of real estate';

ALTER TABLE tinkoff_invest.real_estate
    ADD CONSTRAINT real_estate_real_estate__real_estate_price_id_fk
        FOREIGN KEY (purchase_price_id) REFERENCES tinkoff_invest.real_estate_price (real_estate_price_id);