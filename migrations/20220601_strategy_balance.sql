SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_currency
(
    market_currency_id INT AUTO_INCREMENT,
    market_instrument_id INT,
    iso VARCHAR(7) NOT NULL,
    nominal DECIMAL(16, 4) NOT NULL,
    PRIMARY KEY (market_currency_id),
    CONSTRAINT market_currency_market_instrument_market_instrument_id_fk2
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Dictionary broker currencies';

CREATE TABLE IF NOT EXISTS tinkoff_invest.strategy_currency_balance
(
    strategy_currency_balance_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    currency_iso VARCHAR(7) NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    share FLOAT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (strategy_currency_balance_id),
    CONSTRAINT strategy_currency_balance_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    INDEX inx_strategy_currency_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User strategy of currency balance';