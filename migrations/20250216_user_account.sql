SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_account
(
    user_account_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    auth_type enum('YANDEX', 'GOOGLE', 'TELEGRAM') NOT NULL,
    client_id varchar(255) NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (user_account_id),
    CONSTRAINT user_account_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User auth accounts';
