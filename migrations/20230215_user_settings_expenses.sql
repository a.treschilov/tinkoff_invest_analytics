SET NAMES utf8mb4;

ALTER TABLE tinkoff_invest.user_settings
    ADD expenses INT null COMMENT 'Monthly expenses';

ALTER TABLE tinkoff_invest.user_settings
    ADD currency_iso VARCHAR(7) NOT NULL;