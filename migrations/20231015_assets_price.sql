SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.assets_price
(
    assets_price_id INT AUTO_INCREMENT,
    asset_id INT NOT NULL,
    amount DECIMAL (16, 4) NOT NULL,
    currency VARCHAR(7),
    date DATE NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (assets_price_id),
    INDEX  assets_price_asset_id_index (asset_id),
    CONSTRAINT assets_price_assets_asset_id_fk
        FOREIGN KEY (asset_id) REFERENCES tinkoff_invest.assets (asset_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Prices of assets';