SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_future
(
    market_future_id INT AUTO_INCREMENT,
    market_instrument_id INT NOT NULL,
    futures_type ENUM ('physical_delivery', 'cash_settlement'),
    asset_type ENUM ('commodity', 'currency', 'security', 'index'),
    expiration_date DATETIME NOT NULL,
    min_price_increment_amount DOUBLE,
    updated_date DATETIME NOT NULL,
    PRIMARY KEY (market_future_id),
    CONSTRAINT market_future_market_instrument_market_instrument_id_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Futures';