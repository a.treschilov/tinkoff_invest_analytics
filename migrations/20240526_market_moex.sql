SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_moex
(
    market_moex_id INT AUTO_INCREMENT,
    ticker VARCHAR(255) NOT NULL,
    isin VARCHAR(255) NULL,
    name VARCHAR(255) NOT NULL,
    currency VARCHAR(15) NOT NULL,
    min_price_increment FLOAT,
    instrument_type ENUM ('shares', 'bonds', 'index', 'futures', 'options', 'etf') NOT NULL,
    reg_number VARCHAR(255),
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (market_moex_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Moex assets';