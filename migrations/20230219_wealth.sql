SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.wealth_history
(
    wealth_history_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    currency_iso VARCHAR(7) NOT NULL,
    date DATETIME NOT NULL,
    stock DECIMAL (16,4) NOT NULL,
    assets DECIMAL (16,4) NOT NULL,
    real_estate DECIMAL (16,4) NOT NULL,
    deposit DECIMAL (16,4) NOT NULL,
    loan DECIMAL (16,4) NOT NULL,
    expenses INT,
    is_active TINYINT DEFAULT 1 NOT NULL,
    PRIMARY KEY (wealth_history_id),
    INDEX wealth_user_id_index (user_id),
    CONSTRAINT wealth_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Wealth data';