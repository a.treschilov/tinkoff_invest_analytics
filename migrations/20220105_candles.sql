SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.candles
(
    candle_id INT AUTO_INCREMENT,
    market_instrument_id INT NOT NULL,
    is_final TINYINT NOT NULL DEFAULT 1,
    open DOUBLE NOT NULL,
    close DOUBLE NOT NULL,
    high DOUBLE NOT NULL,
    low DOUBLE NOT NULL,
    volume BIGINT NOT NULL,
    currency VARCHAR(8) NOT NULL,
    time DATETIME NOT NULL,
    candle_interval ENUM('1min', '2min', '3min', '5min', '10min', '15min', '30min', '1hour', '2hour', '4hour', '1day', '1week', '1month') NOT NULL,
    is_actual TINYINT NOT NULL DEFAULT 1,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (candle_id),
    CONSTRAINT candles_marker_instrument_market_instrument_figi_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Информация о свечах';