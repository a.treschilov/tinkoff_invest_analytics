SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.item
(
    item_id INT AUTO_INCREMENT,
    type ENUM('market', 'real_estate', 'loan', 'deposit', 'crowdfunding') NOT NULL,
    source ENUM('market', 'potok', 'money_friends', 'jetlend', 'user') NOT NULL,
    external_id VARCHAR(255),
    name VARCHAR(255) NOT NULL,
    logo VARCHAR(255),
    is_outdated TINYINT NOT NULL DEFAULT 0,
    is_deleted TINYINT NOT NULL DEFAULT 0,
    PRIMARY KEY (item_id),
    UNIQUE KEY unq_item_source_external_id(source, external_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Dictionary of items';

START TRANSACTION;
ALTER TABLE tinkoff_invest.market_instrument
    ADD item_id INT AFTER market_instrument_id;

INSERT INTO tinkoff_invest.item (type, source, external_id, name)
SELECT 'market', 'market', figi, name FROM tinkoff_invest.market_instrument WHERE item_id IS NULL;

UPDATE tinkoff_invest.market_instrument mi SET
    item_id = (SELECT item_id FROM tinkoff_invest.item i WHERE i.external_id = mi.figi)
WHERE mi.item_id IS NULL;

ALTER TABLE tinkoff_invest.market_instrument
    MODIFY item_id int NOT NULL;

ALTER TABLE tinkoff_invest.market_instrument
    ADD CONSTRAINT market_instrument_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id);
COMMIT;

INSERT IGNORE INTO tinkoff_invest.broker (broker_id, name, logo, is_active, is_api, is_manual, instruction_link) VALUES
   ('money_friends', 'Money Friends', 'https://app.hakkes.com/images/broker/money_friends.png',  1, 0, 1, '/help/article/moneyFriends'),
   ('potok', 'Potok', 'https://app.hakkes.com/images/broker/potok.png', 1, 0, 1, '/help/article/potok'),
   ('jetlend', 'Jetlend', 'https://app.hakkes.com/images/broker/jetlend.png', 1, 0, 1, '/help/article/jetlend'),
   ('tinkoff2', 'Т-Инвестиции', 'https://app.hakkes.com/images/broker/tbank.png', 1, 0, 1, '/help/article/t-investments'),
   ('alfa_invest', 'Альфа Инвестиции', 'https://app.hakkes.com/images/broker/alfa_bank.svg', 1, 0, 1, '/help/article/alfaInvest'),
   ('sber', 'СберИнвестиции', 'https://app.hakkes.com/images/broker/sber.svg', 1, 0, 1, '/help/article/sberInvest')
;

CREATE TABLE IF NOT EXISTS tinkoff_invest.item_operations
(
    item_operation_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    broker_id VARCHAR(255),
    item_id INT,
    operation_type_id INT,
    external_id VARCHAR(255),
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    quantity DECIMAL (16, 4),
    date DATETIME NOT NULL,
    amount DECIMAL (16, 4),
    currency_id INT,
    status VARCHAR(255) NOT NULL,
    is_deleted TINYINT NOT NULL DEFAULT 0,
    error_code INT DEFAULT NULL,
    PRIMARY KEY (item_operation_id),
    INDEX item_operations_external_id_broker_id_index (external_id, broker_id, operation_type_id),
    INDEX item_operations_user_id_index (user_id),
    INDEX item_operations_broker_id_index (broker_id),
    UNIQUE KEY unq_operations_external_id_broker_id(external_id, broker_id, operation_type_id),
    CONSTRAINT item_operations_broker_broker_id_fk
        FOREIGN KEY (broker_id) REFERENCES tinkoff_invest.broker (broker_id),
    CONSTRAINT item_operations_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT item_operations_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT item_operations_operation_type_operation_type_id_fk
        FOREIGN KEY (operation_type_id) REFERENCES tinkoff_invest.operation_type (operation_type_id),
    CONSTRAINT item_operations_currency_currency_id_fk
        FOREIGN KEY (currency_id) REFERENCES tinkoff_invest.currency (currency_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Operation list';

CREATE TABLE IF NOT EXISTS tinkoff_invest.item_operations_import
(
    item_operations_import_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    user_broker_account_id INT,
    import_date_start DATETIME NOT NULL,
    import_date_end DATETIME NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    added INT NOT NULL,
    updated INT NOT NULL,
    skipped INT NOT NULL,
    PRIMARY KEY (item_operations_import_id),
    INDEX  item_operations_import_user_index (user_id),
    CONSTRAINT item_operations_import_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT item_operations_import_user_broker_account_id_fk
        FOREIGN KEY (user_broker_account_id) REFERENCES tinkoff_invest.user_broker_account (user_broker_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Import operations statistic';