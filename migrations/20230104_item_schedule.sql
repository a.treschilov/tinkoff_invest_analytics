SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.item_payment_schedule
(
    item_payment_schedule_id INT AUTO_INCREMENT,
    is_active TINYINT NOT NULL DEFAULT 1,
    item_id INT NOT NULL,
    interest_amount DECIMAL (16, 4) NOT NULL,
    debt_amount DECIMAL (16, 4) NOT NULL,
    currency_id INT NOT NULL,
    date DATETIME NOT NULL,
    PRIMARY KEY (item_payment_schedule_id),
    INDEX item_payment_schedule_item_id_index (item_id),
    CONSTRAINT item_payment_schedule_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT item_payment_schedule_currency_currency_id_fk
        FOREIGN KEY (currency_id) REFERENCES tinkoff_invest.currency (currency_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Schedule of payments';
