SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_settings
(
    user_settings_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    desired_pension_amount FLOAT COMMENT "monthly desired pension",
    desired_pension_currency VARCHAR(7) NOT NULL,
    expenses_amount FLOAT COMMENT "monthly desired pension",
    expenses_currency VARCHAR(7) NOT NULL ,
    retirement_age INT,
    PRIMARY KEY (user_settings_id),
    INDEX inx_user_settings_user_id(user_id),
    UNIQUE KEY unq_user_settings_user_id(user_id),
    CONSTRAINT user_settings_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User settings';
