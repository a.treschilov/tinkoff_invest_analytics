SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.candles_import
(
    candles_import_id INT AUTO_INCREMENT,
    market_instrument_id INT NOT NULL,
    import_date_start DATETIME NOT NULL,
    import_date_end DATETIME NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    added INT NOT NULL,
    updated INT NOT NULL,
    skipped INT NOT NULL,
    PRIMARY KEY (candles_import_id),
    INDEX  candles_import_market_instrument_id_index (market_instrument_id),
    CONSTRAINT candles_import_market_instrument_market_instrument_id_fk
        FOREIGN KEY (market_instrument_id) REFERENCES tinkoff_invest.market_instrument (market_instrument_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Import candles statistic';
