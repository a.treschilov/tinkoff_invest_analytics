SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_portfolio
(
    user_portfolio_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    date DATETIME NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (user_portfolio_id),
    INDEX  user_portfolio_user_id_index (user_id),
    INDEX  user_portfolio_user_date_active_id_index (user_id, date, is_active),
    CONSTRAINT user_portfolio_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User portfolio';

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_portfolio_item
(
    user_portfolio_item_id INT AUTO_INCREMENT,
    user_portfolio_id INT NOT NULL,
    item_id INT NOT NULL,
    quantity DECIMAL(16, 4) NOT NULL,
    amount DECIMAL(16, 4) NOT NULL,
    currency VARCHAR(7) NOT NULL,
    PRIMARY KEY (user_portfolio_item_id),
    INDEX  user_portfolio_item_user_portfolio_id_index (user_portfolio_id),
    CONSTRAINT user_portfolio_user_portfolio_item_user_portfolio_item_id_fk
        FOREIGN KEY (user_portfolio_id) REFERENCES tinkoff_invest.user_portfolio (user_portfolio_id),
    CONSTRAINT user_portfolio_item_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User portfolio item list';

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_portfolio_balance
(
    user_portfolio_balance_id INT AUTO_INCREMENT,
    user_portfolio_id INT NOT NULL,
    broker_id VARCHAR(255),
    amount DECIMAL(16, 4) NOT NULL,
    currency VARCHAR(7) NOT NULL,
    PRIMARY KEY (user_portfolio_balance_id),
    INDEX  user_portfolio_balance_user_portfolio_id_index (user_portfolio_id),
    CONSTRAINT user_portfolio_balance_portfolio_item_user_portfolio_item_id_fk
        FOREIGN KEY (user_portfolio_id) REFERENCES tinkoff_invest.user_portfolio (user_portfolio_id),
    CONSTRAINT user_portfolio_balance_broker_broker_id_fk
        FOREIGN KEY (broker_id) REFERENCES tinkoff_invest.broker (broker_id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User portfolio balance';