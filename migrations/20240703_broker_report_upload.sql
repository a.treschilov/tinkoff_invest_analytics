SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.broker_report_upload
(
    broker_report_upload_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    date DATETIME NOT NULL,
    broker_id VARCHAR(255) NOT NULL,
    added INT NOT NULL,
    updated INT NOT NULL,
    skipped INT NOT NULL,
    status ENUM ('fail', 'success') NOT NULL,
    PRIMARY KEY (broker_report_upload_id),
    INDEX inx_broker_report_upload_user_id(user_id),
    CONSTRAINT broker_report_upload_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT broker_report_upload_broker_broker_id_fk
        FOREIGN KEY (broker_id) REFERENCES tinkoff_invest.broker (broker_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Broker report uploading logs';