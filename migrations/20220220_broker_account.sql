SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.broker
(
    broker_id VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    logo VARCHAR(255),
    is_active TINYINT NOT NULL DEFAULT 1,
    is_api TINYINT NOT NULL DEFAULT 0 COMMENT 'integration by API',
    is_manual TINYINT NOT NULL DEFAULT 0 COMMENT 'uploading broker report integration',
    instruction_link VARCHAR(255),
    PRIMARY KEY (broker_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник брокеров';

INSERT IGNORE INTO tinkoff_invest.broker (broker_id, name, logo, is_active, is_api, is_manual, instruction_link)
VALUES
    ('tinkoff', 'Тинькофф', 'https://app.hakkes.com/images/broker/tbank.png', 1, 1, 0, '/help/article/t-investments');

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_broker_account
(
    user_broker_account_id INT AUTO_INCREMENT,
    user_credential_id INT NOT NULL,
    external_id VARCHAR(255) NOT NULL,
    name VARCHAR(255),
    type INT NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    is_deleted TINYINT NOT NULL DEFAULT 0,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_broker_account_id),
    INDEX inx_user_broker_account_user_credential_id(user_credential_id),
    CONSTRAINT user_broker_account_user_credential_user_credential_id_fk
        FOREIGN KEY (user_credential_id) REFERENCES tinkoff_invest.user_credential (user_credential_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Брокерские счета пользователя';

CREATE UNIQUE INDEX user_broker_account_user_credential_id_external_id_index
    ON tinkoff_invest.user_broker_account (user_credential_id, external_id);

ALTER TABLE tinkoff_invest.user_credential
    CHANGE broker broker_id VARCHAR(255) not null;

ALTER TABLE tinkoff_invest.user_credential
    ADD CONSTRAINT user_credential_broker_broker_id_fk
        FOREIGN KEY (broker_id) references tinkoff_invest.broker (broker_id);
