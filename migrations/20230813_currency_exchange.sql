SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.currency_exchange
(
    currency_exchange_id INT AUTO_INCREMENT,
    currency_from VARCHAR(7) NOT NULL,
    currency_to VARCHAR(7) NOT NULL,
    exchange_rate DECIMAL(32, 16),
    date DATE NOT NULL,
    PRIMARY KEY (currency_exchange_id),
    UNIQUE KEY unq_currency_exchange(currency_from, currency_to, date),
    INDEX inx_currency_exchange_date(date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Currency exchange rate';