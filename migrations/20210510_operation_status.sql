SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.operation_status
(
    operation_status_id INT AUTO_INCREMENT,
    external_id VARCHAR(255) NOT NULL COMMENT 'Идентификатор статуса операции в Tinkoff',
    name VARCHAR(255) null,
    PRIMARY KEY (operation_status_id),
    INDEX inx_operation_status_external(external_id),
    UNIQUE KEY unq_operation_status_external_id(external_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник статусов операций';

INSERT IGNORE INTO tinkoff_invest.operation_status (external_id, name)
VALUES ('Done', 'Выполнена');

INSERT IGNORE INTO tinkoff_invest.operation_status (external_id, name)
VALUES ('Decline', 'Отклонена');

INSERT IGNORE INTO tinkoff_invest.operation_status (external_id, name)
VALUES ('Progress', 'Активна');

INSERT IGNORE INTO tinkoff_invest.operation_status (external_id, name)
VALUES ('Unspecified', 'Не определена');

