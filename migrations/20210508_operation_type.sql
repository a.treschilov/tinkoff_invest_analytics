SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.operation_type
(
    operation_type_id INT AUTO_INCREMENT,
    external_id VARCHAR(255) NOT NULL COMMENT 'Идентификатор операции в Tinkoff',
    name VARCHAR(255) null,
    PRIMARY KEY (operation_type_id),
    INDEX inx_operation_type_external(external_id),
    UNIQUE KEY unq_operation_type_external_id(external_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Типы операций';

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Unspecified', 'Тип операции не определён');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Input', 'Пополнение счета');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Output', 'Вывод со счета');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('SecurityIn', 'Пополнение счета ценными бумагами');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('SecurityOut', 'Вывод со счета ценных бумаг');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Buy', 'Покупка');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('BuyCard', 'Покупка с внешнего счета');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Sell', 'Продажа');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('SellCard', 'Продажа с внешнего счета');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Repayment', 'Полное/частичное погашение ценной бумаги');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Dividend', 'Доход ценной бумаги');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('DividendCard', 'Доход ценной бумаги на внешний счет');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Tax', 'Налоги');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Fee', 'Комиссия');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Hold', 'Резервирование средств');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('UnHold', 'Перевод средств из резерва');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('Default', 'Дефолт');

INSERT IGNORE INTO tinkoff_invest.operation_type (external_id, name)
VALUES ('DefaultPayouts', 'Выплаты после дефолта');
