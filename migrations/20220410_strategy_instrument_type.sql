SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.instrument_type
(
    instrument_type_id INT AUTO_INCREMENT,
    external_id VARCHAR(255) NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (instrument_type_id),
    UNIQUE INDEX inx_instrument_type_external_id(external_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Справочник типов инструментов';

INSERT IGNORE INTO tinkoff_invest.instrument_type (external_id, name)
VALUES ('Stock', 'Stock'),
       ('Etf', 'Etf'),
       ('Bond', 'Bond'),
       ('Currency', 'Currency'),
       ('Futures', 'Futures'),
       ('Gold', 'Gold'),
       ('FederalBond', 'Federal Bond');

CREATE TABLE IF NOT EXISTS tinkoff_invest.strategy_instrument_type
(
    strategy_instrument_type_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    instrument_type_id INT NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    share FLOAT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (strategy_instrument_type_id),
    CONSTRAINT strategy_instrument_type_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT strategy_instrument_type_instrument_type_instrument_type_id_fk
        FOREIGN KEY (instrument_type_id) REFERENCES tinkoff_invest.instrument_type (instrument_type_id) ,
    INDEX inx_strategy_instrument_type_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Стратегия по типам инструментов';
