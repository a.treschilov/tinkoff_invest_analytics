SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_credential
(
    user_credential_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    broker_id VARCHAR(255) NOT NULL,
    api_key VARCHAR(255),
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (user_credential_id),
    CONSTRAINT user_credential_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Доступы к платформам пользователя';