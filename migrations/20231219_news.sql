SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.news
(
    news_id INT AUTO_INCREMENT,
    title VARBINARY(511) NOT NULL,
    description VARBINARY(2047) NOT NULL,
    date DATETIME NOT NULL,
    image_url VARCHAR(255),
    link VARCHAR(255) NOT NULL,
    source ENUM('marketaux') NOT NULL,
    external_id VARCHAR(255) NOT NULL,
    symbols VARCHAR(255),
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (news_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'News list';