SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.loan
(
    loan_id INT AUTO_INCREMENT,
    item_id INT NOT NULL,
    user_id INT NOT NULL,
    closed_date DATETIME,
    initial_operation_id INT,
    payment_amount DECIMAL(16, 4) NOT NULL,
    payment_currency VARCHAR(7) NOT NULL,
    payout_frequency INT NOT NULL,
    payout_frequency_period ENUM('day', 'month', 'year') NOT NULL,
    interest_percent DECIMAL(16, 4) NOT NULL,
    PRIMARY KEY (loan_id),
    INDEX loan_user_id_index (user_id),
    CONSTRAINT loan_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT loan_item_item_id_fk
        FOREIGN KEY (item_id) REFERENCES tinkoff_invest.item (item_id),
    CONSTRAINT loan_item_operation_operation_id_fk
        FOREIGN KEY (initial_operation_id) REFERENCES tinkoff_invest.item_operations (item_operation_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Loan\'s list';