SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.strategy_currency
(
    strategy_currency_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    currency_id INT NOT NULL,
    is_active INT NOT NULL DEFAULT 1,
    share FLOAT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (strategy_currency_id),
    CONSTRAINT strategy_currency_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id),
    CONSTRAINT strategy_currency_currency_currency_id_fk
        FOREIGN KEY (currency_id) REFERENCES tinkoff_invest.currency (currency_id),
    INDEX inx_strategy_currency_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Стратегия по валютам';