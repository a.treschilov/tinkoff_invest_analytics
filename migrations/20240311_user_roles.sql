SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.user_roles
(
    user_roles_id INT AUTO_INCREMENT,
    user_id INT NOT NULL,
    role ENUM('admin') NOT NULL,
    is_active TINYINT NOT NULL DEFAULT 1,
    PRIMARY KEY (user_roles_id),
    CONSTRAINT user_roles_users_user_id_fk
        FOREIGN KEY (user_id) REFERENCES tinkoff_invest.users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'User roles';