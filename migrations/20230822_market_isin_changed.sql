SET NAMES utf8mb4;

CREATE TABLE IF NOT EXISTS tinkoff_invest.market_isin_changed
(
    market_isin_changed_id INT AUTO_INCREMENT,
    deprecated_isin VARCHAR(255) NOT NULL,
    actual_isin VARCHAR(255) NOT NULL,
    date_created DATETIME NOT NULL,
    is_processed TINYINT NOT NULL DEFAULT 0,
    PRIMARY KEY (market_isin_changed_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Dictionary of changed isin';