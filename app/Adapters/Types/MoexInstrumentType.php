<?php

declare(strict_types=1);

namespace App\Adapters\Types;

enum MoexInstrumentType: string
{
    case Stock = 'shares';
    case Bond = 'bonds';
    case Etf = 'etf';
    case Futures = 'futures';
    case index = 'index';
}
