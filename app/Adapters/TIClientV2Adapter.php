<?php

declare(strict_types=1);

namespace App\Adapters;

use App\Adapters\Entities\MarketIsinChangedEntity;
use App\Adapters\Models\TinkoffInstrumentModel;
use App\Adapters\Services\MarketTinkoffService;
use App\Adapters\Storages\MarketIsinChangedStorage;
use App\Broker\Collections\BondCollection;
use App\Broker\Collections\BondCouponCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\FutureCollection;
use App\Broker\Collections\InstrumentCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerBondCouponModel;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerFutureModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerShareModel;
use App\Broker\Services\Adapters\TinkoffHelper;
use App\Collections\CandleCollection;
use App\Common\Amqp\AmqpClient;
use App\Exceptions\BrokerAdapterException;
use App\Market\Types\CandleIntervalType;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use Tinkoff\Invest\V1\Bond;
use Tinkoff\Invest\V1\Coupon;
use Tinkoff\Invest\V1\Currency;
use Tinkoff\Invest\V1\Future;
use Tinkoff\Invest\V1\HistoricCandle;
use Tinkoff\Invest\V1\IndicativeResponse;
use Tinkoff\Invest\V1\InstrumentIdType;
use Tinkoff\Invest\V1\LastPrice;
use Tinkoff\Invest\V1\Share;

class TIClientV2Adapter implements SourceBrokerInterface
{
    private const BROKER = 'tinkoff2';

    private ?BondCollection $bonds = null;

    public function __construct(
        private readonly TIClient $client,
        private readonly TinkoffHelper $tinkoffHelper,
        private readonly MarketTinkoffService $marketTinkoffService,
        private readonly MarketIsinChangedStorage $marketIsinChangedStorage,
        private readonly AmqpClient $amqpClient
    ) {
    }

    /**
     * @param string $isin
     * @return InstrumentCollection
     */
    public function getInstrumentByIsin(string $isin): InstrumentCollection
    {
        $tiInstruments = $this->marketTinkoffService->getByIsinOrTicker($isin);

        $brokerInstrumentCollection = new InstrumentCollection();

        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstruments->getIterator() as $tiInstrument) {
            $data = [
                'currency' => $this->tinkoffHelper->getCurrencyISO($tiInstrument->getCurrency()),
                'ticker' => $tiInstrument->getTicker(),
                'exchange' => $tiInstrument->getClassCode(),
                'type' => $this->tinkoffHelper->getInstrumentType($tiInstrument->getInstrumentType()),
                'name' => $tiInstrument->getName(),
                'logo' => $tiInstrument->getLogo(),
                'isin' => $tiInstrument->getIsin(),
                'minPriceIncrement' => $tiInstrument->getMinPriceIncrement(),
                'lot' => $tiInstrument->getLot(),
                'first_candle_date' => $tiInstrument->getFirstCandleDate()
            ];
            $brokerInstrumentCollection->add(new BrokerInstrumentModel($data));
        }

        return $brokerInstrumentCollection;
    }

    public function getCurrencyByIsin(string $isin): ?BrokerCurrencyModel
    {
        $currency = null;

        $tiInstruments = $this->marketTinkoffService->getByIsinOrTicker($isin);

        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstruments->getIterator() as $tiInstrument) {
            $currency = $this->getCurrencyByUid($tiInstrument->getInstrumentId());
            if ($currency !== null) {
                break;
            }
        }

        return $currency;
    }

    private function getCurrencyByUid(string $uid): ?BrokerCurrencyModel
    {
        $currency = $this->client->getInstruments()->getCurrencyBy(
            InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
            null,
            $uid
        );

        if ($currency === null) {
            return null;
        }

        return new BrokerCurrencyModel([
            'iso' => $this->tinkoffHelper->getCurrencyISO($currency->getIsoCurrencyName()),
            'nominal' => $this->tinkoffHelper->convertMoneyValueToFloat($currency->getNominal())
        ]);
    }

    public function getCurrencies(): BrokerCurrencyCollection
    {
        $currencyList = $this->client->getInstruments()->getCurrencies();

        $collection = new BrokerCurrencyCollection();

        /** @var Currency $currency */
        foreach ($currencyList->getIterator() as $currency) {
            $this->getOrAddInstrument($currency->getUid(), $currency->getIsin());

            $currencyModel = new BrokerCurrencyModel([
                'iso' => $this->tinkoffHelper->getCurrencyISO($currency->getIsoCurrencyName()),
                'nominal' => $this->tinkoffHelper->convertMoneyValueToFloat($currency->getNominal())
            ]);
            $collection->add($currencyModel);
        }

        return $collection;
    }

    public function getHistoryCandles(
        string $isin,
        \DateTime $from,
        \DateTime $to,
        CandleIntervalType $interval
    ): ?CandleCollection {
        $candleCollection = new CandleCollection();

        $instruments = $this->marketTinkoffService->getByIsinOrTicker($isin);

        /** @var TinkoffInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            if (
                $instrument->getFirstCandleDate()?->getTimestamp() > $to->getTimestamp()
            ) {
                $instruments->removeElement($instrument);
            }
        }

        if ($instruments->count() === 0) {
            return null;
        }

        $bonds = $this->getBonds();

        $tiInterval = $this->convertIntervalToBrokerInterval($interval);

        /** @var TinkoffInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $candles = $this->client->getMarketData()->getCandles(
                $from,
                $to,
                $tiInterval,
                $instrument->getInstrumentId()
            );

            [$rate, $currency] = $this->calculateRateByInstrument($instrument);

            /** @var HistoricCandle $candle */
            foreach ($candles->getIterator() as $candle) {
                $candleModel = new BrokerCandleModel([
                    'open' => $rate * $this->tinkoffHelper->convertQuotationToFloat($candle->getOpen()),
                    'close' => $rate * $this->tinkoffHelper->convertQuotationToFloat($candle->getClose()),
                    'high' => $rate * $this->tinkoffHelper->convertQuotationToFloat($candle->getHigh()),
                    'low' => $rate * $this->tinkoffHelper->convertQuotationToFloat($candle->getLow()),
                    'volume' => $candle->getVolume(),
                    'time' => $this->tinkoffHelper->convertGoogleTimestampToDate($candle->getTime()),
                    'isComplete' => $candle->getIsComplete(),
                    'currency' => $currency,
                    'interval' => $interval->value
                ]);
                $candleCollection->add($candleModel);
            }
        }

        return $candleCollection->filterMainCandles();
    }

    /**
     * @param array $isinList
     * @return BrokerMoneyModel[]
     * @throws TIException
     */
    public function getLastPrices(array $isinList): array
    {
        $result = [];
        $marketInstruments = [];
        $instrumentIdList = [];
        foreach ($isinList as $isin) {
            $instrument = $this->marketTinkoffService->getByIsinOrTicker($isin);
            $marketInstruments[$isin] = $instrument;
            $instrumentIdList = array_merge($instrumentIdList, $instrument->getInstrumentIdList());
        }

        $priceArray = [];
        $priceCollection = $this->client->getMarketData()->getLastPrices($instrumentIdList);
        /** @var LastPrice $price */
        foreach ($priceCollection->getIterator() as $price) {
            $priceArray[$price->getInstrumentUid()] = $price;
        }

        foreach ($isinList as $isin) {
            $instrumentList = $marketInstruments[$isin];
            $time = 0;
            $price = null;

            /** @var TinkoffInstrumentModel $instrument */
            foreach ($instrumentList->getIterator() as $instrument) {
                if (
                    isset($priceArray[$instrument->getInstrumentId()])
                    && $priceArray[$instrument->getInstrumentId()]->getTime() !== null
                    && $priceArray[$instrument->getInstrumentId()]->getTime()->getSeconds() > $time
                ) {
                    $price = [
                        'amount' => $priceArray[$instrument->getInstrumentId()],
                        'currency' => $instrument->getCurrency()
                    ];
                }
            }

            if ($price === null) {
                $result[$isin] = null;
            } else {
                $rate = 1;
                $currency = $price['currency'];
                /** @var TinkoffInstrumentModel $instrument */
                $instrument = $marketInstruments[$isin]->first();
                if ($instrument) {
                    [$rate, $currency] = $this->calculateRateByInstrument($instrument);
                }
                $stockPrice = $rate * $this->tinkoffHelper->convertQuotationToFloat($price['amount']->getPrice());

                $result[$isin] = new BrokerMoneyModel([
                    'amount' => $stockPrice,
                    'currency' => $currency
                ]);
            }
        }

        return $result;
    }

    public function getShares(): ShareCollection
    {
        $brokerInstruments = $this->client->getInstruments()->getShares();

        $collection = new ShareCollection();
        /** @var Share $share */
        foreach ($brokerInstruments->getIterator() as $share) {
            $this->getOrAddInstrument($share->getUid(), $share->getIsin());

            $instrument = new BrokerInstrumentModel([
                'currency' => $this->tinkoffHelper->getCurrencyISO($share->getCurrency()),
                'ticker' => $share->getTicker(),
                'name' => $share->getName(),
                'isin' => $share->getIsin(),
                'minPriceIncrement' => $this->tinkoffHelper->convertQuotationToFloat($share->getMinPriceIncrement()),
                'lot' => $share->getLot(),
                'type' => 'Stock'
            ]);
            $data = [
                'sector' => $this->getSector($share->getSector()),
                'country' => $share->getCountryOfRisk(),
                'industry' => null,
                'instrument' => $instrument
            ];
            $share = new BrokerShareModel($data);
            $collection->add($share);
        }
        return $collection;
    }

    public function getShareByIsin(string $isin): ?BrokerShareModel
    {
        $stock = null;

        $tiInstrumentCollection = $this->marketTinkoffService->getByIsinOrTicker($isin);

        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstrumentCollection->getIterator() as $tiInstrument) {
            try {
                $stock = $this->getShareByUId($tiInstrument->getInstrumentId());
                if ($stock !== null) {
                    $this->marketTinkoffService->resetNotFoundAttempts($tiInstrument->getInstrumentId());
                    break;
                }
            } catch (TIException $exception) {
                if ($exception->getCode() === 50002) {
                    $this->marketTinkoffService->increaseNotFoundAttempt($tiInstrument->getInstrumentId());
                } else {
                    throw new TIException($exception->getMessage(), $exception->getCode());
                }
            }
        }

        return $stock;
    }

    private function getShareByUId(string $instrumentId): ?BrokerShareModel
    {
        $share = $this->client->getInstruments()->getShareBy(
            InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
            null,
            $instrumentId
        );
        if (null === $share) {
            return null;
        }

        $instrument = new BrokerInstrumentModel([
            'currency' => $this->tinkoffHelper->getCurrencyISO($share->getCurrency()),
            'ticker' => $share->getTicker(),
            'name' => $share->getName(),
            'isin' => $share->getIsin(),
            'minPriceIncrement' => $this->tinkoffHelper->convertQuotationToFloat($share->getMinPriceIncrement()),
            'lot' => $share->getLot(),
            'type' => 'Stock'
        ]);
        return new BrokerShareModel([
            'sector' => $this->getSector($share->getSector()),
            'country' => $share->getCountryOfRisk(),
            'industry' => null,
            'instrument' => $instrument
        ]);
    }

    public function getBondByIsin(string $isin): ?BrokerBondModel
    {
        $bond = null;

        $tiInstrumentCollection = $this->marketTinkoffService->getByIsinOrTicker($isin);
        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstrumentCollection->getIterator() as $tiInstrument) {
            try {
                $tiBond = $this->client->getInstruments()->getBondBy(
                    InstrumentIdType::INSTRUMENT_ID_TYPE_POSITION_UID,
                    '',
                    $tiInstrument->getPositionId()
                );
                if ($tiBond !== null) {
                    $bond = $this->convertBondResponseToModel($tiBond);
                    $this->marketTinkoffService->resetNotFoundAttempts($tiInstrument->getInstrumentId());
                    break;
                }
            } catch (TIException $exception) {
                if ($exception->getCode() === 50002) {
                    $this->marketTinkoffService->increaseNotFoundAttempt($tiInstrument->getInstrumentId());
                } else {
                    throw new TIException($exception->getMessage(), $exception->getCode());
                }
            }
        }

        return $bond;
    }

    private function getBondByUid(string $uid): ?BrokerBondModel
    {
        try {
            $bond = $this->client->getInstruments()->getBondBy(
                InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
                '',
                $uid
            );
        } catch (TIException $e) {
            return null;
        }

        return $this->convertBondResponseToModel($bond);
    }

    public function getFutures(): FutureCollection
    {
        $collection = new FutureCollection();

        $futures = $this->client->getInstruments()->getFutures(2);
        foreach ($futures->getIterator() as $future) {
            $this->getOrAddInstrument($future->getUid());
            $collection->add($this->convertFutureResponseToModel($future));
        }

        return $collection;
    }

    public function getFutureByIsin(string $isin): ?BrokerFutureModel
    {
        $future = null;

        $tiInstrumentCollection = $this->marketTinkoffService->getByIsinOrTicker($isin);
        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstrumentCollection->getIterator() as $tiInstrument) {
            try {
                $tiFuture = $this->client->getInstruments()->getFuturesBy(
                    InstrumentIdType::INSTRUMENT_ID_TYPE_POSITION_UID,
                    '',
                    $tiInstrument->getPositionId()
                );
                if ($tiFuture !== null) {
                    $future = $this->convertFutureResponseToModel($tiFuture);
                    $this->marketTinkoffService->resetNotFoundAttempts($tiInstrument->getInstrumentId());
                    break;
                }
            } catch (TIException $exception) {
                if ($exception->getCode() === 50002) {
                    $this->marketTinkoffService->increaseNotFoundAttempt($tiInstrument->getInstrumentId());
                } else {
                    throw new TIException($exception->getMessage(), $exception->getCode());
                }
            }
        }

        return $future;
    }


    private function getFutureByUid($uid): ?BrokerFutureModel
    {
        try {
            $future = $this->client->getInstruments()->getFuturesBy(
                InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
                null,
                $uid
            );
            if ($future !== null) {
                $this->getOrAddInstrument($future->getUid());
            }
        } catch (TIException $e) {
            return null;
        }

        return $this->convertFutureResponseToModel($future);
    }

    public function getBonds(): BondCollection
    {
        if (null !== $this->bonds) {
            return $this->bonds;
        }

        $bonds = $this->client->getInstruments()->getBonds();
        $this->bonds = new BondCollection();
        /** @var Bond $tiBond */
        foreach ($bonds->getIterator() as $tiBond) {
            $this->getOrAddInstrument($tiBond->getUid(), $tiBond->getIsin());

            $bond = $this->convertBondResponseToModel($tiBond);
            $this->bonds->add($bond);
        }

        return $this->bonds;
    }

    public function getBondCouponsIsin(string $isin, \DateTime $from, \DateTime $to): ?BondCouponCollection
    {
        $tiInstruments = $this->marketTinkoffService->getByIsinOrTicker($isin);

        $couponList = null;
        /** @var TinkoffInstrumentModel $tiInstrument */
        foreach ($tiInstruments->getIterator() as $tiInstrument) {
            try {
                $couponList = $this->getBondCoupons($tiInstrument->getInstrumentId(), $from, $to);
                $this->marketTinkoffService->resetNotFoundAttempts($tiInstrument->getInstrumentId());
                break;
            } catch (TIException $exception) {
                if ($exception->getCode() === 50002) {
                    $this->marketTinkoffService->increaseNotFoundAttempt($tiInstrument->getInstrumentId());
                } else {
                    throw new TIException($exception->getMessage(), $exception->getCode());
                }
            }
        }

        return $couponList;
    }

    public function setInstrumentsForUpdate(int $userId): void
    {
        $instruments = $this->marketTinkoffService->getInstruments();
        /** @var TinkoffInstrumentModel $instrument */
        foreach ($instruments as $instrument) {
            $this->amqpClient->publishMessage([
                'type' => 'updateMarketInstrument',
                'data' => [
                    'userId' => $userId,
                    'broker' => self::BROKER,
                    'instrumentId' => $instrument->getInstrumentId()
                ]
            ]);
        }
    }

    public function updateInstrument(string $instrumentId): ?BrokerInstrumentModel
    {
        $instrument = $this->marketTinkoffService->getByInstrumentId($instrumentId);

        $tiInstrument = $this->client->getInstruments()->getInstrumentBy(
            InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
            null,
            $instrument->getInstrumentId()
        );

        $instrument->setLogo($this->marketTinkoffService->getLogo($tiInstrument->getBrand()->getLogoName()));

        $instrumentModel = $this->marketTinkoffService->addTinkoffInstrument($instrument);

        return new BrokerInstrumentModel([
            'ticker' => $instrumentModel->getTicker(),
            'isin' => $instrumentModel->getIsin(),
            'exchange' => $instrumentModel->getExchange(),
            'lot' => $instrumentModel->getLot(),
            'minPriceIncrement' => $instrumentModel->getMinPriceIncrement(),
            'currency' => $instrumentModel->getCurrency(),
            'name' => $instrumentModel->getName(),
            'type' => $instrumentModel->getInstrumentType(),
            'logo' => null,
            'regNumber' => null
        ]);
    }

    public function getIndicatives(): InstrumentCollection
    {
        $result = new InstrumentCollection();
        $indicatives = $this->client->getInstruments()->getIndicatives();

        /** @var IndicativeResponse $indicative */
        foreach ($indicatives->getIterator() as $indicative) {
            $instrument = $this->getOrAddInstrument($indicative->getUid());
            $result->add($instrument);
        }

        return $result;
    }

    private function getBondCoupons(string $instrumentId, \DateTime $from, \DateTime $to): BondCouponCollection
    {
        $tinkoffInstrument = $this->marketTinkoffService->getByInstrumentId($instrumentId);

        $collection = new BondCouponCollection();
        $coupons = $this->client->getInstruments()->getBondCoupons($instrumentId, $from, $to);

        /** @var Coupon $coupon */
        foreach ($coupons->getIterator() as $coupon) {
            if ($coupon->getPayOneBond()->getCurrency() === '') {
                continue;
            }
            $model = new BrokerBondCouponModel([
                'isin' => $tinkoffInstrument->getIsin(),
                'date' => $this->tinkoffHelper->convertGoogleTimestampToDate($coupon->getCouponDate()),
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($coupon->getPayOneBond()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($coupon->getPayOneBond()->getCurrency())
            ]);
            $collection->add($model);
        }

        return $collection;
    }

    public function getOrAddInstrument(string $uid, ?string $isin = null, ?string $figi = null): TinkoffInstrumentModel
    {
        $deprecatedIsinList = [];
        if ($isin === null) {
            $instrument = $this->marketTinkoffService->getByInstrumentId($uid);
        } else {
            $instrumentCollection = $this->marketTinkoffService->getListByUid($uid);

            $instrument = null;
            /** @var TinkoffInstrumentModel $uidInstrument */
            foreach ($instrumentCollection as $uidInstrument) {
                if ($uidInstrument->getIsin() !== $isin) {
                    $uidInstrument->setIsActive(false);
                    $this->marketTinkoffService->addTinkoffInstrument($uidInstrument);
                    $deprecatedIsinList[] = $uidInstrument->getIsin();
                } else {
                    $instrument = $uidInstrument;
                }
            }
        }

        if ($instrument === null) {
            try {
                $tiInstrument = $this->client->getInstruments()->getInstrumentBy(
                    InstrumentIdType::INSTRUMENT_ID_TYPE_UID,
                    null,
                    $uid
                );
            } catch (TIException $e) {
                if ($figi !== null && $figi !== '') {
                    $tiInstrument = $this->client->getInstruments()->getInstrumentBy(
                        InstrumentIdType::INSTRUMENT_ID_TYPE_FIGI,
                        null,
                        $figi
                    );
                    $tiInstrument->setUid($uid);
                } else {
                    throw $e;
                }
            }
            $instrument = $this->marketTinkoffService->addInstrument($tiInstrument);
        }

        if (count($deprecatedIsinList)) {
            foreach ($deprecatedIsinList as $deprecatedIsis) {
                $changedIsin = new MarketIsinChangedEntity();
                $changedIsin->setDeprecatedIsin($deprecatedIsis);
                $changedIsin->setActualIsin($instrument->getIsin());
                $changedIsin->setDateCreated(new \DateTime());
                $changedIsin->setIsProcessed(0);
                $this->marketIsinChangedStorage->addEntity($changedIsin);
            }
        }

        return $instrument;
    }

    private function convertBondResponseToModel(Bond $bond): BrokerBondModel
    {
        return new BrokerBondModel([
            'isin' => $bond->getIsin(),
            'nominal' => new BrokerMoneyModel([
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($bond->getNominal()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($bond->getNominal()->getCurrency())
            ]),
            'initialNominal' => new BrokerMoneyModel([
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($bond->getInitialNominal()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($bond->getInitialNominal()->getCurrency())
            ]),
            'maturityDate' => $bond->getMaturityDate() === null
                ? null
                : $this->tinkoffHelper->convertGoogleTimestampToDate($bond->getMaturityDate())
        ]);
    }

    private function convertFutureResponseToModel(Future $future): BrokerFutureModel
    {
        $deliveryType = match ($future->getFuturesType()) {
            'DELIVERY_TYPE_PHYSICAL_DELIVERY' => 'physical_delivery',
            'DELIVERY_TYPE_CASH_SETTLEMENT' => 'cash_settlement',
            default => null
        };
        $assetType = match ($future->getAssetType()) {
            'TYPE_CURRENCY' => 'currency',
            'TYPE_COMMODITY' => 'commodity',
            'TYPE_SECURITY' => 'security',
            'TYPE_INDEX' => 'index',
            default => null
        };

        return new BrokerFutureModel([
            'firstTradeDate' => $this->tinkoffHelper->convertGoogleTimestampToDate($future->getFirstTradeDate()),
            'lastTradeDate' => $this->tinkoffHelper->convertGoogleTimestampToDate($future->getLastTradeDate()),
            'futureType' => $deliveryType,
            'assetType' => $assetType,
            'initialMarginOnBuy' => new BrokerMoneyModel([
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($future->getInitialMarginOnBuy()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($future->getInitialMarginOnBuy()->getCurrency())
            ]),
            'initialMarginOnSell' => new BrokerMoneyModel([
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($future->getInitialMarginOnSell()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($future->getInitialMarginOnSell()->getCurrency())
            ]),
            'minPriceIncrement' => $this->tinkoffHelper->convertQuotationToFloat($future->getMinPriceIncrement()),
            'minPriceIncrementAmount' => $this->tinkoffHelper
                ->convertQuotationToFloat($future->getMinPriceIncrementAmount()),
            'ticker' => $future->getTicker(),
            'expirationDate' => $this->tinkoffHelper->convertGoogleTimestampToDate($future->getExpirationDate()),
        ]);
    }

    /**
     * @param TinkoffInstrumentModel $instrument
     * @return array{
     *     rate: float,
     *     currency: string
     * }
     */
    private function calculateRateByInstrument(TinkoffInstrumentModel $instrument): array
    {
        $currency = $instrument->getCurrency();
        $rate = 1;
        switch ($instrument->getInstrumentType()) {
            case 'bond':
                $bond = $this->getBondByUid($instrument->getInstrumentId());
                if ($bond !== null) {
                    $rate = $bond->getNominal()->getAmount() / 100;
                    $currency = $bond->getNominal()->getCurrency();
                }
                break;
            case 'futures':
                $future = $this->getFutureByUid($instrument->getInstrumentId());
                if (
                    $future !== null
                    && $future->getMinPriceIncrementAmount() !== null
                    && $future->getMinPriceIncrement() !== null
                ) {
                    $rate = $future->getMinPriceIncrementAmount() / $future->getMinPriceIncrement();
                }
                break;
        }
        return [
            $rate,
            $this->tinkoffHelper->getCurrencyISO($currency)
        ];
    }

    private function getSector(string $sector): string
    {
        return match ($sector) {
            'basic_materials', 'materials' => 'Basic Materials',
            'consumer', 'electrocars' => 'Consumer Cyclical',
            'financial_services', 'financial' => 'Financial Services',
            'real_estate' => 'Real Estate',
            'consumer_defensive' => 'Consumer Defensive',
            'health_care' => 'Health Care',
            'utilities' => 'Utilities',
            'communication_services', 'telecom' => 'Communication Services',
            'energy' => 'Energy',
            'industrials', 'ecomaterials', 'green_buildings' => 'Industrials',
            'it', 'green_energy' => 'Technology',
            default => 'Other'
        };
    }

    private function convertIntervalToBrokerInterval(CandleIntervalType $interval): int
    {
        return match ($interval) {
            CandleIntervalType::MIN_1 => 1,
            CandleIntervalType::MIN_2 => 6,
            CandleIntervalType::MIN_3 => 7,
            CandleIntervalType::MIN_5 => 2,
            CandleIntervalType::MIN_10 => 8,
            CandleIntervalType::MIN_15 => 3,
            CandleIntervalType::MIN_30 => 9,
            CandleIntervalType::HOUR_1 => 4,
            CandleIntervalType::HOUR_2 => 10,
            CandleIntervalType::HOUR_4 => 11,
            CandleIntervalType::DAY_1 => 5,
            CandleIntervalType::WEEK_1 => 12,
            CandleIntervalType::MONTH_1 => 13,
            default => throw new BrokerAdapterException('Unknown candle interval type', 1072)
        };
    }
}
