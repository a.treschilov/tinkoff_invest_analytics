<?php

declare(strict_types=1);

use App\Adapters\MoexAdapter;
use App\Adapters\Services\BrokerIterator;
use App\Adapters\Services\MarketMoexService;
use App\Adapters\Services\MarketTinkoffService;
use App\Adapters\Storages\MarketIsinChangedStorage;
use App\Adapters\Storages\MarketMoexStorage;
use App\Adapters\TIClientV2Adapter;
use App\Broker\Services\Adapters\TinkoffHelper;
use App\Common\Amqp\AmqpClient;
use App\Common\HttpClient;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;
use App\Adapters\Storages\MarketTinkoffStorage;

/** @var ContainerInterface $container */
$container->set(MarketTinkoffStorage::class, function (ContainerInterface $c) {
    return new MarketTinkoffStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketIsinChangedStorage::class, function (ContainerInterface $c) {
    return new MarketIsinChangedStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketTinkoffService::class, function (ContainerInterface $c) {
    return new MarketTinkoffService(
        $c->get(MarketTinkoffStorage::class)
    );
});

$container->set(TIClientV2Adapter::class, function (ContainerInterface $c) {
    $tiClient = new TIClient(getenv('TINKOFF_INVEST_API_KEY'), ['isRateLimitRetry' => true]);

    return new TIClientV2Adapter(
        $tiClient,
        $c->get(TinkoffHelper::class),
        $c->get(MarketTinkoffService::class),
        $c->get(MarketIsinChangedStorage::class),
        $c->get(AmqpClient::class)
    );
});

$container->set('MOEX_API' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://iss.moex.com'
    ]);
    return new HttpClient($client);
});

$container->set(MoexAdapter::class, function (ContainerInterface $c) {
    return new MoexAdapter(
        $c->get('MOEX_API' . HttpClient::class),
        $c->get(MarketMoexService::class),
        $c->get(AmqpClient::class)
    );
});

$container->set(MarketMoexService::class, function (ContainerInterface $c) {
    return new MarketMoexService(
        $c->get(MarketMoexStorage::class)
    );
});

$container->set(MarketMoexStorage::class, function (ContainerInterface $c) {
    return new MarketMoexStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(BrokerIterator::class, function (ContainerInterface $c) {
    return new BrokerIterator(
        $c->get(TIClientV2Adapter::class),
        $c->get(MoexAdapter::class)
    );
});
