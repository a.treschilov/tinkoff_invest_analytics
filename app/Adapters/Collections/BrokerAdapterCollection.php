<?php

declare(strict_types=1);

namespace App\Adapters\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class BrokerAdapterCollection extends ArrayCollection
{
}
