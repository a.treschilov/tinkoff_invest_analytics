<?php

declare(strict_types=1);

namespace App\Adapters\Collections;

use App\Adapters\Models\TinkoffInstrumentModel;
use Doctrine\Common\Collections\ArrayCollection;

class MarketTinkoffCollection extends ArrayCollection
{
    public function getInstrumentIdList(): array
    {
        $result = [];
        /** @var TinkoffInstrumentModel $instrument */
        foreach ($this->getIterator() as $instrument) {
            $result[] = $instrument->getInstrumentId();
        }
        return $result;
    }

    public function filterByIsin(string $isin): MarketTinkoffCollection
    {
        $collection = new MarketTinkoffCollection();
        foreach ($this->getIterator() as $instrument) {
            if ($instrument->getIsin() === $isin) {
                $collection->add($instrument);
            }
        }

        return $collection;
    }
}
