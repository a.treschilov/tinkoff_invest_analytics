<?php

declare(strict_types=1);

namespace App\Adapters\Models;

use App\Adapters\Types\MoexInstrumentType;
use App\Common\Models\BaseModel;

class MoexInstrumentModel extends BaseModel
{
    private ?int $marketMoexId = null;
    private string $ticker;
    private ?string $isin = null;
    private string $name;
    private MoexInstrumentType $instrumentType;
    private ?string $currency = null;
    private float $minPriceIncrement;
    private ?string $regNumber = null;
    private bool $isActive;

    public function getMarketMoexId(): ?int
    {
        return $this->marketMoexId;
    }

    public function setMarketMoexId(?int $marketMoexId): void
    {
        $this->marketMoexId = $marketMoexId;
    }

    public function getTicker(): string
    {
        return $this->ticker;
    }

    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    public function getIsin(): ?string
    {
        return $this->isin;
    }

    public function setIsin(?string $isin): void
    {
        $this->isin = $isin;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getInstrumentType(): MoexInstrumentType
    {
        return $this->instrumentType;
    }

    public function setInstrumentType(MoexInstrumentType $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    public function getMinPriceIncrement(): float
    {
        return $this->minPriceIncrement;
    }

    public function setMinPriceIncrement(float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): void
    {
        $this->regNumber = $regNumber;
    }
}
