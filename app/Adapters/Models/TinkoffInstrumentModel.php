<?php

declare(strict_types=1);

namespace App\Adapters\Models;

use App\Common\Models\BaseModel;

class TinkoffInstrumentModel extends BaseModel
{
    private int $marketTinkoffId;
    private string $ticker;
    private string $exchange;
    private string $isin;
    private ?string $classCode = null;
    private string $instrumentId;
    private string $positionId;
    private ?float $minPriceIncrement = null;
    private int $lot;
    private string $currency;
    private string $name;
    private ?string $logo = null;
    private ?\DateTime $firstCandleDate = null;
    private string $instrumentType;
    private bool $isActive = true;

    /**
     * @return int
     */
    public function getMarketTinkoffId(): int
    {
        return $this->marketTinkoffId;
    }

    /**
     * @param int $marketTinkoffId
     */
    public function setMarketTinkoffId(int $marketTinkoffId): void
    {
        $this->marketTinkoffId = $marketTinkoffId;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return string|null
     */
    public function getClassCode(): ?string
    {
        return $this->classCode;
    }

    /**
     * @param string|null $classCode
     */
    public function setClassCode(?string $classCode): void
    {
        $this->classCode = $classCode;
    }

    /**
     * @return string
     */
    public function getInstrumentId(): string
    {
        return $this->instrumentId;
    }

    /**
     * @param string $instrumentId
     */
    public function setInstrumentId(string $instrumentId): void
    {
        $this->instrumentId = $instrumentId;
    }

    /**
     * @return string
     */
    public function getPositionId(): string
    {
        return $this->positionId;
    }

    /**
     * @param string $positionId
     */
    public function setPositionId(string $positionId): void
    {
        $this->positionId = $positionId;
    }

    /**
     * @return float|null
     */
    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float|null $minPriceIncrement
     */
    public function setMinPriceIncrement(?float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return int
     */
    public function getLot(): int
    {
        return $this->lot;
    }

    /**
     * @param int $lot
     */
    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime|null
     */
    public function getFirstCandleDate(): ?\DateTime
    {
        return $this->firstCandleDate;
    }

    /**
     * @param \DateTime|null $firstCandleDate
     */
    public function setFirstCandleDate(?\DateTime $firstCandleDate): void
    {
        $this->firstCandleDate = $firstCandleDate;
    }

    /**
     * @return string
     */
    public function getInstrumentType(): string
    {
        return $this->instrumentType;
    }

    /**
     * @param string $instrumentType
     */
    public function setInstrumentType(string $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @param string $exchange
     */
    public function setExchange(string $exchange): void
    {
        $this->exchange = $exchange;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return $this->logo;
    }

    /**
     * @param string|null $logo
     */
    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }
}
