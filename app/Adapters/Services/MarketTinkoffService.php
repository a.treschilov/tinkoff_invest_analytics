<?php

declare(strict_types=1);

namespace App\Adapters\Services;

use App\Adapters\Collections\MarketTinkoffCollection;
use App\Adapters\Entities\MarketTinkoffEntity;
use App\Adapters\Models\TinkoffInstrumentModel;
use App\Adapters\Storages\MarketTinkoffStorage;
use Google\Protobuf\Timestamp;
use Tinkoff\Invest\V1\Instrument;
use Tinkoff\Invest\V1\Quotation;

class MarketTinkoffService
{
    private const LOGO_PATH = 'https://invest-brands.cdn-tinkoff.ru';

    public function __construct(
        private readonly MarketTinkoffStorage $marketTinkoffStorage
    ) {
    }

    public function addTinkoffInstrument(TinkoffInstrumentModel $instrument): ?TinkoffInstrumentModel
    {
        $entity = $this->convertInstrumentModelToEntity($instrument);
        $entity = $this->marketTinkoffStorage->addEntity($entity);
        return $this->convertInstrumentEntityToModel($entity);
    }

    public function addInstrument(Instrument $instrument): TinkoffInstrumentModel
    {
        $date = $instrument->getFirst1MinCandleDate() === null
            ? null
            : $this->convertGoogleTimestampToDate($instrument->getFirst1MinCandleDate());

        $entity = new MarketTinkoffEntity();
        $entity->setTicker($instrument->getTicker());
        $entity->setIsin($instrument->getIsin() === "" ? null : $instrument->getIsin());
        $entity->setClassCode($instrument->getClassCode());
        $entity->setExchange($instrument->getExchange());
        $entity->setInstrumentId($instrument->getUid());
        $entity->setPositionId($instrument->getPositionUid());
        $entity->setMinPriceIncrement($this->convertQuotationToFloat($instrument->getMinPriceIncrement()));
        $entity->setLot($instrument->getLot());
        $entity->setCurrency($instrument->getCurrency());
        $entity->setName($instrument->getName());
        $entity->setLogo($this->getLogo($instrument->getBrand()->getLogoName()));
        $entity->setInstrumentType($instrument->getInstrumentType());
        $entity->setFirstCandleDate($date);
        $entity->setIsActive(1);

        $result = $this->marketTinkoffStorage->addEntity($entity);
        return $this->convertInstrumentEntityToModel($result);
    }

    public function getByIsinOrTicker(string $id): MarketTinkoffCollection
    {
        $result = new MarketTinkoffCollection();

        $realId = explode('.', $id);

        if (count($realId) === 1) {
            $instruments = $this->marketTinkoffStorage->getByIsin($realId[0]);

            if (count($instruments) === 0) {
                $instruments = $this->marketTinkoffStorage->getByTicker($realId[0]);
            }
        } else {
            $instruments = $this->marketTinkoffStorage->getByTickerAndExchange($realId[0], $realId[1]);
        }

        foreach ($instruments as $instrument) {
            $result->add($this->convertInstrumentEntityToModel($instrument));
        }

        return $result;
    }

    public function getByInstrumentId(string $instrumentId): ?TinkoffInstrumentModel
    {
        $entity = $this->marketTinkoffStorage->getByInstrumentId($instrumentId);

        if ($entity === null) {
            return null;
        }

        return $this->convertInstrumentEntityToModel($entity);
    }

    public function getListByUid(string $uid): MarketTinkoffCollection
    {
        $collection = new MarketTinkoffCollection();
        $entities = $this->marketTinkoffStorage->getListByUid($uid);

        foreach ($entities as $entity) {
            $collection->add($this->convertInstrumentEntityToModel($entity));
        }

        return $collection;
    }

    public function increaseNotFoundAttempt(string $instrumentId): void
    {
        $maxFailedAttempts = 5;
        $instrument = $this->marketTinkoffStorage->getByInstrumentId($instrumentId);
        $instrument->setNotFoundAttempts($instrument->getNotFoundAttempts() + 1);
        if ($instrument->getNotFoundAttempts() >= $maxFailedAttempts) {
            $instrument->setIsActive(0);
        }
        $this->marketTinkoffStorage->addEntity($instrument);
    }

    public function resetNotFoundAttempts(string $instrumentId): void
    {
        $instrument = $this->marketTinkoffStorage->getByInstrumentId($instrumentId);
        if ($instrument->getNotFoundAttempts() > 0) {
            $instrument->setNotFoundAttempts(0);
            $this->marketTinkoffStorage->addEntity($instrument);
        }
    }

    public function getInstruments(): MarketTinkoffCollection
    {
        $result = new MarketTinkoffCollection();
        $instruments = $this->marketTinkoffStorage->getActiveInstruments();
        foreach ($instruments as $instrument) {
            $result->add($this->convertInstrumentEntityToModel($instrument));
        }
        return $result;
    }

    private function convertInstrumentEntityToModel(MarketTinkoffEntity $entity): TinkoffInstrumentModel
    {
        return new TinkoffInstrumentModel([
            'marketTinkoffId' => $entity->getId(),
            'ticker' => $entity->getTicker(),
            'isin' => $entity->getIsin() ?: $entity->getTicker(),
            'classCode' => $entity->getClassCode(),
            'exchange' => $entity->getExchange(),
            'instrumentId' => $entity->getInstrumentId(),
            'positionId' => $entity->getPositionId(),
            'minPriceIncrement' => $entity->getMinPriceIncrement(),
            'lot' => $entity->getLot(),
            'currency' => $entity->getCurrency(),
            'name' => $entity->getName(),
            'logo' => $entity->getLogo(),
            'firstCandleDate' => $entity->getFirstCandleDate(),
            'instrumentType' => $entity->getInstrumentType(),
            'isActive' => (bool)$entity->getIsActive()
        ]);
    }

    private function convertInstrumentModelToEntity(TinkoffInstrumentModel $model): MarketTinkoffEntity
    {
        if ($model->getMarketTinkoffId() !== null) {
            $entity = $this->marketTinkoffStorage->getActiveById($model->getMarketTinkoffId());
        } else {
            $entity = new MarketTinkoffEntity();
        }

        $entity->setTicker($model->getTicker());
        $entity->setIsin($model->getIsin());
        $entity->setClassCode($model->getClassCode());
        $entity->setExchange($model->getExchange());
        $entity->setInstrumentId($model->getInstrumentId());
        $entity->setPositionId($model->getPositionId());
        $entity->setMinPriceIncrement($model->getMinPriceIncrement());
        $entity->setLot($model->getLot());
        $entity->setCurrency($model->getCurrency());
        $entity->setName($model->getName());
        $entity->setLogo($model->getLogo());
        $entity->setFirstCandleDate($model->getFirstCandleDate());
        $entity->setInstrumentType($model->getInstrumentType());
        $entity->setIsActive($model->isActive() ? 1 : 0);

        return $entity;
    }

    public function getLogo(string $logo): string
    {
        $logoData = explode('.png', $logo);
        return self::LOGO_PATH . '/' . $logoData[0] . 'x160.png';
    }

    private function convertQuotationToFloat(?Quotation $quotation): ?float
    {
        if (null === $quotation) {
            return null;
        }
        return $quotation->getUnits() + $quotation->getNano() / pow(10, 9);
    }

    private function convertGoogleTimestampToDate(Timestamp $date): \DateTime
    {
        return new \DateTime('@' . $date->getSeconds());
    }
}
