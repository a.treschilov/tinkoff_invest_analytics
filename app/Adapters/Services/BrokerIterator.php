<?php

declare(strict_types=1);

namespace App\Adapters\Services;

use App\Adapters\Collections\BrokerAdapterCollection;
use App\Broker\Collections\BondCollection;
use App\Broker\Collections\BondCouponCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\FutureCollection;
use App\Broker\Collections\InstrumentCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerFutureModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerShareModel;
use App\Collections\CandleCollection;
use App\Market\Types\CandleIntervalType;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

readonly class BrokerIterator implements SourceBrokerInterface
{
    private BrokerAdapterCollection $brokerCollection;

    public function __construct(
        SourceBrokerInterface $tiClientV2Adapter,
        SourceBrokerInterface $moexAdapter
    ) {
        $this->brokerCollection = new BrokerAdapterCollection([
            $tiClientV2Adapter,
            $moexAdapter
        ]);
    }

    #[\Override] public function getInstrumentByIsin(string $isin): InstrumentCollection
    {
        $instrument = new InstrumentCollection();
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $instrument = new InstrumentCollection(
                array_merge($instrument->toArray(), $broker->getInstrumentByIsin($isin)->toArray())
            );
        }
        return $instrument;
    }

    #[\Override] public function getCurrencyByIsin(string $isin): ?BrokerCurrencyModel
    {
        // TODO: Implement getCurrencyByIsin() method.
    }

    #[\Override] public function getCurrencies(): BrokerCurrencyCollection
    {
        // TODO: Implement getCurrencies() method.
    }

    #[\Override] public function getHistoryCandles(
        string $isin,
        \DateTime $from,
        \DateTime $to,
        CandleIntervalType $interval
    ): ?CandleCollection {
        $candles = null;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            try {
                $candles = $broker->getHistoryCandles($isin, $from, $to, $interval);
            } catch (TIException $e) {
                if ($e->getCode() === 50002) { // Instrument now found
                    $candles = null;
                } else {
                    throw $e;
                }
            }

            if ($candles !== null) {
                break;
            }
        }
        return $candles;
    }

    #[\Override] public function getLastPrices(array $isinList): array
    {
        $result = [];
        $isinListWithoutPrices = $isinList;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            if (count($isinListWithoutPrices) === 0) {
                break;
            }

            $brokerPrices = $broker->getLastPrices($isinListWithoutPrices);
            $result = array_merge($result, $brokerPrices);

            foreach ($brokerPrices as $isin => $price) {
                if (($price !== null && $key = array_search($isin, $isinListWithoutPrices)) !== false) {
                    unset($isinListWithoutPrices[$key]);
                }
            }
        }

        return $result;
    }

    #[\Override] public function getShares(): ShareCollection
    {
        $shares = new ShareCollection();
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $shares = $shares->merge($broker->getShares());
        }

        return $shares;
    }

    #[\Override] public function getShareByIsin(string $isin): ?BrokerShareModel
    {
        $share = null;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $share = $broker->getShareByIsin($isin);
            if ($share !== null) {
                break;
            }
        }

        return $share;
    }

    #[\Override] public function getBonds(): BondCollection
    {
        $bonds = new BondCollection();
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $bonds = $bonds->merge($broker->getBonds());
        }

        return $bonds;
    }

    #[\Override] public function getBondByIsin(string $isin): ?BrokerBondModel
    {
        $bond = null;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $bond = $broker->getBondByIsin($isin);
            if ($bond !== null) {
                break;
            }
        }

        return $bond;
    }

    #[\Override] public function getBondCouponsIsin(string $isin, \DateTime $from, \DateTime $to): ?BondCouponCollection
    {
        $collection = null;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $collection = $broker->getBondCouponsIsin($isin, $from, $to);
            if ($collection !== null) {
                break;
            }
        }

        if ($collection === null) {
            throw new TIException('Instrument not found', 50002);
        }

        return $collection;
    }

    #[\Override] public function setInstrumentsForUpdate(int $userId): void
    {
        // TODO: Implement setInstrumentsForUpdate() method.
    }

    #[\Override] public function updateInstrument(string $instrumentId): ?BrokerInstrumentModel
    {
        // TODO: Implement updateInstrument() method.
    }

    #[\Override] public function getIndicatives(): InstrumentCollection
    {
        // TODO: Implement getIndicatives() method.
    }

    public function getFutures(): FutureCollection
    {
        $futures = new FutureCollection();
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $futures = $futures->merge($broker->getFutures());
        }

        return $futures;
    }

    public function getFutureByIsin(string $isin): ?BrokerFutureModel
    {
        $future = null;
        /** @var SourceBrokerInterface $broker */
        foreach ($this->brokerCollection as $broker) {
            $future = $broker->getFutureByIsin($isin);
            if ($future !== null) {
                break;
            }
        }

        return $future;
    }
}
