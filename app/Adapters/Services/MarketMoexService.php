<?php

declare(strict_types=1);

namespace App\Adapters\Services;

use App\Adapters\Collections\MarketMoexCollection;
use App\Adapters\Entities\MarketMoexEntity;
use App\Adapters\Models\MoexInstrumentModel;
use App\Adapters\Storages\MarketMoexStorage;
use App\Adapters\Types\MoexInstrumentType;

readonly class MarketMoexService
{
    public function __construct(
        private MarketMoexStorage $marketMoexStorage
    ) {
    }

    public function getInstrumentByTicker(string $ticker): ?MoexInstrumentModel
    {
        $instrument = $this->marketMoexStorage->findActiveByTicker($ticker);

        if ($instrument === null) {
            return null;
        }

        return $this->convertEntityToModel($instrument);
    }

    public function addInstrument(MoexInstrumentModel $instrument): ?MoexInstrumentModel
    {
        $entity = $this->convertModelToEntity($instrument);
        $entity = $this->marketMoexStorage->addEntity($entity);

        return $this->convertEntityToModel($entity);
    }

    public function getInstruments(): MarketMoexCollection
    {
        $collection = new MarketMoexCollection();
        $instruments = $this->marketMoexStorage->findActive();
        foreach ($instruments as $instrument) {
            $collection->add($this->convertEntityToModel($instrument));
        }

        return $collection;
    }

    public function getInstrumentByIsin(string $isin): MarketMoexCollection
    {
        $collection = new MarketMoexCollection();
        $entities = $this->marketMoexStorage->findActiveByIsin($isin);
        if (count($entities) === 0) {
            $entity =  $this->marketMoexStorage->findActiveByTicker($isin);
            if ($entity !== null) {
                $entities[] = $entity;
            }
        }

        if (count($entities) === 0) {
            return $collection;
        }

        foreach ($entities as $entity) {
            $collection->add($this->convertEntityToModel($entity));
        }

        return $collection;
    }

    public function getInstrumentById(int $instrumentId): ?MoexInstrumentModel
    {
        $entity = $this->marketMoexStorage->findActiveById($instrumentId);

        return $entity === null ? null : $this->convertEntityToModel($entity);
    }

    private function convertEntityToModel(MarketMoexEntity $entity): MoexInstrumentModel
    {
        return new MoexInstrumentModel([
            'marketMoexId' => $entity->getId(),
            'ticker' => $entity->getTicker(),
            'isin' => $entity->getIsin(),
            'name' => $entity->getName(),
            'currency' => $entity->getCurrency(),
            'minPriceIncrement' => $entity->getMinPriceIncrement(),
            'instrumentType' => MoexInstrumentType::from($entity->getInstrumentType()),
            'regNumber' => $entity->getRegNumber(),
            'isActive' => (bool)$entity->getIsActive()
        ]);
    }

    private function convertModelToEntity(MoexInstrumentModel $model): MarketMoexEntity
    {
        if ($model->getMarketMoexId() === null) {
            $entity = new MarketMoexEntity();
        } else {
            $entity = $this->marketMoexStorage->findActiveById($model->getMarketMoexId());
        }

        if ($entity === null) {
            $entity = new MarketMoexEntity();
        }

        $entity->setTicker($model->getTicker());
        $entity->setIsin($model->getIsin());
        $entity->setName($model->getName());
        $entity->setCurrency($model->getCurrency() ?? "RUB");
        $entity->setInstrumentType($model->getInstrumentType()->value);
        $entity->setMinPriceIncrement($model->getMinPriceIncrement());
        $entity->setRegNumber($model->getRegNumber());
        $entity->setIsActive($model->isActive() ? 1 : 0);

        return $entity;
    }
}
