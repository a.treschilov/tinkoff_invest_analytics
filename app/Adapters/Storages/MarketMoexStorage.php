<?php

declare(strict_types=1);

namespace App\Adapters\Storages;

use App\Adapters\Entities\MarketMoexEntity;
use App\Adapters\Entities\MarketTinkoffEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketMoexStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function addEntity(MarketMoexEntity $entity): MarketMoexEntity
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    public function findActiveByTicker(string $ticker): ?MarketMoexEntity
    {
        return $this->entityManager->getRepository(MarketMoexEntity::class)
            ->findOneBy(['ticker' => $ticker, 'isActive' => 1]);
    }

    /**
     * @param string $isin
     * @return MarketMoexEntity[]
     */
    public function findActiveByIsin(string $isin): array
    {
        return $this->entityManager->getRepository(MarketMoexEntity::class)
            ->findBy(['isin' => $isin, 'isActive' => 1]);
    }

    public function findActiveById(int $instrumentId): ?MarketMoexEntity
    {
        return $this->entityManager->getRepository(MarketMoexEntity::class)
            ->findOneBy(['id' => $instrumentId, 'isActive' => 1]);
    }

    /**
     * @return MarketMoexEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(MarketMoexEntity::class)
            ->findBy(['isActive' => 1]);
    }
}
