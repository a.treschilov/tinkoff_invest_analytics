<?php

declare(strict_types=1);

namespace App\Adapters\Storages;

use App\Adapters\Entities\MarketTinkoffEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketTinkoffStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function addEntity(MarketTinkoffEntity $entity): MarketTinkoffEntity
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    public function getByInstrumentId(string $instrumentId): ?MarketTinkoffEntity
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findOneBy(['instrumentId' => $instrumentId, 'isActive' => 1]);
    }

    /**
     * @param string $uid
     * @return MarketTinkoffEntity[]
     */
    public function getListByUid(string $uid): array
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findBy(['instrumentId' => $uid, 'isActive' => 1]);
    }

    public function getActiveById(int $id): ?MarketTinkoffEntity
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findOneBy(['id' => $id, 'isActive' => 1]);
    }

    /**
     * @param string $isin
     * @return MarketTinkoffEntity[]
     */
    public function getByIsin(string $isin): array
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findBy(['isin' => $isin, 'isActive' => 1]);
    }


    /**
     * @param string $ticker
     * @return MarketTinkoffEntity[]
     */
    public function getByTicker(string $ticker): array
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findBy(['ticker' => $ticker, 'isActive' => 1]);
    }

    /**
     * @param string $ticker
     * @param string $exchange
     * @return MarketTinkoffEntity[]
     */
    public function getByTickerAndExchange(string $ticker, string $exchange): array
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findBy(['ticker' => $ticker, 'exchange' => $exchange, 'isActive' => 1]);
    }

    /**
     * @return MarketTinkoffEntity[]
     */
    public function getActiveInstruments(): array
    {
        return $this->entityManager->getRepository(MarketTinkoffEntity::class)
            ->findBy(['isActive' => 1]);
    }
}
