<?php

declare(strict_types=1);

namespace App\Adapters\Storages;

use App\Adapters\Entities\MarketIsinChangedEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketIsinChangedStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function addEntity(MarketIsinChangedEntity $entity): MarketIsinChangedEntity
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }
}
