<?php

declare(strict_types=1);

namespace App\Adapters\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_tinkoff')]
#[ORM\Entity]
class MarketTinkoffEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'market_tinkoff_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'ticker', type: 'string')]
    protected string $ticker;

    #[ORM\Column(name: 'isin', type: 'string')]
    protected ?string $isin = null;

    #[ORM\Column(name: 'class_code', type: 'string')]
    protected ?string $classCode = null;

    #[ORM\Column(name: 'exchange', type: 'string')]
    protected string $exchange;

    #[ORM\Column(name: 'instrument_id', type: 'string')]
    protected ?string $instrumentId = null;

    #[ORM\Column(name: 'position_id', type: 'string')]
    protected ?string $positionId = null;

    #[ORM\Column(name: 'min_price_increment', type: 'float')]
    protected ?float $minPriceIncrement = null;

    #[ORM\Column(name: 'lot', type: 'integer')]
    protected int $lot;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    #[ORM\Column(name: 'name', type: 'string')]
    protected string $name;

    #[ORM\Column(name: 'logo', type: 'string')]
    protected ?string $logo = null;

    #[ORM\Column(name: 'instrument_type', type: 'string')]
    protected string $instrumentType;

    #[ORM\Column(name: 'first_candle_date', type: 'datetimetz')]
    protected ?\DateTime $firstCandleDate = null;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    #[ORM\Column(name: 'not_found_attempts', type: 'integer')]
    protected int $notFoundAttempts = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    public function getIsin(): ?string
    {
        return $this->isin;
    }

    public function setIsin(?string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return string|null
     */
    public function getClassCode(): ?string
    {
        return $this->classCode;
    }

    /**
     * @param string|null $classCode
     */
    public function setClassCode(?string $classCode): void
    {
        $this->classCode = $classCode;
    }

    /**
     * @return string|null
     */
    public function getInstrumentId(): ?string
    {
        return $this->instrumentId;
    }

    /**
     * @param string|null $instrumentId
     */
    public function setInstrumentId(?string $instrumentId): void
    {
        $this->instrumentId = $instrumentId;
    }

    /**
     * @return string|null
     */
    public function getPositionId(): ?string
    {
        return $this->positionId;
    }

    /**
     * @param string|null $positionId
     */
    public function setPositionId(?string $positionId): void
    {
        $this->positionId = $positionId;
    }

    /**
     * @return float|null
     */
    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float|null $minPriceIncrement
     */
    public function setMinPriceIncrement(?float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return int
     */
    public function getLot(): int
    {
        return $this->lot;
    }

    /**
     * @param int $lot
     */
    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime|null
     */
    public function getFirstCandleDate(): ?\DateTime
    {
        return $this->firstCandleDate;
    }

    /**
     * @param \DateTime|null $firstCandleDate
     */
    public function setFirstCandleDate(?\DateTime $firstCandleDate): void
    {
        $this->firstCandleDate = $firstCandleDate;
    }

    /**
     * @return string
     */
    public function getInstrumentType(): string
    {
        return $this->instrumentType;
    }

    /**
     * @param string $instrumentType
     */
    public function setInstrumentType(string $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @param string $exchange
     */
    public function setExchange(string $exchange): void
    {
        $this->exchange = $exchange;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getNotFoundAttempts(): int
    {
        return $this->notFoundAttempts;
    }

    /**
     * @param int $notFoundAttempts
     */
    public function setNotFoundAttempts(int $notFoundAttempts): void
    {
        $this->notFoundAttempts = $notFoundAttempts;
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return $this->logo;
    }

    /**
     * @param string|null $logo
     */
    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }
}
