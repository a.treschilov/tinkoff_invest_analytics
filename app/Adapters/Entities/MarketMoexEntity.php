<?php

declare(strict_types=1);

namespace App\Adapters\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_moex')]
#[ORM\Entity]
class MarketMoexEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'market_moex_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'ticker', type: 'string')]
    protected string $ticker;

    #[ORM\Column(name: 'isin', type: 'string')]
    protected ?string $isin = null;

    #[ORM\Column(name: 'name', type: 'string')]
    protected string $name;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    #[ORM\Column(name: 'min_price_increment', type: 'float')]
    protected float $minPriceIncrement;

    #[ORM\Column(name: 'instrument_type', type: 'string')]
    protected string $instrumentType;

    #[ORM\Column(name: 'reg_number', type: 'string')]
    protected ?string $regNumber = null;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTicker(): string
    {
        return $this->ticker;
    }

    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    public function getIsin(): ?string
    {
        return $this->isin;
    }

    public function setIsin(?string $isin): void
    {
        $this->isin = $isin;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getInstrumentType(): string
    {
        return $this->instrumentType;
    }

    public function setInstrumentType(string $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getIsActive(): int
    {
        return $this->isActive;
    }

    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getMinPriceIncrement(): float
    {
        return $this->minPriceIncrement;
    }

    public function setMinPriceIncrement(float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): void
    {
        $this->regNumber = $regNumber;
    }
}
