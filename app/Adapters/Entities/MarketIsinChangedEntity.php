<?php

declare(strict_types=1);

namespace App\Adapters\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_isin_changed')]
#[ORM\Entity]
class MarketIsinChangedEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'market_isin_changed_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'deprecated_isin', type: 'string')]
    protected string $deprecatedIsin;

    #[ORM\Column(name: 'actual_isin', type: 'string')]
    protected string $actualIsin;

    #[ORM\Column(name: 'date_created', type: 'datetime')]
    protected \DateTime $dateCreated;

    #[ORM\Column(name: 'is_processed', type: 'integer')]
    protected int $isProcessed = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDeprecatedIsin(): string
    {
        return $this->deprecatedIsin;
    }

    /**
     * @param string $deprecatedIsin
     */
    public function setDeprecatedIsin(string $deprecatedIsin): void
    {
        $this->deprecatedIsin = $deprecatedIsin;
    }

    /**
     * @return string
     */
    public function getActualIsin(): string
    {
        return $this->actualIsin;
    }

    /**
     * @param string $actualIsin
     */
    public function setActualIsin(string $actualIsin): void
    {
        $this->actualIsin = $actualIsin;
    }

    /**
     * @return string
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return int
     */
    public function getIsProcessed(): int
    {
        return $this->isProcessed;
    }

    /**
     * @param int $isProcessed
     */
    public function setIsProcessed(int $isProcessed): void
    {
        $this->isProcessed = $isProcessed;
    }
}
