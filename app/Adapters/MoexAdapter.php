<?php

declare(strict_types=1);

namespace App\Adapters;

use App\Adapters\Models\MoexInstrumentModel;
use App\Adapters\Services\MarketMoexService;
use App\Adapters\Types\MoexInstrumentType;
use App\Broker\Collections\BondCollection;
use App\Broker\Collections\BondCouponCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\FutureCollection;
use App\Broker\Collections\InstrumentCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerBondCouponModel;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerFutureModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerShareModel;
use App\Broker\Types\BrokerInstrumentType;
use App\Broker\Types\BrokerType;
use App\Collections\CandleCollection;
use App\Common\Amqp\AmqpClient;
use App\Common\HttpClient;
use App\Common\HttpResponseCode;
use App\Exceptions\BrokerAdapterException;
use App\Market\Types\CandleIntervalType;

readonly class MoexAdapter implements SourceBrokerInterface
{
    private const BROKER = BrokerType::MOEX->value;

    public function __construct(
        private HttpClient $httpClient,
        private MarketMoexService $moexService,
        private AmqpClient $amqpClient
    ) {
    }

    #[\Override] public function getInstrumentByIsin(string $isin): InstrumentCollection
    {
        $collection = new InstrumentCollection();
        $instruments = $this->moexService->getInstrumentByIsin($isin);
        /** @var MoexInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $collection->add(
                $this->convertMoexToBrokerModel($instrument)
            );
        }

        return $collection;
    }

    #[\Override] public function getCurrencyByIsin(string $isin): ?BrokerCurrencyModel
    {
        // TODO: Implement getCurrencyByIsin() method.
    }

    #[\Override] public function getCurrencies(): BrokerCurrencyCollection
    {
        // TODO: Implement getCurrencies() method.
    }

    #[\Override] public function getHistoryCandles(
        string $isin,
        \DateTime $from,
        \DateTime $to,
        CandleIntervalType $interval
    ): ?CandleCollection {
        $instrumentCollection = $this->moexService->getInstrumentByIsin($isin);

        if ($instrumentCollection->count() === 0) {
            return null;
        }

        /** @var MoexInstrumentModel $instrument */
        $instrument = $instrumentCollection->first();

        return $this->iterateCandles(
            $instrument->getTicker(),
            $instrument->getInstrumentType(),
            $from,
            $to
        );
    }

    private function iterateCandles(
        string $ticker,
        MoexInstrumentType $type,
        \DateTime $from,
        \DateTime $to
    ): CandleCollection {
        $candleCollection = new CandleCollection();
        $fromFormatted = $from->format('Y-m-d');
        $toFormatted = $to->format('Y-m-d');

        list($engine, $market) = $this->getEngineAndMarketByInstrumentType($type);
        $responseRowCount = 1;
        $start = 0;
        while ($responseRowCount > 0) {
            $response = $this->httpClient->doRequest(
                HttpClient::METHOD_GET,
                '/iss/history/engines/' . $engine . '/markets/' . $market
                . '/securities/' . $ticker . '/candles.json',
                ['from' => $fromFormatted, 'till' => $toFormatted, 'start' => $start]
            );

            if ($response->getStatusCode() !== HttpResponseCode::OK) {
                throw new BrokerAdapterException(
                    'Error during receive candles data from moex'
                );
            }

            $content = json_decode($response->getBody()->getContents(), true);

            $columns = $content['history']['columns'];
            $keys = [
                'low' => array_search('LOW', $columns, true),
                'high' => array_search('HIGH', $columns, true),
                'open' => array_search('OPEN', $columns, true),
                'close' => array_search('CLOSE', $columns, true),
                'volume' => array_search('VOLUME', $columns, true),
                'currency' => array_search('CURRENCYID', $columns, true),
                'time' => array_search('TRADEDATE', $columns, true),
                'bondNominal' => array_search('FACEVALUE', $columns, true)
            ];

            $candles = $content['history']['data'];
            foreach ($candles as $candle) {
                if ($candle[$keys['volume']] > 0) {
                    $rate = match ($type) {
                        MoexInstrumentType::Bond => $candle[$keys['bondNominal']] / 100,
                        default => 1
                    };
                    $candleCollection->add(new BrokerCandleModel([
                        'open' => $candle[$keys['open']] * $rate,
                        'close' => $candle[$keys['close']] * $rate,
                        'high' => $candle[$keys['high']] * $rate,
                        'low' => $candle[$keys['low']] * $rate,
                        'volume' => $candle[$keys['volume']],
                        'time' => new \DateTime($candle[$keys['time']]),
                        'isComplete' => true,
                        'interval' => '1day',
                        'currency' => $this->convertCurrency($candle[$keys['currency']])
                    ]));
                }
            }

            $responseRowCount = count($candles);
            $start = $content['history.cursor']['data'][0][0] + $content['history.cursor']['data'][0][2];
        }

        return $candleCollection;
    }

    /**
     * @param array $isinList
     * @return BrokerMoneyModel[]
     * @throws BrokerAdapterException
     */
    #[\Override] public function getLastPrices(array $isinList): array
    {
        $result = [];

        foreach ($isinList as $isin) {
            $result[$isin] = null;

            $instrumentCollection = $this->getInstrumentByIsin($isin);

            /** @var BrokerInstrumentModel $instrument */
            foreach ($instrumentCollection->getIterator() as $instrument) {
                $moexType = $this->convertInstrumentTypeToMoexFormat($instrument->getType());
                $ticker = $instrument->getTicker();

                list($engine, $market) = $this->getEngineAndMarketByInstrumentType($moexType);

                $response = $this->httpClient->doRequest(
                    HttpClient::METHOD_GET,
                    '/iss/engines/' . $engine . '/markets/' . $market . '/securities/' . $ticker . '.json',
                    []
                );

                if ($response->getStatusCode() !== HttpResponseCode::OK) {
                    throw new BrokerAdapterException(
                        'Error during receive instrument data from moex'
                    );
                }
                $content = json_decode($response->getBody()->getContents(), true);
                $price = $content['marketdata']['data'][0][11];

                $currencyKey = array_search('FACEUNIT', $content['securities']['columns'], true);

                if ($price !== null) {
                    $rate = match ($moexType->name) {
                        'Bond' => $content['securities']['data'][0][10] / 100,
                        default => 1
                    };
                    $price = $rate * $price;
                }

                if ($price !== null) {
                    $result[$isin] = new BrokerMoneyModel([
                        'amount' => $price,
                        'currency' => $this->convertCurrency($content['securities']['data'][0][$currencyKey])
                    ]);
                }
            }
        }

        return $result;
    }

    #[\Override] public function getShares(): ShareCollection
    {
        $shares = new ShareCollection();

        [$instrumentList] = $this->getInstrumentList(MoexInstrumentType::Stock);

        foreach ($instrumentList as $instrument) {
            $this->getOrAddInstrument($instrument->getTicker(), $instrument);
            $shares->add(
                new BrokerShareModel([
                    'country' => 'RU',
                    'instrument' => $this->convertMoexToBrokerModel($instrument)
                ])
            );
        }

        return $shares;
    }

    #[\Override] public function getShareByIsin(string $isin): ?BrokerShareModel
    {
        $collection = $this->moexService->getInstrumentByIsin($isin);
        if ($collection->count() === 0) {
            return null;
        }

        /** @var MoexInstrumentModel $instrument */
        $instrument = $collection->first();

        return new BrokerShareModel([
            'instrument' => $this->convertMoexToBrokerModel($instrument),
            'country' => 'RU'
        ]);
    }

    #[\Override] public function getBonds(): BondCollection
    {
        $collection = new BondCollection();

        [$instrumentList, $content] = $this->getInstrumentList(MoexInstrumentType::Bond);

        foreach ($instrumentList as $instrument) {
            $this->getOrAddInstrument($instrument->getTicker(), $instrument);
        }

        foreach ($content['securities']['data'] as $item) {
            $bond = new BrokerBondModel([
                'isin' => $item[28],
                'maturityDate' => new \DateTime($item[13]),
                'nominal' => new BrokerMoneyModel([
                    'amount' => $item[39] ?? 0,
                    'currency' => $this->convertCurrency($item[25])
                ]),
                'initialNominal' => new BrokerMoneyModel([
                    'amount' => $item[38],
                    'currency' => $this->convertCurrency($item[25])
                ]),
            ]);
            $collection->add($bond);
        }
        return $collection;
    }

    #[\Override] public function getBondByIsin(string $isin): ?BrokerBondModel
    {
        $collection = $this->moexService->getInstrumentByIsin($isin);
        if ($collection->count() === 0) {
            return null;
        }

        /** @var MoexInstrumentModel $instrument */
        $instrument = $collection->first();

        [, $content] = $this->getInstrument(MoexInstrumentType::Bond, $instrument->getTicker());

        if ($content === null) {
            return null;
        }

        $data = $content['securities']['data'][0];

        $bond = new BrokerBondModel([
            'isin' => $data[28],
            'initialNominal' => new BrokerMoneyModel([
                'amount' => $data[38],
                'currency' => $this->convertCurrency($data[25])
            ]),
            'nominal' => new BrokerMoneyModel([
                'amount' => $data[10],
                'currency' => $this->convertCurrency($data[25])
            ]),
            'maturityDate' => $data[13] === '0000-00-00' ? null : new \DateTime($data[13]),
        ]);
        return $bond;
    }

    #[\Override] public function getBondCouponsIsin(string $isin, \DateTime $from, \DateTime $to): ?BondCouponCollection
    {
        $couponCollection = new BondCouponCollection();

        $collection = $this->moexService->getInstrumentByIsin($isin);
        if ($collection->count() === 0) {
            return $couponCollection;
        }

        /** @var MoexInstrumentModel $instrument */
        $instrument = $collection->first();

        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            '/iss/securities/' . $instrument->getTicker() . '/bondization.json',
            []
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new BrokerAdapterException(
                'Error during receive bond data from moex'
            );
        }

        $content = json_decode($response->getBody()->getContents(), true);

        foreach ($content['coupons']['data'] as $coupon) {
            $model = new BrokerBondCouponModel([
                'isin' => $coupon[0],
                'date' => new \DateTime($coupon[3]),
                'amount' => $coupon[9] ?? 0,
                'currency' => $this->convertCurrency($coupon[8])
            ]);

            foreach ($content['amortizations']['data'] as $amortizationItem) {
                if ($amortizationItem[3] === $coupon[3]) {
                    $model->setAmortization(new BrokerMoneyModel([
                        'amount' => $amortizationItem[8],
                        'currency' => $this->convertCurrency($amortizationItem[6])
                    ]));
                    break;
                }
            }
            $couponCollection->add($model);
        }
        return $couponCollection;
    }

    #[\Override] public function setInstrumentsForUpdate(int $userId): void
    {
        $instruments = $this->moexService->getInstruments();
        /** @var MoexInstrumentModel $instrument */
        foreach ($instruments as $instrument) {
            $this->amqpClient->publishMessage([
                'type' => 'updateMarketInstrument',
                'data' => [
                    'userId' => $userId,
                    'broker' => self::BROKER,
                    'instrumentId' => (string)$instrument->getMarketMoexId()
                ]
            ]);
        }
    }

    #[\Override] public function updateInstrument(string $instrumentId): ?BrokerInstrumentModel
    {
        $instrument = $this->moexService->getInstrumentById((int)$instrumentId);

        [$instrumentModel] = $this->getInstrument($instrument->getInstrumentType(), $instrument->getTicker());

        if ($instrumentModel === null) {
            return null;
        }

        $instrumentModel->setMarketMoexId($instrument->getMarketMoexId());
        $instrumentModel = $this->moexService->addInstrument($instrumentModel);

        return new BrokerInstrumentModel([
            'ticker' => $instrumentModel->getTicker(),
            'isin' => $instrumentModel->getIsin(),
            'minPriceIncrement' => $instrumentModel->getMinPriceIncrement(),
            'currency' => $instrumentModel->getCurrency(),
            'name' => $instrumentModel->getName(),
            'type' => $instrumentModel->getInstrumentType()->name,
            'logo' => null,
            'regNumber' => $instrumentModel->getRegNumber()
        ]);
    }

    #[\Override] public function getIndicatives(): InstrumentCollection
    {
        // TODO: Implement getIndicatives() method.
    }

    public function getFutures(): FutureCollection
    {
        $collection = new FutureCollection();

        [$instrumentList, $content] = $this->getInstrumentList(MoexInstrumentType::Futures);

        foreach ($instrumentList as $instrument) {
            $this->getOrAddInstrument($instrument->getTicker(), $instrument);
        }

        foreach ($content['securities']['data'] as $item) {
            $future = $this->convertFutureResponseToModel($content, $item);
            $collection->add($future);
        }
        return $collection;
    }

    public function getFutureByIsin(string $isin): ?BrokerFutureModel
    {
        $collection = $this->moexService->getInstrumentByIsin($isin);
        if ($collection->count() === 0) {
            return null;
        }

        /** @var MoexInstrumentModel $instrument */
        $instrument = $collection->first();

        [, $content] = $this->getInstrument(MoexInstrumentType::Futures, $instrument->getTicker());

        if ($content === null) {
            return null;
        }

        $data = $content['securities']['data'][0];

        $future = $this->convertFutureResponseToModel($content, $data);
        return $future;
    }


    private function convertCurrency(string $currency): string
    {
        return match ($currency) {
            'SUR' => 'RUB',
            default => $currency,
        };
    }

    private function getOrAddInstrument(
        string $ticker,
        MoexInstrumentModel $instrumentModel
    ): MoexInstrumentModel {
        $instrument = $this->moexService->getInstrumentByTicker($ticker);

        if ($instrument === null) {
            $instrument = $this->moexService->addInstrument($instrumentModel);
        }

        return $instrument;
    }

    private function convertInstrumentTypeToMoexFormat(string $instrumentType): MoexInstrumentType
    {
        return match ($instrumentType) {
            BrokerInstrumentType::BOND => MoexInstrumentType::Bond,
            BrokerInstrumentType::FUTURES => MoexInstrumentType::Futures,
            BrokerInstrumentType::ETF => MoexInstrumentType::Etf,
            default => MoexInstrumentType::Stock,
        };
    }

    /**
     * @param MoexInstrumentType $type
     * @param string $ticker
     * @return array{
     *  MoexInstrumentModel,
     *  array
     * } | null
     * @throws BrokerAdapterException
     */
    private function getInstrument(MoexInstrumentType $type, string $ticker): ?array
    {
        list($engine, $market) = $this->getEngineAndMarketByInstrumentType($type);

        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            '/iss/engines/' . $engine . '/markets/' . $market
            . '/securities/' . $ticker . '.json',
            []
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new BrokerAdapterException(
                'Error during request instrument data from moex'
            );
        }

        $content = json_decode($response->getBody()->getContents(), true);

        if (count($content['securities']['data']) === 0) {
            return null;
        }

        $item = $content['securities']['data'][0];

        $columns = $content['securities']['columns'];
        $keys = [
            'ticker' => array_search('SECID', $columns, true),
            'isin' => array_search('ISIN', $columns, true),
            'name' => array_search('SECNAME', $columns, true),
            'currency' => array_search('CURRENCYID', $columns, true),
            'minPriceIncrement' => array_search('MINSTEP', $columns, true),
            'regNumber' => array_search('REGNUMBER', $columns, true),
        ];

        $instrument = new MoexInstrumentModel([
            'ticker' => $item[$keys['ticker']],
            'isin' => $item[$keys['isin']],
            'name' => $item[$keys['name']],
            'currency' => $item[$keys['currency']],
            'instrumentType' => $type,
            'isActive' => true,
            'minPriceIncrement' => $item[$keys['minPriceIncrement']],
            'regNumber' => $item[$keys['regNumber']]
        ]);

        return [
            $instrument,
            $content
        ];
    }

    /**
     * @param MoexInstrumentType $type
     * @return array{
     *     MoexInstrumentModel[], array
     * }
     * @throws BrokerAdapterException
     */
    private function getInstrumentList(MoexInstrumentType $type): array
    {
        $models = [];

        list($engine, $market) = $this->getEngineAndMarketByInstrumentType($type);

        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            '/iss/engines/' . $engine . '/markets/' . $market . '/securities.json',
            []
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new BrokerAdapterException(
                'Error during import instrument list from moex'
            );
        }

        $content = json_decode($response->getBody()->getContents(), true);

        $columns = $content['securities']['columns'];
        $keys = [
            'ticker' => array_search('SECID', $columns, true),
            'isin' => array_search('ISIN', $columns, true),
            'name' => array_search('SECNAME', $columns, true),
            'currency' => array_search('CURRENCYID', $columns, true),
            'minPriceIncrement' => array_search('MINSTEP', $columns, true),
            'regNumber' => array_search('REGNUMBER', $columns, true),
        ];
        foreach ($content['securities']['data'] as $item) {
            $models[] = new MoexInstrumentModel([
                'ticker' => $keys['ticker'] !== false ? $item[$keys['ticker']] : null,
                'isin' => $keys['isin'] !== false ? $item[$keys['isin']] : null,
                'name' => $keys['name'] !== false ? $item[$keys['name']] : null,
                'currency' => $keys['currency'] !== false ? $item[$keys['currency']] : null,
                'instrumentType' => $type,
                'isActive' => true,
                'minPriceIncrement' => $keys['minPriceIncrement'] !== false ? $item[$keys['minPriceIncrement']] : null,
                'regNumber' => $keys['regNumber'] !== false ? $item[$keys['regNumber']] : null,
            ]);
        }

        return [$models, $content];
    }

    private function convertMoexToBrokerModel(MoexInstrumentModel $instrument): BrokerInstrumentModel
    {
        return new BrokerInstrumentModel([
            'ticker' => $instrument->getTicker(),
            'isin' => $instrument->getIsin() ?? $instrument->getTicker(),
            'currency' => $this->convertCurrency($instrument->getCurrency()),
            'name' => $instrument->getName(),
            'type' => $instrument->getInstrumentType()->name,
            'logo' => null,
            'minPriceIncrement' => $instrument->getMinPriceIncrement(),
            'lot' => 1,
            'regNumber' => $instrument->getRegNumber()
        ]);
    }

    private function convertFutureResponseToModel(array $content, array $item): BrokerFutureModel
    {
        $columns = $content['securities']['columns'];
        $keys = [
            'minPriceIncrement' => array_search('MINSTEP', $columns, true),
            'minPriceIncrementAmount' => array_search('STEPPRICE', $columns, true),
            'lastTradeDate' => array_search('LASTTRADEDATE', $columns, true),
            'initialMargin' => array_search('INITIALMARGIN', $columns, true),
            'ticker' => array_search('SECID', $columns, true),
        ];

        return new BrokerFutureModel([
            'ticker' => $item[$keys['ticker']],
            'firstTradeDate' => null,
            'lastTradeDate' => new \DateTime($item[$keys['lastTradeDate']]),
            'expirationDate' => new \DateTime($item[$keys['lastTradeDate']]),
            'minPriceIncrement' => (float)$item[$keys['minPriceIncrement']],
            'minPriceIncrementAmount' => (float)$item[$keys['minPriceIncrementAmount']],
            'initialMarginOnBuy' => new BrokerMoneyModel([
                'amount' => (float)$item[$keys['initialMargin']],
                'currency' => 'RUB'
            ]),
            'initialMarginOnSell' => new BrokerMoneyModel([
                'amount' => (float)$item[$keys['initialMargin']],
                'currency' => 'RUB'
            ])
        ]);
    }

    /**
     * @param MoexInstrumentType $type
     * @return array [$engine, $market]
     */
    private function getEngineAndMarketByInstrumentType(MoexInstrumentType $type): array
    {
        $engine = match ($type) {
            MoexInstrumentType::Futures => 'futures',
            default => 'stock'
        };
        $market = match ($type) {
            MoexInstrumentType::Futures => 'forts',
            default => $type->value
        };

        return [$engine, $market];
    }
}
