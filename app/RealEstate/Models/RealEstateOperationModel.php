<?php

declare(strict_types=1);

namespace App\RealEstate\Models;

use App\Common\Models\BaseModel;
use App\Models\PriceModel;

class RealEstateOperationModel extends BaseModel
{
    private PriceModel $price;
    private \DateTime $date;

    /**
     * @return PriceModel
     */
    public function getPrice(): PriceModel
    {
        return $this->price;
    }

    /**
     * @param PriceModel $price
     */
    public function setPrice(PriceModel $price): void
    {
        $this->price = $price;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}
