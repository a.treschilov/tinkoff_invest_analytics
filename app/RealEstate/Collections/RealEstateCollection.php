<?php

declare(strict_types=1);

namespace App\RealEstate\Collections;

use App\RealEstate\Models\RealEstateModel;
use Doctrine\Common\Collections\ArrayCollection;

class RealEstateCollection extends ArrayCollection
{
    public function getItemIds(): array
    {
        $ids = [];
        /** @var RealEstateModel $realEstate */
        foreach ($this->getIterator() as $realEstate) {
            $ids[] = $realEstate->getItemId();
        }

        return $ids;
    }
}
