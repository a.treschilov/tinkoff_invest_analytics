<?php

declare(strict_types=1);

namespace App\RealEstate\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Item\Collections\OperationCollection;
use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Services\ItemConverterService;
use App\Item\Services\OperationService;
use App\Models\PriceModel;
use App\RealEstate\Collections\RealEstateCollection;
use App\RealEstate\Entities\RealEstateEntity;
use App\RealEstate\Entities\RealEstatePriceEntity;
use App\RealEstate\Exceptions\IncorrectDataException;
use App\RealEstate\Models\RealEstateModel;
use App\RealEstate\Models\RealEstatePriceModel;
use App\RealEstate\Storages\RealEstatePriceStorage;
use App\RealEstate\Storages\RealEstateStorage;
use App\User\Exceptions\UserAccessDeniedException;

class RealEstateService
{
    private const ITEM_TYPE = 'real_estate';

    public function __construct(
        private readonly RealEstateStorage $realEstateStorage,
        private readonly RealEstatePriceStorage $realEstatePriceStorage,
        private readonly OperationService $operationService,
        private readonly \DateTime $currentDateTime,
        private readonly ItemConverterService $itemConverterService
    ) {
    }

    public function getUserRealEstate(int $userId): RealEstateCollection
    {
        $collection = new RealEstateCollection();
        $realEstates = $this->realEstateStorage->findByUser($userId);
        foreach ($realEstates as $realEstate) {
            $collection->add($this->itemConverterService->convertRealEstateEntityToModel($realEstate));
        }

        return $collection;
    }

    public function getUserRealEstateItem(int $userId, int $realEstateId): ?RealEstateModel
    {
        $realEstateEntity = $this->realEstateStorage->findByIdAndUser($realEstateId, $userId);

        if ($realEstateEntity === null || $realEstateEntity->getItem()->getIsDeleted()) {
            return null;
        }

        return $this->itemConverterService->convertRealEstateEntityToModel($realEstateEntity);
    }

    public function updateUserRealEstateItem(int $userId, RealEstateModel $realEstate): void
    {
        $realEstateEntity = $this->realEstateStorage->findByIdAndUser($realEstate->getRealEstateId(), $userId);

        if ($realEstateEntity === null || $realEstateEntity->getItem()->getIsDeleted()) {
            throw new UserAccessDeniedException('Access denied update Real Estate to user');
        }

        $realEstateEntity->getItem()->setName($realEstate->getName());
        $realEstateEntity->setInterestAmount($realEstate->getInterestAmount());
        $realEstateEntity->setInterestCurrency($realEstate->getInterestCurrency());
        $realEstateEntity->setPayoutFrequency($realEstate->getPayoutFrequency());
        $realEstateEntity->setPayoutFrequencyPeriod($realEstate->getPayoutFrequencyPeriod());
        $realEstateEntity->setCurrency($realEstate->getCurrency());

        /** @var RealEstatePriceEntity $priceEntity */
        foreach ($realEstateEntity->getRealEstatePrices() as $priceEntity) {
            $price = $realEstate->getPrices()->findById($priceEntity->getRealEstateId());
            if ($price !== null) {
                $priceEntity->setCurrency($price->getCurrency());
                $priceEntity->setDate($price->getDate());
                $priceEntity->setAmount($price->getAmount());
            }
        }

        /** @var RealEstatePriceModel $price */
        foreach ($realEstate->getPrices()->getIterator() as $price) {
            if ($price->getRealEstatePriceId() === null) {
                if ($price->getAmount() === null) {
                    throw new IncorrectDataException('Amount can`t be empty', 1100);
                }
                $priceEntity = new RealEstatePriceEntity();
                $priceEntity->setAmount($price->getAmount());
                $priceEntity->setCurrency($price->getCurrency());
                $priceEntity->setDate($price->getDate()->setTimezone(new \DateTimeZone('UTC')));
                $priceEntity->setRealEstateId($realEstateEntity->getId());
                $priceEntity->setRealEstate($realEstateEntity);
                $realEstateEntity->getRealEstatePrices()->add($priceEntity);
            }
        }

        $isOperationChanged = false;
        $operation = $realEstateEntity->getPurchaseOperation();
        $purchasePrice = $realEstateEntity->getPurchasePrice();
        if ($operation->getAmount() !== -1 * $realEstate->getPurchaseAmount()) {
            $operation->setAmount(-1 * $realEstate->getPurchaseAmount());
            $operation->setIsAnalyticProceed(0);
            $purchasePrice->setAmount($realEstate->getPurchaseAmount());
            $isOperationChanged = true;
        }

        if ($operation->getDate()->getTimestamp() - $realEstate->getPurchaseDate()->getTimestamp()) {
            $operation->setDate($realEstate->getPurchaseDate()->setTimezone(new \DateTimeZone('UTC')));
            $operation->setIsAnalyticProceed(0);
            $purchasePrice->setDate($realEstate->getPurchaseDate());
            $isOperationChanged = true;
        }

        $this->realEstateStorage->addEntity($realEstateEntity);

        if ($isOperationChanged) {
            $this->operationService->publishOperationEvent($realEstateEntity->getPurchaseOperation()->getId());
        }
    }

    public function addUserRealEstateItem(int $userId, RealEstateModel $realEstate): int
    {
        $operation = new OperationModel([
            'userId' => $userId,
            'externalId' => $this->operationService->generateUserOperationExternalId(),
            'operationType' => BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
            'date' => $realEstate->getPurchaseDate(),
            'amount' => -1 * $realEstate->getPurchaseAmount(),
            'currencyIso' => $realEstate->getCurrency(),
            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
            'quantity' => 1,
            'isAnalyticProceed' => 0
        ]);
        $operationEntity = $this->operationService->convertOperationModelToEntity($operation);

        $item = new ItemEntity();
        $item->setName($realEstate->getName());
        $item->setType(self::ITEM_TYPE);
        $item->setSource('user');
        $item->setIsOutdated(0);

        $purchasePrice = new RealEstatePriceEntity();
        $purchasePrice->setDate($realEstate->getPurchaseDate()->setTimezone(new \DateTimeZone('UTC')));
        $purchasePrice->setCurrency($realEstate->getCurrency());
        $purchasePrice->setAmount($realEstate->getPurchaseAmount());

        $realEstateEntity = new RealEstateEntity();
        $realEstateEntity->setUserId($userId);
        $realEstateEntity->setInterestAmount($realEstate->getInterestAmount());
        $realEstateEntity->setInterestCurrency($realEstate->getInterestCurrency());
        $realEstateEntity->setPayoutFrequency($realEstate->getPayoutFrequency());
        $realEstateEntity->setPayoutFrequencyPeriod($realEstate->getPayoutFrequencyPeriod());
        $realEstateEntity->setCurrency($realEstate->getCurrency());
        $realEstateEntity->setPurchasePrice($purchasePrice);
        $realEstateEntity->setItem($item);
        $realEstateEntity->setPurchaseOperation($operationEntity);

        $operationEntity->setItem($item);
        $item->setRealEstate($realEstateEntity);
        $purchasePrice->setRealEstate($realEstateEntity);

        $realEstateId =  $this->realEstateStorage->addEntity($realEstateEntity);

        /** @var RealEstatePriceModel $price */
        foreach ($realEstate->getPrices() as $price) {
            $priceEntity = new RealEstatePriceEntity();
            if ($price->getAmount() !== null) {
                $priceEntity->setAmount($price->getAmount());
            }
            $priceEntity->setCurrency($price->getCurrency());
            $priceEntity->setDate($price->getDate()->setTimezone(new \DateTimeZone('UTC')));
            $priceEntity->setRealEstate($realEstateEntity);

            $this->realEstatePriceStorage->addEntity($priceEntity);
        }

        $this->operationService->publishOperationEvent($realEstateEntity->getPurchaseOperation()->getId());

        return $realEstateId;
    }

    public function calculateRealEstatePrice(int $realEstateId, \DateTime $date): PriceModel|null
    {
        $realEstateEntity = $this->realEstateStorage->findById($realEstateId);
        if ($realEstateEntity->getItem()->getIsDeleted() === 1) {
            return null;
        }

        $realEstate = $this->itemConverterService->convertRealEstateEntityToModel($realEstateEntity);

        $price =  $realEstate->getPrices()?->getLastPriceBeforeDate($date);

        if ($price === null) {
            return null;
        }

        return new PriceModel([
            'amount' => $price->getAmount(),
            'currency' => $price->getCurrency()
        ]);
    }

    public function buildPayoutSchedule(ItemModel $itemModel): OperationCollection
    {
        $realEstate = $itemModel->getRealEstate();
        $operationCollection = new OperationCollection();

        if (
            $realEstate->getInterestAmount() <= 0
            || $realEstate->getPayoutFrequency() === null
            || $realEstate->getPayoutFrequency() <= 0
        ) {
            return $operationCollection;
        }

        $filter = new OperationFiltersModel([
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'operationType' => [
                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND
            ],
            'itemId' => [$realEstate->getItemId()]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($realEstate->getUserId(), $filter);
        /** @var OperationModel $operation */
        $operation = $operations->sortByDate('DESC')->first();

        $startDate = $operation === false ? clone $this->currentDateTime : clone $operation->getDate();
        $endDate = clone $startDate;
        $endDate->add(new \DateInterval('P5Y'));

        $dateInterval = $this->buildDateInterval(
            $realEstate->getPayoutFrequency(),
            $realEstate->getPayoutFrequencyPeriod()
        );

        while ($startDate->getTimestamp() < $endDate->getTimestamp()) {
            $startDate->add($dateInterval);

            $operationCollection->add(new OperationModel([
                'userId' => $realEstate->getUserId(),
                'itemId' => $realEstate->getItemId(),
                'item' => $itemModel,
                'operationType' => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD,
                'externalId' => md5(
                    $realEstate->getItemId()
                    . (clone $startDate)->getTimestamp()
                    . (clone $endDate)->getTimeStamp()
                ),
                'createdDate' => $this->currentDateTime,
                'date' => clone $startDate,
                'amount' => round(floor($realEstate->getInterestAmount() * 100) / 100, 2),
                'currencyIso' => $realEstate->getInterestCurrency(),
                'status' => BrokerOperationStatusType::OPERATION_STATE_DONE
            ]));
        }

        return $operationCollection;
    }

    public function sellUserRealEstate(int $userId, int $realEstateId, PriceModel $price): void
    {
        $realEstateEntity = $this->realEstateStorage->findById($realEstateId);
        if ($realEstateEntity->getItem()->getIsDeleted() === 1) {
            throw new UserAccessDeniedException("Access denied to user");
        }
        if ($realEstateEntity->getUserId() !== $userId) {
            throw new UserAccessDeniedException("Access denied to user");
        }

        $operation = new OperationModel([
            'userId' => $userId,
            'externalId' => $this->operationService->generateUserOperationExternalId(),
            'itemId' => $realEstateEntity->getItemId(),
            'operationType' => BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
            'date' => new \DateTime(),
            'amount' => $price->getAmount(),
            'currencyIso' => $price->getCurrency(),
            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
            'quantity' => 1,
            'isAnalyticProceed' => 0
        ]);
        $operationEntity = $this->operationService->convertOperationModelToEntity($operation);

        $realEstateEntity->getItem()->setIsOutdated(1);

        $realEstateEntity->setSaleOperation($operationEntity);
        $operationEntity->setItem($realEstateEntity->getItem());

        $this->realEstateStorage->addEntity($realEstateEntity);

        $this->operationService->publishOperationEvent($realEstateEntity->getSaleOperation()->getId());
    }

    private function buildDateInterval(int $duration, string $durationPeriod): \DateInterval
    {
        $intervalPeriod = match ($durationPeriod) {
            'day' => 'D',
            'month' => 'M',
            'year' => 'Y'
        };
        return  new \DateInterval('P' . $duration . $intervalPeriod);
    }
}
