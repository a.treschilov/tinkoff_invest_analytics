<?php

declare(strict_types=1);

namespace App\RealEstate\Storages;

use App\RealEstate\Entities\RealEstateEntity;
use Doctrine\ORM\EntityManagerInterface;

class RealEstateStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(RealEstateEntity $entity): int
    {
        $this->entityManager->persist($entity->getPurchasePrice());
        $this->entityManager->persist($entity);
        if ($entity->getPurchaseOperation() !== null) {
            $this->entityManager->persist($entity->getPurchaseOperation());
        }
        if ($entity->getSaleOperation() !== null) {
            $this->entityManager->persist($entity->getSaleOperation());
        }

        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param int $userId
     * @return RealEstateEntity[]
     */
    public function findByUser(int $userId): array
    {
        $result = [];
        $realEstates = $this->entityManager->getRepository(RealEstateEntity::class)
            ->findBy(['userId' => $userId]);

        foreach ($realEstates as $realEstate) {
            if ($realEstate->getItem()->getIsDeleted() === 0) {
                $result[] = $realEstate;
            }
        }

        return $result;
    }

    public function findByIdAndUser(int $realEstateId, int $userId): ?RealEstateEntity
    {
        $realEstate = $this->entityManager->getRepository(RealEstateEntity::class)
            ->findOneBy(['id' => $realEstateId, 'userId' => $userId]);

        return $realEstate?->getItem()?->getIsDeleted() === 0 ? $realEstate : null;
    }

    public function findById(int $realEstateId): ?RealEstateEntity
    {
        return $this->entityManager->getRepository(RealEstateEntity::class)
            ->findOneBy(['id' => $realEstateId]);
    }
}
