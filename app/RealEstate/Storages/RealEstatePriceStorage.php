<?php

declare(strict_types=1);

namespace App\RealEstate\Storages;

use App\RealEstate\Entities\RealEstatePriceEntity;
use Doctrine\ORM\EntityManagerInterface;

class RealEstatePriceStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(RealEstatePriceEntity $entity): int
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }
}
