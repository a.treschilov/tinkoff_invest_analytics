<?php

declare(strict_types=1);

use App\Item\Services\ItemConverterService;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\RealEstate\Services\RealEstateService;
use App\RealEstate\Storages\RealEstatePriceStorage;
use App\RealEstate\Storages\RealEstateStorage;
use App\Services\ExchangeCurrencyService;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(RealEstateStorage::class, function (ContainerInterface $c) {
    return new RealEstateStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(RealEstatePriceStorage::class, function (ContainerInterface $c) {
    return new RealEstatePriceStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(RealEstateService::class, function (ContainerInterface $c) {
    return new RealEstateService(
        $c->get(RealEstateStorage::class),
        $c->get(RealEstatePriceStorage::class),
        $c->get(OperationService::class),
        $c->get('Now' . DateTime::class),
        $c->get(ItemConverterService::class)
    );
});
