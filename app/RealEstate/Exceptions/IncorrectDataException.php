<?php

declare(strict_types=1);

namespace App\RealEstate\Exceptions;

use App\Common\BaseException;

class IncorrectDataException extends BaseException
{
    protected static $CODE = 1100;
}
