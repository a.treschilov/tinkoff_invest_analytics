<?php

declare(strict_types=1);

namespace App\RealEstate\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.real_estate_price')]
#[ORM\Entity]
class RealEstatePriceEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'real_estate_price_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'real_estate_id', type: 'integer')]
    protected int $realEstateId;

    #[ORM\JoinColumn(name: 'real_estate_id', referencedColumnName: 'real_estate_id')]
    #[ORM\ManyToOne(targetEntity: RealEstateEntity::class, cascade: ['persist'])]
    protected RealEstateEntity $realEstate;

    #[ORM\Column(name: 'amount', type: 'float')]
    protected float $amount;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRealEstateId(): int
    {
        return $this->realEstateId;
    }

    /**
     * @param int $realEstateId
     */
    public function setRealEstateId(int $realEstateId): void
    {
        $this->realEstateId = $realEstateId;
    }

    /**
     * @return RealEstateEntity
     */
    public function getRealEstate(): RealEstateEntity
    {
        return $this->realEstate;
    }

    /**
     * @param RealEstateEntity $realEstate
     */
    public function setRealEstate(RealEstateEntity $realEstate): void
    {
        $this->realEstate = $realEstate;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}
