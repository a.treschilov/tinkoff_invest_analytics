<?php

declare(strict_types=1);

namespace App\RealEstate\Entities;

use App\Item\Entities\ItemEntity;
use App\Item\Entities\ItemOperationEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Table(name: 'tinkoff_invest.real_estate')]
#[ORM\Entity]
class RealEstateEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'real_estate_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemEntity::class, inversedBy: 'realEstate', cascade: ['persist'])]
    protected ItemEntity $item;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'interest_amount', type: 'float')]
    protected ?float $interestAmount = null;

    #[ORM\Column(name: 'interest_currency', type: 'string')]
    protected string $interestCurrency;

    #[ORM\Column(name: 'payout_frequency', type: 'integer')]
    protected ?int $payoutFrequency = null;

    #[ORM\Column(name: 'payout_frequency_period', type: 'string')]
    protected ?string $payoutFrequencyPeriod = null;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected ?string $currency = null;

    /**
     * @var PersistentCollection
     */
    #[ORM\OneToMany(
        targetEntity: RealEstatePriceEntity::class,
        mappedBy: 'realEstate',
        cascade: ['persist'],
        fetch: 'EAGER'
    )]
    private PersistentCollection $realEstatePrices;

    #[ORM\Column(name: 'purchase_operation_id', type: 'integer')]
    private ?int $purchaseOperationId = null;

    #[ORM\JoinColumn(name: 'purchase_operation_id', referencedColumnName: 'item_operation_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemOperationEntity::class)]
    protected ?ItemOperationEntity $purchaseOperation = null;

    #[ORM\Column(name: 'sale_operation_id', type: 'integer')]
    private ?int $saleOperationId = null;

    #[ORM\JoinColumn(name: 'sale_operation_id', referencedColumnName: 'item_operation_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemOperationEntity::class)]
    protected ?ItemOperationEntity $saleOperation = null;

    #[ORM\Column(name: 'purchase_price_id', type: 'integer')]
    private int $purchasePriceId;

    #[ORM\JoinColumn(name: 'purchase_price_id', referencedColumnName: 'real_estate_price_id')]
    #[ORM\OneToOne(targetEntity: \App\RealEstate\Entities\RealEstatePriceEntity::class)]
    protected ?RealEstatePriceEntity $purchasePrice;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float|null
     */
    public function getInterestAmount(): ?float
    {
        return $this->interestAmount;
    }

    /**
     * @param float|null $interestAmount
     */
    public function setInterestAmount(?float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return string
     */
    public function getInterestCurrency(): string
    {
        return $this->interestCurrency;
    }

    /**
     * @param string $interestCurrency
     */
    public function setInterestCurrency(string $interestCurrency): void
    {
        $this->interestCurrency = $interestCurrency;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return PersistentCollection
     */
    public function getRealEstatePrices(): PersistentCollection
    {
        return $this->realEstatePrices;
    }

    /**
     * @param PersistentCollection $realEstatePrices
     */
    public function setRealEstatePrices(PersistentCollection $realEstatePrices): void
    {
        $this->realEstatePrices = $realEstatePrices;
    }

    /**
     * @return int|null
     */
    public function getPayoutFrequency(): ?int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int|null $payoutFrequency
     */
    public function setPayoutFrequency(?int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string|null
     */
    public function getPayoutFrequencyPeriod(): ?string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string|null $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(?string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return int|null
     */
    public function getPurchaseOperationId(): ?int
    {
        return $this->purchaseOperationId;
    }

    /**
     * @param int|null $purchaseOperationId
     */
    public function setPurchaseOperationId(?int $purchaseOperationId): void
    {
        $this->purchaseOperationId = $purchaseOperationId;
    }

    /**
     * @return ItemOperationEntity|null
     */
    public function getPurchaseOperation(): ?ItemOperationEntity
    {
        return $this->purchaseOperation;
    }

    /**
     * @param ItemOperationEntity|null $purchaseOperation
     */
    public function setPurchaseOperation(?ItemOperationEntity $purchaseOperation): void
    {
        $this->purchaseOperation = $purchaseOperation;
    }

    /**
     * @return int
     */
    public function getPurchasePriceId(): int
    {
        return $this->purchasePriceId;
    }

    /**
     * @param int $purchasePriceId
     */
    public function setPurchasePriceId(int $purchasePriceId): void
    {
        $this->purchasePriceId = $purchasePriceId;
    }

    /**
     * @return RealEstatePriceEntity|null
     */
    public function getPurchasePrice(): ?RealEstatePriceEntity
    {
        return $this->purchasePrice;
    }

    /**
     * @param RealEstatePriceEntity|null $purchasePrice
     */
    public function setPurchasePrice(?RealEstatePriceEntity $purchasePrice): void
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return int|null
     */
    public function getSaleOperationId(): ?int
    {
        return $this->saleOperationId;
    }

    /**
     * @param int|null $saleOperationId
     */
    public function setSaleOperationId(?int $saleOperationId): void
    {
        $this->saleOperationId = $saleOperationId;
    }

    /**
     * @return ItemOperationEntity|null
     */
    public function getSaleOperation(): ?ItemOperationEntity
    {
        return $this->saleOperation;
    }

    /**
     * @param ItemOperationEntity|null $saleOperation
     */
    public function setSaleOperation(?ItemOperationEntity $saleOperation): void
    {
        $this->saleOperation = $saleOperation;
    }
}
