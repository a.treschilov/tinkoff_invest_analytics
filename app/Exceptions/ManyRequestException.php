<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Common\BaseException;

class ManyRequestException extends BaseException
{
    public static $CODE = 1060;
}
