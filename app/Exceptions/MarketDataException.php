<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Common\BaseException;

class MarketDataException extends BaseException
{
    public static $CODE = 3000;
}
