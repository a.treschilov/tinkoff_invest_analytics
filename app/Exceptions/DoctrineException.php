<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Common\BaseException;

class DoctrineException extends BaseException
{
    protected static $CODE = 1040;
}
