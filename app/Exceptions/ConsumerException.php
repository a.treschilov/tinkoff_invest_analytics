<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Common\BaseException;

class ConsumerException extends BaseException
{
    protected static $CODE = 2000;
}
