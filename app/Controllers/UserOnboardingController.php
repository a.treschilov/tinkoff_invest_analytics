<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UserOnboardingController
{
    public function __construct(
        private readonly UserService $userService
    ) {
    }

    public function onboardingComplete(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $user = $this->userService->getUser();
        $user->setIsNewUser(0);
        $this->userService->updateUser($user);

        $responseBody = [];

        $statusCode = HttpResponseCode::NO_CONTENT;

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
