<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Broker\Services\BrokerService;
use App\Collections\OperationStatusCollection;
use App\Collections\OperationTypeCollection;
use App\Common\HttpResponseCode;
use App\Exceptions\InvalidRequestDataException;
use App\Exceptions\JsonSchemeException;
use App\Formatters\BrokerListFormatter;
use App\Formatters\ItemOperationImportFormatter;
use App\Formatters\OperationAggregateFormatter;
use App\Formatters\OperationFilterFormatter;
use App\Formatters\OperationFormatter;
use App\Formatters\OperationListFormatter;
use App\Formatters\OperationStatusFormatter;
use App\Formatters\OperationTypesFormatter;
use App\Formatters\PriceFormatter;
use App\Item\Collections\BrokerCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Exceptions\DataSourceException;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Models\OperationFiltersModel;
use App\Item\Types\DataSourceType;
use App\Models\OperationFiltersModel as OperationFilter;
use App\Models\PriceModel;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\UploadedFile;

class OperationController
{
    public function __construct(
        private readonly JsonValidatorService $jsonValidatorService,
        private readonly UserService $userService,
        private readonly OperationService $operationService,
        private readonly BrokerService $brokerService
    ) {
    }

    public function getOperationFilters(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $filters = [
            'brokers' => (new BrokerListFormatter($this->brokerService->getActiveBrokerList()))->format(),
            'operationTypes' => (new OperationTypesFormatter($this->operationService->getOperationTypeList()))
                ->format(),
            'operationStatuses' => (new OperationStatusFormatter($this->operationService->getOperationStatusList()))
                ->format()
        ];
        $response->getBody()->write(json_encode($filters));
        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function apiOperations(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $queryParams = $request->getQueryParams();
        $filters = new OperationFiltersModel([
            'operationType' => $this->getQueryFilterArray('operationType', $queryParams),
            'operationStatus' => $this->getQueryFilterArray('operationStatus', $queryParams),
            'brokerId' => $this->getQueryFilterArray('brokerId', $queryParams),
        ]);
        if (empty($queryParams['timezone'])) {
            $queryParams['timezone'] = 'UTC';
        }

        if (!empty($queryParams['dateFrom'])) {
            $date = new \DateTime($queryParams['dateFrom'], new \DateTimeZone($queryParams['timezone']));
            $date->setTime(0, 0, 0);
            $date->setTimezone(new \DateTimeZone('UTC'));
            $filters->setDateFrom($date);
        }
        if (!empty($queryParams['dateTo'])) {
            $date = new \DateTime($queryParams['dateTo'], new \DateTimeZone($queryParams['timezone']));
            $date->setTime(23, 59, 59);
            $date->setTimezone(new \DateTimeZone('UTC'));
            $filters->setDateTo($date);
        }

        $data = $this->dataOperations($filters);
        $operationData = [
            'dateFrom' => $data['dateFrom']->setTimeZone(new \DateTimeZone($queryParams['timezone']))->format('Y-m-d'),
            'dateTo' => $data['dateTo']->setTimeZone(new \DateTimeZone($queryParams['timezone']))->format('Y-m-d'),
            'filters' => (new OperationFilterFormatter($filters))->format(),
            'operations' => (new OperationListFormatter($data['operations']))->format(),
            'summary' => (new PriceFormatter($data['summary']))->format()
        ];

        $response->getBody()->write(json_encode($operationData));
        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    private function getQueryFilterArray(string $name, array $queryParams): ?array
    {
        if (empty($queryParams[$name])) {
            return null;
        }

        $filter = $queryParams[$name];
        return is_array($filter) ? $filter : [$filter];
    }

    /**
     * @param OperationFiltersModel $filters
     * @return array{
     *     operations: OperationCollection,
     *     summary: PriceModel,
     *     brokers: BrokerCollection,
     *     operationTypes: OperationTypeCollection,
     *     operationStatuses: OperationStatusCollection,
     *     dateFrom: \DateTime,
     *     dateTo: \DateTime
     *     }
     */
    private function dataOperations(OperationFiltersModel $filters): array
    {
        $operations = $this->operationService->getFilteredUserOperation($this->userService->getUserId(), $filters);
        $operations = $this->operationService->prepareAsSafetyOperations($operations);
        $summary = $this->operationService->calculateOperationSummary($operations, 'RUB');
        return [
            'operations' => $operations,
            'summary' => $summary,
            'brokers' => $this->brokerService->getActiveBrokerList(),
            'operationTypes' => $this->operationService->getOperationTypeList(),
            'operationStatuses' => $this->operationService->getOperationStatusList(),
            'dateFrom' => $filters->getDateFrom(),
            'dateTo' => $filters->getDateTo()
        ];
    }

    public function getPayIn(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $filters = $request->getQueryParams();
        if (!isset($filters['dateFrom'])) {
            throw new InvalidRequestDataException('Param "dateFrom" is required', 1080);
        };
        if (!isset($filters['dateTo'])) {
            throw new InvalidRequestDataException('Param "dateTo" is required', 1080);
        };
        if (!isset($filters['aggregatePeriod'])) {
            throw new InvalidRequestDataException('Param "aggregatePeriod" is required', 1080);
        };
        if (!isset($filters['timezone'])) {
            $filters['timezone'] = 'UTC';
        }

        $filters['dateFrom'] = new \DateTime($filters['dateFrom'], new \DateTimeZone($filters['timezone']));
        $filters['dateFrom']->setTime(0, 0, 0);
        $filters['dateFrom']->setTimezone(new \DateTimeZone('UTC'));
        $filters['dateTo'] = new \DateTime($filters['dateTo'], new \DateTimeZone($filters['timezone']));
        $filters['dateTo']->setTime(23, 59, 59);
        $filters['dateTo']->setTimezone(new \DateTimeZone('UTC'));
        $operationData = $this->operationService->getPayIn(
            $this->userService->getUserId(),
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['aggregatePeriod']
        );

        $responseBody = [
            'summary' => $operationData->calculateSum(),
            'data' => (new OperationAggregateFormatter($operationData))->format()
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getEarning(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $filters = $request->getQueryParams();
        if (!isset($filters['dateFrom'])) {
            throw new InvalidRequestDataException('Param "dateFrom" is required', 1080);
        };
        if (!isset($filters['dateTo'])) {
            throw new InvalidRequestDataException('Param "dateTo" is required', 1080);
        };
        if (!isset($filters['aggregatePeriod'])) {
            throw new InvalidRequestDataException('Param "aggregatePeriod" is required', 1080);
        };

        $filters['dateFrom'] = new \DateTime($filters['dateFrom']);
        $filters['dateFrom']->setTime(0, 0, 0);
        $filters['dateTo'] = new \DateTime($filters['dateTo']);
        $filters['dateTo']->setTime(23, 59, 59);
        $operationData = $this->operationService->getEarning(
            $this->userService->getUserId(),
            $filters['dateFrom'],
            $filters['dateTo'],
            $filters['aggregatePeriod']
        );

        $responseBody = [
            'summary' => $operationData->calculateSum(),
            'data' => (new OperationAggregateFormatter($operationData))->format()
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getItemOperations(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $filters = new OperationFiltersModel([
            'itemId' => [$args['id']],
            'dateTo' => new \DateTime('2499-12-12 23:59:59')
        ]);

        $operations = $this->operationService->getFilteredUserOperation($this->userService->getUserId(), $filters);
        $operationData = (new OperationListFormatter($operations))->format();
        $response->getBody()->write(json_encode($operationData));
        $statusCode = HttpResponseCode::OK;

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getStatusList(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $statusList = $this->operationService->getOperationStatusList();

        $statusData = (new OperationStatusFormatter($statusList))->format();
        $response->getBody()->write(json_encode($statusData));
        $statusCode = HttpResponseCode::OK;

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getOperation(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $operationId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $statusCode = HttpResponseCode::OK;

        try {
            $operation = $this->operationService->getActiveUserOperationById($userId, $operationId);
            $responseBody = (new OperationFormatter($operation))->format();
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addOperation(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_add_post.json', $body);
            $statusCode = HttpResponseCode::OK;

            $body['date'] = new \DateTime($body['date']['date'], new \DateTimeZone($body['date']['timezone']));
            $userId = $this->userService->getUserId();

            $operationModel = new OperationModel($body);
            $operationItemId = $this->operationService->addUserOperation($userId, $operationModel);

            $responseBody = [
                'operationItemId' => $operationItemId
            ];
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addOperationBatch(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_add_batch_post.json', $body);
            $statusCode = HttpResponseCode::OK;

            $userId = $this->userService->getUserId();
            $operations = new OperationCollection();
            foreach ($body as $operation) {
                $operation['date'] = new \DateTime(
                    $operation['date']['date'],
                    new \DateTimeZone($operation['date']['timezone'])
                );
                $operations->add(new OperationModel($operation));
            }
            $result = $this->operationService->addUserOperationBatch($userId, $operations);
            $responseBody = (new ItemOperationImportFormatter($result))->format();
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function editOperation(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_edit_put.json', $body);

            $operationId = (int)$args['id'];
            $userId = $this->userService->getUserId();
            $body['date'] = new \DateTime($body['date']['date'], new \DateTimeZone($body['date']['timezone']));

            $operationModel = new OperationModel($body);
            $operationModel->setUserId($userId);
            $operationModel->setItemOperationId($operationId);
            $operationModel->setIsAnalyticProceed(0);

            try {
                $this->operationService->editUserOperation($userId, $operationModel);
                $statusCode = HttpResponseCode::NO_CONTENT;
                $responseBody = [];
            } catch (UserAccessDeniedException $e) {
                $responseBody = [
                    'error_code' => $e->getCode(),
                    'error_message' => $e->getMessage()
                ];
                $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            }
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function deleteUserOperation(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $operationId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        try {
            $this->operationService->deleteUserOperation($userId, $operationId);
            $statusCode = HttpResponseCode::NO_CONTENT;
            $responseBody = [];
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function importUserOperations(
        ServerRequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('operation_import.json', $body);

            $dateTo = new \DateTime($body['dateTo']);
            $dateTo->setTime(23, 59, 59);

            $filters = new OperationFilter([
                'dateFrom' => new \DateTime($body['dateFrom']),
                'dateTo' => $dateTo
            ]);
            $importStatistic = $this->operationService->importOperations($filters);
            $responseBody = (new ItemOperationImportFormatter($importStatistic))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function importFromFile(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $body = $request->getParsedBody();
        $files = $request->getUploadedFiles();

        $scheme = array_merge($body, $files);

        try {
            $this->jsonValidatorService->validate('operation_import_file_post.json', $scheme);

            $user = $this->userService->getUser();
            $source = $body['source'];

            /** @var UploadedFile[] $file */
            $file = $files['operations'];
            if (count($files) === 0 || $file[0]->getError() > 0) {
                throw new JsonSchemeException("There are no files to import");
            }

            $dataSource = DataSourceType::tryFrom($source);
            if ($dataSource === null) {
                throw new DataSourceException("Data source '$source' is not supported");
            }

            $this->operationService->uploadingFromFileRequest(
                $user->getId(),
                DataSourceType::tryFrom($source),
                $file
            );
            $statusCode = HttpResponseCode::ACCEPTED;
            $responseBody = [];
        } catch (JsonSchemeException | DataSourceException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
