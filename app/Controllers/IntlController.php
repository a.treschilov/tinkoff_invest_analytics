<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\CountryFormatter;
use App\Intl\Services\CountryService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class IntlController
{
    public function __construct(
        private CountryService $countryService
    ) {
    }
    public function getCountryList(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $countryList = $this->countryService->getList();

        $responseBody = (new CountryFormatter($countryList))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
