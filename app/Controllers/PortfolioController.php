<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Exceptions\AccountException;
use App\Formatters\ShareFormatter;
use App\Services\AccountService;
use App\Services\PortfolioService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PortfolioController
{
    private PortfolioService $portfolioService;
    private AccountService $accountService;

    public function __construct(
        PortfolioService $portfolioService,
        AccountService $accountService
    ) {

        $this->portfolioService = $portfolioService;
        $this->accountService = $accountService;
    }

    public function apiPortfolio(RequestInterface $request, ResponseInterface $response, $args)
    {
        try {
            $portfolio = $this->accountService->getPortfolio();
            $balance = $this->accountService->getPortfolioBalance();

            if (count($portfolio->getPositions()) === 0) {
                throw new AccountException('Portfolio is empty', 1033);
            }

            $shares = $this->portfolioService->calculateInstrumentShare($portfolio, $balance);
            $sharesInstrumentCurrency = $this->portfolioService->calculateCurrencyShare($portfolio, $balance);
            $sharesSector = $this->portfolioService->calculateSectorShare($portfolio, $balance);
            $sharesCountry = $this->portfolioService->calculateCountryShare($portfolio, $balance);
            $sharesCurrencyBalance = $this->portfolioService->calculateCurrencyBalance($portfolio, $balance);
            $totalAmount = $this->portfolioService->calculateTotalAmount($portfolio, $balance);

            $formattedData = [
                'shares' => $shares->count() ? (new ShareFormatter($shares))->format() : null,
                'sharesCurrency' => $sharesInstrumentCurrency->count()
                    ? (new ShareFormatter($sharesInstrumentCurrency))->format()
                    : null,
                'shareSector' => $sharesSector->count() ?  (new ShareFormatter($sharesSector))->format() : null,
                'shareCountry' => $sharesCountry->count() ? (new ShareFormatter($sharesCountry))->format() : null,
                'shareCurrencyBalance' => $sharesCurrencyBalance->count()
                    ? (new ShareFormatter($sharesCurrencyBalance))->format()
                    : null,
                'totalAmount' => $totalAmount
            ];
            $statusCode = HttpResponseCode::OK;
        } catch (AccountException $e) {
            $formattedData = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($formattedData));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
