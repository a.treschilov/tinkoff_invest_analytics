<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\MarketFutureListFormatter;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Types\InstrumentType;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class FutureController
{
    public function __construct(
        private MarketInstrumentServiceInterface $instrumentService
    ) {
    }

    public function list(RequestInterface $request, ResponseInterface $response, $args)
    {
        $marketInstruments = $this->instrumentService->getByType(InstrumentType::FUTURE);

        $responseBody = (new MarketFutureListFormatter($marketInstruments))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
