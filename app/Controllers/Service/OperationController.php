<?php

declare(strict_types=1);

namespace App\Controllers\Service;

use App\Common\HttpResponseCode;
use App\Formatters\PaymentScheduleCollectionFormatter;
use App\Item\Services\PaymentScheduleService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class OperationController
{
    public function __construct(
        private PaymentScheduleService $paymentScheduleService
    ) {
    }
    public function getEventCalendar(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $query = $request->getQueryParams();

        $userId = (int)$query['userId'];
        $dateFrom = isset($query['dateFrom']) ? new \DateTime($query['dateFrom']) : new \DateTime();
        $dateTo = isset($query['dateTo'])
            ? new \DateTime($query['dateTo'])
            : (clone $dateFrom)->add(new \DateInterval('P1M'));

        $dateFrom->setTime(0, 0, 0);
        $dateTo->setTime(23, 59, 59);

        $assetTypes = isset($query['assetTypes']) ? explode(',', $query['assetTypes']) : null;

        $schedule = $this->paymentScheduleService->getUserEventCalendar($userId, $dateFrom, $dateTo, $assetTypes);

        $responseBody = (new PaymentScheduleCollectionFormatter($schedule))->format();

        $statusCode = HttpResponseCode::OK;

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
