<?php

declare(strict_types=1);

namespace App\Controllers\Service;

use App\Common\HttpResponseCode;
use App\Formatters\DashboardFormatter;
use App\Formatters\PriceFormatter;
use App\Services\AnalyticsRealTimeService;
use App\Services\AnalyticsService;
use App\User\Services\UserReportService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class AnalyticsController
{
    public function __construct(
        private UserService $userService,
        private UserReportService $userReportService,
        private AnalyticsRealTimeService $analyticsRealTimeService,
        private AnalyticsService $analyticsService,
    ) {
    }

    public function getMonthlyReport(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $query = $request->getQueryParams();

        $userId = (int)$query['userId'];
        $time = (int)$query['time'];

        $date = new \DateTime();
        $date->setTimestamp($time);

        $report = $this->userReportService->calculateMonthlyReportData($userId, $date);

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($report));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getCapitalChange(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $userId = (int)$request->getQueryParams()['userId'];
        $user = $this->userService->getActiveUserById($userId);
        $this->userService->setUser($user);

        $currency = $request->getQueryParams()['currency'] ?? null;
        $profit = $this->analyticsRealTimeService->calculateProfitToday($currency);

        $formattedData['revenueToday'] = (new PriceFormatter($profit['revenueToday']))->format();

        $response->getBody()->write(json_encode($formattedData));

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function dashboard(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $query = $request->getQueryParams();

        $userId = (int)$query['userId'];
        $user = $this->userService->getActiveUserById($userId);
        $this->userService->setUser($user);

        $summary = $this->analyticsService->getSummary();
        $formatter = new DashboardFormatter($summary);
        $response->getBody()->write(json_encode($formatter->format()));

        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
