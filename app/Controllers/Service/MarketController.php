<?php

declare(strict_types=1);

namespace App\Controllers\Service;

use App\Adapters\Models\TinkoffInstrumentModel;
use App\Adapters\Services\MarketTinkoffService;
use App\Common\HttpResponseCode;
use App\Formatters\CandleListFormatter;
use App\Market\Interfaces\CandleServiceInterface;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Types\InstrumentType;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

readonly class MarketController
{
    public function __construct(
        private CandleServiceInterface $candleService,
        private MarketInstrumentServiceInterface $marketInstrumentService,
        private MarketTinkoffService $marketTinkoffService
    ) {
    }

    public function getCandles(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $marketInstrumentIds = $request->getParsedBody()['marketInstrumentIds'];

        $body = $request->getQueryParams();
        $dateFrom = (new \DateTime($body['dateFrom'])) ?? null;
        $dateTo =  (new \DateTime($body['dateTo'])) ?? null;
        $dateTo->setTime(23, 59, 59);

        $responseBody = [];
        $statusCode = HttpResponseCode::OK;

        $candles = $this->candleService->getInstrumentsIntervalDayCandles(
            $marketInstrumentIds,
            $dateFrom,
            $dateTo,
        );

        foreach ($candles as $candle) {
            $responseBody[] = [
                'marketInstrumentId' => $candle['marketInstrumentId'],
                'candle' => (new CandleListFormatter($candle['candles']))->format()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getShare(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $responseBody = [];
        $statusCode = HttpResponseCode::OK;

        $instruments = $this->marketInstrumentService->getByType(InstrumentType::STOCK);
        /** @var MarketInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $key => $instrument) {
            if ($instrument->getCurrency() !== 'RUB') {
                $instruments->remove($key);
            }
        }
        $tInstruments = $this->marketTinkoffService->getInstruments();

        /** @var MarketInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $tInstrumentCollection = $tInstruments->filterByIsin($instrument->getIsin());
            if ($tInstrumentCollection->count() === 0) {
                continue;
            }
            $responseInstruments = [];
            /** @var TinkoffInstrumentModel $tInstrument */
            foreach ($tInstrumentCollection as $tInstrument) {
                $responseInstruments[] = [
                    'instrumentId' => $tInstrument->getInstrumentId()
                ];
            }
            $responseBody[] = [
                'marketInstrumentId' => $instrument->getMarketInstrumentId(),
                'itemId' => $instrument->getItemId(),
                'isin' => $instrument->getIsin(),
                'ticker' => $instrument->getTicker(),
                'name' => $instrument->getName(),
                'brokerInstrument' => $responseInstruments
            ];
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
