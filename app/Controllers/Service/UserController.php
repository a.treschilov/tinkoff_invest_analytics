<?php

declare(strict_types=1);

namespace App\Controllers\Service;

use App\Common\HttpResponseCode;
use App\Formatters\UserFormatter;
use App\Formatters\UserListFormatter;
use App\User\Exceptions\UserException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class UserController
{
    public function __construct(
        private UserService $userService
    ) {
    }
    public function getUserList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $users = $this->userService->getUserModelList();

        $responseBody = (new UserListFormatter($users))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUser(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $query = $request->getQueryParams();
        $userId = (int)$query['userId'];

        $user = $this->userService->getUserModelById($userId);

        $responseBody = (new UserFormatter($user))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserByTelegramId(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $query = $request->getQueryParams();
        $telegramId = (int)$query['telegramId'];

        try {
            $user = $this->userService->getUserModelByTelegramId($telegramId);
            $responseBody = (new UserFormatter($user))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (UserException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage(),
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
