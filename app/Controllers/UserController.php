<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Exceptions\JsonSchemeException;
use App\Formatters\UserAccountFormatter;
use App\Models\PriceModel;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Exceptions\UserException;
use App\User\Models\UserModel;
use App\User\Models\UserSettingsModel;
use App\User\Services\UserAccountService;
use App\User\Services\UserCodeService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UserController
{
    public function __construct(
        private readonly JsonValidatorService $jsonValidatorService,
        private readonly UserService $userService,
        private readonly UserCodeService $userCodeService,
        private readonly UserAccountService $userAccountService,
    ) {
    }

    public function data(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $user = $this->userService->getUser();

        if ($user === null) {
            $statusCode = HttpResponseCode::UNAUTHORIZED;
            $responseBody = [
                "error_code" => HttpResponseCode::UNAUTHORIZED,
                "error_message" => "User is not authenticated",
            ];
        } else {
            $responseBody = [
                'user_id' => $user->getId(),
                'login' => $user->getLogin(),
                'name' => $user->getName(),
                'language' => $user->getLanguage(),
                'isNewUser' => $user->getIsNewUser()
            ];
            $statusCode = HttpResponseCode::OK;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getSettings(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $user = $this->userService->getUser();

        $settings = $this->userService->getUserSettings($user->getId());

        $responseBody = [
            'userSettingsId' => $settings?->getUserSettingsId() ?? null,
            'desiredPension' => [
                'amount' => $settings?->getDesiredPension()?->getAmount() ?? null,
                'currency' => $settings?->getDesiredPension()?->getCurrency() ?? null
            ],
            'retirementAge' => $settings?->getRetirementAge() ?? null,
            'expenses' => [
                'amount' => $settings?->getExpenses()?->getAmount() ?? null,
                'currency' => $settings?->getExpenses()?->getCurrency() ?? null
            ],
            'birthday' => $settings?->getUser()?->getBirthday() ?? null
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function setSettings(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_settings_post.json', $body);

            if (empty($body['birthday']['date'])) {
                $body['birthday'] = null;
            } else {
                $body['birthday'] = new \DateTime($body['birthday']['date']);
            }
            $body['desiredPension'] = new PriceModel($body['desiredPension']);
            $body['expenses'] = new PriceModel($body['expenses']);
            $userSettings = new UserSettingsModel($body);

            $this->userService->setUserSettings($userSettings);

            $user = $this->userService->getUser();
            $userSettings = $this->userService->getUserSettings($user->getId());

            $responseBody = [
                'userSettingsId' => $userSettings->getUserSettingsId(),
            ];
            $statusCode = HttpResponseCode::OK;
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateUserData(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_data_put.json', $body);
            $user = new UserModel($body);
            $this->userService->updateUserData($user);
            $responseBody = [];
            $statusCode = HttpResponseCode::NO_CONTENT;
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        } catch (UserException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::BAD_REQUEST;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateSettings(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_settings_put.json', $body);

            if (empty($body['birthday']['date'])) {
                $body['birthday'] = null;
            } else {
                $body['birthday'] = new \DateTime($body['birthday']['date']);
            }
            if (isset($body['desiredPension'])) {
                $body['desiredPension'] = new PriceModel($body['desiredPension']);
            }
            if (isset($body['expenses'])) {
                $body['expenses'] = new PriceModel($body['expenses']);
            }

            $userSettings = new UserSettingsModel($body);

            $this->userService->updateUserSettings($userSettings);

            $responseBody = [];

            $statusCode = HttpResponseCode::NO_CONTENT;
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function releaseCode(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $userId = $this->userService->getUserId();

        $code = $this->userCodeService->releaseCode($userId);
        $statusCode = HttpResponseCode::OK;
        $responseBody = ['code' => $code];
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function confirmCode(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();
        $code = $body['code'];
        $clientId = $body['clientId'];

        $statusCode = HttpResponseCode::NO_CONTENT;
        $responseBody = [];
        try {
            $this->userCodeService->connect($clientId, $code);
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNAUTHORIZED;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getAccounts(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $statusCode = HttpResponseCode::OK;

        $userId = $this->userService->getUserId();
        $accounts = $this->userAccountService->getUserAccountList($userId);

        $responseBody = new UserAccountFormatter($accounts)->format();

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
