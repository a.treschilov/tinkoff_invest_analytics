<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\CountryFormatter;
use App\Formatters\CurrencyFormatter;
use App\Formatters\MarketSectorFormatter;
use App\Formatters\MarketStockListFormatter;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketStockFilterModel;
use App\Market\Services\MarketInstrumentImportService;
use App\Market\Services\StockService as MarketStockService;
use App\Market\Types\InstrumentType;
use App\Services\JsonValidatorService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class StockController
{
    public function __construct(
        private readonly MarketInstrumentImportService $importService,
        private readonly MarketStockService $marketStockService,
        private readonly JsonValidatorService $jsonValidatorService,
        private readonly MarketInstrumentServiceInterface $marketInstrumentService
    ) {
    }

    public function list(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        $this->jsonValidatorService->validate('stock_list_get.json', $queryParams);

        $country = $queryParams['country'] ?? null;
        $currency = $queryParams['currency'] ?? null;
        $marketSector = $queryParams['sector'] ?? null;

        $country = $country === '' ? null : $country;
        $currency = $currency === '' ? null : $currency;
        $marketSector = $marketSector === "0" ? null : (int)$marketSector;

        $stockFilter = new MarketStockFilterModel([
            'country' => $country !== null ? new CountryCollection([$country]) : null,
            'currency' => $currency !== null ? new CurrencyCollection([$currency]) : null,
            'sector' => $marketSector !== null ? new MarketSectorCollection([$marketSector]) : null,
        ]);
        $instruments = $this->marketInstrumentService->getByType(InstrumentType::STOCK);
        $instruments = $this->marketStockService->filter($instruments, $stockFilter);

        $responseBody = [
            'stocks' => (new MarketStockListFormatter($instruments))->format()
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function import(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $importStatistic = $this->importService->import(InstrumentType::STOCK);

        $responseBody = [
            'existsStocks' => $importStatistic->getCountImportedItems(),
            'toImport' => $importStatistic->getCountToImportItems()
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function filters(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $filters = $this->marketStockService->getFilters();
        $responseBody = [
            'country' => (new CountryFormatter($filters['country']))->format(),
            'currency' => (new CurrencyFormatter($filters['currency']))->format(),
            'marketSector' => (new MarketSectorFormatter($filters['sector']))->format()
        ];

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
