<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\ApplicationMode;
use App\Common\HttpResponseCode;
use App\Services\JsonValidatorService;
use App\User\Exceptions\AuthTypeException;
use App\User\Exceptions\UserAccountException;
use App\User\Services\UserAccountService;
use App\User\Services\UserService;
use App\User\Types\AuthType;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class AuthController
{
    public function __construct(
        private JsonValidatorService $jsonValidatorService,
        private UserService $userService,
        private UserAccountService $userAccountService,
    ) {
    }

    public function optionRequests(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $response->getBody()->write('');

        return $response->withStatus(200);
    }

    public function authorization(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $body = $request->getParsedBody();

        $this->jsonValidatorService->validate('auth_authorization_get.json', $body);

        try {
            $this->userService->createAndLogin($body['auth_type'], $body['access_token'], $body['lng']);
            $user = $this->userService->getUser();
            $responseBody = [
                'jwt' => $this->userService->generateJWT($user, $body['auth_type'])
            ];
            $statusCode = HttpResponseCode::OK;
        } catch (AuthTypeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNAUTHORIZED;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function linkAccount(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('auth_link_post.json', $body);

        $statusCode = HttpResponseCode::NO_CONTENT;
        $responseBody = [];

        $authType = AuthType::tryFrom($body['auth_type']);
        $accessToken = $body['access_token'];

        try {
            $this->userService->linkAccount($authType, $accessToken);
        } catch (UserAccountException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function unlinkAccount(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('auth_unlink_delete.json', $body);

        $userId = $this->userService->getUserId();
        $authType = AuthType::tryFrom($body['code']);

        try {
            $statusCode = HttpResponseCode::NO_CONTENT;
            $responseBody = [];
            $this->userAccountService->unlinkAccount($userId, $authType);
        } catch (UserAccountException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }


        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
