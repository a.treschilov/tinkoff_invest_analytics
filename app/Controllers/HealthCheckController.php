<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

readonly class HealthCheckController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    public function up(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $response->getBody()->write(json_encode($this->checkApplication()));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(HttpResponseCode::OK);
    }

    public function details(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $result['app'] = $this->checkApplication();
        $result['db'] = $this->checkDatabase();

        $response->getBody()->write(json_encode($result));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(HttpResponseCode::OK);
    }

    private function checkApplication(): array
    {
        return $this->createStatus('OK');
    }

    private function checkDatabase(): array
    {
        try {
            $this->entityManager->getConnection()->getNativeConnection();
            return $this->createStatus('OK');
        } catch (\Throwable $e) {
            $this->logger->error('Exception while trying to check database connection', ['exception' => $e]);
            return $this->createStatus($e->getMessage(), $e->getCode(), true);
        }
    }

    private function createStatus(string $message, int $code = 0, bool $error = false): array
    {
        return [
            'error' => $error,
            'code' => $code,
            'message' => $message
        ];
    }
}
