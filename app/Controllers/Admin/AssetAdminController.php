<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Common\Amqp\AmqpClient;
use App\Common\HttpResponseCode;
use App\Item\Models\ItemFilterModel;
use App\Item\Models\ItemModel;
use App\Item\Services\ItemService;
use App\Item\Types\ItemType;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class AssetAdminController
{
    public function __construct(
        private ItemService $itemService,
        private AmqpClient $amqpClient
    ) {
    }

    public function updatePrice(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $filter = new ItemFilterModel([
            'type' => [ItemType::CROWD_FUNDING]
        ]);
        $items = $this->itemService->getItemByFilter($filter);

        /** @var ItemModel $item */
        foreach ($items->getIterator() as $item) {
            $message = [
                'type' => 'updateItemData',
                'data' => [
                    'itemId' => $item->getItemId()
                ]
            ];
            $this->amqpClient->publishMessage($message);
        }

        $responseBody = [
            'itemCount' => count($items)
        ];

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
