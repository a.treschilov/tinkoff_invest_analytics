<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Common\HttpResponseCode;
use App\Item\Models\ItemFilterModel;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Types\DataSourceType;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class ItemController
{
    public function __construct(
        private ItemService $itemService,
        private OperationService $operationService
    ) {
    }

    public function removeSourceItems(
        RequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        $body = $request->getParsedBody();

        $source = DataSourceType::tryFrom($body['source']);

        if ($source === null) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error_code' => HttpResponseCode::UNPROCCESSABLE_ENTITY,
                'error_message' => 'Source is not valid'
            ];
        } else {
            $itemFilter = new ItemFilterModel([
                "source" => [$source],
            ]);
            $ids = $this->itemService->getItemByFilter($itemFilter)->getIds();
            $opIds = $this->operationService->getSourceOperation($source)->getIds();

            $itemIds = $this->itemService->deleteItems($ids);
            $operationIds = $this->operationService->deleteOperationBatch($opIds);

            $responseBody = [
                'deletedItems' => count($itemIds),
                'deletedOperations' => count($operationIds),
            ];

            $statusCode = HttpResponseCode::OK;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
