<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Broker\Types\BrokerType;
use App\Common\HttpResponseCode;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\JsonValidatorService;
use App\Services\StockBrokerFactory;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class MarketInstrumentController
{
    public function __construct(
        private UserService $userService,
        private FillMarketInstrumentServiceDecorator $marketInstrument,
        private JsonValidatorService $validatorService
    ) {
    }

    public function updateInstruments(
        RequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        $body = $request->getParsedBody();

        $this->validatorService->validate('admin_market_instruments_instrument_put.json', $body);
        $sources = [BrokerType::TINKOFF->value, BrokerType::MOEX->value];
        if (isset($body['source'])) {
            $sources = $body['source'];
        }
        $this->marketInstrument->setupSourceForUpdate($this->userService->getUserId(), $sources);
        $statusCode = HttpResponseCode::NO_CONTENT;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
