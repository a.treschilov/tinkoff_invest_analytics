<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Exceptions\InvalidRequestDataException;
use App\Exceptions\MarketDataException;
use App\Formatters\DashboardFormatter;
use App\Formatters\PriceFormatter;
use App\Formatters\WealthFormatter;
use App\Services\AnalyticsRealTimeService;
use App\Services\AnalyticsService;
use App\Services\WealthService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AnalyticsController
{
    public function __construct(
        private readonly AnalyticsService $analyticsService,
        private readonly AnalyticsRealTimeService $analyticsRealTimeService,
        private readonly WealthService $wealthService,
        private readonly UserService $userService
    ) {
    }

    public function summaryToday(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $currency = $request->getQueryParams()['currency'] ?? null;
        $profit = $this->analyticsRealTimeService->calculateProfitToday($currency);

        $formattedData['revenueToday'] = (new PriceFormatter($profit['revenueToday']))->format();

        $response->getBody()->write(json_encode($formattedData));

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function dashboard(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $summary = $this->analyticsService->getSummary();
        $formatter = new DashboardFormatter($summary);
        $response->getBody()->write(json_encode($formatter->format()));

        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function wealthDynamic(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->wealthService->userDynamic();

        $responseData = [];
        foreach ($data->getIterator() as $wealth) {
            $responseData[] = (new WealthFormatter($wealth))->format();
        }
        $response->getBody()->write(json_encode($responseData));

        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function wealthTypes(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $wealth = $this->wealthService->getLastWealth();

        $responseData = $wealth === null
            ? null
            : (new WealthFormatter($wealth))->format();

        $response->getBody()->write(json_encode($responseData));

        $statusCode = HttpResponseCode::OK;
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function compareWithIndex(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        $userId = $this->userService->getUserId();
        try {
            if (!isset($queryParams['from'])) {
                throw new InvalidRequestDataException('Param "From" is required', 1080);
            }
            if (!isset($queryParams['to'])) {
                throw new InvalidRequestDataException('Param "To" is required', 1080);
            }
            if (!isset($queryParams['benchmark'])) {
                throw new InvalidRequestDataException('Param "Benchmark" is required', 1080);
            }
            if (!is_array($queryParams['benchmark'])) {
                throw new InvalidRequestDataException('Param "Benchmark" should be array', 1080);
            }

            $dateFrom = new \DateTime($queryParams['from']);
            $dateTo = new \DateTime($queryParams['to']);
            $benchmark = $queryParams['benchmark'];

            $responseData = $this->analyticsService->compareWithIndex($userId, $dateFrom, $dateTo, $benchmark);
            $statusCode = HttpResponseCode::OK;
        } catch (MarketDataException | InvalidRequestDataException $e) {
            $responseData = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseData));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
