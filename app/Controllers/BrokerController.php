<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Broker\Services\BrokerReportUploadService;
use App\Broker\Services\BrokerService;
use App\Common\HttpResponseCode;
use App\Formatters\BrokerListFormatter;
use App\Formatters\BrokerReportUploadFormatter;
use App\Formatters\ItemOperationImportFormatter;
use App\Services\MarketOperationsService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\UploadedFile;

readonly class BrokerController
{
    public function __construct(
        private MarketOperationsService $marketOperationsService,
        private BrokerService $brokerService,
        private UserService $userService,
        private BrokerReportUploadService $brokerReportUploadService
    ) {
    }

    public function manualOperationImport(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $broker = $args['broker'];

        $files = $request->getUploadedFiles();
        /** @var UploadedFile $file */
        $file = $files['report'];

        try {
            $statistic = $this->marketOperationsService->importManual($broker, $file);
            $responseBody = (new ItemOperationImportFormatter($statistic))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (ThirdPartyUploadException $e) {
            $statusCode = HttpResponseCode::BAD_REQUEST;
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getList(
        RequestInterface $request,
        ResponseInterface $response
    ) {
        $brokers = $this->brokerService->getActiveBrokerList();
        $responseBody = (new BrokerListFormatter($brokers))->format();
        $statusCode = HttpResponseCode::OK;

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function uploadReportList(
        RequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        $userId = $this->userService->getUserId();

        $reports = $this->brokerReportUploadService->getUserReports($userId);

        $responseBody = [];
        foreach ($reports->getIterator() as $report) {
            $responseBody[] = (new BrokerReportUploadFormatter($report))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
