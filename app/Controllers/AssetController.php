<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Assets\Services\AssetService;
use App\Common\HttpResponseCode;
use App\Formatters\AssetListFormatter;
use App\Formatters\AssetImportFormatter;
use App\Formatters\PortfolioItemsFormatter;
use App\Item\Models\PortfolioItemModel;
use App\Item\Services\ItemService;
use App\Item\Services\PortfolioService;
use App\Item\Models\OperationFiltersModel;
use App\Services\JsonValidatorService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\UploadedFile;

class AssetController
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly UserService $userService,
        private readonly PortfolioService $portfolioService,
        private readonly ItemService $itemService
    ) {
    }

    public function getAssetTypedList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $type = $args['type'];
        $user = $this->userService->getUser();

        $portfolio = $this->portfolioService->buildPortfolio($user->getId(), $this->currentDateTime);
        $items = $this->itemService->getPortfolioItemsByType($portfolio->getItems(), $type);

        $responseBody = [
            'items' => new AssetListFormatter($items)->format(),
            'portfolio' => new PortfolioItemsFormatter($portfolio->getItems())->format()
        ];

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
