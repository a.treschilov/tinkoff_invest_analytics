<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\NewsFormatter;
use App\News\Services\NewsService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class NewsController
{
    public function __construct(
        private NewsService $newsService
    ) {
    }

    public function getNewsList(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $queryParams = $request->getQueryParams();
        $limit = isset($queryParams['limit']) ? (int)$queryParams['limit'] : 10;
        $page = isset($queryParams['page']) ? (int) $queryParams['page'] : 1;

        $news = $this->newsService->getNews($page, $limit);
        $newsCount = $this->newsService->getNewsCount();

        $responseBody = [
            'meta' => [
                'count' => $newsCount
            ],
            'news' => []
        ];
        foreach ($news->getIterator() as $newsItem) {
            $responseBody['news'][] = (new NewsFormatter($newsItem))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getDateRangeNews(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $queryParams = $request->getQueryParams();
        $from = isset($queryParams['from']) ? new \DateTime($queryParams['from']) : new \DateTime();
        $from->setTime(0, 0, 0);
        $to = isset($queryParams['to']) ? new \DateTime($queryParams['to']) : new \DateTime();
        $to->setTime(0, 0, 0);

        $news = $this->newsService->getNewsByDate($from, $to);

        $responseBody = [];
        foreach ($news->getIterator() as $newsItem) {
            $responseBody[] = (new NewsFormatter($newsItem))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
