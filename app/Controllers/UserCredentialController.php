<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\UserBrokerAccountFormatter;
use App\Formatters\UserCredentialFormatter;
use App\Formatters\UserCredentialListFormatter;
use App\Services\JsonValidatorService;
use App\Services\MarketOperationsService;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UserCredentialController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly UserCredentialService $userCredentialService,
        private readonly UserBrokerAccountService $userBrokerAccountService,
        private readonly MarketOperationsService $marketOperationsService,
        private readonly JsonValidatorService $jsonValidatorService
    ) {
    }

    public function getUserCredentialList(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $userId = $this->userService->getUser()->getId();
        $credentialList = $this->userCredentialService->getCredentialList($userId);

        $formatter = new UserCredentialListFormatter($credentialList);
        $responseBody = $formatter->format();

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserCredential(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $responseBody = [];

        $credentialId = (int)$args['id'];
        $userCredential = $this->userCredentialService->getUserCredentialDecryptedById($credentialId);
        if (null !== $userCredential) {
            $responseBody = (new UserCredentialFormatter($userCredential))->format();
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(HttpResponseCode::OK);
    }

    public function addUserCredential(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        $this->jsonValidatorService->validate('user_credential_post.json', $body);

        $userCredentialModel = new UserCredentialModel($body);
        $userCredentialEntity = $this->userCredentialService->addUserCredential($userCredentialModel);

        $responseBody = ['userCredentialId' => $userCredentialEntity->getId()];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateUserCredential(
        RequestInterface $request,
        ResponseInterface $response,
        $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        $this->jsonValidatorService->validate('user_credential_put.json', $body);

        $credentialId = (int)$args['id'];

        $userCredentialModel = new UserCredentialModel($body);
        $userCredentialModel->setUserCredentialId($credentialId);

        $this->userCredentialService->updateUserCredential($userCredentialModel);

        $responseBody = [];
        $statusCode = HttpResponseCode::NO_CONTENT;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getBrokerAccounts(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $credentialId = (int)$args['id'];

        try {
            $accounts = $this->userBrokerAccountService->getUserAccountListByCredential($credentialId);
            $formatter = new UserBrokerAccountFormatter($accounts);
            $responseBody = $formatter->format();
            $statusCode = HttpResponseCode::OK;
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function setBrokerAccounts(RequestInterface $request, ResponseInterface $response, $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('user_credential_broker_account_post.json', $body);

        $credentialId = (int)$args['id'];

        $accounts = new UserBrokerAccountCollection();
        foreach ($body['userAccount'] as $account) {
            $brokerAccount = new UserBrokerAccountEntity();
            $brokerAccount->setIsActive($account['isActive']);
            $brokerAccount->setExternalId($account['externalId']);
            $accounts->add($brokerAccount);
        }

        $this->userBrokerAccountService->updateUserAccountList($credentialId, $accounts);
        $this->marketOperationsService->getUserOperationsToImport();

        $statusCode = HttpResponseCode::NO_CONTENT;
        $response->getBody()->write(json_encode([]));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
