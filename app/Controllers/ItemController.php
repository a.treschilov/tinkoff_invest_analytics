<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Broker\Types\BrokerOperationStatus;
use App\Common\HttpResponseCode;
use App\Formatters\CandleListFormatter;
use App\Formatters\ItemFormatter;
use App\Formatters\ItemTypeRatioFormatter;
use App\Formatters\OperationListFormatter;
use App\Formatters\PaymentScheduleCollectionFormatter;
use App\Formatters\PortfolioBalanceFormatter;
use App\Formatters\PortfolioFullItemsFormatter;
use App\Formatters\PriceFormatter;
use App\Item\Models\ItemFilterModel;
use App\Item\Models\ItemModel;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;
use App\Item\Services\PortfolioService;
use App\Item\Services\UserItemService;
use App\Item\Models\OperationFiltersModel;
use App\Item\Types\ItemType;
use App\Market\Interfaces\CandleServiceInterface;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

readonly class ItemController
{
    public function __construct(
        private OperationService $itemOperationService,
        private UserService $userService,
        private UserItemService $userItemService,
        private ItemService $itemService,
        private PortfolioService $portfolioService,
        private PaymentScheduleService $paymentScheduleService,
        private CandleServiceInterface $candleService
    ) {
    }

    public function getItemInfo(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $itemId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $statusCode = HttpResponseCode::OK;
        try {
            $itemInfo = $this->itemService->getUserItemById($itemId, $userId);
            $responseBody = (new ItemFormatter($itemInfo))->format();
        } catch (UserAccessDeniedException $e) {
            $statusCode = HttpResponseCode::FORBIDDEN;
            $responseBody = [];
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getOperations(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $itemId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $filter = new OperationFiltersModel([
            'itemId' => [$itemId],
            'operationStatus' => [BrokerOperationStatus::DONE->value]
        ]);
        $operations = $this->itemOperationService->getFilteredUserOperation($userId, $filter);
        $operations = $this->itemOperationService->prepareAsSafetyOperations($operations);
        $summary = $this->itemOperationService->calculateOperationSummary($operations, 'RUB');

        $responseBody = [
            'operations' => (new OperationListFormatter($operations))->format(),
            'summary' => (new PriceFormatter($summary))->format()
        ];
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserItemSummary(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $itemId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $summary = $this->userItemService->getItemSummary($userId, $itemId);

        if ($summary === null) {
            $responseBody = [];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        } else {
            $responseBody = [
                'quantity' => $summary['quantity'],
                'amount' => (new PriceFormatter($summary['amount']))->format(),
                'investments' => (new PriceFormatter($summary['investments']))->format(),
                'income' => [
                    'amount' => (new PriceFormatter($summary['income']['amount']))->format()
                ],
                'tax' => [
                    'amount' => (new PriceFormatter($summary['tax']['amount']))->format(),
                    'percent' => $summary['tax']['percent']
                ],
                'fee' => [
                    'amount' => (new PriceFormatter($summary['fee']['amount']))->format(),
                    'percent' => $summary['fee']['percent']
                ],
                'profit' => [
                    'amount' => (new PriceFormatter($summary['profit']['amount']))->format(),
                    'percent' => $summary['profit']['percent']
                ],
                'valueGrowth' => [
                    'amount' => (new PriceFormatter($summary['valueGrowth']['amount']))->format(),
                    'percent' => $summary['valueGrowth']['percent']
                ],
                'incomeForecast' => (new PriceFormatter($summary['incomeForecast']))->format(),
            ];

            $statusCode = HttpResponseCode::OK;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getPortfolio(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $userId = $this->userService->getUserId();
        $date = new \DateTime();

        $portfolio = $this->portfolioService->buildFullInfoPortfolio($userId, $date);
        $itemTypeStatistic = $this->portfolioService->calculatePortfolioSharesByType($portfolio);

        $responseBody = [
            'balance' => new PortfolioBalanceFormatter($portfolio->getBalance())->format(),
            'items' => new PortfolioFullItemsFormatter($portfolio->getItems())->format(),
            'itemTypeRatio' => new ItemTypeRatioFormatter($itemTypeStatistic)->format()
        ];

        $statusCode = HttpResponseCode::OK;

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function deleteUserItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $itemId = (int)$args['id'];
        $user = $this->userService->getUser();

        $statusCode = HttpResponseCode::OK;
        $responseBody = [];
        try {
            $this->itemService->deleteUserItem($user->getId(), $itemId);
            $this->itemOperationService->deleteUserItemOperations($user->getId(), $itemId);
        } catch (UserAccessDeniedException $e) {
            $statusCode = HttpResponseCode::FORBIDDEN;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getEventCalendar(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $userId = $this->userService->getUserId();

        $query = $request->getQueryParams();
        $dateFrom = isset($query['dateFrom']) ? new \DateTime($query['dateFrom']) : new \DateTime();
        $dateTo = isset($query['dateTo'])
            ? new \DateTime($query['dateTo'])
            : (clone $dateFrom)->add(new \DateInterval('P1M'));

        $dateFrom->setTime(0, 0, 0);
        $dateTo->setTime(23, 59, 59);

        $assetTypes = isset($query['assetTypes']) ? explode(',', $query['assetTypes']) : null;

        $schedule = $this->paymentScheduleService->getUserEventCalendar($userId, $dateFrom, $dateTo, $assetTypes);

        $responseBody = [
            'events' => (new PaymentScheduleCollectionFormatter($schedule))->format()
        ];

        $statusCode = HttpResponseCode::OK;

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getItemEvents(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $itemId = (int)$args['id'];
        $userId = $this->userService->getUserId();

        $query = $request->getQueryParams();
        $dateFrom = isset($query['dateFrom']) ? new \DateTime($query['dateFrom']) : new \DateTime();
        $dateTo = isset($query['dateTo'])
            ? new \DateTime($query['dateTo'])
            : (clone $dateFrom)->add(new \DateInterval('P50Y'));

        $dateFrom->setTime(0, 0, 0);
        $dateTo->setTime(23, 59, 59);

        $operations = $this->paymentScheduleService->getUserItemPaymentSchedule($userId, $itemId, $dateFrom, $dateTo);
        $responseBody = (new OperationListFormatter($operations))->format();

        $statusCode = HttpResponseCode::OK;

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getItem(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $isin = (string)$args['isin'];

        $itemFilter = new ItemFilterModel(['type' => [ItemType::MARKET], 'externalId' => [$isin]]);
        $items = $this->itemService->getItemByFilter($itemFilter);

        if ($items->count() === 0) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'message' => 'Item not found',
                'code' => 1012
            ];
        } else {
            $responseBody = (new ItemFormatter($items->first()))->format();
            $statusCode = HttpResponseCode::OK;
        }

        $response->getBody()->write(json_encode($responseBody));

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function priceHistory(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $isin = (string)$args['isin'];
        $body = $request->getQueryParams();

        $dateTo = isset($body['to']) ? new \DateTime($body['to']) : new \DateTime();
        $dateFrom = isset($body['from'])
            ? new \DateTime($body['from'])
            : (clone $dateTo)->sub(new \DateInterval('P1Y'));

        $itemFilter = new ItemFilterModel(['type' => [ItemType::MARKET], 'externalId' => [$isin]]);
        $items = $this->itemService->getItemByFilter($itemFilter);

        if ($items->count() === 0) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'message' => 'Item not found',
                'code' => 1012
            ];
        } else {
            /** @var ItemModel $item */
            $item = $items->first();
            $candles = $this->candleService->getDayCandleInterval(
                $item->getStock()->getMarketInstrumentId(),
                $dateFrom,
                $dateTo
            );
            $responseBody = (new CandleListFormatter($candles))->format();
            $statusCode = HttpResponseCode::OK;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
