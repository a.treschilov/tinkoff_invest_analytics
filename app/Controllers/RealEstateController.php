<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\RealEstateFormatter;
use App\Models\PriceModel;
use App\RealEstate\Collections\RealEstatePriceCollection;
use App\RealEstate\Exceptions\IncorrectDataException;
use App\RealEstate\Models\RealEstateModel;
use App\RealEstate\Models\RealEstatePriceModel;
use App\RealEstate\Services\RealEstateService;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class RealEstateController
{
    private UserService $userService;
    private RealEstateService $realEstateService;
    private JsonValidatorService $jsonValidatorService;

    public function __construct(
        JsonValidatorService $jsonValidatorService,
        UserService $userService,
        RealEstateService $realEstateService
    ) {
        $this->jsonValidatorService = $jsonValidatorService;
        $this->userService = $userService;
        $this->realEstateService = $realEstateService;
    }

    public function getUserRealEstateList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();
        $realEstates = $this->realEstateService->getUserRealEstate($user->getId());

        $responseBody = [];
        foreach ($realEstates->getIterator() as $realEstate) {
            $responseBody[] = (new RealEstateFormatter($realEstate))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserRealEstateItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();

        $realEstateId = (int)$args['id'];

        $realEstate = $this->realEstateService->getUserRealEstateItem($user->getId(), $realEstateId);

        if ($realEstate === null) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error_code' => 2020,
                'error_message' => 'Access denied for user'
            ];
        } else {
            $statusCode = HttpResponseCode::OK;
            $responseBody = (new RealEstateFormatter($realEstate))->format();
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateUserRealEstateItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('real_estate_update_put.json', $body);

        $user = $this->userService->getUser();

        $priceCollection = new RealEstatePriceCollection();
        foreach ($body['prices'] as $key => $price) {
            $price['date'] = new \DateTime(
                $price['date']['date'],
                new \DateTimeZone($price['date']['timezone'])
            );
            $priceCollection->add(new RealEstatePriceModel($price));
        }
        $body['prices'] = $priceCollection;
        $body['purchaseDate'] = new \DateTime(
            $body['purchaseDate']['date'],
            new \DateTimeZone($body['purchaseDate']['timezone'])
        );
        $realEstate = new RealEstateModel($body);
        $realEstate->setRealEstateId((int)$args['id']);

        try {
            $this->realEstateService->updateUserRealEstateItem($user->getId(), $realEstate);
            $statusCode = HttpResponseCode::NO_CONTENT;
            $responseJson = '';
        } catch (IncorrectDataException | UserAccessDeniedException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseJson = json_encode([
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ]);
        }

        $response->getBody()->write($responseJson);
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addUserRealEstateItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('real_estate_add_post.json', $body);

        $user = $this->userService->getUser();

        $priceCollection = new RealEstatePriceCollection();
        foreach ($body['prices'] as $key => $price) {
            $price['date'] = new \DateTime(
                $price['date']['date'],
                new \DateTimeZone($price['date']['timezone'])
            );
            $priceCollection->add(new RealEstatePriceModel($price));
        }
        $body['prices'] = $priceCollection;
        $body['purchaseDate'] = new \DateTime(
            $body['purchaseDate']['date'],
            new \DateTimeZone($body['purchaseDate']['timezone'])
        );
        $realEstate = new RealEstateModel($body);

        $realEstateId = $this->realEstateService->addUserRealEstateItem($user->getId(), $realEstate);
        $statusCode = HttpResponseCode::OK;
        $responseJson = json_encode(['realEstateId' => $realEstateId]);

        $response->getBody()->write($responseJson);

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function sellUserRealEstateItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();
        $this->jsonValidatorService->validate('real_estate_sell_put.json', $body);

        $price = new PriceModel([
            'amount' => (float)$body['price']['amount'],
            'currency' => $body['price']['currency']
        ]);
        $this->realEstateService->sellUserRealEstate(
            $this->userService->getUserId(),
            (int)$args['id'],
            $price
        );
        $statusCode = HttpResponseCode::OK;
        $responseJson = json_encode([]);

        $response->getBody()->write($responseJson);

        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
