<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Formatters\MarketBondListFormatter;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Types\InstrumentType;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

readonly class BondController
{
    public function __construct(
        private MarketInstrumentServiceInterface $instrumentService
    ) {
    }

    public function list(RequestInterface $request, ResponseInterface $response, $args)
    {
        $marketInstruments = $this->instrumentService->getByType(InstrumentType::BOND)
            ->filterNullableBonds();

        $responseBody = (new MarketBondListFormatter($marketInstruments))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
