<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Exceptions\JsonSchemeException;
use App\Formatters\LoanFormatter;
use App\Formatters\LoanPortfolioItemFormatter;
use App\Formatters\OperationListFormatter;
use App\Item\Collections\OperationCollection;
use App\Item\Services\PaymentScheduleService;
use App\Loan\Models\LoanModel;
use App\Loan\Models\LoanPortfolioModel;
use App\Loan\Services\LoanService;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class LoanController
{
    public function __construct(
        private readonly JsonValidatorService $jsonValidatorService,
        private readonly UserService $userService,
        private readonly LoanService $loanService,
        private readonly PaymentScheduleService $paymentScheduleService
    ) {
    }

    public function getUserLoanList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();
        $loanList = $this->loanService->getUserLoans($user->getId());

        $responseBody = [];
        foreach ($loanList->getIterator() as $loan) {
            $responseBody[] = (new LoanFormatter($loan))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserLoanItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $loanId = (int)$args['id'];

        $user = $this->userService->getUser();

        try {
            $loan = $this->loanService->getUserLoan($user->getId(), $loanId);
            $responseBody = (new LoanFormatter($loan))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getLoanPaymentsSuggestion(
        RequestInterface $request,
        ResponseInterface $response,
    ): ResponseInterface {
        $user = $this->userService->getUser();

        $from = new \DateTime('1950-01-01 00:00:00');
        $to = new \DateTime();
        $loans = $this->loanService->getUserLoans($user->getId());

        $operations = new OperationCollection();
        /** @var LoanModel $loan */
        foreach ($loans->getIterator() as $loan) {
            $depositOperations = $this->paymentScheduleService
                ->getUserItemPaymentSchedule($user->getId(), $loan->getItemId(), $from, $to);
            $operations = new OperationCollection(
                array_merge($operations->toArray(), $depositOperations->toArray())
            );
        }

        $responseBody = (new OperationListFormatter($operations))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateUserLoanItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_loan_item_update_put.json', $body);

            $user = $this->userService->getUser();
            $body['date'] = new \DateTime($body['date']['date'], new \DateTimeZone($body['date']['timezone']));
            $loan = new LoanModel($body);

            try {
                $this->loanService->updateUserLoan($user->getId(), $loan);
                $responseBody = [];
                $statusCode = HttpResponseCode::NO_CONTENT;
            } catch (UserAccessDeniedException $e) {
                $responseBody = [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                ];
                $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            }
        } catch (JsonSchemeException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addUserLoanItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();
        try {
            $this->jsonValidatorService->validate('user_loan_item_add_post.json', $body);

            $user = $this->userService->getUser();
            $body['date'] = new \DateTime($body['date']['date'], new \DateTimeZone($body['date']['timezone']));
            $loan = new LoanModel($body);

            $loanId = $this->loanService->addUserLoan($user->getId(), $loan);
            $responseBody = ['id' => $loanId];
            $statusCode = HttpResponseCode::OK;
        } catch (JsonSchemeException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserLoanPortfolio(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();
        $loanList = $this->loanService->getUserLoanPortfolio($user->getId());

        $responseBody = [];
        /** @var LoanPortfolioModel $loan */
        foreach ($loanList->getIterator() as $loan) {
            $responseBody[] = (new LoanPortfolioItemFormatter($loan))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function closeUserLoanItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $loanId = (int)$args['id'];
        $user = $this->userService->getUser();

        $statusCode = HttpResponseCode::OK;
        $responseBody = [];
        try {
            $this->loanService->closeUserLoan($user->getId(), $loanId);
        } catch (UserAccessDeniedException $e) {
            $statusCode = HttpResponseCode::FORBIDDEN;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
