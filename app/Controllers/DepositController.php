<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Common\HttpResponseCode;
use App\Deposit\Models\DepositModel;
use App\Deposit\Services\DepositService;
use App\Exceptions\JsonSchemeException;
use App\Formatters\DepositFormatter;
use App\Formatters\DepositPortfolioItemFormatter;
use App\Formatters\OperationListFormatter;
use App\Item\Collections\OperationCollection;
use App\Item\Services\PaymentScheduleService;
use App\Services\JsonValidatorService;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DepositController
{
    public function __construct(
        private readonly JsonValidatorService $jsonValidatorService,
        private readonly UserService $userService,
        private readonly DepositService $depositService,
        private readonly PaymentScheduleService $paymentScheduleService
    ) {
    }

    public function getUserDepositList(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();
        $depositList = $this->depositService->getUserDeposits($user->getId());

        $responseBody = [];
        foreach ($depositList->getIterator() as $deposit) {
            $responseBody[] = (new DepositFormatter($deposit))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserDepositItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $depositId = (int)$args['id'];

        $user = $this->userService->getUser();

        try {
            $deposit = $this->depositService->getUserDeposit($user->getId(), $depositId);
            $responseBody = (new DepositFormatter($deposit))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserDepositPortfolio(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();

        $portfolio = $this->depositService->getUserDepositPortfolio($user->getId());
        $responseBody = [];
        foreach ($portfolio->getIterator() as $portfolioItem) {
            $responseBody[] = (new DepositPortfolioItemFormatter($portfolioItem))->format();
        }

        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserDepositItemInfo(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $depositId = (int)$args['id'];
        $user = $this->userService->getUser();

        try {
            $deposit = $this->depositService->getUserDepositInfo($user->getId(), $depositId);
            $responseBody = (new DepositPortfolioItemFormatter($deposit))->format();
            $statusCode = HttpResponseCode::OK;
        } catch (UserAccessDeniedException $e) {
            $responseBody = [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function getUserDepositPayouts(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $user = $this->userService->getUser();

        $from = new \DateTime('1950-01-01 00:00:00');
        $to = new \DateTime();
        $deposits = $this->depositService->getUserDeposits($user->getId());

        $operations = new OperationCollection();
        /** @var DepositModel $deposit */
        foreach ($deposits->getIterator() as $deposit) {
            $depositOperations = $this->paymentScheduleService
                ->getUserItemPaymentSchedule($user->getId(), $deposit->getItemId(), $from, $to);
            $operations = new OperationCollection(
                array_merge($operations->toArray(), $depositOperations->toArray())
            );
        }

        $responseBody = (new OperationListFormatter($operations))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function addUserDepositItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();
        try {
            $this->jsonValidatorService->validate('user_deposit_item_add_post.json', $body);

            $body['dealDate'] = new \DateTime(
                $body['dealDate']['date'],
                new \DateTimeZone($body['dealDate']['timezone'])
            );

            $user = $this->userService->getUser();
            $deposit = new DepositModel($body);

            $depositId = $this->depositService->addUserDeposit($user->getId(), $deposit);
            $responseBody = ['id' => $depositId];
            $statusCode = HttpResponseCode::OK;
        } catch (JsonSchemeException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function updateUserDepositItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_deposit_item_update_put.json', $body);

            $user = $this->userService->getUser();

            $body['dealDate'] = new \DateTime(
                $body['dealDate']['date'],
                new \DateTimeZone($body['dealDate']['timezone'])
            );
            $deposit = new DepositModel($body);

            try {
                $this->depositService->updateUserDeposit($user->getId(), $deposit);
                $responseBody = [];
                $statusCode = HttpResponseCode::NO_CONTENT;
            } catch (UserAccessDeniedException $e) {
                $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
                $responseBody = [
                    'error' => $e->getMessage(),
                    'code' => $e->getCode()
                ];
            }
        } catch (JsonSchemeException $e) {
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function closeUserDepositItem(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $depositId = (int)$args['id'];
        $user = $this->userService->getUser();

        $statusCode = HttpResponseCode::OK;
        $responseBody = [];
        try {
            $this->depositService->closeUserDeposit($user->getId(), $depositId);
        } catch (UserAccessDeniedException $e) {
            $statusCode = HttpResponseCode::FORBIDDEN;
            $responseBody = [
                'error' => $e->getMessage(),
                'code' => $e->getCode()
            ];
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
