<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Collections\ShareCollection;
use App\Common\HttpResponseCode;
use App\Exceptions\JsonSchemeException;
use App\Formatters\TargetShareFormatter;
use App\Models\TargetShareModel;
use App\Services\JsonValidatorService;
use App\Services\StrategyService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UserStrategyShareController
{
    private StrategyService $strategyService;
    private JsonValidatorService $jsonValidatorService;

    public function __construct(
        JsonValidatorService $jsonValidatorService,
        StrategyService $strategyService
    ) {
            $this->strategyService = $strategyService;
            $this->jsonValidatorService = $jsonValidatorService;
    }

    public function getTargetShare(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $type = (string)$args['type'];

        $strategy = $this->strategyService->getTargetShare($type);

        $responseBody = (new TargetShareFormatter($strategy))->format();
        $statusCode = HttpResponseCode::OK;
        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }

    public function postTargetShare(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $body = $request->getParsedBody();

        try {
            $this->jsonValidatorService->validate('user_strategy_share_post.json', $body);
            $type = (string)$args['type'];

            $shares = new ShareCollection();
            foreach ($body as $share) {
                $shares->add(new TargetShareModel($share));
            };

            $this->strategyService->updateTargetShare($type, $shares);

            $statusCode = HttpResponseCode::NO_CONTENT;
            $responseBody = '';
        } catch (JsonSchemeException $e) {
            $responseBody = [
                'error_code' => $e->getCode(),
                'error_message' => $e->getMessage()
            ];
            $statusCode = HttpResponseCode::UNPROCCESSABLE_ENTITY;
        }

        $response->getBody()->write(json_encode($responseBody));
        return $response->withHeader('Content-Type', 'application/json')->withStatus($statusCode);
    }
}
