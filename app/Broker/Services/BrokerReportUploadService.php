<?php

declare(strict_types=1);

namespace App\Broker\Services;

use App\Broker\Collections\BrokerReportUploadCollection;
use App\Broker\Entities\BrokerReportUploadEntity;
use App\Broker\Models\BrokerReportUploadModel;
use App\Broker\Storages\BrokerReportUploadStorage;
use App\Broker\Types\BrokerType;

class BrokerReportUploadService
{
    public function __construct(
        private readonly BrokerReportUploadStorage $brokerReportUploadStorage,
        private readonly BrokerService $brokerService,
        private readonly BrokerHelper $brokerHelper
    ) {
    }

    public function addReport(BrokerReportUploadModel $report): int
    {
        $entity = new BrokerReportUploadEntity();
        $entity->setUserId($report->getUserId());
        $entity->setDate($report->getDate());
        $entity->setBrokerId($report->getBrokerId()->value);
        $entity->setBroker($this->brokerService->getBrokerEntityById($report->getBrokerId()->value));
        $entity->setAdded($report->getAdded());
        $entity->setUpdated($report->getUpdated());
        $entity->setSkipped($report->getSkipped());
        $entity->setStatus($report->getStatus());

        return $this->brokerReportUploadStorage->addEntity($entity);
    }

    public function getUserReports(int $userId): BrokerReportUploadCollection
    {
        $reportCollection = new BrokerReportUploadCollection();
        $reports = $this->brokerReportUploadStorage->findByUserId($userId);
        foreach ($reports as $report) {
            $reportCollection->add($this->convertEntityToModel($report));
        }

        return $reportCollection;
    }

    private function convertEntityToModel(BrokerReportUploadEntity $entity): BrokerReportUploadModel
    {
        return new BrokerReportUploadModel([
            'brokerUploadReportId' => $entity->getId(),
            'userId' => $entity->getUserId(),
            'date' => $entity->getDate(),
            'brokerId' => BrokerType::tryFrom($entity->getBrokerId()),
            'broker' => $this->brokerHelper->convertBrokerEntityToModel($entity->getBroker()),
            'added' => $entity->getAdded(),
            'updated' => $entity->getUpdated(),
            'skipped' => $entity->getSkipped(),
            'status' => $entity->getStatus()
        ]);
    }
}
