<?php

declare(strict_types=1);

namespace App\Broker\Services\Adapters;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Types\BrokerOperationStatus;
use App\Exceptions\BrokerAdapterException;
use App\Item\Types\OperationType;
use App\Services\MarketInstrumentService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

readonly class SberInvest implements UserBrokerInterface
{
    public function __construct(
        private MarketInstrumentService $marketInstrumentService
    ) {
    }
    private const BROKER = 'sber';
    public function manualOperationImport(UploadedFile $file): OperationCollection
    {
        $reader = IOFactory::createReaderForFile($file->getFilePath(), ['Xls', 'Xlsx']);
        $spreadSheet = $reader->load($file->getFilePath());

        $operations = $this->parseDeals($spreadSheet);
        $operations->merge($this->parsePayouts($spreadSheet));

        return $operations;
    }

    public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection
    {
        // TODO: Implement getOperations() method.
    }

    public function getAccounts(): AccountCollection
    {
        // TODO: Implement getAccounts() method.
    }

    public function getPortfolio(string $accountId): BrokerPortfolioModel
    {
        // TODO: Implement getPortfolio() method.
    }

    public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection
    {
        // TODO: Implement getPortfolioBalance() method.
    }

    private function parseDeals(Spreadsheet $spreadSheet): OperationCollection
    {
        $operations = new OperationCollection();

        $sheet = $spreadSheet->getSheetByName('Сделки');
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $rowCount = $sheet->getHighestRow();
        if ($rowCount === 1) {
            return $operations;
        }

        foreach ($sheet->getRowIterator(2) as $row) {
            $rowNum = $row->getRowIndex();

            $instrument = $this->marketInstrumentService->getInstrumentByTicker(
                $sheet->getCell('D' . $rowNum)->getValue()
            );
            if ($instrument === null) {
                throw new BrokerAdapterException("Instrument is not exist", 1074);
            }

            $currency = $sheet->getCell('L' . $rowNum)->getValue();
            $operation = new BrokerOperationModel([
                'id' => $sheet->getCell('B' . $rowNum)->getValue(),
                'currency' => $currency,
                'payment' => new BrokerMoneyModel([
                    'amount' => $sheet->getCell('K' . $rowNum)->getValue(),
                    'currency' => $currency,
                ]),
                'price' =>  new BrokerMoneyModel([
                    'amount' => $sheet->getCell('I' . $rowNum)->getValue(),
                    'currency' => $currency,
                ]),
                'status' => BrokerOperationStatus::DONE->value,
                'quantity' => $sheet->getCell('H' . $rowNum)->getValue(),
                'isin' => $instrument->getIsin(),
                'date' => new \DateTime($sheet->getCell('C' . $rowNum)->getFormattedValue()),
                'type' => $this->convertOperationTypeToBroker(
                    $sheet->getCell('G' . $rowNum)->getValue()
                )->value,
                'instrumentType' => $instrument->getType(),
                'broker' => self::BROKER,
            ]);

            if ($this->isInvertPrice($operation->getType())) {
                $operation->getPrice()->setAmount(-1 * $operation->getPrice()->getAmount());
                $operation->getPayment()->setAmount(-1 * $operation->getPayment()->getAmount());
            }

            $operations->add($operation);
        }

        return $operations;
    }

    private function parsePayouts(Spreadsheet $spreadSheet): OperationCollection
    {
        $operations = new OperationCollection();

        $sheet = $spreadSheet->getSheetByName('Движение ДС');
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $dateCounter = [];

        foreach ($sheet->getRowIterator(2) as $row) {
            $rowNum = $row->getRowIndex();

            $currency = $sheet->getCell('G' . $rowNum)->getValue();
            $date = new \DateTime($sheet->getCell('C' . $rowNum)->getFormattedValue());
            $timestamp = $date->getTimestamp();
            $dateCounter[$timestamp] = isset($dateCounter[$timestamp]) ? $dateCounter[$timestamp] + 1 : 1;

            $instrument = null;
            if ($sheet->getCell('E' . $rowNum)->getValue() !== null) {
                $instrument = $this->marketInstrumentService->getInstrumentByTicker(
                    $sheet->getCell('E' . $rowNum)->getValue()
                );
            }

            $operationArray = [
                'currency' => $currency,
                'payment' => new BrokerMoneyModel([
                    'amount' => $sheet->getCell('F' . $rowNum)->getValue(),
                    'currency' => $currency,
                ]),
                'status' => BrokerOperationStatus::DONE->value,
                'date' => $date,
                'isin' => $instrument?->getIsin(),
                'type' => $this->convertOperationTypeToBroker(
                    $sheet->getCell('D' . $rowNum)->getValue()
                )->value,
                'broker' => self::BROKER,
            ];

            if ($this->isInvertPrice($operationArray['type'])) {
                $operationArray['payment']->setAmount(-1 * $operationArray['payment']->getAmount());
            }

            $operationArray['id'] = md5(
                $operationArray['broker']
                . $dateCounter[$timestamp] . $timestamp
                . $operationArray['currency'] . $operationArray['payment']->getAmount()
            );
            $operation = new BrokerOperationModel($operationArray);

            $operations->add($operation);
        }

        return $operations;
    }

    private function convertOperationTypeToBroker(string $type): OperationType
    {
        return match ($type) {
            'Покупка' => OperationType::BUY,
            'Продажа' => OperationType::SELL,
            'Вывод со счёта' => OperationType::OUTPUT,
            'Пополнение счёта' => OperationType::INPUT,
            'Выплата дивидендов', 'Выплата купонов' => OperationType::DIVIDEND,
            'Комиссия' => OperationType::FEE,
            'Налог' => OperationType::TAX,
            default => throw new BrokerAdapterException('Invalid OperationType', 1073),
        };
    }

    private function isInvertPrice(string $type): bool
    {
        return in_array(
            $type,
            [
                OperationType::FEE->value,
                OperationType::OUTPUT->value,
                OperationType::TAX->value,
                OperationType::BUY->value
            ]
        );
    }
}
