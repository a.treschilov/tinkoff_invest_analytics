<?php

declare(strict_types=1);

namespace App\Broker\Services\Adapters;

use App\Broker\Types\BrokerInstrumentType;
use App\Broker\Types\BrokerOperationStatus;
use App\Exceptions\BrokerAdapterException;
use App\Item\Types\OperationType;
use Google\Protobuf\Timestamp;
use JetBrains\PhpStorm\Pure;
use Tinkoff\Invest\V1\MoneyValue;
use Tinkoff\Invest\V1\Quotation;

class TinkoffHelper
{
    #[Pure]
    public function convertMoneyValueToFloat(MoneyValue $moneyValue): float
    {
        return $moneyValue->getUnits() + $moneyValue->getNano() / pow(10, 9);
    }

    public function getCurrencyISO(string $currency): string
    {
        return match ($currency) {
            'usd' => 'USD',
            'rub' => 'RUB',
            'eur' => 'EUR',
            default => strtoupper($currency)
        };
    }

    public function getOperationStatus(int $status): BrokerOperationStatus
    {
        return match ($status) {
            0 => BrokerOperationStatus::UNSPECIFIED,
            1 => BrokerOperationStatus::DONE,
            2 => BrokerOperationStatus::DECLINED,
            3 => BrokerOperationStatus::PROGRESS,
            default => throw new BrokerAdapterException('Unknown operation status', 1070)
        };
    }

    public function convertGoogleTimestampToDate(Timestamp $date): \DateTime
    {
        return new \DateTime('@' . $date->getSeconds());
    }

    public function getOperationType(int $type): OperationType
    {
        return match ($type) {
            0, 57, 58, 61, 64 => OperationType::UNSPECIFIED,
            1, 51, 54, 60 => OperationType::INPUT,
            2, 5, 8, 11, 13, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 44
            => OperationType::TAX,
            3 => OperationType::SECURITY_OUT,
            4, 21, 23, 26, 63 => OperationType::DIVIDEND,
            6, 10 => OperationType::REPAYMENT,
            7 => OperationType::SELL_CARD,
            9, 25, 27, 50, 53, 59 => OperationType::OUTPUT,
            12, 14, 19, 24, 30, 31, 45, 46, 47, 55, 56, 62 => OperationType::FEE,
            15, 20, 28 => OperationType::BUY,
            16 => OperationType::BUY_CARD,
            17 => OperationType::SECURITY_IN,
            18, 22, 29 => OperationType::SELL,
            43 => OperationType::DIVIDEND_CARD,
            default => throw new BrokerAdapterException('Unknown operation type', 1071)
        };
    }

    public function getInstrumentType(string $type): string
    {
        return match ($type) {
            'share' => BrokerInstrumentType::STOCK,
            'currency' => BrokerInstrumentType::CURRENCY,
            'bond' => BrokerInstrumentType::BOND,
            'etf' => BrokerInstrumentType::ETF,
            'futures' => BrokerInstrumentType::FUTURES,
            default => $type
        };
    }

    #[Pure]
    public function convertQuotationToFloat(?Quotation $quotation): ?float
    {
        if (null === $quotation) {
            return null;
        }
        return $quotation->getUnits() + $quotation->getNano() / pow(10, 9);
    }
}
