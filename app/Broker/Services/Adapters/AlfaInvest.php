<?php

declare(strict_types=1);

namespace App\Broker\Services\Adapters;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Types\BrokerOperationStatus;
use App\Broker\Types\BrokerType;
use App\Item\Types\OperationType;
use App\Services\MarketInstrumentService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

readonly class AlfaInvest implements UserBrokerInterface
{
    private const BROKER = BrokerType::ALFA_INVEST->value;

    public function __construct(
        private MarketInstrumentService $marketInstrumentService
    ) {
    }

    #[\Override] public function manualOperationImport(UploadedFile $file): OperationCollection
    {
        $reader = IOFactory::createReaderForFile($file->getFilePath(), ['Xls', 'Xlsx']);
        $spreadSheet = $reader->load($file->getFilePath());

        $operations = $this->parseDeals($spreadSheet);
        $deals = $this->parsePayouts($spreadSheet);

        return $operations->merge($deals);
    }

    #[\Override] public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection
    {
        // TODO: Implement getOperations() method.
    }

    #[\Override] public function getAccounts(): AccountCollection
    {
        // TODO: Implement getAccounts() method.
    }

    #[\Override] public function getPortfolio(string $accountId): BrokerPortfolioModel
    {
        // TODO: Implement getPortfolio() method.
    }

    #[\Override] public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection
    {
        // TODO: Implement getPortfolioBalance() method.
    }

    private function parseDeals(Spreadsheet $spreadSheet): OperationCollection
    {
        $sheet = $spreadSheet->getSheetByName('Завершенные сделки');
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $headers = [
            'externalId' => '',
            'dealDate' => '',
            'isin' => '',
            'quantity' => '',
            'amount' => '',
            'currency' => '',
            'nkd' => '',
            'fee' => '',
            'feeCurrency' => ''
        ];
        $isOperationStarted = false;
        $isOperationEnded = false;
        $assetsOperations = new OperationCollection();
        foreach ($sheet->getRowIterator() as $row) {
            if (!$isOperationStarted) {
                foreach ($row->getCellIterator('E', 'E') as $cell) {
                    if ($cell->getValue() === "№ сделки") {
                        $isOperationStarted = true;

                        foreach ($row->getCellIterator('E', 'AE') as $headerCell) {
                            switch ((string)$headerCell->getValue()) {
                                case "№ сделки":
                                    $headers['externalId'] = $headerCell->getColumn();
                                    break;
                                case "Дата\nзаключен.":
                                    $headers['dealDate'] = $headerCell->getColumn();
                                    break;
                                case "ISIN/рег.код":
                                    $headers['isin'] = $headerCell->getColumn();
                                    break;
                                case "\nКуплено\n(продано),\nшт./грамм":
                                    $headers['quantity'] = $headerCell->getColumn();
                                    break;
                                case "Сумма\nсделки⁷ ":
                                    $headers['amount'] = $headerCell->getColumn();
                                    break;
                                case "Вал.⁸":
                                    $headers['currency'] = $headerCell->getColumn();
                                    break;
                                case "В т.ч. \nНКД ⁹":
                                    $headers['nkd'] = $headerCell->getColumn();
                                    break;
                                case "\nКомиссия\nбанка":
                                    $headers['fee'] = $headerCell->getColumn();
                                    break;
                                case "Вал.¹⁰":
                                    $headers['feeCurrency'] = $headerCell->getColumn();
                                    break;
                            }
                        }
                    }
                }
                continue;
            }

            if ($isOperationStarted && !$isOperationEnded) {
                foreach ($row->getCellIterator('E', 'E') as $cell) {
                    if ($cell->getValue() === null) {
                        $isOperationEnded = true;
                    }
                }
            }

            if ($isOperationStarted && !$isOperationEnded) {
                $operation = new BrokerOperationModel([
                    'currency' => 'RUB',
                    'status' => BrokerOperationStatus::DONE->value,
                    'broker' => self::BROKER,
                    'type' => OperationType::BUY->value
                ]);
                $fee = new BrokerOperationModel([
                    'currency' => 'RUB',
                    'status' => BrokerOperationStatus::DONE->value,
                    'broker' => self::BROKER,
                    'type' => OperationType::FEE->value,
                    'quantity' => null,
                    'price' => null
                ]);
                $nkd = 0;
                foreach ($row->getCellIterator('E', 'AC') as $cell) {
                    $nkd = 0;
                    /** @var RichText $cellValue */
                    $cellValue = $cell->getValue();
                    switch ($cell->getColumn()) {
                        case $headers['externalId']:
                            $text = trim($cellValue->getRichTextElements()[1]->getText());
                            $operation->setId($text . '_buy');
                            $fee->setId($text . '_fee');
                            $fee->setParentOperationId($text . '_buy');
                            break;
                        case $headers['dealDate']:
                            $date = trim($cellValue->getRichTextElements()[0]->getText());
                            $time = trim($cellValue->getRichTextElements()[2]->getText());
                            $dateTime = new \DateTime($date . ' ' . $time);
                            $operation->setDate($dateTime);
                            $fee->setDate($dateTime);
                            break;
                        case $headers['isin']:
                            $operation->setIsin((string)$cell->getValue());
                            $fee->setIsin((string)$cell->getValue());
                            break;
                        case $headers['quantity']:
                            $operation->setQuantity((int)$cell->getValue());
                            break;
                        case $headers['amount']:
                            $operation->setPayment(new BrokerMoneyModel([
                                'currency' => 'RUB',
                                'amount' => -1 * (float)$cell->getValue()
                            ]));
                            break;
                        case $headers['nkd']:
                            $nkd = (float)$cell->getValue();
                            break;
                        case $headers['currency']:
                            $operation->setPayment(new BrokerMoneyModel([
                                'currency' => (string)$cell->getValue(),
                                'amount' => $operation->getPayment()->getAmount()
                            ]));
                            break;
                        case $headers['fee']:
                            $feeAmount = (float)$cell->getValue() === 0.0 ? 0 : -1 * (float)$cell->getValue();
                            $fee->setPayment(new BrokerMoneyModel([
                                'currency' => 'RUB',
                                'amount' => $feeAmount
                            ]));
                            break;
                        case $headers['feeCurrency']:
                            $fee->setPayment(new BrokerMoneyModel([
                                'currency' => $cell->getValue() === null ? "RUB" : (string)$cell->getValue(),
                                'amount' => $fee->getPayment()->getAmount()
                            ]));
                            break;
                    }
                }
                $operation->setPrice(new BrokerMoneyModel([
                    'amount' => ($operation->getPayment()->getAmount() - $nkd) / $operation->getQuantity(),
                    'currency' => $operation->getPayment()->getCurrency()
                ]));
                $assetsOperations->add($operation);
                $assetsOperations->add($fee);
            }
        }

        return $assetsOperations;
    }

    private function parsePayouts(Spreadsheet $spreadSheet): OperationCollection
    {
        $operations = new OperationCollection();

        $sheet = $spreadSheet->getSheetByName(' Движение ДС');
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $isOperationStarted = false;
        $isOperationEnded = false;
        foreach ($sheet->getRowIterator() as $row) {
            if (!$isOperationStarted) {
                foreach ($row->getCellIterator('R', 'R') as $cell) {
                    if ($cell->getValue() === "ден. позиций") {
                        $isOperationStarted = true;
                    }
                }
                continue;
            }

            if ($isOperationStarted && !$isOperationEnded) {
                foreach ($row->getCellIterator('K', 'K') as $cell) {
                    if ($cell->getValue() === 'Итого:') {
                        $isOperationEnded = true;
                    }
                }
            }

            if ($isOperationStarted && !$isOperationEnded) {
                $assetOperation = [];
                foreach ($row->getCellIterator('C', 'AA') as $cell) {
                    switch ($cell->getColumn()) {
                        case 'G':
                            $unixDate = ($cell->getValue() - 25569) * 86400;
                            $assetOperation['date'] = (new \DateTime())->setTimestamp((int)$unixDate);
                            break;
                        case 'J':
                            $type = (string)$cell->getValue();
                            break;
                        case 'K':
                            $assetOperation['comment'] = (string)$cell->getValue();
                            break;
                        case 'O':
                            $assetOperation['amount'] = (float)$cell->getValue();
                            break;
                    }
                }
                $assetOperation['id'] = md5(
                    $assetOperation['date']->getTimestamp() . $assetOperation['comment'] . $assetOperation['amount']
                );
                $operation = $this->convertToBrokerOperation($type, $assetOperation);
                if ($operation !== null) {
                    $operations->add($operation);
                }
            }
        }

        return $operations;
    }

    private function convertToBrokerOperation(string $type, array $operation): ?BrokerOperationModel
    {
        if (!in_array($type, ['Перевод', 'НДФЛ'])) {
            return null;
        }

        $model = new BrokerOperationModel([
            'id' => $operation['id'],
            'currency' => 'RUB',
            'status' => BrokerOperationStatus::DONE->value,
            'broker' => self::BROKER,
            'date' => $operation['date'],
            'quantity' => null,
            'payment' => new BrokerMoneyModel([
                'amount' => $operation['amount'],
                'currency' => 'RUB'
            ])
        ]);

        if ($type === 'Перевод') {
            if (preg_match('/^погашение купона (\S+)/', $operation['comment'], $matches)) {
                $regNumber = $matches[1];
                $instrument = $this->marketInstrumentService->getInstrumentByRegNumber($regNumber);
                if ($instrument === null) {
                    $model = null;
                } else {
                    $model->setIsin($instrument->getIsin());
                    $model->setInstrumentType($instrument->getType());
                    $model->setType(OperationType::DIVIDEND->value);
                }
            } if (mb_strpos($operation['comment'], "Дивиденды за") === 0) {
                $pattern = '/\(([^)]+)\)/';

                $regNumber = '';
                if (preg_match_all($pattern, $operation['comment'], $matches)) {
                    $regNumber = $matches[1][0];
                }
                $instrument = $this->marketInstrumentService->getInstrumentByRegNumber($regNumber);
                if ($instrument === null) {
                    $model = null;
                } else {
                    $model->setIsin($instrument->getIsin());
                    $model->setInstrumentType($instrument->getType());
                    $model->setType(OperationType::DIVIDEND->value);
                }
            } else {
                $model->setType(OperationType::INPUT->value);
            }
        } elseif ($type === 'НДФЛ') {
            $model->setType(OperationType::TAX->value);
        }

        return $model;
    }
}
