<?php

declare(strict_types=1);

namespace App\Broker\Services\Adapters;

use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Models\BrokerAccountModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use App\User\Exceptions\UserAccessDeniedException;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use Slim\Psr7\UploadedFile;
use Tinkoff\Invest\V1\MoneyValue;
use Tinkoff\Invest\V1\Operation;
use Tinkoff\Invest\V1\PortfolioPosition;

class TinkoffInvest implements UserBrokerInterface
{
    private const string BROKER = 'tinkoff2';

    public function __construct(
        private readonly TIClient $client,
        private readonly SourceBrokerInterface $sourceBroker,
        private readonly TinkoffHelper $tinkoffHelper
    ) {
    }

    #[\Override] public function manualOperationImport(UploadedFile $file): OperationCollection
    {
        // TODO: Implement manualOperationImport() method.
    }

    #[\Override] public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection
    {
        $operations = $this->client->getOperations()->getOperations($accountId, $from, $to);

        $collection = new OperationCollection();
        /** @var Operation $operation */
        foreach ($operations->getIterator() as $operation) {
            $isin = null;
            $errorCode = null;
            if ($operation->getInstrumentUid() !== '') {
                try {
                    $isin = $this->sourceBroker->getOrAddInstrument(
                        $operation->getInstrumentUid(),
                        null,
                        $operation->getFigi()
                    )->getIsin();
                } catch (TIException $e) {
                    if ($e->getCode() === 50002) {
                        $errorCode = $e->getCode();
                    } else {
                        throw $e;
                    }
                }
            }

            $data = [
                'id' => $operation->getId(),
                'parentOperationId' => $operation->getParentOperationId() === ''
                    ? null
                    : $operation->getParentOperationId(),
                'currency' => $this->tinkoffHelper->getCurrencyISO($operation->getCurrency()),
                'payment' => new BrokerMoneyModel([
                    'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($operation->getPayment()),
                    'currency' => $this->tinkoffHelper->getCurrencyISO($operation->getPayment()->getCurrency())
                ]),
                'price' => $operation->getPrice() === null
                || $operation->getPrice()->getUnits() === 0 && $operation->getPrice()->getNano() === 0
                    ? null
                    : new BrokerMoneyModel([
                        'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($operation->getPrice()),
                        'currency' => $this->tinkoffHelper->getCurrencyISO($operation->getPrice()->getCurrency())
                    ]),
                'status' => $this->tinkoffHelper->getOperationStatus($operation->getState())->value,
                'quantity' => $operation->getQuantity() === 0
                    ? null
                    : $operation->getQuantity() - $operation->getQuantityRest(),
                'isin' => $isin,
                'date' => $this->tinkoffHelper->convertGoogleTimestampToDate($operation->getDate()),
                'type' => $this->tinkoffHelper->getOperationType($operation->getOperationType())->value,
                'instrumentType' => $operation->getInstrumentType() === ''
                    ? null
                    : $this->tinkoffHelper->getInstrumentType($operation->getInstrumentType()),
                'broker' => self::BROKER,
                'errorCode' => $errorCode
            ];
            $collection->add(new BrokerOperationModel($data));
        }
        return $collection;
    }

    #[\Override] public function getAccounts(): AccountCollection
    {
        $result = [];
        try {
            $accounts = $this->client->getUser()->getAccounts();
        } catch (TIException $e) {
            throw new UserAccessDeniedException($e->getMessage(), 2023);
        }

        foreach ($accounts as $account) {
            $openedDate = $account->getOpenedDate()->getSeconds() === 0
                ? null
                : $this->tinkoffHelper->convertGoogleTimestampToDate($account->getOpenedDate());
            $closedDate = $account->getClosedDate()->getSeconds() === 0
                ? null
                : $this->tinkoffHelper->convertGoogleTimestampToDate($account->getClosedDate());
            $data = [
                'id' => $account->getId(),
                'type' => BrokerAccountTypeType::from($account->getType()),
                'name' => $account->getName(),
                'openedDate' => $openedDate,
                'closedDate' => $closedDate,
                'status' => BrokerAccountStatusType::from($account->getStatus())
            ];
            $result[] = new BrokerAccountModel($data);
        }

        return new AccountCollection($result);
    }

    #[\Override] public function getPortfolio(string $accountId): BrokerPortfolioModel
    {
        $positions = [];

        $tiPortfolio = $this->client->getOperations()->getPortfolio($accountId)->getPositions();

        /** @var PortfolioPosition $instrument */
        foreach ($tiPortfolio as $instrument) {
            try {
                $tiInstrument = $this->sourceBroker->getOrAddInstrument($instrument->getInstrumentUid());
            } catch (TIException $e) {
                if ($e->getCode() === 50002) {
                    continue;
                } else {
                    throw $e;
                }
            }


            $currency = $instrument->getAveragePositionPrice()->getCurrency() !== ''
                ? $instrument->getAveragePositionPrice()->getCurrency()
                : $instrument->getCurrentPrice()->getCurrency();

            $expectedYield = [
                'currency' => $this->tinkoffHelper->getCurrencyISO($currency),
                'amount' => $this->tinkoffHelper->convertQuotationToFloat($instrument->getExpectedYield())
            ];

            $averagePositionPrice = [
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($instrument->getAveragePositionPrice()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($currency)
            ];

            $currentPrice = [
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($instrument->getCurrentPrice()),
                'currency' => $this->tinkoffHelper->getCurrencyISO($instrument->getCurrentPrice()->getCurrency())
            ];

            $currentNkd = null;
            if ($instrument->getCurrentNkd() !== null && $instrument->getCurrentNkd()->getCurrency() !== '') {
                $currentNkd = new BrokerMoneyModel([
                    'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($instrument->getCurrentNkd()),
                    'currency' => $this->tinkoffHelper->getCurrencyISO($instrument->getCurrentNkd()->getCurrency())
                ]);
            }

            $data = [
                'instrumentType' => $this->tinkoffHelper->getInstrumentType($instrument->getInstrumentType()),
                'isin' => $tiInstrument->getIsin(),
                'quantity' => $this->tinkoffHelper->convertQuotationToFloat($instrument->getQuantity()),
                'averagePositionPrice' => new BrokerMoneyModel($averagePositionPrice),
                'expectedYield' => new BrokerMoneyModel($expectedYield),
                'currentNkd' => $currentNkd,
                'currentPrice' => new BrokerMoneyModel($currentPrice)
            ];
            $positions[] = new BrokerPortfolioPositionModel($data);
        }

        $portfolio = new BrokerPortfolioModel();
        $portfolio->setPositions(new PortfolioPositionCollection($positions));

        return $portfolio;
    }

    #[\Override] public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection
    {
        $currencies = $this->client->getOperations()->getWithdrawLimits($accountId);

        $currencyCollection = new PortfolioBalanceCollection();
        /** @var MoneyValue $currency */
        foreach ($currencies->getMoney() as $currency) {
            $data = [
                'amount' => $this->tinkoffHelper->convertMoneyValueToFloat($currency),
                'currency' => $this->tinkoffHelper->getCurrencyISO($currency->getCurrency()),
                'blockedAmount' => 0
            ];

            /** @var MoneyValue $blockedCurrency */
            foreach ($currencies->getBlocked() as $blockedCurrency) {
                if ($blockedCurrency->getCurrency() === $currency->getCurrency()) {
                    $data['blockedAmount'] = $this->tinkoffHelper->convertMoneyValueToFloat($blockedCurrency);
                }
            }
            $currencyCollection->add(new BrokerPortfolioBalanceModel($data));
        }
        return $currencyCollection;
    }
}
