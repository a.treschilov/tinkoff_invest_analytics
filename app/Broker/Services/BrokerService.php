<?php

declare(strict_types=1);

namespace App\Broker\Services;

use App\Item\Collections\BrokerCollection;
use App\Broker\Entities\BrokerEntity;
use App\Broker\Models\BrokerModel;
use App\Broker\Storages\BrokerStorage;

class BrokerService
{
    public function __construct(
        private readonly BrokerStorage $brokerStorage,
        private readonly BrokerHelper $brokerHelper
    ) {
    }

    public function getActiveBrokerList(): BrokerCollection
    {
        $brokers = new BrokerCollection();

        $brokerEntities = $this->brokerStorage->findActive();
        foreach ($brokerEntities as $entity) {
            $brokers->add($this->brokerHelper->convertBrokerEntityToModel($entity));
        }

        return $brokers;
    }

    public function getBrokerById(string $id): ?BrokerModel
    {
        $broker = $this->brokerStorage->findById($id);

        return $broker === null ? null : $this->brokerHelper->convertBrokerEntityToModel($broker);
    }

    public function getBrokerEntityById(string $id): ?BrokerEntity
    {
        return $this->brokerStorage->findById($id);
    }
}
