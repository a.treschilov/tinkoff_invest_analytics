<?php

declare(strict_types=1);

namespace App\Broker\Services;

use App\Broker\Entities\BrokerEntity;
use App\Broker\Models\BrokerModel;

class BrokerHelper
{
    public function convertBrokerEntityToModel(BrokerEntity $broker): BrokerModel
    {
        return new BrokerModel([
            'brokerId' => $broker->getId(),
            'isActive' => $broker->getIsActive(),
            'name' => $broker->getName(),
            'logo' => $broker->getLogo(),
            'isApi' => (bool)$broker->getIsApi(),
            'isManual' => (bool)$broker->getIsManual(),
            'instructionLink' => $broker->getInstructionLink()
        ]);
    }
}
