<?php

declare(strict_types=1);

namespace App\Broker\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.broker')]
#[ORM\Entity]
class BrokerEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'broker_id', type: 'string')]
    protected string $id;

    #[ORM\Column(name: 'name', type: 'string')]
    protected string $name;

    #[ORM\Column(name: 'logo', type: 'string')]
    protected ?string $logo = null;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    #[ORM\Column(name: 'is_api', type: 'integer')]
    protected int $isApi = 0;

    #[ORM\Column(name: 'is_manual', type: 'integer')]
    protected int $isManual = 0;

    #[ORM\Column(name: 'instruction_link', type: 'string')]
    protected ?string $instructionLink = null;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getIsApi(): int
    {
        return $this->isApi;
    }

    public function setIsApi(int $isApi): void
    {
        $this->isApi = $isApi;
    }

    public function getIsManual(): int
    {
        return $this->isManual;
    }

    public function setIsManual(int $isManual): void
    {
        $this->isManual = $isManual;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getInstructionLink(): ?string
    {
        return $this->instructionLink;
    }

    public function setInstructionLink(?string $instructionLink): void
    {
        $this->instructionLink = $instructionLink;
    }
}
