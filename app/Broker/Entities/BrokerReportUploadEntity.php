<?php

declare(strict_types=1);

namespace App\Broker\Entities;

use App\Broker\Types\BrokerReportUploadStatusType;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.broker_report_upload')]
#[ORM\Entity]
class BrokerReportUploadEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'broker_report_upload_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'broker_id', type: 'string')]
    protected string $brokerId;

    #[ORM\JoinColumn(name: 'broker_id', referencedColumnName: 'broker_id')]
    #[ORM\ManyToOne(targetEntity: BrokerEntity::class)]
    protected BrokerEntity $broker;

    #[ORM\Column(name: 'added', type: 'integer')]
    protected int $added;

    #[ORM\Column(name: 'updated', type: 'integer')]
    protected int $updated;

    #[ORM\Column(name: 'skipped', type: 'integer')]
    protected int $skipped;

    #[ORM\Column(name: 'status', type: 'string', enumType: BrokerReportUploadStatusType::class)]
    protected BrokerReportUploadStatusType $status;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    public function getAdded(): int
    {
        return $this->added;
    }

    public function setAdded(int $added): void
    {
        $this->added = $added;
    }

    public function getUpdated(): int
    {
        return $this->updated;
    }

    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    public function getSkipped(): int
    {
        return $this->skipped;
    }

    public function setSkipped(int $skipped): void
    {
        $this->skipped = $skipped;
    }

    public function getStatus(): BrokerReportUploadStatusType
    {
        return $this->status;
    }

    public function setStatus(BrokerReportUploadStatusType $status): void
    {
        $this->status = $status;
    }

    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    public function getBroker(): BrokerEntity
    {
        return $this->broker;
    }

    public function setBroker(BrokerEntity $broker): void
    {
        $this->broker = $broker;
    }
}
