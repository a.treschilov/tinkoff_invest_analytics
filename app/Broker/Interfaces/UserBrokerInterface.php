<?php

declare(strict_types=1);

namespace App\Broker\Interfaces;

use App\Broker\Collections\AccountCollection;
use App\Broker\Collections\OperationCollection;
use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioModel;
use Slim\Psr7\UploadedFile;

interface UserBrokerInterface
{
    public function manualOperationImport(UploadedFile $file): OperationCollection;
    public function getOperations(string $accountId, \DateTime $from, \DateTime $to): OperationCollection;
    public function getAccounts(): AccountCollection;
    public function getPortfolio(string $accountId): BrokerPortfolioModel;
    public function getPortfolioBalance(string $accountId): PortfolioBalanceCollection;
}
