<?php

declare(strict_types=1);

namespace App\Broker\Interfaces;

use App\Broker\Collections\BondCouponCollection;
use App\Broker\Collections\BondCollection;
use App\Broker\Collections\BrokerCurrencyCollection;
use App\Broker\Collections\FutureCollection;
use App\Broker\Collections\InstrumentCollection;
use App\Broker\Collections\ShareCollection;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerCurrencyModel;
use App\Broker\Models\BrokerFutureModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerShareModel;
use App\Collections\CandleCollection;
use App\Market\Types\CandleIntervalType;

interface SourceBrokerInterface
{
    public function getInstrumentByIsin(string $isin): InstrumentCollection;
    public function getCurrencyByIsin(string $isin): ?BrokerCurrencyModel;
    public function getCurrencies(): BrokerCurrencyCollection;
    public function getHistoryCandles(
        string $isin,
        \DateTime $from,
        \DateTime $to,
        CandleIntervalType $interval
    ): ?CandleCollection;
    public function getLastPrices(array $isinList): array;
    public function getShares(): ShareCollection;
    public function getShareByIsin(string $isin): ?BrokerShareModel;
    public function getBonds(): BondCollection;
    public function getBondByIsin(string $isin): ?BrokerBondModel;
    public function getBondCouponsIsin(string $isin, \DateTime $from, \DateTime $to): ?BondCouponCollection;
    public function getFutures(): FutureCollection;
    public function getFutureByIsin(string $isin): ?BrokerFutureModel;
    public function setInstrumentsForUpdate(int $userId): void;
    public function updateInstrument(string $instrumentId): ?BrokerInstrumentModel;
    public function getIndicatives(): InstrumentCollection;
}
