<?php

declare(strict_types=1);

namespace App\Broker\Storages;

use App\Broker\Entities\BrokerEntity;
use Doctrine\ORM\EntityManagerInterface;

class BrokerStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findById(string $id): ?BrokerEntity
    {
        return $this->entityManager->getRepository(BrokerEntity::class)
            ->findOneBy(['id' => $id]);
    }

    /**
     * @return BrokerEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(BrokerEntity::class)
            ->findBy(['isActive' => 1]);
    }
}
