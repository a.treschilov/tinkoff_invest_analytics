<?php

declare(strict_types=1);

namespace App\Broker\Storages;

use App\Broker\Entities\BrokerReportUploadEntity;
use Doctrine\ORM\EntityManager;

class BrokerReportUploadStorage
{
    public function __construct(
        private readonly EntityManager $entityManager
    ) {
    }

    public function addEntity(BrokerReportUploadEntity $entity): int
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param int $userId
     * @return BrokerReportUploadEntity[]
     */
    public function findByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(BrokerReportUploadEntity::class)
            ->findBy(['userId' => $userId], ['date' => 'DESC']);
    }
}
