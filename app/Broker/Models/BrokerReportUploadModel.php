<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Broker\Types\BrokerReportUploadStatusType;
use App\Broker\Types\BrokerType;
use App\Common\Models\BaseModel;

class BrokerReportUploadModel extends BaseModel
{
    private ?int $brokerUploadReportId = null;
    private int $userId;
    private \DateTime $date;
    private BrokerType $brokerId;
    private BrokerModel $broker;
    private int $added;
    private int $updated;
    private int $skipped;
    private BrokerReportUploadStatusType $status;

    public function getBrokerUploadReportId(): ?int
    {
        return $this->brokerUploadReportId;
    }

    public function setBrokerUploadReportId(?int $brokerUploadReportId = null): void
    {
        $this->brokerUploadReportId = $brokerUploadReportId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    public function getBrokerId(): BrokerType
    {
        return $this->brokerId;
    }

    public function setBrokerId(BrokerType $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    public function getAdded(): int
    {
        return $this->added;
    }

    public function setAdded(int $added): void
    {
        $this->added = $added;
    }

    public function getUpdated(): int
    {
        return $this->updated;
    }

    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    public function getSkipped(): int
    {
        return $this->skipped;
    }

    public function setSkipped(int $skipped): void
    {
        $this->skipped = $skipped;
    }

    public function getStatus(): BrokerReportUploadStatusType
    {
        return $this->status;
    }

    public function setStatus(BrokerReportUploadStatusType $status): void
    {
        $this->status = $status;
    }

    public function getBroker(): BrokerModel
    {
        return $this->broker;
    }

    public function setBroker(BrokerModel $broker): void
    {
        $this->broker = $broker;
    }
}
