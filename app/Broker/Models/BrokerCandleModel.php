<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;
use App\Market\Types\CandleIntervalType;

class BrokerCandleModel extends BaseModel
{
    private float $open;
    private float $close;
    private float $high;
    private float $low;
    private int $volume;
    private \DateTime $time;
    private ?bool $isComplete = null;
    private string $currency;
    private string $interval = CandleIntervalType::DAY_1->value;
    private int $marketInstrumentId;

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     */
    public function setOpen(float $open): void
    {
        $this->open = $open;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     */
    public function setClose(float $close): void
    {
        $this->close = $close;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     */
    public function setHigh(float $high): void
    {
        $this->high = $high;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     */
    public function setLow(float $low): void
    {
        $this->low = $low;
    }

    /**
     * @return int
     */
    public function getVolume(): int
    {
        return $this->volume;
    }

    /**
     * @param int $volume
     */
    public function setVolume(int $volume): void
    {
        $this->volume = $volume;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time): void
    {
        $this->time = $time;
    }

    /**
     * @return bool|null
     */
    public function getIsComplete(): ?bool
    {
        return $this->isComplete;
    }

    /**
     * @param bool|null $isComplete
     */
    public function setIsComplete(?bool $isComplete): void
    {
        $this->isComplete = $isComplete;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getInterval(): string
    {
        return $this->interval;
    }

    public function setInterval(string $interval): void
    {
        $this->interval = $interval;
    }

    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }
}
