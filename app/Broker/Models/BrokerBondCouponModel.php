<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerBondCouponModel extends BaseModel
{
    private string $isin;
    private \DateTime $date;
    private float $amount;
    private string $currency;
    private ?BrokerMoneyModel $amortization = null;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }

    public function getAmortization(): ?BrokerMoneyModel
    {
        return $this->amortization;
    }

    public function setAmortization(?BrokerMoneyModel $amortization): void
    {
        $this->amortization = $amortization;
    }
}
