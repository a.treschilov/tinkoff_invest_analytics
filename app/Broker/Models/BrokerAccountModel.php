<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Broker\Types\BrokerAccountStatusType;
use App\Broker\Types\BrokerAccountTypeType;
use App\Common\Models\BaseModel;

class BrokerAccountModel extends BaseModel
{
    private string $id;
    private BrokerAccountTypeType $type;
    private string|null $name = null;
    private BrokerAccountStatusType $status;
    private \DateTime|null $openedDate = null;
    private \DateTime|null $closedDate = null;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return BrokerAccountTypeType
     */
    public function getType(): BrokerAccountTypeType
    {
        return $this->type;
    }

    /**
     * @param BrokerAccountTypeType $type
     */
    public function setType(BrokerAccountTypeType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return BrokerAccountStatusType
     */
    public function getStatus(): BrokerAccountStatusType
    {
        return $this->status;
    }

    /**
     * @param BrokerAccountStatusType $status
     */
    public function setStatus(BrokerAccountStatusType $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime|null
     */
    public function getOpenedDate(): ?\DateTime
    {
        return $this->openedDate;
    }

    /**
     * @param \DateTime|null $openedDate
     */
    public function setOpenedDate(?\DateTime $openedDate): void
    {
        $this->openedDate = $openedDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedDate(): ?\DateTime
    {
        return $this->closedDate;
    }

    /**
     * @param \DateTime|null $closedDate
     */
    public function setClosedDate(?\DateTime $closedDate): void
    {
        $this->closedDate = $closedDate;
    }
}
