<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerShareModel extends BaseModel
{
    private BrokerInstrumentModel $instrument;
    private ?string $sector = null;
    private ?string $country = null;
    private ?string $industry = null;

    /**
     * @return string|null
     */
    public function getSector(): ?string
    {
        return $this->sector;
    }

    /**
     * @param string|null $sector
     */
    public function setSector(?string $sector): void
    {
        $this->sector = $sector;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    /**
     * @param string|null $industry
     */
    public function setIndustry(?string $industry): void
    {
        $this->industry = $industry;
    }

    /**
     * @return BrokerInstrumentModel
     */
    public function getInstrument(): BrokerInstrumentModel
    {
        return $this->instrument;
    }

    /**
     * @param BrokerInstrumentModel $instrument
     */
    public function setInstrument(BrokerInstrumentModel $instrument): void
    {
        $this->instrument = $instrument;
    }
}
