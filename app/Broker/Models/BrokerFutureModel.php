<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerFutureModel extends BaseModel
{
    private \DateTime $firstTradeDate;
    private \DateTime $lastTradeDate;
    private ?string $futureType = null;
    private ?string $assetType = null;
    private BrokerMoneyModel $initialMarginOnBuy;
    private BrokerMoneyModel $initialMarginOnSell;
    private ?float $minPriceIncrement = null;
    private ?float $minPriceIncrementAmount = null;
    private string $ticker;
    private \DateTime $expirationDate;

    /**
     * @return \DateTime
     */
    public function getFirstTradeDate(): \DateTime
    {
        return $this->firstTradeDate;
    }

    /**
     * @param \DateTime $firstTradeDate
     */
    public function setFirstTradeDate(\DateTime $firstTradeDate): void
    {
        $this->firstTradeDate = $firstTradeDate;
    }

    /**
     * @return \DateTime
     */
    public function getLastTradeDate(): \DateTime
    {
        return $this->lastTradeDate;
    }

    /**
     * @param \DateTime $lastTradeDate
     */
    public function setLastTradeDate(\DateTime $lastTradeDate): void
    {
        $this->lastTradeDate = $lastTradeDate;
    }

    public function getFutureType(): ?string
    {
        return $this->futureType;
    }

    public function setFutureType(?string $futureType): void
    {
        $this->futureType = $futureType;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getInitialMarginOnBuy(): BrokerMoneyModel
    {
        return $this->initialMarginOnBuy;
    }

    /**
     * @param BrokerMoneyModel $initialMarginOnBuy
     */
    public function setInitialMarginOnBuy(BrokerMoneyModel $initialMarginOnBuy): void
    {
        $this->initialMarginOnBuy = $initialMarginOnBuy;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getInitialMarginOnSell(): BrokerMoneyModel
    {
        return $this->initialMarginOnSell;
    }

    /**
     * @param BrokerMoneyModel $initialMarginOnSell
     */
    public function setInitialMarginOnSell(BrokerMoneyModel $initialMarginOnSell): void
    {
        $this->initialMarginOnSell = $initialMarginOnSell;
    }

    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    public function setMinPriceIncrement(?float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    public function getMinPriceIncrementAmount(): ?float
    {
        return $this->minPriceIncrementAmount;
    }

    public function setMinPriceIncrementAmount(?float $minPriceIncrementAmount): void
    {
        $this->minPriceIncrementAmount = $minPriceIncrementAmount;
    }

    public function getExpirationDate(): \DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getTicker(): string
    {
        return $this->ticker;
    }

    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    public function getAssetType(): ?string
    {
        return $this->assetType;
    }

    public function setAssetType(?string $assetType): void
    {
        $this->assetType = $assetType;
    }
}
