<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerInstrumentModel extends BaseModel
{
    private string $ticker;
    private string $exchange;
    private string $isin;
    private ?float $minPriceIncrement = null;
    private int $lot;
    private string $currency;
    private string $name;
    private string $type;
    private ?string $logo = null;
    private ?string $regNumber = null;
    private ?\DateTime $firstCandleDate = null;
    private ?BrokerCurrencyModel $currencyModel = null;
    private ?BrokerBondModel $bondModel = null;

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return float|null
     */
    public function getMinPriceIncrement(): ?float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float|null $minPriceIncrement
     */
    public function setMinPriceIncrement(?float $minPriceIncrement = null): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return int
     */
    public function getLot(): int
    {
        return $this->lot;
    }

    /**
     * @param int $lot
     */
    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return BrokerCurrencyModel|null
     */
    public function getCurrencyModel(): ?BrokerCurrencyModel
    {
        return $this->currencyModel;
    }

    /**
     * @param BrokerCurrencyModel|null $currencyModel
     */
    public function setCurrencyModel(?BrokerCurrencyModel $currencyModel): void
    {
        $this->currencyModel = $currencyModel;
    }

    /**
     * @return BrokerBondModel|null
     */
    public function getBondModel(): ?BrokerBondModel
    {
        return $this->bondModel;
    }

    /**
     * @param BrokerBondModel|null $bondModel
     */
    public function setBondModel(?BrokerBondModel $bondModel): void
    {
        $this->bondModel = $bondModel;
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @param string $exchange
     */
    public function setExchange(string $exchange): void
    {
        $this->exchange = $exchange;
    }

    /**
     * @return \DateTime|null
     */
    public function getFirstCandleDate(): ?\DateTime
    {
        return $this->firstCandleDate;
    }

    /**
     * @param \DateTime|null $firstCandleDate
     */
    public function setFirstCandleDate(?\DateTime $firstCandleDate): void
    {
        $this->firstCandleDate = $firstCandleDate;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): void
    {
        $this->regNumber = $regNumber;
    }
}
