<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerBondModel extends BaseModel
{
    private string $isin;
    private BrokerMoneyModel $nominal;
    private BrokerMoneyModel $initialNominal;
    private ?\DateTime $maturityDate = null;

    /**
     * @return BrokerMoneyModel
     */
    public function getNominal(): BrokerMoneyModel
    {
        return $this->nominal;
    }

    /**
     * @param BrokerMoneyModel $nominal
     */
    public function setNominal(BrokerMoneyModel $nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getInitialNominal(): BrokerMoneyModel
    {
        return $this->initialNominal;
    }

    /**
     * @param BrokerMoneyModel $initialNominal
     */
    public function setInitialNominal(BrokerMoneyModel $initialNominal): void
    {
        $this->initialNominal = $initialNominal;
    }

    /**
     * @return \DateTime|null
     */
    public function getMaturityDate(): ?\DateTime
    {
        return $this->maturityDate;
    }

    /**
     * @param \DateTime|null $maturityDate
     */
    public function setMaturityDate(?\DateTime $maturityDate): void
    {
        $this->maturityDate = $maturityDate;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }
}
