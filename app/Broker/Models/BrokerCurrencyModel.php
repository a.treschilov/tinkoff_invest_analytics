<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerCurrencyModel extends BaseModel
{
    private string $iso;
    private float $nominal;

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    /**
     * @return float
     */
    public function getNominal(): float
    {
        return $this->nominal;
    }

    /**
     * @param float $nominal
     */
    public function setNominal(float $nominal): void
    {
        $this->nominal = $nominal;
    }
}
