<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerModel extends BaseModel
{
    private string $brokerId;
    private int $isActive;
    private string $name;
    private ?string $logo = null;
    private bool $isApi = false;
    private bool $isManual = false;
    private ?string $instructionLink = null;

    /**
     * @return string
     */
    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    /**
     * @param string $brokerId
     */
    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isApi(): bool
    {
        return $this->isApi;
    }

    public function setIsApi(bool $isApi): void
    {
        $this->isApi = $isApi;
    }

    public function isManual(): bool
    {
        return $this->isManual;
    }

    public function setIsManual(bool $isManual): void
    {
        $this->isManual = $isManual;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getInstructionLink(): ?string
    {
        return $this->instructionLink;
    }

    public function setInstructionLink(?string $instructionLink): void
    {
        $this->instructionLink = $instructionLink;
    }
}
