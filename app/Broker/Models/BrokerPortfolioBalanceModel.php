<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerPortfolioBalanceModel extends BaseModel
{
    private string $currency;
    private float $amount;
    private float $blockedAmount;

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getBlockedAmount(): float
    {
        return $this->blockedAmount;
    }

    /**
     * @param float $blockedAmount
     */
    public function setBlockedAmount(float $blockedAmount): void
    {
        $this->blockedAmount = $blockedAmount;
    }
}
