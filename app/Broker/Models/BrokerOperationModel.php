<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerOperationModel extends BaseModel
{
    private string $id;
    private ?string $parentOperationId = null;
    private string $currency;
    private BrokerMoneyModel $payment;
    private ?BrokerMoneyModel $price = null;
    private string $status;
    private ?int $quantity = null;
    private ?string $isin = null;
    private \DateTime $date;
    private string $type;
    private ?string $instrumentType = null;
    private ?string $broker = null;
    private ?int $errorCode = null;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getParentOperationId(): ?string
    {
        return $this->parentOperationId;
    }

    /**
     * @param string|null $parentOperationId
     */
    public function setParentOperationId(?string $parentOperationId): void
    {
        $this->parentOperationId = $parentOperationId;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getPayment(): BrokerMoneyModel
    {
        return $this->payment;
    }

    /**
     * @param BrokerMoneyModel $payment
     */
    public function setPayment(BrokerMoneyModel $payment): void
    {
        $this->payment = $payment;
    }

    /**
     * @return BrokerMoneyModel|null
     */
    public function getPrice(): ?BrokerMoneyModel
    {
        return $this->price;
    }

    /**
     * @param BrokerMoneyModel|null $price
     */
    public function setPrice(?BrokerMoneyModel $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getInstrumentType(): ?string
    {
        return $this->instrumentType;
    }

    /**
     * @param string|null $instrumentType
     */
    public function setInstrumentType(?string $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    /**
     * @return string|null
     */
    public function getBroker(): ?string
    {
        return $this->broker;
    }

    /**
     * @param string|null $broker
     */
    public function setBroker(?string $broker): void
    {
        $this->broker = $broker;
    }

    /**
     * @return string|null
     */
    public function getIsin(): ?string
    {
        return $this->isin;
    }

    /**
     * @param string|null $isin
     */
    public function setIsin(?string $isin): void
    {
        $this->isin = $isin;
    }

    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    public function setErrorCode(?int $errorCode): void
    {
        $this->errorCode = $errorCode;
    }
}
