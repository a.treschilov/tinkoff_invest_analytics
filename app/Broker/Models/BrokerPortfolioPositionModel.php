<?php

declare(strict_types=1);

namespace App\Broker\Models;

use App\Common\Models\BaseModel;

class BrokerPortfolioPositionModel extends BaseModel
{
    private string $isin;
    private string $instrumentType;
    private float $quantity;
    private BrokerMoneyModel $averagePositionPrice;
    private BrokerMoneyModel $expectedYield;
    private ?BrokerMoneyModel $currentNkd = null;
    private ?BrokerMoneyModel $currentPrice = null;

    /**
     * @return string
     */
    public function getInstrumentType(): string
    {
        return $this->instrumentType;
    }

    /**
     * @param string $instrumentType
     */
    public function setInstrumentType(string $instrumentType): void
    {
        $this->instrumentType = $instrumentType;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getAveragePositionPrice(): BrokerMoneyModel
    {
        return $this->averagePositionPrice;
    }

    /**
     * @param BrokerMoneyModel $averagePositionPrice
     */
    public function setAveragePositionPrice(BrokerMoneyModel $averagePositionPrice): void
    {
        $this->averagePositionPrice = $averagePositionPrice;
    }

    /**
     * @return BrokerMoneyModel
     */
    public function getExpectedYield(): BrokerMoneyModel
    {
        return $this->expectedYield;
    }

    /**
     * @param BrokerMoneyModel $expectedYield
     */
    public function setExpectedYield(BrokerMoneyModel $expectedYield): void
    {
        $this->expectedYield = $expectedYield;
    }

    /**
     * @return BrokerMoneyModel|null
     */
    public function getCurrentNkd(): ?BrokerMoneyModel
    {
        return $this->currentNkd;
    }

    /**
     * @param BrokerMoneyModel|null $currentNkd
     */
    public function setCurrentNkd(?BrokerMoneyModel $currentNkd): void
    {
        $this->currentNkd = $currentNkd;
    }

    /**
     * @return BrokerMoneyModel|null
     */
    public function getCurrentPrice(): ?BrokerMoneyModel
    {
        return $this->currentPrice;
    }

    /**
     * @param BrokerMoneyModel|null $currentPrice
     */
    public function setCurrentPrice(?BrokerMoneyModel $currentPrice): void
    {
        $this->currentPrice = $currentPrice;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }
}
