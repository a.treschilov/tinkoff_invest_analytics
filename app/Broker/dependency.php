<?php

declare(strict_types=1);

use App\Adapters\TIClientV2Adapter;
use App\Broker\Services\Adapters\TinkoffHelper;
use App\Broker\Services\Adapters\TinkoffInvest;
use App\Broker\Services\BrokerHelper;
use App\Broker\Services\BrokerReportUploadService;
use App\Broker\Services\BrokerService;
use App\Broker\Storages\BrokerReportUploadStorage;
use App\Broker\Storages\BrokerStorage;
use ATreschilov\TinkoffInvestApiSdk\TIClient;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(TinkoffInvest::class, function (ContainerInterface $c) {
    return new TinkoffInvest(
        $c->get(TIClient::class),
        $c->get(TIClientV2Adapter::class),
        $c->get(TinkoffHelper::class)
    );
});

$container->set(TinkoffHelper::class, function () {
    return new TinkoffHelper();
});

$container->set(BrokerHelper::class, function () {
    return new BrokerHelper();
});

$container->set(BrokerStorage::class, function (ContainerInterface $c) {
    return new BrokerStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(BrokerReportUploadStorage::class, function (ContainerInterface $c) {
    return new BrokerReportUploadStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(BrokerService::class, function (ContainerInterface $c) {
    return new BrokerService(
        $c->get(BrokerStorage::class),
        $c->get(BrokerHelper::class)
    );
});

$container->set(BrokerReportUploadService::class, function (ContainerInterface $c) {
    return new BrokerReportUploadService(
        $c->get(BrokerReportUploadStorage::class),
        $c->get(BrokerService::class),
        $c->get(BrokerHelper::class)
    );
});
