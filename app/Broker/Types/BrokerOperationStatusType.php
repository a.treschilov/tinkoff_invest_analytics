<?php

declare(strict_types=1);

namespace App\Broker\Types;

/** @deprecated Use BrokerOperationStatus instead */
class BrokerOperationStatusType
{
    public const OPERATION_STATE_UNSPECIFIED = 'Unspecified';
    public const OPERATION_STATE_DONE = 'Done';
    public const OPERATION_STATE_DECLINED = 'Decline';
    public const OPERATION_STATE_PROGRESS = 'Progress';
}
