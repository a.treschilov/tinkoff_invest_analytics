<?php

declare(strict_types=1);

namespace App\Broker\Types;

enum BrokerType: string
{
    case TINKOFF = 'tinkoff2';
    case ALFA_INVEST = 'alfa_invest';
    case JETLEND = 'jetlend';
    case POTOK = 'potok';
    case MONEY_FRIENDS = 'money_friends';
    case MOEX = 'moex';
    case SBER = 'sber';
}
