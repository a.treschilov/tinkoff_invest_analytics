<?php

declare(strict_types=1);

namespace App\Broker\Types;

enum BrokerOperationStatus: string
{
    case UNSPECIFIED = 'Unspecified';
    case DONE = 'Done';
    case DECLINED = 'Decline';
    case PROGRESS = 'Progress';
}
