<?php

declare(strict_types=1);

namespace App\Broker\Types;

enum BrokerAccountTypeType: int
{
    case ACCOUNT_TYPE_UNSPECIFIED = 0; // Тип аккаунта не определён
    case ACCOUNT_TYPE_GENERAL = 1; // Брокерский счёт
    case ACCOUNT_TYPE_IIS = 2; // ИИС
    case ACCOUNT_TYPE_INVEST_BOX = 3; // Инвесткопилка
}
