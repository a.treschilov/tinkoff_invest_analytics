<?php

declare(strict_types=1);

namespace App\Broker\Types;

class BrokerInstrumentType
{
    public const STOCK = 'Stock';
    public const ETF = 'Etf';
    public const BOND = 'Bond';
    public const CURRENCY = 'Currency';
    public const FUTURES = 'Futures';
}
