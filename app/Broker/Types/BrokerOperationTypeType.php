<?php

declare(strict_types=1);

namespace App\Broker\Types;

/** @deprecated Use OperationType instead */
class BrokerOperationTypeType
{
    public const OPERATION_TYPE_UNSPECIFIED = 'Unspecified'; // Тип операции не определен
    public const OPERATION_TYPE_INPUT = 'Input'; // Пополнение денежных средств
    public const OPERATION_TYPE_OUTPUT = 'Output'; // Вывод денежных средств
    public const OPERATION_TYPE_SECURITY_IN = 'SecurityIn'; // Пополнение счета ценными бумагами
    public const OPERATION_TYPE_SECURITY_OUT = 'SecurityOut'; // Вывод со счета ценных бумаг
    public const OPERATION_TYPE_BUY = 'Buy'; // Покупка ЦБ
    public const OPERATION_TYPE_BUY_CARD = 'BuyCard'; // Покупка ЦБ с карты
    public const OPERATION_TYPE_SELL = 'Sell'; // Продажа
    public const OPERATION_TYPE_SELL_CARD = 'SellCard'; // Продажа на внешний счет
    public const OPERATION_TYPE_REPAYMENT = 'Repayment'; // Полное/частичное погашение ценной бумаги
    public const OPERATION_TYPE_DIVIDEND = 'Dividend'; //Доход ценной бумаги
    public const OPERATION_TYPE_DIVIDEND_CARD = 'DividendCard'; //Доход ценной бумаги на внешний счет
    public const OPERATION_TYPE_TAX = 'Tax'; // Удержание налога
    public const OPERATION_TYPE_FEE = 'Fee'; // Удержание комиссии
    public const OPERATION_TYPE_HOLD = 'Hold'; // Резервирование средств
    public const OPERATION_TYPE_UN_HOLD = 'UnHold'; // Перевод средств из резерва
    public const OPERATION_TYPE_DEFAULT = 'Default'; //Дефолт
    public const OPERATION_TYPE_DEFAULT_PAYOUTS = 'DefaultPayouts'; //Выплаты после дефолта
}
