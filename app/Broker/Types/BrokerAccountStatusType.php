<?php

declare(strict_types=1);

namespace App\Broker\Types;

enum BrokerAccountStatusType: int
{
    case ACCOUNT_STATUS_UNSPECIFIED = 0;  // Статус не определён
    case ACCOUNT_STATUS_NEW = 1; // Новый, в процессе открытия
    case ACCOUNT_STATUS_OPEN = 2; // Открытый и активный счёт
    case ACCOUNT_STATUS_CLOSED = 3; // Закрытый счет
}
