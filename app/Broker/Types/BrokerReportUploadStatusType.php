<?php

declare(strict_types=1);

namespace App\Broker\Types;

enum BrokerReportUploadStatusType: string
{
    case FAIL = 'fail';
    case SUCCESS = 'success';
}
