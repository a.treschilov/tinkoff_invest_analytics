<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerPortfolioPositionModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioPositionCollection extends ArrayCollection
{
}
