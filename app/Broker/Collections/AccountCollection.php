<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerAccountModel;
use Doctrine\Common\Collections\ArrayCollection;

class AccountCollection extends ArrayCollection
{
    public function filterByType(int $type): AccountCollection
    {
        $filteredCollection = new AccountCollection();

        /** @var BrokerAccountModel $account */
        foreach ($this->getIterator() as $account) {
            if ($account->getType() === $type) {
                $filteredCollection->add($account);
            }
        }

        return $filteredCollection;
    }

    public function findById(string $id): ?BrokerAccountModel
    {
        /** @var BrokerAccountModel $account */
        foreach ($this->getIterator() as $account) {
            if ($account->getId() === $id) {
                return  $account;
            }
        }

        return null;
    }
}
