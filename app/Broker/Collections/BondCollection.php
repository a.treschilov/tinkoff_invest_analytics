<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerBondModel;
use Doctrine\Common\Collections\ArrayCollection;

class BondCollection extends ArrayCollection
{
    public function findByIsin(string $isin): ?BrokerBondModel
    {
        /** @var BrokerBondModel $bond */
        foreach ($this->getIterator() as $bond) {
            if ($bond->getIsin() === $isin) {
                return $bond;
            }
        }

        return null;
    }

    public function merge(BondCollection $bonds): BondCollection
    {
        /** @var BrokerBondModel $bond */
        foreach ($bonds as $bond) {
            if ($this->findByIsin($bond->getIsin()) === null) {
                $this->add($bond);
            }
        }
        return $this;
    }
}
