<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerShareModel;
use Doctrine\Common\Collections\ArrayCollection;

class ShareCollection extends ArrayCollection
{
    public function merge(ShareCollection $shares): ShareCollection
    {
        /** @var BrokerShareModel $share */
        foreach ($shares as $share) {
            if ($this->findByIsin($share->getInstrument()->getIsin()) === null) {
                $this->add($share);
            }
        }

        return $this;
    }

    public function findByIsin(string $isin): ?BrokerShareModel
    {
        /** @var BrokerShareModel $share */
        foreach ($this->getIterator() as $share) {
            if ($share->getInstrument()->getIsin() === $isin) {
                return $share;
            }
        }

        return null;
    }
}
