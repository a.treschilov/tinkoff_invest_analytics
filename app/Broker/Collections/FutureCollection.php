<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerFutureModel;
use Doctrine\Common\Collections\ArrayCollection;

class FutureCollection extends ArrayCollection
{
    private function findByTicker(string $ticker): ?BrokerFutureModel
    {
        /** @var BrokerFutureModel $future */
        foreach ($this->getIterator() as $future) {
            if ($future->getTicker() === $ticker) {
                return $future;
            }
        }

        return null;
    }

    public function merge(FutureCollection $futures): FutureCollection
    {
        /** @var BrokerFutureModel $future */
        foreach ($futures as $future) {
            if ($this->findByTicker($future->getTicker()) === null) {
                $this->add($future);
            }
        }
        return $this;
    }
}
