<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerOperationModel;
use Doctrine\Common\Collections\ArrayCollection;

class OperationCollection extends ArrayCollection
{
    public function merge(OperationCollection $collection)
    {
        /** @var BrokerOperationModel $operation */
        foreach ($collection->getIterator() as $operation) {
            if ($this->findById($operation->getId()) === null) {
                $this->add($operation);
            }
        }

        return $this;
    }

    public function findById(string $id): ?BrokerOperationModel
    {
        /** @var BrokerOperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getId() === $id) {
                return $operation;
            }
        }

        return null;
    }
}
