<?php

declare(strict_types=1);

namespace App\Broker\Collections;

use App\Broker\Models\BrokerInstrumentModel;
use Doctrine\Common\Collections\ArrayCollection;

class InstrumentCollection extends ArrayCollection
{
    public function mergeSame(): ?BrokerInstrumentModel
    {
        if ($this->count() === 0) {
            return null;
        }

        $result = $this->first();
        /** @var BrokerInstrumentModel $instrument */
        foreach ($this->getIterator() as $instrument) {
            if ($result->getMinPriceIncrement() === null) {
                $result->setMinPriceIncrement($instrument->getMinPriceIncrement());
            }

            if ($result->getLogo() === null) {
                $result->setLogo($instrument->getLogo());
            }

            if ($result->getRegNumber() === null) {
                $result->setRegNumber($instrument->getRegNumber());
            }

            if ($result->getFirstCandleDate() === null) {
                $result->setFirstCandleDate($instrument->getFirstCandleDate());
            }

            if ($result->getCurrencyModel() === null) {
                $result->setCurrencyModel($instrument->getCurrencyModel());
            }

            if ($result->getBondModel() === null) {
                $result->setBondModel($instrument->getBondModel());
            }
        }

        return $result;
    }
}
