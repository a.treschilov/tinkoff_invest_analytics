<?php

declare(strict_types=1);

namespace App\News\Models;

use App\Common\Models\BaseModel;

class NewsModel extends BaseModel
{
    private ?int $newsId = null;
    private string $title;
    private string $description;
    private \DateTime $date;
    private ?string $imageUrl = null;
    private string $link;
    private string $source;
    private string $externalId;
    private ?string $symbols;
    private bool $isActive = true;

    /**
     * @return int|null
     */
    public function getNewsId(): ?int
    {
        return $this->newsId;
    }

    /**
     * @param int|null $newsId
     */
    public function setNewsId(?int $newsId): void
    {
        $this->newsId = $newsId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     */
    public function setImageUrl(?string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|null
     */
    public function getSymbols(): ?string
    {
        return $this->symbols;
    }

    /**
     * @param string|null $symbols
     */
    public function setSymbols(?string $symbols): void
    {
        $this->symbols = $symbols;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}
