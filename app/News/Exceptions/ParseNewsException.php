<?php

declare(strict_types=1);

namespace App\News\Exceptions;

use App\Common\BaseException;

class ParseNewsException extends BaseException
{
    protected static $CODE = 4000;
}
