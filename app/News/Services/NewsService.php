<?php

declare(strict_types=1);

namespace App\News\Services;

use App\News\Collections\NewsCollection;
use App\News\Entities\NewsEntity;
use App\News\Models\NewsModel;
use App\News\Services\ThirdParty\NewsProviderInterface;
use App\News\Storages\NewsStorage;

class NewsService
{
    public function __construct(
        private readonly NewsStorage $newsStorage,
        private readonly NewsProviderInterface $newsProvider
    ) {
    }

    /**
     * @return array{imported: int, skipped: int}
     * @throws \Exception
     */
    public function parseNews(): array
    {
        $entityArray = [];

        $news = $this->newsProvider->getNews();
        /** @var NewsModel $newsModel */
        foreach ($news->getIterator() as $newsModel) {
            if (
                $this->newsStorage->getEntityByExternalId(
                    $newsModel->getExternalId(),
                    $newsModel->getSource()
                ) === null
            ) {
                $entityArray[] = $this->convertNewsModelToEntity($newsModel);
            }
        }

        $this->newsStorage->addEntityArray($entityArray);

        return [
            'imported' => count($entityArray),
            'skipped' => $news->count() - count($entityArray)
        ];
    }

    public function getNews(int $page, int $limit): NewsCollection
    {
        $from = ($page - 1) * $limit;
        $newsCollection = new NewsCollection();
        $entities = $this->newsStorage->findActive($limit, $from);
        foreach ($entities as $news) {
            $newsCollection->add(
                $this->convertNewsEntityToModel($news)
            );
        }

        return $newsCollection;
    }

    public function getNewsByDate(\DateTime $from, \DateTime $to): NewsCollection
    {
        $newsCollection = new NewsCollection();
        $entities = $this->newsStorage->findActiveByDate($from, $to);
        foreach ($entities as $news) {
            $newsCollection->add(
                $this->convertNewsEntityToModel($news)
            );
        }

        return $newsCollection;
    }

    public function getNewsCount(): int
    {
        return $this->newsStorage->findActiveCount();
    }

    private function convertNewsModelToEntity(NewsModel $news): NewsEntity
    {
        if (strlen($news->getSymbols()) > 500) {
            $pos = strpos($news->getSymbols(), ',', 490);
            $news->setSymbols(substr($news->getSymbols(), 0, $pos));
        }

        $entity = new NewsEntity();
        if ($news->getNewsId() !== null) {
            $entity->setId($news->getNewsId());
        }
        $entity->setTitle(substr($news->getTitle(), 0, 511));
        $entity->setDescription(substr($news->getDescription(), 0, 2047));
        $entity->setDate($news->getDate());
        $entity->setImageUrl($news->getImageUrl());
        $entity->setLink($news->getLink());
        $entity->setSource($news->getSource());
        $entity->setExternalId($news->getExternalId());
        $entity->setSymbols($news->getSymbols());
        $entity->setIsActive($news->isActive() ? 1 : 0);

        return $entity;
    }

    private function convertNewsEntityToModel(NewsEntity $news): NewsModel
    {
        return new NewsModel([
            'newsId' => $news->getId(),
            'title' => $news->getTitle(),
            'description' => $news->getDescription(),
            'date' => $news->getDate(),
            'imageUrl' => $news->getImageUrl(),
            'link' => $news->getLink(),
            'source' => $news->getSource(),
            'externalId' => $news->getExternalId(),
            'symbols' => $news->getSymbols(),
            'isActive' => (bool)$news->getIsActive()
        ]);
    }
}
