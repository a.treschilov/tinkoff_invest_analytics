<?php

declare(strict_types=1);

namespace App\News\Services\ThirdParty;

use App\News\Collections\NewsCollection;

interface NewsProviderInterface
{
    public function getNews(): NewsCollection;
}
