<?php

declare(strict_types=1);

namespace App\News\Services\ThirdParty;

use App\Common\HttpClient;
use App\Common\HttpResponseCode;
use App\News\Collections\NewsCollection;
use App\News\Exceptions\ParseNewsException;
use App\News\Models\NewsModel;

class MarketAuxService implements NewsProviderInterface
{
    private const SOURCE = 'marketaux';
    private const ARTICLE_LIMIT = 3;

    public function __construct(
        private HttpClient $httpClient,
        private $apiKey
    ) {
    }

    public function getNews(): NewsCollection
    {
        $maxPages = 5;
        $response = $this->getServiceResponse(1);

        $newsCollection = new NewsCollection();
        foreach ($response['data'] as $news) {
            $newsCollection->add(
                $this->convertNewsItemToModel($news)
            );
        }

        $page = 2;
        while ($response['meta']['returned'] * $page < $response['meta']['found'] && $page <= $maxPages) {
            $news = $this->getPageArticles($page);

            foreach ($news->getIterator() as $newsItem) {
                $newsCollection->add($newsItem);
            }
            $page++;
        }

        return $newsCollection;
    }

    private function getPageArticles(int $page): NewsCollection
    {
        $data = $this->getServiceResponse($page);

        $newsCollection = new NewsCollection();
        foreach ($data['data'] as $news) {
            $newsModel = $this->convertNewsItemToModel($news);
            $newsCollection->add(
                $newsModel
            );
        }

        return $newsCollection;
    }

    /**
     * @param int $page
     * @return array{
     *     meta: array{
     *      found: int, returned: int, limit: int, page: int
     *     },
     *     data: array
     * }
     * @throws ParseNewsException
     */
    private function getServiceResponse(int $page): array
    {
        $url = 'news/all';
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            $url,
            [
                'api_token' => $this->apiKey,
                'language' => 'ru',
                'countries' => 'global,ru,us,cn,de,tw,kr,jp,hk,in,my,tr',
                'must_have_entities' => true,
                'limit' => self::ARTICLE_LIMIT,
                'page' => $page
            ]
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new ParseNewsException('Error while get news from newsdata.io');
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    private function convertNewsItemToModel($news): NewsModel
    {
        $symbols = [];
        foreach ($news['entities'] as $marketEntity) {
            $symbols[] = $marketEntity['symbol'];
        }

        return new NewsModel([
            'title' => $news['title'],
            'description' => $news['description'],
            'date' => new \DateTime($news['published_at']),
            'imageUrl' => $news['image_url'] === '' ? null : $news['image_url'],
            'link' => $news['url'],
            'source' => self::SOURCE,
            'externalId' => $news['uuid'],
            'symbols' => count($symbols) ? implode(',', $symbols) : null,
            'isActive' => true
        ]);
    }
}
