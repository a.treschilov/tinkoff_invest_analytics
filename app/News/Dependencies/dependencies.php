<?php

declare(strict_types=1);

use App\Common\HttpClient;
use App\News\Services\NewsService;
use App\News\Services\ThirdParty\MarketAuxService;
use App\News\Services\ThirdParty\NewsProviderInterface;
use App\News\Storages\NewsStorage;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(NewsStorage::class, function (ContainerInterface $c) {
    return new NewsStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(NewsService::class, function (ContainerInterface $c) {
    return new NewsService(
        $c->get(NewsStorage::class),
        $c->get(MarketAuxService::class)
    );
});

$container->set('MARKETAUX_API' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://api.marketaux.com/v1/'
    ]);
    return new HttpClient($client);
});

$container->set(MarketAuxService::class, function (ContainerInterface $c) {
    return new MarketAuxService(
        $c->get('MARKETAUX_API' . HttpClient::class),
        getenv('MARKETAUX_API_KEY')
    );
});
