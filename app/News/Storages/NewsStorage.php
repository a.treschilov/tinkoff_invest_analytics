<?php

declare(strict_types=1);

namespace App\News\Storages;

use App\News\Entities\NewsEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManagerInterface;

class NewsStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param NewsEntity[] $newsList
     * @return void
     */
    public function addEntityArray(array $newsList): void
    {
        foreach ($newsList as $news) {
            $this->entityManager->persist($news);
        }

        $this->entityManager->flush();
    }

    public function getEntityByExternalId(string $externalId, string $source): ?NewsEntity
    {
        return $this->entityManager->getRepository(NewsEntity::class)
            ->findOneBy(['externalId' => $externalId, 'source' => $source]);
    }

    /**
     * @return NewsEntity[]
     */
    public function findActive(int $limit, int $offset): array
    {
        return $this->entityManager->getRepository(NewsEntity::class)
            ->findBy(['isActive' => 1], ['date' => 'DESC'], $limit, $offset);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return NewsEntity[]
     */
    public function findActiveByDate(\DateTime $from, \DateTime $to): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isActive', 1))
            ->andWhere(Criteria::expr()->gte('date', $from))
            ->andWhere(Criteria::expr()->lte('date', $to))
            ->orderBy(['date' => Order::Descending]);
        return $this->entityManager->getRepository(NewsEntity::class)
            ->matching($criteria)->toArray();
    }

    public function findActiveCount(): int
    {
        return $this->entityManager->getRepository(NewsEntity::class)
            ->count([
                'isActive' => 1
            ]);
    }
}
