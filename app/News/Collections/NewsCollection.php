<?php

declare(strict_types=1);

namespace App\News\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class NewsCollection extends ArrayCollection
{
}
