<?php

declare(strict_types=1);

namespace App\Deposit\Models;

use App\Common\Models\BaseModel;
use App\Item\Collections\OperationCollection;

class DepositPortfolioItemModel extends BaseModel
{
    private float $amount;
    private string $currency;
    private float $income;
    private float $inputAmount;
    private DepositModel $deposit;
    private OperationCollection $operations;

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return DepositModel
     */
    public function getDeposit(): DepositModel
    {
        return $this->deposit;
    }

    /**
     * @param DepositModel $deposit
     */
    public function setDeposit(DepositModel $deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return OperationCollection
     */
    public function getOperations(): OperationCollection
    {
        return $this->operations;
    }

    /**
     * @param OperationCollection $operations
     */
    public function setOperations(OperationCollection $operations): void
    {
        $this->operations = $operations;
    }

    /**
     * @return float
     */
    public function getIncome(): float
    {
        return $this->income;
    }

    /**
     * @param float $income
     */
    public function setIncome(float $income): void
    {
        $this->income = $income;
    }

    /**
     * @return float
     */
    public function getInputAmount(): float
    {
        return $this->inputAmount;
    }

    /**
     * @param float $inputAmount
     */
    public function setInputAmount(float $inputAmount): void
    {
        $this->inputAmount = $inputAmount;
    }
}
