<?php

declare(strict_types=1);

namespace App\Deposit\Models;

use App\Common\Models\BaseModel;

class DepositModel extends BaseModel
{
    private int $depositId;
    private int $itemId;
    private string $name;
    private int $userId;
    private int $isActive;
    private \DateTime $dealDate;
    private ?\DateTime $closedDate = null;
    private float $interestPercent;
    private int $duration;
    private string $durationPeriod;
    private int $payoutFrequency;
    private string $payoutFrequencyPeriod;
    private float $amount;
    private string $currency;

    /**
     * @return int
     */
    public function getDepositId(): int
    {
        return $this->depositId;
    }

    /**
     * @param int $depositId
     */
    public function setDepositId(int $depositId): void
    {
        $this->depositId = $depositId;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getDealDate(): \DateTime
    {
        return $this->dealDate;
    }

    /**
     * @param \DateTime $dealDate
     */
    public function setDealDate(\DateTime $dealDate): void
    {
        $this->dealDate = $dealDate;
    }

    /**
     * @return float
     */
    public function getInterestPercent(): float
    {
        return $this->interestPercent;
    }

    /**
     * @param float $interestPercent
     */
    public function setInterestPercent(float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDurationPeriod(): string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string $durationPeriod
     */
    public function setDurationPeriod(string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedDate(): ?\DateTime
    {
        return $this->closedDate;
    }

    /**
     * @param \DateTime|null $closedDate
     */
    public function setClosedDate(?\DateTime $closedDate): void
    {
        $this->closedDate = $closedDate;
    }
}
