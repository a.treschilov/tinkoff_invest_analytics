<?php

declare(strict_types=1);

namespace App\Deposit\Entities;

use App\Item\Entities\ItemEntity;
use App\Item\Entities\ItemOperationEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.deposit')]
#[ORM\Entity]
class DepositEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'deposit_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemEntity::class, inversedBy: 'deposit', cascade: ['persist'])]
    protected ItemEntity $item;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'deal_date', type: 'datetime')]
    protected \DateTime $dealDate;

    #[ORM\Column(name: 'closed_date', type: 'datetime')]
    protected ?\DateTime $closedDate = null;

    #[ORM\Column(name: 'duration', type: 'integer')]
    protected int $duration;

    #[ORM\Column(name: 'duration_period', type: 'string')]
    protected string $durationPeriod;

    #[ORM\Column(name: 'interest_percent', type: 'float')]
    protected float $interestPercent;

    #[ORM\Column(name: 'payout_frequency', type: 'integer')]
    protected int $payoutFrequency;

    #[ORM\Column(name: 'payout_frequency_period', type: 'string')]
    protected string $payoutFrequencyPeriod;

    #[ORM\Column(name: 'initial_operation_id', type: 'integer')]
    protected ?int $operationId = null;

    #[ORM\JoinColumn(name: 'initial_operation_id', referencedColumnName: 'item_operation_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemOperationEntity::class)]
    protected ?ItemOperationEntity $operation = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return \DateTime
     */
    public function getDealDate(): \DateTime
    {
        return $this->dealDate;
    }

    /**
     * @param \DateTime $dealDate
     */
    public function setDealDate(\DateTime $dealDate): void
    {
        $this->dealDate = $dealDate;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDurationPeriod(): string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string $durationPeriod
     */
    public function setDurationPeriod(string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return float
     */
    public function getInterestPercent(): float
    {
        return $this->interestPercent;
    }

    /**
     * @param float $interestPercent
     */
    public function setInterestPercent(float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return int|null
     */
    public function getOperationId(): ?int
    {
        return $this->operationId;
    }

    /**
     * @param int|null $operationId
     */
    public function setOperationId(?int $operationId): void
    {
        $this->operationId = $operationId;
    }

    /**
     * @return ItemOperationEntity|null
     */
    public function getOperation(): ?ItemOperationEntity
    {
        return $this->operation;
    }

    /**
     * @param ItemOperationEntity|null $operation
     */
    public function setOperation(?ItemOperationEntity $operation): void
    {
        $this->operation = $operation;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedDate(): ?\DateTime
    {
        return $this->closedDate;
    }

    /**
     * @param \DateTime|null $closedDate
     */
    public function setClosedDate(?\DateTime $closedDate): void
    {
        $this->closedDate = $closedDate;
    }
}
