<?php

declare(strict_types=1);

namespace App\Deposit\Collections;

use App\Deposit\Models\DepositModel;
use Doctrine\Common\Collections\ArrayCollection;

class DepositCollection extends ArrayCollection
{
    public function getItemIds(): array
    {
        $ids = [];
        /** @var DepositModel $deposit */
        foreach ($this->getIterator() as $deposit) {
            $ids[] = $deposit->getItemId();
        }

        return $ids;
    }
}
