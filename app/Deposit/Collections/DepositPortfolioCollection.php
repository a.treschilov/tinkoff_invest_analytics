<?php

declare(strict_types=1);

namespace App\Deposit\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class DepositPortfolioCollection extends ArrayCollection
{
}
