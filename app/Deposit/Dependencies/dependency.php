<?php

declare(strict_types=1);

use App\Deposit\Services\DepositService;
use App\Deposit\Storages\DepositStorage;
use App\Intl\Services\CurrencyService;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Services\ExchangeCurrencyService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(DepositStorage::class, function (ContainerInterface $c) {
    return new DepositStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(DepositService::class, function (ContainerInterface $c) {
    return new DepositService(
        $c->get('Now' .  DateTime::class),
        $c->get(DepositStorage::class),
        $c->get(OperationService::class),
        $c->get(CurrencyService::class),
        $c->get(ExchangeCurrencyService::class)
    );
});
