<?php

declare(strict_types=1);

namespace App\Deposit\Storages;

use App\Deposit\Entities\DepositEntity;
use Doctrine\ORM\EntityManagerInterface;

class DepositStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(DepositEntity $entity): int
    {
        $this->entityManager->persist($entity);
        $this->entityManager->persist($entity->getOperation());
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param int $userId
     * @return DepositEntity[]
     */
    public function findByUser(int $userId): array
    {
        $result = [];
        $deposits = $this->entityManager->getRepository(DepositEntity::class)
            ->findBy(['userId' => $userId]);
        foreach ($deposits as $deposit) {
            if ($deposit->getItem()->getIsDeleted() === 0) {
                $result[] = $deposit;
            }
        }

        return $result;
    }

    public function findOneByDepositId(int $depositId): DepositEntity|null
    {
        return $this->entityManager->getRepository(DepositEntity::class)
            ->findOneBy(['id' => $depositId]);
    }
}
