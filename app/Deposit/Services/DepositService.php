<?php

declare(strict_types=1);

namespace App\Deposit\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Deposit\Collections\DepositCollection;
use App\Deposit\Collections\DepositPortfolioCollection;
use App\Deposit\Entities\DepositEntity;
use App\Deposit\Models\DepositModel;
use App\Deposit\Models\DepositPortfolioItemModel;
use App\Deposit\Storages\DepositStorage;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Types\OperationType;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use App\User\Exceptions\UserAccessDeniedException;
use DateTimeZone;

class DepositService
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly DepositStorage $depositStorage,
        private readonly OperationService $operationService,
        private readonly CurrencyService $currencyService,
        private readonly ExchangeCurrencyService $exchangeCurrencyService
    ) {
    }

    public function getUserDeposits(int $userId): DepositCollection
    {
        $deposits = $this->depositStorage->findByUser($userId);

        $depositCollection = new DepositCollection();
        foreach ($deposits as $deposit) {
            $depositCollection->add($this->convertDepositEntityToModel($deposit));
        }

        return $depositCollection;
    }

    public function calculateDepositPayout(int $userId, ItemModel $item, \DateTime $dateTo): OperationCollection
    {
        $collection = new OperationCollection();
        $deposit = $item->getDeposit();

        $filter = new OperationFiltersModel([
            'itemId' => [$deposit->getItemId()],
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'dateTo' => $dateTo
        ]);
        $depositOperations = $this->operationService->getFilteredUserOperation($userId, $filter);
        if (!$item->isOutdated()) {
            $operationCollection = $this->buildDepositPayoutSchedule($item, $depositOperations);

            /** @var OperationModel $lastDividendOperation */
            $lastDividendOperation = $depositOperations
                ->filterByType([
                    OperationType::DIVIDEND_CARD,
                    OperationType::DIVIDEND
                ])
                ->sortByDateAsc()->last();
            $startFilteredDate = $lastDividendOperation
                ? clone $lastDividendOperation->getDate()
                : clone $deposit->getDealDate();
            $startFilteredDate->setTime(23, 59, 59);
            $operationCollection =
                $operationCollection->filterByDateInterval($startFilteredDate, $dateTo);

            foreach ($operationCollection->getIterator() as $operation) {
                $collection->add($operation);
            }
        }

        return $collection;
    }

    public function getUserDeposit(int $userId, int $depositId): DepositModel
    {
        $deposit = $this->depositStorage->findOneByDepositId($depositId);

        if (
            $deposit === null
            || $deposit->getItem()->getIsDeleted() === 1
            || $deposit->getUserId() !== $userId
        ) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }

        return $this->convertDepositEntityToModel($deposit);
    }

    public function getUserDepositPortfolio(int $userId): DepositPortfolioCollection
    {
        $collection = new DepositPortfolioCollection();

        $deposits = $this->getUserDeposits($userId);
        /** @var DepositModel $deposit */
        foreach ($deposits->getIterator() as $deposit) {
            $collection->add(
                $this->getUserDepositInfo($userId, $deposit->getDepositId())
            );
        }

        return $collection;
    }

    public function getUserDepositInfo(
        int $userId,
        int $depositId,
    ): DepositPortfolioItemModel {
        $dateTo = new \DateTime('2099-12-31');
        $deposit = $this->getUserDeposit($userId, $depositId);

        $currency = $deposit->getCurrency();
        $amount = $this->getUserDepositPrice($userId, $deposit->getItemId(), $dateTo, $currency);

        $filter = new OperationFiltersModel([
            'itemId' => [$deposit->getItemId()],
            'dateTo' => $dateTo
        ]);

        $operations = $this->operationService->getFilteredUserOperation($userId, $filter);

        $dividendOperations = $operations->filterByType([OperationType::DIVIDEND_CARD]);
        $income = $this->operationService->calculateOperationSummary($dividendOperations, $currency);

        $inputOperations = $operations->filterByType([
            OperationType::BUY_CARD,
            OperationType::SELL_CARD
        ]);
        $input = $this->operationService->calculateOperationSummary($inputOperations, $currency);
        $input->setAmount($input->getAmount() * -1);

        return new DepositPortfolioItemModel([
            'deposit' => $deposit,
            'operations' => $operations,
            'amount' => $amount->getAmount(),
            'income' => $income->getAmount(),
            'inputAmount' => $input->getAmount(),
            'currency' => $amount->getCurrency()
        ]);
    }

    public function getUserDepositPrice(
        int $userId,
        int $itemId,
        \DateTime $date,
        string $currency
    ): PriceModel {
        $filter = new OperationFiltersModel([
            'itemId' => [$itemId],
            'dateTo' => $date,
            'operationType' => [
                BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND
            ]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $filter);
        $amount = 0;
        /** @var OperationModel $operation */
        foreach ($operations as $operation) {
            $operationAmount =
                in_array($operation->getOperationType(), [
                    BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                    BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD
                ])
                    ? -1 * $operation->getAmount()
                    : $operation->getAmount();
            $amount += $this->exchangeCurrencyService
                    ->getExchangeRate($operation->getCurrencyIso(), $currency, $operation->getDate())
                * $operationAmount;
        }

        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    public function addUserDeposit(int $userId, DepositModel $deposit): int
    {
        $deposit->getDealDate()->setTimezone(new DateTimeZone("UTC"));

        $operation = new OperationModel([
            'userId' => $userId,
            'externalId' => $this->operationService->generateUserOperationExternalId(),
            'operationType' => BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
            'date' => $deposit->getDealDate(),
            'amount' => -1 * $deposit->getAmount(),
            'currencyIso' => $deposit->getCurrency(),
            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
            'isAnalyticProceed' => 0,
            'quantity' => 1
        ]);
        $operationEntity = $this->operationService->convertOperationModelToEntity($operation);

        $itemEntity = new ItemEntity();
        $itemEntity->setName($deposit->getName());
        $itemEntity->setType('deposit');
        $itemEntity->setSource('user');
        $itemEntity->setIsOutdated(($deposit->getIsActive() + 1) % 2);

        $entity = new DepositEntity();
        $entity->setUserId($userId);
        $entity->setDealDate($deposit->getDealDate());
        $entity->setClosedDate($deposit->getClosedDate());
        $entity->setInterestPercent($deposit->getInterestPercent());
        $entity->setDuration($deposit->getDuration());
        $entity->setDurationPeriod($deposit->getDurationPeriod());
        $entity->setPayoutFrequency($deposit->getPayoutFrequency());
        $entity->setPayoutFrequencyPeriod($deposit->getPayoutFrequencyPeriod());
        $entity->setItem($itemEntity);
        $entity->setOperation($operationEntity);

        $itemEntity->setDeposit($entity);
        $operationEntity->setItem($itemEntity);

        $depositEntity = $this->depositStorage->addEntity($entity);

        $this->operationService->publishOperationEvent($entity->getOperation()->getId());

        return $depositEntity;
    }

    public function updateUserDeposit(int $userId, DepositModel $deposit): void
    {
        $deposit->getDealDate()->setTimezone(new DateTimeZone("UTC"));
        $depositEntity = $this->depositStorage->findOneByDepositId($deposit->getDepositId());

        if ($depositEntity->getItem()->getIsDeleted() === 1 || $depositEntity->getUserId() !== $userId) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }
        $depositEntity->getItem()->setIsOutdated(($deposit->getIsActive() + 1) % 2);

        $depositEntity->setDealDate($deposit->getDealDate());
        $depositEntity->setClosedDate($deposit->getClosedDate());
        $depositEntity->setInterestPercent($deposit->getInterestPercent());
        $depositEntity->setDuration($deposit->getDuration());
        $depositEntity->setDurationPeriod($deposit->getDurationPeriod());
        $depositEntity->setPayoutFrequency($deposit->getPayoutFrequency());
        $depositEntity->setPayoutFrequencyPeriod($deposit->getPayoutFrequencyPeriod());
        $depositEntity->getItem()->setName($deposit->getName());

        if ($depositEntity->getOperation()->getAmount() !== -1 * $deposit->getAmount()) {
            $depositEntity->getOperation()->setAmount(-1 * $deposit->getAmount());
            $depositEntity->getOperation()->setIsAnalyticProceed(0);
        }

        if ($depositEntity->getOperation()->getCurrency()->getIso() !== $deposit->getCurrency()) {
            $currency = $this->currencyService->getCurrencyByIso($deposit->getCurrency());
            $depositEntity->getOperation()->setCurrency($currency);
            $depositEntity->getOperation()->setCurrencyId($currency->getId());
            $depositEntity->getOperation()->setIsAnalyticProceed(0);
        }

        if ($depositEntity->getOperation()->getDate()->getTimestamp() - $deposit->getDealDate()->getTimestamp()) {
            $depositEntity->getOperation()->setDate($deposit->getDealDate());
            $depositEntity->getOperation()->setIsAnalyticProceed(0);
        }

        $this->depositStorage->addEntity($depositEntity);

        $this->operationService->publishOperationEvent($depositEntity->getOperation()->getId());
    }

    public function closeUserDeposit(int $userId, int $depositId): void
    {
        $deposit = $this->depositStorage->findOneByDepositId($depositId);
        if ($deposit?->getItem()->getIsDeleted() === 1 || $deposit?->getUserId() !== $userId) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }

        $deposit->setClosedDate($this->currentDateTime);
        $deposit->getItem()->setIsOutdated(1);

        $depositInfo = $this->getUserDepositInfo($userId, $depositId);

        $operation = new OperationModel([
            'userId' => $userId,
            'externalId' => $this->operationService->generateUserOperationExternalId(),
            'itemId' => $depositInfo->getDeposit()->getItemId(),
            'operationType' => OperationType::SELL_CARD->value,
            'date' => $this->currentDateTime,
            'amount' => $depositInfo->getAmount(),
            'currencyIso' => $depositInfo->getCurrency(),
            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
            'quantity' => 1
        ]);

        $this->operationService->addUserOperation($userId, $operation);
        $this->depositStorage->addEntity($deposit);
    }

    private function convertDepositEntityToModel(DepositEntity $deposit): DepositModel
    {
        return new DepositModel([
            'depositId' => $deposit->getId(),
            'itemId' => $deposit->getItemId(),
            'name' => $deposit->getItem()->getName(),
            'userId' => $deposit->getUserId(),
            'isActive' => ($deposit->getItem()->getIsOutdated() + 1) % 2,
            'dealDate' => $deposit->getDealDate(),
            'closedDate' => $deposit->getClosedDate(),
            'interestPercent' => $deposit->getInterestPercent(),
            'duration' => $deposit->getDuration(),
            'durationPeriod' => $deposit->getDurationPeriod(),
            'payoutFrequency' => $deposit->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $deposit->getPayoutFrequencyPeriod(),
            'amount' => -1 * $deposit->getOperation()->getAmount(),
            'currency' => $deposit->getOperation()->getCurrency()->getIso()
        ]);
    }

    private function buildDepositPayoutSchedule(
        ItemModel $itemModel,
        OperationCollection $operations
    ): OperationCollection {
        $operationCollection = new OperationCollection();

        $deposit = $itemModel->getDeposit();
        if ($deposit->getPayoutFrequency() <= 0) {
            return $operationCollection;
        }

        $depositCurrency = $deposit->getCurrency();
        $amount = 0;
        $startPeriod = $deposit->getDealDate();
        $endDate = clone $startPeriod;
        $endDate->add($this->buildDateInterval(
            $deposit->getDuration(),
            $deposit->getDurationPeriod()
        ));
        $payoutInterval = $this->buildDateInterval(
            $deposit->getPayoutFrequency(),
            $deposit->getPayoutFrequencyPeriod()
        );
        $endPeriod = clone $startPeriod;

        do {
            $endPeriod = min($endDate, $endPeriod->add($payoutInterval));
            $diff = $endPeriod->diff($startPeriod);

            $percent = $diff->days / (date('L') ? 366 : 365) * $deposit->getInterestPercent();
            $interest = -1 * $percent * $amount;
            $amount += $interest;

            $periodOperations = $operations->filterByDateInterval($startPeriod, $endPeriod);

            /** @var OperationModel $operation */
            foreach ($periodOperations->getIterator() as $operation) {
                if (
                    in_array(
                        $operation->getOperationType(),
                        [
                            BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
                            BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD
                        ]
                    )
                ) {
                    $diff = $endPeriod->diff((clone $operation->getDate())->setTime(0, 0, 0));
                    $percent = $diff->days / 365 * $deposit->getInterestPercent();

                    $rate = $this->exchangeCurrencyService
                        ->getExchangeRate($operation->getCurrencyIso(), $depositCurrency, $operation->getDate());
                    $interest += -1 * $operation->getAmount() * $percent * $rate;
                    $amount += -1 * $interest + $operation->getAmount();
                }
            }

            if ($interest > 0) {
                $operationModel = new OperationModel([
                    'userId' => $deposit->getUserId(),
                    'itemId' => $deposit->getItemId(),
                    'item' => $itemModel,
                    'operationType' => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                    'externalId' => md5(
                        $deposit->getItemId()
                        . (clone $startPeriod)->getTimestamp()
                        . (clone $endPeriod)->getTimeStamp()
                    ),
                    'createdDate' => $this->currentDateTime,
                    'date' => clone $endPeriod,
                    'amount' => round(round($interest * 100) / 100, 2),
                    'currencyIso' => $depositCurrency,
                    'status' => BrokerOperationStatusType::OPERATION_STATE_DONE
                ]);
                $operationCollection->add($operationModel);
            }

            $startPeriod = clone $endPeriod;
        } while ($endPeriod < $endDate);

        return $operationCollection;
    }

    private function buildDateInterval(int $duration, string $durationPeriod): \DateInterval
    {
        $intervalPeriod = match ($durationPeriod) {
            'day' => 'D',
            'month' => 'M',
            'year' => 'Y'
        };
        return  new \DateInterval('P' . $duration . $intervalPeriod);
    }
}
