<?php

declare(strict_types=1);

use App\Common\HttpClient;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\ThirdParty\ExchangeRatesApiService;
use App\Intl\Storages\CountryStorage;
use App\Intl\Storages\CurrencyExchangeStorage;
use App\Intl\Storages\CurrencyStorage;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(CountryStorage::class, function (ContainerInterface $c) {
    return new CountryStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CurrencyStorage::class, function (ContainerInterface $c) {
    return new CurrencyStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CurrencyExchangeStorage::class, function (ContainerInterface $c) {
    return new CurrencyExchangeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CountryService::class, function (ContainerInterface $c) {
    return new CountryService(
        $c->get(CountryStorage::class)
    );
});

$container->set(CurrencyService::class, function (ContainerInterface $c) {
    return new CurrencyService(
        $c->get(CurrencyStorage::class),
        $c->get(ExchangeRatesApiService::class)
    );
});

$container->set('EXCHANGE_RATES_API' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'http://api.exchangeratesapi.io/v1/'
    ]);
    return new HttpClient($client);
});

$container->set(ExchangeRatesApiService::class, function (ContainerInterface $c) {
    return new ExchangeRatesApiService(
        $c->get('EXCHANGE_RATES_API' . HttpClient::class),
        getenv('EXCHANGE_RATES_API_KEY')
    );
});
