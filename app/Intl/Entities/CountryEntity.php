<?php

declare(strict_types=1);

namespace App\Intl\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.country')]
#[ORM\Entity]
class CountryEntity
{
    /**
     * @var string
     */
    #[ORM\Id]
    #[ORM\Column(name: 'iso', type: 'string')]
    protected $iso;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string')]
    protected $name;

    /**
     * @var string
     */
    #[ORM\Column(name: 'continent', type: 'string')]
    protected $continent;

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContinent(): string
    {
        return $this->continent;
    }

    /**
     * @param string $continent
     */
    public function setContinent(string $continent): void
    {
        $this->continent = $continent;
    }
}
