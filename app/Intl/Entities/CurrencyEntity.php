<?php

declare(strict_types=1);

namespace App\Intl\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.currency')]
#[ORM\Entity]
class CurrencyEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'currency_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'iso', type: 'string')]
    protected $iso;

    /**
     * @var string
     */
    #[ORM\Column(name: 'symbol', type: 'string')]
    protected $symbol;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string')]
    protected $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol(string $symbol): void
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
