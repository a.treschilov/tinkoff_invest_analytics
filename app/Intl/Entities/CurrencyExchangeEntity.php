<?php

declare(strict_types=1);

namespace App\Intl\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.currency_exchange')]
#[ORM\Entity]
class CurrencyExchangeEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'currency_exchange_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'currency_from', type: 'string')]
    protected string $currencyFrom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'currency_to', type: 'string')]
    protected string $currencyTo;

    /**
     * @var float
     */
    #[ORM\Column(name: 'exchange_rate', type: 'float')]
    protected float $exchangeRate;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCurrencyFrom(): string
    {
        return $this->currencyFrom;
    }

    /**
     * @param string $currencyFrom
     */
    public function setCurrencyFrom(string $currencyFrom): void
    {
        $this->currencyFrom = $currencyFrom;
    }

    /**
     * @return string
     */
    public function getCurrencyTo(): string
    {
        return $this->currencyTo;
    }

    /**
     * @param string $currencyTo
     */
    public function setCurrencyTo(string $currencyTo): void
    {
        $this->currencyTo = $currencyTo;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchangeRate;
    }

    /**
     * @param float $exchangeRate
     */
    public function setExchangeRate(float $exchangeRate): void
    {
        $this->exchangeRate = $exchangeRate;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}
