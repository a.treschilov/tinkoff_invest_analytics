<?php

declare(strict_types=1);

namespace App\Intl\Storages;

use App\Intl\Entities\CountryEntity;
use Doctrine\ORM\EntityManagerInterface;

class CountryStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return CountryEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(CountryEntity::class)
            ->findBy([], ['name' => 'ASC']);
    }

    public function findOneByName(string $name): CountryEntity|null
    {
        return $this->entityManager->getRepository(CountryEntity::class)
            ->findOneBy(['name' => $name]);
    }

    public function findOneByIso(string $iso): CountryEntity|null
    {
        return $this->entityManager->getRepository(CountryEntity::class)
            ->findOneBy(['iso' => $iso]);
    }
}
