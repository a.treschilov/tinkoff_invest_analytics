<?php

declare(strict_types=1);

namespace App\Intl\Storages;

use App\Intl\Entities\CurrencyExchangeEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyExchangeStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param CurrencyExchangeEntity[] $arrayEntity
     * @return void
     */
    public function addArrayEntity(array $arrayEntity): void
    {
        foreach ($arrayEntity as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    /**
     * @param \DateTime $date
     * @return CurrencyExchangeEntity[]
     */
    public function findByDate(\DateTime $date): array
    {
        $foundDate = clone $date;
        return $this->entityManager->getRepository(CurrencyExchangeEntity::class)
            ->findBy(['date' => $foundDate->setTime(0, 0, 0)]);
    }

    public function findByCurrenciesAndDate(
        string $currencyFrom,
        string $currencyTo,
        \DateTime $date
    ): ?CurrencyExchangeEntity {
        $foundDate = clone $date;
        return $this->entityManager->getRepository(CurrencyExchangeEntity::class)
            ->findOneBy([
                'currencyFrom' => $currencyFrom,
                'currencyTo' => $currencyTo,
                'date' => $foundDate->setTime(0, 0, 0)
            ]);
    }

    public function findLastByCurrencyAndDate(
        string $currencyFrom,
        string $currencyTo,
        \DateTime $date
    ): ?CurrencyExchangeEntity {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('currencyFrom', $currencyFrom));
        $criteria->andWhere(Criteria::expr()->eq('currencyTo', $currencyTo));
        $criteria->andWhere(Criteria::expr()->lte('date', $date));
        $criteria->orderBy(['date' => Order::Descending]);
        $criteria->setMaxResults(1);

        return $this->entityManager->getRepository(CurrencyExchangeEntity::class)
            ->matching($criteria)->get(0);
    }
}
