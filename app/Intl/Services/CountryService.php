<?php

declare(strict_types=1);

namespace App\Intl\Services;

use App\Intl\Collections\CountryCollection;
use App\Intl\Entities\CountryEntity;
use App\Intl\Storages\CountryStorage;

class CountryService
{
    private CountryStorage $countryStorage;

    public function __construct(CountryStorage $countryStorage)
    {
        $this->countryStorage = $countryStorage;
    }

    public function getList(): CountryCollection
    {
        return new CountryCollection($this->countryStorage->findAll());
    }

    public function getCountryByName(string $countryName): CountryEntity|null
    {
        $countryName = match ($countryName) {
            'Russia' => 'Russian Federation',
            'South Korea' => 'Korea, Republic of',
            'Taiwan' => 'Taiwan, Province of China',
            'Macau' => 'China',
            default => $countryName
        };
        return $this->countryStorage->findOneByName($countryName);
    }

    public function getCountryByIso(string $iso): CountryEntity|null
    {
        return $this->countryStorage->findOneByIso($iso);
    }
}
