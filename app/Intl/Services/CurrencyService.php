<?php

declare(strict_types=1);

namespace App\Intl\Services;

use App\Intl\Collections\CurrencyCollection;
use App\Intl\Entities\CurrencyEntity;
use App\Intl\Models\CurrencyModel;
use App\Intl\Services\ThirdParty\ExchangeRateApiInterface;
use App\Intl\Storages\CurrencyStorage;

class CurrencyService
{
    public function __construct(
        private CurrencyStorage $currencyStorage,
        private ExchangeRateApiInterface $exchangeRateApi
    ) {
    }

    public function getList(): CurrencyCollection
    {
        return new CurrencyCollection($this->currencyStorage->findAll());
    }

    public function getCurrencyByIso(string $iso): ?CurrencyEntity
    {
        return $this->currencyStorage->findOneByIso($iso);
    }

    public function getCurrencyById(int $id): ?CurrencyEntity
    {
        return $this->currencyStorage->findOneById($id);
    }

    public function importCurrencyList(): array
    {
        $currencyList = $this->exchangeRateApi->getCurrencyList();

        $newCurrencyList = [];
        /** @var CurrencyModel $currency */
        foreach ($currencyList->getIterator() as $currency) {
            $dbCurrency = $this->currencyStorage->findOneByIso($currency->getIso());
            if ($dbCurrency === null) {
                $currencyEntity = new CurrencyEntity();
                $currencyEntity->setIso($currency->getIso());
                $currencyEntity->setName($currency->getName());
                $currencyEntity->setSymbol($currency->getSymbol());
                $newCurrencyList[] =  $currencyEntity;
            }
        }

        $this->currencyStorage->addArrayEntities($newCurrencyList);

        $result = [
            'imported' => count($newCurrencyList),
            'symbols' => []
        ];
        foreach ($newCurrencyList as $data) {
            $result['symbols'][] = $data->getIso();
        }

        return $result;
    }
}
