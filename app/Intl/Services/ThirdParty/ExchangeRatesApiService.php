<?php

//exchangeratesapi.io
declare(strict_types=1);

namespace App\Intl\Services\ThirdParty;

use App\Common\HttpClient;
use App\Common\HttpResponseCode;
use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Models\CurrencyModel;

class ExchangeRatesApiService implements ExchangeRateApiInterface
{
    public function __construct(
        private HttpClient $httpClient,
        private $apiKey
    ) {
    }

    /**
     * @param string $currency
     * @param \DateTime $date
     * @return array
     * @throws ExchangeUnsupportedCurrencyException
     */
    public function getExchangeRate(string $currency, \DateTime $date): array
    {
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            $date->format("Y-m-d"),
            ['access_key' => $this->apiKey]
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new ExchangeUnsupportedCurrencyException(
                'Error of importing Exchange currency rate for ' . $date->format("Y-m-d")
            );
        }

        $content = json_decode($response->getBody()->getContents(), true);

        $result = [];
        $baseRatio = $content['rates'][$currency];

        foreach ($content['rates'] as $currencyIso => $rate) {
            $result[$currencyIso] = $rate / $baseRatio;
        }

        return $result;
    }

    /**
     * @return CurrencyCollection
     * @throws ExchangeUnsupportedCurrencyException
     */
    public function getCurrencyList(): CurrencyCollection
    {
        $currencyCollection = new CurrencyCollection();
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            'symbols',
            ['access_key' => $this->apiKey]
        );

        if ($response->getStatusCode() !== HttpResponseCode::OK) {
            throw new ExchangeUnsupportedCurrencyException('Error of importing currency list');
        }

        $content = json_decode($response->getBody()->getContents(), true);

        foreach ($content['symbols'] as $iso => $name) {
            $currencyCollection->add(new CurrencyModel([
                'iso' => $iso,
                'symbol' => $iso,
                'name' => $name
            ]));
        }
        return $currencyCollection;
    }
}
