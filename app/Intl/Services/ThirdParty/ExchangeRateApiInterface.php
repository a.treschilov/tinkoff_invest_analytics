<?php

declare(strict_types=1);

namespace App\Intl\Services\ThirdParty;

use App\Intl\Collections\CurrencyCollection;

interface ExchangeRateApiInterface
{
    public function getExchangeRate(string $currency, \DateTime $date);
    public function getCurrencyList(): CurrencyCollection;
}
