<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function findByLogin(string $login): ?UserEntity
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findOneBy(['login' => $login]);
    }

    public function findByTelegramId(int $telegramId): ?UserEntity
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findOneBy(['telegramId' => $telegramId]);
    }


    /**
     * @return UserEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findAll();
    }

    /**
     * @return UserEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(UserEntity::class)
            ->findBy(['isActive' => 1]);
    }

    public function findById(int $id): ?UserEntity
    {
        return $this->entityManager->getRepository(UserEntity::class)->findOneBy(['id' => $id]);
    }

    public function findActiveById(int $id): ?UserEntity
    {
        return $this->entityManager->getRepository(UserEntity::class)->findOneBy(['id' => $id, 'isActive' => 1]);
    }

    /**
     * @param UserEntity $user
     * @return UserEntity
     */
    public function addEntity(UserEntity $user): UserEntity
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
