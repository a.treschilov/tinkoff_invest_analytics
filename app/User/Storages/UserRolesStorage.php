<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserRolesEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserRolesStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return UserRolesEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(UserRolesEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }
}
