<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserAuthEntity;
use App\User\Types\AuthType;
use Doctrine\ORM\EntityManagerInterface;

class UserAuthStorage
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function findActiveByAuthTypeAndToken(AuthType $authType, string $token): ?UserAuthEntity
    {
        return $this->entityManager->getRepository(UserAuthEntity::class)
            ->findOneBy(['authType' => $authType->value, 'accessToken' => $token, 'isActive' => 1]);
    }

    /**
     * @param string $authType
     * @param string $client_id
     * @return UserAuthEntity|null
     */
    public function findByAuthTypeAndClientId(string $authType, string $client_id): ?object
    {
        return $this->entityManager->getRepository(UserAuthEntity::class)
            ->findOneBy(['authType' => $authType, 'clientId' => $client_id]);
    }

    /**
     * @param int $userId
     * @return UserAuthEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(UserAuthEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param UserAuthEntity $user
     */
    public function addEntity(UserAuthEntity $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param UserAuthEntity[] $userAuthEntities
     * @return UserAuthEntity[]
     */
    public function addEntityArray(array $userAuthEntities): array
    {
        foreach ($userAuthEntities as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();

        return $userAuthEntities;
    }
}
