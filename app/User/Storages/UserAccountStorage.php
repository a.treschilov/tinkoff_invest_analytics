<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserAccountEntity;
use App\User\Types\AuthType;
use Doctrine\ORM\EntityManagerInterface;

readonly class UserAccountStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function addEntity(UserAccountEntity $userAccount): UserAccountEntity
    {
        $this->entityManager->persist($userAccount);
        $this->entityManager->flush();

        return $userAccount;
    }

    public function findByAuthTypeAndClientId(AuthType $authType, string $clientId): ?UserAccountEntity
    {
        return $this->entityManager->getRepository(UserAccountEntity::class)
            ->findOneBy(['authType' => $authType->value, 'clientId' => $clientId, 'isActive' => 1]);
    }

    /**
     * @param int $userId
     * @return UserAccountEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(UserAccountEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    public function findActiveByUserIdAndAuthType(int $userId, AuthType $authType): ?UserAccountEntity
    {
        return $this->entityManager->getRepository(UserAccountEntity::class)
            ->findOneBy(['userId' => $userId, 'authType' => $authType->value, 'isActive' => 1]);
    }
}
