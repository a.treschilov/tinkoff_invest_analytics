<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserBrokerAccountEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserBrokerAccountStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userCredentialId
     * @return UserBrokerAccountEntity[]
     */
    public function findByCredentialId(int $userCredentialId): array
    {
        return $this->entityManager->getRepository(UserBrokerAccountEntity::class)
            ->findBy(['userCredentialId' => $userCredentialId]);
    }

    /**
     * @param int $userCredentialId
     * @return UserBrokerAccountEntity[]
     */
    public function findActiveByCredentialId(int $userCredentialId): array
    {
        return $this->entityManager->getRepository(UserBrokerAccountEntity::class)
            ->findBy(['userCredentialId' => $userCredentialId, 'isActive' => 1, 'isDeleted' => 0]);
    }

    public function addEntity(UserBrokerAccountEntity $userBrokerAccountEntity): void
    {
        $this->entityManager->persist($userBrokerAccountEntity);
        $this->entityManager->flush();
    }

    public function findActiveById(int $userBrokerAccountId): ?UserBrokerAccountEntity
    {
        return $this->entityManager->getRepository(UserBrokerAccountEntity::class)
            ->findOneBy(['id' => $userBrokerAccountId, 'isDeleted' => 0, 'isActive' => 1]);
    }
}
