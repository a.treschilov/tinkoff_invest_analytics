<?php

namespace App\User\Storages;

use App\User\Entities\UserSettingsEntity;
use Doctrine\ORM\EntityManagerInterface;

class UserSettingsStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return UserSettingsEntity|null
     */
    public function findByUserId(int $userId): ?object
    {
        return $this->entityManager->getRepository(UserSettingsEntity::class)
            ->findOneBy(['userId' => $userId]);
    }

    public function addEntity(UserSettingsEntity $userSettings): UserSettingsEntity
    {
        $this->entityManager->persist($userSettings);
        $this->entityManager->flush();

        return $userSettings;
    }
}
