<?php

declare(strict_types=1);

namespace App\User\Storages;

use App\User\Entities\UserCodeEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

readonly class UserCodeStorage
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function findByCodeBeforeDate(string $code, \DateTime $expireDate): ?UserCodeEntity
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('code', $code));
        $criteria->andWhere(Criteria::expr()->gte('expireDate', $expireDate));

        return $this->entityManager->getRepository(UserCodeEntity::class)
            ->matching($criteria)->get(0);
    }

    public function addEntity(UserCodeEntity $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
