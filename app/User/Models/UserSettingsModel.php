<?php

namespace App\User\Models;

use App\Common\Models\BaseModel;
use App\Models\PriceModel;

class UserSettingsModel extends BaseModel
{
    private ?int $userSettingsId = null;
    private int $userId;
    private UserModel $user;
    private ?PriceModel $desiredPension = null;
    private ?int $retirementAge = null;
    private ?PriceModel $expenses = null;
    private ?\DateTime $birthday = null;

    /**
     * @return int|null
     */
    public function getUserSettingsId(): ?int
    {
        return $this->userSettingsId;
    }

    /**
     * @param int|null $userSettingsId
     */
    public function setUserSettingsId(?int $userSettingsId): void
    {
        $this->userSettingsId = $userSettingsId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return PriceModel|null
     */
    public function getDesiredPension(): ?PriceModel
    {
        return $this->desiredPension;
    }

    /**
     * @param PriceModel|null $desiredPension
     */
    public function setDesiredPension(?PriceModel $desiredPension): void
    {
        $this->desiredPension = $desiredPension;
    }

    /**
     * @return int|null
     */
    public function getRetirementAge(): ?int
    {
        return $this->retirementAge;
    }

    /**
     * @param int|null $retirementAge
     */
    public function setRetirementAge(?int $retirementAge): void
    {
        $this->retirementAge = $retirementAge;
    }

    /**
     * @return PriceModel|null
     */
    public function getExpenses(): ?PriceModel
    {
        return $this->expenses;
    }

    /**
     * @param PriceModel|null $expenses
     */
    public function setExpenses(?PriceModel $expenses): void
    {
        $this->expenses = $expenses;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime|null $birthday
     */
    public function setBirthday(?\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return UserModel
     */
    public function getUser(): UserModel
    {
        return $this->user;
    }

    /**
     * @param UserModel $user
     */
    public function setUser(UserModel $user): void
    {
        $this->user = $user;
    }
}
