<?php

declare(strict_types=1);

namespace App\User\Models;

use App\Common\Models\BaseModel;
use App\Broker\Models\BrokerModel;
use App\User\Collections\UserBrokerAccountCollection;

class UserCredentialModel extends BaseModel
{
    private ?int $userCredentialId;
    private int $userId;
    private string $brokerId;
    private BrokerModel $broker;
    private ?string $apiKey = null;
    private ?int $isActive = null;
    private UserBrokerAccountCollection $brokerAccounts;

    /**
     * @return int|null
     */
    public function getUserCredentialId(): ?int
    {
        return $this->userCredentialId;
    }

    /**
     * @param int|null $userCredentialId
     */
    public function setUserCredentialId(?int $userCredentialId): void
    {
        $this->userCredentialId = $userCredentialId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    /**
     * @param string $brokerId
     */
    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @param string|null $apiKey
     */
    public function setApiKey(?string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return int|null
     */
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    /**
     * @param int|null $isActive
     */
    public function setIsActive(?int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return UserBrokerAccountCollection
     */
    public function getBrokerAccounts(): UserBrokerAccountCollection
    {
        return $this->brokerAccounts;
    }

    /**
     * @param UserBrokerAccountCollection $brokerAccounts
     */
    public function setBrokerAccounts(UserBrokerAccountCollection $brokerAccounts): void
    {
        $this->brokerAccounts = $brokerAccounts;
    }

    /**
     * @return BrokerModel
     */
    public function getBroker(): BrokerModel
    {
        return $this->broker;
    }

    /**
     * @param BrokerModel $broker
     */
    public function setBroker(BrokerModel $broker): void
    {
        $this->broker = $broker;
    }
}
