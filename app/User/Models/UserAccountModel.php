<?php

declare(strict_types=1);

namespace App\User\Models;

use App\Common\Models\BaseModel;
use App\User\Types\AuthType;

class UserAccountModel extends BaseModel
{
    public int $userAccountId;
    public int $userId;
    public AuthType $authType;
    public string $clientId;
    public bool $isActive;

    public function getUserAccountId(): int
    {
        return $this->userAccountId;
    }

    public function setUserAccountId(int $userAccountId): void
    {
        $this->userAccountId = $userAccountId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getAuthType(): AuthType
    {
        return $this->authType;
    }

    public function setAuthType(AuthType $authType): void
    {
        $this->authType = $authType;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}
