<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Common\HttpClient;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Types\AuthType;

class GoogleAuthService implements AuthServiceInterface
{
    private HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getUserInfo(string $accessToken): ?UserAuthModel
    {
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            'people/me',
            [
                'personFields' => 'emailAddresses,names,birthdays'
            ],
            ['Authorization' => 'Bearer ' . $accessToken]
        );

        if (in_array($response->getStatusCode(), [401, 403])) {
            return null;
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);

        $birthday = isset($responseBody['birthdays']) ? $responseBody['birthdays'][0]['date'] : null;

        $birthday = $birthday === null || !isset($birthday['year'])
            ? null
            : new \DateTime($birthday['year'] . '-' . $birthday['month'] . '-' . $birthday['day']);
        $modelData = [
            'access_token' => $accessToken,
            'authType' => AuthType::GOOGLE,
            'login' => $responseBody['emailAddresses'][0]['value'],
            'name' => $responseBody['names'][0]['displayName'],
            'email' => $responseBody['emailAddresses'][0]['value'],
            'clientId' => $responseBody['resourceName'],
            'birthday' => $birthday
        ];
        return new UserAuthModel($modelData);
    }
}
