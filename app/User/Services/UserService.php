<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Common\Mailer\Mailer;
use App\Models\PriceModel;
use App\Services\AuthService;
use App\User\Collections\UserCollection;
use App\User\Collections\UserModelCollection;
use App\User\Entities\UserAccountEntity;
use App\User\Entities\UserAuthEntity;
use App\User\Entities\UserEntity;
use App\User\Entities\UserSettingsEntity;
use App\User\Exceptions\AuthTypeException;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Exceptions\UserAccountException;
use App\User\Exceptions\UserException;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Models\UserModel;
use App\User\Models\UserSettingsModel;
use App\User\Storages\UserAuthStorage;
use App\User\Storages\UserRolesStorage;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use App\User\Types\AuthType;
use Doctrine\Common\Collections\ArrayCollection;
use JetBrains\PhpStorm\Pure;

class UserService
{
    private ?UserEntity $user = null;

    private const DEFAULT_CURRENCY = 'RUB';
    private const ACCEPT_LANGUAGES = ['en', 'ru'];

    public function __construct(
        private readonly UserStorage $userStorage,
        private readonly UserAuthStorage $userAuthStorage,
        private readonly UserSettingsStorage $userSettingsStorage,
        private readonly UserRolesStorage $userRolesStorage,
        private readonly UserAccountService $userAccountService,
        private readonly AuthServiceInterface $yandexAuthService,
        private readonly AuthServiceInterface $googleAuthService,
        private readonly AuthServiceInterface $telegramAuthService,
        private readonly AuthServiceInterface $telegramWebAuthService,
        private readonly AuthService $authService,
        private readonly Mailer $mailer
    ) {
    }

    #[Pure]
    public function getUser(): ?UserEntity
    {
        return $this->user;
    }

    #[Pure]
    public function getUserId(): int
    {
        return $this->getUser()->getId();
    }

    public function setUser(?UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @throws AuthTypeException
     */
    public function createAndLogin(string $authType, string $accessToken, string $lng): void
    {
        $auth = AuthType::tryFrom($authType);
        if ($auth === null) {
            throw new AuthTypeException('Auth type is not supported');
        }

        $user = $this->authorizeUser($auth, $accessToken);

        if (!in_array($lng, self::ACCEPT_LANGUAGES)) {
            $lng = 'en';
        }

        $this->getAndCreateUser($user, $lng);
    }

    public function login(AuthType $authType, string $token): ?UserEntity
    {
        $userAuth = $this->userAuthStorage->findActiveByAuthTypeAndToken($authType, $token);
        if ($userAuth === null) {
            return $this->user;
        }

        $authService = $this->getAuthService($authType);
        $userAuthModel = $authService->getUserInfo($userAuth->getAccessToken());
        if (null !== $userAuthModel && $userAuthModel->getClientId() === $userAuth->getClientId()) {
            $this->setUser($userAuth->getUser());
        }

        return $this->user;
    }

    public function linkAccount(AuthType $authType, string $accessToken): void
    {
        $user = $this->authorizeUser($authType, $accessToken);

        $userEntity = $this->userAccountService->getUserByAuthModel($user);
        if ($userEntity?->getId() === $this->getUserId()) {
            throw new UserAccountException("Account is already linked");
        }

        if ($userEntity !== null) {
            $userEntity->setIsActive(0);
            foreach ($userEntity->getUserAccount()->getIterator() as $userAccount) {
                $userAccount->setIsActive(0);
            }
            $this->userStorage->addEntity($userEntity);

            $userAuthEntities = $this->userAuthStorage->findActiveByUserId($userEntity->getId());
            foreach ($userAuthEntities as $userAuthEntity) {
                $userAuthEntity->setIsActive(0);
            }
            $this->userAuthStorage->addEntityArray($userAuthEntities);
        }

        $userEntity = $this->userStorage->findActiveById($this->getUserId());
        $userAccount = new UserAccountEntity();
        $userAccount->setUser($userEntity);
        $userAccount->setUserId($this->getUserId());
        $userAccount->setAuthType($authType);
        $userAccount->setClientId($user->getClientId());
        $userAccount->setIsActive(1);
        $userEntity->getUserAccount()->add($userAccount);
        $this->userStorage->addEntity($userEntity);

        $userAuth = new UserAuthEntity();
        $userAuth->setAccessToken($user->getAccessToken());
        $userAuth->setAuthType($user->getAuthType()->value);
        $userAuth->setClientId($user->getClientId());
        $userAuth->setIsActive(1);
        $userAuth->setUser($userEntity);

        $this->userAuthStorage->addEntity($userAuth);
    }

    private function authorizeUser(AuthType $authType, string $accessToken): UserAuthModel
    {
        $authService = $this->getAuthService($authType);

        if (null === $authService) {
            throw new AuthTypeException('Auth type is not supported');
        }

        $user = $authService->getUserInfo($accessToken);
        if (null === $user) {
            throw new AuthTypeException('User is not authorized', 2011);
        }

        return $user;
    }

    private function getAndCreateUser(UserAuthModel $user, string $lng): void
    {
        $userEntity = $this->userAccountService->getUserByAuthModel($user);

        //TODO: remove it after creation user_accounts for existing users
        if ($userEntity === null && $user->getAuthType() === AuthType::YANDEX) {
            $userEntity = $this->userStorage->findByLogin($user->getLogin());

            if ($userEntity !== null) {
                $userAccount = new UserAccountEntity();
                $userAccount->setUserId($userEntity->getId());
                $userAccount->setAuthType($user->getAuthType());
                $userAccount->setClientId($user->getClientId());
                $userAccount->setUser($userEntity);
                if ($userEntity->getUserAccount() === null) {
                    $userEntity->setUserAccount(new ArrayCollection([$userAccount]));
                } else {
                    $userEntity->getUserAccount()->add($userAccount);
                }

                $this->userStorage->addEntity($userEntity);
            }
        }

        if (null === $userEntity) {
            $userEntity = new UserEntity();
            $userEntity->setLogin($user->getLogin());
            $userEntity->setEmail($user->getEmail());
            $userEntity->setName($user->getName());
            $userEntity->setBirthday($user->getBirthday());
            $userEntity->setLanguage($lng);

            $userSettingsEntity = new UserSettingsEntity();
            $userSettingsEntity->setExpensesCurrency('RUB');
            $userSettingsEntity->setDesiredPensionCurrency('RUB');
            $userSettingsEntity->setUser($userEntity);

            $userSettingsEntity = $this->userSettingsStorage->addEntity($userSettingsEntity);
            $userEntity = $userSettingsEntity->getUser();

            $userAccount = new UserAccountEntity();
            $userAccount->setAuthType($this->userAccountService->getAuthType($user->getAuthType()));
            $userAccount->setClientId($user->getClientId());
            $userAccount->setUser($userEntity);
            if ($userEntity->getUserAccount() === null) {
                $userEntity->setUserAccount(new ArrayCollection([$userAccount]));
            } else {
                $userEntity->getUserAccount()->add($userAccount);
            }

            $this->mailer->send([$user->getEmail()], 'registration');
        }

        $userAuth = $this->userAuthStorage->findActiveByAuthTypeAndToken(
            $user->getAuthType(),
            $user->getAccessToken()
        );

        if (null === $userAuth) {
            $userAuth = new UserAuthEntity();
            $userAuth->setAccessToken($user->getAccessToken());
            $userAuth->setAuthType($user->getAuthType()->value);
            $userAuth->setClientId($user->getClientId());
            $userAuth->setIsActive(1);
            $userAuth->setUser($userEntity);
            $this->userAuthStorage->addEntity($userAuth);
        }

        $this->login($user->getAuthType(), $userAuth->getAccessToken());
    }

    public function getActiveUserById(int $userId): ?UserEntity
    {
        return $this->userStorage->findActiveById($userId);
    }

    public function getUserModelById(int $userId): UserModel
    {
        return $this->convertUserEntityToModel($this->userStorage->findActiveById($userId));
    }

    public function getUserModelByTelegramId(int $telegramId): UserModel
    {
        $user = $this->userStorage->findByTelegramId($telegramId);
        if (null === $user) {
            throw new UserException('User not found', 2032);
        }

        return $this->convertUserEntityToModel($this->userStorage->findByTelegramId($telegramId));
    }

    public function getUserSettings(int $userId): ?UserSettingsModel
    {
        $userSettingsEntity = $this->userSettingsStorage->findByUserId($userId);

        if ($userSettingsEntity === null) {
            return null;
        }

        return $this->convertUserSettingsEntityToModel(
            $userSettingsEntity
        );
    }

    public function setUserSettings(UserSettingsModel $userSettings): void
    {
        $userSettingsEntity = $this->userSettingsStorage->findByUserId($this->getUserId());
        if ($userSettingsEntity?->getId() !== $userSettings?->getUserSettingsId()) {
            throw new UserAccessDeniedException('Access denied for user', 2024);
        }

        if ($userSettingsEntity === null) {
            $userSettingsEntity = new UserSettingsEntity();
            $userSettingsEntity->setUserId($this->getUserId());
            $userSettingsEntity->setUser($this->userStorage->findActiveById($this->getUserId()));
        }

        $userSettingsEntity->setDesiredPensionAmount($userSettings->getDesiredPension()?->getAmount());
        $userSettingsEntity->setDesiredPensionCurrency(
            $userSettings->getDesiredPension()?->getCurrency() ?? 'RUB'
        );
        $userSettingsEntity->setRetirementAge($userSettings->getRetirementAge());
        $userSettingsEntity->setExpensesAmount($userSettings->getExpenses()?->getAmount());
        $userSettingsEntity->setExpensesCurrency(
            $userSettings->getExpenses()?->getCurrency() ?? 'RUB'
        );

        $userSettingsEntity->getUser()->setBirthday($userSettings->getBirthday());

        $this->userSettingsStorage->addEntity($userSettingsEntity);
    }

    public function updateUserSettings(UserSettingsModel $userSettings): void
    {
        $userSettingsEntity = $this->userSettingsStorage->findByUserId($this->getUserId());
        if ($userSettingsEntity === null) {
            $user = $this->userStorage->findActiveById($this->getUserId());
            $userSettingsEntity = new UserSettingsEntity();
            $userSettingsEntity->setUser($user);
        }

        if ($userSettings->getRetirementAge() !== null) {
            $userSettingsEntity->setRetirementAge($userSettings->getRetirementAge());
        }

        if ($userSettings->getBirthday() !== null) {
            $userSettingsEntity->getUser()->setBirthday($userSettings->getBirthday());
        }

        if ($userSettings->getExpenses()?->getAmount() !== null) {
            $userSettingsEntity->setExpensesAmount($userSettings->getExpenses()->getAmount());
            $userSettingsEntity->setExpensesCurrency($userSettings->getExpenses()->getCurrency());
        }

        if ($userSettings->getDesiredPension()?->getAmount() !== null) {
            $userSettingsEntity->setDesiredPensionAmount($userSettings->getDesiredPension()->getAmount());
            $userSettingsEntity->setDesiredPensionCurrency($userSettings->getDesiredPension()->getCurrency());
        }

        if ($userSettingsEntity->getDesiredPensionCurrency() === null) {
            $userSettingsEntity->setDesiredPensionCurrency(self::DEFAULT_CURRENCY);
        }
        if ($userSettingsEntity->getExpensesCurrency() === null) {
            $userSettingsEntity->setExpensesCurrency(self::DEFAULT_CURRENCY);
        }

        $this->userSettingsStorage->addEntity($userSettingsEntity);
    }

    public function updateUserData(UserModel $userModel): void
    {
        $userEntity = $this->user;
        if ($userModel->getLanguage() !== null) {
            if (!in_array($userModel->getLanguage(), self::ACCEPT_LANGUAGES)) {
                throw new UserException('Language is not supported', 2030);
            }

            $userEntity->setLanguage($userModel->getLanguage());
        }

        $this->userStorage->addEntity($userEntity);
    }

    public function generateJWT(UserEntity $user, string $authType)
    {
        $payload = [
            'iis' => 'tinkoff_invest_analytics',
            'sub' => 'user data',
            'auth_type' => $authType,
            'userId' => $user->getId(),
            'userLogin' => $user->getLogin()
        ];

        return $this->authService->generateJWT($payload);
    }

    public function getUserList(): UserCollection
    {
        return new UserCollection($this->userStorage->findAll());
    }

    public function getActiveUserList(): UserCollection
    {
        return new UserCollection($this->userStorage->findActive());
    }

    public function getUserModelList(): UserModelCollection
    {
        $collection = new UserModelCollection();
        $users = $this->userStorage->findAll();

        foreach ($users as $user) {
            $collection->add($this->convertUserEntityToModel($user));
        }

        return $collection;
    }

    public function addUser(UserEntity $user): int
    {
        $this->userStorage->addEntity($user);
        return $user->getId();
    }

    public function updateUser(UserEntity $user): void
    {
        $this->userStorage->addEntity($user);
    }

    public function getUserRoles(): array
    {
        $result = [];
        $roles = $this->userRolesStorage->findActiveByUserId($this->getUserId());
        foreach ($roles as $role) {
            $result[] = $role->getRole();
        }
        return $result;
    }

    private function getAuthService(AuthType $authType): ?AuthServiceInterface
    {
        return match ($authType) {
            AuthType::YANDEX => $this->yandexAuthService,
            AuthType::GOOGLE => $this->googleAuthService,
            AuthType::TELEGRAM => $this->telegramAuthService,
            AuthType::TELEGRAM_WEB => $this->telegramWebAuthService,
            default => null
        };
    }

    private function convertUserSettingsEntityToModel(UserSettingsEntity $entity): UserSettingsModel
    {
        return new UserSettingsModel([
            'userSettingsId' => $entity->getId(),
            'userId' => $entity->getUserId(),
            'user' => $this->convertUserEntityToModel($entity->getUser()),
            'desiredPension' => $entity->getDesiredPensionAmount() === null
                ? null
                : new PriceModel([
                    'amount' => $entity->getDesiredPensionAmount(),
                    'currency' => $entity->getDesiredPensionCurrency()
                ]),
            'retirementAge' => $entity->getRetirementAge(),
            'expenses' => $entity->getExpensesAmount() === null
                ? null
                : new PriceModel([
                    'amount' => $entity->getExpensesAmount(),
                    'currency' => $entity->getExpensesCurrency()
                ])
        ]);
    }

    private function convertUserEntityToModel(UserEntity $entity): UserModel
    {
        return new UserModel([
            'userId' => $entity->getId(),
            'login' => $entity->getLogin(),
            'email' => $entity->getEmail(),
            'name' => $entity->getName(),
            'birthday' => $entity->getBirthday(),
            'language' => $entity->getLanguage(),
            'isNewUser' => (bool)$entity->getIsNewUser(),
            'telegramId' => $entity->getTelegramId(),
        ]);
    }
}
