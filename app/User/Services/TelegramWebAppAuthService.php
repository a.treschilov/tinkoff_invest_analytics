<?php

declare(strict_types=1);

namespace App\User\Services;

use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Types\AuthType;

class TelegramWebAppAuthService implements AuthServiceInterface
{
    public function __construct(
        private readonly string $telegramToken
    ) {
    }
    public function getUserInfo(string $accessToken): ?UserAuthModel
    {
        $data = json_decode(base64_decode($accessToken), true);

        parse_str($data, $authData);

        $checkHash = $authData['hash'];
        unset($authData['hash']);

        ksort($authData);

        $dataString = '';
        foreach ($authData as $key => $value) {
            $dataString .= "{$key}={$value}\n";
        }
        $dataCheckString = rtrim($dataString, "\n");

        $secretKey = hash_hmac('sha256', $this->telegramToken, "WebAppData", true);
        $hash = hash_hmac('sha256', $dataCheckString, $secretKey);
        if (strcmp($hash, $checkHash) !== 0) {
            return null;
        }
        if ((time() - $authData['auth_date']) > 180 * 24 * 60 * 60) {
            return null;
        }

        $user = json_decode($authData['user'], true);
        return new UserAuthModel([
            'authType' => AuthType::TELEGRAM_WEB,
            'login' => $user['username'],
            'name' => $user['first_name'] . " " . $user['last_name'],
            'clientId' => (string)$user['id'],
            'accessToken' => $accessToken,
        ]);
    }
}
