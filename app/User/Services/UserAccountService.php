<?php

declare(strict_types=1);

namespace App\User\Services;

use App\User\Collections\UserAccountCollection;
use App\User\Entities\UserAccountEntity;
use App\User\Entities\UserEntity;
use App\User\Exceptions\UserAccountException;
use App\User\Models\UserAccountModel;
use App\User\Models\UserAuthModel;
use App\User\Storages\UserAccountStorage;
use App\User\Types\AuthType;

readonly class UserAccountService
{
    public function __construct(
        private UserAccountStorage $userAccountStorage,
    ) {
    }

    public function getAuthType(AuthType $authType): AuthType
    {
        return match ($authType) {
            AuthType::TELEGRAM_WEB => AuthType::TELEGRAM,
            default => $authType,
        };
    }

    public function getUserByAuthModel(UserAuthModel $user): ?UserEntity
    {
        $authType = $this->getAuthType($user->getAuthType());

        $userAccount = $this->userAccountStorage->findByAuthTypeAndClientId(
            $authType,
            $user->getClientId()
        );

        return $userAccount?->getUser();
    }

    public function getUserAccountList(int $userId): UserAccountCollection
    {
        $collection = new UserAccountCollection();

        $entities = $this->userAccountStorage->findActiveByUserId($userId);
        foreach ($entities as $entity) {
            $collection->add($this->convertEntityToModel($entity));
        }

        return $collection;
    }

    public function unlinkAccount(int $userId, AuthType $authType): void
    {
        $userAccounts = $this->getUserAccountList($userId);
        if (count($userAccounts) === 1) {
            throw new UserAccountException("There are impossible to delete an single account", 2042);
        }

        $userAccount = $this->userAccountStorage->findActiveByUserIdAndAuthType($userId, $authType);
        if ($userAccount === null) {
            throw new UserAccountException("The account does not exist", 2041);
        }

        $userAccount->setIsActive(0);
        $this->userAccountStorage->addEntity($userAccount);
    }

    private function convertEntityToModel(UserAccountEntity $userAccountEntity): UserAccountModel
    {
        return new UserAccountModel([
            'id' => $userAccountEntity->getId(),
            'userId' => $userAccountEntity->getUserId(),
            'authType' => $this->getAuthType($userAccountEntity->getAuthType()),
            'clientId' => $userAccountEntity->getClientId(),
            'isActive' => (bool)$userAccountEntity->getIsActive(),
        ]);
    }
}
