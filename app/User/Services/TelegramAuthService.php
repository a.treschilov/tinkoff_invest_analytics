<?php

declare(strict_types=1);

namespace App\User\Services;

use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Types\AuthType;

class TelegramAuthService implements AuthServiceInterface
{
    public function __construct(
        private readonly string $telegramToken
    ) {
    }
    public function getUserInfo(string $accessToken): ?UserAuthModel
    {
        $authData = json_decode(base64_decode($accessToken), true);
        $checkHash = $authData['hash'];
        unset($authData['hash']);
        $dataCheckArr = [];
        foreach ($authData as $key => $value) {
            $dataCheckArr[] = $key . '=' . $value;
        }
        sort($dataCheckArr);
        $dataCheckString = implode("\n", $dataCheckArr);
        $secretKey = hash('sha256', $this->telegramToken, true);
        $hash = hash_hmac('sha256', $dataCheckString, $secretKey);
        if (strcmp($hash, $checkHash) !== 0) {
            return null;
        }
        if ((time() - $authData['auth_date']) > 180 * 24 * 60 * 60) {
            return null;
        }

        return new UserAuthModel([
            'authType' => AuthType::TELEGRAM,
            'login' => $authData['username'],
            'name' => $authData['first_name'] . " " . $authData['last_name'],
            'clientId' => (string)$authData['id'],
            'accessToken' => $accessToken,
        ]);
    }
}
