<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Broker\Models\BrokerAccountModel;
use App\Services\StockBrokerFactory;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Storages\UserBrokerAccountStorage;

class UserBrokerAccountService
{
    private StockBrokerFactory $stockBrokerFactory;
    private UserCredentialService $userCredentialService;
    private UserBrokerAccountStorage $userBrokerAccountStorage;

    public function __construct(
        StockBrokerFactory $stockBrokerFactory,
        UserCredentialService $userCredentialService,
        UserBrokerAccountStorage $userBrokerAccountStorage
    ) {
        $this->stockBrokerFactory = $stockBrokerFactory;
        $this->userCredentialService = $userCredentialService;
        $this->userBrokerAccountStorage = $userBrokerAccountStorage;
    }

    /**
     * @param int $credentialId
     * @return UserBrokerAccountCollection
     * @throws \App\Exceptions\AccountException
     * @throws \App\User\Exceptions\UserAccessDeniedException
     */
    public function getUserAccountListByCredential(int $credentialId): UserBrokerAccountCollection
    {
        $credential = $this->userCredentialService->getUserCredentialById($credentialId);
        $client = $this->stockBrokerFactory->getBroker($credential);
        $brokerAccounts = $client->getAccounts();

        $userBrokerAccounts = new UserBrokerAccountCollection(
            $this->userBrokerAccountStorage->findByCredentialId($credentialId)
        );

        $result = new UserBrokerAccountCollection();
        /** @var BrokerAccountModel $brokerAccount */
        foreach ($brokerAccounts->getIterator() as $brokerAccount) {
            $account = $userBrokerAccounts->findByExternalId($brokerAccount->getId()) ?? new UserBrokerAccountEntity();
            $account->setName($brokerAccount->getName());
            $account->setExternalId($brokerAccount->getId());
            $account->setIsActive($account->getIsActive() ?? 0);
            $account->setType($brokerAccount->getType()->value);
            $result->add($account);
        }

        return $result;
    }

    public function getActiveUserAccountListByCredential(int $credentialId): UserBrokerAccountCollection
    {
        return new UserBrokerAccountCollection(
            $this->userBrokerAccountStorage->findActiveByCredentialId($credentialId)
        );
    }

    public function getUserAccountByUserId(int $userId): UserBrokerAccountCollection
    {
        $credentials = $this->userCredentialService->getActiveCredentialList($userId);

        $result = new UserBrokerAccountCollection();
        /** @var UserCredentialModel $credential */
        foreach ($credentials->getIterator() as $credential) {
            $accountCredentials = $this->getActiveUserAccountListByCredential($credential->getUserCredentialId());
            /** @var UserBrokerAccountEntity $accountCredential */
            foreach ($accountCredentials->getIterator() as $accountCredential) {
                if ($accountCredential->getType() !== 3) {
                    $result->add($accountCredential);
                }
            }
        }
        return $result;
    }

    /**
     * @throws \App\Exceptions\AccountException
     * @throws \App\User\Exceptions\UserAccessDeniedException
     */
    public function updateUserAccountList(int $credentialId, UserBrokerAccountCollection $accountCollection)
    {
        $credential = $this->userCredentialService->getUserCredentialById($credentialId);

        $accounts = $this->userBrokerAccountStorage->findByCredentialId($credential->getId());
        foreach ($accounts as $userAccountEntity) {
            $account = $accountCollection->findByExternalId($userAccountEntity->getExternalId());
            $needUpdate = false;
            if (null === $account) {
                if ($userAccountEntity->getIsDeleted() === 0) {
                    $userAccountEntity->setIsDeleted(1);
                    $needUpdate = true;
                }
            } else {
                if ($userAccountEntity->getIsActive() !== $account->getIsActive()) {
                    $needUpdate = true;
                    $userAccountEntity->setIsActive($account->getIsActive());
                }
            }

            if ($needUpdate) {
                $userAccountEntity->setUpdatedDate(new \DateTime());
                $this->userBrokerAccountStorage->addEntity($userAccountEntity);
            }
        }

        $client = $this->stockBrokerFactory->getBroker($credential);
        $brokerAccounts = $client->getAccounts();
        /** @var UserBrokerAccountEntity $userAccountEntity */
        foreach ($accountCollection->getIterator() as $userAccountEntity) {
            $isAccountExist = false;
            foreach ($accounts as $account) {
                if ($account->getExternalId() === $userAccountEntity->getExternalId()) {
                    $isAccountExist = true;
                    break;
                }
            }
            if ($isAccountExist === false) {
                $brokerAccount = $brokerAccounts->findById($userAccountEntity->getExternalId());
                $userAccountEntity->setUserCredentialId($credential->getId());
                $userAccountEntity->setName($brokerAccount->getName());
                $userAccountEntity->setType($brokerAccount->getType()->value);
                $userAccountEntity->setIsDeleted(0);
                $userAccountEntity->setCreatedDate(new \DateTime());
                $userAccountEntity->setUpdatedDate(new \DateTime());
                $userAccountEntity->setUserCredential($credential);
                $this->userBrokerAccountStorage->addEntity($userAccountEntity);
            }
        }
    }

    public function getUserBrokerAccountById(int $userBrokerAccountId): ?UserBrokerAccountEntity
    {
        return $this->userBrokerAccountStorage->findActiveById($userBrokerAccountId);
    }
}
