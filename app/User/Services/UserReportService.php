<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Broker\Types\BrokerOperationStatus;
use App\Common\Amqp\AmqpClient;
use App\Common\Mailer\Mailer;
use App\Item\Models\OperationFiltersModel;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService;
use App\Item\Types\OperationType;
use App\Services\WealthService;
use App\User\Entities\UserEntity;

class UserReportService
{
    public function __construct(
        private readonly AmqpClient $amqpClient,
        private readonly UserService $userService,
        private readonly PortfolioService $portfolioService,
        private readonly WealthService $wealthService,
        private readonly OperationService $operationService,
        private readonly Mailer $mailSender
    ) {
    }

    public function scheduleMonthlyReportUser(\DateTime $date): array
    {
        $messages = [];

        $users = $this->userService->getActiveUserList();
        /** @var UserEntity $user */
        foreach ($users->getIterator() as $user) {
            if ($user->getIsNewUser() === 0) {
                $startOfMonth = $this->getFirstMonthDay($date);
                $endOfMonth = $this->getLastMonthDay($date);

                if (
                    $this->portfolioService->getSavedDayPortfolio($user->getId(), $startOfMonth) !== null
                    && $this->portfolioService->getSavedDayPortfolio($user->getId(), $endOfMonth) !== null
                ) {
                    $message = [
                        'type' => 'monthlyReport',
                        'data' => [
                            'date' => (clone $date)->getTimestamp(),
                            'userId' => $user->getId(),
                        ]
                    ];
                    $this->amqpClient->publishMessage($message);

                    $messages[] = $message;
                }
            }
        }

        return $messages;
    }

    public function sendMonthlyReport(UserEntity $user, \DateTime $date): void
    {
        $data = $this->calculateMonthlyReportData($user->getId(), $date);

        $positiveColor = "rgb(25, 135, 84)";
        $negativeColor = "rgb(220, 53, 69)";
        $neutralColor = "#000";
        $positiveIcon = '▲';
        $negativeIcon = '▼';
        $neutralIcon = "";

        $emailData = [
            'name' => $user->getLogin(),
            'monthSubject' => mb_strtolower($this->formatMonthToRussian((int)$date->format('n'))),
            'month' => $this->formatMonthToRussian((int)$date->format('n')) . ' ' . $date->format('Y'),
            'wealthRate' => $data['wealthRate'] === null
                ? '—'
                : number_format($data['wealthRate'], 2, ',', ' '),
            'wealthRateValueChange' => $data['wealthRateValueChange'] !== null
                ? number_format($data['wealthRateValueChange'], 2, ',', ' ')
                : '—',
            'wealthRateValueColor' => $data['wealthRateValueChange'] === null
                ? $neutralColor
                : ($data['wealthRateValueChange'] >= 0 ? $positiveColor : $negativeColor),
            'wealthRateChangeIcon' => $data['wealthRateValueChange'] === null
                ? $neutralIcon
                : ($data['wealthRateValueChange'] >= 0 ? $positiveIcon : $negativeIcon),
            'capitalValue' => number_format($data['capitalValue'], 2, ',', ' ') . ' ₽',
            'changeCapitalValue' => number_format($data['changeCapitalValue'], 0, ',', ' ') . ' ₽',
            'changeCapitalValueChange' => number_format(
                $data['changeCapitalValueChange'] * 100,
                1,
                ',',
                ' '
            ) . '%',
            'changeCapitalValueColor' => $data['changeCapitalValue'] >= 0 ? $positiveColor : $negativeColor,
            'changeCapitalValueIcon' => $data['changeCapitalValue'] >= 0 ? $positiveIcon : $negativeIcon,
            'payinValue' => number_format($data['payinValue'], 0, ',', ' ') . ' ₽',
            'passiveIncomeValue' => number_format($data['passiveIncomeValue'], 0, ',', ' ') . ' ₽',
            'passiveIncomeChangeValue' => $data['passiveIncomeChangeValue'] !== null
                ? number_format(
                    abs($data['passiveIncomeChangeValue']) * 100,
                    1,
                    ',',
                    ' '
                ) . '%'
                : '—',
            'passiveIncomeColor' => $data['passiveIncomeValue'] >= $data['passiveIncomePreviousValue']
                ? $positiveColor
                : $negativeColor,
            'passiveIncomeChangeIcon'  => $data['passiveIncomeValue'] >= $data['passiveIncomePreviousValue']
                ? $positiveIcon
                : $negativeIcon,
            'pension' => number_format($data['pension'], 0, ',', ' ') . ' ₽',
            'pensionChange' => number_format($data['pensionChange'], 0, ',', ' ') . ' ₽',
            'asm' => [
                "group_id" => $data["asm"]["group_id"],
                "groups_to_display" => $data["asm"]["groups_to_display"],
            ]
        ];

        $this->mailSender->send([$user->getEmail()], 'monthlyReport', $emailData);
    }

    private function getFirstMonthDay(\DateTime $date): \DateTime
    {
        $startOfMonth = clone $date;
        $startOfMonth->setDate(
            (int)$startOfMonth->format('Y'),
            (int)$startOfMonth->format('m'),
            1
        );

        return $startOfMonth;
    }

    private function getLastMonthDay(\DateTime $date): \DateTime
    {
        $endOfMonth = clone $date;
        $endOfMonth->setDate(
            (int)$endOfMonth->format('Y'),
            (int)$endOfMonth->format('m') + 1,
            1
        );

        return $endOfMonth;
    }

    private function getFirstPreviousMonthDay(\DateTime $date): \DateTime
    {
        $startOfMonth = clone $date;
        $startOfMonth->setDate(
            (int)$startOfMonth->format('Y'),
            (int)$startOfMonth->format('m') - 1,
            1
        );

        return $startOfMonth;
    }

    /**
     * @param int $userId
     * @param \DateTime $date
     * @return array{
     *  wealthRate: float|null,
     *  wealthRateValueChange: float|null,
     *  capitalValue: float,
     *  changeCapitalValue: float,
     *  changeCapitalValueChange: float,
     *  payinValue: float,
     *  passiveIncomeValue: float,
     *  passiveIncomePreviousValue: float,
     *  passiveIncomeChangeValue: float|null,
     *  pension: float,
     *  pensionChange: float
     * }
     */
    public function calculateMonthlyReportData(int $userId, \DateTime $date): array
    {
        $baseCurrency = 'RUB';
        $unsubscribeGroupId = 22073;

        $firstPreviousPeriodDate = $this->getFirstPreviousMonthDay($date);
        $firstPreviousPeriodDate->setTime(0, 0, 0);
        $firstPeriodDate = $this->getFirstMonthDay($date);
        $firstPeriodDate->setTime(0, 0, 0);
        $lastPeriodDate = $this->getLastMonthDay($date);
        $lastPeriodDate->setTime(0, 0, 0);

        $operationInputFilter = new OperationFiltersModel([
            'dateFrom' => $firstPeriodDate,
            'dateTo' => $lastPeriodDate,
            'operationStatus' => [BrokerOperationStatus::DONE->value],
            'operationType' => [OperationType::INPUT->value],
        ]);

        $wealthEnd = $this->wealthService->getWealthByDay($userId, $lastPeriodDate);
        $wealthStart = $this->wealthService->getWealthByDay($userId, $firstPeriodDate);

        $wealthRateValueChange = $wealthEnd->getWealthRate() !== null || $wealthStart->getWealthRate() !== null
            ? $wealthEnd->getWealthRate() - $wealthStart->getWealthRate()
            : null;
        $changeCapitalValue = $this->wealthService->calculateCapital($wealthEnd)
            - $this->wealthService->calculateCapital($wealthStart);
        $input = $this->operationService->calculateOperationSummary(
            $this->operationService->getFilteredUserOperation($userId, $operationInputFilter),
            $baseCurrency
        );
        $passiveIncome = $this->operationService->calculateIncome($userId, $firstPeriodDate, $lastPeriodDate);
        $previousPassiveIncome = $this->operationService->calculateIncome(
            $userId,
            $firstPreviousPeriodDate,
            $firstPeriodDate
        );
        $passiveIncomeChange = $passiveIncome - $previousPassiveIncome;

        return [
            'wealthRate' => $wealthEnd?->getWealthRate(),
            'wealthRateValueChange' => $wealthRateValueChange,
            'capitalValue' => $this->wealthService->calculateCapital($wealthEnd),
            'changeCapitalValue' => $changeCapitalValue,
            'changeCapitalValueChange' => $this->wealthService->calculateCapital($wealthStart) !== 0.0
                ? abs($changeCapitalValue / $this->wealthService->calculateCapital($wealthStart))
                : null
            ,
            'payinValue' => $input->getAmount(),
            'passiveIncomeValue' => $passiveIncome,
            'passiveIncomePreviousValue' => $previousPassiveIncome,
            'passiveIncomeChangeValue' => $previousPassiveIncome !== 0.0
                ? abs($passiveIncomeChange / $previousPassiveIncome)
                : null,
            'pension' => $this->wealthService->calculateCapital($wealthEnd) * 0.04 / 12,
            'pensionChange' => $changeCapitalValue * 0.04 / 12,
            'asm' => [
                "group_id" => $unsubscribeGroupId,
                "groups_to_display" => [$unsubscribeGroupId],
            ]
        ];
    }

    private function formatMonthToRussian(int $month): string
    {
        return match ($month) {
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь',
            default => '',
        };
    }
}
