<?php

declare(strict_types=1);

namespace App\User\Services;

use App\User\Entities\UserCodeEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Storages\UserCodeStorage;
use Ramsey\Uuid\Uuid;

readonly class UserCodeService
{
    public function __construct(
        private UserCodeStorage $userCodeStorage,
        private UserService $userService
    ) {
    }

    public function releaseCode($userId): string
    {
        $user = $this->userService->getUser();
        $date = new \DateTime();
        $date->add(new \DateInterval("PT1H"));

        $code = $this->generateCode();

        $entity = new UserCodeEntity();
        $entity->setUserId($userId);
        $entity->setUser($user);
        $entity->setCode($code);
        $entity->setExpireDate($date);
        $this->userCodeStorage->addEntity($entity);

        return $code;
    }

    public function connect($clientId, $code): void
    {
        $userCode = $this->checkCode($code);

        if ($userCode === null) {
            throw new UserAccessDeniedException('Code is invalid or expired. Please try again.');
        }

        $userCode->getUser()->setTelegramId($clientId);
        $this->userCodeStorage->addEntity($userCode);
    }

    private function generateCode(): string
    {
        return substr(Uuid::uuid4()->toString(), 0, 8);
    }

    private function checkCode($code): ?UserCodeEntity
    {
        $date = new \DateTime();
        return $this->userCodeStorage->findByCodeBeforeDate($code, $date);
    }
}
