<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Common\HttpClient;
use App\User\Interfaces\AuthServiceInterface;
use App\User\Models\UserAuthModel;
use App\User\Types\AuthType;

class YandexAuthService implements AuthServiceInterface
{
    private HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getUserInfo(string $accessToken): ?UserAuthModel
    {
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            'info',
            ['format' => 'json'],
            ['Authorization' => 'OAuth ' . $accessToken]
        );

        if ($response->getStatusCode() === 401) {
            return null;
        }

        $responseBody = json_decode($response->getBody()->getContents(), true);
        $modelData = [
            'access_token' => $accessToken,
            'authType' => AuthType::YANDEX,
            'login' => $responseBody['default_email'],
            'name' => $responseBody['display_name'],
            'email' => $responseBody['default_email'],
            'clientId' => $responseBody['id'],
            'birthday' => new \DateTime($responseBody['birthday'])
        ];
        return new UserAuthModel($modelData);
    }
}
