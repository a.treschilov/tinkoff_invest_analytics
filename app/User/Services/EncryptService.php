<?php

namespace App\User\Services;

class EncryptService
{
    private string $encryptKey;

    public function __construct(string $encryptKey)
    {
        $this->encryptKey = $encryptKey;
    }

    public function encrypt(string $string): string
    {
        $iv = random_bytes(16);
        $code = openssl_encrypt(
            $string,
            "AES-256-CBC",
            $this->encryptKey,
            0,
            $iv
        );

        $result = [
            'code' => $code,
            'vector' => bin2hex($iv)
        ];

        return base64_encode(json_encode($result));
    }

    public function decrypt(?string $string): ?string
    {
        if ($string === null) {
            return null;
        }

        $data = json_decode(base64_decode($string), true);

        return openssl_decrypt(
            $data['code'],
            "AES-256-CBC",
            $this->encryptKey,
            0,
            hex2bin($data['vector'])
        );
    }
}
