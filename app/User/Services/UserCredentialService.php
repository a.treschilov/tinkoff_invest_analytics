<?php

declare(strict_types=1);

namespace App\User\Services;

use App\Broker\Services\BrokerHelper;
use App\Broker\Services\BrokerService;
use App\Exceptions\DoctrineException;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserCredentialEntity;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Models\UserCredentialModel;
use App\User\Storages\UserCredentialStorage;

class UserCredentialService
{
    public function __construct(
        private readonly EncryptService $encryptService,
        private readonly UserCredentialStorage $userCredentialStorage,
        private readonly UserService $userService,
        private readonly BrokerService $brokerService,
        private readonly BrokerHelper $brokerHelper
    ) {
    }

    public function getCredentialList(int $userId): UserCredentialCollection
    {
        $userCredentialCollection = new UserCredentialCollection();
        $credentialList = $this->userCredentialStorage->findByUserId($userId);
        foreach ($credentialList as $credential) {
            $userCredentialCollection->add($this->convertEntityToModel($credential));
        }
        return $userCredentialCollection;
    }

    public function getActiveCredentialList(int $userId): UserCredentialCollection
    {
        $userCredentialCollection = new UserCredentialCollection();
        $credentialList = $this->userCredentialStorage->findActiveByUserId($userId);
        foreach ($credentialList as $credential) {
            $userCredentialCollection->add($this->convertEntityToModel($credential));
        }
        return $userCredentialCollection;
    }

    public function getAllActiveCredentialList(): UserCredentialCollection
    {
        $collection = new UserCredentialCollection();
        $credentialEntities = $this->userCredentialStorage->findActive();
        foreach ($credentialEntities as $entity) {
            $collection->add($this->convertEntityToModel($entity));
        }
        return $collection;
    }

    public function getUserActiveCredential(int $userId, string $broker): ?UserCredentialModel
    {
        $credential = $this->userCredentialStorage->findActiveByUserIdAndBroker($userId, $broker);
        return $credential === null ? null : $this->convertEntityToModel($credential);
    }

    public function getUserCredentialById(int $userCredentialId): ?UserCredentialEntity
    {
        $credential = $this->userCredentialStorage->findOneById($userCredentialId);

        if (null === $credential) {
            throw new UserAccessDeniedException('User Credential not found', 2020);
        }

        if ($credential->getUserId() !== $this->userService->getUserId()) {
            throw new UserAccessDeniedException('Access denied for user', 2021);
        }

        return  $credential;
    }

    public function getUserCredentialDecryptedById(int $userCredentialId): ?UserCredentialModel
    {
        $credentialEntity = $this->getUserCredentialById($userCredentialId);
        return $this->convertEntityToModel($credentialEntity);
    }

    public function addUserCredential(UserCredentialModel $userCredentialModel): ?UserCredentialEntity
    {
        $user = $this->userService->getUser();
        $userCredentialModel->setUserId($this->userService->getUserId());
        $userCredentialModel->setIsActive($userCredentialModel->getIsActive() ?? 1);


        $userCredentialEntity = $this->convertModelToEntity($userCredentialModel);
        $userCredentialEntity->setUser($user);

        try {
            $this->userCredentialStorage->addEntity($userCredentialEntity);
        } catch (\Exception $e) {
            throw new DoctrineException($e->getMessage());
        }

        return $userCredentialEntity;
    }

    public function updateUserCredential(UserCredentialModel $userCredentialModel): void
    {
        $userCredentialEntity = $this->userCredentialStorage->findOneById($userCredentialModel->getUserCredentialId());

        if ($this->userService->getUserId() !== $userCredentialEntity->getUserId()) {
            throw new UserAccessDeniedException('Access denied for user', 2022);
        }

        $key = $this->encryptService->encrypt($userCredentialModel->getApiKey());

        $userCredentialEntity->setBrokerId($userCredentialModel->getBrokerId());
        $userCredentialEntity->setApiKey($key);
        $userCredentialEntity->setActive($userCredentialModel->getIsActive() ?? 1);

        $this->userCredentialStorage->addEntity($userCredentialEntity);
    }

    public function convertModelToEntity(UserCredentialModel $credentialModel): UserCredentialEntity
    {
        $key = $this->encryptService->encrypt($credentialModel->getApiKey());

        $brokerEntity = $this->brokerService->getBrokerEntityById($credentialModel->getBrokerId());

        $userCredentialEntity = new UserCredentialEntity();
        $userCredentialEntity->setUserId($credentialModel->getUserId());
        $userCredentialEntity->setBrokerId($credentialModel->getBrokerId());
        $userCredentialEntity->setApiKey($key);
        $userCredentialEntity->setActive($credentialModel->getIsActive());
        $userCredentialEntity->setBroker($brokerEntity);

        return $userCredentialEntity;
    }

    public function convertEntityToModel(UserCredentialEntity $credentialEntity): UserCredentialModel
    {
        $key = $this->encryptService->decrypt($credentialEntity->getApiKey());

        $brokerAccounts = new UserBrokerAccountCollection();
        foreach ($credentialEntity->getUserBrokerAccount() as $brokerAccount) {
            $brokerAccounts->add($brokerAccount);
        }

        return new UserCredentialModel([
            'userCredentialId' => $credentialEntity->getId(),
            'userId' => $credentialEntity->getUserId(),
            'brokerId' => $credentialEntity->getBrokerId(),
            'broker' => $this->brokerHelper->convertBrokerEntityToModel($credentialEntity->getBroker()),
            'apiKey' => $key,
            'isActive' => $credentialEntity->getIsActive(),
            'brokerAccounts' => $brokerAccounts
        ]);
    }
}
