<?php

declare(strict_types=1);

use App\Broker\Services\BrokerHelper;
use App\Broker\Services\BrokerService;
use App\Common\HttpClient;
use App\Common\Mailer\Mailer;
use App\Services\AuthService;
use App\Services\StockBrokerFactory;
use App\User\Services\EncryptService;
use App\User\Services\GoogleAuthService;
use App\User\Services\TelegramAuthService;
use App\User\Services\TelegramWebAppAuthService;
use App\User\Services\UserAccountService;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCodeService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use App\User\Services\YandexAuthService;
use App\User\Storages\UserAccountStorage;
use App\User\Storages\UserAuthStorage;
use App\User\Storages\UserBrokerAccountStorage;
use App\User\Storages\UserCodeStorage;
use App\User\Storages\UserCredentialStorage;
use App\User\Storages\UserRolesStorage;
use App\User\Storages\UserSettingsStorage;
use App\User\Storages\UserStorage;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set('YANDEX_LOGIN' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://login.yandex.ru/'
    ]);
    return new HttpClient($client);
});

/** @var ContainerInterface $container */
$container->set('GOOGLE_LOGIN' . HttpClient::class, function () {
    $client = new Client([
        'base_uri' => 'https://people.googleapis.com/v1/'
    ]);
    return new HttpClient($client);
});

$container->set(YandexAuthService::class, function (ContainerInterface $c) {
    return new YandexAuthService(
        $c->get('YANDEX_LOGIN' . HttpClient::class)
    );
});

$container->set(GoogleAuthService::class, function (ContainerInterface $c) {
    return new GoogleAuthService(
        $c->get('GOOGLE_LOGIN' . HttpClient::class)
    );
});

$container->set(TelegramAuthService::class, function (ContainerInterface $c) {
    return new TelegramAuthService(
        getenv('TELEGRAM_BOT_TOKEN')
    );
});

$container->set(TelegramWebAppAuthService::class, function (ContainerInterface $c) {
    return new TelegramWebAppAuthService(
        getenv('TELEGRAM_BOT_TOKEN')
    );
});

$container->set(UserStorage::class, function (ContainerInterface $c) {
    return new UserStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserAuthStorage::class, function (ContainerInterface $c) {
    return new UserAuthStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserAccountStorage::class, function (ContainerInterface $c) {
    return new UserAccountStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserRolesStorage::class, function (ContainerInterface $c) {
    return new UserRolesStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserCodeStorage::class, function (ContainerInterface $c) {
    return new UserCodeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserService::class, function (ContainerInterface $c) {
    return new UserService(
        $c->get(UserStorage::class),
        $c->get(UserAuthStorage::class),
        $c->get(UserSettingsStorage::class),
        $c->get(UserRolesStorage::class),
        $c->get(UserAccountService::class),
        $c->get(YandexAuthService::class),
        $c->get(GoogleAuthService::class),
        $c->get(TelegramAuthService::class),
        $c->get(TelegramWebAppAuthService::class),
        $c->get(AuthService::class),
        $c->get('SERVICE_' . Mailer::class),
    );
});

$container->set(UserAccountService::class, function (ContainerInterface $c) {
    return new UserAccountService(
        $c->get(UserAccountStorage::class)
    );
});

$container->set(UserCredentialStorage::class, function (ContainerInterface $c) {
    return new UserCredentialStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserBrokerAccountStorage::class, function (ContainerInterface $c) {
    return new UserBrokerAccountStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserSettingsStorage::class, function (ContainerInterface $c) {
    return new UserSettingsStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserCredentialService::class, function (ContainerInterface $c) {
    return new UserCredentialService(
        $c->get(EncryptService::class),
        $c->get(UserCredentialStorage::class),
        $c->get(UserService::class),
        $c->get(BrokerService::class),
        $c->get(BrokerHelper::class)
    );
});

$container->set(UserBrokerAccountService::class, function (ContainerInterface $c) {
    return new UserBrokerAccountService(
        $c->get(StockBrokerFactory::class),
        $c->get(UserCredentialService::class),
        $c->get(UserBrokerAccountStorage::class)
    );
});

$container->set(EncryptService::class, function (ContainerInterface $c) {
    $key = getenv('ENCRYPT_KEY');
    return new EncryptService(
        $key
    );
});

$container->set(UserCodeService::class, function (ContainerInterface $c) {
    return new UserCodeService(
        $c->get(UserCodeStorage::class),
        $c->get(UserService::class)
    );
});
