<?php

declare(strict_types=1);

namespace App\User\Entities;

use App\Broker\Entities\BrokerEntity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity]
#[ORM\Table(name: 'tinkoff_invest.user_credential')]
class UserCredentialEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_credential_id', type: Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: 'UserEntity', cascade: ['persist'], fetch: 'EAGER', inversedBy: 'userCredential')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'user_id')]
    protected UserEntity $user;

    #[ORM\OneToMany(
        targetEntity: 'UserBrokerAccountEntity',
        mappedBy: 'userCredential',
        cascade: ['persist'],
        fetch: 'EAGER'
    )]
    protected PersistentCollection $userBrokerAccount;

    #[ORM\Column(name: 'user_id', type: Types::INTEGER)]
    protected int $userId;

    #[ORM\Column(name: 'broker_id', type: Types::STRING)]
    protected string $brokerId;

    #[ORM\ManyToOne(targetEntity: 'App\Broker\Entities\BrokerEntity', fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'broker_id', referencedColumnName: 'broker_id')]
    protected BrokerEntity $broker;

    #[ORM\Column(name: 'api_key', type: Types::STRING)]
    protected ?string $apiKey = null;

    #[ORM\Column(name: 'is_active', type: Types::INTEGER)]
    protected int $isActive = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    /**
     * @param string $brokerId
     */
    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @param string|null $apiKey
     */
    public function setApiKey(?string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @return PersistentCollection
     */
    public function getUserBrokerAccount(): PersistentCollection
    {
        return $this->userBrokerAccount;
    }

    /**
     * @param PersistentCollection $userBrokerAccount
     */
    public function setUserBrokerAccount(PersistentCollection $userBrokerAccount): void
    {
        $this->userBrokerAccount = $userBrokerAccount;
    }

    /**
     * @return BrokerEntity
     */
    public function getBroker(): BrokerEntity
    {
        return $this->broker;
    }

    /**
     * @param BrokerEntity $broker
     */
    public function setBroker(BrokerEntity $broker): void
    {
        $this->broker = $broker;
    }
}
