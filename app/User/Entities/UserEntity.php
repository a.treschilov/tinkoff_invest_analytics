<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'tinkoff_invest.users')]
class UserEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_id', type: Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\OneToMany(targetEntity: 'UserAuthEntity', mappedBy: 'user', fetch: 'LAZY')]
    protected ?Collection $userAuth = null;

    #[ORM\OneToMany(targetEntity: 'UserAccountEntity', mappedBy: 'user', cascade: ['persist'], fetch: 'LAZY',)]
    protected ?Collection $userAccount = null;

    #[ORM\OneToMany(targetEntity: 'UserCredentialEntity', mappedBy: 'user', fetch: 'LAZY')]
    protected Collection $userCredential;

    #[ORM\Column(name: 'login', type: Types::STRING)]
    protected ?string $login = null;

    #[ORM\Column(name: 'email', type: Types::STRING)]
    protected ?string $email = null;

    #[ORM\Column(name: 'name', type: Types::STRING)]
    protected ?string $name = null;

    #[ORM\Column(name: 'birthday', type: Types::DATETIME_MUTABLE)]
    protected \DateTime|null $birthday = null;

    #[ORM\Column(name: 'language', type: Types::STRING)]
    protected string $language;

    #[ORM\Column(name: 'is_new_user', type: Types::INTEGER)]
    protected int $isNewUser = 1;

    #[ORM\Column(name: 'telegram_id', type: Types::INTEGER)]
    protected ?int $telegramId = null;

    #[ORM\Column(name: 'is_active', type: Types::INTEGER)]
    protected int $isActive = 1;

    public function __construct()
    {
        $this->userCredential = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getUserAuth(): ?Collection
    {
        return $this->userAuth;
    }

    /**
     * @param Collection $userAuth
     */
    public function setUserAuth(Collection $userAuth): void
    {
        $this->userAuth = $userAuth;
    }

    public function getUserCredential(): Collection
    {
        return $this->userCredential;
    }

    public function setUserCredential(Collection $userCredential): void
    {
        $this->userCredential = $userCredential;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime|null $birthday
     */
    public function setBirthday(?\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return int
     */
    public function getIsNewUser(): int
    {
        return $this->isNewUser;
    }

    /**
     * @param int $isNewUser
     */
    public function setIsNewUser(int $isNewUser): void
    {
        $this->isNewUser = $isNewUser;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    public function getTelegramId(): ?int
    {
        return $this->telegramId;
    }

    public function setTelegramId(?int $telegramId): void
    {
        $this->telegramId = $telegramId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getUserAccount(): ?Collection
    {
        return $this->userAccount;
    }

    public function setUserAccount(?Collection $userAccount): void
    {
        $this->userAccount = $userAccount;
    }

    public function getIsActive(): int
    {
        return $this->isActive;
    }

    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }
}
