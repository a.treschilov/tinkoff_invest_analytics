<?php

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.user_settings')]
#[ORM\Entity]
class UserSettingsEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_settings_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    /**
     * @var UserEntity
     */
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'user_id')]
    #[ORM\ManyToOne(targetEntity: UserEntity::class, cascade: ['persist'], fetch: 'EAGER')]
    private UserEntity $user;

    #[ORM\Column(name: 'desired_pension_amount', type: 'float')]
    protected ?float $desiredPensionAmount = null;

    #[ORM\Column(name: 'desired_pension_currency', type: 'string')]
    protected ?string $desiredPensionCurrency = null;


    #[ORM\Column(name: 'retirement_age', type: 'float')]
    protected ?int $retirementAge = null;

    #[ORM\Column(name: 'expenses_amount', type: 'float')]
    protected ?float $expensesAmount = null;

    #[ORM\Column(name: 'expenses_currency', type: 'string')]
    protected ?string $expensesCurrency = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return float|null
     */
    public function getDesiredPensionAmount(): ?float
    {
        return $this->desiredPensionAmount;
    }

    /**
     * @param float|null $desiredPensionAmount
     */
    public function setDesiredPensionAmount(?float $desiredPensionAmount): void
    {
        $this->desiredPensionAmount = $desiredPensionAmount;
    }

    /**
     * @return int|null
     */
    public function getRetirementAge(): ?int
    {
        return $this->retirementAge;
    }

    /**
     * @param int|null $retirementAge
     */
    public function setRetirementAge(?int $retirementAge): void
    {
        $this->retirementAge = $retirementAge;
    }

    /**
     * @return float|null
     */
    public function getExpensesAmount(): ?float
    {
        return $this->expensesAmount;
    }

    /**
     * @param float|null $expensesAmount
     */
    public function setExpensesAmount(?float $expensesAmount): void
    {
        $this->expensesAmount = $expensesAmount;
    }

    /**
     * @return string|null
     */
    public function getExpensesCurrency(): ?string
    {
        return $this->expensesCurrency;
    }

    /**
     * @param string $expensesCurrency
     */
    public function setExpensesCurrency(string $expensesCurrency): void
    {
        $this->expensesCurrency = $expensesCurrency;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string|null
     */
    public function getDesiredPensionCurrency(): ?string
    {
        return $this->desiredPensionCurrency;
    }

    /**
     * @param string $desiredPensionCurrency
     */
    public function setDesiredPensionCurrency(string $desiredPensionCurrency): void
    {
        $this->desiredPensionCurrency = $desiredPensionCurrency;
    }
}
