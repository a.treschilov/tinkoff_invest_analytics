<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity]
#[ORM\Table(name: 'tinkoff_invest.user_code')]
class UserCodeEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_code_id', type: Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: Types::INTEGER)]
    protected int $userId;

    #[ORM\ManyToOne(targetEntity: UserEntity::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'user_id')]
    private UserEntity $user;

    #[ORM\Column(name: 'code', type: Types::STRING)]
    protected string $code;

    #[ORM\Column(name: 'expire_date', type: Types::DATETIME_MUTABLE)]
    protected \DateTime $expireDate;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getUser(): UserEntity
    {
        return $this->user;
    }

    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getExpireDate(): \DateTime
    {
        return $this->expireDate;
    }

    public function setExpireDate(\DateTime $expireDate): void
    {
        $this->expireDate = $expireDate;
    }
}
