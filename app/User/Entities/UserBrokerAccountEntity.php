<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.user_broker_account')]
#[ORM\Entity]
class UserBrokerAccountEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_broker_account_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected ?int $id = null;

    #[ORM\Column(name: 'user_credential_id', type: 'integer')]
    protected int $userCredentialId;

    /**
     * @var UserCredentialEntity
     */
    #[ORM\JoinColumn(name: 'user_credential_id', referencedColumnName: 'user_credential_id')]
    #[ORM\ManyToOne(targetEntity: \App\User\Entities\UserCredentialEntity::class)]
    protected UserCredentialEntity $userCredential;

    #[ORM\Column(name: 'external_id', type: 'string')]
    protected string $externalId;

    #[ORM\Column(name: 'name', type: 'string')]
    protected ?string $name = null;

    #[ORM\Column(name: 'type', type: 'integer')]
    protected int $type;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 0;

    #[ORM\Column(name: 'is_deleted', type: 'integer')]
    protected int $isDeleted = 0;

    #[ORM\Column(name: 'created_date', type: 'datetimetz')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'updated_date', type: 'datetimetz')]
    protected \DateTime $updatedDate;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserCredentialId(): int
    {
        return $this->userCredentialId;
    }

    /**
     * @param int $userCredentialId
     */
    public function setUserCredentialId(int $userCredentialId): void
    {
        $this->userCredentialId = $userCredentialId;
    }

    /**
     * @return UserCredentialEntity
     */
    public function getUserCredential(): UserCredentialEntity
    {
        return $this->userCredential;
    }

    /**
     * @param UserCredentialEntity $userCredential
     */
    public function setUserCredential(UserCredentialEntity $userCredential): void
    {
        $this->userCredential = $userCredential;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return int
     */
    public function getIsDeleted(): int
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted(int $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }
}
