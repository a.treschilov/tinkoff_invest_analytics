<?php

declare(strict_types=1);

namespace App\User\Entities;

use App\User\Types\AuthType;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'tinkoff_invest.user_account')]
class UserAccountEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_account_id', type: Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: 'UserEntity', cascade: ['persist'], fetch: 'EAGER', inversedBy: 'userAccount')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'user_id')]
    private UserEntity $user;

    #[ORM\Column(name: 'user_id', type: Types::INTEGER)]
    private int $userId;

    #[ORM\Column(name: 'auth_type', type: Types::STRING, enumType: AuthType::class)]
    private AuthType $authType;

    #[ORM\Column(name: 'client_id', type: Types::STRING)]
    private string $clientId;

    #[ORM\Column(name: 'is_active', type: Types::SMALLINT)]
    private ?int $isActive = 1;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUser(): UserEntity
    {
        return $this->user;
    }

    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getAuthType(): AuthType
    {
        return $this->authType;
    }

    public function setAuthType(AuthType $authType): void
    {
        $this->authType = $authType;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    public function setIsActive(?int $isActive): void
    {
        $this->isActive = $isActive;
    }
}
