<?php

declare(strict_types=1);

namespace App\User\Entities;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'tinkoff_invest.user_auth')]
class UserAuthEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_auth_id', type: Types::INTEGER)]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: 'UserEntity', cascade: ['persist'], fetch: 'EAGER', inversedBy: 'userAuth')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'user_id')]
    protected ?UserEntity $user = null;

    #[ORM\Column(name: 'user_id', type: Types::INTEGER)]
    protected int $userId;

    #[ORM\Column(name: 'is_active', type: Types::INTEGER)]
    protected int $isActive;

    #[ORM\Column(name: 'auth_date', type: Types::DATETIME_MUTABLE)]
    protected \DateTime $authDate;

    #[ORM\Column(name: 'auth_type', type: Types::STRING)]
    protected string $authType;

    #[ORM\Column(name: 'access_token', type: Types::STRING)]
    protected string $accessToken;

    #[ORM\Column(name: 'client_id', type: Types::STRING)]
    protected string $clientId;

    public function __construct()
    {
        $currentDateTime = new \DateTime();
        $this->authDate = $currentDateTime;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function isActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \Datetime
     */
    public function getAuthDate(): \Datetime
    {
        return $this->authDate;
    }

    /**
     * @param \Datetime $authDate
     */
    public function setAuthDate(\Datetime $authDate): void
    {
        $this->authDate = $authDate;
    }

    /**
     * @return string
     */
    public function getAuthType(): string
    {
        return $this->authType;
    }

    /**
     * @param string $authType
     */
    public function setAuthType(string $authType): void
    {
        $this->authType = $authType;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return ?UserEntity
     */
    public function getUser(): ?UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity|null $user
     */
    public function setUser(?UserEntity $user): void
    {
        $this->user = $user;
    }
}
