<?php

declare(strict_types=1);

namespace App\User\Types;

enum AuthType: string
{
    case YANDEX = 'YANDEX';
    case GOOGLE = 'GOOGLE';
    case TELEGRAM = 'TELEGRAM';
    case TELEGRAM_WEB = 'TELEGRAM_WEB';
}
