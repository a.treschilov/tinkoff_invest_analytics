<?php

declare(strict_types=1);

namespace App\User\Collections;

use App\User\Entities\UserBrokerAccountEntity;
use Doctrine\Common\Collections\ArrayCollection;

class UserBrokerAccountCollection extends ArrayCollection
{
    public function findByExternalId(string $externalId): ?UserBrokerAccountEntity
    {
        /** @var UserBrokerAccountEntity $account */
        foreach ($this->getIterator() as $account) {
            if ($account->getExternalId() == $externalId) {
                return $account;
            }
        }

        return null;
    }

    public function filterActiveAccounts(): UserBrokerAccountCollection
    {
        $result = new UserBrokerAccountCollection();
        /** @var UserBrokerAccountEntity $brokerAccount */
        foreach ($this->getIterator() as $brokerAccount) {
            if ($brokerAccount->getIsActive() === 1) {
                $result->add($brokerAccount);
            }
        }

        return $result;
    }
}
