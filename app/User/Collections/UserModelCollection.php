<?php

declare(strict_types=1);

namespace App\User\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class UserModelCollection extends ArrayCollection
{
}
