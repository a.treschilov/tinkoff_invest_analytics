<?php

declare(strict_types=1);

namespace App\User\Exceptions;

use App\Common\BaseException;

class UserAccessDeniedException extends BaseException
{
    protected static $CODE = 2020;
}
