<?php

declare(strict_types=1);

namespace App\Models;

use App\Common\Models\BaseModel;

class OperationFiltersModel extends BaseModel
{
    private string|null $operationType = null;
    private string|null $operationStatus = null;
    private \DateTime $dateFrom;
    private \DateTime $dateTo;

    public static $DEFAULT_DATE_FROM = '2018-01-01';
    private static $DEFAULT_DATE_TO = 'today';

    /**
     * @return string|null
     */
    public function getOperationType(): ?string
    {
        return $this->operationType;
    }

    /**
     * @param string|null $operationType
     */
    public function setOperationType(?string $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * @return string|null
     */
    public function getOperationStatus(): ?string
    {
        return $this->operationStatus;
    }

    /**
     * @param string|null $operationStatus
     */
    public function setOperationStatus(?string $operationStatus): void
    {
        $this->operationStatus = $operationStatus;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom(): \DateTime
    {
        return $this->dateFrom ?? new \DateTime(self::$DEFAULT_DATE_FROM);
    }

    /**
     * @param string|\DateTime $dateFrom
     */
    public function setDateFrom(string|\DateTime $dateFrom): void
    {
        if (is_string($dateFrom)) {
            $this->dateFrom = new \DateTime($dateFrom);
        } else {
            $this->dateFrom = $dateFrom;
        }
    }

    /**
     * @return string|\DateTime
     */
    public function getDateTo(): \DateTime
    {
        return $this->dateTo ?? new \DateTime(self::$DEFAULT_DATE_TO);
    }

    /**
     * @param \DateTime $dateTo
     */
    public function setDateTo(string|\DateTime $dateTo): void
    {
        if (is_string($dateTo)) {
            $this->dateTo = new \DateTime($dateTo);
        } else {
            $this->dateTo = $dateTo;
        }
    }
}
