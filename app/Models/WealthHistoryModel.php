<?php

declare(strict_types=1);

namespace App\Models;

use App\Common\Models\BaseModel;

class WealthHistoryModel extends BaseModel
{
    private ?int $wealthHistoryId;
    private int $userId;
    private string $currencyIso;
    private \DateTime $date;
    private float $stock;
    private float $assets;
    private float $realEstate;
    private float $deposit;
    private float $loan;
    private ?float $expenses = null;
    private ?float $wealthRate = null;

    /**
     * @return int|null
     */
    public function getWealthHistoryId(): ?int
    {
        return $this->wealthHistoryId;
    }

    /**
     * @param int|null $wealthHistoryId
     */
    public function setWealthHistoryId(?int $wealthHistoryId): void
    {
        $this->wealthHistoryId = $wealthHistoryId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getStock(): float
    {
        return $this->stock;
    }

    /**
     * @param float $stock
     */
    public function setStock(float $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return float
     */
    public function getAssets(): float
    {
        return $this->assets;
    }

    /**
     * @param float $assets
     */
    public function setAssets(float $assets): void
    {
        $this->assets = $assets;
    }

    /**
     * @return float
     */
    public function getRealEstate(): float
    {
        return $this->realEstate;
    }

    /**
     * @param float $realEstate
     */
    public function setRealEstate(float $realEstate): void
    {
        $this->realEstate = $realEstate;
    }

    /**
     * @return float
     */
    public function getLoan(): float
    {
        return $this->loan;
    }

    /**
     * @param float $loan
     */
    public function setLoan(float $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @return float|null
     */
    public function getExpenses(): ?float
    {
        return $this->expenses;
    }

    /**
     * @param float|null $expenses
     */
    public function setExpenses(?float $expenses): void
    {
        $this->expenses = $expenses;
    }

    /**
     * @return float
     */
    public function getDeposit(): float
    {
        return $this->deposit;
    }

    /**
     * @param float $deposit
     */
    public function setDeposit(float $deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return float|null
     */
    public function getWealthRate(): ?float
    {
        return $this->wealthRate;
    }

    /**
     * @param float|null $wealthRate
     */
    public function setWealthRate(?float $wealthRate): void
    {
        $this->wealthRate = $wealthRate;
    }
}
