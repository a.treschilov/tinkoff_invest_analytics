<?php

declare(strict_types=1);

namespace App\Models;

use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Common\Models\BaseModel;
use jamesRUS52\TinkoffInvest\TIPortfolioInstrument;

class InstrumentModel extends BaseModel
{
    public const TYPE_STOCK = 'Stock';
    public const TYPE_ETF = 'Etf';
    public const TYPE_CURRENCY = 'Currency';
    public const TYPE_BOND = 'Bond';
    public const TYPE_FUTURES = 'Futures';

    private BrokerPortfolioPositionModel $instrument;
    private float $amount;

    /**
     * @return BrokerPortfolioPositionModel
     */
    public function getInstrument(): BrokerPortfolioPositionModel
    {
        return $this->instrument;
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     */
    public function setInstrument(BrokerPortfolioPositionModel $instrument): void
    {
        $this->instrument = $instrument;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }
}
