<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Item\Types\ItemTypes;
use App\Item\Collections\OperationCollection;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Models\OperationModel;
use App\Item\Models\PortfolioBalanceModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Models\PortfolioModel;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use JetBrains\PhpStorm\ArrayShape;

class PortfolioService
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly OperationService $operationService,
        private readonly ItemPaymentScheduleStorage $itemPaymentScheduleStorage,
        private readonly ExchangeCurrencyService $exchangeCurrencyService,
        private readonly ItemService $itemService,
        private readonly PortfolioBuilderService $portfolioBuilderService
    ) {
    }

    public function getSavedDayPortfolio(int $userId, \DateTime $date): PortfolioModel|null
    {
        $day = clone $date;
        $day->setTime(23, 59, 59);
        $portfolio = $this->portfolioBuilderService->getLastPortfolio($userId, $day);

        if ($portfolio === null) {
            return null;
        }
        if ($portfolio->getDate()->format('Ymd') !== $day->format('Ymd')) {
            return null;
        }

        return $portfolio;
    }

    public function buildPortfolio(int $userId, ?\DateTime $date): PortfolioModel
    {
        $currency = 'RUB';
        $date = $date ?? $this->currentDateTime;

        return $this->portfolioBuilderService->buildPortfolio($userId, $date, $currency);
    }

    public function buildFullInfoPortfolio(int $userId, ?\DateTime $date): PortfolioModel
    {
        $portfolio = $this->buildPortfolio($userId, $date);
        $items = $this->itemService->getItemsByIds($portfolio->getItems()->getItemIds());
        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getItems()->getIterator() as $item) {
            $item->setItem($items->findById($item->getItemId()));
        }

        return $this->calculatePortfolioShares($portfolio);
    }

    #[ArrayShape([
        ItemTypes::MARKET => ["amount" => "PriceModel", "ratio" => "float"],
        ItemTypes::CROWD_FUNDING => ["amount" => "PriceModel", "ratio" => "float"],
        ItemTypes::LOAN => ["amount" => "PriceModel", "ratio" => "float"],
        ItemTypes::REAL_ESTATE => ["amount" => "PriceModel", "ratio" => "float"],
        ItemTypes::DEPOSIT => ["amount" => "PriceModel", "ratio" => "float"]
    ])] public function calculatePortfolioSharesByType(PortfolioModel $portfolio): array
    {
        $baseCurrency = 'RUB';
        $separatedPortfolio = $this->separatePortfolioByItemType($portfolio->getItems());

        $amounts = [
            ItemTypes::MARKET => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::MARKET],
                $baseCurrency
            ),
            ItemTypes::CROWD_FUNDING => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::CROWD_FUNDING],
                $baseCurrency
            ),
            ItemTypes::LOAN => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::LOAN],
                $baseCurrency
            ),
            ItemTypes::DEPOSIT => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::DEPOSIT],
                $baseCurrency
            ),
            ItemTypes::REAL_ESTATE => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::REAL_ESTATE],
                $baseCurrency
            ),
        ];

        $totalAmount =
            $amounts[ItemTypes::MARKET]->getAmount()
            + $amounts[ItemTypes::CROWD_FUNDING]->getAmount()
            + $amounts[ItemTypes::DEPOSIT]->getAmount()
            + $amounts[ItemTypes::REAL_ESTATE]->getAmount();
        $totalAmount = $totalAmount === 0.0 ? 1 : $totalAmount;

        return [
            ItemTypes::MARKET => [
                'amount' => new PriceModel([
                    'amount' => $amounts[ItemTypes::MARKET]->getAmount(),
                    'currency' => $baseCurrency
                ]),
                'ratio' => $amounts[ItemTypes::MARKET]->getAmount() / $totalAmount
            ],
            ItemTypes::CROWD_FUNDING => [
                'amount' => new PriceModel([
                    'amount' => $amounts[ItemTypes::CROWD_FUNDING]->getAmount(),
                    'currency' => $baseCurrency
                ]),
                'ratio' => $amounts[ItemTypes::CROWD_FUNDING]->getAmount() / $totalAmount
            ],
            ItemTypes::DEPOSIT => [
                'amount' => new PriceModel([
                    'amount' => $amounts[ItemTypes::DEPOSIT]->getAmount(),
                    'currency' => $baseCurrency
                ]),
                'ratio' => $amounts[ItemTypes::DEPOSIT]->getAmount() / $totalAmount
            ],
            ItemTypes::REAL_ESTATE => [
                'amount' => new PriceModel([
                    'amount' => $amounts[ItemTypes::REAL_ESTATE]->getAmount(),
                    'currency' => $baseCurrency
                ]),
                'ratio' => $amounts[ItemTypes::REAL_ESTATE]->getAmount() / $totalAmount
            ],
            ItemTypes::LOAN => [
                'amount' => new PriceModel([
                    'amount' => $amounts[ItemTypes::LOAN]->getAmount(),
                    'currency' => $baseCurrency
                ]),
                'ratio' => -1 * $amounts[ItemTypes::LOAN]->getAmount() / $totalAmount
            ]
        ];
    }

    public function securityOutPortfolio(int $userId, $itemId): float
    {
        $date = new \DateTime();
        $portfolio = $this->portfolioBuilderService->buildBasePortfolio($userId, $date);
        $portfolioItem = $portfolio->getItems()->getByItemId($itemId);

        $quantity = 0;
        if ($portfolioItem !== null && $portfolioItem->getQuantity() > 0) {
            $item = $this->itemService->getItemById($itemId);
            $quantity = $portfolioItem->getQuantity();
            $operationModel = new OperationModel([
                'userId' => $userId,
                'brokerId' => $item->getSource() === 'market' ? 'tinkoff2' : $item->getSource(),
                'itemId' => $itemId,
                'item' => $item,
                'operationType' => BrokerOperationTypeType::OPERATION_TYPE_SECURITY_OUT,
                'quantity' => $quantity,
                'date' => $date,
                'amount' => 0,
                'currencyIso' => 'RUB',
                'status' => BrokerOperationStatusType::OPERATION_STATE_DONE
            ]);

            $this->operationService->addUserOperation($userId, $operationModel);
        }

        return $quantity;
    }

    #[ArrayShape([
        ItemTypes::MARKET => "\App\Item\Collections\PortfolioItemCollection",
        ItemTypes::CROWD_FUNDING => "\App\Item\Collections\PortfolioItemCollection",
        ItemTypes::LOAN => "\App\Item\Collections\PortfolioItemCollection",
        ItemTypes::REAL_ESTATE => "\App\Item\Collections\PortfolioItemCollection",
        ItemTypes::DEPOSIT => "\App\Item\Collections\PortfolioItemCollection"])
    ] private function separatePortfolioByItemType(PortfolioItemCollection $portfolio): array
    {
        $result = [
            ItemTypes::MARKET => new PortfolioItemCollection(),
            ItemTypes::CROWD_FUNDING => new PortfolioItemCollection(),
            ItemTypes::LOAN => new PortfolioItemCollection(),
            ItemTypes::REAL_ESTATE => new PortfolioItemCollection(),
            ItemTypes::DEPOSIT => new PortfolioItemCollection(),
        ];
        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getIterator() as $item) {
            $result[$item->getItem()->getType()]->add($item);
        }

        return $result;
    }

    private function calculatePortfolioShares(PortfolioModel $portfolio): PortfolioModel
    {
        $baseCurrency = 'RUB';
        $separatedPortfolio = $this->separatePortfolioByItemType($portfolio->getItems());

        $amounts = [
            ItemTypes::MARKET => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::MARKET],
                $baseCurrency
            ),
            ItemTypes::CROWD_FUNDING => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::CROWD_FUNDING],
                $baseCurrency
            ),
            ItemTypes::LOAN => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::LOAN],
                $baseCurrency
            ),
            ItemTypes::DEPOSIT => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::DEPOSIT],
                $baseCurrency
            ),
            ItemTypes::REAL_ESTATE => $this->calculatePortfolioItemsAmount(
                $separatedPortfolio[ItemTypes::REAL_ESTATE],
                $baseCurrency
            ),
        ];

        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getItems()->getIterator() as $item) {
            if ($amounts[$item->getItem()->getType()]->getAmount() === 0.0) {
                $item->setItemTypeShare(0);
            } else {
                $item->setItemTypeShare(
                    $item->getBaseAmount()->getAmount()
                    / $amounts[$item->getItem()->getType()]->getAmount()
                );
            }
        }

        return $portfolio;
    }

    public function calculatePortfolioTotalAmount(PortfolioModel $portfolio, string $currency): PriceModel
    {
        $rates = [
            $currency => 1
        ];

        $amount = 0;

        /** @var PortfolioBalanceModel $balance */
        foreach ($portfolio->getBalance()->getIterator() as $balance) {
            $currencyIso = $balance->getCurrencyIso();
            if (!isset($rates[$currencyIso])) {
                $rates[$currencyIso] =
                    $this->exchangeCurrencyService->getExchangeRate(
                        $currencyIso,
                        $currency,
                        $this->currentDateTime
                    );
            }
            $amount += $rates[$currencyIso] * $balance->getAmount();
        }

        $itemsAmount = $this->calculatePortfolioItemsAmount($portfolio->getItems(), $currency);

        return new PriceModel([
            'amount' => $amount + $itemsAmount->getAmount(),
            'currency' => $currency
        ]);
    }

    public function calculatePortfolioItemsAmount(PortfolioItemCollection $items, string $currency): PriceModel
    {
        $amount = 0;
        $rates = [
            $currency => 1
        ];
        /** @var PortfolioItemModel $item */
        foreach ($items->getIterator() as $item) {
            $currencyIso = $item->getCurrencyIso();
            if (!isset($rates[$currencyIso])) {
                $rates[$currencyIso] =
                    $this->exchangeCurrencyService->getExchangeRate(
                        $currencyIso,
                        $currency,
                        $this->currentDateTime
                    );
            }

            $amount += $rates[$currencyIso] * $item->getAmount();
        }

        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    public function calculatePortfolioItemsQuantity(PortfolioItemCollection $items): float
    {
        $quantity = 0;
        /** @var PortfolioItemModel $item */
        foreach ($items->getIterator() as $item) {
            $quantity += $item->getQuantity();
        }

        return $quantity;
    }

    public function buildPortfolioByOperations(
        OperationCollection $operations
    ): PortfolioModel {
        $portfolioOperations = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $portfolioOperations[] = $this->portfolioBuilderService->convertOperationToPortfolio($operation);
        }

        return $this->portfolioBuilderService->mergePortfolios($portfolioOperations);
    }

    public function buildPricedPortfolioByOperations(
        OperationCollection $operations,
        \DateTime $date,
        string $currency
    ): PortfolioModel {
        /** @var OperationModel|null $operation */
        $operation = $operations->first();
        if ($operation === false) {
            return new PortfolioModel([
                'balance' => new PortfolioBalanceCollection(),
                'items' => new PortfolioItemCollection()
            ]);
        }
        $userId = $operation->getUserId();

        $portfolioModel = $this->buildPortfolioByOperations($operations);

        $portfolioModel->setItems(
            $this->portfolioBuilderService->calculatePortfolioItemPrices(
                $userId,
                $portfolioModel,
                $date
            )
        );
        $portfolioModel->setItems(
            $this->portfolioBuilderService->fillPortfolioBasePrice(
                $portfolioModel->getItems(),
                $currency,
                $date
            )
        );

        return $portfolioModel;
    }

    public function calculateForecastIncome(int $userId, \DateTime $startDate, \DateTime $endDate): float
    {
        $currency = 'RUB';
        $portfolio = $this->portfolioBuilderService->buildBasePortfolio($userId, $this->currentDateTime);

        return $this->calculateItemsForecastIncome(
            $portfolio->getItems(),
            $currency,
            $startDate,
            $endDate
        );
    }

    public function calculateItemsForecastIncome(
        PortfolioItemCollection $items,
        string $currency,
        \DateTime $startDate,
        \DateTime $endDate
    ): float {
        $income = 0;
        $rates = [
            $currency => 1
        ];

        /** @var PortfolioItemModel $item */
        foreach ($items->getIterator() as $item) {
            if ($item->getQuantity() !== 0) {
                $payments = $this->itemPaymentScheduleStorage
                    ->findActiveByItemIdAndDateInterval($item->getItemId(), $startDate, $endDate);
                foreach ($payments as $payment) {
                    $currencyIso = $payment->getCurrency()->getIso();
                    if (!isset($rates[$currencyIso])) {
                        $rates[$currencyIso] =
                            $this->exchangeCurrencyService->getExchangeRate(
                                $currencyIso,
                                $currency,
                                min($endDate, $this->currentDateTime)
                            );
                    }

                    $income += $payment->getInterestAmount() * $rates[$currencyIso] * $item->getQuantity();
                }
            }
        }

        return $income;
    }
}
