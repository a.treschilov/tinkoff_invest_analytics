<?php

declare(strict_types=1);

namespace App\Item\Services\Sources;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Assets\Models\AssetModel;
use App\Broker\Types\BrokerOperationStatus;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use App\Item\Services\Sources\Interfaces\Source;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemType;
use App\Item\Types\OperationType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

class Jetlend implements Source
{
    private const string PAYOUT_TYPE = 'annuitant';
    private const string ASSET_SHEET_NAME = 'Sheet1';
    private const string OPERATION_SHEET_NAME = 'Sheet1';
    public const DataSourceType PLATFORM = DataSourceType::JETLEND;
    private const ItemType ASSET_TYPE = ItemType::CROWD_FUNDING;

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return ItemCollection
     */
    public function parseItemsFromFile(int $userId, array $files): ItemCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $items = new ItemCollection([]);
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $items = $this->getAssetsFromSpreadSheet($spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $items;
    }

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return OperationCollection
     */
    public function parseOperationsFromFile(int $userId, array $files): OperationCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $operations = new OperationCollection();
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $operations = $this->getOperationsFromSpreadSheet($userId, $spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $operations;
    }

    private function getAssetsFromSpreadSheet(Spreadsheet $spreadSheet): ItemCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::ASSET_SHEET_NAME);
        if ($sheet === null || $sheet->getCell([3,1])->getValue() !== 'Заемщик') {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $items = new ItemCollection();

        foreach ($sheet->getRowIterator() as $row) {
            foreach ($row->getCellIterator('A', 'A') as $headerCell) {
                if (is_int($headerCell->getValue())) {
                    $item = new ItemModel([
                        'type' => self::ASSET_TYPE->value,
                        'source' => self::PLATFORM->value
                    ]);
                    $assetModel = new AssetModel([
                        'platform' => self::PLATFORM->value,
                        'type' => self::ASSET_TYPE->value,
                        'durationPeriod' => 'day',
                        'payoutFrequencyPeriod' => 'month',
                        'payoutFrequency' => 1,
                        'payoutType' => self::PAYOUT_TYPE,
                        'interestAmount' => 0,
                        'nominalAmount' => 1,
                        'currencyIso' => 'RUB'
                    ]);

                    $name = '';
                    $nextPaymentDate = new \DateTime();
                    foreach ($row->getCellIterator('A') as $cell) {
                        switch ($cell->getColumn()) {
                            case 'B':
                                $externalId = $cell->getValue();
                                break;
                            case 'C':
                                $name = $cell->getValue();
                                break;
                            case 'G':
                                $assetModel->setInterestPercent((float)$cell->getValue());
                                break;
                            case 'D':
                                $dealDate = new \DateTime($cell->getFormattedValue());
                                break;
                            case 'H':
                                $assetModel->setDuration((int)$cell->getValue());
                                break;
                            case 'K':
                                $nextPaymentDate = (new \DateTime($cell->getFormattedValue()));
                                break;
                            case 'L':
                                $assetModel->setDebtRest((float)$cell->getValue());
                                break;
                        }
                    }
                    $assetModel->setName($name);
                    $assetModel->setExternalId($name);
                    $assetModel->setDealDate($dealDate->setDate(
                        (int)$dealDate->format('Y'),
                        (int)$dealDate->format('m'),
                        (int)$nextPaymentDate->format('d')
                    ));

                    $item->setExternalId($assetModel->getExternalId());
                    $item->setName($assetModel->getName());
                    $item->setAsset($assetModel);

                    $items->add($item);
                }
            }
        }

        return $items;
    }

    private function getOperationsFromSpreadSheet(int $userId, Spreadsheet $spreadSheet): OperationCollection
    {
        $operations = new OperationCollection();
        $sheet = $spreadSheet->getSheetByName(self::OPERATION_SHEET_NAME);
        if ($sheet === null || $sheet->getCell([3,1])->getValue() !== 'Тип операции') {
            throw new ThirdPartyUploadException('Incorrect file format', 3001);
        }

        $date = null;
        $dayOrder = 0;
        foreach ($sheet->getRowIterator(2) as $row) {
            $cell = $row->getCellIterator('A')->current();

            if ((int)$cell->getValue() > 0) {
                $rowOperations = new OperationCollection();

                $operation = new OperationModel([
                    'brokerId' => self::PLATFORM->value,
                    'status' => BrokerOperationStatus::DONE->value,
                    'currencyIso' => 'RUB',
                    'userId' => $userId,
                ]);
                $credit = 0;
                $debt = 0;
                $income = 0;
                $taxes = 0;
                $input = 0;
                $bonus = 0;
                foreach ($row->getCellIterator('B') as $cell) {
                    switch ($cell->getColumn()) {
                        case 'B':
                            $operation->setDate(new \DateTime(
                                $cell->getFormattedValue(),
                                new \DateTimeZone('Europe/Moscow')
                            ));
                            $operation->getDate()->setTimezone(new \DateTimeZone('UTC'));
                            break;
                        case 'C':
                            $operation->setOperationType($this->convertOperationType($cell->getValue()));
                            break;
                        case 'D':
                            $operation->setItemExternalId($cell->getValue());
                            break;
                        case 'H':
                            $input = (float)$cell->getValue();
                            break;
                        case 'I':
                            $credit = (float)$cell->getValue();
                            break;
                        case 'J':
                            $debt = (float)$cell->getValue();
                            break;
                        case 'M':
                            $income = (float)$cell->getValue();
                            break;
                        case 'N':
                            $taxes = (float)$cell->getValue();
                            break;
                        case 'O':
                            $bonus = (float)$cell->getValue();
                    }
                }

                $dayOrder = $operation->getDate()->format("YYmd") === $date ? $dayOrder + 1 : 0;
                $date = $operation->getDate()->format("YYmd");

                switch ($operation->getOperationType()) {
                    case OperationType::REPAYMENT->value:
                        if ($income != 0) {
                            $incomeOperation = clone $operation;
                            $incomeOperation->setAmount($income);
                            $incomeOperation->setOperationType(OperationType::DIVIDEND->value);
                            $rowOperations->add($incomeOperation);
                        }

                        if ($taxes != 0) {
                            $taxOperation = clone $operation;
                            $taxOperation->setOperationType(OperationType::TAX->value);
                            $taxOperation->setAmount($taxes);
                            $rowOperations->add($taxOperation);
                        }

                        if ($debt != 0) {
                            $debtOperation = clone $operation;
                            $debtOperation->setOperationType(OperationType::REPAYMENT->value);
                            $debtOperation->setAmount(-1 * $debt);
                            $rowOperations->add($debtOperation);
                        }
                        break;
                    case OperationType::BUY->value:
                        $operation->setAmount(-1 * $credit);
                        $operation->setQuantity($credit);
                        $rowOperations->add($operation);
                        break;
                    case OperationType::INPUT->value:
                        $input = $input > 0 ? $input : $bonus;
                        $operation->setAmount($input);
                        $rowOperations->add($operation);
                        break;
                    case OperationType::DEFAULT->value:
                        $operation->setAmount(-1 * $income);
                        $rowOperations->add($operation);
                        break;
                    case OperationType::DEFAULT_PAYOUTS->value:
                        if ($credit != 0) {
                            $feeOperation = clone $operation;
                            $feeOperation->setOperationType(OperationType::FEE->value);
                            $feeOperation->setAmount($credit);
                            $rowOperations->add($feeOperation);
                        }

                        if ($debt != 0) {
                            $incomeOperation = clone $operation;
                            $incomeOperation->setOperationType(
                                OperationType::DEFAULT_PAYOUTS->value
                            );
                            $incomeOperation->setAmount(-1 * $debt);
                            $rowOperations->add($incomeOperation);
                        }
                        break;
                    default:
                        throw new ThirdPartyUploadException('Unspecified operation type', 3002);
                }

                /** @var OperationModel $operation */
                foreach ($rowOperations->getIterator() as $operation) {
                    $operation->setExternalId(
                        md5(
                            $operation->getDate()->getTimestamp() .
                            $operation->getAmount() .
                            $operation->getOperationType() .
                            $operation->getItemExternalId() .
                            $dayOrder
                        )
                    );
                    $operations->add($operation);
                }
            }
        }
        return $operations;
    }

    private function convertOperationType(string $type): string
    {
        return match ($type) {
            'Пополнение счета', 'Бонус от платформы' => OperationType::INPUT->value,
            'Выдача займа', 'Покупка займа на вторичном рынке' => OperationType::BUY->value,
            'Платеж по займу' => OperationType::REPAYMENT->value,
            'Дефолт' => OperationType::DEFAULT->value,
            'Зачисление по судебному взысканию' => OperationType::DEFAULT_PAYOUTS->value,
            default => OperationType::UNSPECIFIED->value,
        };
    }
}
