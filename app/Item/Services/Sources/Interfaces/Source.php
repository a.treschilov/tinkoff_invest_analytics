<?php

declare(strict_types=1);

namespace App\Item\Services\Sources\Interfaces;

use App\Item\Collections\ItemCollection;
use App\Item\Collections\OperationCollection;
use Slim\Psr7\UploadedFile;

interface Source
{
    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return ItemCollection
     */
    public function parseItemsFromFile(int $userId, array $files): ItemCollection;

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return OperationCollection
     */
    public function parseOperationsFromFile(int $userId, array $files): OperationCollection;
}
