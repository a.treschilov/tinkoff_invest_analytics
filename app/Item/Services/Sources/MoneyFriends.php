<?php

declare(strict_types=1);

namespace App\Item\Services\Sources;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Assets\Models\AssetModel;
use App\Broker\Types\BrokerOperationStatus;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use App\Item\Services\Sources\Interfaces\Source;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemType;
use App\Item\Types\OperationType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

class MoneyFriends implements Source
{
    private const string PAYOUT_TYPE = 'annuitant';
    private const string SHEET_NAME = 'Отчет по сделкам';
    public const DataSourceType PLATFORM = DataSourceType::MONEY_FRIENDS;
    private const ItemType ASSET_TYPE = ItemType::CROWD_FUNDING;

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return ItemCollection
     */
    public function parseItemsFromFile(int $userId, array $files): ItemCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $items = new ItemCollection([]);
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $items = $this->getAssetsFromSpreadSheet($spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $items;
    }

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return OperationCollection
     */
    public function parseOperationsFromFile(int $userId, array $files): OperationCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $operations = new OperationCollection();
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $operations = $this->getOperationsFromSpreadSheet($userId, $spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $operations;
    }

    private function getAssetsFromSpreadSheet(Spreadsheet $spreadSheet): ItemCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $items = new ItemCollection();

        $isStartAssets = false;
        $isEndAssets = false;
        foreach ($sheet->getRowIterator() as $row) {
            if (!$isStartAssets) {
                foreach ($row->getCellIterator('C', 'C') as $cell) {
                    if ($cell->getValue() === "Сводные данные по активным займам\n") {
                        $isStartAssets = true;
                    }
                }
            }

            if ($isStartAssets && !$isEndAssets) {
                foreach ($row->getCellIterator('B', 'B') as $headerCell) {
                    if (is_int($headerCell->getValue())) {
                        $item = new ItemModel([
                            'type' => self::ASSET_TYPE->value,
                            'source' => self::PLATFORM->value,
                        ]);
                        $assetModel = new AssetModel([
                            'platform' => self::PLATFORM->value,
                            'type' => self::ASSET_TYPE->value,
                            'durationPeriod' => 'day',
                            'payoutFrequencyPeriod' => 'month',
                            'payoutFrequency' => 1,
                            'payoutType' => self::PAYOUT_TYPE,
                            'interestAmount' => 0,
                            'nominalAmount' => 1,
                            'currencyIso' => 'RUB'
                        ]);

                        $name = '';
                        foreach ($row->getCellIterator('E') as $cell) {
                            switch ($cell->getColumn()) {
                                case 'G':
                                    $assetModel->setExternalId((string)$cell->getValue());
                                    break;
                                case 'O':
                                    $assetModel->setDealDate(new \DateTime($cell->getFormattedValue()));
                                    break;
                                case 'V':
                                    $assetModel->setDuration($cell->getValue());
                                    break;
                                case 'AG':
                                    $name = $cell->getValue();
                                    break;
                            }
                        }
                        $assetModel->setName($assetModel->getExternalId() . ' - ' . $name);
                        $item->setName($assetModel->getName());
                        $item->setExternalId($assetModel->getExternalId());
                        $item->setAsset($assetModel);

                        $items->add($item);
                    }
                }
            }

            if (!$isEndAssets) {
                foreach ($row->getCellIterator('B', 'B') as $cell) {
                    if ($cell->getValue() === "Комиссия платформе\n") {
                        $isEndAssets = true;
                    }
                }
            }
        }

        return $items;
    }

    public function getOperationsFromSpreadSheet(int $userId, Spreadsheet $spreadSheet): OperationCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }

        $operations = new OperationCollection();
        $isStartOperations = false;
        $isEndOperations = null;
        $dayOrder = 0;
        $date = null;

        foreach ($sheet->getRowIterator() as $row) {
            if (!$isStartOperations) {
                foreach ($row->getCellIterator('C', 'C') as $cell) {
                    if ($cell->getValue() === "Сводные данные по движению денежных средств по счету инвестора\n") {
                        $isStartOperations = true;
                    }
                }
            }

            if ($isEndOperations) {
                break;
            }
            if ($isStartOperations) {
                foreach ($row->getCellIterator('D', 'D') as $headerCell) {
                    if (is_int($headerCell->getValue())) {
                        $isEndOperations = false;
                        $operation = new OperationModel([
                            'brokerId' => self::PLATFORM->value,
                            'status' => BrokerOperationStatus::DONE->value,
                            'currencyIso' => 'RUB',
                            'userId' => $userId,
                        ]);

                        $comment = '';
                        foreach ($row->getCellIterator('J') as $cell) {
                            switch ($cell->getColumn()) {
                                case 'J':
                                    $operation->setDate(new \DateTime(
                                        $cell->getValue(),
                                        new \DateTimeZone('Europe/Moscow')
                                    ));
                                    $operation->getDate()->setTimezone(new \DateTimeZone('UTC'));
                                    break;
                                case 'Q':
                                    $operation->setAmount((float)$cell->getValue());
                                    break;
                                case 'U':
                                    $comment = $cell->getValue();
                                    break;
                                case 'AT':
                                    $operation->setOperationType($this->convertOperationType($cell->getValue())->value);
                                    break;
                            }
                        }

                        preg_match_all('/№([0-9]+)/', $comment, $matches);
                        $externalId = $matches[1][0] ?? null;
                        $operation->setItemExternalId($externalId);

                        $dayOrder = $operation->getDate()->format("YYmd") === $date ? $dayOrder + 1 : 0;
                        $date = $operation->getDate()->format("YYmd");

                        if (in_array($operation->getOperationType(), [OperationType::BUY, OperationType::HOLD])) {
                            $operation->setAmount(-1 * $operation->getAmount());
                        }

                        if (in_array($operation->getOperationType(), [OperationType::BUY, OperationType::DEFAULT])) {
                            $operation->setQuantity(-1 * $operation->getAmount());
                        }

                        $operation->setExternalId(md5(
                            $externalId .
                            $operation->getDate()->getTimestamp() .
                            $operation->getAmount() .
                            $operation->getOperationType() .
                            $dayOrder
                        ));

                        $operations->add($operation);
                    } elseif ($isEndOperations === false) {
                        $isEndOperations = true;
                    }
                }
            }
        }
        return $operations;
    }

    private function convertOperationType(string $type): OperationType
    {
        return match ($type) {
            'Пополнение средств' => OperationType::INPUT,
            'Перевод в резерв' => OperationType::HOLD,
            'Выдача займа' => OperationType::BUY,
            'Возврат долга' => OperationType::REPAYMENT,
            'Возврат процентов по займу', 'Возврат пени' => OperationType::DIVIDEND,
            'Возврат средств' => OperationType::UN_HOLD,
            'Списание по цессии' => OperationType::DEFAULT,
            'Возврат средств по цессии' => OperationType::DEFAULT_PAYOUTS,
            default => OperationType::UNSPECIFIED,
        };
    }
}
