<?php

declare(strict_types=1);

namespace App\Item\Services\Sources;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Assets\Models\AssetModel;
use App\Broker\Types\BrokerOperationStatus;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationModel;
use App\Item\Services\Sources\Interfaces\Source;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemType;
use App\Item\Types\OperationType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Slim\Psr7\UploadedFile;

class Potok implements Source
{
    private const string PAYOUT_TYPE = 'differentiatedConstantBase';
    private const string ASSET_SHEET_NAME = 'Список займов (Портфель)';
    private const string OPERATION_SHEET_NAME = 'История операций';
    public const DataSourceType PLATFORM = DataSourceType::POTOK;
    public const ItemType ITEM_TYPE = ItemType::CROWD_FUNDING;

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return ItemCollection
     */
    public function parseItemsFromFile(int $userId, array $files): ItemCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $items = new ItemCollection([]);
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $items = $this->getAssetsFromSpreadSheet($spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $items;
    }

    /**
     * @param int $userId
     * @param UploadedFile[] $files
     * @return OperationCollection
     */
    public function parseOperationsFromFile(int $userId, array $files): OperationCollection
    {
        $type = 'Xlsx';
        $reader = IOFactory::createReader($type);

        $operations = new OperationCollection();
        foreach ($files as $file) {
            $spreadSheet = $reader->load($file->getFilePath());
            try {
                $operations = $this->getOperationsFromSpreadSheet($userId, $spreadSheet);
            } catch (ThirdPartyUploadException $e) {
            }
        }

        return $operations;
    }

    private function getAssetsFromSpreadSheet(Spreadsheet $spreadSheet): ItemCollection
    {
        $itemCollection = new ItemCollection();

        $sheet = $spreadSheet->getSheetByName(self::ASSET_SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3000);
        }
        foreach ($sheet->getRowIterator(3) as $row) {
            $assetModel = new AssetModel([
                'platform' => self::PLATFORM->value,
                'type' => self::ITEM_TYPE->value,
                'durationPeriod' => 'day',
                'payoutFrequencyPeriod' => 'day',
                'payoutType' => self::PAYOUT_TYPE,
                'interestAmount' => 0,
                'nominalAmount' => 1,
                'currencyIso' => 'RUB'
            ]);

            $itemModel = new ItemModel([
                'type' => self::ITEM_TYPE->value,
                'source' => DataSourceType::POTOK->value,
            ]);

            $name = '';
            foreach ($row->getCellIterator('A') as $cell) {
                switch ($cell->getColumn()) {
                    case 'B':
                        $name = $cell->getValue();
                        break;
                    case 'D':
                        $assetModel->setExternalId($cell->getValue());
                        break;
                    case 'G':
                        $assetModel->setInterestPercent($cell->getValue() / 100);
                        break;
                    case 'H':
                        $assetModel->setPayoutFrequency($cell->getValue());
                        break;
                    case 'I':
                        $assetModel->setDuration($cell->getValue());
                        break;
                    case 'J':
                        $timestamp = (int)(((float)$cell->getValue() - 25569) * 86400);
                        $timestamp -= 3 * 60 * 60; //Convert to Moscow timezone
                        $date = new \DateTime();
                        $date->setTimestamp($timestamp);
                        $assetModel->setDealDate(clone $date);
                        break;
                }
            }

            $assetModel->setName($assetModel->getExternalId() . ' - ' . $name);
            $itemModel->setName($assetModel->getName());
            $itemModel->setExternalId($assetModel->getExternalId());
            $itemModel->setAsset($assetModel);

            $itemCollection->add($itemModel);
        }
        return $itemCollection;
    }

    /**
     * @param int $userId
     * @param Spreadsheet $spreadSheet
     * @return OperationCollection
     * @throws ThirdPartyUploadException
     */
    private function getOperationsFromSpreadSheet(int $userId, Spreadsheet $spreadSheet): OperationCollection
    {
        $sheet = $spreadSheet->getSheetByName(self::OPERATION_SHEET_NAME);
        if ($sheet === null) {
            throw new ThirdPartyUploadException('Incorrect file format', 3001);
        }

        $dayOrder = 0;
        $date = '00000000';
        $operations = new OperationCollection();

        foreach ($sheet->getRowIterator(7) as $row) {
            $cell = $row->getCellIterator('A')->current();
            if ($cell->getValue() === null) {
                break;
            }

            $operation = new OperationModel([
                'brokerId' => self::PLATFORM->value,
                'status' => BrokerOperationStatus::DONE->value,
                'currencyIso' => 'RUB',
                'userId' => $userId,
            ]);

            $comment = '';
            foreach ($row->getCellIterator('A') as $cell) {
                switch ($cell->getColumn()) {
                    case 'A':
                        $timestamp = Date::excelToTimestamp($cell->getValue());
                        $operation->setDate((new \DateTime())->setTimestamp($timestamp));
                        break;
                    case 'D':
                        $operation->setOperationType(
                            $this->convertOperationType($cell->getValue())->value
                        );
                        break;
                    case 'E':
                        $operation->setAmount((float)$cell->getValue());
                        break;
                    case 'F':
                        if ($cell->getValue() !== 0) {
                            $operation->setAmount(-1 * (float)$cell->getValue());
                        }
                        break;
                    case 'H':
                        $comment = (string)$cell->getValue();
                        $comments = explode('-', $comment);
                        $operation->setItemExternalId($comments[0]);
                        break;
                }
            }

            $dayOrder = $operation->getDate()->format("YYmd") === $date ? $dayOrder + 1 : 0;
            $date = $operation->getDate()->format("YYmd");

            $operation->setExternalId(
                md5(
                    $operation->getDate()->getTimestamp() .
                    $operation->getAmount() .
                    $operation->getOperationType() .
                    $comment .
                    $dayOrder
                )
            );
            if ($operation->getOperationType() === OperationType::BUY->value) {
                $operation->setQuantity(-1 * $operation->getAmount());
            }

            $operations->add($operation);
        }

        return $operations;
    }

    private function convertOperationType(string $type): OperationType
    {
        return match ($type) {
            'Пополнение л/с', 'Выравнивающий платеж' => OperationType::INPUT,
            'Выдача займа', 'Расход по покупке прав требований' => OperationType::BUY,
            'Получение дохода (проценты, пени)' => OperationType::DIVIDEND,
            'Возврат основного долга' => OperationType::REPAYMENT,
            'Расход по цессии' => OperationType::FEE,
            'Возврат основного долга по цессии' => OperationType::DEFAULT_PAYOUTS,
            default => OperationType::UNSPECIFIED
        };
    }
}
