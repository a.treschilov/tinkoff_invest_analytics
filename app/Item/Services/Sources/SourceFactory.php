<?php

declare(strict_types=1);

namespace App\Item\Services\Sources;

use App\Item\Services\Sources\Interfaces\Source;
use App\Item\Types\DataSourceType;

class SourceFactory
{
    public function getSource(DataSourceType $source): Source
    {
        return match ($source) {
            DataSourceType::POTOK => new Potok(),
            DataSourceType::MONEY_FRIENDS => new MoneyFriends(),
            DataSourceType::JETLEND => new Jetlend()
        };
    }
}
