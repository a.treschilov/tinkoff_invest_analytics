<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Services\AssetService;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Models\ItemFilterModel;
use App\Item\Models\ItemModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Storages\ItemStorage;
use App\Item\Types\ItemType;
use App\User\Exceptions\UserAccessDeniedException;

class ItemService
{
    public function __construct(
        private readonly ItemStorage $itemStorage,
        private readonly ItemConverterService $itemConverterService,
        private readonly AssetService $assetService
    ) {
    }

    public function getPortfolioItemsByType(
        PortfolioItemCollection $portfolioItemCollection,
        string $type
    ): ItemCollection {
        $itemCollection = new ItemCollection();
        /** @var PortfolioItemModel $item */
        foreach ($portfolioItemCollection->getIterator() as $portfolioItem) {
            $item = $this->getItemById($portfolioItem->getItemId());

            if ($item->getType() === $type) {
                $itemCollection->add($item);
            }
        }

        return $itemCollection;
    }

    public function getItemsByIds(array $ids): ItemCollection
    {
        $items = new ItemCollection();
        foreach ($ids as $id) {
            $item = $this->getItemById($id);
            $items->add($item);
        }

        return $items;
    }

    public function getItemById(int $itemId): ItemModel
    {
        $itemEntity = $this->itemStorage->findById($itemId);
        return $this->itemConverterService->convertItemEntityToModel($itemEntity);
    }

    public function getItemByFilter(ItemFilterModel $filter): ItemCollection
    {
        $collection = new ItemCollection();
        $items = $this->itemStorage->findItem($filter);
        foreach ($items as $item) {
            $collection->add($this->itemConverterService->convertItemEntityToModel($item));
        }

        return $collection;
    }

    public function getUserItemById(int $itemId, int $userId): ItemModel
    {
        $item = $this->getItemById($itemId);

        if (!$this->isAccessibleItem($userId, $item)) {
            throw new UserAccessDeniedException('Access denied for user', 2028);
        }

        return $item;
    }

    public function deleteUserItem(int $userId, int $itemId): void
    {
        $item = $this->getItemById($itemId);

        if (!$this->isDeletableItem($item)) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }

        if (!$this->isAccessibleItem($userId, $item)) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }

        $this->deleteItems([$itemId]);
    }

    /**
     * @param int[] $ids
     * @return int[] $ids
     */
    public function deleteItems(array $ids): array
    {
        $result = [];
        $timeStamp = new \DateTime()->getTimestamp();

        $items = $this->itemStorage->findByIds($ids);
        foreach ($items as $item) {
            $externalId = $item->getExternalId() !== null ? $item->getExternalId() : '';
            $item->setExternalId($externalId . '_deleted_' . $timeStamp);
            $item->setIsDeleted(1);

            $result[] = $item->getId();
        }

        $this->itemStorage->addEntityArray($items);

        return $result;
    }

    public function updateItemPrice(int $userId, int $itemId, \DateTime $date): void
    {
        $item = $this->getItemById($itemId);

        if (!$item->isDeleted()) {
            switch ($item->getType()) {
                case ItemType::CROWD_FUNDING->value:
                    $this->assetService->updateItemPrice($userId, $item, $date);
                    break;
            }
        }
    }

    public function getItemByExternalId(string $externalId): ?ItemModel
    {
        $item = $this->itemStorage->findActiveByExternalId($externalId);

        if ($item === null) {
            return null;
        }
        return $this->itemConverterService->convertItemEntityToModel($item);
    }

    public function setOutdatedItem(int $itemId, bool $isOutDated = false): void
    {
        $entity = $this->itemStorage->findById($itemId);
        $entity->setIsOutdated((int)$isOutDated);
        $this->itemStorage->addEntity($entity);
    }

    public function isAccessibleItem(int $userId, ItemModel $item): bool
    {
        if ($item->getType() === ItemType::REAL_ESTATE->value && $item->getRealEstate()->getUserId() !== $userId) {
            return false;
        }

        if ($item->getType() === ItemType::LOAN->value && $item->getLoan()->getUserId() !== $userId) {
            return false;
        }

        if ($item->getType() === ItemType::DEPOSIT->value && $item->getDeposit()->getUserId() !== $userId) {
            return false;
        }

        return true;
    }

    private function isDeletableItem(ItemModel $item): bool
    {
        if (
            !in_array(
                $item->getType(),
                [ItemType::LOAN->value, ItemType::DEPOSIT->value, ItemType::REAL_ESTATE->value]
            )
        ) {
            return false;
        }
        if ($item->isDeleted() === true) {
            return false;
        }

        return true;
    }
}
