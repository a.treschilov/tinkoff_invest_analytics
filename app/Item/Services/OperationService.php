<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Models\ImportStatisticModel as AssetImportStatisticModel;
use App\Broker\Services\BrokerService;
use App\Broker\Types\BrokerOperationStatus;
use App\Collections\OperationAggregateCollection;
use App\Common\Amqp\AmqpClient;
use App\Entities\OperationStatusEntity;
use App\Entities\OperationTypeEntity;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\OperationCollection;
use App\Item\Collections\OperationStatusCollection;
use App\Item\Collections\OperationTypeCollection;
use App\Item\Entities\ItemOperationEntity;
use App\Item\Models\ItemFilterModel;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Models\OperationStatusModel;
use App\Item\Models\OperationTypeModel;
use App\Item\Services\Sources\SourceFactory;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Item\Types\DataSourceType;
use App\Item\Types\OperationType;
use App\Models\ImportStatisticModel;
use App\Models\OperationAggregateModel;
use App\Models\OperationFiltersModel as OperationFilter;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use App\Services\MarketOperationsService;
use App\User\Exceptions\UserAccessDeniedException;
use Ramsey\Uuid\Uuid;
use Slim\Psr7\UploadedFile;

class OperationService
{
    public function __construct(
        private readonly ItemOperationStorage $itemOperationStorage,
        private readonly ItemStorage $itemStorage,
        private readonly CurrencyService $currencyService,
        private readonly OperationTypeStorage $operationTypeStorage,
        private readonly OperationStatusStorage $operationStatusStorage,
        private readonly ExchangeCurrencyService $exchangeCurrencyService,
        private readonly ItemConverterService $itemConverterService,
        private readonly AmqpClient $amqpClient,
        private readonly MarketOperationsService $marketOperationService,
        private readonly BrokerService $brokerService,
        private readonly SourceFactory $sourceFactory,
    ) {
    }

    public function calculateIncome(int $userId, \DateTime $startDate, \DateTime $endDate): float
    {
        $currency = 'RUB';

        $filter = new OperationFiltersModel([
            'dateFrom' => $startDate,
            'dateTo' => $endDate,
            'operationStatus' => [BrokerOperationStatus::DONE->value],
            'operationType' => [
                OperationType::DIVIDEND->value,
                OperationType::DIVIDEND_CARD->value
            ]
        ]);
        $operations = $this->getFilteredUserOperation($userId, $filter);
        return $this->calculateOperationSummary($operations, $currency)->getAmount();
    }

    public function addOrUpdateOperationBatch(OperationCollection $operations): AssetImportStatisticModel
    {
        $statistic = [
            'skipped' => 0,
            'updated' => 0,
            'added' => 0
        ];

        $operationsForUpdate = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationType = $this->operationTypeStorage->findByExternalId($operation->getOperationType());
            $itemOperation = $this->itemOperationStorage->findByExternalIdAndBrokerAndType(
                $operation->getExternalId(),
                $operation->getBrokerId(),
                $operationType->getId()
            );

            if (null === $itemOperation) {
                $statistic['added']++;
                $operationEntity = $this->convertOperationModelToEntity($operation);
                $operationsForUpdate[] = clone $operationEntity;
            } elseif ($itemOperation->getStatus() !== $operation->getStatus()) {
                $statistic['updated']++;
                $itemOperation->setStatus($operation->getStatus());
                $itemOperation->setIsAnalyticProceed($operation->getIsAnalyticProceed());
                $operationsForUpdate[] = clone $itemOperation;
            } else {
                $statistic['skipped']++;
            }
        }
        $operationsForUpdate = $this->itemOperationStorage->addEntityArray($operationsForUpdate);

        foreach ($operationsForUpdate as $operation) {
            $this->publishOperationEvent($operation->getId());
        }

        return new AssetImportStatisticModel($statistic);
    }

    public function getOperationTypeList(): OperationTypeCollection
    {
        $operationTypeCollection = new OperationTypeCollection();

        $operationTypes = $this->operationTypeStorage->findAll();
        foreach ($operationTypes as $type) {
            $operationTypeCollection->add($this->convertOperationTypeEntityToModel($type));
        }

        return $operationTypeCollection;
    }

    public function getOperationStatusList(): OperationStatusCollection
    {
        $operationStatusList = new OperationStatusCollection();

        $operationStatues = $this->operationStatusStorage->findAll();
        foreach ($operationStatues as $status) {
            $operationStatusList->add($this->convertOperationStatusEntityToModel($status));
        }

        return $operationStatusList;
    }

    public function getFilteredUserOperation(?int $userId, OperationFiltersModel $filter): OperationCollection
    {
        $operations = new OperationCollection();
        $types = [];
        if ($filter->getOperationType() !== null) {
            foreach ($filter->getOperationType() as $type) {
                $types[] = $this->operationTypeStorage->findByExternalId($type)?->getId();
            }
        }
        $statuses = $filter->getOperationStatus() === null ? [] : $filter->getOperationStatus();
        $brokers = $filter->getBrokerId() === null ? [] : $filter->getBrokerId();
        $itemId = $filter->getItemId() === null ? [] : $filter->getItemId();
        $isAnalyticProceed = $filter->getIsAnalyticProceed() === null ? null : $filter->getIsAnalyticProceed();
        $isDeleted = $filter->getIsDeleted() === null ? null : $filter->getIsDeleted();

        $operationEntityList = $this->itemOperationStorage->findByUserAndFilters(
            $userId,
            $itemId,
            $filter->getDateFrom(),
            $filter->getDateTo(),
            $types,
            $statuses,
            $brokers,
            $isAnalyticProceed,
            $isDeleted
        );

        /** @var ItemOperationEntity $operation */
        foreach ($operationEntityList->getIterator() as $operation) {
            $operations->add($this->convertOperationEntityToModel($operation));
        }
        return $operations;
    }

    public function prepareAsSafetyOperations(OperationCollection $operations): OperationCollection
    {
        $preparedAsSafety = new OperationCollection();
        /** @var OperationModel $operation*/
        foreach ($operations->getIterator() as $operation) {
            $preparedOperation = clone $operation;
            if (
                in_array(
                    $operation->getOperationType(),
                    [
                        OperationType::SELL_CARD->value,
                        OperationType::BUY_CARD->value
                    ]
                )
            ) {
                $preparedOperation->setAmount(-1 * $operation->getAmount());
            }

            $preparedAsSafety->add($preparedOperation);
        }

        return $preparedAsSafety;
    }

    public function calculateOperationSummary(OperationCollection $operations, string $currency): PriceModel
    {
        $amount = 0;
        $rates = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationCurrency = $operation->getCurrencyIso();
            if (!isset($rates[$operationCurrency])) {
                $rates[$operationCurrency] = $this->exchangeCurrencyService->getExchangeRate(
                    $operationCurrency,
                    $currency,
                    $operation->getDate()
                );
            }
            $amount += $operation->getAmount() * $rates[$operationCurrency];
        }
        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    public function getPayIn(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $operationTypes = [
            OperationType::INPUT->value,
            OperationType::OUTPUT->value,
            OperationType::BUY_CARD->value,
            OperationType::SELL_CARD->value
        ];
        return $this->calculateSummaryByOperationTypes(
            $userId,
            $dateFrom,
            $dateTo,
            $operationTypes,
            $aggregatePeriod
        );
    }

    public function getEarning(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $operationTypes = [
            OperationType::DIVIDEND_CARD->value,
            OperationType::DIVIDEND->value
        ];
        return $this->calculateSummaryByOperationTypes(
            $userId,
            $dateFrom,
            $dateTo,
            $operationTypes,
            $aggregatePeriod
        );
    }

    public function getActiveUserOperationById(int $userId, int $operationId): OperationModel
    {
        $operationEntity = $this->itemOperationStorage->findActiveById($operationId);

        if (
            $operationEntity === null
            || $operationEntity->getUserId() !== $userId
        ) {
            throw new UserAccessDeniedException('Access denied for user', 2025);
        }

        return $this->convertOperationEntityToModel($operationEntity);
    }

    public function getOperationById(int $operationId): OperationModel
    {
        $operationEntity = $this->itemOperationStorage->findById($operationId);

        if ($operationEntity === null) {
            throw new UserAccessDeniedException('Access denied for user', 2025);
        }

        return $this->convertOperationEntityToModel($operationEntity);
    }

    public function getSourceOperation(DataSourceType $source): OperationCollection
    {
        $operations = new OperationCollection();
        $entities = $this->itemOperationStorage->findByUserAndFilters(
            null,
            null,
            null,
            null,
            [],
            [],
            [$source->value],
            null,
            [0]
        );
        foreach ($entities as $entity) {
            $operations->add($this->convertOperationEntityToModel($entity));
        }

        return $operations;
    }

    public function generateUserOperationExternalId(): string
    {
        return Uuid::uuid4()->toString();
    }

    public function addUserOperation(int $userId, OperationModel $operationModel): int
    {
        $operationModel->setUserId($userId);
        $operationModel->setExternalId($this->generateUserOperationExternalId());
        $operationEntity = $this->convertOperationModelToEntity($operationModel);
        $operationEntity->setIsAnalyticProceed($operationModel->getIsAnalyticProceed());
        $operationId = $this->itemOperationStorage->addEntity($operationEntity);

        $this->publishOperationEvent($operationId);

        return $operationId;
    }

    public function addUserOperationBatch(int $userId, OperationCollection $operations): ImportStatisticModel
    {
        $added = 0;
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationId = $this->addUserOperation($userId, $operation);
            if ($operationId > 0) {
                $added++;
            }
        }
        return new ImportStatisticModel([
            'added' => $added,
            'skipped' => 0,
            'updated' => 0
        ]);
    }

    public function editUserOperation($userId, OperationModel $operationModel): void
    {
        $operation = $this->prepareEntityForUpdate($userId, $operationModel);
        $operationId = $this->itemOperationStorage->addEntity($operation);

        $this->publishOperationEvent($operationId);
    }

    public function editUserOperationBatch(int $userId, OperationCollection $operations): void
    {
        $operationEntities = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationEntities[] = $this->prepareEntityForUpdate($userId, $operation);
            $updatedItems[$operation->getItemId()] = true;
        }
        $this->itemOperationStorage->addEntityArray($operationEntities);

        foreach ($operationEntities as $entity) {
            $this->publishOperationEvent($entity->getId());
        }
    }

    public function deleteUserOperation(int $userId, int $operationId): void
    {
        $operation = $this->itemOperationStorage->findActiveById($operationId);
        if ($operation === null || $userId !== $operation->getUserId()) {
            throw new UserAccessDeniedException('Access denied update operation for user', 2025);
        }

        $operation->setExternalId(
            $operation->getExternalId() . '_deleted_' .  $this->generateUserOperationExternalId()
        );
        $operation->setIsDeleted(1);
        $operation->setIsAnalyticProceed(0);

        $operationId = $this->itemOperationStorage->addEntity($operation);
        $this->publishOperationEvent($operationId);
    }

    public function deleteUserOperationsBatch(int $userId, OperationCollection $operations): void
    {
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operation->setExternalId(
                $operation->getExternalId() . '_deleted_' .  $this->generateUserOperationExternalId()
            );
            $operation->setIsDeleted(1);
            $operation->setIsAnalyticProceed(0);
        }

        $this->editUserOperationBatch($userId, $operations);
    }

    /**
     * @param int[] $itemIds
     * @return int[]
     */
    public function deleteItemOperation(array $itemIds): array
    {
        if (count($itemIds) === 0) {
            return [];
        }

        $ids = [];

        $operations = $this->itemOperationStorage->findByUserAndFilters(null, $itemIds);
        foreach ($operations as $operation) {
            $ids[] = $operation->getId();
        }

        return $this->deleteOperationBatch($ids);
    }

    /**
     * @param int[] $operationIds
     * @return int[]
     */
    public function deleteOperationBatch(array $operationIds): array
    {
        $results = [];

        $operations = $this->itemOperationStorage->findByIds($operationIds);
        foreach ($operations as $operation) {
            $operation->setExternalId(
                $operation->getExternalId() . '_deleted_' .  $this->generateUserOperationExternalId()
            );
            $operation->setIsDeleted(1);
            $operation->setIsAnalyticProceed(0);
        }

        $entities = $this->itemOperationStorage->addEntityArray($operations);
        foreach ($entities as $entity) {
            $results[] = $entity->getId();
            $this->publishOperationEvent($entity->getId());
        }

        return $results;
    }

    public function importOperations(OperationFilter $filter): ImportStatisticModel
    {
        return $this->marketOperationService->importUserOperations($filter);
    }

    /**
     * @param int $userId
     * @param DataSourceType $source
     * @param UploadedFile[] $files
     * @throws \Exception
     */
    public function uploadingFromFileRequest(int $userId, DataSourceType $source, array $files): void
    {
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../tmp";
        $filesPath = [];
        $timestamp = new \DateTime()->getTimestamp();

        foreach ($files as $file) {
            $fileName = $dir . "/" . $timestamp . "_" . rand(10000, 99999);
            $file->moveTo($fileName);
            $filesPath[] = $fileName;
        }

        $this->amqpClient->publishMessage([
            "type" => "uploadingOperationsFile",
            "data" => [
                "source" => $source->value,
                "userId" => $userId,
                "files" => $filesPath
            ]
        ]);
    }

    /**
     * @param int $userId
     * @param DataSourceType $source
     * @param UploadedFile[] $files
     * @return ImportStatisticModel
     * @throws \Exception
     */
    public function importFromFile(int $userId, DataSourceType $source, array $files): ImportStatisticModel
    {
        $source = $this->sourceFactory->getSource($source);

        $items = $source->parseItemsFromFile($userId, $files);
        $newItems = new ItemCollection();
        /** @var ItemModel $item */
        foreach ($items as $item) {
            $filter = new ItemFilterModel(['externalId' => [$item->getExternalId()]]);
            $itemEntity = $this->itemStorage->findItem($filter);
            if (
                $newItems->findByExternalIdAndSource($item->getExternalId(), $item->getSource())->count() === 0
                && count($itemEntity) === 0
            ) {
                $newItems->add($this->itemConverterService->convertItemModelToEntity($item));
            }
        }
        if (count($newItems) > 0) {
            $this->itemStorage->addEntityArray($newItems->toArray());
        }

        $operations = $source->parseOperationsFromFile($userId, $files);

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $key => $operation) {
            if ($operation->getItemExternalId() !== null) {
                $operationItem = $this->itemStorage->findActiveByExternalId($operation->getItemExternalId());
                if ($operationItem !== null) {
                    $operation->setItemId($operationItem->getId());
                } else {
                    $operations->remove($key);
                }
            }
        }
        $statistic = $this->addOrUpdateOperationBatch($operations);
        return new ImportStatisticModel([
            'added' => $statistic->getAdded(),
            'skipped' => $statistic->getSkipped(),
            'updated' => $statistic->getUpdated(),
        ]);
    }

    private function prepareEntityForUpdate($userId, OperationModel $operationModel): ItemOperationEntity
    {
        $operation = $this->itemOperationStorage->findById($operationModel->getItemOperationId());

        if (
            $operation === null
            || $userId !== $operation->getUserId()
            || $operation->getUserId() !== $operationModel->getUserId()
        ) {
            throw new UserAccessDeniedException('Access denied update operation for user', 2025);
        }

        $currency = $this->currencyService->getCurrencyByIso($operationModel->getCurrencyIso());

        $operationType = $this->operationTypeStorage->findByExternalId($operationModel->getOperationType());

        $operation->setStatus($operationModel->getStatus());
        $operation->setOperationTypeId($operationType->getId());
        $operation->setOperationType($operationType);
        $operation->setDate($operationModel->getDate()->setTimezone(new \DateTimeZone('UTC')));
        $operation->setAmount($operationModel->getAmount());
        $operation->setQuantity($operationModel->getQuantity());
        $operation->setIsAnalyticProceed($operationModel->getIsAnalyticProceed());
        $operation->setCurrency($currency);
        $operation->setCurrencyId($currency->getId());
        $operation->setIsDeleted($operationModel->getIsDeleted());

        return $operation;
    }

    public function convertOperationModelToEntity(
        OperationModel $operation
    ): ItemOperationEntity {
        $operationType = $this->operationTypeStorage->findByExternalId($operation->getOperationType());
        $broker = $operation->getBrokerId() === null
            ? null
            : $this->brokerService->getBrokerEntityById($operation->getBrokerId());
        $item = $operation->getItemId() ? $this->itemStorage->findById($operation->getItemId()) : null;
        $currency = $this->currencyService->getCurrencyByIso($operation->getCurrencyIso());

        $operationEntity = new ItemOperationEntity();
        $operationEntity->setId($operation->getItemOperationId());
        $operationEntity->setItem($item);
        $operationEntity->setItemId($operation->getItemId());
        $operationEntity->setExternalId($operation->getExternalId());
        $operationEntity->setUserId($operation->getUserId());
        $operationEntity->setOperationType($operationType);
        $operationEntity->setBrokerId($operation->getBrokerId());
        $operationEntity->setBroker($broker);
        $operationEntity->setCreatedDate($operation->getCreatedDate());
        $operationEntity->setQuantity($operation->getQuantity());
        $operationEntity->setDate($operation->getDate()->setTimezone(new \DateTimeZone('UTC')));
        $operationEntity->setAmount($operation->getAmount());
        $operationEntity->setCurrencyId($currency->getId());
        $operationEntity->setCurrency($currency);
        $operationEntity->setStatus($operation->getStatus());
        $operationEntity->setIsAnalyticProceed($operation->getIsAnalyticProceed());
        $operationEntity->setIsDeleted($operation->getIsDeleted());

        return $operationEntity;
    }

    public function publishOperationEvent(int $operationId): void
    {
        $message = [
            'type' => 'operationChange',
            'data' => [
                'operationId' => $operationId
            ]
        ];

        $this->amqpClient->publishMessage($message);
    }

    public function deleteUserItemOperations(int $userId, int $itemId): void
    {
        $filter = new OperationFiltersModel([
            'itemId' => [$itemId],
            'dateTo' => new \DateTime('+10Y')
        ]);
        $operations = $this->getFilteredUserOperation($userId, $filter);
        $this->deleteUserOperationsBatch($userId, $operations);
    }

    private function convertOperationEntityToModel(ItemOperationEntity $operation): OperationModel
    {
        $item = null;
        if ($operation->getItem() !== null) {
            $item = $this->itemConverterService->convertItemEntityToModel($operation->getItem());
        }

        return new OperationModel([
            'itemOperationId' => $operation->getId(),
            'userId' => $operation->getUserId(),
            'brokerId' => $operation->getBrokerId(),
            'itemId' => $operation->getItemId(),
            'item' => $item,
            'operationType' => $operation->getOperationType()->getExternalId(),
            'externalId' => $operation->getExternalId(),
            'createdDate' => $operation->getCreatedDate(),
            'quantity' => $operation->getQuantity(),
            'date' => $operation->getDate(),
            'amount' => $operation->getAmount(),
            'currencyIso' => $operation->getCurrency()->getIso(),
            'status' => $operation->getStatus(),
            'isAnalyticProceed' => $operation->getIsAnalyticProceed(),
            'isDeleted' => $operation->getIsDeleted()
        ]);
    }

    private function convertOperationTypeEntityToModel(OperationTypeEntity $operationType): OperationTypeModel
    {
        return new OperationTypeModel([
            'operationTypeId' => $operationType->getId(),
            'externalId' => $operationType->getExternalId(),
            'name' => $operationType->getName()
        ]);
    }

    private function convertOperationStatusEntityToModel(OperationStatusEntity $status): OperationStatusModel
    {
        return new OperationStatusModel([
            'operationStatusId' => $status->getId(),
            'externalId' => $status->getExternalId(),
            'name' => $status->getName()
        ]);
    }

    private function calculateSummaryByOperationTypes(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        array $operationTypes,
        string $aggregatePeriod
    ): OperationAggregateCollection {
        $filterModel = new OperationFiltersModel([
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'operationStatus' => [BrokerOperationStatus::DONE->value],
            'operationType' => $operationTypes
        ]);
        $operations = $this->getFilteredUserOperation($userId, $filterModel);
        $operations = $this->prepareAsSafetyOperations($operations);

        return match ($aggregatePeriod) {
            'month' => $this->calculateMonthlySummaryByOperationTypes($dateFrom, $dateTo, $operations),
            'year' => $this->calculateYearSummaryByOperationTypes($dateFrom, $dateTo, $operations)
        };
    }

    private function calculateMonthlySummaryByOperationTypes(
        \DateTime $dateFrom,
        \DateTime $dateTo,
        OperationCollection $operations
    ): OperationAggregateCollection {
        $currentPeriod = clone $dateFrom;
        $currentPeriod->setDate(
            (int)$currentPeriod->format('Y'),
            (int)$currentPeriod->format('m'),
            1
        )->setTime(0, 0, 0);
        $aggregateCollection = new OperationAggregateCollection();

        while ($currentPeriod < $dateTo) {
            $monthOperations = $operations->filterByMonth($currentPeriod);

            $price = $this->calculateOperationSummary($monthOperations, 'RUB');

            $aggregateCollection->add(
                new OperationAggregateModel([
                    'date' => $currentPeriod->format('Y-m-d'),
                    'amount' => $price->getAmount()
                ])
            );

            $currentPeriod->add(new \DateInterval('P1M'));
        }

        return $aggregateCollection;
    }

    private function calculateYearSummaryByOperationTypes(
        \DateTime $dateFrom,
        \DateTime $dateTo,
        OperationCollection $operations
    ): OperationAggregateCollection {
        $currentPeriod = clone $dateFrom;
        $currentPeriod->setDate((int)$currentPeriod->format('Y'), 1, 1)
            ->setTime(0, 0, 0);

        $aggregateCollection = new OperationAggregateCollection();

        while ($currentPeriod < $dateTo) {
            $yearOperations = $operations->filterByYear($currentPeriod);

            $price = $this->calculateOperationSummary($yearOperations, 'RUB');
            $aggregateCollection->add(
                new OperationAggregateModel([
                    'date' => $currentPeriod->format('Y-m-d'),
                    'amount' => $price->getAmount()
                ])
            );

            $currentPeriod->add(new \DateInterval('P1Y'));
        }

        return $aggregateCollection;
    }
}
