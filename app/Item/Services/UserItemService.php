<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Analytics\Math;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Item\Collections\OperationCollection;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Types\ItemTypes;
use App\Item\Types\OperationType;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;

class UserItemService
{
    public function __construct(
        private \DateTime $currentDateTime,
        private PortfolioService $portfolioService,
        private ExchangeCurrencyService $exchangeCurrencyService,
        private OperationService $operationService
    ) {
    }

    /**
     * @param int $userId
     * @param int $itemId
     * @return array{
     *     quantity: int,
     *     amount: PriceModel,
     *     investments: PriceModel,
     *     income: array {amount: PriceModel},
     *     tax: array {amount: PriceModel, percent: float},
     *     fee: array {amount: PriceModel, percent: float},
     *     profit: array {amount: PriceModel, percent: float},
     *     valueGrowth: array {amount: PriceModel, percent: float},
     *     incomeForecast: PriceModel
     * }|null
     */
    public function getItemSummary(int $userId, int $itemId): array|null
    {
        $operations = $this->getItemOperations($userId, [$itemId]);

        $portfolio = $this->portfolioService->buildPricedPortfolioByOperations(
            $operations,
            $this->currentDateTime,
            'RUB'
        );
        /** @var PortfolioItemModel|boolean $item */
        $item = $portfolio->getItems()->first();
        if ($item === false) {
            return null;
        }

        $currency = $item->getCurrencyIso();
        $itemsAmount = $this->portfolioService->calculatePortfolioItemsAmount($portfolio->getItems(), $currency)
            ->getAmount();
        $itemQuantity = $this->portfolioService->calculatePortfolioItemsQuantity($portfolio->getItems());

        $income = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::DIVIDEND,
                OperationType::DIVIDEND_CARD
            ],
            $currency
        );
        $tax = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::TAX
            ],
            $currency
        );
        $fee = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::FEE
            ],
            $currency
        );
        $buySellBalance = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::BUY,
                OperationType::BUY_CARD,
                OperationType::SELL,
                OperationType::SELL_CARD,
                OperationType::INPUT,
                OperationType::OUTPUT
            ],
            $currency
        );
        $invested = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::BUY,
                OperationType::BUY_CARD,
                OperationType::INPUT
            ],
            $currency
        );

        $repayment = $this->getItemBalanceByOperationType(
            $operations,
            [
                OperationType::REPAYMENT
            ],
            $currency
        );

        $forecastStartDate = clone $this->currentDateTime;
        $forecastEndDate = clone $forecastStartDate;
        $forecastEndDate->add(new \DateInterval('P1Y'));

        $profit = $itemsAmount
            + $buySellBalance->getAmount()
            + $tax->getAmount()
            + $fee->getAmount()
            + $repayment->getAmount();


        if (
            in_array(
                $item->getItem()->getType(),
                [ItemTypes::MARKET, ItemTypes::REAL_ESTATE, ItemTypes::CROWD_FUNDING]
            )
        ) {
            $profit += $income->getAmount();
        }

        $xirr = $this->calculateXirr($operations, $portfolio->getItems()->first());
        $buySellFlow = $this->calculateCashFlow($operations, $currency);

        return [
            'quantity' => $itemQuantity,
            'amount' => new PriceModel([
                'amount' => $itemsAmount,
                'currency' => $currency
            ]),
            'investments' => new PriceModel([
                'amount' => -1 * $invested->getAmount(),
                'currency' => $invested->getCurrency()
            ]),
            'income' => [
                'amount' => new PriceModel([
                    'amount' => $income->getAmount(),
                    'currency' => $income->getCurrency()
                ])
            ],
            'tax' => [
                'amount' => new PriceModel([
                    'amount' => $tax->getAmount() === 0.0 ? 0 : -1 * $tax->getAmount(),
                    'currency' => $tax->getCurrency()
                ]),
                'percent' => $income->getAmount() === 0.0 ? 0 : $tax->getAmount() / $income->getAmount()
            ],
            'fee' => [
                'amount' => new PriceModel([
                    'amount' => $fee->getAmount() === 0.0 ? 0 : -1 * $fee->getAmount(),
                    'currency' => $fee->getCurrency()
                ]),
                'percent' => $fee->getAmount() / $buySellFlow->getAmount()
            ],
            'profit' => [
                'amount' => new PriceModel([
                    'amount' => $profit,
                    'currency' => $currency
                ]),
                'percent' => $xirr === null ? 0 : $xirr
            ],
            'valueGrowth' => [
                'amount' => new PriceModel([
                        'amount' => $itemsAmount + $buySellBalance->getAmount() + $repayment->getAmount(),
                        'currency' => $currency
                    ]),
                'percent' => $invested->getAmount() !== 0.0
                    ? -1 * ($itemsAmount + $buySellBalance->getAmount() + $repayment->getAmount())
                    / $invested->getAmount()
                    : 0
            ],
            'incomeForecast' => new PriceModel([
                'amount' => $this->portfolioService->calculateItemsForecastIncome(
                    $portfolio->getItems(),
                    $currency,
                    $forecastStartDate,
                    $forecastEndDate
                ),
                'currency' => $currency
            ])
        ];
    }

    private function getItemOperations(int $userId, array $itemIds): OperationCollection
    {
        $filter = new OperationFiltersModel([
            'itemId' => $itemIds,
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'operationType' => [
                BrokerOperationTypeType::OPERATION_TYPE_BUY,
                BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_SELL,
                BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_INPUT,
                BrokerOperationTypeType::OPERATION_TYPE_OUTPUT,
                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_TAX,
                BrokerOperationTypeType::OPERATION_TYPE_FEE,
                BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT,
                BrokerOperationTypeType::OPERATION_TYPE_SECURITY_OUT,
                BrokerOperationTypeType::OPERATION_TYPE_SECURITY_IN
            ],
            'dateTo' => $this->currentDateTime
        ]);
        return $this->operationService->getFilteredUserOperation($userId, $filter);
    }

    private function calculateXirr(OperationCollection $operations, PortfolioItemModel $item): float|null
    {
        $baseCurrency = $item->getCurrencyIso();

        $preparedData = [];
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $rate = $this->exchangeCurrencyService->getExchangeRate(
                $operation->getCurrencyIso(),
                $baseCurrency,
                $operation->getDate()
            );
            $preparedData[] = [
                'date' => $operation->getDate(),
                'amount' => $rate * $operation->getAmount()
            ];
        }

        $rate = $this->exchangeCurrencyService->getExchangeRate(
            $item->getPrice()->getCurrency(),
            $baseCurrency,
            $this->currentDateTime
        );
        $preparedData = array_reverse($preparedData);
        $preparedData[] = [
            'date' => $this->currentDateTime,
            'amount' => $rate * $item->getPrice()->getAmount() * $item->getQuantity()
        ];

        $calculator = new Math();
        try {
            return $calculator->xirr($preparedData);
        } catch (\Exception $e) {
            return null;
        }
    }

    private function calculateCashFlow(OperationCollection $operations, $currency): PriceModel
    {
        $types = [
            OperationType::BUY,
            OperationType::BUY_CARD,
            OperationType::SELL,
            OperationType::SELL_CARD,
            OperationType::INPUT,
            OperationType::OUTPUT
        ];

        $filteredOperation = $operations->filterByType($types);
        $sum = 0;
        /** @var OperationModel $operation */
        foreach ($filteredOperation->getIterator() as $operation) {
            $rate = $this->exchangeCurrencyService->getExchangeRate(
                $operation->getCurrencyIso(),
                $currency,
                $this->currentDateTime
            );
            $sum += abs($operation->getAmount() * $rate);
        }

        return new PriceModel([
            'amount' => $sum,
            'currency' => $currency
        ]);
    }

    private function getItemBalanceByOperationType(
        OperationCollection $operations,
        array $operationType,
        string $currency,
    ): PriceModel {
        $filteredOperations = $operations->filterByType($operationType);
        return $this->operationService->calculateOperationSummary($filteredOperations, $currency);
    }
}
