<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Entities\AssetsEntity;
use App\Assets\Models\AssetModel;
use App\Assets\Storages\PayoutTypeStorage;
use App\Deposit\Models\DepositModel;
use App\Intl\Services\CurrencyService;
use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Types\ItemType;
use App\Loan\Models\LoanModel;
use App\Market\Services\MarketInstrumentHelperService;
use App\Models\PriceModel;
use App\RealEstate\Collections\RealEstatePriceCollection;
use App\RealEstate\Entities\RealEstateEntity;
use App\RealEstate\Entities\RealEstatePriceEntity;
use App\RealEstate\Models\RealEstateModel;
use App\RealEstate\Models\RealEstateOperationModel;
use App\RealEstate\Models\RealEstatePriceModel;

class ItemConverterService
{
    public function __construct(
        private readonly MarketInstrumentHelperService $marketInstrumentHelperService,
        private readonly CurrencyService $currencyService,
        private readonly PayoutTypeStorage $payoutTypeStorage,
    ) {
    }

    /**
     * @var ItemModel[]
     */
    private array $items = [];

    public function convertItemEntityToModel(ItemEntity $itemEntity): ItemModel
    {
        if (!isset($this->items[$itemEntity->getId()])) {
            $asset = null;
            $loan = null;
            $deposit = null;
            $instrument = null;
            $realEstate = null;
            switch ($itemEntity->getType()) {
                case ItemType::CROWD_FUNDING->value:
                    $assetEntity = $itemEntity->getAsset();
                    $asset = new AssetModel([
                        'assetId' => $assetEntity->getId(),
                        'externalId' => $assetEntity->getExternalId(),
                        'platform' => $assetEntity->getPlatform(),
                        'name' => $assetEntity->getName(),
                        'type' => $assetEntity->getType(),
                        'payoutType' => $assetEntity->getPayoutType()?->getExternalId(),
                        'dealDate' => $assetEntity->getDealDate(),
                        'duration' => $assetEntity->getDuration(),
                        'durationPeriod' => $assetEntity->getDurationPeriod(),
                        'interestPercent' => $assetEntity->getInterestPercent(),
                        'interestAmount' => $assetEntity->getInterestAmount(),
                        'payoutFrequency' => $assetEntity->getPayoutFrequency(),
                        'payoutFrequencyPeriod' => $assetEntity->getPayoutFrequencyPeriod(),
                        'nominalAmount' => $assetEntity->getNominalAmount(),
                        'currencyIso' => $assetEntity->getCurrency()?->getIso()
                    ]);
                    break;
                case ItemType::LOAN->value:
                    $loan = new LoanModel([
                        'loanId' => $itemEntity->getLoan()->getId(),
                        'itemId' => $itemEntity->getLoan()->getItemId(),
                        'userId' => $itemEntity->getLoan()->getUserId(),
                        'isActive' => ($itemEntity->getLoan()->getItem()->getIsOutdated() + 1) % 2,
                        'name' => $itemEntity->getLoan()->getItem()->getName(),
                        'amount' => -1 * $itemEntity->getLoan()->getOperation()->getAmount(),
                        'currency' => $itemEntity->getLoan()->getOperation()->getCurrency()->getIso(),
                        'date' => $itemEntity->getLoan()->getOperation()->getDate(),
                        'paymentAmount' => $itemEntity->getLoan()->getPaymentAmount(),
                        'paymentCurrency' => $itemEntity->getLoan()->getPaymentCurrency(),
                        'payoutFrequency' => $itemEntity->getLoan()->getPayoutFrequency(),
                        'payoutFrequencyPeriod' => $itemEntity->getLoan()->getPayoutFrequencyPeriod(),
                        'interestPercent' => $itemEntity->getLoan()->getInterestPercent(),
                    ]);
                    break;
                case ItemType::DEPOSIT->value:
                    $deposit = new DepositModel([
                        'depositId' => $itemEntity->getDeposit()->getId(),
                        'itemId' => $itemEntity->getDeposit()->getItemId(),
                        'name' => $itemEntity->getDeposit()->getItem()->getName(),
                        'userId' => $itemEntity->getDeposit()->getUserId(),
                        'isActive' => ($itemEntity->getIsOutdated() + 1) % 2,
                        'dealDate' => $itemEntity->getDeposit()->getDealDate(),
                        'interestPercent' => $itemEntity->getDeposit()->getInterestPercent(),
                        'duration' => $itemEntity->getDeposit()->getDuration(),
                        'durationPeriod' => $itemEntity->getDeposit()->getDurationPeriod(),
                        'payoutFrequency' => $itemEntity->getDeposit()->getPayoutFrequency(),
                        'payoutFrequencyPeriod' => $itemEntity->getDeposit()->getPayoutFrequencyPeriod(),
                        'amount' => $itemEntity->getDeposit()->getOperation()->getAmount(),
                        'currency' => $itemEntity->getDeposit()->getOperation()->getCurrency()->getIso()
                    ]);
                    break;
                case ItemType::REAL_ESTATE->value:
                    $realEstate = $this->convertRealEstateEntityToModel($itemEntity->getRealEstate());
                    break;
                case ItemType::MARKET->value:
                    $entity = $itemEntity->getMarketInstrument();
                    if ($entity !== null) {
                        $instrument = $this->marketInstrumentHelperService->convertEntityToModel($entity);
                    }
                    break;
            }

            $this->items[$itemEntity->getId()] = new ItemModel([
                'itemId' => $itemEntity->getId(),
                'type' => $itemEntity->getType(),
                'source' => $itemEntity->getSource(),
                'externalId' => $itemEntity->getExternalId(),
                'name' => $itemEntity->getName(),
                'logo' => $itemEntity->getLogo(),
                'isDeleted' => !!$itemEntity->getIsDeleted(),
                'isOutdated' => !!$itemEntity->getIsOutdated(),
                'asset' => $asset,
                'stock' => $instrument,
                'realEstate' => $realEstate,
                'loan' => $loan,
                'deposit' => $deposit
            ]);
        }

        return $this->items[$itemEntity->getId()];
    }

    public function convertItemModelToEntity(ItemModel $item): ItemEntity
    {
        $entity = new ItemEntity();

        $entity->setType($item->getType());
        $entity->setSource($item->getSource());
        $entity->setExternalId($item->getExternalId());
        $entity->setName($item->getName());
        $entity->setLogo($item->getLogo());

        if ($item->getAsset() !== null) {
            $assetEntity = new AssetsEntity();
            $assetEntity->setExternalId($item->getAsset()->getExternalId());
            $assetEntity->setNominalAmount($item->getAsset()->getNominalAmount());
            $assetEntity->setPlatform($item->getAsset()->getPlatform());
            $assetEntity->setName($item->getAsset()->getName());
            $assetEntity->setType($item->getAsset()->getType());
            $assetEntity->setDealDate($item->getAsset()->getDealDate());
            $assetEntity->setDuration($item->getAsset()->getDuration());
            $assetEntity->setDurationPeriod($item->getAsset()->getDurationPeriod());
            $assetEntity->setInterestPercent($item->getAsset()->getInterestPercent());
            $assetEntity->setInterestAmount($item->getAsset()->getInterestAmount());
            $assetEntity->setPayoutFrequency($item->getAsset()->getPayoutFrequency());
            $assetEntity->setPayoutFrequencyPeriod($item->getAsset()->getPayoutFrequencyPeriod());
            $assetEntity->setNominalAmount($item->getAsset()->getNominalAmount());

            $currency = $this->currencyService->getCurrencyByIso($item->getAsset()->getCurrencyIso());
            $assetEntity->setCurrency($currency);
            $assetEntity->setCurrencyId($currency->getId());

            $payoutType = $this->payoutTypeStorage->findByExternalId($item->getAsset()->getPayoutType());
            $assetEntity->setPayoutType($payoutType);
            $assetEntity->setPayoutTypeId($payoutType->getId());

            $assetEntity->setItem($entity);
            $entity->setAsset($assetEntity);
        }

        return $entity;
    }

    public function convertRealEstateEntityToModel(RealEstateEntity $entity): RealEstateModel
    {
        $realEstatePrices = $entity->getRealEstatePrices();

        $prices = [];
        /** @var RealEstatePriceEntity $price */
        foreach ($realEstatePrices->getIterator() as $price) {
            $prices[] = $this->convertRealEstatePriceEntityToModel($price);
        }

        return new RealEstateModel([
            'realEstateId' => $entity->getId(),
            'itemId' => $entity->getItemId(),
            'userId' => $entity->getUserId(),
            'name' => $entity->getItem()->getName(),
            'isActive' => ($entity->getItem()->getIsOutdated() + 1) % 2,
            'payoutFrequency' => $entity->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $entity->getPayoutFrequencyPeriod(),
            'interestAmount' => $entity->getInterestAmount(),
            'interestCurrency' => $entity->getInterestCurrency(),
            'currency' => $entity->getCurrency(),
            'prices' => count($prices) ? new RealEstatePriceCollection($prices) : null,
            'purchaseDate' => $entity->getPurchaseOperation()->getDate(),
            'purchaseAmount' => -1 * $entity->getPurchaseOperation()->getAmount(),
            'saleOperation' => $entity->getSaleOperation() !== null
                ? new RealEstateOperationModel([
                    'price' => new PriceModel([
                        'amount' => $entity->getSaleOperation()->getAmount(),
                        'currency' => $entity->getSaleOperation()->getCurrency()->getIso()
                    ]),
                    'date' => $entity->getSaleOperation()->getDate()
                ])
                : null
        ]);
    }

    private function convertRealEstatePriceEntityToModel(RealEstatePriceEntity $entity): RealEstatePriceModel
    {
        return new RealEstatePriceModel([
            'realEstatePriceId' => $entity->getId(),
            'realEstateId' => $entity->getRealEstateId(),
            'amount' => $entity->getAmount(),
            'currency' => $entity->getCurrency(),
            'date' => $entity->getDate()
        ]);
    }
}
