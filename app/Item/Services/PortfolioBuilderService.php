<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Services\AssetService;
use App\Broker\Types\BrokerOperationStatus;
use App\Deposit\Services\DepositService;
use App\Item\Collections\ItemCollection;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;
use App\Item\Entities\UserPortfolioBalanceEntity;
use App\Item\Entities\UserPortfolioEntity;
use App\Item\Entities\UserPortfolioItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Models\PortfolioBalanceModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Models\PortfolioModel;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\UserPortfolioStorage;
use App\Item\Types\ItemType;
use App\Item\Types\ItemTypes;
use App\Item\Types\OperationType;
use App\Loan\Services\LoanService;
use App\Market\Interfaces\CandleServiceInterface;
use App\Models\PriceModel;
use App\RealEstate\Services\RealEstateService;
use App\Services\ExchangeCurrencyService;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioBuilderService
{
    public function __construct(
        private readonly UserPortfolioStorage $userPortfolioStorage,
        private readonly OperationService $operationService,
        private readonly RealEstateService $realEstateService,
        private readonly DepositService $depositService,
        private readonly LoanService $loanService,
        private readonly AssetService $assetService,
        private readonly CandleServiceInterface $candleService,
        private readonly ExchangeCurrencyService $exchangeCurrencyService,
        private readonly ItemStorage $itemStorage,
        private readonly ItemConverterService $itemConverterService
    ) {
    }

    public function buildPortfolio(int $userId, \DateTime $date, string $currency): PortfolioModel
    {
        $portfolio = $this->buildBasePortfolio($userId, $date);
        $portfolio->setItems(
            $this->calculatePortfolioItemPrices(
                $userId,
                $portfolio,
                $date
            )
        );
        $portfolio->setItems(
            $this->fillPortfolioBasePrice(
                $portfolio->getItems(),
                $currency,
                $date
            )
        );

        return $portfolio;
    }

    public function buildBasePortfolio(int $userId, \DateTime $date): PortfolioModel
    {
        $portfolio = [];
        $isCached = true;

        $userPortfolio = $this->getLastPortfolio($userId, $date);
        if ($userPortfolio !== null) {
            $portfolio[] = $userPortfolio;
            $dateFrom = $userPortfolio->getDate();
        } else {
            $dateFrom = new \DateTime('2000-01-01 00:00:00');
        }

        $operationFilter = new OperationFiltersModel([
            'operationStatus' => [BrokerOperationStatus::DONE->value],
            'dateFrom' => $dateFrom,
            'dateTo' => $date,
            'isDeleted' => [0]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $operationFilter);

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $portfolio[] = $this->convertOperationToPortfolio($operation);
            $isCached = false;
        }

        $result = $this->mergePortfolios($portfolio);
        $result->setIsCached($isCached);
        $result->setItems($result->getItems()->filterNonZeroItems());

        $result->setDate($dateFrom);

        return $result;
    }

    public function calculatePortfolioItemPrices(
        int $userId,
        PortfolioModel $portfolio,
        \DateTime $date
    ): PortfolioItemCollection {
        $items = $portfolio->getItems();

        $stockItemCollection = new ItemCollection();
        /** @var PortfolioItemModel $item */
        foreach ($items->getIterator() as $item) {
            $itemType = ItemType::tryFrom($item->getItem()->getType());
            switch ($itemType) {
                case ItemType::MARKET:
                    $stockItemCollection->add($item->getItem());
                    break;
                case ItemType::REAL_ESTATE:
                    $realEstate = $item->getItem()->getRealEstate();
                    $price = $this->realEstateService->calculateRealEstatePrice(
                        $realEstate->getRealEstateId(),
                        $date
                    );
                    $item->setAmount($price->getAmount() * $item->getQuantity());
                    $item->setCurrencyIso($price->getCurrency());
                    $item->setPrice(new PriceModel([
                        'amount' => $price->getAmount(),
                        'currency' => $price->getCurrency()
                    ]));
                    break;
                case ItemType::DEPOSIT:
                    $deposit = $item->getItem()->getDeposit();
                    if ($item->getPrice() === null) {
                        $price = $this->depositService->getUserDepositPrice(
                            $deposit->getUserId(),
                            $deposit->getItemId(),
                            $date,
                            $deposit->getCurrency()
                        );
                    } else {
                        $price = $item->getPrice();
                    }
                    $item->setAmount($price->getAmount());
                    $item->setCurrencyIso($price->getCurrency());
                    $item->setPrice(new PriceModel([
                        'amount' => $price->getAmount(),
                        'currency' => $price->getCurrency()
                    ]));
                    break;
                case ItemType::LOAN:
                    $loan = $item->getItem()->getLoan();
                    if ($item->getPrice() === null) {
                        $price = $this->loanService->getUserLoanPrice(
                            $loan->getUserId(),
                            $loan->getItemId(),
                            $date
                        );
                    } else {
                        $price = $item->getPrice();
                    }
                    $item->setAmount($price->getAmount());
                    $item->setCurrencyIso($price->getCurrency());
                    $item->setPrice(new PriceModel([
                        'amount' => $price->getAmount(),
                        'currency' => $price->getCurrency()
                    ]));
                    break;
                case ItemType::CROWD_FUNDING:
                    if ($item->getPrice() === null) {
                        $price = $this->assetService->getUserAssetPrice(
                            $item->getItem()->getAsset()->getAssetId(),
                            $date
                        );
                        if ($price === null) {
                            $price = new PriceModel([
                                'amount' => 0,
                                'currency' => 'RUB'
                            ]);
                        }
                    } else {
                        $price = $item->getPrice();
                    }
                    $item->setAmount($price->getAmount() * $item->getQuantity());
                    $item->setCurrencyIso($price->getCurrency());
                    $item->setPrice(new PriceModel([
                        'amount' => $price->getAmount(),
                        'currency' => $price->getCurrency()
                    ]));
                    break;
            }
        }

        /** Update prices for market items */
        if (
            $stockItemCollection->count() > 0
            && (
                $portfolio->getDate()?->format("Ymd") !== $date->format("Ymd")
                || $portfolio->isCached() === false
            )
        ) {
            $isinList = [];
            /** @var ItemModel $stock */
            foreach ($stockItemCollection as $stock) {
                $isinList[] = $stock->getStock()->getIsin();
            }
            $stockPrices = $this->candleService->getPricesIsin($isinList, $date);
            foreach ($items->getIterator() as $item) {
                $portfolioItemModel = $item->getItem();
                if ($portfolioItemModel->getType() === 'market') {
                    $isin = $portfolioItemModel->getStock()->getIsin();
                    foreach ($stockPrices as $externalId => $price) {
                        if ($isin === $externalId) {
                            if ($price === null) {
                                $item->setAmount(0);
                                $item->setCurrencyIso('RUB');
                                $item->setPrice(new PriceModel([
                                    'amount' => 0,
                                    'currency' => 'RUB'
                                ]));
                            } else {
                                $item->setAmount($price->getAmount() * $item->getQuantity());
                                $item->setCurrencyIso($price->getCurrency());
                                $item->setPrice(new PriceModel([
                                    'amount' => $price->getAmount(),
                                    'currency' => $price->getCurrency()
                                ]));
                            }
                            break;
                        }
                    }
                }
            }
        }

        return $items;
    }

    public function fillPortfolioBasePrice(
        PortfolioItemCollection $items,
        string $currency,
        \DateTime $date
    ): PortfolioItemCollection {
        $rate = [
            $currency => 1
        ];
        /** @var PortfolioItemModel $item */
        foreach ($items->getIterator() as $item) {
            $itemCurrency = $item->getCurrencyIso();
            if (!isset($rate[$itemCurrency])) {
                $rate[$itemCurrency] = $this->exchangeCurrencyService
                    ->getExchangeRate($itemCurrency, $currency, $date);
            }
            $item->setBaseAmount(
                new PriceModel([
                    'amount' => $rate[$itemCurrency] * $item->getAmount(),
                    'currency' => $currency
                ])
            );
        }

        return $items;
    }

    public function convertOperationToPortfolio(OperationModel $operation): PortfolioModel
    {
        $balanceCollection = new PortfolioBalanceCollection();
        $itemCollection = new PortfolioItemCollection();

        $operationType = OperationType::tryFrom($operation->getOperationType());
        if ($operation->getStatus() === BrokerOperationStatus::DONE->value) {
            if (
                in_array(
                    $operationType,
                    [
                        OperationType::INPUT,
                        OperationType::OUTPUT,
                        OperationType::REPAYMENT,
                        OperationType::DIVIDEND,
                        OperationType::BUY,
                        OperationType::SELL,
                        OperationType::TAX,
                        OperationType::FEE
                    ]
                )
            ) {
                $balance = new PortfolioBalanceModel([
                    'brokerId' => $operation->getBrokerId(),
                    'currencyIso' => $operation->getCurrencyIso(),
                    'amount' => $operation->getAmount()
                ]);
                $balanceCollection->add($balance);
            }

            if (
                in_array(
                    $operationType,
                    [
                        OperationType::REPAYMENT,
                        OperationType::BUY,
                        OperationType::BUY_CARD,
                        OperationType::SELL,
                        OperationType::SELL_CARD,
                        OperationType::DEFAULT,
                        OperationType::SECURITY_OUT,
                        OperationType::SECURITY_IN
                    ]
                )
            ) {
                $quantity = match ($operationType) {
                    OperationType::SELL,
                    OperationType::SELL_CARD,
                    OperationType::DEFAULT,
                    OperationType::SECURITY_OUT => (-1 * $operation->getQuantity()),
                    default => $operation->getQuantity()
                };

                $brokerId = 'tinkoff2';
                if ($operation->getItem()->getStock()?->getCurrencyModel() !== null) {
                    $balance = new PortfolioBalanceModel([
                        'brokerId' => $brokerId,
                        'currencyIso' => $operation->getItem()->getStock()?->getCurrencyModel()->getIso(),
                        'amount' => $quantity
                    ]);
                    $balanceCollection->add($balance);
                } else {
                    $item = new PortfolioItemModel([
                        'itemId' => $operation->getItemId(),
                        'item' => $operation->getItem(),
                        'currencyIso' => $operation->getCurrencyIso(),
                        'quantity' => $quantity ?? 0
                    ]);
                    $itemCollection->add($item);
                }
            }
        }
        return new PortfolioModel([
            'balance' => $balanceCollection,
            'items' => $itemCollection,
            'date' => $operation->getDate()
        ]);
    }

    /**
     * @param PortfolioModel[] $portfolios
     * @return PortfolioModel
     * @throws \Exception
     */
    public function mergePortfolios(array $portfolios): PortfolioModel
    {
        $result = new PortfolioModel([
            'balance' => new PortfolioBalanceCollection(),
            'items' => new PortfolioItemCollection()
        ]);

        foreach ($portfolios as $portfolio) {
            /** @var PortfolioBalanceModel $balance */
            foreach ($portfolio->getBalance()->getIterator() as $balance) {
                $resultBalance = $result->getBalance()
                    ->getByBrokerAndCurrency($balance->getBrokerId(), $balance->getCurrencyIso());

                if ($resultBalance === null) {
                    $result->getBalance()->add($balance);
                } else {
                    $resultBalance->setAmount($resultBalance->getAmount() + $balance->getAmount());
                }
            }

            /** @var PortfolioItemModel $item */
            foreach ($portfolio->getItems()->getIterator() as $item) {
                $resultItem = $result->getItems()->getByItemId($item->getItemId());

                if ($resultItem === null) {
                    $result->getItems()->add($item);
                } else {
                    $resultItem->setQuantity($resultItem->getQuantity() + $item->getQuantity());
                    if ($item->getItem()->getType() !== ItemTypes::MARKET) {
                        $resultItem->setPrice($item->getPrice());
                    }
                }
            }
        }

        return $result;
    }

    public function savePortfolio(int $userId, PortfolioModel $portfolio, \DateTime $date): void
    {
        $userPortfolio = new UserPortfolioEntity();
        $userPortfolio->setUserId($userId);
        $userPortfolio->setDate($date);
        $userPortfolio->setIsActive(1);
        $userPortfolio->setUserPortfolioBalance(new ArrayCollection());
        $userPortfolio->setUserPortfolioItems(new ArrayCollection());

        /** @var PortfolioBalanceModel $balance */
        foreach ($portfolio->getBalance()->getIterator() as $balance) {
            $userBalance = new UserPortfolioBalanceEntity();
            $userBalance->setBrokerId($balance->getBrokerId());
            $userBalance->setAmount($balance->getAmount());
            $userBalance->setCurrency($balance->getCurrencyIso());
            $userBalance->setUserPortfolio($userPortfolio);

            $userPortfolio->getUserPortfolioBalance()->add(clone $userBalance);
        }

        /** @var PortfolioItemModel $item */
        foreach ($portfolio->getItems()->getIterator() as $item) {
            $itemEntity = $this->itemStorage->findById($item->getItemId());

            $userItem = new UserPortfolioItemEntity();
            $userItem->setItemId($item->getItemId());
            $userItem->setItem($itemEntity);
            $userItem->setQuantity($item->getQuantity());
            $userItem->setAmount($item->getPrice()->getAmount());
            $userItem->setCurrency($item->getPrice()->getCurrency());
            $userItem->setUserPortfolio($userPortfolio);

            $userPortfolio->getUserPortfolioItems()->add(clone $userItem);
        }

        $this->userPortfolioStorage->addEntity($userPortfolio);
    }

    private function convertPortfolioEntityToModel(UserPortfolioEntity $portfolioEntity): PortfolioModel
    {
        $itemCollection = new PortfolioItemCollection();
        /** @var UserPortfolioItemEntity $itemEntity */
        foreach ($portfolioEntity->getUserPortfolioItems()->getIterator() as $itemEntity) {
            $itemCollection->add(new PortfolioItemModel([
                'itemId' => $itemEntity->getItemId(),
                'item' => $this->itemConverterService->convertItemEntityToModel($itemEntity->getItem()),
                'quantity' => $itemEntity->getQuantity(),
                'price' => new PriceModel([
                    'amount' => $itemEntity->getAmount(),
                    'currency' => $itemEntity->getCurrency()
                ]),
                'currencyIso' => $itemEntity->getCurrency(),
                'amount' => $itemEntity->getQuantity() * $itemEntity->getAmount()
            ]));
        }

        $balanceCollection = new PortfolioBalanceCollection();
        /** @var UserPortfolioBalanceEntity $balanceEntity */
        foreach ($portfolioEntity->getUserPortfolioBalance()->getIterator() as $balanceEntity) {
            $balanceCollection->add(new PortfolioBalanceModel([
                'brokerId' => $balanceEntity->getBrokerId(),
                'amount' => $balanceEntity->getAmount(),
                'currencyIso' => $balanceEntity->getCurrency()
            ]));
        }

        return new PortfolioModel([
            'balance' => $balanceCollection,
            'items' => $itemCollection,
            'date' => $portfolioEntity->getDate()
        ]);
    }

    public function setOutdatedPortfolio(int $userId, \DateTime $dateFrom, \DateTime $dateTo): void
    {
        $portfolioArray =
            $this->userPortfolioStorage->findByDateIntervalAndUser($userId, $dateFrom, $dateTo);
        $toUpdate = [];
        foreach ($portfolioArray as $portfolioEntity) {
            $portfolioEntity->setIsActive(0);
            $toUpdate[] = $portfolioEntity;
        }
        $this->userPortfolioStorage->addEntityArray($toUpdate);
    }

    public function getLastPortfolio(int $userId, \DateTime $date): ?PortfolioModel
    {
        $userPortfolio = $this->userPortfolioStorage->getLastPortfolio($userId, $date);
        if ($userPortfolio === null) {
            return null;
        }

        return $this->convertPortfolioEntityToModel($userPortfolio);
    }
}
