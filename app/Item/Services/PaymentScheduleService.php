<?php

declare(strict_types=1);

namespace App\Item\Services;

use App\Assets\Services\AssetService;
use App\Broker\Types\BrokerOperationStatus;
use App\Broker\Types\BrokerType;
use App\Deposit\Services\DepositService;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Collections\PaymentScheduleCollection;
use App\Item\Entities\ItemPaymentScheduleEntity;
use App\Item\Models\OperationModel;
use App\Item\Models\PaymentScheduleModel;
use App\Item\Models\PortfolioItemModel;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemType;
use App\Item\Types\OperationType;
use App\Loan\Services\LoanService;
use App\RealEstate\Services\RealEstateService;

class PaymentScheduleService
{
    public function __construct(
        private readonly ItemPaymentScheduleStorage $itemPaymentScheduleStorage,
        private readonly CurrencyService $currencyService,
        private readonly ItemService $itemService,
        private readonly DepositService $depositService,
        private readonly LoanService $loanService,
        private readonly RealEstateService $realEstateService,
        private readonly AssetService $assetService,
        private readonly PortfolioBuilderService $portfolioBuilderService
    ) {
    }

    public function getUserEventCalendar(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        ?array $assetTypes
    ): PaymentScheduleCollection {
        $schedule = new PaymentScheduleCollection();

        $portfolio = $this->portfolioBuilderService->buildBasePortfolio($userId, new \DateTime());

        if ($assetTypes !== null) {
            /** @var PortfolioItemModel $item */
            foreach ($portfolio->getItems()->getIterator() as $key => $item) {
                if (!in_array($item->getItem()->getType(), $assetTypes)) {
                    $portfolio->getItems()->remove($key);
                }
            }
        }

        $ids = $portfolio->getItems()->getItemIds();

        $scheduleEntities = $this->itemPaymentScheduleStorage->findActiveByItemIdsAndInterval($ids, $dateFrom, $dateTo);
        foreach ($scheduleEntities as $entity) {
            $scheduleModel = $this->convertEntityToModel($entity);

            $item = $portfolio->getItems()->getByItemId($scheduleModel->getItemId());
            $scheduleModel->setDebtAmount($scheduleModel->getDebtAmount() * $item->getQuantity());
            $scheduleModel->setInterestAmount($scheduleModel->getInterestAmount() * $item->getQuantity());

            $scheduleModel->setItem($this->itemService->getItemById($scheduleModel->getItemId()));
            $schedule->add($scheduleModel);
        }

        return $schedule;
    }

    public function getUserItemPaymentSchedule(
        int $userId,
        int $itemId,
        \DateTime $from,
        \DateTime $to
    ): OperationCollection {
        $operations = new OperationCollection();
        $item = $this->itemService->getItemById($itemId);

        switch ($item->getType()) {
            case ItemType::DEPOSIT->value:
                $operations = $this->depositService->calculateDepositPayout($userId, $item, $to);
                break;
            case ItemType::LOAN->value:
                $operations = $this->loanService->calculatePaymentSchedule($userId, $item, $to);
                break;
            default:
                $brokerId = null;
                if ($item->getType() === ItemType::MARKET->value) {
                    $brokerId = BrokerType::TINKOFF->value;
                } elseif ($item->getType() === ItemType::CROWD_FUNDING->value) {
                    $brokerId = match ($item->getAsset()->getPlatform()) {
                        DataSourceType::JETLEND->value => DataSourceType::JETLEND->value,
                        DataSourceType::MONEY_FRIENDS->value => DataSourceType::MONEY_FRIENDS->value,
                        DataSourceType::POTOK->value => DataSourceType::POTOK->value,
                        default => null
                    };
                }

                $paymentScheduleEntities = $this->itemPaymentScheduleStorage
                    ->findActiveByItemIdsAndInterval([$itemId], $from, $to);
                foreach ($paymentScheduleEntities as $paymentEntity) {
                    $payment = $this->convertEntityToModel($paymentEntity);

                    $operation = new OperationModel([
                        'userId' => $userId,
                        'brokerId' => $brokerId,
                        'itemId' => $itemId,
                        'item' => $item,
                        'createdDate' => new \DateTime(),
                        'date' => $paymentEntity->getDate(),
                        'status' => BrokerOperationStatus::DONE->value,
                    ]);

                    if ($payment->getInterestAmount() !== 0.0) {
                        $interestOperation = clone $operation;
                        $operationType = match ($item->getType()) {
                            'real_estate' => OperationType::DIVIDEND_CARD,
                            default => OperationType::DIVIDEND
                        };
                        $interestOperation->setOperationType($operationType->value);
                        $interestOperation->setAmount($payment->getInterestAmount());
                        $interestOperation->setCurrencyIso($payment->getCurrency());
                        $interestOperation->setExternalId(
                            md5(
                                $interestOperation->getDate()->getTimestamp()
                                . $userId
                                . $interestOperation->getAmount()
                                . $interestOperation->getOperationType()
                            )
                        );

                        $operations->add($interestOperation);
                    }

                    if ($payment->getDebtAmount() !== 0.0) {
                        $debtOperation = clone $operation;
                        $debtOperation->setOperationType(OperationType::REPAYMENT->value);
                        $debtOperation->setAmount($payment->getDebtAmount());
                        $debtOperation->setCurrencyIso($payment->getCurrency());
                        $debtOperation->setExternalId(
                            md5(
                                $debtOperation->getDate()->getTimestamp()
                                . $userId
                                . $debtOperation->getAmount()
                                . $debtOperation->getOperationType()
                            )
                        );

                        $operations->add($debtOperation);
                    }
                }
        }

        return $operations;
    }

    public function updateItemPaymentSchedule(int $itemId): void
    {
        $item = $this->itemService->getItemById($itemId);
        $dateTo = new \DateTime('2199-01-01 00:00:00');

        if ($item->isOutdated() || $item->isDeleted()) {
            $schedule = new PaymentScheduleCollection();
        } else {
            switch ($item->getType()) {
                case ItemType::DEPOSIT->value:
                    $operations = $this->depositService->calculateDepositPayout(
                        $item->getDeposit()->getUserId(),
                        $item,
                        $dateTo
                    );
                    $schedule = $this->convertOperationsToPayments($operations);
                    break;
                case ItemType::LOAN->value:
                    $operations = $this->loanService->calculatePaymentSchedule(
                        $item->getLoan()->getUserId(),
                        $item,
                        $dateTo
                    );
                    $schedule = $this->convertOperationsToPayments($operations);
                    break;
                case ItemType::REAL_ESTATE->value:
                    $operations = $this->realEstateService->buildPayoutSchedule($item);
                    $schedule = $this->convertOperationsToPayments($operations);
                    break;
                case ItemType::CROWD_FUNDING->value:
                    $schedule = $this->assetService->calculatePaymentSchedule($item);
                    break;
                default:
                    $schedule = new PaymentScheduleCollection();
            }
        }

        $this->setPaymentSchedule($itemId, $schedule);
    }

    public function setPaymentSchedule(int $itemId, PaymentScheduleCollection $paymentSchedule): void
    {
        $paymentScheduleEntities = $this->itemPaymentScheduleStorage->findActiveByItemId($itemId);

        $isTheSameSchedule = true;

        if ($paymentSchedule->count() !== count($paymentScheduleEntities)) {
            $isTheSameSchedule = false;
        }

        if ($isTheSameSchedule) {
            foreach ($paymentScheduleEntities as $entity) {
                $paymentScheduleModel = $this->convertEntityToModel($entity);

                if ($paymentSchedule->findEqualPayment($paymentScheduleModel) === null) {
                    $isTheSameSchedule = false;
                    break;
                }
            }
        }

        if (!$isTheSameSchedule) {
            foreach ($paymentScheduleEntities as $entity) {
                $entity->setIsActive(0);
            }

            /** @var PaymentScheduleModel $paymentItem */
            foreach ($paymentSchedule->getIterator() as $paymentItem) {
                $paymentScheduleEntities[] = $this->convertModelToEntity($paymentItem);
            }

            $this->itemPaymentScheduleStorage->addEntityArray($paymentScheduleEntities);
        }
    }

    private function convertEntityToModel(ItemPaymentScheduleEntity $entity): PaymentScheduleModel
    {
        $currency = $this->currencyService->getCurrencyById($entity->getCurrencyId());
        return new PaymentScheduleModel([
            'itemPaymentScheduleId' => $entity->getId(),
            'isActive' => $entity->getIsActive(),
            'itemId' => $entity->getItemId(),
            'interestAmount' => $entity->getInterestAmount(),
            'debtAmount' => $entity->getDebtAmount(),
            'date' => $entity->getDate(),
            'currency' => $currency->getIso()
        ]);
    }

    private function convertModelToEntity(PaymentScheduleModel $model): ItemPaymentScheduleEntity
    {
        $currency = $this->currencyService->getCurrencyByIso($model->getCurrency());

        $entity = new ItemPaymentScheduleEntity();
        $entity->setIsActive($model->getIsActive());
        $entity->setItemId($model->getItemId());
        $entity->setInterestAmount($model->getInterestAmount());
        $entity->setDebtAmount($model->getDebtAmount());
        $entity->setDate($model->getDate());
        $entity->setCurrency($currency);
        $entity->setCurrencyId($currency->getId());

        return $entity;
    }

    private function convertOperationToPayment(OperationModel $operation): PaymentScheduleModel
    {
        $operationType = OperationType::tryFrom($operation->getOperationType());
        return new PaymentScheduleModel([
            'itemId' => $operation->getItemId(),
            'interestAmount' => in_array(
                $operationType,
                [
                    OperationType::DIVIDEND,
                    OperationType::DIVIDEND_CARD
                ]
            ) ? $operation->getAmount() : 0,
            'debtAmount' => in_array(
                $operationType,
                [
                    OperationType::REPAYMENT,
                    OperationType::BUY_CARD,
                    OperationType::BUY,
                    OperationType::SELL,
                    OperationType::SELL_CARD
                ]
            ) ? $operation->getAmount() : 0,
            'currency' => $operation->getCurrencyIso(),
            'date' => $operation->getDate()
        ]);
    }

    public function convertOperationsToPayments(OperationCollection $collection): PaymentScheduleCollection
    {
        $paymentCollection = new PaymentScheduleCollection();
        /** @var OperationModel $operation */
        foreach ($collection->getIterator() as $operation) {
            $paymentCollection->add($this->convertOperationToPayment($operation));
        }

        return $paymentCollection;
    }
}
