<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Assets\Services\AssetService;
use App\Assets\Storages\PayoutTypeStorage;
use App\Broker\Services\BrokerService;
use App\Common\Amqp\AmqpClient;
use App\Deposit\Services\DepositService;
use App\Intl\Services\CurrencyService;
use App\Item\Services\ItemConverterService;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;
use App\Item\Services\PortfolioBuilderService;
use App\Item\Services\PortfolioService;
use App\Item\Services\Sources\SourceFactory;
use App\Item\Services\UserItemService;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Item\Storages\ItemPaymentScheduleStorage;
use App\Item\Storages\ItemStorage;
use App\Item\Storages\OperationStatusStorage;
use App\Item\Storages\OperationTypeStorage;
use App\Item\Storages\UserPortfolioStorage;
use App\Loan\Services\LoanService;
use App\Market\Services\MarketInstrumentHelperService;
use App\RealEstate\Services\RealEstateService;
use App\Market\Services\CandleDbStoreServiceDecorator;
use App\Services\ExchangeCurrencyService;
use App\Services\MarketOperationsService;
use Psr\Container\ContainerInterface;

$container->set(ItemService::class, function (ContainerInterface $c) {
    return new ItemService(
        $c->get(ItemStorage::class),
        $c->get(ItemConverterService::class),
        $c->get(AssetService::class)
    );
});

$container->set(ItemStorage::class, function (ContainerInterface $c) {
    return new ItemStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemOperationStorage::class, function (ContainerInterface $c) {
    return new ItemOperationStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemOperationImportStorage::class, function (ContainerInterface $c) {
    return new ItemOperationImportStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationTypeStorage::class, function (ContainerInterface $c) {
    return new OperationTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationStatusStorage::class, function (ContainerInterface $c) {
    return new OperationStatusStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(ItemPaymentScheduleStorage::class, function (ContainerInterface $c) {
    return new ItemPaymentScheduleStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(UserPortfolioStorage::class, function (ContainerInterface $c) {
    return new UserPortfolioStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationService::class, function (ContainerInterface $c) {
    return new OperationService(
        $c->get(ItemOperationStorage::class),
        $c->get(ItemStorage::class),
        $c->get(CurrencyService::class),
        $c->get(OperationTypeStorage::class),
        $c->get(OperationStatusStorage::class),
        $c->get(ExchangeCurrencyService::class),
        $c->get(ItemConverterService::class),
        $c->get(AmqpClient::class),
        $c->get(MarketOperationsService::class),
        $c->get(BrokerService::class),
        $c->get(SourceFactory::class)
    );
});

$container->set(SourceFactory::class, function () {
    return new SourceFactory();
});

$container->set(PortfolioService::class, function (ContainerInterface $c) {
    return new PortfolioService(
        $c->get('Now' . DateTime::class),
        $c->get(OperationService::class),
        $c->get(ItemPaymentScheduleStorage::class),
        $c->get(ExchangeCurrencyService::class),
        $c->get(ItemService::class),
        $c->get(PortfolioBuilderService::class)
    );
});

$container->set(PortfolioBuilderService::class, function (ContainerInterface $c) {
    return new PortfolioBuilderService(
        $c->get(UserPortfolioStorage::class),
        $c->get(OperationService::class),
        $c->get(RealEstateService::class),
        $c->get(DepositService::class),
        $c->get(LoanService::class),
        $c->get(AssetService::class),
        $c->get(CandleDbStoreServiceDecorator::class),
        $c->get(ExchangeCurrencyService::class),
        $c->get(ItemStorage::class),
        $c->get(ItemConverterService::class)
    );
});

$container->set(PaymentScheduleService::class, function (ContainerInterface $c) {
    return new PaymentScheduleService(
        $c->get(ItemPaymentScheduleStorage::class),
        $c->get(CurrencyService::class),
        $c->get(ItemService::class),
        $c->get(DepositService::class),
        $c->get(LoanService::class),
        $c->get(RealEstateService::class),
        $c->get(AssetService::class),
        $c->get(PortfolioBuilderService::class)
    );
});

$container->set(UserItemService::class, function (ContainerInterface $c) {
    return new UserItemService(
        $c->get('Now' . DateTime::class),
        $c->get(PortfolioService::class),
        $c->get(ExchangeCurrencyService::class),
        $c->get(OperationService::class)
    );
});

$container->set(ItemConverterService::class, function (ContainerInterface $c) {
    return new ItemConverterService(
        $c->get(MarketInstrumentHelperService::class),
        $c->get(CurrencyService::class),
        $c->get(PayoutTypeStorage::class)
    );
});
