<?php

declare(strict_types=1);

namespace App\Item\Exceptions;

use App\Common\BaseException;

class ItemBaseException extends BaseException
{
    protected static $CODE = 5000;
}
