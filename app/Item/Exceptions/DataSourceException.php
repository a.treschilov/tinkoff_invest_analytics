<?php

declare(strict_types=1);

namespace App\Item\Exceptions;

class DataSourceException extends ItemBaseException
{
    protected static $CODE = 5001;
}
