<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Collections\PortfolioItemCollection;

class PortfolioModel extends BaseModel
{
    private PortfolioBalanceCollection $balance;
    private PortfolioItemCollection $items;
    private ?\DateTime $date = null;
    private bool $isCached = true;

    /**
     * @return PortfolioBalanceCollection
     */
    public function getBalance(): PortfolioBalanceCollection
    {
        return $this->balance;
    }

    /**
     * @param PortfolioBalanceCollection $balance
     */
    public function setBalance(PortfolioBalanceCollection $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return PortfolioItemCollection
     */
    public function getItems(): PortfolioItemCollection
    {
        return $this->items;
    }

    /**
     * @param PortfolioItemCollection $items
     */
    public function setItems(PortfolioItemCollection $items): void
    {
        $this->items = $items;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return bool
     */
    public function isCached(): bool
    {
        return $this->isCached;
    }

    /**
     * @param bool $isCached
     */
    public function setIsCached(bool $isCached): void
    {
        $this->isCached = $isCached;
    }
}
