<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class OperationTypeModel extends BaseModel
{
    private int $operationTypeId;
    private string $externalId;
    private string $name;

    /**
     * @return int
     */
    public function getOperationTypeId(): int
    {
        return $this->operationTypeId;
    }

    /**
     * @param int $operationTypeId
     */
    public function setOperationTypeId(int $operationTypeId): void
    {
        $this->operationTypeId = $operationTypeId;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
