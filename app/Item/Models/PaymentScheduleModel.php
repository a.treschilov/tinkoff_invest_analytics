<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class PaymentScheduleModel extends BaseModel
{
    private ?int $itemPaymentScheduleId = null;
    private int $isActive = 1;
    private int $itemId;
    private ?ItemModel $item = null;
    private float $interestAmount;
    private float $debtAmount;
    private string $currency;
    private \DateTime $date;

    /**
     * @return int|null
     */
    public function getItemPaymentScheduleId(): ?int
    {
        return $this->itemPaymentScheduleId;
    }

    /**
     * @param int|null $itemPaymentScheduleId
     */
    public function setItemPaymentScheduleId(?int $itemPaymentScheduleId): void
    {
        $this->itemPaymentScheduleId = $itemPaymentScheduleId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemModel|null
     */
    public function getItem(): ?ItemModel
    {
        return $this->item;
    }

    /**
     * @param ItemModel|null $item
     */
    public function setItem(?ItemModel $item): void
    {
        $this->item = $item;
    }

    /**
     * @return float
     */
    public function getInterestAmount(): float
    {
        return $this->interestAmount;
    }

    /**
     * @param float $interestAmount
     */
    public function setInterestAmount(float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return float
     */
    public function getDebtAmount(): float
    {
        return $this->debtAmount;
    }

    /**
     * @param float $debtAmount
     */
    public function setDebtAmount(float $debtAmount): void
    {
        $this->debtAmount = $debtAmount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }
}
