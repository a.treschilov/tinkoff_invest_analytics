<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemTypes;

class ItemFilterModel extends BaseModel
{
    /** @var ItemTypes[] */
    public array $type = [];

    /**
     * @var string[]
     */
    public array $externalId = [];

    /**
     * @var bool[]
     */
    public array $isOutdated = [];

    /**
     * @var DataSourceType[]
     */
    public array $source = [];

    /**
     * @var bool[]
     */
    public array $isDeleted = [false];

    public function getType(): array
    {
        return $this->type;
    }

    public function setType(array $type): void
    {
        $this->type = $type;
    }

    public function getExternalId(): array
    {
        return $this->externalId;
    }

    public function setExternalId(array $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getIsOutdated(): array
    {
        return $this->isOutdated;
    }

    public function setIsOutdated(array $isOutdated): void
    {
        $this->isOutdated = $isOutdated;
    }

    public function getSource(): array
    {
        return $this->source;
    }

    public function setSource(array $source): void
    {
        $this->source = $source;
    }

    public function getIsDeleted(): array
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(array $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }
}
