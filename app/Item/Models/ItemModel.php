<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Assets\Models\AssetModel;
use App\Common\Models\BaseModel;
use App\Deposit\Models\DepositModel;
use App\Item\Types\ItemTypes;
use App\Loan\Models\LoanModel;
use App\Market\Models\MarketInstrumentModel;
use App\RealEstate\Models\RealEstateModel;

class ItemModel extends BaseModel
{
    private ?int $itemId = null;
    private string $type;
    private string $source;
    private ?string $externalId = null;
    private string $name;
    private ?string $logo = null;
    private bool $isOutdated = false;
    private bool $isDeleted = false;
    private ?AssetModel $asset = null;
    private ?MarketInstrumentModel $stock = null;
    private ?RealEstateModel $realEstate = null;
    private ?LoanModel $loan = null;
    private ?DepositModel $deposit = null;

    /**
     * @return int|null
     */
    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    /**
     * @param int|null $itemId
     */
    public function setItemId(?int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemTypes::DEPOSIT|ItemTypes::LOAN|ItemTypes::REAL_ESTATE|ItemTypes::MARKET|ItemTypes::CROWDFUNDING
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return AssetModel|null
     */
    public function getAsset(): ?AssetModel
    {
        return $this->asset;
    }

    /**
     * @param AssetModel|null $asset
     */
    public function setAsset(?AssetModel $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return MarketInstrumentModel|null
     */
    public function getStock(): ?MarketInstrumentModel
    {
        return $this->stock;
    }

    /**
     * @param MarketInstrumentModel|null $stock
     */
    public function setStock(?MarketInstrumentModel $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return RealEstateModel|null
     */
    public function getRealEstate(): ?RealEstateModel
    {
        return $this->realEstate;
    }

    /**
     * @param RealEstateModel|null $realEstate
     */
    public function setRealEstate(?RealEstateModel $realEstate): void
    {
        $this->realEstate = $realEstate;
    }

    /**
     * @return LoanModel|null
     */
    public function getLoan(): ?LoanModel
    {
        return $this->loan;
    }

    /**
     * @param LoanModel|null $loan
     */
    public function setLoan(?LoanModel $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @return DepositModel|null
     */
    public function getDeposit(): ?DepositModel
    {
        return $this->deposit;
    }

    /**
     * @param DepositModel|null $deposit
     */
    public function setDeposit(?DepositModel $deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted(bool $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function isOutdated(): bool
    {
        return $this->isOutdated;
    }

    public function setIsOutdated(bool $isOutdated): void
    {
        $this->isOutdated = $isOutdated;
    }
}
