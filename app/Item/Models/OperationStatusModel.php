<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class OperationStatusModel extends BaseModel
{
    private int $operationStatusId;
    private string $externalId;
    private string $name;

    /**
     * @return int
     */
    public function getOperationStatusId(): int
    {
        return $this->operationStatusId;
    }

    /**
     * @param int $operationStatusId
     */
    public function setOperationStatusId(int $operationStatusId): void
    {
        $this->operationStatusId = $operationStatusId;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
