<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;
use App\Models\PriceModel;

class PortfolioItemModel extends BaseModel
{
    private int $itemId;
    private float $quantity;
    private float $amount;
    private string $currencyIso;
    private ?PriceModel $price = null;
    private ?PriceModel $baseAmount = null;
    private ?ItemModel $item = null;
    private ?float $itemTypeShare = null;

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return ItemModel|null
     */
    public function getItem(): ?ItemModel
    {
        return $this->item;
    }

    /**
     * @param ItemModel|null $item
     */
    public function setItem(?ItemModel $item): void
    {
        $this->item = $item;
    }

    /**
     * @return PriceModel|null
     */
    public function getPrice(): ?PriceModel
    {
        return $this->price;
    }

    /**
     * @param PriceModel|null $price
     */
    public function setPrice(?PriceModel $price): void
    {
        $this->price = $price;
    }

    /**
     * @return PriceModel|null
     */
    public function getBaseAmount(): ?PriceModel
    {
        return $this->baseAmount;
    }

    /**
     * @param PriceModel|null $baseAmount
     */
    public function setBaseAmount(?PriceModel $baseAmount): void
    {
        $this->baseAmount = $baseAmount;
    }

    /**
     * @return float|null
     */
    public function getItemTypeShare(): ?float
    {
        return $this->itemTypeShare;
    }

    /**
     * @param float|null $itemTypeShare
     */
    public function setItemTypeShare(?float $itemTypeShare): void
    {
        $this->itemTypeShare = $itemTypeShare;
    }
}
