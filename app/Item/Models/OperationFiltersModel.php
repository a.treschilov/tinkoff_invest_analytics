<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class OperationFiltersModel extends BaseModel
{
    private array|null $operationType = null;
    private array|null $operationStatus = null;
    private array|null $brokerId = null;
    private \DateTime $dateFrom;
    private \DateTime $dateTo;
    private array|null $itemId = null;
    private int|null $isAnalyticProceed = null;
    private array|null $isDeleted = [0];

    public static $DEFAULT_DATE_FROM = '2018-01-01';
    private static $DEFAULT_DATE_TO = '';

    /**
     * @return array|null
     */
    public function getOperationType(): ?array
    {
        return $this->operationType;
    }

    /**
     * @param array|null $operationType
     */
    public function setOperationType(?array $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * @return array|null
     */
    public function getOperationStatus(): ?array
    {
        return $this->operationStatus;
    }

    /**
     * @param array|null $operationStatus
     */
    public function setOperationStatus(?array $operationStatus): void
    {
        $this->operationStatus = $operationStatus;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom(): \DateTime
    {
        return $this->dateFrom ?? new \DateTime(self::$DEFAULT_DATE_FROM);
    }

    /**
     * @param string|\DateTime $dateFrom
     */
    public function setDateFrom(string|\DateTime $dateFrom): void
    {
        if (is_string($dateFrom)) {
            $this->dateFrom = new \DateTime($dateFrom);
        } else {
            $this->dateFrom = $dateFrom;
        }
    }

    /**
     * @return string|\DateTime
     */
    public function getDateTo(): \DateTime
    {
        return $this->dateTo ?? new \DateTime(self::$DEFAULT_DATE_TO);
    }

    public function setDateTo(string|\DateTime $dateTo): void
    {
        if (is_string($dateTo)) {
            $this->dateTo = new \DateTime($dateTo);
        } else {
            $this->dateTo = $dateTo;
        }
    }

    /**
     * @return array|null
     */
    public function getBrokerId(): ?array
    {
        return $this->brokerId;
    }

    /**
     * @param array|null $brokerId
     */
    public function setBrokerId(?array $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return array|null
     */
    public function getItemId(): ?array
    {
        return $this->itemId;
    }

    /**
     * @param array|null $itemId
     */
    public function setItemId(?array $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int|null
     */
    public function getIsAnalyticProceed(): ?int
    {
        return $this->isAnalyticProceed;
    }

    /**
     * @param int|null $isAnalyticProceed
     */
    public function setIsAnalyticProceed(?int $isAnalyticProceed): void
    {
        $this->isAnalyticProceed = $isAnalyticProceed;
    }

    /**
     * @return array|null
     */
    public function getIsDeleted(): ?array
    {
        return $this->isDeleted;
    }

    /**
     * @param array|null $isDeleted
     */
    public function setIsDeleted(?array $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }
}
