<?php

declare(strict_types=1);

namespace App\Item\Models;

use App\Common\Models\BaseModel;

class PortfolioBalanceModel extends BaseModel
{
    private ?string $brokerId = null;
    private float $amount;
    private string $currencyIso;

    /**
     * @return string|null
     */
    public function getBrokerId(): ?string
    {
        return $this->brokerId;
    }

    /**
     * @param string|null $brokerId
     */
    public function setBrokerId(?string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }
}
