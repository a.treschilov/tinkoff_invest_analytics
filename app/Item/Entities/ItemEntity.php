<?php

declare(strict_types=1);

namespace App\Item\Entities;

use App\Assets\Entities\AssetsEntity;
use App\Deposit\Entities\DepositEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Loan\Entities\LoanEntity;
use App\RealEstate\Entities\RealEstateEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.item')]
#[ORM\Entity]
class ItemEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'item_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected ?int $id = null;

    #[ORM\Column(name: 'type', type: 'string')]
    protected string $type;

    #[ORM\Column(name: 'source', type: 'string')]
    protected string $source;

    #[ORM\Column(name: 'external_id', type: 'string')]
    protected ?string $externalId = null;

    #[ORM\Column(name: 'name', type: 'string')]
    protected string $name;

    #[ORM\Column(name: 'logo', type: 'string')]
    protected ?string $logo = null;

    #[ORM\Column(name: 'is_outdated', type: 'integer')]
    protected int $isOutdated = 0;

    #[ORM\Column(name: 'is_deleted', type: 'integer')]
    protected int $isDeleted = 0;

    #[ORM\OneToOne(
        targetEntity: MarketInstrumentEntity::class,
        mappedBy: 'item',
        cascade: ['persist'],
        fetch: 'LAZY'
    )]
    private ?MarketInstrumentEntity $marketInstrument = null;

    #[ORM\OneToOne(
        targetEntity: AssetsEntity::class,
        mappedBy: 'item',
        cascade: ['persist'],
        fetch: 'LAZY'
    )]
    private ?AssetsEntity $asset = null;

    #[ORM\OneToOne(targetEntity: RealEstateEntity::class, mappedBy: 'item', cascade: ['persist'], fetch: 'LAZY')]
    private ?RealEstateEntity $realEstate = null;

    #[ORM\OneToOne(targetEntity: LoanEntity::class, mappedBy: 'item', cascade: ['persist'], fetch: 'LAZY')]
    private ?LoanEntity $loan = null;

    #[ORM\OneToOne(targetEntity: DepositEntity::class, mappedBy: 'item', cascade: ['persist'], fetch: 'LAZY')]
    private ?DepositEntity $deposit = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return MarketInstrumentEntity|null
     */
    public function getMarketInstrument(): ?MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity|null $marketInstrument
     */
    public function setMarketInstrument(?MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return AssetsEntity|null
     */
    public function getAsset(): ?AssetsEntity
    {
        return $this->asset;
    }

    /**
     * @param AssetsEntity|null $asset
     */
    public function setAsset(?AssetsEntity $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return RealEstateEntity|null
     */
    public function getRealEstate(): ?RealEstateEntity
    {
        return $this->realEstate;
    }

    /**
     * @param RealEstateEntity|null $realEstate
     */
    public function setRealEstate(?RealEstateEntity $realEstate): void
    {
        $this->realEstate = $realEstate;
    }

    /**
     * @return LoanEntity|null
     */
    public function getLoan(): ?LoanEntity
    {
        return $this->loan;
    }

    /**
     * @param LoanEntity|null $loan
     */
    public function setLoan(?LoanEntity $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @return DepositEntity|null
     */
    public function getDeposit(): ?DepositEntity
    {
        return $this->deposit;
    }

    /**
     * @param DepositEntity|null $deposit
     */
    public function setDeposit(?DepositEntity $deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return int
     */
    public function getIsDeleted(): int
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted(int $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getIsOutdated(): int
    {
        return $this->isOutdated;
    }

    public function setIsOutdated(int $isOutdated): void
    {
        $this->isOutdated = $isOutdated;
    }
}
