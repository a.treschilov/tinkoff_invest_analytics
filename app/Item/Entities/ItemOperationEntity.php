<?php

declare(strict_types=1);

namespace App\Item\Entities;

use App\Broker\Entities\BrokerEntity;
use App\Entities\OperationTypeEntity;
use App\Intl\Entities\CurrencyEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.item_operations')]
#[ORM\Entity]
class ItemOperationEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'item_operation_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected ?int $id = null;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'broker_id', type: 'string')]
    protected ?string $brokerId = null;

    #[ORM\JoinColumn(name: 'broker_id', referencedColumnName: 'broker_id')]
    #[ORM\ManyToOne(targetEntity: BrokerEntity::class)]
    protected ?BrokerEntity $broker = null;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected ?int $itemId = null;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\ManyToOne(targetEntity: ItemEntity::class, cascade: ['persist'])]
    protected ?ItemEntity $item = null;

    #[ORM\Column(name: 'operation_type_id', type: 'integer')]
    protected int $operationTypeId;

    #[ORM\JoinColumn(name: 'operation_type_id', referencedColumnName: 'operation_type_id')]
    #[ORM\ManyToOne(targetEntity: \App\Entities\OperationTypeEntity::class)]
    protected OperationTypeEntity $operationType;

    #[ORM\Column(name: 'external_id', type: 'string')]
    protected ?string $externalId = null;

    #[ORM\Column(name: 'created_date', type: 'datetime')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'quantity', type: 'float', precision: 10, scale: 4)]
    protected ?float $quantity = null;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'amount', type: 'float')]
    protected ?float $amount = null;

    #[ORM\Column(name: 'currency_id', type: 'integer')]
    protected ?int $currencyId = null;

    #[ORM\Column(name: 'status', type: 'string')]
    protected string $status;

    #[ORM\Column(name: 'is_analytic_proceed', type: 'integer')]
    protected int $isAnalyticProceed = 0;

    #[ORM\Column(name: 'is_deleted', type: 'integer')]
    protected int $isDeleted = 0;

    #[ORM\JoinColumn(name: 'currency_id', referencedColumnName: 'currency_id')]
    #[ORM\ManyToOne(targetEntity: \App\Intl\Entities\CurrencyEntity::class)]
    protected ?CurrencyEntity $currency = null;

    #[ORM\Column(name: 'error_code', type: 'integer')]
    protected ?int $errorCode = null;

    public function __construct()
    {
        $currentDateTime = new \DateTime();
        $this->createdDate = $currentDateTime;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getBrokerId(): ?string
    {
        return $this->brokerId;
    }

    /**
     * @param string|null $brokerId
     */
    public function setBrokerId(?string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return BrokerEntity|null
     */
    public function getBroker(): ?BrokerEntity
    {
        return $this->broker;
    }

    /**
     * @param BrokerEntity|null $broker
     */
    public function setBroker(?BrokerEntity $broker): void
    {
        $this->broker = $broker;
    }

    /**
     * @return int|null
     */
    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    /**
     * @param int|null $itemId
     */
    public function setItemId(?int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return int
     */
    public function getOperationTypeId(): int
    {
        return $this->operationTypeId;
    }

    /**
     * @param int $operationTypeId
     */
    public function setOperationTypeId(int $operationTypeId): void
    {
        $this->operationTypeId = $operationTypeId;
    }

    /**
     * @return OperationTypeEntity
     */
    public function getOperationType(): OperationTypeEntity
    {
        return $this->operationType;
    }

    /**
     * @param OperationTypeEntity $operationType
     */
    public function setOperationType(OperationTypeEntity $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * @return ItemEntity|null
     */
    public function getItem(): ?ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity|null $item
     */
    public function setItem(?ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     */
    public function setQuantity(?float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return int|null
     */
    public function getCurrencyId(): ?int
    {
        return $this->currencyId;
    }

    /**
     * @param int|null $currencyId
     */
    public function setCurrencyId(?int $currencyId): void
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return CurrencyEntity|null
     */
    public function getCurrency(): ?CurrencyEntity
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEntity|null $currency
     */
    public function setCurrency(?CurrencyEntity $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getIsAnalyticProceed(): int
    {
        return $this->isAnalyticProceed;
    }

    /**
     * @param int $isAnalyticProceed
     */
    public function setIsAnalyticProceed(int $isAnalyticProceed): void
    {
        $this->isAnalyticProceed = $isAnalyticProceed;
    }

    /**
     * @return int
     */
    public function getIsDeleted(): int
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted(int $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }

    public function setErrorCode(?int $errorCode): void
    {
        $this->errorCode = $errorCode;
    }
}
