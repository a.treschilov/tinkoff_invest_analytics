<?php

declare(strict_types=1);

namespace App\Item\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.item_operations_import')]
#[ORM\Entity]
class ItemOperationsImportEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'item_operations_import_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'user_broker_account_id', type: 'integer')]
    protected int $userBrokerAccountId;

    #[ORM\Column(name: 'import_date_start', type: 'datetime')]
    protected \DateTime $importDateStart;

    #[ORM\Column(name: 'import_date_end', type: 'datetime')]
    protected \DateTime $importDateEnd;

    #[ORM\Column(name: 'created_date', type: 'datetime')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'added', type: 'integer')]
    protected int $added;

    #[ORM\Column(name: 'updated', type: 'integer')]
    protected int $updated;

    #[ORM\Column(name: 'skipped', type: 'integer')]
    protected int $skipped;

    public function __construct()
    {
        $currentDateTime = new \DateTime();
        $this->createdDate = $currentDateTime;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return \DateTime
     */
    public function getImportDateStart(): \DateTime
    {
        return $this->importDateStart;
    }

    /**
     * @param \DateTime $importDateStart
     */
    public function setImportDateStart(\DateTime $importDateStart): void
    {
        $this->importDateStart = $importDateStart;
    }

    /**
     * @return \DateTime
     */
    public function getImportDateEnd(): \DateTime
    {
        return $this->importDateEnd;
    }

    /**
     * @param \DateTime $importDateEnd
     */
    public function setImportDateEnd(\DateTime $importDateEnd): void
    {
        $this->importDateEnd = $importDateEnd;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getAdded(): int
    {
        return $this->added;
    }

    /**
     * @param int $added
     */
    public function setAdded(int $added): void
    {
        $this->added = $added;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getSkipped(): int
    {
        return $this->skipped;
    }

    /**
     * @param int $skipped
     */
    public function setSkipped(int $skipped): void
    {
        $this->skipped = $skipped;
    }

    public function getUserBrokerAccountId(): int
    {
        return $this->userBrokerAccountId;
    }

    public function setUserBrokerAccountId(int $userBrokerAccountId): void
    {
        $this->userBrokerAccountId = $userBrokerAccountId;
    }
}
