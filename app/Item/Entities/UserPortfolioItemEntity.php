<?php

declare(strict_types=1);

namespace App\Item\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.user_portfolio_item')]
#[ORM\Entity]
class UserPortfolioItemEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_portfolio_item_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_portfolio_id', type: 'integer')]
    protected int $userPortfolioId;

    #[ORM\JoinColumn(name: 'user_portfolio_id', referencedColumnName: 'user_portfolio_id')]
    #[ORM\ManyToOne(targetEntity: UserPortfolioEntity::class, cascade: ['persist'])]
    protected UserPortfolioEntity $userPortfolio;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\ManyToOne(targetEntity: ItemEntity::class, cascade: ['persist'], fetch: 'EAGER')]
    protected ItemEntity $item;

    #[ORM\Column(name: 'quantity', type: 'float')]
    protected float $quantity;

    #[ORM\Column(name: 'amount', type: 'float')]
    protected float $amount;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserPortfolioId(): int
    {
        return $this->userPortfolioId;
    }

    /**
     * @param int $userPortfolioId
     */
    public function setUserPortfolioId(int $userPortfolioId): void
    {
        $this->userPortfolioId = $userPortfolioId;
    }

    /**
     * @return UserPortfolioEntity
     */
    public function getUserPortfolio(): UserPortfolioEntity
    {
        return $this->userPortfolio;
    }

    /**
     * @param UserPortfolioEntity $userPortfolio
     */
    public function setUserPortfolio(UserPortfolioEntity $userPortfolio): void
    {
        $this->userPortfolio = $userPortfolio;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }
}
