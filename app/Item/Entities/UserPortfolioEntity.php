<?php

declare(strict_types=1);

namespace App\Item\Entities;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Table(name: 'tinkoff_invest.user_portfolio')]
#[ORM\Entity]
class UserPortfolioEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_portfolio_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    #[ORM\OneToMany(
        targetEntity: UserPortfolioItemEntity::class,
        mappedBy: 'userPortfolio',
        cascade: ['persist'],
        fetch: 'LAZY'
    )]
    private ?Collection $userPortfolioItems = null;

    #[ORM\OneToMany(
        targetEntity: UserPortfolioBalanceEntity::class,
        mappedBy: 'userPortfolio',
        cascade: ['persist'],
        fetch: 'LAZY'
    )]
    private ?Collection $userPortfolioBalance = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return Collection|null
     */
    public function getUserPortfolioItems(): ?Collection
    {
        return $this->userPortfolioItems;
    }

    /**
     * @param Collection|null $userPortfolioItems
     */
    public function setUserPortfolioItems(?Collection $userPortfolioItems): void
    {
        $this->userPortfolioItems = $userPortfolioItems;
    }

    /**
     * @return Collection|null
     */
    public function getUserPortfolioBalance(): ?Collection
    {
        return $this->userPortfolioBalance;
    }

    /**
     * @param Collection|null $userPortfolioBalance
     */
    public function setUserPortfolioBalance(?Collection $userPortfolioBalance): void
    {
        $this->userPortfolioBalance = $userPortfolioBalance;
    }
}
