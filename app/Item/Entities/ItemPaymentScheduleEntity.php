<?php

declare(strict_types=1);

namespace App\Item\Entities;

use App\Intl\Entities\CurrencyEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.item_payment_schedule')]
#[ORM\Entity]
class ItemPaymentScheduleEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'item_payment_schedule_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected ?int $id = null;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'interest_amount', type: 'float')]
    protected float $interestAmount;

    #[ORM\Column(name: 'debt_amount', type: 'float')]
    protected float $debtAmount;

    #[ORM\Column(name: 'currency_id', type: 'integer')]
    protected int $currencyId;

    #[ORM\JoinColumn(name: 'currency_id', referencedColumnName: 'currency_id')]
    #[ORM\ManyToOne(targetEntity: \App\Intl\Entities\CurrencyEntity::class)]
    protected CurrencyEntity $currency;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getInterestAmount(): float
    {
        return $this->interestAmount;
    }

    /**
     * @param float $interestAmount
     */
    public function setInterestAmount(float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return int
     */
    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    /**
     * @param int $currencyId
     */
    public function setCurrencyId(int $currencyId): void
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return CurrencyEntity
     */
    public function getCurrency(): CurrencyEntity
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEntity $currency
     */
    public function setCurrency(CurrencyEntity $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return float
     */
    public function getDebtAmount(): float
    {
        return $this->debtAmount;
    }

    /**
     * @param float $debtAmount
     */
    public function setDebtAmount(float $debtAmount): void
    {
        $this->debtAmount = $debtAmount;
    }
}
