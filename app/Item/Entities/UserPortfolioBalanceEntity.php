<?php

declare(strict_types=1);

namespace App\Item\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.user_portfolio_balance')]
#[ORM\Entity]
class UserPortfolioBalanceEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'user_portfolio_balance_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'user_portfolio_id', type: 'integer')]
    protected int $userPortfolioId;

    #[ORM\JoinColumn(name: 'user_portfolio_id', referencedColumnName: 'user_portfolio_id')]
    #[ORM\ManyToOne(targetEntity: UserPortfolioEntity::class, cascade: ['persist'])]
    protected UserPortfolioEntity $userPortfolio;

    #[ORM\Column(name: 'broker_id', type: 'string')]
    protected ?string $brokerId = null;

    #[ORM\Column(name: 'amount', type: 'float')]
    protected float $amount;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserPortfolioId(): int
    {
        return $this->userPortfolioId;
    }

    /**
     * @param int $userPortfolioId
     */
    public function setUserPortfolioId(int $userPortfolioId): void
    {
        $this->userPortfolioId = $userPortfolioId;
    }

    /**
     * @return UserPortfolioEntity
     */
    public function getUserPortfolio(): UserPortfolioEntity
    {
        return $this->userPortfolio;
    }

    /**
     * @param UserPortfolioEntity $userPortfolio
     */
    public function setUserPortfolio(UserPortfolioEntity $userPortfolio): void
    {
        $this->userPortfolio = $userPortfolio;
    }

    /**
     * @return string|null
     */
    public function getBrokerId(): ?string
    {
        return $this->brokerId;
    }

    /**
     * @param string|null $brokerId
     */
    public function setBrokerId(?string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }
}
