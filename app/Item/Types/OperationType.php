<?php

declare(strict_types=1);

namespace App\Item\Types;

enum OperationType: string
{
    case UNSPECIFIED = 'Unspecified'; // Тип операции не определен
    case INPUT = 'Input'; // Пополнение денежных средств
    case OUTPUT = 'Output'; // Вывод денежных средств
    case SECURITY_IN = 'SecurityIn'; // Пополнение счета ценными бумагами
    case SECURITY_OUT = 'SecurityOut'; // Вывод со счета ценных бумаг
    case BUY = 'Buy'; // Покупка ЦБ
    case BUY_CARD = 'BuyCard'; // Покупка ЦБ с карты
    case SELL = 'Sell'; // Продажа
    case SELL_CARD = 'SellCard'; // Продажа на внешний счет
    case REPAYMENT = 'Repayment'; // Полное/частичное погашение ценной бумаги
    case DIVIDEND = 'Dividend'; //Доход ценной бумаги
    case DIVIDEND_CARD = 'DividendCard'; //Доход ценной бумаги на внешний счет
    case TAX = 'Tax'; // Удержание налога
    case FEE = 'Fee'; // Удержание комиссии
    case HOLD = 'Hold'; // Резервирование средств
    case UN_HOLD = 'UnHold'; // Перевод средств из резерва
    case DEFAULT = 'Default'; //Дефолт
    case DEFAULT_PAYOUTS = 'DefaultPayouts'; //Выплаты после дефолта
}
