<?php

declare(strict_types=1);

namespace App\Item\Types;

enum DataSourceType: string
{
    case POTOK = 'potok'; // Potok - crowd landing platform
    case MONEY_FRIENDS = 'money_friends'; // Money friends - crowd landing platform
    case JETLEND = 'jetlend'; // Jetlend - crowd landing platform
}
