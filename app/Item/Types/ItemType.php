<?php

declare(strict_types=1);

namespace App\Item\Types;

enum ItemType: string
{
    case MARKET = 'market'; // фондовый рынок
    case REAL_ESTATE = 'real_estate'; // фондовый рынок
    case CROWD_FUNDING = 'crowdfunding'; // краудфандинг
    case LOAN = 'loan'; // кредит
    case DEPOSIT = 'deposit'; // банковский вклад
}
