<?php

declare(strict_types=1);

namespace App\Item\Types;

/** @deprecated use ItemType instead */
class ItemTypes
{
    public const MARKET = 'market'; // фондовый рынок
    public const REAL_ESTATE = 'real_estate'; // фондовый рынок
    public const CROWD_FUNDING = 'crowdfunding'; // краудфандинг
    public const LOAN = 'loan'; // кредит
    public const DEPOSIT = 'deposit'; // банковский вклад
}
