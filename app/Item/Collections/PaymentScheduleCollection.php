<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\PaymentScheduleModel;
use Doctrine\Common\Collections\ArrayCollection;

class PaymentScheduleCollection extends ArrayCollection
{
    public function findEqualPayment(PaymentScheduleModel $model): ?PaymentScheduleModel
    {
        /** @var PaymentScheduleModel $paymentSchedule */
        foreach ($this->getIterator() as $paymentSchedule) {
            if (
                0 === $paymentSchedule->getDate()->getTimestamp() - $model->getDate()->getTimestamp()
                && $paymentSchedule->getDebtAmount() === $model->getDebtAmount()
                && $paymentSchedule->getInterestAmount() === $model->getInterestAmount()
                && $paymentSchedule->getCurrency() === $model->getCurrency()
                && $paymentSchedule->getIsActive() === $model->getIsActive()
                && $paymentSchedule->getItemId() === $model->getItemId()
            ) {
                return $paymentSchedule;
            }
        }

        return null;
    }
}
