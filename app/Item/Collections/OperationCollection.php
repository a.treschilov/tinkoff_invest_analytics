<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\OperationModel;
use App\Item\Types\OperationType;
use Doctrine\Common\Collections\ArrayCollection;

class OperationCollection extends ArrayCollection
{
    /**
     * @param 'ASC'|'DESC' $order
     * @return OperationCollection
     * @throws \Exception
     */
    public function sortByDate(string $order = 'ASC'): OperationCollection
    {
        $firstReturn = match ($order) {
            'ASC' => 1,
            'DESC' => -1,
            default => throw new \Exception('Invalid `order` param')
        };

        $iterator = $this->getIterator();
        $iterator->uasort(function (OperationModel $op1, OperationModel $op2) use ($firstReturn) {
            if ($op1->getDate() === $op2->getDate()) {
                return 0;
            }

            return $op1->getDate() > $op2->getDate() ? $firstReturn : -1 * $firstReturn;
        });

        return new OperationCollection(iterator_to_array($iterator, false));
    }

    public function sortByDateAsc(): OperationCollection
    {
        return $this->sortByDate('ASC');
    }

    public function filterByMonth(\DateTime $date): OperationCollection
    {
        $filteredCollection = new OperationCollection();

        $startOfMonth = new \DateTime();
        $startOfMonth->setDate((int)$date->format('Y'), (int)$date->format('m'), 1)
            ->setTime(0, 0, 0);

        $endOfMonth = clone $startOfMonth;
        $endOfMonth->add(new \DateInterval('P1M'));

        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getDate() >= $startOfMonth && $operation->getDate() < $endOfMonth) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }

    public function filterByYear(\DateTime $date): OperationCollection
    {
        $filteredCollection = new OperationCollection();

        $startOfYear = new \DateTime();
        $startOfYear->setDate((int)$date->format('Y'), 1, 1)
            ->setTime(0, 0, 0);

        $endOfYear = clone $startOfYear;
        $endOfYear->add(new \DateInterval('P1Y'));

        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getDate() >= $startOfYear && $operation->getDate() < $endOfYear) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }

    public function filterByDateInterval(\DateTime $start, \DateTime $end): OperationCollection
    {
        $collection = new OperationCollection();
        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getDate() >= $start && $operation->getDate() <= $end) {
                $collection->add($operation);
            }
        }

        return $collection;
    }

    /**
     * @param OperationType[] $operationTypes
     * @return OperationCollection
     */
    public function filterByType(array $operationTypes): OperationCollection
    {
        $types = array_map(function (OperationType $type): string {
            return $type->value;
        }, $operationTypes);

        $collection = new OperationCollection();
        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if (in_array($operation->getOperationType(), $types)) {
                $collection->add($operation);
            }
        }

        return $collection;
    }

    public function getIds(): array
    {
        $operations = [];
        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            $operations[] = $operation->getItemOperationId();
        }

        return $operations;
    }
}
