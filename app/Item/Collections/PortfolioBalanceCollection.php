<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\PortfolioBalanceModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioBalanceCollection extends ArrayCollection
{
    public function getByBrokerAndCurrency(?string $broker, string $currency): ?PortfolioBalanceModel
    {
        /** @var PortfolioBalanceModel $balance */
        foreach ($this->getIterator() as $balance) {
            if ($balance->getBrokerId() === $broker && $balance->getCurrencyIso() === $currency) {
                return $balance;
            }
        }

        return null;
    }
}
