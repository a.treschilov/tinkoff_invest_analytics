<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\OperationTypeModel;
use Doctrine\Common\Collections\ArrayCollection;

class OperationTypeCollection extends ArrayCollection
{
}
