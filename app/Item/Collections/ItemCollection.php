<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\ItemModel;
use Doctrine\Common\Collections\ArrayCollection;

class ItemCollection extends ArrayCollection
{
    public function findById(int $itemId): ?ItemModel
    {
        /** @var ItemModel $item */
        foreach ($this->getIterator() as $item) {
            if ($item->getItemId() === $itemId) {
                return $item;
            }
        }

        return null;
    }

    public function findByExternalIdAndSource(string $externalId, string $source): ItemCollection
    {
        $collection = new ItemCollection();
        foreach ($this->getIterator() as $item) {
            if ($item->getExternalId() === $externalId && $item->getSource() === $source) {
                $collection->add($item);
            }
        }

        return $collection;
    }

    /**
     * @return int[]
     * @throws \Exception
     */
    public function getIds(): array
    {
        $ids = [];
        /** @var ItemModel $item */
        foreach ($this->getIterator() as $item) {
            $ids[] = $item->getItemId();
        }

        return $ids;
    }
}
