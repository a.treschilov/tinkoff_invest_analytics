<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\PortfolioItemModel;
use Doctrine\Common\Collections\ArrayCollection;

class PortfolioItemCollection extends ArrayCollection
{
    public function getByItemId(int $itemId): ?PortfolioItemModel
    {
        /** @var PortfolioItemModel $item */
        foreach ($this->getIterator() as $item) {
            if ($item->getItemId() === $itemId) {
                return $item;
            }
        }

        return null;
    }

    public function getItemIds(): array
    {
        $ids = [];
        /** @var PortfolioItemModel $item */
        foreach ($this->getIterator() as $item) {
            $ids[] = $item->getItemId();
        }

        return $ids;
    }

    public function filterNonZeroItems(): PortfolioItemCollection
    {
        $itemCollection = new PortfolioItemCollection();
        /** @var PortfolioItemModel $item */
        foreach ($this->getIterator() as $item) {
            if ($item->getQuantity() !== 0.0) {
                $itemCollection->add($item);
            }
        }

        return $itemCollection;
    }
}
