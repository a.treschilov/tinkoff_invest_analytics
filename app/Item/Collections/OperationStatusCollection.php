<?php

declare(strict_types=1);

namespace App\Item\Collections;

use App\Item\Models\OperationStatusModel;
use Doctrine\Common\Collections\ArrayCollection;

class OperationStatusCollection extends ArrayCollection
{
    public function getById(int $id): ?OperationStatusModel
    {
        /** @var OperationStatusModel $status */
        foreach ($this->getIterator() as $status) {
            if ($status->getOperationStatusId() === $id) {
                return $status;
            }
        }

        return null;
    }
}
