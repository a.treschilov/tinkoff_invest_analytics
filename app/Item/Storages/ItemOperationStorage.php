<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemOperationEntity;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManagerInterface;

class ItemOperationStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemOperationEntity $itemEntity): int
    {
        $this->entityManager->persist($itemEntity);
        $this->entityManager->flush();

        return $itemEntity->getId();
    }

    /**
     * @param ItemOperationEntity[] $items
     * @return ItemOperationEntity[]
     */
    public function addEntityArray(array $items): array
    {
        foreach ($items as $itemEntity) {
            $this->entityManager->persist($itemEntity);
        }
        $this->entityManager->flush();

        return $items;
    }

    public function findByExternalIdAndBrokerAndType(
        string $externalId,
        string $brokerId,
        int $typeId
    ): ?ItemOperationEntity {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findOneBy([
                'isDeleted' => 0,
                'externalId' => $externalId,
                'brokerId' => $brokerId,
                'operationTypeId' => $typeId,
            ]);
    }

    public function findByUserItemAmountDateType(
        int $userId,
        string $brokerId,
        int $itemId,
        \DateTime $date,
        int $typeId,
        float $amount
    ): ?ItemOperationEntity {
        $entities = $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy([
                'isDeleted' => 0,
                'userId' => $userId,
                'brokerId' => $brokerId,
                'itemId' => $itemId,
                'date' => $date,
                'operationTypeId' => $typeId,
                'errorCode' => null,
            ]);

        /** @var ItemOperationEntity $operation */
        foreach ($entities as $operation) {
            if (sha1($operation->getAmount() . '') === sha1($amount . '')) {
                return $operation;
            }
        }

        return null;
    }

    public function findByUserAndAmountAndDate(
        int $userId,
        float $amount,
        \DateTime $date,
        string $brokerId
    ): ?ItemOperationEntity {
        $entities = $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy([
                'isDeleted' => 0,
                'userId' => $userId,
                'date' => $date,
                'brokerId' => $brokerId,
                'errorCode' => null,
            ]);

        /** @var ItemOperationEntity $operation */
        foreach ($entities as $operation) {
            if (sha1($operation->getAmount() . '') === sha1($amount . '')) {
                return $operation;
            }
        }

        return null;
    }

    /**
     * @param int $userId
     * @return ItemOperationEntity[]
     */
    public function findByUser(int $userId): array
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy(['isDeleted' => 0, 'userId' => $userId, 'errorCode' => null]);
    }

    /**
     * @param int $userId
     * @param \DateTime $start
     * @param \DateTime $end
     * @return ItemOperationEntity[]
     */
    public function findByUserAndDateInterval(int $userId, \DateTime $start, \DateTime $end): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId))
            ->andWhere(Criteria::expr()->eq('isDeleted', 0))
            ->andWhere(Criteria::expr()->gte('date', $start))
            ->andWhere(Criteria::expr()->lte('date', $end))
            ->andWhere(Criteria::expr()->eq('errorCode', null));
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->matching($criteria)->toArray();
    }

    /**
     * @param int|null $userId
     * @param array|null $itemId
     * @param \DateTime|null $start
     * @param \DateTime|null $end
     * @param array $operationTypes
     * @param array $operationStatuses
     * @param array $brokers
     * @param int|null $isAnalyticProceed
     * @param array|null $isDeleted
     * @return AbstractLazyCollection
     */
    public function findByUserAndFilters(
        ?int $userId = null,
        ?array $itemId = null,
        ?\DateTime $start = null,
        ?\DateTime $end = null,
        array $operationTypes = [],
        array $operationStatuses = [],
        array $brokers = [],
        ?int $isAnalyticProceed = null,
        ?array $isDeleted = null,
    ): AbstractLazyCollection {
        $criteria = new Criteria();

        if ($userId === null) {
            $criteria->where(Criteria::expr()->gt('userId', 0));
        } else {
            $criteria->where(Criteria::expr()->eq('userId', $userId));
        }

        $criteria->andWhere(Criteria::expr()->eq('errorCode', null));

        if ($isDeleted !== null) {
            $criteria->andWhere(Criteria::expr()->in('isDeleted', $isDeleted));
        }

        if ($start !== null) {
            $criteria->andWhere(Criteria::expr()->gte('date', $start));
        }
        if ($end !== null) {
            $criteria->andWhere(Criteria::expr()->lt('date', $end));
        }
        if (count($operationTypes) > 0) {
            $criteria->andWhere(Criteria::expr()->in('operationType', $operationTypes));
        }
        if (count($operationStatuses) > 0) {
            $criteria->andWhere(Criteria::expr()->in('status', $operationStatuses));
        }
        if (count($brokers) > 0) {
            $criteria->andWhere(Criteria::expr()->in('brokerId', $brokers));
        }
        if ($itemId !== null && count($itemId) > 0) {
            $criteria->andWhere(Criteria::expr()->in('itemId', $itemId));
        }

        if ($isAnalyticProceed !== null) {
            $criteria->andWhere(Criteria::expr()->eq('isAnalyticProceed', $isAnalyticProceed));
        }

        $criteria->orderBy(['date' => Order::Descending]);

        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->matching($criteria);
    }

    public function findActiveById(int $id): ?ItemOperationEntity
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findOneBy(['isDeleted' => 0, 'id' => $id, 'errorCode' => null]);
    }

    public function findById(int $id): ?ItemOperationEntity
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param int[] $ids
     * @return ItemOperationEntity[]
     */
    public function findByIds(array $ids): array
    {
        return $this->entityManager->getRepository(ItemOperationEntity::class)
            ->findBy(['id' => $ids]);
    }
}
