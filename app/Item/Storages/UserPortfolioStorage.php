<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\UserPortfolioEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class UserPortfolioStorage
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param int $userId
     * @param \DateTime $date
     * @return UserPortfolioEntity|null
     */
    public function getLastPortfolio(int $userId, \DateTime $date): ?UserPortfolioEntity
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId))
            ->andWhere(Criteria::expr()->eq('isActive', 1))
            ->andWhere(Criteria::expr()->lt('date', $date))
            ->orderBy(["date" => "DESC"])
            ->setMaxResults(1)
            ->getFirstResult();

        $portfolio = $this->entityManager->getRepository(UserPortfolioEntity::class)
            ->matching($criteria)->first();

        return $portfolio ?: null;
    }

    public function addEntity(UserPortfolioEntity $entity): UserPortfolioEntity
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    public function addEntityArray(array $entityArray): void
    {
        foreach ($entityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    /**
     * @param int $userId
     * @param \DateTime $from
     * @param \DateTime $to
     * @return UserPortfolioEntity[]
     */
    public function findByDateIntervalAndUser(int $userId, \DateTime $from, \DateTime $to): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId));
        $criteria->andWhere(Criteria::expr()->gte('date', $from));
        $criteria->andWhere(Criteria::expr()->lte('date', $to));

        return $this->entityManager->getRepository(UserPortfolioEntity::class)
            ->matching($criteria)->toArray();
    }
}
