<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemFilterModel;
use App\Item\Types\DataSourceType;
use App\Item\Types\ItemType;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class ItemStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemEntity $itemEntity): int
    {
        $this->entityManager->persist($itemEntity);
        $this->entityManager->flush();

        return $itemEntity->getId();
    }

    /**
     * @param ItemEntity[] $items
     * @return ItemEntity[]
     */
    public function addEntityArray(array $items): array
    {
        foreach ($items as $itemEntity) {
            $this->entityManager->persist($itemEntity);
        }
        $this->entityManager->flush();

        return $items;
    }

    public function findById(int $id): ?ItemEntity
    {
        return $this->entityManager->getRepository(ItemEntity::class)
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param int[] $ids
     * @return ItemEntity[]
     */
    public function findByIds(array $ids): array
    {
        return $this->entityManager->getRepository(ItemEntity::class)
            ->findBy(['id' => $ids]);
    }

    public function findActiveByExternalId(string $externalId): ?ItemEntity
    {
        return $this->entityManager->getRepository(ItemEntity::class)
            ->findOneBy(['externalId' => $externalId, 'isDeleted' => 0]);
    }

    /**
     * @param ItemFilterModel $filter
     * @return ItemEntity[]
     */
    public function findItem(ItemFilterModel $filter): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->gt('id', 0));

        if (count($filter->getType()) > 0) {
            $types = array_map(function (ItemType $type) {
                return $type->value;
            }, $filter->getType());
            $criteria->andWhere(Criteria::expr()->in('type', $types));
        }

        if (count($filter->getExternalId()) > 0) {
            $criteria->andWhere(Criteria::expr()->in('externalId', $filter->getExternalId()));
        }

        if (count($filter->getIsOutdated()) > 0) {
            $outDated = [];
            foreach ($filter->getIsOutdated() as $outDatedItem) {
                $outDated[] = $outDatedItem ? 1 : 0;
            }
            $criteria->andWhere(Criteria::expr()->in('isOutdated', $outDated));
        }

        if (count($filter->getIsDeleted()) > 0) {
            $isDeleted = [];
            foreach ($filter->getIsDeleted() as $isDeletedItem) {
                $isDeleted[] = $isDeletedItem ? 1 : 0;
            }
            $criteria->andWhere(Criteria::expr()->in('isDeleted', $isDeleted));
        }

        if (count($filter->source) > 0) {
            $sources = array_map(function (DataSourceType $source) {
                return $source->value;
            }, $filter->source);
            $criteria->andWhere(Criteria::expr()->in('source', $sources));
        }

        return $this->entityManager->getRepository(ItemEntity::class)->matching($criteria)->toArray();
    }
}
