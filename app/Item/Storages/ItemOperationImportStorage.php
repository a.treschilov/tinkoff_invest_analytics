<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemOperationsImportEntity;
use Doctrine\ORM\EntityManagerInterface;

class ItemOperationImportStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemOperationsImportEntity $entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    public function findByUserAndBroker(int $userId, string $brokerId): ItemOperationsImportEntity|null
    {
        return $this->entityManager->getRepository(ItemOperationsImportEntity::class)
            ->findOneBy(['userId' => $userId, 'brokerId' => $brokerId], ['importDateEnd' => "DESC"]);
    }

    public function findByUserBrokerAccount(int $userBrokerAccountId): ItemOperationsImportEntity|null
    {
        return $this->entityManager->getRepository(ItemOperationsImportEntity::class)
            ->findOneBy(['userBrokerAccountId' => $userBrokerAccountId], ['importDateEnd' => "DESC"]);
    }
}
