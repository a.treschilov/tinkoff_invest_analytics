<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Item\Entities\ItemPaymentScheduleEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class ItemPaymentScheduleStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(ItemPaymentScheduleEntity $itemPaymentScheduleEntity): void
    {
        $this->entityManager->persist($itemPaymentScheduleEntity);
        $this->entityManager->flush();
    }

    /**
     * @param ItemPaymentScheduleEntity[] $entities
     * @return void
     */
    public function addEntityArray(array $entities): void
    {
        foreach ($entities as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    /**
     * @param int $itemId
     * @return ItemPaymentScheduleEntity[]
     */
    public function findActiveByItemId(int $itemId): array
    {
        return $this->entityManager->getRepository(ItemPaymentScheduleEntity::class)
            ->findBy(['itemId' => $itemId, 'isActive' => 1]);
    }

    /**
     * @param array $itemIds
     * @param \DateTime $start
     * @param \DateTime $end
     * @return ItemPaymentScheduleEntity[]
     */
    public function findActiveByItemIdsAndInterval(array $itemIds, \DateTime $start, \DateTime $end): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->in('itemId', $itemIds))
            ->andWhere(Criteria::expr()->eq('isActive', 1))
            ->andWhere(Criteria::expr()->gte('date', $start))
            ->andWhere(Criteria::expr()->lte('date', $end));
        return $this->entityManager->getRepository(ItemPaymentScheduleEntity::class)
            ->matching($criteria)->toArray();
    }

    /**
     * @param int $userId
     * @param \DateTime $start
     * @param \DateTime $end
     * @return ItemPaymentScheduleEntity[]
     */
    public function findActiveByItemIdAndDateInterval(int $itemId, \DateTime $start, \DateTime $end): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('itemId', $itemId))
            ->andWhere(Criteria::expr()->eq('isActive', 1))
            ->andWhere(Criteria::expr()->gte('date', $start))
            ->andWhere(Criteria::expr()->lte('date', $end));
        return $this->entityManager->getRepository(ItemPaymentScheduleEntity::class)
            ->matching($criteria)->toArray();
    }
}
