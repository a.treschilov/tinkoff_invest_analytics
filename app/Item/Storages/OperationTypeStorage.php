<?php

declare(strict_types=1);

namespace App\Item\Storages;

use App\Entities\OperationTypeEntity;
use Doctrine\ORM\EntityManagerInterface;

class OperationTypeStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return OperationTypeEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(OperationTypeEntity::class)
            ->findAll();
    }

    public function findByExternalId(string $externalId): ?OperationTypeEntity
    {
        return $this->entityManager->getRepository(OperationTypeEntity::class)
            ->findOneBy(['externalId' => $externalId]);
    }
}
