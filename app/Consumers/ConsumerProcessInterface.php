<?php

declare(strict_types=1);

namespace App\Consumers;

interface ConsumerProcessInterface
{
    public function process(array $data): array;
}
