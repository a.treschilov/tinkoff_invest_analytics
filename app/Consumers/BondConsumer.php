<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Exceptions\ConsumerException;
use App\Market\Services\MarketInstrumentImportService;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

class BondConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly MarketInstrumentImportService $marketInstrumentImportService,
    ) {
    }
    public function process(array $data): array
    {
        try {
            $itemId = $this->marketInstrumentImportService->createOrUpdate($data['isin']);
        } catch (TIException $e) {
            if (in_array($e->getCode(), [70001, 70002, 70003])) {
                throw new ConsumerException('Tinkoff invest temporary error', 2000);
            } else {
                throw $e;
            }
        }

        return [
            'item_id' => $itemId,
            'isin' => $data['isin']
        ];
    }
}
