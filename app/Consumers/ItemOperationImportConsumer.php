<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Exceptions\ConsumerException;
use App\Interfaces\MarketOperationServiceInterface;
use App\Models\OperationFiltersModel;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

class ItemOperationImportConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly UserService $userService,
        private readonly MarketOperationServiceInterface $operationService
    ) {
    }

    public function process(array $data): array
    {
        $user = $this->userService->getActiveUserById($data['userId']);
        $this->userService->setUser($user);

        $filter = new OperationFiltersModel([
            'dateFrom' => new \DateTime($data['dateFrom']),
            'dateTo' => new \DateTime($data['dateTo']),
        ]);
        $userBrokerAccountId = $data['userBrokerAccountId'];

        try {
            $statistic = $this->operationService->importBrokerOperations($filter, $userBrokerAccountId);
        } catch (TIException $e) {
            if (in_array($e->getCode(), [70001, 70002, 70003])) {
                throw new ConsumerException('Tinkoff invest temporary error', 2000);
            } else {
                throw $e;
            }
        }

        return [
            'added' => $statistic->getAdded(),
            'updated' => $statistic->getUpdated(),
            'skipped' => $statistic->getSkipped()
        ];
    }
}
