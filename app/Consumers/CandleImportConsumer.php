<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Exceptions\ConsumerException;
use App\Market\Entities\CandlesImportEntity;
use App\Market\Interfaces\CandleServiceInterface;
use App\Market\Storages\CandleImportStorage;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

readonly class CandleImportConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private UserService $userService,
        private CandleServiceInterface $candleService,
        private CandleImportStorage $candleImportStorage,
    ) {
    }

    public function process(array $data): array
    {
        $now = new \DateTime();
        $user = $this->userService->getActiveUserById(1);
        $this->userService->setUser($user);

        $from = clone $now;
        $from->setTimestamp($data['timeStart']);
        $to = clone $now;
        $to->setTimestamp($data['timeEnd']);

        try {
            $candles = $this->candleService->getDayCandleInterval(
                $data['marketInstrumentId'],
                $from,
                $to
            );
        } catch (TIException $e) {
            if (in_array($e->getCode(), [70001, 70002, 70003])) {
                throw new ConsumerException('Tinkoff invest temporary error', 2000);
            } else {
                throw $e;
            }
        }

        $candleImport = new CandlesImportEntity();
        $candleImport->setMarketInstrumentId($data['marketInstrumentId']);
        $candleImport->setCreatedDate(clone $now);
        $candleImport->setUpdated(0);
        $candleImport->setSkipped(0);
        $candleImport->setAdded($candles->count());
        $candleImport->setImportDateStart($from);
        $candleImport->setImportDateEnd($to);
        $this->candleImportStorage->addEntity($candleImport);

        return [
            'from' => $from->format('Y-m-d'),
            'to' => $to->format('Y-m-d'),
            'added' => $candleImport->getAdded()
        ];
    }
}
