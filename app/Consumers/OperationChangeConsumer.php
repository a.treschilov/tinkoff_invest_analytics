<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;

readonly class OperationChangeConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private OperationService $operationService,
        private PaymentScheduleService $paymentScheduleService,
        private ItemService $itemService
    ) {
    }

    public function process(array $data): array
    {
        $operationId = $data['operationId'];

        $operation = $this->operationService->getOperationById($operationId);

        if ($operation->getItemId() !== null) {
            $this->paymentScheduleService->updateItemPaymentSchedule($operation->getItemId());
            $this->itemService->updateItemPrice(
                $operation->getUserId(),
                $operation->getItemId(),
                $operation->getDate()
            );
        }

        return [
            'operationId' => $operationId
        ];
    }
}
