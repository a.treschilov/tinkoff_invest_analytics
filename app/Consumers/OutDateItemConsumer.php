<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Item\Services\ItemService;
use App\Item\Services\PortfolioService;
use App\User\Entities\UserEntity;
use App\User\Services\UserService;

class OutDateItemConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly PortfolioService $portfolioService,
        private readonly UserService $userService,
        private readonly ItemService $itemService
    ) {
    }

    public function process(array $data): array
    {
        $itemId = $data['itemId'];

        $result = [];

        $users = $this->userService->getUserList();
        /** @var UserEntity $user */
        foreach ($users->getIterator() as $user) {
            if ($user->getIsNewUser() === 0) {
                $this->userService->setUser($user);

                $quantity = $this->portfolioService->securityOutPortfolio($user->getId(), $itemId);

                $result[] = [
                    'userId' => $user->getId(),
                    'itemId' => $itemId,
                    'quantity' => $quantity
                ];
            }
        }

        $this->itemService->setOutdatedItem($itemId, true);

        return $result;
    }
}
