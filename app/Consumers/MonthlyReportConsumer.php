<?php

declare(strict_types=1);

namespace App\Consumers;

use App\User\Services\UserReportService;
use App\User\Services\UserService;

class MonthlyReportConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly UserService $userService,
        private readonly UserReportService $userReportService
    ) {
    }

    public function process(array $data): array
    {
        $user = $this->userService->getActiveUserById($data['userId']);
        $this->userService->setUser($user);

        $date = new \DateTime();
        $date->setTimestamp($data['date']);

        $this->userReportService->sendMonthlyReport($user, $date);

        return [
            'userId' => $this->userService->getUser()->getId()
        ];
    }
}
