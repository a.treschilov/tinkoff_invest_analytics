<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Exceptions\ConsumerException;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

class MarketSourceConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly UserService $userService,
        private readonly FillMarketInstrumentServiceDecorator $marketInstrumentService
    ) {
    }
    public function process(array $data): array
    {
        $user = $this->userService->getActiveUserById($data['userId']);
        $this->userService->setUser($user);

        try {
            $instrument = $this->marketInstrumentService->updateInstrument($data['broker'], $data['instrumentId']);
        } catch (TIException $e) {
            if (in_array($e->getCode(), [70001, 70002, 70003])) {
                throw new ConsumerException('Tinkoff invest temporary error', 2000);
            } else {
                throw $e;
            }
        }

        if ($instrument === null) {
            return [
                'result' => 'Instruments is not found'
            ];
        }

        return [
            'isin' => $instrument?->getIsin()
        ];
    }
}
