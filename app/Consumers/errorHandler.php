<?php

declare(strict_types=1);

use DI\Container;
use App\Common\Logger\Factory as LoggerFactory;

function fatalErrorHandler(): void
{
    $container = new Container();

    require __DIR__ . '/../Common/dependencies.php';

    /** @var LoggerFactory $factory */
    $factory =  $container->get(LoggerFactory::class);
    $logger = $factory->create('consumer');

    $logger->error('Consumer is broken with fatal error', ['error' => error_get_last()]);
}
