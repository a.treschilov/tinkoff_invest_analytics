<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Item\Models\OperationFiltersModel;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;

readonly class UpdateItemDataConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private OperationService $operationService,
        private PaymentScheduleService $paymentScheduleService,
        private ItemService $itemService
    ) {
    }
    #[\Override] public function process(array $data): array
    {
        $itemId = $data['itemId'];
        $startDate = new \DateTime('2010-01-01');

        $this->paymentScheduleService->updateItemPaymentSchedule($itemId);

        $filter = new OperationFiltersModel([
            'itemId' => [$itemId]
        ]);
        $operations = $this->operationService->getFilteredUserOperation(null, $filter);
        if (count($operations) > 0) {
            $operation = $operations->first();
            $this->itemService->updateItemPrice(
                $operation->getUserId(),
                $itemId,
                $startDate
            );
        }

        return [
            'itemId' => $itemId
        ];
    }
}
