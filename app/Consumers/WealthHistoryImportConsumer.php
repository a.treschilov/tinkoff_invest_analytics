<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Exceptions\ConsumerException;
use App\Services\AnalyticsService;
use App\User\Services\UserService;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

class WealthHistoryImportConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private readonly AnalyticsService $analyticsService,
        private readonly UserService $userService
    ) {
    }

    public function process(array $data): array
    {
        $user = $this->userService->getActiveUserById($data['userId']);
        $this->userService->setUser($user);

        $date = new \DateTime();
        $date->setTimestamp($data['timestamp']);

        try {
            $this->analyticsService->saveUserHistoryData($user->getId(), $date);
        } catch (TIException $e) {
            if (in_array($e->getCode(), [70001, 70002, 70003])) {
                throw new ConsumerException('Tinkoff invest temporary error', 2000);
            } else {
                throw $e;
            }
        }

        return [
            'userId' => $user->getId(),
            'date' => $date
        ];
    }
}
