<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Common\Amqp\AmqpClient;
use App\Common\Logger\Factory as LoggerFactory;
use App\Exceptions\ConsumerException;
use DI\Container;
use Doctrine\DBAL\Exception\ConnectionException;
use Doctrine\ORM\Exception\EntityManagerClosed;
use ErrorException;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Message\AMQPMessage;

class MainConsumer
{
    private const WAIT_BEFORE_RECONNECT = 2;
    private const MAXIMUM_RETRY_ATTEMPTS = 5;
    private const LOG_PATH = 'consumer';
    private Container $container;
    private LoggerFactory $loggerFactory;
    private AmqpClient $amqpClient;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;

        /** @var LoggerFactory $factory */
        $this->loggerFactory =  $container->get(LoggerFactory::class);
        $this->amqpClient = $container->get(AmqpClient::class);
    }

    public function listen(): void
    {
        while (true) {
            try {
                register_shutdown_function(array($this, 'shutdown'));

                $this->setupChannel();
            } catch (AMQPRuntimeException $e) {
                $logger = $this->loggerFactory->create(self::LOG_PATH);
                $logger->warning('AMPQ runtime exception', ['exception' => $e]);
                sleep(self::WAIT_BEFORE_RECONNECT);
                $this->amqpClient->reconnect();
            } catch (\RuntimeException $e) {
                $logger = $this->loggerFactory->create(self::LOG_PATH);
                $logger->warning('Runtime exception', ['exception' => $e]);
                sleep(self::WAIT_BEFORE_RECONNECT);
                $this->amqpClient->reconnect();
            } catch (ErrorException $e) {
                $logger = $this->loggerFactory->create(self::LOG_PATH);
                $logger->warning('Error exception', ['exception' => $e]);
                sleep(self::WAIT_BEFORE_RECONNECT);
                $this->amqpClient->reconnect();
            }
        }
    }

    public function process(AMQPMessage $message): void
    {
        $logger = $this->loggerFactory->create(self::LOG_PATH);

        try {
            $data = json_decode($message->getBody(), true);
            $data['retryNumber'] = $data['retryNumber'] ?? 1;

            $context = [
                'date' => new \DateTime()->format('Y-m-d H:i:s'),
                'message' => $data
            ];

            $logger->info('Start process message', $context);

            switch ($data['type']) {
                case 'stockImport':
                    $service = $this->container->get(StockConsumer::class);
                    break;
                case 'bondImport':
                    $service = $this->container->get(BondConsumer::class);
                    break;
                case 'futureImport':
                    $service = $this->container->get(FutureConsumer::class);
                    break;
                case 'operationImport':
                    $service = $this->container->get(ItemOperationImportConsumer::class);
                    break;
                case 'candleImport':
                    $service = $this->container->get(CandleImportConsumer::class);
                    break;
                case 'calculateWealthHistory':
                    $service = $this->container->get(WealthHistoryImportConsumer::class);
                    break;
                case 'monthlyReport':
                    $service = $this->container->get(MonthlyReportConsumer::class);
                    break;
                case 'operationChange':
                    $service = $this->container->get(OperationChangeConsumer::class);
                    break;
                case 'outDateItem':
                    $service = $this->container->get(OutDateItemConsumer::class);
                    break;
                case 'updateMarketInstrument':
                    $service = $this->container->get(MarketSourceConsumer::class);
                    break;
                case 'updateItemData':
                    $service = $this->container->get(UpdateItemDataConsumer::class);
                    break;
                case "uploadingOperationsFile":
                    $service = $this->container->get(UploadingOperationFileConsumer::class);
                    break;
                default:
                    $logger->warning('Unknown message', ['message' => $data]);
                    $message->ack();
                    return;
            }

            $result = $service->process($data['data']);
            $message->ack();

            $logger->info('Message processed', $result);
        } catch (ConsumerException $e) {
            $message->ack();

            if ($e->getCode() === 2000 && $data['retryNumber'] < self::MAXIMUM_RETRY_ATTEMPTS) {
                $data['retryNumber'] = $data['retryNumber'] + 1;

                $this->amqpClient->publishMessage($data);

                $logger->warning('Temporary error', ['exception' => $e]);
            } else {
                $logger->error('Error message process', ['exception' => $e]);
            }
        } catch (ConnectionException | EntityManagerClosed $e) {
            $logger->error('Error message process', ['exception' => $e]);
            $this->shutdown();
        } catch (\Exception $e) {
            $message->ack();
            $logger->error('Error message process', ['exception' => $e]);
        }

        $context = null;
        $result = null;
        $logger = null;
        $data = null;
        $service = null;
    }

    private function setupChannel(): void
    {
        $channel = $this->amqpClient->getChannel();

        $channel->basic_qos(0, 1, 0);
        $channel->basic_consume(
            'main',
            '',
            false,
            false,
            false,
            false,
            array($this, 'process')
        );

        while (count($channel->callbacks)) {
            $channel->wait(null, false, 60 * 60 * 1000 * 1);
            usleep(1000);
        }
    }

    public function shutdown()
    {
        $this->amqpClient->close();
        $logger = $this->loggerFactory->create(self::LOG_PATH);

        if (!is_null($e = error_get_last())) {
            $logger->error('Consumer is broken', ['exception' => $e]);
        }
    }
}
