<?php

declare(strict_types=1);

namespace App\Consumers;

use App\Item\Services\OperationService;
use App\Item\Types\DataSourceType;
use Slim\Psr7\UploadedFile;

readonly class UploadingOperationFileConsumer implements ConsumerProcessInterface
{
    public function __construct(
        private OperationService $operationService
    ) {
    }

    public function process(array $data): array
    {
        $userId = $data['userId'];
        $source = DataSourceType::tryFrom($data['source']);
        $uploadedFile = [];
        foreach ($data["files"] as $file) {
            $uploadedFile[] = new UploadedFile($file);
        }

        $statistic = $this->operationService->importFromFile($userId, $source, $uploadedFile);

        foreach ($uploadedFile as $file) {
            unlink($file->getFilePath());
        }
        return [
            'added' => $statistic->getAdded(),
            'skipped' => $statistic->getSkipped(),
            'updated' => $statistic->getUpdated(),
        ];
    }
}
