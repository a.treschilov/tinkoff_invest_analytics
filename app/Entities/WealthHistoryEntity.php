<?php

declare(strict_types=1);

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.wealth_history')]
#[ORM\Entity]
class WealthHistoryEntity
{
    /**
     * @var int
     */
    #[ORM\Id]
    #[ORM\Column(name: 'wealth_history_id', type: 'integer')]
    #[ORM\GeneratedValue]
    protected int $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'currency_iso', type: 'string')]
    protected string $currencyIso;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'stock', type: 'float')]
    protected float $stock;

    #[ORM\Column(name: 'assets', type: 'float')]
    protected float $assets;

    #[ORM\Column(name: 'real_estate', type: 'float')]
    protected float $realEstate;

    #[ORM\Column(name: 'deposit', type: 'float')]
    protected float $deposit;

    #[ORM\Column(name: 'loan', type: 'float')]
    protected float $loan;

    #[ORM\Column(name: 'expenses', type: 'integer')]
    protected ?float $expenses = null;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getStock(): float
    {
        return $this->stock;
    }

    /**
     * @param float $stock
     */
    public function setStock(float $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return float
     */
    public function getAssets(): float
    {
        return $this->assets;
    }

    /**
     * @param float $assets
     */
    public function setAssets(float $assets): void
    {
        $this->assets = $assets;
    }

    /**
     * @return float
     */
    public function getRealEstate(): float
    {
        return $this->realEstate;
    }

    /**
     * @param float $realEstate
     */
    public function setRealEstate(float $realEstate): void
    {
        $this->realEstate = $realEstate;
    }

    /**
     * @return float
     */
    public function getDeposit(): float
    {
        return $this->deposit;
    }

    /**
     * @param float $deposit
     */
    public function setDeposit(float $deposit): void
    {
        $this->deposit = $deposit;
    }

    /**
     * @return float
     */
    public function getLoan(): float
    {
        return $this->loan;
    }

    /**
     * @param float $loan
     */
    public function setLoan(float $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @return float|null
     */
    public function getExpenses(): ?float
    {
        return $this->expenses;
    }

    /**
     * @param float|null $expenses
     */
    public function setExpenses(?float $expenses): void
    {
        $this->expenses = $expenses;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }
}
