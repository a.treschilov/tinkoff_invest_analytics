<?php

declare(strict_types=1);

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.strategy_sector')]
#[ORM\Entity]
class StrategySectorEntity
{
    /**
     * @var int
     */
    #[ORM\Id]
    #[ORM\Column(name: 'strategy_sector_id', type: 'integer')]
    #[ORM\GeneratedValue]
    protected $id;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'market_sector_id', type: 'integer')]
    protected int $marketSectorId;

    /**
     * @var MarketSectorEntity
     */
    #[ORM\JoinColumn(name: 'market_sector_id', referencedColumnName: 'market_sector_id')]
    #[ORM\ManyToOne(targetEntity: \App\Entities\MarketSectorEntity::class)]
    protected MarketSectorEntity $marketSector;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive = 1;

    #[ORM\Column(name: 'share', type: 'float')]
    protected float $share;

    #[ORM\Column(name: 'created_date', type: 'datetimetz')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'updated_date', type: 'datetimetz')]
    protected \DateTime $updatedDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getMarketSectorId(): int
    {
        return $this->marketSectorId;
    }

    /**
     * @param int $marketSectorId
     */
    public function setMarketSectorId(int $marketSectorId): void
    {
        $this->marketSectorId = $marketSectorId;
    }

    /**
     * @return MarketSectorEntity
     */
    public function getMarketSector(): MarketSectorEntity
    {
        return $this->marketSector;
    }

    /**
     * @param MarketSectorEntity $marketSector
     */
    public function setMarketSector(MarketSectorEntity $marketSector): void
    {
        $this->marketSector = $marketSector;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return float
     */
    public function getShare(): float
    {
        return $this->share;
    }

    /**
     * @param float $share
     */
    public function setShare(float $share): void
    {
        $this->share = $share;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}
