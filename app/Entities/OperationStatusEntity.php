<?php

declare(strict_types=1);

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.operation_status')]
#[ORM\Entity]
class OperationStatusEntity
{
    /**
     * @var int
     */
    #[ORM\Id]
    #[ORM\Column(name: 'operation_status_id', type: 'integer')]
    #[ORM\GeneratedValue]
    protected $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'external_id', type: 'string')]
    protected $externalId;

    /**
     * @var ?string
     */
    #[ORM\Column(name: 'name', type: 'string')]
    protected $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}
