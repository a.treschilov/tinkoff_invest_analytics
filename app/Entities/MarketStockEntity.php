<?php

declare(strict_types=1);

namespace App\Entities;

use App\Intl\Entities\CountryEntity;
use App\Market\Entities\MarketInstrumentEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_stock')]
#[ORM\Entity]
class MarketStockEntity
{
    /**
     * @var int
     */
    #[ORM\Id]
    #[ORM\Column(name: 'market_stock_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var int
     */
    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected int $marketInstrumentId;

    /**
     * @var MarketInstrumentEntity
     */
    #[ORM\JoinColumn(name: 'market_instrument_id', referencedColumnName: 'market_instrument_id')]
    #[ORM\OneToOne(targetEntity: MarketInstrumentEntity::class, inversedBy: 'marketStock')]
    protected MarketInstrumentEntity $marketInstrument;

    /**
     * @var string|null
     */
    #[ORM\JoinColumn(name: 'country', referencedColumnName: 'iso')]
    #[ORM\Column(name: 'country', type: 'string')]
    protected ?string $countryIso = null;

    /**
     * @var CountryEntity|null
     */
    #[ORM\JoinColumn(name: 'country', referencedColumnName: 'iso')]
    #[ORM\ManyToOne(targetEntity: CountryEntity::class)]
    protected ?CountryEntity $country = null;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'sector', type: 'string')]
    protected ?string $sector = null;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'industry', type: 'string')]
    protected ?string $industry = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return MarketInstrumentEntity
     */
    public function getMarketInstrument(): MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity $marketInstrument
     */
    public function setMarketInstrument(MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return string|null
     */
    public function getCountryIso(): ?string
    {
        return $this->countryIso;
    }

    /**
     * @param string|null $countryIso
     */
    public function setCountryIso(?string $countryIso): void
    {
        $this->countryIso = $countryIso;
    }

    /**
     * @return string|null
     */
    public function getSector(): ?string
    {
        return $this->sector;
    }

    /**
     * @param string|null $sector
     */
    public function setSector(?string $sector): void
    {
        $this->sector = $sector;
    }

    /**
     * @return string|null
     */
    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    /**
     * @param string|null $industry
     */
    public function setIndustry(?string $industry): void
    {
        $this->industry = $industry;
    }

    /**
     * @return CountryEntity|null
     */
    public function getCountry(): ?CountryEntity
    {
        return $this->country;
    }

    /**
     * @param CountryEntity|null $country
     */
    public function setCountry(?CountryEntity $country): void
    {
        $this->country = $country;
    }
}
