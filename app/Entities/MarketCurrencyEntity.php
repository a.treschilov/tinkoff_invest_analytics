<?php

declare(strict_types=1);

namespace App\Entities;

use App\Market\Entities\MarketInstrumentEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_currency')]
#[ORM\Entity]
class MarketCurrencyEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'market_currency_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected ?int $marketInstrumentId = null;

    #[ORM\JoinColumn(name: 'market_instrument_id', referencedColumnName: 'market_instrument_id')]
    #[ORM\OneToOne(targetEntity: MarketInstrumentEntity::class, inversedBy: 'marketCurrency')]
    protected ?MarketInstrumentEntity $marketInstrument = null;


    #[ORM\Column(name: 'iso', type: 'string')]
    protected string $iso;

    #[ORM\Column(name: 'nominal', type: 'float')]
    protected float $nominal;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    /**
     * @return int|null
     */
    public function getMarketInstrumentId(): ?int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int|null $marketInstrumentId
     */
    public function setMarketInstrumentId(?int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return MarketInstrumentEntity|null
     */
    public function getMarketInstrument(): ?MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity|null $marketInstrument
     */
    public function setMarketInstrument(?MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return float
     */
    public function getNominal(): float
    {
        return $this->nominal;
    }

    /**
     * @param float $nominal
     */
    public function setNominal(float $nominal): void
    {
        $this->nominal = $nominal;
    }
}
