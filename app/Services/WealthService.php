<?php

declare(strict_types=1);

namespace App\Services;

use App\Collections\WealthCollection;
use App\Entities\WealthHistoryEntity;
use App\Models\WealthHistoryModel;
use App\Storages\WealthHistoryStorage;
use App\User\Services\UserService;

class WealthService
{
    public function __construct(
        private readonly WealthHistoryStorage $wealthHistoryStorage,
        private readonly UserService $userService
    ) {
    }

    /**
     * @return WealthHistoryModel[]
     * @throws \Exception
     */
    public function userDynamic(): WealthCollection
    {
        $userId = $this->userService->getUserId();
        $wealthEntities = $this->wealthHistoryStorage->findActiveByUser($userId);

        $wealthCollection = new WealthCollection();
        foreach ($wealthEntities as $entity) {
            $wealthCollection->add($this->convertWealthEntityToModel($entity));
        }

        return $wealthCollection;
    }

    public function getWealthByPeriod(int $userId, \DateTime $from, \DateTime $to): WealthCollection
    {
        $result = new WealthCollection();
        $wealthEntities = $this->wealthHistoryStorage->findActiveByDateIntervalAndUser($userId, $from, $to);
        foreach ($wealthEntities as $wealth) {
            $result->add($this->convertWealthEntityToModel($wealth));
        }

        return $result;
    }

    public function getLastWealth(): ?WealthHistoryModel
    {
        $userId = $this->userService->getUserId();
        $wealthEntity = $this->wealthHistoryStorage->findLastWealth($userId);

        if ($wealthEntity === null) {
            return null;
        }

        return $this->convertWealthEntityToModel($wealthEntity);
    }

    public function getWealthByDay(int $userId, \DateTime $dateTime): ?WealthHistoryModel
    {
        $date = clone $dateTime;
        $date->setTime(23, 59, 59);

        $wealthEntity = $this->wealthHistoryStorage->findLastWealthByDate($userId, $date);

        if ($wealthEntity === null) {
            return null;
        }

        return $this->convertWealthEntityToModel($wealthEntity);
    }

    public function calculateActives(WealthHistoryModel $wealth): float
    {
        return $wealth->getStock()
        + $wealth->getAssets()
        + $wealth->getRealEstate()
        + $wealth->getDeposit();
    }

    public function calculatePassives(WealthHistoryModel $wealth): float
    {
        return -1 * $wealth->getLoan();
    }

    public function calculateCapital(WealthHistoryModel $wealth): float
    {
        return $this->calculateActives($wealth) - $this->calculatePassives($wealth);
    }

    private function convertWealthEntityToModel(WealthHistoryEntity $entity): WealthHistoryModel
    {
        $wealthRate = $entity->getExpenses() === null || (int)$entity->getExpenses() === 0
            ? null
            : ($entity->getStock() + $entity->getDeposit() + $entity->getRealEstate() + $entity->getAssets()
                + $entity->getLoan())
                / $entity->getExpenses() / 12;
        return new WealthHistoryModel([
            'wealthHistoryId' => $entity->getId(),
            'userId' => $entity->getUserId(),
            'currencyIso' => $entity->getCurrencyIso(),
            'date' => $entity->getDate(),
            'stock' => $entity->getStock(),
            'assets' => $entity->getAssets(),
            'realEstate' => $entity->getRealEstate(),
            'deposit' => $entity->getDeposit(),
            'loan' => $entity->getLoan(),
            'expenses' => $entity->getExpenses(),
            'wealthRate' => $wealthRate
        ]);
    }
}
