<?php

declare(strict_types=1);

namespace App\Services;

use App\Assets\Exceptions\ThirdPartyUploadException;
use App\Broker\Collections\OperationCollection as BrokerOperationCollection;
use App\Broker\Entities\BrokerReportUploadEntity;
use App\Broker\Models\BrokerOperationModel;
use App\Broker\Models\BrokerReportUploadModel;
use App\Broker\Services\BrokerReportUploadService;
use App\Broker\Services\BrokerService;
use App\Broker\Storages\BrokerReportUploadStorage;
use App\Broker\Types\BrokerReportUploadStatusType;
use App\Broker\Types\BrokerType;
use App\Collections\OperationCollection;
use App\Common\Amqp\AmqpClient;
use App\Entities\OperationTypeEntity;
use App\Exceptions\AccountException;
use App\Exceptions\BrokerAdapterException;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\MarketOperationServiceInterface;
use App\Intl\Services\CurrencyService;
use App\Item\Entities\ItemOperationEntity;
use App\Item\Storages\ItemOperationImportStorage;
use App\Item\Storages\ItemOperationStorage;
use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;
use App\Models\OperationModel;
use App\Storages\OperationTypeStorage;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Collections\UserCredentialCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserService;
use Slim\Psr7\UploadedFile;

class MarketOperationsService implements MarketOperationServiceInterface
{
    public function __construct(
        private readonly StockBrokerFactory $stockBrokerFactory,
        private readonly OperationTypeStorage $operationTypeStorage,
        private readonly MarketInstrumentServiceInterface $marketInstrumentService,
        private readonly AccountService $accountService,
        private readonly UserService $userService,
        private readonly BrokerService $brokerService,
        private readonly ItemOperationStorage $itemOperationStorage,
        private readonly CurrencyService $currencyService,
        private readonly \DateTime $currentDateTime,
        private readonly UserCredentialService $userCredentialService,
        private readonly AmqpClient $amqpClient,
        private readonly ItemOperationImportStorage $itemOperationImportStorage,
        private readonly UserBrokerAccountService $userBrokerAccountService,
        private readonly BrokerReportUploadService $brokerReportUploadService
    ) {
    }

    /**
     * @param OperationFiltersModel $filters
     * @return OperationCollection
     * @throws AccountException
     */
    private function getOperations(OperationFiltersModel $filters): OperationCollection
    {
        $accounts = $this->accountService->getUserBrokerAccounts();

        return $this->getAccountsOperations($accounts, $filters);
    }

    public function importBrokerOperations(
        OperationFiltersModel $operationFilter,
        int $userBrokerAccountId
    ): ImportStatisticModel {
        $operations = new OperationCollection();

        $account = $this->userBrokerAccountService->getUserBrokerAccountById($userBrokerAccountId);
        if ($account !== null) {
            $accounts = new UserBrokerAccountCollection([
                $this->userBrokerAccountService->getUserBrokerAccountById($userBrokerAccountId)
            ]);
            $operations = $this->getAccountsOperations($accounts, $operationFilter);
        }

        return $this->importOperations($operations);
    }

    public function importUserOperations(OperationFiltersModel $operationFilter): ImportStatisticModel
    {
        $operations = $this->getOperations($operationFilter);

        return $this->importOperations($operations);
    }

    public function importManual(string $broker, UploadedFile $brokerReport): ImportStatisticModel
    {
        $reportUpload = new BrokerReportUploadModel();
        $reportUpload->setDate($this->currentDateTime);
        $reportUpload->setUserId($this->userService->getUserId());
        $reportUpload->setBrokerId(BrokerType::tryFrom($broker));

        $brokerClient = $this->stockBrokerFactory->getBrokerClientById($broker);

        try {
            $brokerOperations = $brokerClient->manualOperationImport($brokerReport);

            $operations = new OperationCollection();
            /** @var BrokerOperationModel $brokerOperation */
            foreach ($brokerOperations->getIterator() as $brokerOperation) {
                $operations->add($this->convertBrokerOperationToModel($brokerOperation));
            }

            $statistic = $this->importOperations($operations);

            $reportUpload->setAdded($statistic->getAdded());
            $reportUpload->setUpdated($statistic->getUpdated());
            $reportUpload->setSkipped($statistic->getSkipped());
            $reportUpload->setStatus(BrokerReportUploadStatusType::SUCCESS);
        } catch (ThirdPartyUploadException $e) {
            $reportUpload->setAdded(0);
            $reportUpload->setUpdated(0);
            $reportUpload->setSkipped(0);
            $reportUpload->setStatus(BrokerReportUploadStatusType::FAIL);
        }

        $this->brokerReportUploadService->addReport($reportUpload);

        if ($reportUpload->getStatus() === BrokerReportUploadStatusType::FAIL) {
            throw new ThirdPartyUploadException($e->getMessage(), $e->getCode());
        }

        return $statistic;
    }

    public function getOperationsToImport(): array
    {
        $credentials = $this->userCredentialService->getAllActiveCredentialList();
        return $this->importCredentialOperation($credentials);
    }

    public function getUserOperationsToImport(): array
    {
        $userId = $this->userService->getUserId();
        $credentials = $this->userCredentialService->getActiveCredentialList($userId);

        return $this->importCredentialOperation($credentials);
    }

    private function importCredentialOperation(UserCredentialCollection $credentials): array
    {
        $messages = [];
        /** @var UserCredentialModel $credential */
        foreach ($credentials->getIterator() as $credential) {
            $accounts = $this->accountService->getCredentialAccounts($credential);

            /** @var UserBrokerAccountEntity $brokerAccount */
            foreach ($accounts as $brokerAccount) {
                $lastImport = $this->itemOperationImportStorage
                    ->findByUserBrokerAccount($brokerAccount->getId());

                if ($lastImport === null) {
                    $dateFrom = new \DateTime(OperationFiltersModel::$DEFAULT_DATE_FROM);
                } else {
                    $dateFrom = clone $lastImport->getImportDateEnd();
                    $dateFrom = min($dateFrom, clone $this->currentDateTime);
                    $dateFrom->sub(new \DateInterval('P14D'));
                }

                do {
                    $dateTo = clone $dateFrom;
                    $dateTo->add(new \DateInterval('P3M'));
                    $dateTo->sub(new \DateInterval('P1D'));
                    $dateTo = min($dateTo, clone $this->currentDateTime);

                    $message = [
                        'type' => 'operationImport',
                        'data' => [
                            'dateFrom' => (clone $dateFrom)->format('Y-m-d'),
                            'dateTo' => (clone $dateTo)->format('Y-m-d'),
                            'userId' => $credential->getUserId(),
                            'userBrokerAccountId' => $brokerAccount->getId()
                        ]
                    ];
                    $this->amqpClient->publishMessage($message);

                    $messages[] = $message;
                    $dateFrom->add(new \DateInterval('P3M'));
                } while ($dateTo < $this->currentDateTime);
            }
        }

        return $messages;
    }

    /**
     * @param UserBrokerAccountCollection $accounts
     * @param OperationFiltersModel $filters
     * @return OperationCollection
     * @throws AccountException
     */
    private function getAccountsOperations(
        UserBrokerAccountCollection $accounts,
        OperationFiltersModel $filters
    ): OperationCollection {
        if ($accounts->count() === 0) {
            throw new AccountException('You don`t have broker account. Please try T-Invest Api Key', 1030);
        }

        $dateFrom = $filters->getDateFrom();
        $dateTo = $filters->getDateTo();
        $dateTo->setTime(23, 59, 59);

        $brokerOperations = new BrokerOperationCollection();
        /** @var UserBrokerAccountEntity $account */
        foreach ($accounts->getIterator() as $account) {
            $broker = $this->stockBrokerFactory->getBroker($account->getUserCredential());
            $brokerOperations = new BrokerOperationCollection(
                array_merge(
                    $brokerOperations->toArray(),
                    $broker->getOperations($account->getExternalId(), $dateFrom, $dateTo)->toArray()
                )
            );
        }

        $operations = new OperationCollection();
        /** @var BrokerOperationModel $operation */
        foreach ($brokerOperations->getIterator() as $brokerOperation) {
            $operation = $this->convertBrokerOperationToModel($brokerOperation);
            $operations->add($operation);
        }

        if (null !== $filters->getOperationType() && '' !== $filters->getOperationType()) {
            $operations = $operations->filterByOperationTypes([$filters->getOperationType()]);
        }
        if (null !== $filters->getOperationStatus() && '' !== $filters->getOperationStatus()) {
            $operations = $operations->filterByOperationStatus($filters->getOperationStatus());
        }

        return $operations->getSortedByDate();
    }

    public function getOperationTypeByExternalId(string $externalId): ?OperationTypeEntity
    {
        return $this->operationTypeStorage->findByExternalId($externalId);
    }

    private function convertBrokerOperationToModel(BrokerOperationModel $operation): OperationModel
    {
        $instrument = $operation->getIsin() !== null
            ? $this->marketInstrumentService->getInstrumentByIsin($operation->getIsin())
            : null;

        return new OperationModel([
            'id' => $operation->getId(),
            'parentOperationId' => $operation->getParentOperationId(),
            'type' => $operation->getType(),
            'date' => $operation->getDate(),
            'instrumentType' => $operation->getInstrumentType(),
            'isin' => $instrument?->getIsin(),
            'quantity' => $operation->getQuantity(),
            'status' => $operation->getStatus(),
            'currency' => $operation->getCurrency(),
            'payment' => $operation->getPayment(),
            'price' => $operation->getPrice(),
            'instrument' => $instrument,
            'broker' => $operation->getBroker(),
            'errorCode' => $operation->getErrorCode(),
        ]);
    }

    private function importOperations(OperationCollection $operations): ImportStatisticModel
    {
        //TODO: Use operationService instead operationStorage
        $statistic = [
            'skipped' => 0,
            'updated' => 0,
            'added' => 0
        ];

        $user = $this->userService->getUser();

        $forUpdate = [];

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $broker = $this->brokerService->getBrokerById($operation->getBroker());

            if ($broker === null) {
                throw new BrokerAdapterException('Broker not found');
            }

            $operationType = $this->operationTypeStorage->findByExternalId($operation->getType());
            $itemOperation = $this->itemOperationStorage
                ->findByExternalIdAndBrokerAndType(
                    $operation->getId(),
                    $broker->getBrokerId(),
                    $operationType->getId()
                );

            if (null === $itemOperation) {
                $itemOperation = $this->convertOperationModelToEntity($operation, $user);
                $itemOperation->setIsAnalyticProceed(0);
                $forUpdate[] = $itemOperation;
                $statistic['added'] += 1;
            } elseif (
                $itemOperation->getStatus() !== $operation->getStatus()
                || (float)$itemOperation->getQuantity() !== (float)$operation->getQuantity()
            ) {
                $itemOperation->setStatus($operation->getStatus());
                $itemOperation->setQuantity($operation->getQuantity());
                $itemOperation->setIsAnalyticProceed(0);
                $forUpdate[] = $itemOperation;
                $statistic['updated'] += 1;
            } else {
                $statistic['skipped'] += 1;
            }
        }

        $this->itemOperationStorage->addEntityArray($forUpdate);

        return new ImportStatisticModel($statistic);
    }


    private function convertOperationModelToEntity(
        OperationModel $operation,
        UserEntity $user
    ): ItemOperationEntity {
        $item = null;
        $itemId = null;
        $currency = $this->currencyService
            ->getCurrencyByIso($operation->getPayment()->getCurrency());

        $broker = $this->brokerService->getBrokerEntityById($operation->getBroker());

        if ($operation->getIsin() !== null) {
            $item = $this->marketInstrumentService
                ->getInstrumentByIsin($operation->getIsin())
                ->getItem();
            $itemId = $item->getId();
        }
        $externalId = $operation->getId() === '' ? null : $operation->getId();

        $operationType = $this->getOperationTypeByExternalId($operation->getType());

        $itemOperation = new ItemOperationEntity();
        $itemOperation->setUserId($user->getId());
        $itemOperation->setItemId($itemId);
        $itemOperation->setItem($item);
        $itemOperation->setExternalId($externalId);
        $itemOperation->setQuantity($operation->getQuantity());
        $itemOperation->setDate($operation->getDate());
        $itemOperation->setAmount($operation->getPayment()->getAmount());
        $itemOperation->setCurrencyId($currency->getId());
        $itemOperation->setCurrency($currency);
        $itemOperation->setCreatedDate($this->currentDateTime);
        $itemOperation->setOperationTypeId($operationType->getId());
        $itemOperation->setOperationType($operationType);
        $itemOperation->setStatus($operation->getStatus());
        $itemOperation->setBrokerId($broker->getId());
        $itemOperation->setBroker($broker);
        $itemOperation->setErrorCode($operation->getErrorCode());

        return $itemOperation;
    }
}
