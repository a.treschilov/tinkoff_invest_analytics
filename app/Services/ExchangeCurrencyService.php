<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Intl\Entities\CurrencyExchangeEntity;
use App\Intl\Services\ThirdParty\ExchangeRateApiInterface;
use App\Intl\Storages\CurrencyExchangeStorage;

/**
 * Class ExchangeCurrency
 * @package App\Services
 */
class ExchangeCurrencyService
{
    public function __construct(
        private readonly CurrencyExchangeStorage $currencyExchangeStorage,
        private readonly ExchangeRateApiInterface $exchangeRateApi
    ) {
    }

    public function getExchangeRate(string $fromCurrency, string $toCurrency, \DateTime $dateTime): float
    {
        $toCurrencyExchangeRate = $this->currencyExchangeStorage
            ->findByCurrenciesAndDate('RUB', $fromCurrency, $dateTime);
        $fromCurrencyExchangeRate = $this->currencyExchangeStorage
            ->findByCurrenciesAndDate('RUB', $toCurrency, $dateTime);

        if ($toCurrencyExchangeRate === null && $fromCurrencyExchangeRate === null) {
            $this->importExchangeRates($dateTime);

            $toCurrencyExchangeRate = $this->currencyExchangeStorage
                ->findByCurrenciesAndDate('RUB', $fromCurrency, $dateTime);
            $fromCurrencyExchangeRate = $this->currencyExchangeStorage
                ->findByCurrenciesAndDate('RUB', $toCurrency, $dateTime);
        }

        if ($fromCurrencyExchangeRate !== null) {
            $fromCurrencyExchangeRate = $fromCurrencyExchangeRate->getExchangeRate();
        } else {
            $fromCurrencyExchangeRate = $this->currencyExchangeStorage
                ->findLastByCurrencyAndDate('RUB', $fromCurrency, $dateTime)?->getExchangeRate();

            if ($fromCurrencyExchangeRate === null) {
                throw new ExchangeUnsupportedCurrencyException(
                    'The currency ' . $fromCurrency . 'does not have exchange rate'
                );
            }
        }

        if ($toCurrencyExchangeRate !== null) {
            $toCurrencyExchangeRate = $toCurrencyExchangeRate->getExchangeRate();
        } else {
            $toCurrencyExchangeRate = $this->currencyExchangeStorage
                ->findLastByCurrencyAndDate('RUB', $toCurrency, $dateTime)?->getExchangeRate();

            if ($toCurrencyExchangeRate === null) {
                throw new ExchangeUnsupportedCurrencyException(
                    'The currency ' . $toCurrency . 'does not have exchange rate'
                );
            }
        }

        return $fromCurrencyExchangeRate / $toCurrencyExchangeRate;
    }

    public function importExchangeRates(\DateTime $date): array
    {
        $currencyFrom = 'RUB';

        $rates = $this->exchangeRateApi->getExchangeRate($currencyFrom, $date);
        $dbRates = $this->currencyExchangeStorage->findByDate($date);

        $entityArray = [];
        foreach ($rates as $currency => $rate) {
            $isExist = false;
            foreach ($dbRates as $dbRate) {
                if ($dbRate->getCurrencyTo() === $currency && $dbRate->getCurrencyFrom() === $currencyFrom) {
                    $isExist = true;
                    break;
                }
            }
            if ($isExist) {
                continue;
            }
            $entity = new CurrencyExchangeEntity();
            $entity->setCurrencyFrom($currencyFrom);
            $entity->setCurrencyTo($currency);
            $entity->setDate($date);
            $entity->setExchangeRate($rate);
            $entityArray[] = $entity;
        }

        $this->currencyExchangeStorage->addArrayEntity($entityArray);

        return [
            'imported' => count($entityArray),
            'date' => $date
        ];
    }
}
