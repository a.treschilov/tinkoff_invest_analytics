<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Interfaces\UserBrokerInterface;
use App\Broker\Services\Adapters\AlfaInvest;
use App\Broker\Services\Adapters\SberInvest;
use App\Broker\Services\Adapters\TinkoffHelper;
use App\Broker\Services\Adapters\TinkoffInvest;
use App\Broker\Types\BrokerType;
use App\Exceptions\AccountException;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserCredentialService;
use ATreschilov\TinkoffInvestApiSdk\TIClient as TIClientV2;

class StockBrokerFactory
{
    private array $brokerInstances = [];
    private array $brokerClients = [];
    private array $stockBrokerAdapters = [];

    public function __construct(
        private readonly UserCredentialService $userCredentialService,
        private readonly SourceBrokerInterface $sourceTinkoff,
        private readonly SourceBrokerInterface $sourceMoex,
        private readonly MarketInstrumentService $marketInstrumentService,
    ) {
    }

    public function getBroker(UserCredentialEntity $credential): UserBrokerInterface
    {
        if (isset($this->brokerInstances[$credential->getId()])) {
            return $this->brokerInstances[$credential->getId()];
        }

        $credentialModel = $this->userCredentialService->convertEntityToModel($credential);
        $this->brokerInstances[$credentialModel->getUserCredentialId()] = $this->getBrokerClient($credentialModel);

        return $this->brokerInstances[$credentialModel->getUserCredentialId()];
    }

    public function getBrokerClientById(string $brokerId): UserBrokerInterface
    {
        if (!isset($this->brokerClients[$brokerId])) {
            $this->brokerClients[$brokerId] = match (BrokerType::from($brokerId)) {
                BrokerType::ALFA_INVEST => new AlfaInvest($this->marketInstrumentService),
                BrokerType::SBER => new SberInvest($this->marketInstrumentService),
                default => throw new AccountException('Unknown broker', 1032)
            };
        }

        return $this->brokerClients[$brokerId];
    }

    public function getSourceService($brokerCode): SourceBrokerInterface
    {
        $broker = match (BrokerType::tryFrom($brokerCode)) {
            BrokerType::TINKOFF => $this->sourceTinkoff,
            BrokerType::MOEX => $this->sourceMoex,
            default => throw new AccountException('Unknown broker', 1032)
        };
        $this->stockBrokerAdapters[$brokerCode] = $broker;

        return $this->stockBrokerAdapters[$brokerCode];
    }

    private function getBrokerClient(UserCredentialModel $credential): UserBrokerInterface
    {
        if (isset($this->brokerInstances[$credential->getUserCredentialId()])) {
            return $this->brokerInstances[$credential->getUserCredentialId()];
        }

        $this->brokerInstances[$credential->getUserCredentialId()] =
        match (BrokerType::tryFrom($credential->getBrokerId())) {
            BrokerType::TINKOFF => new TinkoffInvest(
                new TIClientV2($credential->getApiKey(), ['isRateLimitRetry' => true]),
                $this->sourceTinkoff,
                new TinkoffHelper(),
            ),
            default => throw new AccountException('Unknown broker', 1032)
        };

        return $this->brokerInstances[$credential->getUserCredentialId()];
    }
}
