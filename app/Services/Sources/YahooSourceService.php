<?php

declare(strict_types=1);

namespace App\Services\Sources;

use App\Common\HttpClient;
use App\Entities\MarketStockEntity;
use App\Intl\Services\CountryService;

class YahooSourceService
{
    private HttpClient $httpClient;
    private CountryService $countryService;

    public function __construct(HttpClient $httpClient, CountryService $countryService)
    {
        $this->httpClient = $httpClient;
        $this->countryService = $countryService;
    }

    public function getCompanyProfile(string $ticker): ?MarketStockEntity
    {
        $postfixes = ['', '.ME'];

        $ticker = str_replace('@DE', '.DE', $ticker);
        $ticker = str_replace(' GY', '.DE', $ticker);

        foreach ($postfixes as $postfix) {
            $marketStock = $this->getCompanyProfileByTicker($ticker . $postfix);
            if (null !== $marketStock) {
                return $marketStock;
            }
        }

        if ((int)$ticker !== 0) {
            $ticker = substr('0000' . $ticker, -4) . '.HK';
            $marketStock = $this->getCompanyProfileByTicker($ticker);
            if ($marketStock !== null) {
                return $marketStock;
            }
        }

        return null;
    }

    private function getCompanyProfileByTicker(string $ticker): ?MarketStockEntity
    {
        $body = [
            'modules' => 'assetProfile'
        ];
        $response = $this->httpClient->doRequest(
            HttpClient::METHOD_GET,
            'v10/finance/quoteSummary/' . $ticker,
            $body
        );
        $content = json_decode($response->getBody()->getContents(), true);

        if (!isset($content['quoteSummary']['result'])) {
            return null;
        }

        $profile = $content['quoteSummary']['result'][0]['assetProfile'];

        if (!isset($profile['country']) || !isset($profile['industry']) || !isset($profile['sector'])) {
            return null;
        }
        $country = $this->countryService->getCountryByName($profile['country']);
        $marketStock = new MarketStockEntity();
//        $marketStock = new BrokerShareModel([
//            'country' => $country?->getIso(),
//            'industry' => $profile['industry'],
//            'sector' => $profile['sector']
//        ]);
        $marketStock->setCountryIso($country?->getIso());
        $marketStock->setIndustry($profile['industry']);
        $marketStock->setSector($profile['sector']);
        $marketStock->setCountry($country);

        return $marketStock;
    }
}
