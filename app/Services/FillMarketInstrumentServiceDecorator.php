<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\InstrumentCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerBondModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Types\BrokerInstrumentType;
use App\Broker\Types\BrokerType;
use App\Collections\MarketInstrumentCollection;
use App\Entities\MarketCurrencyEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use App\Exceptions\UnsupportedInstrumentException;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Intl\Services\CountryService;
use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use App\Market\Entities\MarketBondEntity;
use App\Market\Models\MarketInstrumentModel;
use App\Services\Sources\YahooSourceService;

/** @deprecated ise MarketInstrumentDbServiceStoreDecorator */
class FillMarketInstrumentServiceDecorator implements MarketInstrumentServiceInterface
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly MarketInstrumentServiceInterface $marketInstrumentService,
        private readonly ItemStorage $itemStorage,
        private readonly StockBrokerFactory $stockBrokerFactory,
        private readonly YahooSourceService $yahooService,
        private readonly CountryService $countryService,
        private readonly SourceBrokerInterface $brokerIterator
    ) {
    }

    public function getInstrumentByIsin(string $isin): ?MarketInstrumentEntity
    {
        $instrument = $this->marketInstrumentService->getInstrumentByIsin($isin);

        if ($instrument === null) {
            $broker = $this->stockBrokerFactory->getSourceService(BrokerType::TINKOFF->value);
            $instrumentCollection = $this->brokerIterator->getInstrumentByIsin($isin);
            if ($instrumentCollection->count() === 0) {
                throw new UnsupportedInstrumentException('Unknown instrument', 1011);
            }

            /** @var BrokerInstrumentModel $brokerInstrument */
            $brokerInstrument = $instrumentCollection->mergeSame();

            $sku = $brokerInstrument->getIsin()
                ?: $brokerInstrument->getTicker() . '.' . $brokerInstrument->getExchange();

            $item = new ItemEntity();
            $item->setName($brokerInstrument->getName());
            $item->setLogo($brokerInstrument->getLogo());
            $item->setType('market');
            $item->setSource('market');
            $item->setExternalId($sku);

            $instrument = new MarketInstrumentEntity();
            $instrument->setCurrency($brokerInstrument->getCurrency());
            $instrument->setIsin($sku);
            $instrument->setLot($brokerInstrument->getLot());
            $instrument->setMinPriceIncrement($brokerInstrument->getMinPriceIncrement());
            $instrument->setName($brokerInstrument->getName());
            $instrument->setTicker($brokerInstrument->getTicker());
            $instrument->setType($brokerInstrument->getType());
            $instrument->setRegNumber($brokerInstrument->getRegNumber());

            if ($instrument->getType() === BrokerInstrumentType::STOCK) {
                $shareModel = $this->brokerIterator->getShareByIsin($brokerInstrument->getIsin());
                if (null !== $shareModel) {
                    $marketStock = new MarketStockEntity();
                    $marketStock->setCountryIso($shareModel->getCountry());
                    $marketStock->setCountry($this->countryService->getCountryByIso($shareModel->getCountry()));
                    $marketStock->setIndustry($shareModel->getIndustry());
                    $marketStock->setSector($shareModel->getSector());
                }

                if ($shareModel === null) {
                    $marketStock = $this->yahooService->getCompanyProfile($brokerInstrument->getTicker());
                } elseif (
                    in_array($shareModel->getCountry(), [null, ''])
                    || $shareModel->getIndustry() === null
                    || $shareModel->getSector() === null
                ) {
                    $yahooMarketStock = $this->yahooService->getCompanyProfile($brokerInstrument->getTicker());

                    if ($yahooMarketStock !== null) {
                        if (in_array($shareModel->getCountry(), [null, ''])) {
                            $marketStock->setCountry($yahooMarketStock->getCountry());
                            $marketStock->setCountryIso($yahooMarketStock->getCountryIso());
                        }
                        if ($shareModel->getIndustry() === null) {
                            $marketStock->setIndustry($yahooMarketStock->getIndustry());
                        }
                        if ($shareModel->getSector() === null) {
                            $marketStock->setSector($yahooMarketStock->getSector());
                        }
                    }
                }

                if (null !== $marketStock) {
                    $marketStock->setMarketInstrument($instrument);
                    $instrument->setMarketStock($marketStock);
                }
            }

            if ($instrument->getType() === 'Bond') {
                $bond = $this->brokerIterator->getBondByIsin($brokerInstrument->getIsin());

                if ($bond !== null) {
                    $marketBond = $this->convertBondModelToEntity($bond);
                    $marketBond->setMarketInstrument($instrument);
                    $marketBond->setUpdatedDate($this->currentDateTime);

                    $instrument->setMarketBond($marketBond);
                }
            }

            if ($instrument->getType() === 'Currency') {
                $currency = $broker->getCurrencyByIsin($instrument->getIsin());
                $marketCurrencyEntity = new MarketCurrencyEntity();
                $marketCurrencyEntity->setIso($currency->getIso());
                $marketCurrencyEntity->setNominal($currency->getNominal());
                $marketCurrencyEntity->setMarketInstrument($instrument);
                $instrument->setMarketCurrency($marketCurrencyEntity);
            }

            $item->setMarketInstrument($instrument);
            $instrument->setItem($item);

            $this->itemStorage->addEntity($item);
        }

        return $instrument;
    }

    public function getInstrumentByIsinList(array $isinList): MarketInstrumentCollection
    {
        return $this->marketInstrumentService->getInstrumentByIsinList($isinList);
    }

    private function convertBondModelToEntity(BrokerBondModel $bond): MarketBondEntity
    {
        $marketBond = new MarketBondEntity();
        $marketBond->setInitialNominal($bond->getInitialNominal()->getAmount());
        $marketBond->setNominal($bond->getNominal()->getAmount());
        $marketBond->setCurrencyIso($bond->getNominal()->getCurrency());
        $marketBond->setMaturityDate($bond->getMaturityDate());

        return $marketBond;
    }

    public function getIndicatives(): InstrumentCollection
    {
        $broker = $this->stockBrokerFactory->getSourceService(BrokerType::TINKOFF->value);
        return $broker->getIndicatives();
    }

    public function getInstrumentEntityById(int $marketInstrumentId): ?MarketInstrumentEntity
    {
        return $this->marketInstrumentService->getInstrumentEntityById($marketInstrumentId);
    }

    public function setupSourceForUpdate(int $userId, array $sources): void
    {
        foreach ($sources as $source) {
            $adapter = $this->stockBrokerFactory->getSourceService($source);
            $adapter->setInstrumentsForUpdate($userId);
        }
    }

    public function updateInstrument(string $broker, string $instrumentId): ?BrokerInstrumentModel
    {
        $adapter = $this->stockBrokerFactory->getSourceService($broker);
        $instrument = $adapter->updateInstrument($instrumentId);

        if ($instrument === null) {
            return null;
        }

        $instrumentEntity = $this->getInstrumentByIsin($instrument->getIsin());

        if ($instrument->getLogo() !== null) {
            $instrumentEntity->getItem()->setLogo($instrument->getLogo());
        }
        if ($instrument->getRegNumber() !== null) {
            $instrumentEntity->setRegNumber($instrument->getRegNumber());
        }

        $this->itemStorage->addEntity($instrumentEntity->getItem());

        return $instrument;
    }
}
