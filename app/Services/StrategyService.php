<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\CompareCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Models\TargetShareModel;
use App\User\Services\UserService;

class StrategyService
{
    private ShareStrategyFactory $shareStrategyFactory;
    private UserService $userService;

    public function __construct(ShareStrategyFactory $factory, UserService $userService)
    {
        $this->shareStrategyFactory = $factory;
        $this->userService = $userService;
    }

    public function getTargetShare(string $type): ShareCollection
    {
        $shareStrategy = $this->shareStrategyFactory->getShareCalculator($type);
        return $shareStrategy->getTargetShare($this->userService->getUserId());
    }

    public function updateTargetShare(string $type, ShareCollection $shares): void
    {
        $shareStrategy = $this->shareStrategyFactory->getShareCalculator($type);
        $shareStrategy->updateTargetShare($this->userService->getUserId(), $shares);
    }

    public function calculateShare(
        string $type,
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $shareStrategy = $this->shareStrategyFactory->getShareCalculator($type);
        return $this->compareWithTargetShare(
            $shareStrategy->calculateShare($instruments, $balance),
            $shareStrategy->getTargetShare($this->userService->getUserId()),
            $shareStrategy->calculateTotalAmount($instruments, $balance)
        );
    }

    private function compareWithTargetShare(
        ShareCollection $shareCollection,
        ShareCollection $targetShare,
        float $instrumentsAmount
    ): CompareCollection {
        $compareShare = [];
        $collection = clone  $shareCollection;
        /** @var TargetShareModel $share */
        foreach ($targetShare->getIterator() as $share) {
            $instrumentName = $share->getName();
            $targetValue = $share->getValue();

            $shareValue = $collection->get($instrumentName);
            $collection->remove($instrumentName);
            $compareShare[$instrumentName] = [
                'share' => $shareValue,
                'targetShare' => $targetValue,
                'diffPercent' => $shareValue - $targetValue,
                'diffAmount' => ($shareValue - $targetValue) * $instrumentsAmount
            ];
        }

        /**
         * @var  string $instrumentKey
         * @var  float $share
         */
        foreach ($collection->getIterator() as $instrumentKey => $share) {
            $compareShare[$instrumentKey] = [
                'share' => $share,
                'targetShare' => 0,
                'diffPercent' => $share,
                'diffAmount' => $share * $instrumentsAmount
            ];
        }

        return new CompareCollection($compareShare);
    }
}
