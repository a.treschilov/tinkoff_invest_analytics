<?php

declare(strict_types=1);

namespace App\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategyCurrencyBalanceEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\ShareStrategyInterface;
use App\Intl\Storages\CurrencyStorage;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Storages\StrategyCurrencyBalanceStorage;

class BalanceShare implements ShareStrategyInterface
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly StrategyCurrencyBalanceStorage $strategyCurrencyBalanceStorage,
        private readonly CurrencyStorage $currencyStorage,
        private readonly MarketInstrumentServiceInterface $marketInstrumentService
    ) {
    }

    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection {
        $sum = $this->calculateTotalAmount($instruments, $balance);
        $shares = [];

        /** @var InstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $isin = $instrument->getInstrument()->getIsin();
            $currencyEntity = $this->marketInstrumentService->getInstrumentByIsin($isin)->getMarketCurrency();
            $currency = $currencyEntity !== null ? $currencyEntity->getIso() : 'Other';

            if (!isset($shares[$currency])) {
                $shares[$currency] = 0;
            }

            $shares[$currency] += $instrument->getAmount() / $sum;
        }

        ksort($shares);

        return new ShareCollection($shares);
    }

    public function getTargetShare(int $userId): ShareCollection
    {
        $currencies = $this->currencyStorage->findAll();
        $userStrategies = $this->strategyCurrencyBalanceStorage->findActiveByUserId($userId);

        $shareCollection = new ShareCollection();
        foreach ($currencies as $currency) {
            if (!in_array($currency->getIso(), CurrencyShare::ENABLED_CURRENCIES)) {
                continue;
            }
            $share = 0;
            foreach ($userStrategies as $strategy) {
                if ($currency->getIso() === $strategy->getCurrencyIso()) {
                    $share = $strategy->getShare();
                }
            }

            $shareCollection->add(new TargetShareModel([
                'externalId' => $currency->getIso(),
                'name' => $currency->getIso(),
                'value' => $share
            ]));
        }
        return $shareCollection;
    }

    public function updateTargetShare(int $userId, ShareCollection $shares): void
    {
        $toUpdateStrategyEntities = [];
        $currentStrategy = $this->strategyCurrencyBalanceStorage->findActiveByUserId($userId);
        foreach ($currentStrategy as $currencyStrategyEntity) {
            $currencyStrategyEntity->setIsActive(0);
            $currencyStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $currencyStrategyEntity;
        }

        /** @var TargetShareModel $currencyStrategy */
        foreach ($shares->getIterator() as $currencyStrategy) {
            $currencyEntity = $this->currencyStorage->findOneByIso($currencyStrategy->getExternalId());
            $currencyStrategyEntity = new StrategyCurrencyBalanceEntity();
            $currencyStrategyEntity->setUserId($userId);
            $currencyStrategyEntity->setIsActive(1);
            $currencyStrategyEntity->setCurrencyIso($currencyEntity->getIso());
            $currencyStrategyEntity->setShare($currencyStrategy->getValue());
            $currencyStrategyEntity->setCreatedDate($this->currentDateTime);
            $currencyStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $currencyStrategyEntity;
        }

        $this->strategyCurrencyBalanceStorage->updateArrayEntities($toUpdateStrategyEntities);
    }

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float {
        return $instruments->calculateTotalAmount();
    }
}
