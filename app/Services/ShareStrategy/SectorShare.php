<?php

declare(strict_types=1);

namespace App\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategySectorEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\ShareStrategyInterface;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Storages\MarketSectorStorage;
use App\Storages\StrategySectorStorage;
use JetBrains\PhpStorm\Pure;

class SectorShare implements ShareStrategyInterface
{
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $currentDateTime;
    private StrategySectorStorage $strategySectorStorage;
    private MarketSectorStorage $marketSectorStorage;

    public function __construct(
        \DateTime $currentDateTime,
        MarketInstrumentServiceInterface $marketInstrumentService,
        StrategySectorStorage $strategySectorStorage,
        MarketSectorStorage $marketSectorStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->strategySectorStorage = $strategySectorStorage;
        $this->marketSectorStorage = $marketSectorStorage;
    }

    /**
     * @param InstrumentCollection $instruments
     * @return ShareCollection
     */
    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection {
         $sum = $instruments->calculateTotalAmount();

        /** @var InstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $isin = $instrument->getInstrument()->getIsin();
            $marketInstrument = $this->marketInstrumentService->getInstrumentByIsin($isin);
            $sector = $marketInstrument->getMarketStock()?->getSector();

            if (!isset($shares[$sector])) {
                $shares[$sector] = 0;
            }

            $shares[$sector] += $instrument->getAmount() / $sum;
        }

        ksort($shares);

        return new ShareCollection($shares);
    }

    public function getTargetShare(int $userId): ShareCollection
    {
        $sectors = $this->marketSectorStorage->findAll();
        $strategyList = $this->strategySectorStorage->findActiveByUserId($userId);

        $shareCollection = new ShareCollection();
        foreach ($sectors as $sector) {
            $share = 0;

            foreach ($strategyList as $strategy) {
                if ($strategy->getMarketSectorId() === $sector->getId()) {
                    $share = $strategy->getShare();
                }
            }

            $shareCollection->add(new TargetShareModel([
                'externalId' => $sector->getExternalId(),
                'name' => $sector->getName(),
                'value' => $share
            ]));
        }
        return $shareCollection;
    }

    public function updateTargetShare(int $userId, ShareCollection $shares): void
    {
        $toUpdateStrategyEntities = [];
        $currentStrategy = $this->strategySectorStorage->findActiveByUserId($userId);
        foreach ($currentStrategy as $sectorStrategyEntity) {
            $sectorStrategyEntity->setIsActive(0);
            $sectorStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $sectorStrategyEntity;
        }

        /** @var TargetShareModel $currencyStrategy */
        foreach ($shares->getIterator() as $currencyStrategy) {
            $sectorEntity = $this->marketSectorStorage->findOneByExternalId($currencyStrategy->getExternalId());
            $sectorStrategyEntity = new StrategySectorEntity();
            $sectorStrategyEntity->setUserId($userId);
            $sectorStrategyEntity->setIsActive(1);
            $sectorStrategyEntity->setMarketSectorId($sectorEntity->getId());
            $sectorStrategyEntity->setMarketSector($sectorEntity);
            $sectorStrategyEntity->setShare($currencyStrategy->getValue());
            $sectorStrategyEntity->setCreatedDate($this->currentDateTime);
            $sectorStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $sectorStrategyEntity;
        }

        $this->strategySectorStorage->updateArrayEntities($toUpdateStrategyEntities);
    }

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float {
        return $instruments->filteredByInstrumentType(InstrumentModel::TYPE_STOCK)->calculateTotalAmount();
    }
}
