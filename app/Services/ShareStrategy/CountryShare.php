<?php

declare(strict_types=1);

namespace App\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategyCountryEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\ShareStrategyInterface;
use App\Intl\Services\CountryService;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Storages\MarketCountryStorage;
use App\Storages\StrategyCountryStorage;

class CountryShare implements ShareStrategyInterface
{
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private \DateTime $currentDateTime;
    private CountryService $countryService;
    private StrategyCountryStorage $strategyCountryStorage;
    private MarketCountryStorage $marketCountryStorage;

    public function __construct(
        \DateTime $currentDateTime,
        MarketInstrumentServiceInterface $marketInstrumentService,
        CountryService $countryService,
        StrategyCountryStorage $strategyCountryStorage,
        MarketCountryStorage $marketCountryStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->strategyCountryStorage = $strategyCountryStorage;
        $this->marketCountryStorage = $marketCountryStorage;
        $this->countryService = $countryService;
    }

    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection {
        $instruments = $instruments->filteredByInstrumentType(InstrumentModel::TYPE_STOCK);
        $sum = $instruments->calculateTotalAmount();

        /** @var InstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $isin = $instrument->getInstrument()->getIsin();
            $marketInstrument = $this->marketInstrumentService->getInstrumentByIsin($isin);
            $country = $marketInstrument->getMarketStock()?->getCountry()->getName();
            $country = $this->convertCountry($country);

            if (!isset($shares[$country])) {
                $shares[$country] = 0;
            }

            $shares[$country] += $instrument->getAmount() / $sum;
        }

        ksort($shares);

        return new ShareCollection($shares);
    }

    public function getTargetShare(int $userId): ShareCollection
    {
        $countryList = $this->marketCountryStorage->findAll();
        $strategyList = $this->strategyCountryStorage->findActiveByUserId($userId);

        $result = new ShareCollection();
        foreach ($countryList as $country) {
            $share = 0;

            foreach ($strategyList as $strategy) {
                if ($strategy->getMarketCountryId() === $country->getId()) {
                    $share = $strategy->getShare();
                }
            }

            $result->add(new TargetShareModel([
                'externalId' => $country->getIso(),
                'name' => $this->countryService->getCountryByIso($country->getIso())?->getName(),
                'value' => $share
            ]));
        }
        return $result;
    }

    public function updateTargetShare(int $userId, ShareCollection $shares): void
    {
        $toUpdateStrategyEntities = [];
        $currentStrategy = $this->strategyCountryStorage->findActiveByUserId($userId);
        foreach ($currentStrategy as $countryStrategyEntity) {
            $countryStrategyEntity->setIsActive(0);
            $countryStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $countryStrategyEntity;
        }

        /** @var TargetShareModel $currencyStrategy */
        foreach ($shares->getIterator() as $currencyStrategy) {
            $countryEntity = $this->marketCountryStorage->findOneByIso($currencyStrategy->getExternalId());
            $countryStrategyEntity = new StrategyCountryEntity();
            $countryStrategyEntity->setUserId($userId);
            $countryStrategyEntity->setIsActive(1);
            $countryStrategyEntity->setMarketCountryId($countryEntity->getId());
            $countryStrategyEntity->setMarketCountry($countryEntity);
            $countryStrategyEntity->setShare($currencyStrategy->getValue());
            $countryStrategyEntity->setCreatedDate($this->currentDateTime);
            $countryStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $countryStrategyEntity;
        }

        $this->strategyCountryStorage->updateArrayEntities($toUpdateStrategyEntities);
    }

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float {
        return $instruments->filteredByInstrumentType(InstrumentModel::TYPE_STOCK)->calculateTotalAmount();
    }

    private function convertCountry(string $country): string
    {
        return match ($country) {
            'Ireland' => 'United Kingdom',
            'Taiwan, Province of China', 'Hong Kong' => 'China',
            default => $country
        };
    }
}
