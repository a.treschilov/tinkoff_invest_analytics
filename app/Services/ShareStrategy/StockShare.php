<?php

declare(strict_types=1);

namespace App\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategyInstrumentTypeEntity;
use App\Exceptions\UnsupportedInstrumentException;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\ShareStrategyInterface;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Storages\InstrumentTypeStorage;
use App\Storages\StrategyInstrumentTypeStorage;

class StockShare implements ShareStrategyInterface
{
    public static $GOLD = ['TGLD', 'FXGD', 'VTBG', 'GLDRUB_TOM'];

    private MarketInstrumentServiceInterface $marketInstrumentService;
    private StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage;
    private InstrumentTypeStorage $instrumentTypeStorage;
    private \DateTime $currentDateTime;

    public function __construct(
        \DateTime $currentDateTime,
        MarketInstrumentServiceInterface $marketInstrumentService,
        StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage,
        InstrumentTypeStorage $instrumentTypeStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->strategyInstrumentTypeStorage = $strategyInstrumentTypeStorage;
        $this->instrumentTypeStorage = $instrumentTypeStorage;
    }

    public function getTargetShare(int $userId): ShareCollection
    {
        $result = [];

        $instrumentTypes = $this->instrumentTypeStorage->findActive();
        $userShares = $this->strategyInstrumentTypeStorage->findActiveByUserId($userId);

        $shares = [];
        foreach ($userShares as $share) {
            $shares[$share->getInstrumentType()->getId()] = $share;
        }

        foreach ($instrumentTypes as $instrumentType) {
            $value = isset($shares[$instrumentType->getId()])
                ? $shares[$instrumentType->getId()]->getShare()
                : 0;
            $result[] = new TargetShareModel([
                'name' => $instrumentType->getName(),
                'value' => $value,
                'externalId' => $instrumentType->getExternalId()
            ]);
        }

        return new ShareCollection($result);
    }

    public function updateTargetShare(int $userId, ShareCollection $shares): void
    {
        $currentShares = $this->strategyInstrumentTypeStorage->findActiveByUserId($userId);

        $toUpdateShares = [];
        foreach ($currentShares as $share) {
            $share->setIsActive(0);
            $share->setUpdatedDate($this->currentDateTime);
            $toUpdateShares[] = $share;
        }

        /** @var TargetShareModel $share */
        foreach ($shares as $share) {
            $instrumentType = $this->instrumentTypeStorage->findByExternalId($share->getExternalId());

            $entity = new StrategyInstrumentTypeEntity();
            $entity->setUserId($userId);
            $entity->setIsActive(1);
            $entity->setShare($share->getValue());
            $entity->setInstrumentTypeId($instrumentType->getId());
            $entity->setInstrumentType($instrumentType);
            $entity->setCreatedDate($this->currentDateTime);
            $entity->setUpdatedDate($this->currentDateTime);

            $toUpdateShares[] = $entity;
        }

        $this->strategyInstrumentTypeStorage->updateArrayEntities($toUpdateShares);
    }

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float {
        return $instruments->calculateTotalAmount() + $balance->calculateAmountByCurrency('RUB');
    }

    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection {
        $sum = $this->calculateTotalAmount($instruments, $balance);
        $share = [];

        $rubleBalance = $balance->calculateAmountByCurrency('RUB');
        $share['Currency'] = $rubleBalance / $sum;

        /** @var InstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $amount = $instrument->getAmount();
            switch (true) {
                case $this->isStock($instrument->getInstrument()):
                    $share['Stock'] = $share['Stock'] ?? 0;
                    $share['Stock'] += $amount / $sum;
                    break;
                case $this->isGold($instrument->getInstrument()):
                    $share['Gold'] = $share['Gold'] ?? 0;
                    $share['Gold'] += $amount / $sum;
                    break;
                case $this->isBond($instrument->getInstrument()):
                    $share['Bond'] = $share['Bond'] ?? 0;
                    $share['Bond'] += $amount / $sum;
                    break;
                case $this->isFederalBond($instrument->getInstrument()):
                    $share['Federal Bond'] = $share['Federal Bond'] ?? 0;
                    $share['Federal Bond'] += $amount / $sum;
                    break;
                case $this->isEtf($instrument->getInstrument()):
                    $share['Etf'] = $share['Etf'] ?? 0;
                    $share['Etf'] += $amount / $sum;
                    break;
                case $this->isCurrency($instrument->getInstrument()):
                    $share['Currency'] = $share['Currency'] ?? 0;
                    $share['Currency'] += $amount / $sum;
                    break;
                default:
                    throw new UnsupportedInstrumentException(
                        'Unknown Instrument in portfolio. Isin: ' . $instrument->getInstrument()->getIsin()
                    );
            }
        };



        return new ShareCollection($share);
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isBond(BrokerPortfolioPositionModel $instrument): bool
    {
        return $instrument->getInstrumentType() === 'Bond' && !$this->isFederalBond($instrument);
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isStock(BrokerPortfolioPositionModel $instrument): bool
    {
        return $instrument->getInstrumentType() === 'Stock';
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isCurrency(BrokerPortfolioPositionModel $instrument): bool
    {
        return $instrument->getInstrumentType() === 'Currency';
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isGold(BrokerPortfolioPositionModel $instrument): bool
    {
        if (!in_array($instrument->getInstrumentType(), ['Etf', 'Currency'])) {
            return false;
        }

        $instrumentEntity = $this->marketInstrumentService->getInstrumentByIsin($instrument->getIsin());
        return in_array($instrumentEntity->getTicker(), self::$GOLD);
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isFederalBond(BrokerPortfolioPositionModel $instrument): bool
    {
        if ($instrument->getInstrumentType() !== 'Bond') {
            return false;
        }

        $instrumentEntity = $this->marketInstrumentService->getInstrumentByIsin($instrument->getIsin());
        return str_starts_with($instrumentEntity->getTicker(), 'SU2')
                || str_starts_with($instrumentEntity->getTicker(), 'XS0') //Russia euro bonds
                || str_starts_with($instrumentEntity->getTicker(), 'RU000A101R') // Kazakhstan
                || str_starts_with($instrumentEntity->getTicker(), 'RU000A100D'); // Belarus
    }

    /**
     * @param BrokerPortfolioPositionModel $instrument
     * @return bool
     */
    private function isEtf(BrokerPortfolioPositionModel $instrument): bool
    {
        if ($instrument->getInstrumentType() !== 'Etf') {
            return false;
        }
        $instrumentEntity = $this->marketInstrumentService->getInstrumentByIsin($instrument->getIsin());
        return !in_array($instrumentEntity->getTicker(), self::$GOLD);
    }
}
