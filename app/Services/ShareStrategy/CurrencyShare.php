<?php

declare(strict_types=1);

namespace App\Services\ShareStrategy;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;
use App\Entities\StrategyCurrencyEntity;
use App\Interfaces\ShareStrategyInterface;
use App\Intl\Storages\CurrencyStorage;
use App\Models\InstrumentModel;
use App\Models\TargetShareModel;
use App\Storages\StrategyCurrencyStorage;

class CurrencyShare implements ShareStrategyInterface
{
    public const ENABLED_CURRENCIES = ['RUB', 'USD', 'EUR', 'HKD', 'CNY', 'JPY'];
    private StrategyCurrencyStorage $strategyCurrencyStorage;
    private CurrencyStorage $currencyStorage;
    private \DateTime $currentDateTime;

    public function __construct(
        \DateTime $currentDateTime,
        StrategyCurrencyStorage $strategyCurrencyStorage,
        CurrencyStorage $currencyStorage
    ) {
        $this->strategyCurrencyStorage = $strategyCurrencyStorage;
        $this->currencyStorage = $currencyStorage;
        $this->currentDateTime = $currentDateTime;
    }

    /**
     * @param InstrumentCollection $instruments
     * @return ShareCollection
     * @throws \Exception
     */
    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection {
        $sum = $instruments->calculateTotalAmount();

        /** @var InstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $currency = $instrument->getInstrument()->getAveragePositionPrice()->getCurrency();
            if (!isset($shares[$currency])) {
                $shares[$currency] = 0;
            }

            $shares[$currency] += $instrument->getAmount() / $sum;
        }

        ksort($shares);

        return new ShareCollection($shares);
    }

    public function getTargetShare(int $userId): ShareCollection
    {
        $shares = [];
        $currencies = $this->currencyStorage->findAll();
        $currencyShares = $this->strategyCurrencyStorage->findActiveByUserId($userId);

        foreach ($currencies as $currency) {
            if (!in_array($currency->getIso(), self::ENABLED_CURRENCIES)) {
                continue;
            }
            $issetShare = false;
            foreach ($currencyShares as $share) {
                if ($currency->getId() === $share->getCurrencyId()) {
                    $shares[] = new TargetShareModel([
                        'name' => $currency->getIso(),
                        'value' => $share->getShare(),
                        'externalId' => $currency->getIso()
                    ]);
                    $issetShare = true;
                }
            }

            if ($issetShare === false) {
                $shares[] = new TargetShareModel([
                    'name' => $currency->getIso(),
                    'value' => 0,
                    'externalId' => $currency->getIso()
                ]);
            }
        }

        return new ShareCollection($shares);
    }

    public function updateTargetShare(int $userId, ShareCollection $shares): void
    {
        $toUpdateStrategyEntities = [];
        $currentStrategy = $this->strategyCurrencyStorage->findActiveByUserId($userId);
        foreach ($currentStrategy as $currencyStrategyEntity) {
            $currencyStrategyEntity->setIsActive(0);
            $currencyStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $currencyStrategyEntity;
        }

        /** @var TargetShareModel $currencyStrategy */
        foreach ($shares->getIterator() as $currencyStrategy) {
            $currencyEntity = $this->currencyStorage->findOneByIso($currencyStrategy->getExternalId());
            $currencyStrategyEntity = new StrategyCurrencyEntity();
            $currencyStrategyEntity->setUserId($userId);
            $currencyStrategyEntity->setIsActive(1);
            $currencyStrategyEntity->setCurrencyId($currencyEntity->getId());
            $currencyStrategyEntity->setCurrency($currencyEntity);
            $currencyStrategyEntity->setShare($currencyStrategy->getValue());
            $currencyStrategyEntity->setCreatedDate($this->currentDateTime);
            $currencyStrategyEntity->setUpdatedDate($this->currentDateTime);
            $toUpdateStrategyEntities[] = $currencyStrategyEntity;
        }

        $this->strategyCurrencyStorage->updateArrayEntities($toUpdateStrategyEntities);
    }

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float {
        return $instruments->calculateTotalAmount();
    }
}
