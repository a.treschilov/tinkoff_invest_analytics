<?php

declare(strict_types=1);

namespace App\Services;

use App\Interfaces\MarketOperationServiceInterface;
use App\Item\Entities\ItemOperationsImportEntity;
use App\Item\Storages\ItemOperationImportStorage;
use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;
use App\User\Services\UserService;

class MarketOperationServiceFillDecorator implements MarketOperationServiceInterface
{
    private MarketOperationsService $marketOperationsService;
    private UserService $userService;
    private \DateTime $currentDateTime;
    private ItemOperationImportStorage $itemOperationImportStorage;

    public function __construct(
        \DateTime $currentDateTime,
        UserService $userService,
        MarketOperationsService $marketOperationsService,
        ItemOperationImportStorage $itemOperationImportStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->userService = $userService;
        $this->marketOperationsService = $marketOperationsService;
        $this->itemOperationImportStorage = $itemOperationImportStorage;
    }

    public function importBrokerOperations(
        OperationFiltersModel $operationFilter,
        int $userBrokerAccountId
    ): ImportStatisticModel {
        $statistic = $this->marketOperationsService->importBrokerOperations($operationFilter, $userBrokerAccountId);

        $importEntity = new ItemOperationsImportEntity();
        $importEntity->setImportDateStart($operationFilter->getDateFrom());
        $importEntity->setImportDateEnd($operationFilter->getDateTo());
        $importEntity->setUserBrokerAccountId($userBrokerAccountId);
        $importEntity->setUserId($this->userService->getUserId());
        $importEntity->setCreatedDate($this->currentDateTime);
        $importEntity->setAdded($statistic->getAdded());
        $importEntity->setUpdated($statistic->getUpdated());
        $importEntity->setSkipped($statistic->getSkipped());
        $this->itemOperationImportStorage->addEntity($importEntity);

        return $statistic;
    }
}
