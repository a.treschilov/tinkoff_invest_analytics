<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Collections\PortfolioPositionCollection;
use App\Broker\Models\BrokerPortfolioBalanceModel;
use App\Broker\Models\BrokerPortfolioModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\CompareCollection;
use App\Collections\InstrumentCollection;
use App\Collections\PortfolioCollection;
use App\Exceptions\ExchangeUnsupportedCurrencyException;
use App\Models\InstrumentModel;
use Exception;

class PortfolioService
{
    private StrategyService $strategyService;
    private ExchangeCurrencyService $exchangeCurrency;
    private \DateTime $currentDateTime;

    public function __construct(
        \DateTime $currentDateTime,
        StrategyService $strategyService,
        ExchangeCurrencyService $exchangeCurrency
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->strategyService = $strategyService;
        $this->exchangeCurrency = $exchangeCurrency;
    }

    public function calculateInstrumentShare(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        return $this->strategyService->calculateShare('stock', $instrumentCollection, $balance);
    }

    public function calculateTotalAmount(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): float {
        if ($portfolio->getPositions()->count() === 0) {
            return 0;
        }
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        $amount = $instrumentCollection->calculateTotalAmount();

        /** @var BrokerPortfolioBalanceModel $currency */
        foreach ($balance->getIterator() as $currency) {
            if ($currency->getCurrency() === 'RUB') {
                $amount += $currency->getAmount() - $currency->getBlockedAmount();
            }
        }
        return $amount;
    }

    public function calculateCurrencyShare(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        $instrumentTypes = [
            InstrumentModel::TYPE_STOCK,
            InstrumentModel::TYPE_ETF,
            InstrumentModel::TYPE_BOND,
            InstrumentModel::TYPE_FUTURES
        ];
        $filteredShareInstrumentCollection = new InstrumentCollection();
        foreach ($instrumentTypes as $type) {
            $typeCollection = $instrumentCollection->filteredByInstrumentType($type);
            foreach ($typeCollection->getIterator() as $instrument) {
                $filteredShareInstrumentCollection->add($instrument);
            }
        }
        return $this->strategyService->calculateShare('currency', $filteredShareInstrumentCollection, $balance);
    }

    public function calculateSectorShare(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        $instrumentCollection = $instrumentCollection->filteredByInstrumentType(InstrumentModel::TYPE_STOCK);
        return $this->strategyService->calculateShare('sector', $instrumentCollection, $balance);
    }

    public function calculateCountryShare(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        return $this->strategyService->calculateShare('country', $instrumentCollection, $balance);
    }

    public function calculateCurrencyBalance(
        BrokerPortfolioModel $portfolio,
        PortfolioBalanceCollection $balance
    ): CompareCollection {
        $instrumentCollection = $this->convertToRub($portfolio->getPositions());
        $currencyCollection = $instrumentCollection->filteredByInstrumentType('Currency');
        return $this->strategyService->calculateShare('currencyBalance', $currencyCollection, $balance);
    }

    /**
     * @param PortfolioPositionCollection $instruments
     * @return InstrumentCollection
     * @throws ExchangeUnsupportedCurrencyException
     */
    public function convertToRub(PortfolioPositionCollection $instruments): InstrumentCollection
    {
        $exchangeRate = [
            'RUB' => 1
        ];

        $instrumentCollection = new InstrumentCollection();

        /** @var BrokerPortfolioPositionModel $position */
        foreach ($instruments as $position) {
            if (null === $position->getAveragePositionPrice()) {
                continue;
            }

            if (
                $position->getAveragePositionPrice()->getCurrency() === ''
                && $position->getAveragePositionPrice()->getAmount() === 0.0
            ) {
                continue;
            }
            if (!isset($exchangeRate[$position->getAveragePositionPrice()->getCurrency()])) {
                $exchangeRate[$position->getAveragePositionPrice()->getCurrency()] =
                    $this->exchangeCurrency->getExchangeRate(
                        $position->getAveragePositionPrice()->getCurrency(),
                        'RUB',
                        $this->currentDateTime
                    );
            }

            $amount = $position->getQuantity() * $position->getCurrentPrice()->getAmount();
            $instrumentCollection->add(new InstrumentModel(array(
                'instrument' => $position,
                'amount' => $amount * $exchangeRate[$position->getAveragePositionPrice()->getCurrency()]
            )));
        }

        return $instrumentCollection;
    }

    public function mergePortfolios(PortfolioCollection $portfolioCollection): BrokerPortfolioModel
    {
        $positions = [];

        /** @var BrokerPortfolioModel $portfolio */
        foreach ($portfolioCollection->getIterator() as $portfolio) {
            $positions = array_merge($positions, $portfolio->getPositions()->toArray());
        }

        return new BrokerPortfolioModel(['positions' => new PortfolioPositionCollection($positions)]);
    }
}
