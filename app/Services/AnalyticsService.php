<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Common\Amqp\AmqpClient;
use App\Entities\WealthHistoryEntity;
use App\Exceptions\MarketDataException;
use App\Item\Models\PortfolioModel;
use App\Item\Services\ItemService;
use App\Item\Services\PortfolioBuilderService;
use App\Item\Types\ItemTypes;
use App\Market\Interfaces\CandleServiceInterface;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Services\PortfolioService as ItemPortfolioServiceAlias;
use App\Models\PriceModel;
use App\Models\WealthHistoryModel;
use App\Storages\WealthHistoryStorage;
use App\User\Services\UserService;

class AnalyticsService
{
    public function __construct(
        private readonly WealthHistoryStorage $wealthHistoryStorage,
        private readonly CandleServiceInterface $candleService,
        private readonly ExchangeCurrencyService $exchangeCurrencyService,
        private readonly \DateTime $currentDateTime,
        private readonly UserService $userService,
        private readonly ItemPortfolioServiceAlias $itemPortfolioService,
        private readonly OperationService $operationService,
        private readonly PortfolioBuilderService $portfolioBuilderService,
        private readonly AmqpClient $amqpClient,
        private readonly WealthService $wealthService,
        private readonly ItemService $itemService
    ) {
    }

    public function getSummary(): array
    {
        $user = $this->userService->getUser();
        if ($user === null) {
            return [];
        }

        $baseCurrency = 'RUB';

        $userSettings = $this->userService->getUserSettings($user->getId());
        $userId = $user->getId();

        $safeRatio = $this->calculateSafeRatio(
            $user->getId(),
            (clone $this->currentDateTime)->sub(new \DateInterval('P1Y')),
            $this->currentDateTime
        );

        $wealth1DayBefore = $this->wealthService->getLastWealth();
        $capital1DayBefore = $wealth1DayBefore !== null
            ? $this->wealthService->calculateCapital($wealth1DayBefore)
            : null;

        $monthBefore = clone $this->currentDateTime;
        $monthBefore->sub(new \DateInterval('P1M'));
        $wealth1MonthBefore = $this->wealthService->getWealthByDay($userId, $monthBefore);
        $capital1MonthBefore = $wealth1MonthBefore !== null
            ? $this->wealthService->calculateCapital($wealth1MonthBefore)
            : null;

        $yearBefore = clone $this->currentDateTime;
        $yearBefore->sub(new \DateInterval('P1Y'));
        $wealth1YearBefore = $this->wealthService->getWealthByDay($userId, $yearBefore);
        $capital1YearBefore = $wealth1YearBefore !== null
            ? $this->wealthService->calculateCapital($wealth1YearBefore)
            : null;

        $wealth = $this->calculateUserPortfolioSummary($userId, $this->currentDateTime);

        $actives = $this->wealthService->calculateActives($wealth);
        $passives = $this->wealthService->calculatePassives($wealth);
        $capital = $this->wealthService->calculateCapital($wealth);

        $targetCapital = $userSettings !== null ? $userSettings->getDesiredPension()->getAmount() / 0.04 * 12 : null;
        if ($targetCapital !== null) {
            $targetCapitalRate = $this->exchangeCurrencyService->getExchangeRate(
                $userSettings->getDesiredPension()->getCurrency(),
                $baseCurrency,
                $this->currentDateTime
            );
            $targetCapital = $targetCapital * $targetCapitalRate;
        }
        $shouldIncrease = $targetCapital - $actives - $wealth->getLoan();

        $yearBefore = clone  $this->currentDateTime;
        $yearBefore->sub(new \DateInterval('P1Y'));
        $lastYearInterest = $this->operationService
            ->calculateIncome($userId, $yearBefore, $this->currentDateTime);

        $yearLater = clone  $this->currentDateTime;
        $yearLater->add(new \DateInterval('P1Y'));
        $predictInterest = $this->itemPortfolioService
            ->calculateForecastIncome($userId, $this->currentDateTime, $yearLater);

        $pension = $capital * 0.04 / 12;
        $futurePension = ($capital + $predictInterest) * 0.04 / 12;

        if (null === $userSettings || null === $userSettings->getRetirementAge() || null === $user->getBirthday()) {
            $saveSpeed = null;
            $wealthRate = null;
        } else {
            $expenses = null;
            if ($userSettings->getExpenses()?->getAmount() !== null && $userSettings->getExpenses()->getAmount() > 0) {
                $expensesRate = $this->exchangeCurrencyService->getExchangeRate(
                    $userSettings->getExpenses()->getCurrency(),
                    $baseCurrency,
                    $this->currentDateTime
                );
                $expenses = $expensesRate * $userSettings->getExpenses()->getAmount();
            }

            $retirementDate = clone $user->getBirthday();
            $retirementDate->add(new \DateInterval('P' . $userSettings->getRetirementAge() . 'Y'));
            $saveSpeed = $shouldIncrease / $retirementDate->diff($this->currentDateTime)->days * 365 / 12;
            $wealthRate = $expenses !== null
                ? ($actives - $passives) / ($expenses * 12)
                : null;
        }

        return [
            'targetCapital' => $targetCapital,
            'actives' => $actives,
            'passives' => $wealth->getLoan() === 0.0 ? 0 : -1 * $wealth->getLoan(),
            'shouldIncrease' => $shouldIncrease,
            'saveSpeed' => $saveSpeed,
            'pension' => $pension,
            'futurePension' => $futurePension,
            'capital' => $capital,
            'predictInterest' => $predictInterest,
            'lastYearInterest' => $lastYearInterest,
            'wealthRate' => $wealthRate,
            'safeRatio' => $safeRatio,
            'capitalBefore' => [
                'day' => $capital1DayBefore === null
                    ? null
                    : new PriceModel(['amount' => $capital - $capital1DayBefore, 'currency' => $baseCurrency]),
                'month' => $capital1MonthBefore === null
                    ? null
                    : new PriceModel(['amount' => $capital - $capital1MonthBefore, 'currency' => $baseCurrency]),
                'year' => $capital1YearBefore === null
                    ? null
                    : new PriceModel(['amount' => $capital - $capital1YearBefore, 'currency' => $baseCurrency])
            ]
        ];
    }

    public function saveUserHistoryData(int $userId, \DateTime $date): void
    {
        $currency = 'RUB';

        $portfolio = $this->itemPortfolioService->buildPortfolio($userId, $date);
        $wealth = $this->getWealModelByPortfolio($userId, $portfolio, $currency, $date);

        $wealthEntity = new WealthHistoryEntity();
        $wealthEntity->setUserId($userId);
        $wealthEntity->setAssets($wealth->getAssets());
        $wealthEntity->setStock($wealth->getStock());
        $wealthEntity->setDeposit($wealth->getDeposit());
        $wealthEntity->setRealEstate($wealth->getRealEstate());
        $wealthEntity->setLoan($wealth->getLoan());
        $wealthEntity->setExpenses($wealth->getExpenses());
        $wealthEntity->setCurrencyIso($wealth->getCurrencyIso());
        $wealthEntity->setDate($date);
        $wealthEntity->setIsActive(1);
        $this->wealthHistoryStorage->addEntity($wealthEntity);

        $this->portfolioBuilderService->savePortfolio($userId, $portfolio, $date);
    }

    public function outdatedAnalyticsDates(int $userId): array
    {
        $messages = [];

        $dates = $this->getOutdatedDates($userId);
        if (count($dates) > 0) {
            $startDate = clone $dates[0];
            $endDate = clone $this->currentDateTime;

            $this->setAsOperationsProcessed($userId);
            $this->setOutdatedWealthHistory($userId, $startDate, $endDate);
            $this->portfolioBuilderService->setOutdatedPortfolio($userId, $startDate, $endDate);

            foreach ($dates as $date) {
                $message = [
                    'type' => 'calculateWealthHistory',
                    'data' => [
                        'timestamp' => (clone $date)->getTimestamp(),
                        'userId' => $userId,
                    ]
                ];

                $this->amqpClient->publishMessage($message);

                $messages[] = $message;
            }
        }

        return $messages;
    }

    public function compareWithIndex(
        int $userId,
        \DateTime $dateFrom,
        \DateTime $dateTo,
        array $benchmark
    ): array {
        $itemIsin = $benchmark[0];
        $marketInstrumentId = $this->itemService->getItemByExternalId($itemIsin)->getStock()->getMarketInstrumentId();
        $result = [];

        $candles = $this->candleService->getDayCandleInterval($marketInstrumentId, $dateFrom, $dateTo);
        $firstCandle = $candles->getFirstCandle();
        $previousCandle = $firstCandle;

        $wealthCollection = $this->wealthService->getWealthByPeriod($userId, $dateFrom, $dateTo);
        $firstWealth = $wealthCollection->getFirstWealth();
        $previousWealth = $firstWealth;

        if ($candles->count() === 0) {
            throw new MarketDataException('Benchmark data are empty', 3000);
        }

        if ($wealthCollection->count() === 0) {
            throw new MarketDataException('Wealth history is empty', 3001);
        }

        $ratio = $this->wealthService->calculateCapital($firstWealth) / $firstCandle->getClose();

        $date = clone $dateFrom;
        while ($date <= $dateTo) {
            $wealth = $wealthCollection->findDayWealth($date);
            $wealth = $wealth ?? $previousWealth;

            $candle = $candles->findDayCandle($date);
            $candleValue = $candle?->getClose() ?? $previousCandle->getClose();
            $result[] = [
                'capital' => $this->wealthService->calculateCapital($wealth),
                'time' => $date->getTimestamp(),
                'index' => [
                    [
                        'id' => $itemIsin,
                        'name' => $itemIsin,
                        'value' => $candleValue,
                        'normalizedIndex' => $ratio * $candleValue
                    ]
                ]
            ];

            $previousCandle = $candle ?? $previousCandle;
            $previousWealth = $wealth;
            $date->add(new \DateInterval('P1D'));
        }

        return $result;
    }

    private function calculateSafeRatio(int $userId, \DateTime $dateFrom, \DateTime $dateTo): ?float
    {
        $userSettings = $this->userService->getUserSettings($userId);
        if (
            $userSettings === null
            || $userSettings->getExpenses() === null
            || $userSettings->getExpenses()->getAmount() <= 0
        ) {
            return null;
        }

        $currency = $userSettings->getExpenses()->getCurrency();
        $datesInterval = $dateTo->diff($dateFrom);
        $months = $datesInterval->y * 12 + $datesInterval->m + $datesInterval->d / 30;

        $filter = new OperationFiltersModel([
            'userId' => $userId,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'operationType' => [
                BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                BrokerOperationTypeType::OPERATION_TYPE_SECURITY_OUT,
                BrokerOperationTypeType::OPERATION_TYPE_SECURITY_IN,
                BrokerOperationTypeType::OPERATION_TYPE_INPUT,
                BrokerOperationTypeType::OPERATION_TYPE_OUTPUT
            ]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $filter);
        $operations = $this->operationService->prepareAsSafetyOperations($operations);
        $safeAmount = $this->operationService->calculateOperationSummary($operations, $currency);

        return $safeAmount->getAmount()
            / ($userSettings->getExpenses()->getAmount() * $months + $safeAmount->getAmount());
    }

    /**
     * @param $userId
     * @return \DateTime[]
     */
    private function getOutdatedDates($userId): array
    {
        $filterModel = new OperationFiltersModel([
            'isAnalyticProceed' => 0,
            'dateFrom' => new \DateTime('1950-00-00 00:00:00'),
            'isDeleted' => [0, 1]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $filterModel)->sortByDateAsc();
        if ($operations->count() > 0) {
            /** @var OperationModel $operation */
            $operation = $operations->offsetGet(0);
            $operationDate = clone $operation->getDate();
        } else {
            $operationDate = new \DateTime('2200-01-01 00:00:00');
        }

        $portfolio = $this->portfolioBuilderService->getLastPortfolio($userId, $this->currentDateTime);
        if ($portfolio === null) {
            $portfolioDate = new \DateTime('2018-01-01 00:00:00');
        } else {
            $portfolioDate = clone $portfolio->getDate();
            $portfolioDate->add(new \DateInterval('PT1S'));
        }

        $firstDate = min($operationDate, $portfolioDate);
        $lastDate = clone $this->currentDateTime;

        $dates = [];
        while ($firstDate <= $lastDate) {
            $dates[] = clone $firstDate;
            $firstDate->add(new \DateInterval('P1D'));
        }

        return $dates;
    }

    private function setAsOperationsProcessed(int $userId): void
    {
        $filterModel = new OperationFiltersModel([
            'isAnalyticProceed' => 0,
            'dateFrom' => new \DateTime('1950-00-00 00:00:00'),
            'isDeleted' => [0, 1]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $filterModel)->sortByDateAsc();
        if ($operations->count() > 0) {
            /** @var OperationModel $operation */
            foreach ($operations->getIterator() as $operation) {
                $operation->setIsAnalyticProceed(1);
            }
        }
        $this->operationService->editUserOperationBatch($userId, $operations);
    }

    private function setOutdatedWealthHistory(int $userId, \DateTime $dateFrom, \DateTime $dateTo): void
    {
        $wealthHistoryArray =
            $this->wealthHistoryStorage->findByDateIntervalAndUser($userId, $dateFrom, $dateTo);
        foreach ($wealthHistoryArray as $wealthHistoryEntity) {
            $wealthHistoryEntity->setIsActive(0);
        }
        $this->wealthHistoryStorage->addEntityArray($wealthHistoryArray);
    }

    private function calculateUserPortfolioSummary(int $userId, \DateTime $date): WealthHistoryModel
    {
        $currency = 'RUB';
        $portfolio = $this->itemPortfolioService->buildPortfolio($userId, $date);
        return $this->getWealModelByPortfolio($userId, $portfolio, $currency, $date);
    }

    private function getWealModelByPortfolio(
        int $userId,
        PortfolioModel $portfolio,
        string $currency,
        \DateTime $date
    ): WealthHistoryModel {
        $userSettings = $this->userService->getUserSettings($userId);

        $shares = $this->itemPortfolioService->calculatePortfolioSharesByType($portfolio);

        $expenses = null;
        if ($userSettings?->getExpenses() !== null) {
            $expensesRate = $this->exchangeCurrencyService->getExchangeRate(
                $userSettings->getExpenses()->getCurrency(),
                'RUB',
                $date
            );
            $expenses = $expensesRate * $userSettings->getExpenses()->getAmount();
        }

        return new WealthHistoryModel([
            'currencyIso' => $currency,
            'stock' => $shares[ItemTypes::MARKET]['amount']->getAmount(),
            'assets' => $shares[ItemTypes::CROWD_FUNDING]['amount']->getAmount(),
            'realEstate' => $shares[ItemTypes::REAL_ESTATE]['amount']->getAmount(),
            'deposit' => $shares[ItemTypes::DEPOSIT]['amount']->getAmount(),
            'loan' => $shares[ItemTypes::LOAN]['amount']->getAmount(),
            'expenses' => $expenses
        ]);
    }
}
