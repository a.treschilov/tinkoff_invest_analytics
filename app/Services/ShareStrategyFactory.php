<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\ShareStrategyException;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Interfaces\ShareStrategyInterface;
use App\Intl\Services\CountryService;
use App\Intl\Storages\CurrencyStorage;
use App\Services\ShareStrategy\CountryShare;
use App\Services\ShareStrategy\BalanceShare;
use App\Services\ShareStrategy\CurrencyShare;
use App\Services\ShareStrategy\SectorShare;
use App\Services\ShareStrategy\StockShare;
use App\Storages\InstrumentTypeStorage;
use App\Storages\MarketCountryStorage;
use App\Storages\MarketSectorStorage;
use App\Storages\StrategyCountryStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use App\Storages\StrategyCurrencyStorage;
use App\Storages\StrategyInstrumentTypeStorage;
use App\Storages\StrategySectorStorage;

class ShareStrategyFactory
{
    private \DateTime $currentDateTime;
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private CountryService $countryService;
    private StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage;
    private StrategyCurrencyStorage $strategyCurrencyStorage;
    private StrategyCurrencyBalanceStorage $strategyCurrencyBalanceStorage;
    private StrategyCountryStorage $strategyCountryStorage;
    private StrategySectorStorage $strategySectorStorage;
    private InstrumentTypeStorage $instrumentTypeStorage;
    private CurrencyStorage $currencyStorage;
    private MarketCountryStorage $marketCountryStorage;
    private MarketSectorStorage $marketSectorStorage;

    public function __construct(
        \DateTime $currentDateTime,
        MarketInstrumentServiceInterface $marketInstrumentService,
        CountryService $countryService,
        StrategyInstrumentTypeStorage $strategyInstrumentTypeStorage,
        StrategyCurrencyStorage $strategyCurrencyStorage,
        StrategyCurrencyBalanceStorage $strategyCurrencyBalanceStorage,
        StrategyCountryStorage $strategyCountryStorage,
        StrategySectorStorage $strategySectorStorage,
        CurrencyStorage $currencyStorage,
        InstrumentTypeStorage $instrumentTypeStorage,
        MarketCountryStorage $marketCountryStorage,
        MarketSectorStorage $marketSectorStorage
    ) {
        $this->currentDateTime = $currentDateTime;
        $this->marketInstrumentService = $marketInstrumentService;
        $this->countryService = $countryService;
        $this->strategyInstrumentTypeStorage = $strategyInstrumentTypeStorage;
        $this->strategyCurrencyStorage = $strategyCurrencyStorage;
        $this->strategyCurrencyBalanceStorage = $strategyCurrencyBalanceStorage;
        $this->strategyCountryStorage = $strategyCountryStorage;
        $this->strategySectorStorage = $strategySectorStorage;
        $this->currencyStorage = $currencyStorage;
        $this->instrumentTypeStorage = $instrumentTypeStorage;
        $this->marketCountryStorage = $marketCountryStorage;
        $this->marketSectorStorage = $marketSectorStorage;
    }

    public function getShareCalculator(string $type): ShareStrategyInterface
    {
        switch ($type) {
            case 'currency':
                return new CurrencyShare(
                    $this->currentDateTime,
                    $this->strategyCurrencyStorage,
                    $this->currencyStorage
                );
            case 'stock':
                return new StockShare(
                    $this->currentDateTime,
                    $this->marketInstrumentService,
                    $this->strategyInstrumentTypeStorage,
                    $this->instrumentTypeStorage
                );
            case 'sector':
                return new SectorShare(
                    $this->currentDateTime,
                    $this->marketInstrumentService,
                    $this->strategySectorStorage,
                    $this->marketSectorStorage
                );
            case 'country':
                return new CountryShare(
                    $this->currentDateTime,
                    $this->marketInstrumentService,
                    $this->countryService,
                    $this->strategyCountryStorage,
                    $this->marketCountryStorage
                );
            case 'currencyBalance':
                return new BalanceShare(
                    $this->currentDateTime,
                    $this->strategyCurrencyBalanceStorage,
                    $this->currencyStorage,
                    $this->marketInstrumentService
                );
        }
        throw new ShareStrategyException('Unknown Share Strategy');
    }
}
