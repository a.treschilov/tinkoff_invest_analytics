<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Broker\Models\BrokerPortfolioModel;
use App\Collections\PortfolioCollection;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;
use App\User\Entities\UserCredentialEntity;
use App\User\Models\UserCredentialModel;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserService;

class AccountService
{
    public function __construct(
        private readonly StockBrokerFactory $stockBrokerFactory,
        private readonly PortfolioService $portfolioService,
        private readonly UserBrokerAccountService $userBrokerAccountService,
        private readonly UserService $userService
    ) {
    }

    public function getPortfolio(): BrokerPortfolioModel
    {
        $userBrokerAccounts = $this->getUserBrokerAccounts();
        $portfolioCollection = new PortfolioCollection();

        /** @var UserBrokerAccountEntity $userBrokerAccount */
        foreach ($userBrokerAccounts->getIterator() as $userBrokerAccount) {
            $broker = $this->stockBrokerFactory->getBroker($userBrokerAccount->getUserCredential());
            $portfolioCollection->add($broker->getPortfolio($userBrokerAccount->getExternalId()));
        }

        return $this->portfolioService->mergePortfolios($portfolioCollection);
    }

    public function getPortfolioBalance(): PortfolioBalanceCollection
    {
        $userBrokerAccounts = $this->getUserBrokerAccounts();
        $portfolioBalanceCollection = new PortfolioBalanceCollection();

        /** @var UserBrokerAccountEntity $userBrokerAccount */
        foreach ($userBrokerAccounts->getIterator() as $userBrokerAccount) {
            $broker = $this->stockBrokerFactory->getBroker($userBrokerAccount->getUserCredential());
            $balances = $broker->getPortfolioBalance($userBrokerAccount->getExternalId());
            foreach ($balances->getIterator() as $balance) {
                $portfolioBalanceCollection->add($balance);
            };
        }

        return $portfolioBalanceCollection;
    }

    public function getUserBrokerAccounts(): UserBrokerAccountCollection
    {
        $userId = $this->userService->getUserId();
        return $this->userBrokerAccountService->getUserAccountByUserId($userId);
    }

    public function getCredentialAccounts(UserCredentialModel $credential): UserBrokerAccountCollection
    {
        return $this->userBrokerAccountService
            ->getActiveUserAccountListByCredential($credential->getUserCredentialId());
    }
}
