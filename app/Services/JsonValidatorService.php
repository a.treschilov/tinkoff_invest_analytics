<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\JsonSchemeException;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Validator;

class JsonValidatorService
{
    private Validator $validator;
    private ErrorFormatter $errorFormatter;

    public const SCHEME_PATH = __DIR__ . '/../ValidationSchemes/';
    private const SCHEME_URL = 'https://hakkes.com/schemes/';

    public function __construct(Validator $validator, ErrorFormatter $errorFormatter)
    {
        $this->validator = $validator;
        $this->errorFormatter = $errorFormatter;
    }

    public function validate(string $scheme, $requestBody): bool
    {
        $this->validator->resolver()->registerPrefix(self::SCHEME_URL, self::SCHEME_PATH);

        $data = json_decode(json_encode($requestBody));
        $result = $this->validator->validate($data, self::SCHEME_URL . $scheme);

        if (!$result->isValid()) {
            $message = $this->errorFormatter->format($result->error(), false);
            throw new JsonSchemeException(current($message));
        }

        return true;
    }
}
