<?php

declare(strict_types=1);

namespace App\Services;

use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Market\Interfaces\CandleServiceInterface;
use App\Models\PriceModel;

class AnalyticsRealTimeService
{
    private const DEFAULT_CURRENCY = 'USD';

    public function __construct(
        private readonly AccountService $accountService,
        private readonly CandleServiceInterface $candleService,
        private readonly ExchangeCurrencyService $exchangeCurrencyService,
        private readonly \DateTime $currentDateTime,
    ) {
    }

    public function calculateProfitToday(?string $currency = null): array
    {
        $currency = $currency ?? self::DEFAULT_CURRENCY;
        $portfolio = $this->accountService->getPortfolio();
        $instruments = $portfolio->getPositions();

        $profit = 0;
        $rates = [
            $currency => 1
        ];

        /** @var BrokerPortfolioPositionModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $candle = $this->candleService->getTodayCandle($instrument->getIsin());

            if ($candle === null) {
                continue;
            }

            if (!isset($rates[$instrument->getExpectedYield()->getCurrency()])) {
                $rates[$instrument->getExpectedYield()->getCurrency()] =
                    $this->exchangeCurrencyService->getExchangeRate(
                        $instrument->getExpectedYield()->getCurrency(),
                        $currency,
                        $this->currentDateTime
                    );
            }

            $exchangeRate = $rates[$instrument->getExpectedYield()->getCurrency()];

            $profit += $instrument->getQuantity()
                * $exchangeRate
                * ($candle->getClose() - $candle->getOpen());
        }

        $revenueToday = new PriceModel([
            'amount' => $profit,
            'currency' => $currency
        ]);
        return ['revenueToday' => $revenueToday];
    }
}
