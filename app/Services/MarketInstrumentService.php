<?php

declare(strict_types=1);

namespace App\Services;

use App\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketFutureEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Entities\MarketBondEntity;
use App\Market\Models\MarketBondModel;
use App\Market\Models\MarketFutureModel;
use App\Market\Models\MarketInstrumentModel;
use App\Models\PriceModel;
use App\Market\Storages\MarketInstrumentStorage;

/** @deprecated use App\Market\Services\MarketInstrumentDbService instead */
class MarketInstrumentService implements MarketInstrumentServiceInterface
{
    public function __construct(
        private readonly MarketInstrumentStorage $marketInstrumentStorage
    ) {
    }

    public function getInstrumentByIsin(string $isin): ?MarketInstrumentEntity
    {
        return $this->marketInstrumentStorage->findOneByIsin($isin);
    }

    public function getInstrumentByIsinList(array $isinList): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();
        $instruments = $this->marketInstrumentStorage->findByIsinList($isinList);
        foreach ($instruments as $instrument) {
            $collection->add($this->convertEntityToModel($instrument));
        }

        return $collection;
    }

    public function getStocks(
        ?string $country = null,
        ?string $currency = null,
        ?string $marketSector = null
    ): MarketInstrumentCollection {
        $stockCollection = new MarketInstrumentCollection($this->marketInstrumentStorage->findStocks($currency));

        if (null !== $country) {
            $stockCollection = $stockCollection->filterByCountry($country);
        }

        if (null !== $marketSector) {
            $stockCollection = $stockCollection->filterBySector($marketSector);
        }

        return $stockCollection;
    }

    public function getInstrumentEntityById(int $marketInstrumentId): ?MarketInstrumentEntity
    {
        return $this->marketInstrumentStorage->findById($marketInstrumentId);
    }

    public function getInstrumentByRegNumber(string $regNumber): ?MarketInstrumentModel
    {
        $entity = $this->marketInstrumentStorage->findByRegNumber($regNumber);

        return $entity === null ? null : $this->convertEntityToModel($entity);
    }

    public function getInstrumentByTicker(string $ticker): ?MarketInstrumentModel
    {
        $entity = $this->marketInstrumentStorage->findByTicker($ticker);

        return $entity === null ? null : $this->convertEntityToModel($entity);
    }

    private function convertEntityToModel(MarketInstrumentEntity $entity): MarketInstrumentModel
    {

        return new MarketInstrumentModel([
            'marketInstrumentId' => $entity->getId(),
            'itemId' => $entity->getItem()->getId(),
            'ticker' => $entity->getTicker(),
            'isin' => $entity->getIsin(),
            'minPriceIncrement' => $entity->getMinPriceIncrement(),
            'lot' => $entity->getLot(),
            'currency' => $entity->getCurrency(),
            'name' => $entity->getName(),
            'type' => $entity->getType(),
            'bond' => $entity->getMarketBond() === null
                ? null
                : $this->convertBondEntityToModel($entity->getMarketBond()),
            'future' => $entity->getMarketFuture() === null
                ? null
                : $this->convertFutureEntityToModel($entity->getMarketFuture()),
        ]);
    }

    private function convertBondEntityToModel(MarketBondEntity $entity): MarketBondModel
    {
        return new MarketBondModel([
            'marketBondId' => $entity->getId(),
            'nominal' => new PriceModel([
                'amount' => $entity->getNominal(),
                'currency' => $entity->getCurrencyIso()
            ]),
            'initialNominal' => new PriceModel([
                'amount' => $entity->getInitialNominal(),
                'currency' => $entity->getCurrencyIso()
            ]),
            'maturityDate' => $entity->getMaturityDate()
        ]);
    }

    private function convertFutureEntityToModel(MarketFutureEntity $entity): MarketFutureModel
    {
        return new MarketFutureModel([
            'marketFutureId' => $entity->getId(),
            'futureType' => $entity->getFuturesType(),
            'assetType' => $entity->getAssetType(),
            'expirationDate' => $entity->getExpirationDate()
        ]);
    }
}
