<?php

declare(strict_types=1);

namespace App\Middlewares;

use App\User\Exceptions\AuthTypeException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ServiceMiddleware
{
    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $apiKey = $request->getHeader('X-API-KEY');

        if ($apiKey[0] !== getenv('INTERNAL_SERVICE_API_KEY')) {
            throw new AuthTypeException('Access denied', 2011);
        }

        return $handler->handle($request);
    }
}
