<?php

declare(strict_types=1);

namespace App\Middlewares;

use App\Services\AuthService;
use App\User\Exceptions\AuthTypeException;
use App\User\Services\UserService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

readonly class JwtMiddleware
{
    public function __construct(
        private AuthService $authService,
        private UserService $userService
    ) {
    }

    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $auth = $request->getHeader('Authorization');

        if (!isset($auth[0])) {
            throw new AuthTypeException('User is not authorized', 2012);
        }

        if (!preg_match('/Bearer\s(\S+)/', $auth[0], $matches)) {
            throw new AuthTypeException('User is not authorized', 2011);
        }
        $jwt = $matches[1];

        if (!$this->authService->validateJWT($jwt)) {
            throw new AuthTypeException('User is not authorized', 2011);
        };

        $userData = $this->authService->decodeJWT($jwt);
        $user = $this->userService->getActiveUserById($userData['userId']);
        $this->userService->setUser($user);

        return $handler->handle($request);
    }
}
