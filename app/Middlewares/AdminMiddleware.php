<?php

declare(strict_types=1);

namespace App\Middlewares;

use App\User\Exceptions\AuthTypeException;
use App\User\Exceptions\UserAccessDeniedException;
use App\User\Services\UserService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AdminMiddleware
{
    private const ADMIN_ROLE = 'admin';

    public function __construct(
        private readonly UserService $userService
    ) {
    }

    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $roles = $this->userService->getUserRoles();

        if (!in_array(self::ADMIN_ROLE, $roles)) {
            throw new UserAccessDeniedException('Access denied for user', 2029);
        }

        return $handler->handle($request);
    }
}
