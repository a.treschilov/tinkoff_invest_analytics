<?php

declare(strict_types=1);

use App\Crons\ImportNewsCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportNewsCron.php';

$cron = new ImportNewsCron();
$cron->tryProcess();
