<?php

use App\Crons\ImportStockCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportStockCron.php';

$cron = new ImportStockCron();
$cron->tryProcess();
