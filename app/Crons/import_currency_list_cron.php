<?php

declare(strict_types=1);

use App\Crons\ImportCurrencyListCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportCurrencyListCron.php';

$cron = new ImportCurrencyListCron();
$cron->tryProcess();
