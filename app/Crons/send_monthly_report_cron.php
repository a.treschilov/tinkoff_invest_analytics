<?php

declare(strict_types=1);

use App\Crons\SendMonthlyReportCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/SendMonthlyReportCron.php';

$cron = new SendMonthlyReportCron();
$cron->run();
