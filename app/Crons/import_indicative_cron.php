<?php

declare(strict_types=1);

use App\Crons\ImportIndicativeCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportIndicativeCron.php';

$cron = new ImportIndicativeCron();
$cron->tryProcess();
