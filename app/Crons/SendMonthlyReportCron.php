<?php

declare(strict_types=1);

namespace App\Crons;

use App\User\Services\UserReportService;

class SendMonthlyReportCron extends AbstractCron
{
    private UserReportService $userReportService;
    private \DateTime $currentDateTime;

    public function __construct()
    {
        parent::__construct();
        $this->userReportService = $this->container->get(UserReportService::class);
        $this->currentDateTime = $this->container->get('Now' . \DateTime::class);
    }

    public function run(): void
    {
        $this->currentDateTime->sub(new \DateInterval('P1M'));
        $this->currentDateTime->setDate(
            (int)$this->currentDateTime->format('Y'),
            (int)$this->currentDateTime->format('m'),
            1
        );
        $this->currentDateTime->setTime(0, 0, 0);
        $context = [
            'messages' => $this->userReportService->scheduleMonthlyReportUser($this->currentDateTime)
        ];

        $this->logger->info('Monthly report for users send to queue', $context);
    }
}
