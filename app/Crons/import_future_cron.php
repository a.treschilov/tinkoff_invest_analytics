<?php

declare(strict_types=1);

use App\Crons\ImportFutureCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportFutureCron.php';

$cron = new ImportFutureCron();
$cron->tryProcess();
