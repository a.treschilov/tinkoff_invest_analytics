<?php

declare(strict_types=1);

namespace App\Crons;

use App\Item\Models\ItemFilterModel;
use App\Item\Models\ItemModel;
use App\Item\Services\ItemService;
use App\Item\Types\ItemType;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Services\CandleBrokerService;
use App\User\Services\UserService;

class ImportCandlesCron extends AbstractCron
{
    private MarketInstrumentServiceInterface $marketInstrumentService;
    private CandleBrokerService $candleService;
    private UserService $userService;
    private ItemService $itemService;

    public function __construct()
    {
        parent::__construct();
        $this->candleService = $this->container->get(CandleBrokerService::class);
        $this->userService = $this->container->get(UserService::class);
        $this->itemService = $this->container->get(ItemService::class);
    }

    public function run(): void
    {
        $user = $this->userService->getActiveUserById(1);
        $this->userService->setUser($user);

        $filter = new ItemFilterModel([
            'type' => [ItemType::MARKET],
            'isOutdated' => [false]
        ]);
        $items = $this->itemService->getItemByFilter($filter);

        $toImportInstruments = [];
        /** @var ItemModel $item */
        foreach ($items->getIterator() as $item) {
            if ($item->getStock() !== null) {
                $toImportInstruments[] = $item->getStock()->getMarketInstrumentId();
            }
        }

        $result = $this->candleService->importCandles($toImportInstruments);

        $context = [
            'result' => $result
        ];

        $this->logger->info('Imported candles', $context);
    }
}
