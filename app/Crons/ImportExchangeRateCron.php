<?php

declare(strict_types=1);

namespace App\Crons;

use App\Services\ExchangeCurrencyService;

class ImportExchangeRateCron extends AbstractCron
{
    private ExchangeCurrencyService $exchangeCurrencyService;

    public function __construct()
    {
        parent::__construct();
        $this->exchangeCurrencyService = $this->container->get(ExchangeCurrencyService::class);
    }

    public function run(): void
    {
        $date = new \DateTime();
        $result = $this->exchangeCurrencyService->importExchangeRates($date);

        $context = [
            'result' => $result
        ];

        $this->logger->info('Exchange currency rate imported', $context);
    }
}
