<?php

declare(strict_types=1);

use App\Crons\ImportExchangeRateCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportExchangeRateCron.php';

$cron = new ImportExchangeRateCron();
$cron->tryProcess();
