<?php

declare(strict_types=1);

use App\Crons\UserHistoryUpdateCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/UserHistoryUpdateCron.php';

$cron = new UserHistoryUpdateCron();
$cron->tryProcess();
