<?php

declare(strict_types=1);

namespace App\Crons;

use App\Market\Services\MarketInstrumentImportService;
use App\Market\Types\InstrumentType;

class ImportStockCron extends AbstractCron
{
    private MarketInstrumentImportService $importService;

    public function __construct()
    {
        parent::__construct();
        $this->importService = $this->container->get(MarketInstrumentImportService::class);
    }

    public function run(): void
    {
        $result = $this->importService->import(InstrumentType::STOCK);

        $context = [
            'result' => [
                'importedItems' => $result->getCountImportedItems(),
                'toImport' => $result->getCountToImportItems()
            ]
        ];

        $this->logger->info('Imported stocks', $context);
    }
}
