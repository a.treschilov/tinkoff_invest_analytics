<?php

declare(strict_types=1);

use App\Crons\ImportCandlesCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportCandlesCron.php';

$cron = new ImportCandlesCron();
$cron->tryProcess();
