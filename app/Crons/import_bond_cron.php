<?php

declare(strict_types=1);

use App\Crons\ImportBondCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportBondCron.php';

$cron = new ImportBondCron();
$cron->tryProcess();
