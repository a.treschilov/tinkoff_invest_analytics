<?php

declare(strict_types=1);

namespace App\Crons;

use App\Common\Logger\Factory as LoggerFactory;
use DI\Container;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractCron
{
    protected ?LoggerInterface $logger = null;
    protected ?ContainerInterface $container = null;

    public function __construct()
    {
        require_once __DIR__ . '/../../vendor/autoload.php';
        $container = new Container();
        require __DIR__ . '/../Common/dependencies.php';

        $this->container = $container;
        /** @var LoggerFactory $factory */
        $factory = $this->container->get(LoggerFactory::class);

        $nameParts = explode('\\', static::class);
        $name = str_replace('.php', '', $nameParts[count($nameParts) - 1]);

        $this->logger = $factory->create('cron/' . $name);
    }

    abstract public function run(): void;

    public function tryProcess(): void
    {
        try {
            $this->logger->info('Cron started');
            $this->run();
            $this->logger->info('Cron finished successfully');
        } catch (\Throwable $exception) {
            $this->logger->error(
                "Error in cron: {$exception->getMessage()}",
                [
                    'exception' => $exception,
                ]
            );
        }
    }
}
