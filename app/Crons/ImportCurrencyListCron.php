<?php

declare(strict_types=1);

namespace App\Crons;

use App\Intl\Services\CurrencyService;

class ImportCurrencyListCron extends AbstractCron
{
    private CurrencyService $currencyService;

    public function __construct()
    {
        parent::__construct();
        $this->currencyService = $this->container->get(CurrencyService::class);
    }

    public function run(): void
    {
        $result = $this->currencyService->importCurrencyList();

        $context = [
            'result' => $result
        ];

        $this->logger->info('Currencies imported', $context);
    }
}
