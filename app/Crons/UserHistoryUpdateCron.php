<?php

declare(strict_types=1);

namespace App\Crons;

use App\Services\AnalyticsService;
use App\User\Entities\UserEntity;
use App\User\Services\UserService;

class UserHistoryUpdateCron extends AbstractCron
{
    private UserService $userService;
    private AnalyticsService $analyticsService;

    public function __construct()
    {
        parent::__construct();
        $this->userService = $this->container->get(UserService::class);
        $this->analyticsService = $this->container->get(AnalyticsService::class);
    }

    public function run(): void
    {
        $users = $this->userService->getActiveUserList();

        $savedUsersData = [];
        /** @var UserEntity $user */
        foreach ($users->getIterator() as $user) {
            if ($user->getIsNewUser() === 0) {
                $this->userService->setUser($user);
                $jobId = $this->analyticsService->outdatedAnalyticsDates($user->getId());
                $savedUsersData[] = [
                    'userId' => $user->getId(),
                    'jobId' => $jobId
                ];
            }
        }

        $context = [
            'result' => $savedUsersData
        ];
        $this->logger->info('Updated users', $context);
    }
}
