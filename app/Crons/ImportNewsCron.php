<?php

declare(strict_types=1);

namespace App\Crons;

use App\News\Services\NewsService;

class ImportNewsCron extends AbstractCron
{
    private NewsService $newsService;
    public function __construct()
    {
        parent::__construct();
        $this->newsService = $this->container->get(NewsService::class);
    }

    public function run(): void
    {
        $context = [
            'result' => $this->newsService->parseNews()
        ];

        $this->logger->info('Imported news', $context);
    }
}
