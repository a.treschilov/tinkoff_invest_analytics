<?php

declare(strict_types=1);

namespace App\Crons;

use App\Market\Services\MarketIndicativesService;
use App\User\Entities\UserEntity;
use App\User\Services\UserService;

class ImportIndicativeCron extends AbstractCron
{
    private MarketIndicativesService $marketIndicativesService;
    private UserService $userService;

    public function __construct()
    {
        parent::__construct();
        $this->marketIndicativesService = $this->container->get(MarketIndicativesService::class);
        $this->userService = $this->container->get(UserService::class);
    }
    #[\Override] public function run(): void
    {
        $user = new UserEntity();
        $user->setId(1);
        $this->userService->setUser($user);

        $context = [
            'result' => $this->marketIndicativesService->import()
        ];

        $this->logger->info('Indicatives processed', $context);
    }
}
