<?php

use App\Crons\ImportOperationsCron;

require_once __DIR__ . '/AbstractCron.php';
require_once __DIR__ . '/ImportOperationsCron.php';

$cron = new ImportOperationsCron();
$cron->tryProcess();
