<?php

declare(strict_types=1);

namespace App\Crons;

use App\Services\MarketOperationsService;

class ImportOperationsCron extends AbstractCron
{
    private MarketOperationsService $marketOperationsService;

    public function __construct()
    {
        parent::__construct();
        $this->marketOperationsService = $this->container->get(MarketOperationsService::class);
    }

    public function run(): void
    {
        $context = [
            'result' => $this->marketOperationsService->getOperationsToImport()
        ];

        $this->logger->info('Imported operations', $context);
    }
}
