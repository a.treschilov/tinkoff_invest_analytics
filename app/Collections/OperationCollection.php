<?php

declare(strict_types=1);

namespace App\Collections;

use App\Models\OperationModel;
use Doctrine\Common\Collections\ArrayCollection;

class OperationCollection extends ArrayCollection
{
    public function getSortedByDate(): OperationCollection
    {
        $iterator = $this->getIterator();
        $iterator->uasort(function (OperationModel $a, OperationModel $b) {
            if ($a->getDate() == $b->getDate()) {
                return 0;
            }
            return $a->getDate() > $b->getDate() ? -1 : 1;
        });

        return new OperationCollection(iterator_to_array($iterator, false));
    }

    public function filterByOperationTypes(array $operationTypes): OperationCollection
    {
        $filteredCollection = new OperationCollection();
        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if (in_array($operation->getType(), $operationTypes)) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }

    public function filterByOperationStatus($operationStatus): OperationCollection
    {
        $filteredCollection = new OperationCollection();
        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getStatus() === $operationStatus) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }

    public function filterByMonth(\DateTime $date): OperationCollection
    {
        $filteredCollection = new OperationCollection();

        $startOfMonth = new \DateTime();
        $startOfMonth->setDate((int)$date->format('Y'), (int)$date->format('m'), 1)
            ->setTime(0, 0, 0);

        $endOfMonth = clone $startOfMonth;
        $endOfMonth->add(new \DateInterval('P1M'));

        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getDate() >= $startOfMonth && $operation->getDate() < $endOfMonth) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }

    public function filterByYear(\DateTime $date): OperationCollection
    {
        $filteredCollection = new OperationCollection();

        $startOfYear = new \DateTime();
        $startOfYear->setDate((int)$date->format('Y'), 1, 1)
            ->setTime(0, 0, 0);

        $endOfYear = clone $startOfYear;
        $endOfYear->add(new \DateInterval('P1Y'));

        /** @var OperationModel $operation */
        foreach ($this->getIterator() as $operation) {
            if ($operation->getDate() >= $startOfYear && $operation->getDate() < $endOfYear) {
                $filteredCollection->add($operation);
            }
        }
        return $filteredCollection;
    }
}
