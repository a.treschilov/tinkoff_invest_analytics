<?php

declare(strict_types=1);

namespace App\Collections;

use App\Broker\Models\BrokerCandleModel;
use Doctrine\Common\Collections\ArrayCollection;

class CandleCollection extends ArrayCollection
{
    public function getLastCandle(): ?BrokerCandleModel
    {
        if ($this->count() === 0) {
            return null;
        }
        $iterator = $this->getIterator();

        $iterator->uasort(function (BrokerCandleModel $a, BrokerCandleModel $b) {
            if ($a->getTime() === $b->getTime()) {
                return 0;
            }
            return $a->getTime() > $b->getTime() ? -1 : 1;
        });

        return $iterator->current();
    }

    public function getLastFinalCandle(): ?BrokerCandleModel
    {
        if ($this->count() === 0) {
            return null;
        }

        $iterator = clone $this->filter(function (BrokerCandleModel $candle) {
            return $candle->getIsComplete();
        });


        $iterator = $iterator->getIterator();
        $iterator->uasort(function (BrokerCandleModel $a, BrokerCandleModel $b) {
            if ($a->getTime() === $b->getTime()) {
                return 0;
            }
            return $a->getTime() > $b->getTime() ? -1 : 1;
        });

        return $iterator->current();
    }

    public function getFirstCandle(): ?BrokerCandleModel
    {
        if ($this->count() === 0) {
            return null;
        }
        $iterator = $this->getIterator();

        $iterator->uasort(function (BrokerCandleModel $a, BrokerCandleModel $b) {
            if ($a->getTime() === $b->getTime()) {
                return 0;
            }
            return $a->getTime() < $b->getTime() ? -1 : 1;
        });

        return $iterator->current();
    }

    public function findDayCandle(\DateTime $date): ?BrokerCandleModel
    {
        if ($this->count() === 0) {
            return null;
        }

        return $this->findFirst(function (int $key, BrokerCandleModel $candle) use ($date) {
            return $candle->getTime()->format('Y-m-d') === $date->format('Y-m-d');
        });
    }

    public function filterMainCandles(): CandleCollection
    {
        $candles = new CandleCollection();

        /** @var BrokerCandleModel $candle */
        foreach ($this->getIterator() as $candle) {
            $isExists = false;
            /** @var BrokerCandleModel $resultCandle */
            foreach ($candles->getIterator() as $key => $resultCandle) {
                if (
                    $resultCandle->getTime()->getTimestamp() === $candle->getTime()->getTimestamp()
                    && $resultCandle->getIsComplete() === $candle->getIsComplete()
                    && $resultCandle->getInterval() === $candle->getInterval()
                ) {
                    if ($resultCandle->getVolume() < $candle->getVolume()) {
                        $candles->remove($key);
                    } else {
                        $isExists = true;
                    }
                }
            }

            if (!$isExists) {
                $candles->add($candle);
            }
        }
        return $candles;
    }
}
