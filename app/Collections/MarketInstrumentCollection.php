<?php

declare(strict_types=1);

namespace App\Collections;

use App\Market\Entities\MarketInstrumentEntity;
use Doctrine\Common\Collections\ArrayCollection;

class MarketInstrumentCollection extends ArrayCollection
{
    public function filterByCountry(string $country)
    {
        $collection = new MarketInstrumentCollection();

        /** @var MarketInstrumentEntity $marketInstrument */
        foreach ($this->getIterator() as $marketInstrument) {
            if ($marketInstrument->getMarketStock()?->getCountryIso() === $country) {
                $collection->add($marketInstrument);
            }
        }

        return $collection;
    }

    public function filterBySector(string $sector)
    {
        $collection = new MarketInstrumentCollection();

        /** @var MarketInstrumentEntity $marketInstrument */
        foreach ($this->getIterator() as $marketInstrument) {
            if ($marketInstrument->getMarketStock()?->getSector() === $sector) {
                $collection->add($marketInstrument);
            }
        }

        return $collection;
    }
}
