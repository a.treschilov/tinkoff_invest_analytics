<?php

declare(strict_types=1);

namespace App\Collections;

use App\Models\InstrumentModel;
use Doctrine\Common\Collections\ArrayCollection;

class InstrumentCollection extends ArrayCollection
{
    public function calculateTotalAmount()
    {
        $sum = 0;
        /** @var InstrumentModel $instrument */
        foreach ($this->getIterator() as $instrument) {
            $sum += $instrument->getAmount();
        }

        return $sum;
    }

    public function filteredByInstrumentType(string $type): InstrumentCollection
    {
        $filteredCollection = new InstrumentCollection();

        /** @var InstrumentModel $instrument */
        foreach ($this->getIterator() as $instrument) {
            if ($instrument->getInstrument()->getInstrumentType() === $type) {
                $filteredCollection->add($instrument);
            }
        }

        return $filteredCollection;
    }
}
