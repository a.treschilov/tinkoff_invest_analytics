<?php

declare(strict_types=1);

namespace App\Collections;

use App\Models\OperationAggregateModel;
use Doctrine\Common\Collections\ArrayCollection;

class OperationAggregateCollection extends ArrayCollection
{
    public function calculateSum(): float
    {
        $sum = 0;
        /** @var OperationAggregateModel $operation */
        foreach ($this->getIterator() as $operation) {
            $sum += $operation->getAmount();
        }

        return $sum;
    }
}
