<?php

declare(strict_types=1);

namespace App\Collections;

use App\Models\WealthHistoryModel;
use Doctrine\Common\Collections\ArrayCollection;

class WealthCollection extends ArrayCollection
{
    public function getFirstWealth(): ?WealthHistoryModel
    {
        if ($this->count() === 0) {
            return null;
        }
        $iterator = $this->getIterator();

        $iterator->uasort(function (WealthHistoryModel $a, WealthHistoryModel $b) {
            if ($a->getDate() === $b->getDate()) {
                return 0;
            }
            return $a->getDate() < $b->getDate() ? -1 : 1;
        });

        return $iterator->current();
    }

    public function findDayWealth(\DateTime $date): ?WealthHistoryModel
    {
        if ($this->count() === 0) {
            return null;
        }

        return $this->findFirst(function (int $key, WealthHistoryModel $candle) use ($date) {
            return $candle->getDate()->format('Y-m-d') === $date->format('Y-m-d');
        });
    }
}
