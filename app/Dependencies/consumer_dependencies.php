<?php

declare(strict_types=1);

use App\Consumers\BondConsumer;
use App\Consumers\CandleImportConsumer;
use App\Consumers\ItemOperationImportConsumer;
use App\Consumers\MarketSourceConsumer;
use App\Consumers\MonthlyReportConsumer;
use App\Consumers\OperationChangeConsumer;
use App\Consumers\OutDateItemConsumer;
use App\Consumers\StockConsumer;
use App\Consumers\UpdateItemDataConsumer;
use App\Consumers\UploadingOperationFileConsumer;
use App\Consumers\WealthHistoryImportConsumer;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;
use App\Item\Services\PortfolioService;
use App\Market\Services\CandleDbStoreServiceDecorator;
use App\Market\Services\MarketInstrumentImportService;
use App\Market\Storages\CandleImportStorage;
use App\Services\AnalyticsService;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\MarketOperationServiceFillDecorator;
use App\User\Services\UserReportService;
use App\User\Services\UserService;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(StockConsumer::class, function (ContainerInterface $c) {
    return new StockConsumer(
        $c->get(MarketInstrumentImportService::class)
    );
});

$container->set(ItemOperationImportConsumer::class, function (ContainerInterface $c) {
    return new ItemOperationImportConsumer(
        $c->get(UserService::class),
        $c->get(MarketOperationServiceFillDecorator::class)
    );
});

$container->set(CandleImportConsumer::class, function (ContainerInterface $c) {
    return new CandleImportConsumer(
        $c->get(UserService::class),
        $c->get(CandleDbStoreServiceDecorator::class),
        $c->get(CandleImportStorage::class)
    );
});

$container->set(WealthHistoryImportConsumer::class, function (ContainerInterface $c) {
    return new WealthHistoryImportConsumer(
        $c->get(AnalyticsService::class),
        $c->get(UserService::class)
    );
});

$container->set(MonthlyReportConsumer::class, function (ContainerInterface $c) {
    return new MonthlyReportConsumer(
        $c->get(UserService::class),
        $c->get(UserReportService::class)
    );
});

$container->set(OperationChangeConsumer::class, function (ContainerInterface $c) {
    return new OperationChangeConsumer(
        $c->get(OperationService::class),
        $c->get(PaymentScheduleService::class),
        $c->get(ItemService::class)
    );
});

$container->set(OutDateItemConsumer::class, function (ContainerInterface $c) {
    return new OutDateItemConsumer(
        $c->get(PortfolioService::class),
        $c->get(UserService::class),
        $c->get(ItemService::class)
    );
});

$container->set(MarketSourceConsumer::class, function (ContainerInterface $c) {
    return new MarketSourceConsumer(
        $c->get(UserService::class),
        $c->get(FillMarketInstrumentServiceDecorator::class)
    );
});

$container->set(UpdateItemDataConsumer::class, function (ContainerInterface $c) {
    return new UpdateItemDataConsumer(
        $c->get(OperationService::class),
        $c->get(PaymentScheduleService::class),
        $c->get(ItemService::class),
    );
});

$container->set(BondConsumer::class, function (ContainerInterface $c) {
    return new BondConsumer(
        $c->get(MarketInstrumentImportService::class)
    );
});

$container->set(UploadingOperationFileConsumer::class, function (ContainerInterface $c) {
    return new UploadingOperationFileConsumer(
        $c->get(OperationService::class)
    );
});
