<?php

declare(strict_types=1);

use App\Adapters\Services\MarketTinkoffService;
use App\Assets\Services\AssetService;
use App\Broker\Services\BrokerReportUploadService;
use App\Broker\Services\BrokerService;
use App\Common\Amqp\AmqpClient;
use App\Controllers\Admin\AssetAdminController;
use App\Controllers\Admin\ItemController as AdminItemController;
use App\Controllers\Admin\MarketInstrumentController;
use App\Controllers\AssetController;
use App\Controllers\AuthController;
use App\Controllers\BondController;
use App\Controllers\BrokerController;
use App\Controllers\DepositController;
use App\Controllers\FutureController;
use App\Controllers\HealthCheckController;
use App\Controllers\IntlController;
use App\Controllers\ItemController;
use App\Controllers\LoanController;
use App\Controllers\NewsController;
use App\Controllers\OperationController;
use App\Controllers\PortfolioController;
use App\Controllers\RealEstateController;
use App\Controllers\Service\AnalyticsController as ServiceAnalyticsController;
use App\Controllers\Service\MarketController;
use App\Controllers\Service\OperationController as ServiceOperationController;
use App\Controllers\Service\UserController as ServiceUserController;
use App\Controllers\StockController;
use App\Controllers\UserController;
use App\Controllers\UserCredentialController;
use App\Controllers\UserOnboardingController;
use App\Controllers\UserStrategyShareController;
use App\Deposit\Services\DepositService;
use App\Intl\Services\CountryService;
use App\Item\Services\ItemService;
use App\Item\Services\OperationService;
use App\Item\Services\PaymentScheduleService;
use App\Item\Services\PortfolioService as ItemPortfolioService;
use App\Item\Services\UserItemService;
use App\Loan\Services\LoanService;
use App\Market\Services\CandleDbService;
use App\Market\Services\MarketInstrumentDbService;
use App\Market\Services\MarketInstrumentImportService;
use App\Market\Services\StockService as MarketStockService;
use App\News\Services\NewsService;
use App\RealEstate\Services\RealEstateService;
use App\Services\AccountService;
use App\Services\AnalyticsRealTimeService;
use App\Services\AnalyticsService;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\JsonValidatorService;
use App\Services\MarketOperationsService;
use App\Services\PortfolioService;
use App\Services\StrategyService;
use App\Services\WealthService;
use App\User\Services\UserAccountService;
use App\User\Services\UserBrokerAccountService;
use App\User\Services\UserCodeService;
use App\User\Services\UserCredentialService;
use App\User\Services\UserReportService;
use App\User\Services\UserService;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use App\Controllers\AnalyticsController;

/** @var ContainerInterface $container */
$container->set(PortfolioController::class, function (ContainerInterface $c) {
    return new PortfolioController(
        $c->get(PortfolioService::class),
        $c->get(AccountService::class)
    );
});

$container->set(OperationController::class, function (ContainerInterface $c) {
    return new OperationController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(OperationService::class),
        $c->get(BrokerService::class)
    );
});

$container->set(UserController::class, function (ContainerInterface $c) {
    return new UserController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(UserCodeService::class),
        $c->get(UserAccountService::class)
    );
});

$container->set(AuthController::class, function (ContainerInterface $c) {
    return new AuthController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(UserAccountService::class)
    );
});

$container->set(AnalyticsController::class, function (ContainerInterface $c) {
    return new AnalyticsController(
        $c->get(AnalyticsService::class),
        $c->get(AnalyticsRealTimeService::class),
        $c->get(WealthService::class),
        $c->get(UserService::class)
    );
});

$container->set(StockController::class, function (ContainerInterface $c) {
    return new StockController(
        $c->get(MarketInstrumentImportService::class),
        $c->get(MarketStockService::class),
        $c->get(JsonValidatorService::class),
        $c->get(MarketInstrumentDbService::class),
    );
});

$container->set(UserCredentialController::class, function (ContainerInterface $c) {
    return new UserCredentialController(
        $c->get(UserService::class),
        $c->get(UserCredentialService::class),
        $c->get(UserBrokerAccountService::class),
        $c->get(MarketOperationsService::class),
        $c->get(JsonValidatorService::class)
    );
});

$container->set(UserStrategyShareController::class, function (ContainerInterface $c) {
    return new UserStrategyShareController(
        $c->get(JsonValidatorService::class),
        $c->get(StrategyService::class)
    );
});

$container->set(AssetController::class, function (ContainerInterface $c) {
    return new AssetController(
        $c->get('Now' . DateTime::class),
        $c->get(UserService::class),
        $c->get(ItemPortfolioService::class),
        $c->get(ItemService::class)
    );
});

$container->set(ItemController::class, function (ContainerInterface $c) {
    return new ItemController(
        $c->get(OperationService::class),
        $c->get(UserService::class),
        $c->get(UserItemService::class),
        $c->get(ItemService::class),
        $c->get(ItemPortfolioService::class),
        $c->get(PaymentScheduleService::class),
        $c->get(CandleDbService::class)
    );
});

$container->set(RealEstateController::class, function (ContainerInterface $c) {
    return new RealEstateController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(RealEstateService::class)
    );
});

$container->set(LoanController::class, function (ContainerInterface $c) {
    return new LoanController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(LoanService::class),
        $c->get(PaymentScheduleService::class)
    );
});

$container->set(DepositController::class, function (ContainerInterface $c) {
    return new DepositController(
        $c->get(JsonValidatorService::class),
        $c->get(UserService::class),
        $c->get(DepositService::class),
        $c->get(PaymentScheduleService::class)
    );
});

$container->set(HealthCheckController::class, function (ContainerInterface $c) {
    return new HealthCheckController(
        $c->get('doctrine.orm.entity_manager'),
        $c->get(Logger::class)
    );
});

$container->set(NewsController::class, function (ContainerInterface $c) {
    return new NewsController(
        $c->get(NewsService::class)
    );
});

$container->set(UserOnboardingController::class, function (ContainerInterface $c) {
    return new UserOnboardingController(
        $c->get(UserService::class),
    );
});

$container->set(MarketInstrumentController::class, function (ContainerInterface $c) {
    return new MarketInstrumentController(
        $c->get(UserService::class),
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get(JsonValidatorService::class)
    );
});

$container->set(BrokerController::class, function (ContainerInterface $c) {
    return new BrokerController(
        $c->get(MarketOperationsService::class),
        $c->get(BrokerService::class),
        $c->get(UserService::class),
        $c->get(BrokerReportUploadService::class)
    );
});

$container->set(AssetAdminController::class, function (ContainerInterface $c) {
    return new AssetAdminController(
        $c->get(ItemService::class),
        $c->get(AmqpClient::class)
    );
});

$container->set(ServiceUserController::class, function (ContainerInterface $c) {
    return new ServiceUserController(
        $c->get(UserService::class)
    );
});

$container->set(ServiceOperationController::class, function (ContainerInterface $c) {
    return new ServiceOperationController(
        $c->get(PaymentScheduleService::class)
    );
});

$container->set(ServiceAnalyticsController::class, function (ContainerInterface $c) {
    return new ServiceAnalyticsController(
        $c->get(UserService::class),
        $c->get(UserReportService::class),
        $c->get(AnalyticsRealTimeService::class),
        $c->get(AnalyticsService::class)
    );
});

$container->set(FutureController::class, function (ContainerInterface $c) {
    return new FutureController(
        $c->get(MarketInstrumentDbService::class)
    );
});

$container->set(BondController::class, function (ContainerInterface $c) {
    return new BondController(
        $c->get(MarketInstrumentDbService::class)
    );
});

$container->set(IntlController::class, function (ContainerInterface $c) {
    return new IntlController(
        $c->get(CountryService::class)
    );
});

$container->set(MarketController::class, function (ContainerInterface $c) {
    return new MarketController(
        $c->get(CandleDbService::class),
        $c->get(MarketInstrumentDbService::class),
        $c->get(MarketTinkoffService::class)
    );
});

$container->set(AdminItemController::class, function (ContainerInterface $c) {
    return new AdminItemController(
        $c->get(ItemService::class),
        $c->get(OperationService::class)
    );
});
