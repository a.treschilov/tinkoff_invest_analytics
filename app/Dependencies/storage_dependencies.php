<?php

declare(strict_types=1);

use App\Market\Storages\CandleStorage;
use App\Storages\InstrumentTypeStorage;
use App\Storages\MarketCountryStorage;
use App\Storages\MarketCurrencyStorage;
use App\Storages\MarketSectorStorage;
use App\Storages\MarketStockStorage;
use App\Storages\OperationTypeStorage;
use App\Storages\StrategyCountryStorage;
use App\Storages\StrategyCurrencyBalanceStorage;
use App\Storages\StrategyCurrencyStorage;
use App\Storages\StrategyInstrumentTypeStorage;
use App\Storages\StrategySectorStorage;
use App\Storages\WealthHistoryStorage;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(InstrumentTypeStorage::class, function (ContainerInterface $c) {
    return new InstrumentTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(OperationTypeStorage::class, function (ContainerInterface $c) {
    return new OperationTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketStockStorage::class, function (ContainerInterface $c) {
    return new MarketStockStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(StrategyInstrumentTypeStorage::class, function (ContainerInterface $c) {
    return new StrategyInstrumentTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(StrategyCurrencyStorage::class, function (ContainerInterface $c) {
    return new StrategyCurrencyStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CandleStorage::class, function (ContainerInterface $c) {
    return new CandleStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketCurrencyStorage::class, function (ContainerInterface $c) {
    return new MarketCurrencyStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(StrategyCurrencyBalanceStorage::class, function (ContainerInterface $c) {
    return new StrategyCurrencyBalanceStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketCountryStorage::class, function (ContainerInterface $c) {
    return new MarketCountryStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(StrategyCountryStorage::class, function (ContainerInterface $c) {
    return new StrategyCountryStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketSectorStorage::class, function (ContainerInterface $c) {
    return new MarketSectorStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(StrategySectorStorage::class, function (ContainerInterface $c) {
    return new StrategySectorStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(WealthHistoryStorage::class, function (ContainerInterface $c) {
    return new WealthHistoryStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});
