<?php

declare(strict_types=1);

namespace App\Loan\Models;

use App\Common\Models\BaseModel;
use App\Models\OperationModel;

class LoanModel extends BaseModel
{
    private int $loanId;
    private int $itemId;
    private string $name;
    private int $userId;
    private int $isActive;
    private ?\DateTime $closedDate = null;
    private float $amount;
    private string $currency;
    private \DateTime $date;
    private float $paymentAmount;
    private string $paymentCurrency;
    private int $payoutFrequency;
    private string $payoutFrequencyPeriod;
    private float $interestPercent;

    /**
     * @return int
     */
    public function getLoanId(): int
    {
        return $this->loanId;
    }

    /**
     * @param int $loanId
     */
    public function setLoanId(int $loanId): void
    {
        $this->loanId = $loanId;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float
     */
    public function getPaymentAmount(): float
    {
        return $this->paymentAmount;
    }

    /**
     * @param float $paymentAmount
     */
    public function setPaymentAmount(float $paymentAmount): void
    {
        $this->paymentAmount = $paymentAmount;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency(): string
    {
        return $this->paymentCurrency;
    }

    /**
     * @param string $paymentCurrency
     */
    public function setPaymentCurrency(string $paymentCurrency): void
    {
        $this->paymentCurrency = $paymentCurrency;
    }

    /**
     * @return float
     */
    public function getInterestPercent(): float
    {
        return $this->interestPercent;
    }

    /**
     * @param float $interestPercent
     */
    public function setInterestPercent(float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedDate(): ?\DateTime
    {
        return $this->closedDate;
    }

    /**
     * @param \DateTime|null $closedDate
     */
    public function setClosedDate(?\DateTime $closedDate): void
    {
        $this->closedDate = $closedDate;
    }
}
