<?php

namespace App\Loan\Models;

use App\Common\Models\BaseModel;
use App\Models\PriceModel;

class LoanOverpaymentModel extends BaseModel
{
    private PriceModel $price;
    private float $percent;

    /**
     * @return PriceModel
     */
    public function getPrice(): PriceModel
    {
        return $this->price;
    }

    /**
     * @param PriceModel $price
     */
    public function setPrice(PriceModel $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent(float $percent): void
    {
        $this->percent = $percent;
    }
}
