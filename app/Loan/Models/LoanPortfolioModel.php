<?php

declare(strict_types=1);

namespace App\Loan\Models;

use App\Common\Models\BaseModel;

class LoanPortfolioModel extends BaseModel
{
    private LoanModel $loan;
    private float $amount;
    private string $currency;
    private LoanOverpaymentModel $overpayment;

    /**
     * @return LoanModel
     */
    public function getLoan(): LoanModel
    {
        return $this->loan;
    }

    /**
     * @param LoanModel $loan
     */
    public function setLoan(LoanModel $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return LoanOverpaymentModel
     */
    public function getOverpayment(): LoanOverpaymentModel
    {
        return $this->overpayment;
    }

    /**
     * @param LoanOverpaymentModel $overpayment
     */
    public function setOverpayment(LoanOverpaymentModel $overpayment): void
    {
        $this->overpayment = $overpayment;
    }
}
