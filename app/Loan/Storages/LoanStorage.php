<?php

declare(strict_types=1);

namespace App\Loan\Storages;

use App\Loan\Entities\LoanEntity;
use Doctrine\ORM\EntityManagerInterface;

class LoanStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(LoanEntity $entity): int
    {
        $this->entityManager->persist($entity);
        if ($entity->getOperation() !== null) {
            $this->entityManager->persist($entity->getOperation());
        }
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param int $userId
     * @return LoanEntity[]
     */
    public function findByUser(int $userId): array
    {
        $result = [];
        $loans = $this->entityManager->getRepository(LoanEntity::class)
            ->findBy(['userId' => $userId]);

        foreach ($loans as $loan) {
            if ($loan->getItem()->getIsDeleted() === 0) {
                $result[] = $loan;
            }
        }

        return $result;
    }

    public function findById(int $loanId): ?LoanEntity
    {
        return $this->entityManager->getRepository(LoanEntity::class)
            ->findOneBy(['id' => $loanId]);
    }

    public function findByItem(int $itemId): ?LoanEntity
    {
        return $this->entityManager->getRepository(LoanEntity::class)
            ->findOneBy(['itemId' => $itemId]);
    }
}
