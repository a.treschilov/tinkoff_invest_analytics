<?php

declare(strict_types=1);

namespace App\Loan\Entities;

use App\Item\Entities\ItemOperationEntity;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Item\Entities\ItemEntity;

#[ORM\Table(name: 'tinkoff_invest.loan')]
#[ORM\Entity]
class LoanEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'loan_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemEntity::class, inversedBy: 'loan', cascade: ['persist'])]
    protected ItemEntity $item;

    #[ORM\Column(name: 'user_id', type: 'integer')]
    protected int $userId;

    #[ORM\Column(name: 'closed_date', type: 'datetime')]
    protected ?DateTime $closedDate = null;

    #[ORM\Column(name: 'initial_operation_id', type: 'integer')]
    protected ?int $operationId = null;

    #[ORM\JoinColumn(name: 'initial_operation_id', referencedColumnName: 'item_operation_id')]
    #[ORM\OneToOne(targetEntity: \App\Item\Entities\ItemOperationEntity::class)]
    protected ?ItemOperationEntity $operation = null;

    #[ORM\Column(name: 'payment_amount', type: 'float')]
    private float $paymentAmount;

    #[ORM\Column(name: 'payment_currency', type: 'string')]
    private string $paymentCurrency;

    #[ORM\Column(name: 'payout_frequency', type: 'integer')]
    private int $payoutFrequency;

    #[ORM\Column(name: 'payout_frequency_period', type: 'string')]
    private string $payoutFrequencyPeriod;

    #[ORM\Column(name: 'interest_percent', type: 'float')]
    private float $interestPercent;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    public function getOperationId(): ?int
    {
        return $this->operationId;
    }

    /**
     * @param int|null $operationId
     */
    public function setOperationId(?int $operationId): void
    {
        $this->operationId = $operationId;
    }

    /**
     * @return ItemOperationEntity|null
     */
    public function getOperation(): ?ItemOperationEntity
    {
        return $this->operation;
    }

    /**
     * @param ItemOperationEntity|null $operation
     */
    public function setOperation(?ItemOperationEntity $operation): void
    {
        $this->operation = $operation;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float
     */
    public function getPaymentAmount(): float
    {
        return $this->paymentAmount;
    }

    /**
     * @param float $paymentAmount
     */
    public function setPaymentAmount(float $paymentAmount): void
    {
        $this->paymentAmount = $paymentAmount;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency(): string
    {
        return $this->paymentCurrency;
    }

    /**
     * @param string $paymentCurrency
     */
    public function setPaymentCurrency(string $paymentCurrency): void
    {
        $this->paymentCurrency = $paymentCurrency;
    }

    /**
     * @return float
     */
    public function getInterestPercent(): float
    {
        return $this->interestPercent;
    }

    /**
     * @param float $interestPercent
     */
    public function setInterestPercent(float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return DateTime|null
     */
    public function getClosedDate(): ?DateTime
    {
        return $this->closedDate;
    }

    /**
     * @param DateTime|null $closedDate
     */
    public function setClosedDate(?DateTime $closedDate): void
    {
        $this->closedDate = $closedDate;
    }
}
