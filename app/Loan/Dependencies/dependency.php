<?php

declare(strict_types=1);

use App\Intl\Services\CurrencyService;
use App\Item\Services\OperationService;
use App\Loan\Services\LoanService;
use App\Loan\Storages\LoanStorage;
use App\Services\ExchangeCurrencyService;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(LoanStorage::class, function (ContainerInterface $c) {
    return new LoanStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(LoanService::class, function (ContainerInterface $c) {
    return new LoanService(
        $c->get('Now' . DateTime::class),
        $c->get(LoanStorage::class),
        $c->get(OperationService::class),
        $c->get(CurrencyService::class),
        $c->get(ExchangeCurrencyService::class)
    );
});
