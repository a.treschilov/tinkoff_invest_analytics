<?php

declare(strict_types=1);

namespace App\Loan\Collections;

use App\Loan\Models\LoanModel;
use Doctrine\Common\Collections\ArrayCollection;

class LoanCollection extends ArrayCollection
{
    public function getItemIds(): array
    {
        $ids = [];
        /** @var LoanModel $loan */
        foreach ($this->getIterator() as $loan) {
            $ids[] = $loan->getItemId();
        }

        return $ids;
    }
}
