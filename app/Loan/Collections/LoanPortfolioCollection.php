<?php

declare(strict_types=1);

namespace App\Loan\Collections;

use Doctrine\Common\Collections\ArrayCollection;

class LoanPortfolioCollection extends ArrayCollection
{
}
