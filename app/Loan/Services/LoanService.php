<?php

declare(strict_types=1);

namespace App\Loan\Services;

use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\OperationCollection;
use App\Item\Entities\ItemEntity;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Item\Types\OperationType;
use App\Loan\Collections\LoanCollection;
use App\Loan\Collections\LoanPortfolioCollection;
use App\Loan\Entities\LoanEntity;
use App\Loan\Models\LoanModel;
use App\Loan\Models\LoanOverpaymentModel;
use App\Loan\Models\LoanPortfolioModel;
use App\Loan\Storages\LoanStorage;
use App\Models\PriceModel;
use App\Services\ExchangeCurrencyService;
use App\User\Exceptions\UserAccessDeniedException;

class LoanService
{
    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly LoanStorage $loanStorage,
        private readonly OperationService $operationService,
        private readonly CurrencyService $currencyService,
        private readonly ExchangeCurrencyService $exchangeCurrencyService
    ) {
    }

    public function getUserLoans(int $userId): LoanCollection
    {
        $loanCollection = new LoanCollection();

        $loanList = $this->loanStorage->findByUser($userId);
        foreach ($loanList as $loan) {
            $loanCollection->add($this->convertLoanEntityToModel($loan));
        }

        return $loanCollection;
    }

    public function getUserLoan(int $userId, int $loanId): LoanModel
    {
        $loan = $this->loanStorage->findById($loanId);

        if ($loan === null || $loan->getItem()->getIsDeleted() === 1 || $loan->getUserId() !== $userId) {
            throw new UserAccessDeniedException('Access denied for user', 2026);
        }

        return $this->convertLoanEntityToModel($loan);
    }

    public function updateUserLoan(int $userId, LoanModel $loan): void
    {
        $loanEntity = $this->loanStorage->findById($loan->getLoanId());

        if ($loanEntity->getUserId() !== $userId || $loanEntity->getItem()->getIsDeleted() === 1) {
            throw new UserAccessDeniedException('Access denied for user', 2027);
        }

        $loanEntity->getItem()->setIsOutdated((int)(!$loan->getIsActive()));
        $loanEntity->getItem()->setName($loan->getName());

        $loanEntity->setClosedDate($loanEntity->getClosedDate());
        $loanEntity->setPaymentAmount($loan->getPaymentAmount());
        $loanEntity->setPaymentCurrency($loan->getPaymentCurrency());
        $loanEntity->setPayoutFrequency($loan->getPayoutFrequency());
        $loanEntity->setPayoutFrequencyPeriod($loan->getPayoutFrequencyPeriod());
        $loanEntity->setInterestPercent($loan->getInterestPercent());

        if ($loanEntity->getOperation()->getAmount() !== $loan->getAmount()) {
            $loanEntity->getOperation()->setAmount($loan->getAmount());
            $loanEntity->getOperation()->setIsAnalyticProceed(0);
        }
        if ($loanEntity->getOperation()->getCurrency()->getIso() !== $loan->getCurrency()) {
            $currency = $this->currencyService->getCurrencyByIso($loan->getCurrency());
            $loanEntity->getOperation()->setCurrency($currency);
            $loanEntity->getOperation()->setCurrencyId($currency->getId());
            $loanEntity->getOperation()->setIsAnalyticProceed(0);
        }

        $date = $loan->getDate()->setTimezone(new \DateTimeZone('UTC'));
        if ($loanEntity->getOperation()->getDate()->getTimestamp() - $date->getTimestamp()) {
            $loanEntity->getOperation()->setDate($date);
            $loanEntity->getOperation()->setIsAnalyticProceed(0);
        }

        $this->loanStorage->addEntity($loanEntity);

        $this->operationService->publishOperationEvent($loanEntity->getOperation()->getId());
    }

    public function closeUserLoan($userId, $loanId): void
    {
        $loan = $this->loanStorage->findById($loanId);
        if ($loan === null || $loan->getUserId() !== $userId || $loan->getItem()->getIsDeleted() === 1) {
            throw new UserAccessDeniedException('Access denied for user', 2028);
        }

        $loan->setClosedDate($this->currentDateTime);
        $loan->getItem()->setIsOutdated(1);

        $loanInfo = $this->getUserPortfolioItem($userId, $loanId);

        if ($loanInfo->getAmount() !== 0.0) {
            $operation = new OperationModel([
                'userId' => $userId,
                'externalId' => $this->operationService->generateUserOperationExternalId(),
                'itemId' => $loanInfo->getLoan()->getItemId(),
                'operationType' => BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                'date' => $this->currentDateTime,
                'amount' => $loanInfo->getAmount(),
                'currencyIso' => $loanInfo->getCurrency(),
                'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
                'quantity' => 1
            ]);

            $this->operationService->addUserOperation($userId, $operation);
        }

        $this->loanStorage->addEntity($loan);
    }

    public function addUserLoan(int $userId, LoanModel $loan): int
    {
        $operation = new OperationModel([
            'userId' => $userId,
            'externalId' => $this->operationService->generateUserOperationExternalId(),
            'operationType' => BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
            'date' => $loan->getDate()->setTimezone(new \DateTimeZone('UTC')),
            'amount' => $loan->getAmount(),
            'currencyIso' => $loan->getCurrency(),
            'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
            'isAnalyticProceed' => 0,
            'quantity' => 1
        ]);
        $operationEntity = $this->operationService->convertOperationModelToEntity($operation);

        $itemEntity = new ItemEntity();
        $itemEntity->setName($loan->getName());
        $itemEntity->setType('loan');
        $itemEntity->setSource('user');
        $itemEntity->setIsOutdated(($loan->getIsActive() + 1) % 2);

        $entity = new LoanEntity();
        $entity->setUserId($userId);
        $entity->setClosedDate($loan->getClosedDate());
        $entity->setPaymentAmount($loan->getPaymentAmount());
        $entity->setPaymentCurrency($loan->getPaymentCurrency());
        $entity->setPayoutFrequency($loan->getPayoutFrequency());
        $entity->setPayoutFrequencyPeriod($loan->getPayoutFrequencyPeriod());
        $entity->setInterestPercent($loan->getInterestPercent());

        $entity->setOperation($operationEntity);
        $entity->setItem($itemEntity);

        $itemEntity->setLoan($entity);
        $operationEntity->setItem($itemEntity);

        $loanId = $this->loanStorage->addEntity($entity);

        $this->operationService->publishOperationEvent($entity->getOperation()->getId());

        return $loanId;
    }

    public function getUserLoanPortfolio(int $userId): LoanPortfolioCollection
    {
        $loanPortfolio = new LoanPortfolioCollection();

        $loanCollection = $this->getUserLoans($userId);

        /** @var LoanModel $loan */
        foreach ($loanCollection as $loan) {
            $loanPortfolio->add($this->getLoanPortfolioItemByLoan($loan));
        }

        return $loanPortfolio;
    }

    public function getUserLoanPortfolioItem(int $userId, int $loanId): LoanPortfolioModel
    {
        $loan = $this->getUserLoan($userId, $loanId);

        return $this->getLoanPortfolioItemByLoan($loan);
    }

    public function getUserLoanPrice(int $userId, int $itemId, \DateTime $date): PriceModel
    {
        $operations = $this->getLoanOperations($userId, $itemId, $date);
        $loan = $this->loanStorage->findByItem($itemId);
        if ($loan === null) {
            throw new UserAccessDeniedException('Loan is not exists');
        }
        return $this->calculateLoanPriceByOperations($operations, $loan->getOperation()->getCurrency()->getIso());
    }

    public function calculatePaymentSchedule(int $userId, ItemModel $item, \DateTime $dateTo): OperationCollection
    {
        $futureOperations = new OperationCollection();
        $loanPortfolio = $this->getUserLoanPortfolioItem($userId, $item->getLoan()->getLoanId());

        $loan = $item->getLoan();
        if (
            $item->isOutdated()
            || $loan->getInterestPercent() <= 0
            || $loanPortfolio->getAmount() >= -0.001
        ) {
            return $futureOperations;
        }

        $loanOperations = $this
            ->getLoanOperations($userId, $loan->getItemId(), $this->currentDateTime);
        $payouts = $loanOperations
            ->filterByType([OperationType::DIVIDEND])
            ->sortByDateAsc();
        /** @var OperationModel $lastPayout */
        $lastPayout = $payouts->last();

        $startDate = $lastPayout !== false
            ? clone $lastPayout->getDate()
            : clone $loan->getDate();

        $amount = $this->getUserLoanPrice($userId, $loan->getItemId(), $startDate);

        $currentPayoutStart = clone $startDate;
        $currentPayoutEnd = clone $startDate;
        $currentPayoutEnd->add($this->buildDateInterval(
            $loan->getPayoutFrequency(),
            $loan->getPayoutFrequencyPeriod()
        ));

        while (
            $amount->getAmount() < 0
            && $currentPayoutEnd->getTimestamp() < $dateTo->getTimestamp()
        ) {
            $diff = $currentPayoutEnd->diff($currentPayoutStart);
            $percent = $diff->days / (date('L') ? 366 : 365) * $loan->getInterestPercent();
            $percentAmount = $percent * $amount->getAmount();

            $currentPayoutStart->add(new \DateInterval("PT1S"));
            $operations = $loanOperations->filterByDateInterval($currentPayoutStart, $currentPayoutEnd);

            /** @var OperationModel $operation */
            foreach ($operations->getIterator() as $operation) {
                $diff = $currentPayoutEnd->diff($operation->getDate());
                $percent = $diff->days / (date('L') ? 366 : 365) * $loan->getInterestPercent();
                $percentAmount -= $percent * $operation->getAmount();

                $amount->setAmount(
                    $amount->getAmount() -
                    $operation->getAmount() * $this->exchangeCurrencyService->getExchangeRate(
                        $operation->getCurrencyIso(),
                        $amount->getCurrency(),
                        $operation->getDate()
                    )
                );
            }

            $operation = new OperationModel([
                'userId' => $userId,
                'externalId' => $this->operationService->generateUserOperationExternalId(),
                'brokerId' => null,
                'itemId' => $loan->getItemId(),
                'item' => $item,
                'operationType' => BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND,
                'createdDate' => $this->currentDateTime,
                'quantity' => null,
                'date' => clone $currentPayoutEnd,
                'amount' => $percentAmount * $this->exchangeCurrencyService->getExchangeRate(
                    $amount->getCurrency(),
                    $loan->getPaymentCurrency(),
                    $this->currentDateTime > $currentPayoutEnd
                        ? clone $currentPayoutEnd
                        : $this->currentDateTime
                ),
                'currencyIso' => $loan->getPaymentCurrency(),
                'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
                'isAnalyticProceed' => 0
            ]);
            $futureOperations->add($operation);

            $operation = new OperationModel([
                'userId' => $userId,
                'externalId' => $this->operationService->generateUserOperationExternalId(),
                'brokerId' => null,
                'itemId' => $loan->getItemId(),
                'item' => $item,
                'operationType' => BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD,
                'createdDate' => $this->currentDateTime,
                'quantity' => null,
                'date' => clone $currentPayoutEnd,
                'amount' => max(-1 * $loan->getPaymentAmount(), $amount->getAmount()),
                'currencyIso' => $loan->getPaymentCurrency(),
                'status' => BrokerOperationStatusType::OPERATION_STATE_DONE,
                'isAnalyticProceed' => 0
            ]);
            $futureOperations->add($operation);

            $currentPayoutStart = clone $currentPayoutEnd;
            $currentPayoutEnd->add($this->buildDateInterval(
                $loan->getPayoutFrequency(),
                $loan->getPayoutFrequencyPeriod()
            ));

            $amount->setAmount($amount->getAmount() - $operation->getAmount());
        }

        return $futureOperations;
    }

    private function getLoanPortfolioItemByLoan(LoanModel $loan): LoanPortfolioModel
    {
        $operations = $this->getLoanOperations($loan->getUserId(), $loan->getItemId(), $this->currentDateTime);
        $price = $this->calculateLoanPriceByOperations($operations, $loan->getCurrency());
        $overPayment = $this->calculateOverPaymentByOperations($operations, $loan->getCurrency());
        return new LoanPortfolioModel([
            'loan' => $loan,
            'amount' => $price->getAmount(),
            'currency' => $price->getCurrency(),
            'overPayment' => new LoanOverpaymentModel([
                'price' => $overPayment,
                'percent' => $loan->getAmount() !== 0.0 ? $overPayment->getAmount() / $loan->getAmount() : 0
            ])
        ]);
    }

    private function calculateLoanPriceByOperations(OperationCollection $operations, string $currency): PriceModel
    {
        $amount = 0;
        $operations = $operations->filterByType([
            OperationType::BUY_CARD,
            OperationType::SELL_CARD,
            OperationType::DIVIDEND
        ]);
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            $operationAmount = $operation->getAmount() * $this->exchangeCurrencyService->getExchangeRate(
                $operation->getCurrencyIso(),
                $currency,
                $operation->getDate()
            );
            $operationAmount = in_array(
                $operation->getOperationType(),
                [
                    BrokerOperationTypeType::OPERATION_TYPE_BUY_CARD,
                    BrokerOperationTypeType::OPERATION_TYPE_SELL_CARD
                ]
            )
                ? -1 * $operationAmount
                : $operationAmount;
            $amount += $operationAmount;
        }

        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    private function calculateOverPaymentByOperations(
        OperationCollection $operationCollection,
        string $currency
    ): PriceModel {
        $overPayment = 0;
        /** @var OperationModel $operation */
        foreach ($operationCollection->getIterator() as $operation) {
            if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_DIVIDEND) {
                $overPayment += -1 * $operation->getAmount()
                * $this->exchangeCurrencyService->getExchangeRate(
                    $operation->getCurrencyIso(),
                    $currency,
                    $operation->getDate()
                );
            }
        }

        return new PriceModel([
            "amount" => $overPayment,
            "currency" => $currency
        ]);
    }

    private function getLoanOperations($userId, $itemId, \DateTime $date): OperationCollection
    {
        $filter = new OperationFiltersModel([
            'itemId' => [$itemId],
            'operationStatus' => [BrokerOperationStatusType::OPERATION_STATE_DONE],
            'dateTo' => $date
        ]);
        return $this->operationService->getFilteredUserOperation($userId, $filter);
    }

    private function getUserPortfolioItem($userId, $loanId): LoanPortfolioModel|null
    {
        $loan = $this->getUserLoan($userId, $loanId);

        $price = $this->getUserLoanPrice($userId, $loan->getItemId(), $this->currentDateTime);

        return new LoanPortfolioModel([
            'loan' => $loan,
            'amount' => $price->getAmount(),
            'currency' => $price->getCurrency()
        ]);
    }

    private function convertLoanEntityToModel(LoanEntity $loan): LoanModel
    {
        return new LoanModel([
            'loanId' => $loan->getId(),
            'itemId' => $loan->getItemId(),
            'userId' => $loan->getUserId(),
            'isActive' => ($loan->getItem()->getIsOutdated() + 1) % 2,
            'closedDate' => $loan->getClosedDate(),
            'name' => $loan->getItem()->getName(),
            'amount' => $loan->getOperation()->getAmount(),
            'currency' => $loan->getOperation()->getCurrency()->getIso(),
            'date' => $loan->getOperation()->getDate(),
            'paymentAmount' => $loan->getPaymentAmount(),
            'paymentCurrency' => $loan->getPaymentCurrency(),
            'payoutFrequency' => $loan->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $loan->getPayoutFrequencyPeriod(),
            'interestPercent' => $loan->getInterestPercent(),
        ]);
    }

    private function buildDateInterval(int $duration, string $durationPeriod): \DateInterval
    {
        $intervalPeriod = match ($durationPeriod) {
            'day' => 'D',
            'month' => 'M',
            'year' => 'Y'
        };
        return new \DateInterval('P' . $duration . $intervalPeriod);
    }
}
