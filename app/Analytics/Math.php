<?php

declare(strict_types=1);

namespace App\Analytics;

class Math
{
    /**
     * @param $cashFlow array{date: \DateTime, amount: float}
     * @return float|null
     */
    public function xirr(array $cashFlow): float|null
    {
        $epsilon = 0.00001; // This is a very small number that determines the accuracy of the solution.
        /**
         * The maximum number of iterations that the algorithm should perform.
         * The Newton-Raphson method, used here, is an iterative algorithm that gradually approximates the solution.
         */
        $maxIter = 10000;
        /**
         * The initial guess for the interest rate. It's the starting point for the calculation.
         * If you don't have a specific starting point in mind,
         * it's common to start the guess at 10% (or 0.1 as a decimal).
         */
        $guess = 0.1;

        /** @var \DateTime $dateStart */
        $dateStart = reset($cashFlow)['date'];
        /** @var \DateTime $dateEnd */
        $dateEnd = end($cashFlow)['date'];
        $days = $dateStart->diff($dateEnd)->days;

        if ($days === 0) {
            return null;
        }

        for ($i = 0; $i < $maxIter; $i++) {
            $total = 0.0;
            $totalDeriv = 0.0;
            /** @var array{date: \DateTime, amount: float} $operation */
            foreach ($cashFlow as $operation) {
                $interval = $operation['date']->diff(reset($cashFlow)['date']);
                $fracOfyear = $interval->days / $days;
                $total += $operation['amount'] / pow((1.0 + $guess), $fracOfyear);
                $totalDeriv += -$interval->days * $operation['amount'] / pow((1.0 + $guess), $fracOfyear + 1.0);
            }
            $nextGuess = $guess - $total / $totalDeriv;
            if (abs($nextGuess - $guess) < $epsilon) {
                return $nextGuess;
            }
            $guess = $nextGuess;
        }
        return null;
    }
}
