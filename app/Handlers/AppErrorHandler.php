<?php

declare(strict_types=1);

namespace App\Handlers;

use App\Common\ApplicationMode;
use App\Common\HttpResponseCode;
use App\Exceptions\AccountException;
use App\Exceptions\DoctrineException;
use App\Exceptions\InvalidRequestDataException;
use App\Exceptions\ManyRequestException;
use App\User\Exceptions\UserAccessDeniedException;
use Slim\Handlers\ErrorHandler;
use Slim\Exception\HttpException;
use App\User\Exceptions\AuthTypeException;
use Psr\Http\Message\ResponseInterface;

class AppErrorHandler extends ErrorHandler
{
    protected function respond(): ResponseInterface
    {
        $exception = $this->exception;
        $statusCode = 500;

        if ($exception instanceof HttpException) {
            $statusCode = $exception->getCode();
        }

        $statusCode = match ($exception::class) {
            AuthTypeException::class => HttpResponseCode::UNAUTHORIZED,
            AccountException::class => HttpResponseCode::UNPROCCESSABLE_ENTITY,
            UserAccessDeniedException::class => HttpResponseCode::UNPROCCESSABLE_ENTITY,
            DoctrineException::class => HttpResponseCode::UNPROCCESSABLE_ENTITY,
            ManyRequestException::class => HttpResponseCode::MANY_REQUESTS,
            InvalidRequestDataException::class => HttpResponseCode::UNPROCCESSABLE_ENTITY,
            default => $statusCode
        };

        $error = [
            'error_code' => $exception->getCode(),
            'error_message' => $exception->getMessage(),
        ];
        $extendErrorInfo = [];
        if (ApplicationMode::isDevMode()) {
            $extendErrorInfo = [
                'error_file' => $exception->getFile(),
                'error_line' => $exception->getLine(),
                'error_trace' => $exception->getTrace()
            ];
        }
        $error = array_merge($error, $extendErrorInfo);

        $payload = json_encode($error, JSON_PRETTY_PRINT);

        $response = $this->responseFactory->createResponse($statusCode);
        $response->getBody()->write($payload);

        if (!$response->hasHeader('Access-Control-Allow-Origin')) {
            $response = $response->withHeader('Access-Control-Allow-Origin', '*');
        }
        return $response->withHeader('Content-Type', 'application/json');
    }
}
