<?php

declare(strict_types=1);

/** @var App $app */

use App\Controllers\Service\AnalyticsController;
use App\Controllers\Service\MarketController;
use App\Controllers\Service\OperationController;
use App\Controllers\Service\UserController;
use App\Middlewares\ServiceMiddleware;
use Slim\App;

$app->group('/api/v1/service', function ($app) {
    $app->get('/user/list', UserController::class . ':getUserList');
    $app->get('/user/telegram', UserController::class . ':getUserByTelegramId');
    $app->get('/user', UserController::class . ':getUser');
    $app->get('/operation/calendar', OperationController::class . ':getEventCalendar');
    $app->get('/analytics/monthlyReport', AnalyticsController::class . ':getMonthlyReport');
    $app->get('/analytics/capitalChange', AnalyticsController::class . ':getCapitalChange');
    $app->get('/analytics/dashboard', AnalyticsController::class . ':dashboard');
    $app->get('/market/candles', MarketController::class . ':getCandles');
    $app->get('/market/instrument/share', MarketController::class . ':getShare');
})
    ->add(ServiceMiddleware::class);
