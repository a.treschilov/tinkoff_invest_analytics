<?php

declare(strict_types=1);

/** @var App $app */

use App\Controllers\BondController;
use App\Controllers\FutureController;
use App\Controllers\IntlController;
use App\Controllers\ItemController;
use App\Controllers\StockController;
use Slim\App;

$app->group('/api/v1/market', function ($app) {
    $app->get('/bond/list', BondController::class . ':list');
    $app->get('/future/list', FutureController::class . ':list');
    $app->get('/stock/list', StockController::class . ':list');
    $app->get('/stock/filters', StockController::class . ':filters');
    $app->get('/item/price/{isin}', ItemController::class . ':priceHistory');
    $app->get('/item/{isin}', ItemController::class . ':getItem');
});

$app->get('/api/v1/dictionary/country', IntlController::class . ':getCountryList');
