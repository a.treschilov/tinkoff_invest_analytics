<?php

declare(strict_types=1);

/** @var App $app */

use App\Controllers\Admin\AssetAdminController;
use App\Controllers\Admin\ItemController;
use App\Controllers\Admin\MarketInstrumentController;
use App\Controllers\StockController;
use App\Middlewares\AdminMiddleware;
use App\Middlewares\JwtMiddleware;
use Slim\App;

$app->group('/api/v1/admin', function ($app) {
    $app->put('/crowdfunding/price', AssetAdminController::class . ':updatePrice');
    $app->post('/market/stock/import', StockController::class . ':import');
    $app->put('/market/instrument', MarketInstrumentController::class . ':updateInstruments');
    $app->delete('/item/source', ItemController::class . ':removeSourceItems');
})
    ->add(AdminMiddleware::class)
    ->add(JwtMiddleware::class);
