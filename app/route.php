<?php

declare(strict_types=1);

/** @var App $app */

use App\Controllers\AnalyticsController;
use App\Controllers\AssetController;
use App\Controllers\AuthController;
use App\Controllers\BrokerController;
use App\Controllers\DepositController;
use App\Controllers\HealthCheckController;
use App\Controllers\ItemController;
use App\Controllers\LoanController;
use App\Controllers\NewsController;
use App\Controllers\OperationController;
use App\Controllers\PortfolioController;
use App\Controllers\RealEstateController;
use App\Controllers\UserController;
use App\Controllers\UserCredentialController;
use App\Controllers\UserOnboardingController;
use App\Controllers\UserStrategyShareController;
use App\Middlewares\HeadersMiddleware;
use App\Middlewares\JsonBodyParserMiddleware;
use App\Middlewares\JwtMiddleware;
use Slim\App;

$app->add(JsonBodyParserMiddleware::createFromContainer($app));
$app->add(HeadersMiddleware::createFromContainer($app));

$app->options('/{routes:.+}', AuthController::class . ':optionRequests');

$app->group('/api/v1/auth', function ($app) {
    $app->post('/authorization', AuthController::class . ':authorization');
    $app->post('/link', AuthController::class . ':linkAccount')->add(JwtMiddleware::class);
    $app->delete('/link', AuthController::class . ':unlinkAccount')->add(JwtMiddleware::class);
});

$app->get('/api/v1/portfolio', PortfolioController::class . ':apiPortfolio')
    ->add(JwtMiddleware::class);

$app->group('/api/v1/operations', function ($app) {
    $app->post('', OperationController::class . ':addOperation')
        ->add(JwtMiddleware::class);
    $app->group('/operation/{id}', function ($app) {
        $app->get('', OperationController::class . ':getOperation')
            ->add(JwtMiddleware::class);
        $app->delete('', OperationController::class . ':deleteUserOperation')
            ->add(JwtMiddleware::class);
        $app->put('', OperationController::class . ':editOperation')
            ->add(JwtMiddleware::class);
    });
    $app->get('/filters', OperationController::class . ':getOperationFilters')
        ->add(JwtMiddleware::class);
    $app->get('/list', OperationController::class . ':apiOperations')
        ->add(JwtMiddleware::class);
    $app->get('/item/{id}', OperationController::class . ':getItemOperations')
        ->add(JwtMiddleware::class);
    $app->post('/addBatch', OperationController::class . ':addOperationBatch')
        ->add(JwtMiddleware::class);
    $app->get('/payin', OperationController::class . ':getPayIn')
        ->add(JwtMiddleware::class);
    $app->get('/earning', OperationController::class . ':getEarning')
        ->add(JwtMiddleware::class);
    $app->get('/status/list', OperationController::class . ':getStatusList')
        ->add(JwtMiddleware::class);

    $app->group('/import', function ($app) {
        $app->post('/files', OperationController::class . ':importFromFile')
            ->add(JwtMiddleware::class);
        $app->post('', OperationController::class . ':importUserOperations')
            ->add(JwtMiddleware::class);
    });
});

$app->group('/api/v1/user', function ($app) {
    $app->group('/data', function ($app) {
        $app->get('', UserController::class . ':data')
            ->add(JwtMiddleware::class);
        $app->put('', UserController::class . ':updateUserData')
            ->add(JwtMiddleware::class);
    });

    $app->group('/credential', function ($app) {
        $app->get('', UserCredentialController::class . ':getUserCredentialList')
            ->add(JwtMiddleware::class);
        $app->post('', UserCredentialController::class . ':addUserCredential')
            ->add(JwtMiddleware::class);
        $app->get('/{id}', UserCredentialController::class . ':getUserCredential')
            ->add(JwtMiddleware::class);
        $app->put('/{id}', UserCredentialController::class . ':updateUserCredential')
            ->add(JwtMiddleware::class);
        $app->get('/{id}/account', UserCredentialController::class . ':getBrokerAccounts')
            ->add(JwtMiddleware::class);
        $app->post(
            '/{id}/account',
            UserCredentialController::class . ':setBrokerAccounts'
        )->add(JwtMiddleware::class);
    });

    $app->group('/settings', function ($app) {
        $app->get('', UserController::class . ':getSettings')
            ->add(JwtMiddleware::class);
        $app->post('', UserController::class . ':setSettings')
            ->add(JwtMiddleware::class);
        $app->put('', UserController::class . ':updateSettings')
            ->add(JwtMiddleware::class);
    });

    $app->group('/onboarding', function ($app) {
        $app->put('/complete', UserOnboardingController::class . ':onboardingComplete')
            ->add(JwtMiddleware::class);
    });

    $app->group('/strategy', function ($app) {
        $app->get('/{type}', UserStrategyShareController::class . ':getTargetShare')
            ->add(JwtMiddleware::class);
        $app->post('/{type}', UserStrategyShareController::class . ':postTargetShare')
            ->add(JwtMiddleware::class);
    });

    $app->group('/code', function ($app) {
        $app->post('/release', UserController::class . ':releaseCode')->add(JwtMiddleware::class);
        $app->put('/confirm', UserController::class . ':confirmCode');
    });

    $app->group('/account', function ($app) {
        $app->get('', UserController::class . ':getAccounts');
    })->add(JwtMiddleware::class);
});

$app->group('/api/v1/analytics', function ($app) {
    $app->get('/summary/today', AnalyticsController::class . ':summaryToday')
        ->add(JwtMiddleware::class);
    $app->get('/dashboard', AnalyticsController::class . ':dashboard')
        ->add(JwtMiddleware::class);
    $app->get('/wealthDynamic', AnalyticsController::class . ':wealthDynamic')
        ->add(JwtMiddleware::class);
    $app->get('/wealthTypes', AnalyticsController::class . ':wealthTypes')
        ->add(JwtMiddleware::class);
    $app->get('/compareWithIndex', AnalyticsController::class . ':compareWithIndex')
        ->add(JwtMiddleware::class);
});

$app->group('/api/v1/asset', function ($app) {
    $app->get('/list/{type}', AssetController::class . ':getAssetTypedList')
        ->add(JwtMiddleware::class);
});


$app->group('/api/v1/item', function ($app) {
    $app->get('/info/{id}', ItemController::class . ':getItemInfo')
        ->add(JwtMiddleware::class);
    $app->delete('/info/{id}', ItemController::class . ':deleteUserItem')
        ->add(JwtMiddleware::class);

    $app->get('/portfolio', ItemController::class . ':getPortfolio')
        ->add(JwtMiddleware::class);
    $app->get('/portfolio/eventsCalendar', ItemController::class . ':getEventCalendar')
        ->add(JwtMiddleware::class);

    $app->post('/operations/import', ItemController::class . ':importOperations')
        ->add(JwtMiddleware::class);

    $app->group('/details/{id}', function ($app) {
        $app->get('/operations', ItemController::class . ':getOperations')
            ->add(JwtMiddleware::class);
        $app->get('/summary', ItemController::class . ':getUserItemSummary')
            ->add(JwtMiddleware::class);
        $app->get('/events', ItemController::class . ':getItemEvents')
            ->add(JwtMiddleware::class);
    });
});


$app->group('/api/v1/real_estate', function ($app) {
    $app->get('/list', RealEstateController::class . ':getUserRealEstateList')
        ->add(JwtMiddleware::class);
    $app->get('/item/{id}', RealEstateController::class . ':getUserRealEstateItem')
        ->add(JwtMiddleware::class);
    $app->put('/item/{id}', RealEstateController::class . ':updateUserRealEstateItem')
        ->add(JwtMiddleware::class);
    $app->post('/item', RealEstateController::class . ':addUserRealEstateItem')
        ->add(JwtMiddleware::class);
    $app->put('/sell/{id}', RealEstateController::class . ':sellUserRealEstateItem')
        ->add(JwtMiddleware::class);
});

$app->group('/api/v1/loan', function ($app) {
    $app->get('/item/{id}', LoanController::class . ':getUserLoanItem')
        ->add(JwtMiddleware::class);
    $app->put('/item/{id}', LoanController::class . ':updateUserLoanItem')
        ->add(JwtMiddleware::class);
    $app->post('/item', LoanController::class . ':addUserLoanItem')
        ->add(JwtMiddleware::class);
    $app->get('/list', LoanController::class . ':getUserLoanList')
        ->add(JwtMiddleware::class);
    $app->get('/portfolio', LoanController::class . ':getUserLoanPortfolio')
        ->add(JwtMiddleware::class);
    $app->get('/paymentsSuggestion', LoanController::class . ':getLoanPaymentsSuggestion')
        ->add(JwtMiddleware::class);
    $app->put('/close/{id}', LoanController::class . ':closeUserLoanItem')
        ->add(JwtMiddleware::class);
});

$app->group('/api/v1/health_check', function ($app) {
    $app->get('/up', HealthCheckController::class . ':up');
    $app->get('/details', HealthCheckController::class . ':details');
});

$app->group('/api/v1/deposit', function ($app) {
    $app->get('/list', DepositController::class . ':getUserDepositList')
        ->add(JwtMiddleware::class);
    $app->get('/portfolio', DepositController::class . ':getUserDepositPortfolio')
        ->add(JwtMiddleware::class);
    $app->get('/portfolio/{id}', DepositController::class . ':getUserDepositItemInfo')
        ->add(JwtMiddleware::class);
    $app->get('/payouts', DepositController::class . ':getUserDepositPayouts')
        ->add(JwtMiddleware::class);
    $app->post('/item', DepositController::class . ':addUserDepositItem')
        ->add(JwtMiddleware::class);
    $app->put('/item', DepositController::class . ':updateUserDepositItem')
        ->add(JwtMiddleware::class);
    $app->get('/item/{id}', DepositController::class . ':getUserDepositItem')
        ->add(JwtMiddleware::class);
    $app->put('/close/{id}', DepositController::class . ':closeUserDepositItem')
        ->add(JwtMiddleware::class);
});

$app->group('/api/v1/news', function ($app) {
    $app->get('/list', NewsController::class . ':getNewsList');
    $app->get('/list/period', NewsController::class . ':getDateRangeNews');
});

$app->group('/api/v1/broker', function ($app) {
    $app->post('/manualImport/{broker}', BrokerController::class . ':manualOperationImport')
        ->add(JwtMiddleware::class);
    $app->get('/list', BrokerController::class . ':getList');
    $app->get('/brokerUploadReports', BrokerController::class . ':uploadReportList')
        ->add(JwtMiddleware::class);
});

require_once dirname(__FILE__) . '/Routes/adminRoute.php';
require_once dirname(__FILE__) . '/Routes/serviceRoute.php';
require_once dirname(__FILE__) . '/Routes/freeRoute.php';
