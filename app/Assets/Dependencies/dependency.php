<?php

declare(strict_types=1);

use App\Assets\Services\AssetFactory;
use App\Assets\Services\AssetPriceService;
use App\Assets\Services\AssetService;
use App\Assets\Storages\AssetsPriceStorage;
use App\Assets\Storages\PayoutTypeStorage;
use App\Common\Amqp\AmqpClient;
use App\Intl\Services\CurrencyService;
use App\Item\Services\OperationService;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(AssetsPriceStorage::class, function (ContainerInterface $c) {
    return new AssetsPriceStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(AssetService::class, function (ContainerInterface $c) {
    return new AssetService(
        $c->get(AssetFactory::class),
        $c->get(CurrencyService::class),
        $c->get(AssetPriceService::class)
    );
});

/** @var ContainerInterface $container */
$container->set(PayoutTypeStorage::class, function (ContainerInterface $c) {
    return new PayoutTypeStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(AssetFactory::class, function (ContainerInterface $c) {
    return new AssetFactory();
});

$container->set(AssetPriceService::class, function (ContainerInterface $c) {
    return new AssetPriceService(
        $c->get(AssetsPriceStorage::class),
        $c->get(OperationService::class),
        $c->get(AmqpClient::class)
    );
});
