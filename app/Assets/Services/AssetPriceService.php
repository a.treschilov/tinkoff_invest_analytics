<?php

declare(strict_types=1);

namespace App\Assets\Services;

use App\Assets\Entities\AssetsPriceEntity;
use App\Assets\Models\AssetModel;
use App\Assets\Storages\AssetsPriceStorage;
use App\Broker\Types\BrokerOperationStatus;
use App\Broker\Types\BrokerOperationStatusType;
use App\Broker\Types\BrokerOperationTypeType;
use App\Common\Amqp\AmqpClient;
use App\Item\Collections\OperationCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\OperationFiltersModel;
use App\Item\Models\OperationModel;
use App\Item\Services\OperationService;
use App\Models\PriceModel;

class AssetPriceService
{
    public function __construct(
        private readonly AssetsPriceStorage $assetsPriceStorage,
        private readonly OperationService $operationService,
        private readonly AmqpClient $amqpClient
    ) {
    }

    public function getPrice(int $assetId, \DateTime $date): ?PriceModel
    {
        $price = $this->assetsPriceStorage->findLastPrice($assetId, $date);

        if ($price === null) {
            return null;
        }

        return new PriceModel([
            'amount' => $price->getAmount(),
            'currency' => $price->getCurrency()
        ]);
    }

    public function updateAssetPrice(int $userId, ItemModel $item, \DateTime $date): void
    {
        $currentDate = new \DateTime();

        $this->setOutDatePrices($item->getAsset()->getAssetId(), $date, $currentDate);
        $this->setAssetPrice($item, $userId, $date, $currentDate);

        if ($this->isOutDatedAsset($item->getAsset())) {
            $message = [
                'type' => 'outDateItem',
                'data' => [
                    'itemId' => $item->getItemId()
                ]
            ];
            $this->amqpClient->publishMessage($message);
        }
    }

    private function setOutDatePrices(int $assetId, \DateTime $from, \DateTime $to): void
    {
        $startDate = clone $from;
        $startDate->setTime(0, 0, 0);
        $prices = $this->assetsPriceStorage->findByAssetAndDateInterval($assetId, $startDate, $to);

        foreach ($prices as $price) {
            $price->setIsActive(0);
        }

        $this->assetsPriceStorage->addEntityArray($prices);
    }

    private function setAssetPrice(ItemModel $item, int $userId, \DateTime $from, \DateTime $to): void
    {
        $assetPrices = [];

        $dealDate = new \DateTime(OperationFiltersModel::$DEFAULT_DATE_FROM);
        $filter = new OperationFiltersModel([
            'itemId' => [$item->getItemId()],
            'operationStatus' => [BrokerOperationStatus::DONE->value]
        ]);
        $operations = $this->operationService->getFilteredUserOperation($userId, $filter);

        $updatedOperations = $operations->filterByDateInterval($from, $to);
        /** @var OperationModel $updatedOperation */
        foreach ($updatedOperations->getIterator() as $updatedOperation) {
            $skip = false;
            $currentDate = $updatedOperation->getDate();

            /** @var AssetsPriceEntity $price */
            foreach ($assetPrices as $price) {
                if ($price->getDate()->getTimestamp() === $currentDate->getTimestamp()) {
                    $skip = true;
                    break;
                }
            }
            if ($skip) {
                continue;
            }
            $dateOperations = $operations->filterByDateInterval($dealDate, $currentDate);
            $price = $this->calculateUserAssetPrice($dateOperations);

            $assetPrice = new AssetsPriceEntity();
            $assetPrice->setAssetId($item->getAsset()->getAssetId());
            $assetPrice->setAmount($price->getAmount());
            $assetPrice->setCurrency($price->getCurrency());
            $assetPrice->setDate(clone $currentDate);
            $assetPrice->setIsActive(1);
            $assetPrices[] = $assetPrice;
        }

        $this->assetsPriceStorage->addEntityArray($assetPrices);
    }

    private function calculateUserAssetPrice(OperationCollection $operations): PriceModel
    {
        $currency = 'RUB';
        $quantity = 0;

        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_BUY) {
                $quantity += -1 * $operation->getAmount();
            }
        }
        /** @var OperationModel $operation */
        foreach ($operations->getIterator() as $operation) {
            if (
                in_array(
                    $operation->getOperationType(),
                    [
                        BrokerOperationTypeType::OPERATION_TYPE_SELL,
                        BrokerOperationTypeType::OPERATION_TYPE_DEFAULT
                    ]
                )
            ) {
                $quantity -= $operation->getAmount();
            }
        }

        $amount = 1;
        if ($quantity === 0) {
            $amount = 0;
        } else {
            /** @var OperationModel $operation */
            foreach ($operations->getIterator() as $operation) {
                if ($operation->getOperationType() === BrokerOperationTypeType::OPERATION_TYPE_REPAYMENT) {
                    $amount -= $operation->getAmount() / $quantity;
                }
            }
        }

        return new PriceModel([
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    private function isOutDatedAsset(AssetModel $asset): bool
    {
        $today = new \DateTime();
        $price = $this->getPrice($asset->getAssetId(), $today);
        return $price->getAmount() < 0.01 && $price->getAmount() > -0.01;
    }
}
