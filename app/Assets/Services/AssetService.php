<?php

declare(strict_types=1);

namespace App\Assets\Services;

use App\Assets\Models\PaymentScheduleCalculatorModel;
use App\Assets\Models\PayoutModel;
use App\Intl\Services\CurrencyService;
use App\Item\Collections\PaymentScheduleCollection;
use App\Item\Models\ItemModel;
use App\Item\Models\PaymentScheduleModel;
use App\Models\PriceModel;

class AssetService
{
    public function __construct(
        private readonly AssetFactory $assetFactory,
        private readonly CurrencyService $currencyService,
        private readonly AssetPriceService $assetPriceService
    ) {
    }

    public function getUserAssetPrice(int $assetId, \DateTime $date): ?PriceModel
    {
        return $this->assetPriceService->getPrice($assetId, $date);
    }

    public function calculatePaymentSchedule(ItemModel $item): PaymentScheduleCollection
    {
        $paymentScheduleCollection = new PaymentScheduleCollection();

        $asset = $item->getAsset();

        if ($asset->getDealDate() === null) {
            return $paymentScheduleCollection;
        }

        if ($asset->getInterestAmount() === 0.0 && $asset->getInterestPercent() === null) {
            return $paymentScheduleCollection;
        }

        $calculator = $this->assetFactory->getCalculator(
            $asset->getPayoutType()
        );

        $scheduleModel = new PaymentScheduleCalculatorModel([
            'date' => $asset->getDealDate(),
            'interestPercent' => $asset->getInterestPercent(),
            'interestAmount' => $asset->getInterestAmount(),
            'payoutFrequency' => $asset->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $asset->getPayoutFrequencyPeriod(),
            'duration' => $asset->getDuration(),
            'durationPeriod' => $asset->getDurationPeriod(),
            'nominalAmount' => $asset->getNominalAmount(),
            'currencyId' => $this->currencyService->getCurrencyByIso($asset->getCurrencyIso())->getId()
        ]);
        $calculator->setScheduleModel($scheduleModel);
        $payoutCollection = $calculator->calculatePaymentSchedule();

        /** @var PayoutModel $payout */
        foreach ($payoutCollection->getIterator() as $payout) {
            $paymentScheduleCollection->add(new PaymentScheduleModel([
                'itemId' => $item->getItemId(),
                'interestAmount' => $payout->getInterest(),
                'debtAmount' => $payout->getDebtPayout(),
                'currency' => 'RUB',
                'date' => $payout->getDate()
            ]));
        }

        return $paymentScheduleCollection;
    }

    public function updateItemPrice(int $userId, ItemModel $item, \DateTime $date): void
    {
        $this->assetPriceService->updateAssetPrice($userId, $item, $date);
    }
}
