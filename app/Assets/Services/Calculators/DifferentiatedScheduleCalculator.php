<?php

declare(strict_types=1);

namespace App\Assets\Services\Calculators;

use App\Assets\Collections\PayoutCollection;
use App\Assets\Interfaces\AssetCalculatorInterface;
use App\Assets\Models\PaymentScheduleCalculatorModel;
use App\Assets\Models\PayoutModel;

class DifferentiatedScheduleCalculator implements AssetCalculatorInterface
{
    private PaymentScheduleCalculatorModel $paymentScheduleModel;

    public function setScheduleModel(PaymentScheduleCalculatorModel $paymentScheduleModel): void
    {
        $this->paymentScheduleModel = $paymentScheduleModel;
    }

    public function calculatePaymentSchedule(): PayoutCollection
    {
        $schedule = new PayoutCollection();

        $endDate = clone $this->paymentScheduleModel->getDate();
        $interval = $this->buildDateInterval(
            $this->paymentScheduleModel->getDuration(),
            $this->paymentScheduleModel->getDurationPeriod()
        );
        $endDate->add($interval);

        $payoutFrequency = $this->buildDateInterval(
            $this->paymentScheduleModel->getPayoutFrequency(),
            $this->paymentScheduleModel->getPayoutFrequencyPeriod()
        );

        $isLastPayout = false;
        $startOfPeriod = clone $this->paymentScheduleModel->getDate();
        $payoutDate = clone $this->paymentScheduleModel->getDate();
        $payoutDate = min($endDate, $payoutDate->add($payoutFrequency));

        $debt = $this->paymentScheduleModel->getNominalAmount();

        $schedule->add(new PayoutModel([
            'date' => clone $startOfPeriod,
            'interest' => 0,
            'debtPayout' => 0
        ]));
        while ($isLastPayout === false) {
            if ($payoutDate === $endDate) {
                $isLastPayout = true;
            }
            $interval = $payoutDate->diff($startOfPeriod);

            $interest = $debt
                * $this->paymentScheduleModel->getInterestPercent()
                / (date('L') ? 366 : 365)  * $interval->days;
            $debtPayout = $this->paymentScheduleModel->getNominalAmount()
                / ($this->paymentScheduleModel->getDate()->diff($endDate))->days
                * $interval->days;
            $debt -= $debtPayout;
            $schedule->add(new PayoutModel([
                'date' => clone $payoutDate,
                'interest' => $interest,
                'debtPayout' => $debtPayout
            ]));

            $startOfPeriod = clone $payoutDate;
            $payoutDate = min($endDate, $payoutDate->add($payoutFrequency));
        }
        return $schedule;
    }

    private function buildDateInterval(int $duration, string $durationPeriod): \DateInterval
    {
        $intervalPeriod = match ($durationPeriod) {
            'day' => 'D',
            'month' => 'M',
            'year' => 'Y'
        };
        return  new \DateInterval('P' . $duration . $intervalPeriod);
    }
}
