<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class ImportStatisticModel extends BaseModel
{
    private int $added = 0;
    private int $updated = 0;
    private int $skipped = 0;

    /**
     * @return int
     */
    public function getAdded(): int
    {
        return $this->added;
    }

    /**
     * @param int $added
     */
    public function setAdded(int $added): void
    {
        $this->added = $added;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getSkipped(): int
    {
        return $this->skipped;
    }

    /**
     * @param int $skipped
     */
    public function setSkipped(int $skipped): void
    {
        $this->skipped = $skipped;
    }
}
