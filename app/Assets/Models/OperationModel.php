<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class OperationModel extends BaseModel
{
    private string $externalId;
    private \DateTime $date;
    private float $amount;
    private string $currencyIso;
    private string $operationType;
    private string $comment;
    private string $brokerId;
    private ?float $quantity = null;
    private string $status;
    private ?AssetModel $asset = null;
    private ?int $dayOrder = null;

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return string
     */
    public function getOperationType(): string
    {
        return $this->operationType;
    }

    /**
     * @param string $operationType
     */
    public function setOperationType(string $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return AssetModel|null
     */
    public function getAsset(): ?AssetModel
    {
        return $this->asset;
    }

    /**
     * @param AssetModel|null $asset
     */
    public function setAsset(?AssetModel $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return string
     */
    public function getBrokerId(): string
    {
        return $this->brokerId;
    }

    /**
     * @param string $brokerId
     */
    public function setBrokerId(string $brokerId): void
    {
        $this->brokerId = $brokerId;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param float|null $quantity
     */
    public function setQuantity(?float $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getDayOrder(): ?int
    {
        return $this->dayOrder;
    }

    public function setDayOrder(?int $dayOrder): void
    {
        $this->dayOrder = $dayOrder;
    }
}
