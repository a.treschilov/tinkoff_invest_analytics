<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class AssetModel extends BaseModel
{
    private ?int $assetId = null;
    private ?string $externalId = null;
    private string $platform;
    private string $name;
    private string $type;
    private ?string $payoutType = null;
    private ?\DateTime $dealDate = null;
    private ?int $duration = null;
    private ?string $durationPeriod = null;
    private ?float $interestPercent = null;
    private ?float $interestAmount = null;
    private ?int $payoutFrequency = null;
    private ?string $payoutFrequencyPeriod = null;
    private ?float $nominalAmount = null;
    private ?string $currencyIso = null;
    private ?float $debtRest = null;

    /**
     * @return int|null
     */
    public function getAssetId(): ?int
    {
        return $this->assetId;
    }

    /**
     * @param int|null $assetId
     */
    public function setAssetId(?int $assetId): void
    {
        $this->assetId = $assetId;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform(string $platform): void
    {
        $this->platform = $platform;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getPayoutType(): ?string
    {
        return $this->payoutType;
    }

    /**
     * @param string|null $payoutType
     */
    public function setPayoutType(?string $payoutType): void
    {
        $this->payoutType = $payoutType;
    }

    /**
     * @return \DateTime|null
     */
    public function getDealDate(): ?\DateTime
    {
        return $this->dealDate;
    }

    /**
     * @param \DateTime|null $dealDate
     */
    public function setDealDate(?\DateTime $dealDate): void
    {
        $this->dealDate = $dealDate;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     */
    public function setDuration(?int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string|null
     */
    public function getDurationPeriod(): ?string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string|null $durationPeriod
     */
    public function setDurationPeriod(?string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return float|null
     */
    public function getInterestPercent(): ?float
    {
        return $this->interestPercent;
    }

    /**
     * @param float|null $interestPercent
     */
    public function setInterestPercent(?float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return float|null
     */
    public function getInterestAmount(): ?float
    {
        return $this->interestAmount;
    }

    /**
     * @param float|null $interestAmount
     */
    public function setInterestAmount(?float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return int|null
     */
    public function getPayoutFrequency(): ?int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int|null $payoutFrequency
     */
    public function setPayoutFrequency(?int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string|null
     */
    public function getPayoutFrequencyPeriod(): ?string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string|null $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(?string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float|null
     */
    public function getNominalAmount(): ?float
    {
        return $this->nominalAmount;
    }

    /**
     * @param float|null $nominalAmount
     */
    public function setNominalAmount(?float $nominalAmount): void
    {
        $this->nominalAmount = $nominalAmount;
    }

    /**
     * @return string|null
     */
    public function getCurrencyIso(): ?string
    {
        return $this->currencyIso;
    }

    /**
     * @param string|null $currencyIso
     */
    public function setCurrencyIso(?string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return float|null
     */
    public function getDebtRest(): ?float
    {
        return $this->debtRest;
    }

    /**
     * @param float|null $debtRest
     */
    public function setDebtRest(?float $debtRest): void
    {
        $this->debtRest = $debtRest;
    }
}
