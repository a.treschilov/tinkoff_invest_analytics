<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class PaymentScheduleCalculatorModel extends BaseModel
{
    private \DateTime $date;
    private ?float $interestPercent = null;
    private ?float $interestAmount = null;
    private int $payoutFrequency;
    private string $payoutFrequencyPeriod;
    private int $duration;
    private string $durationPeriod;
    private float $nominalAmount;
    private int $currencyId;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float|null
     */
    public function getInterestPercent(): ?float
    {
        return $this->interestPercent;
    }

    /**
     * @param float|null $interestPercent
     */
    public function setInterestPercent(?float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return float|null
     */
    public function getInterestAmount(): ?float
    {
        return $this->interestAmount;
    }

    /**
     * @param float|null $interestAmount
     */
    public function setInterestAmount(?float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return int
     */
    public function getPayoutFrequency(): int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int $payoutFrequency
     */
    public function setPayoutFrequency(int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string
     */
    public function getPayoutFrequencyPeriod(): string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getDurationPeriod(): string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string $durationPeriod
     */
    public function setDurationPeriod(string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return float
     */
    public function getNominalAmount(): float
    {
        return $this->nominalAmount;
    }

    /**
     * @param float $nominalAmount
     */
    public function setNominalAmount(float $nominalAmount): void
    {
        $this->nominalAmount = $nominalAmount;
    }

    /**
     * @return int
     */
    public function getCurrencyId(): int
    {
        return $this->currencyId;
    }

    /**
     * @param int $currencyId
     */
    public function setCurrencyId(int $currencyId): void
    {
        $this->currencyId = $currencyId;
    }
}
