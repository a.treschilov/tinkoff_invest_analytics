<?php

declare(strict_types=1);

namespace App\Assets\Models;

use App\Common\Models\BaseModel;

class PayoutModel extends BaseModel
{
    private \DateTime $date;
    private float $interest;
    private float $debtPayout;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return float
     */
    public function getInterest(): float
    {
        return $this->interest;
    }

    /**
     * @param float $interest
     */
    public function setInterest(float $interest): void
    {
        $this->interest = $interest;
    }

    /**
     * @return float
     */
    public function getDebtPayout(): float
    {
        return $this->debtPayout;
    }

    /**
     * @param float $debtPayout
     */
    public function setDebtPayout(float $debtPayout): void
    {
        $this->debtPayout = $debtPayout;
    }
}
