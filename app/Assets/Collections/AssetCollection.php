<?php

declare(strict_types=1);

namespace App\Assets\Collections;

use App\Assets\Models\AssetModel;
use Doctrine\Common\Collections\ArrayCollection;

class AssetCollection extends ArrayCollection
{
    public function findByExternalId(string $externalId): ?AssetModel
    {
        /** @var AssetModel $asset */
        foreach ($this->getIterator() as $asset) {
            if ($asset->getExternalId() === $externalId) {
                return $asset;
            }
        }

        return null;
    }
}
