<?php

declare(strict_types=1);

namespace App\Assets\Exceptions;

use App\Common\BaseException;

class ThirdPartyUploadException extends BaseException
{
    protected static $CODE = 3000;
}
