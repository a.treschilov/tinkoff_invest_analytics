<?php

declare(strict_types=1);

namespace App\Assets\Storages;

use App\Assets\Entities\AssetsPriceEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class AssetsPriceStorage
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param int $assetId
     * @param \DateTime $from
     * @param \DateTime $to
     * @return AssetsPriceEntity[]
     */
    public function findByAssetAndDateInterval(int $assetId, \DateTime $from, \DateTime $to): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('assetId', $assetId));
        $criteria->andWhere(Criteria::expr()->eq('isActive', 1));
        $criteria->andWhere(Criteria::expr()->gte('date', $from));
        $criteria->andWhere(Criteria::expr()->lte('date', $to));

        return $this->entityManager->getRepository(AssetsPriceEntity::class)
            ->matching($criteria)->toArray();
    }

    public function findLastPrice(int $assetId, \DateTime $date): AssetsPriceEntity|null
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('assetId', $assetId));
        $criteria->andWhere(Criteria::expr()->eq('isActive', 1));
        $criteria->andWhere(Criteria::expr()->lte('date', $date));
        $criteria->orderBy(['date' => Criteria::DESC]);
        $criteria->setMaxResults(1);

        $price = $this->entityManager->getRepository(AssetsPriceEntity::class)
            ->matching($criteria)->first();

        return $price === false ? null : $price;
    }

    /**
     * @param AssetsPriceEntity[] $prices
     * @return void
     */
    public function addEntityArray(array $prices): void
    {
        foreach ($prices as $price) {
            $this->entityManager->persist($price);
        }

        $this->entityManager->flush();
    }
}
