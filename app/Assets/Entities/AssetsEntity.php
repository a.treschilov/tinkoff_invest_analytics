<?php

declare(strict_types=1);

namespace App\Assets\Entities;

use App\Intl\Entities\CurrencyEntity;
use App\Item\Entities\ItemEntity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.assets')]
#[ORM\Entity]
class AssetsEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'asset_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'item_id', type: 'integer')]
    protected int $itemId;

    #[ORM\JoinColumn(name: 'item_id', referencedColumnName: 'item_id')]
    #[ORM\OneToOne(targetEntity: ItemEntity::class, inversedBy: 'asset', cascade: ['persist'])]
    protected ItemEntity $item;

    #[ORM\JoinColumn(name: 'payout_type_id', referencedColumnName: 'payout_type_id')]
    #[ORM\OneToOne(targetEntity: PayoutTypeEntity::class)]
    protected ?PayoutTypeEntity $payoutType = null;

    #[ORM\Column(name: 'payout_type_id', type: 'integer')]
    protected ?int $payoutTypeId = null;

    #[ORM\Column(name: 'external_id', type: 'string')]
    protected ?string $externalId = null;

    #[ORM\Column(name: 'platform', type: 'string')]
    protected string $platform;

    #[ORM\Column(name: 'name', type: 'string')]
    protected string $name;

    #[ORM\Column(name: 'type', type: 'string')]
    protected string $type;

    #[ORM\Column(name: 'deal_date', type: 'datetime')]
    protected ?\DateTime $dealDate = null;

    #[ORM\Column(name: 'duration', type: 'integer')]
    protected ?int $duration = null;

    #[ORM\Column(name: 'duration_period', type: 'string')]
    protected ?string $durationPeriod = null;

    #[ORM\Column(name: 'interest_percent', type: 'float')]
    protected ?float $interestPercent = null;

    #[ORM\Column(name: 'interest_amount', type: 'float')]
    protected ?float $interestAmount = null;

    #[ORM\Column(name: 'payout_frequency', type: 'integer')]
    protected ?int $payoutFrequency = null;

    #[ORM\Column(name: 'payout_frequency_period', type: 'string')]
    protected ?string $payoutFrequencyPeriod = null;

    #[ORM\Column(name: 'nominal_amount', type: 'float')]
    protected ?float $nominalAmount = null;

    #[ORM\Column(name: 'currency_id', type: 'integer')]
    protected ?int $currencyId = null;

    #[ORM\JoinColumn(name: 'currency_id', referencedColumnName: 'currency_id')]
    #[ORM\ManyToOne(targetEntity: CurrencyEntity::class)]
    protected ?CurrencyEntity $currency = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform(string $platform): void
    {
        $this->platform = $platform;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return ItemEntity
     */
    public function getItem(): ItemEntity
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item): void
    {
        $this->item = $item;
    }

    /**
     * @return PayoutTypeEntity|null
     */
    public function getPayoutType(): ?PayoutTypeEntity
    {
        return $this->payoutType;
    }

    /**
     * @param PayoutTypeEntity|null $payoutType
     */
    public function setPayoutType(?PayoutTypeEntity $payoutType): void
    {
        $this->payoutType = $payoutType;
    }

    /**
     * @return int|null
     */
    public function getPayoutTypeId(): ?int
    {
        return $this->payoutTypeId;
    }

    /**
     * @param int|null $payoutTypeId
     */
    public function setPayoutTypeId(?int $payoutTypeId): void
    {
        $this->payoutTypeId = $payoutTypeId;
    }

    /**
     * @return \DateTime|null
     */
    public function getDealDate(): ?\DateTime
    {
        return $this->dealDate;
    }

    /**
     * @param \DateTime|null $dealDate
     */
    public function setDealDate(?\DateTime $dealDate): void
    {
        $this->dealDate = $dealDate;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     */
    public function setDuration(?int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string|null
     */
    public function getDurationPeriod(): ?string
    {
        return $this->durationPeriod;
    }

    /**
     * @param string|null $durationPeriod
     */
    public function setDurationPeriod(?string $durationPeriod): void
    {
        $this->durationPeriod = $durationPeriod;
    }

    /**
     * @return float|null
     */
    public function getInterestPercent(): ?float
    {
        return $this->interestPercent;
    }

    /**
     * @param float|null $interestPercent
     */
    public function setInterestPercent(?float $interestPercent): void
    {
        $this->interestPercent = $interestPercent;
    }

    /**
     * @return float|null
     */
    public function getInterestAmount(): ?float
    {
        return $this->interestAmount;
    }

    /**
     * @param float|null $interestAmount
     */
    public function setInterestAmount(?float $interestAmount): void
    {
        $this->interestAmount = $interestAmount;
    }

    /**
     * @return int|null
     */
    public function getPayoutFrequency(): ?int
    {
        return $this->payoutFrequency;
    }

    /**
     * @param int|null $payoutFrequency
     */
    public function setPayoutFrequency(?int $payoutFrequency): void
    {
        $this->payoutFrequency = $payoutFrequency;
    }

    /**
     * @return string|null
     */
    public function getPayoutFrequencyPeriod(): ?string
    {
        return $this->payoutFrequencyPeriod;
    }

    /**
     * @param string|null $payoutFrequencyPeriod
     */
    public function setPayoutFrequencyPeriod(?string $payoutFrequencyPeriod): void
    {
        $this->payoutFrequencyPeriod = $payoutFrequencyPeriod;
    }

    /**
     * @return float|null
     */
    public function getNominalAmount(): ?float
    {
        return $this->nominalAmount;
    }

    /**
     * @param float|null $nominalAmount
     */
    public function setNominalAmount(?float $nominalAmount): void
    {
        $this->nominalAmount = $nominalAmount;
    }

    /**
     * @return int|null
     */
    public function getCurrencyId(): ?int
    {
        return $this->currencyId;
    }

    /**
     * @param ?int $currencyId
     */
    public function setCurrencyId(?int $currencyId): void
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return CurrencyEntity|null
     */
    public function getCurrency(): ?CurrencyEntity
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEntity|null $currency
     */
    public function setCurrency(?CurrencyEntity $currency): void
    {
        $this->currency = $currency;
    }
}
