<?php

declare(strict_types=1);

namespace App\Assets\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.assets_price')]
#[ORM\Entity]
class AssetsPriceEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'assets_price_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'asset_id', type: 'integer')]
    protected int $assetId;

    #[ORM\Column(name: 'amount', type: 'float')]
    protected float $amount;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    #[ORM\Column(name: 'date', type: 'datetime')]
    protected \DateTime $date;

    #[ORM\Column(name: 'is_active', type: 'integer')]
    protected int $isActive;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAssetId(): int
    {
        return $this->assetId;
    }

    /**
     * @param int $assetId
     */
    public function setAssetId(int $assetId): void
    {
        $this->assetId = $assetId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }
}
