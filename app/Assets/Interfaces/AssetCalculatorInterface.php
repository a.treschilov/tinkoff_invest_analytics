<?php

declare(strict_types=1);

namespace App\Assets\Interfaces;

use App\Assets\Collections\PayoutCollection;
use App\Assets\Models\PaymentScheduleCalculatorModel;

interface AssetCalculatorInterface
{
    public function setScheduleModel(PaymentScheduleCalculatorModel $paymentScheduleModel): void;
    public function calculatePaymentSchedule(): PayoutCollection;
}
