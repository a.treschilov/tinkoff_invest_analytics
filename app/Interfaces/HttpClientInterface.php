<?php

declare(strict_types=1);

namespace App\Interfaces;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    public function __construct(Client $client);

    public function doRequest(
        string $method,
        string $url,
        array $body,
        array $headers,
        array $options
    ): ResponseInterface;
}
