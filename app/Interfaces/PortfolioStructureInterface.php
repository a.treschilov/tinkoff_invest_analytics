<?php

declare(strict_types=1);

namespace App\Interfaces;

interface PortfolioStructureInterface
{
    public function toArray(): array;
}
