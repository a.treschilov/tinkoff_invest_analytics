<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Models\MarketInstrumentModel;

interface MarketInstrumentServiceInterface
{
    public function getInstrumentByIsin(string $isin): ?MarketInstrumentEntity;
    public function getInstrumentByIsinList(array $isinList): MarketInstrumentCollection;
    public function getInstrumentEntityById(int $marketInstrumentId): ?MarketInstrumentEntity;
}
