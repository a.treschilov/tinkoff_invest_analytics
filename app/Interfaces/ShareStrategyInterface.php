<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Broker\Collections\PortfolioBalanceCollection;
use App\Collections\InstrumentCollection;
use App\Collections\ShareCollection;

interface ShareStrategyInterface
{
    /**
     * @param InstrumentCollection $instruments
     * @return ShareCollection
     */
    public function calculateShare(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): ShareCollection;

    public function getTargetShare(int $userId): ShareCollection;

    public function updateTargetShare(int $userId, ShareCollection $shares): void;

    public function calculateTotalAmount(
        InstrumentCollection $instruments,
        PortfolioBalanceCollection $balance
    ): float;
}
