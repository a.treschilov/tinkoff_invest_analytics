<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Models\ImportStatisticModel;
use App\Models\OperationFiltersModel;

interface MarketOperationServiceInterface
{
    public function importBrokerOperations(
        OperationFiltersModel $operationFilter,
        int $userBrokerAccountId
    ): ImportStatisticModel;
}
