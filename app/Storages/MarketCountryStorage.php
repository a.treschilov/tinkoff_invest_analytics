<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\MarketCountryEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketCountryStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return MarketCountryEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(MarketCountryEntity::class)
            ->findAll();
    }

    /**
     * @return MarketCountryEntity|null
     */
    public function findOneByIso(string $iso): ?object
    {
        return $this->entityManager->getRepository(MarketCountryEntity::class)
            ->findOneBy(['iso' => $iso]);
    }
}
