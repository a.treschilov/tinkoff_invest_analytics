<?php

declare(strict_types=1);

namespace App\Storages;

use App\Collections\OperationTypeCollection;
use App\Entities\OperationTypeEntity;
use Doctrine\ORM\EntityManagerInterface;

class OperationTypeStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function findByExternalId(string $externalId): ?OperationTypeEntity
    {
        return $this->entityManager->getRepository(OperationTypeEntity::class)
            ->findOneBy(['externalId' => $externalId]);
    }
}
