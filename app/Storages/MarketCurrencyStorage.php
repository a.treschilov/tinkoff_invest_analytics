<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\MarketCurrencyEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketCurrencyStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return MarketCurrencyEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(MarketCurrencyEntity::class)
            ->findAll();
    }

    public function addEntity(MarketCurrencyEntity $currency): void
    {
        $this->entityManager->persist($currency);
        $this->entityManager->flush();
    }

    /**
     * @param string $iso
     * @return MarketCurrencyEntity|null
     */
    public function findOneByIso(string $iso): ?object
    {
        return $this->entityManager->getRepository(MarketCurrencyEntity::class)
            ->findOneBy(['iso' => $iso]);
    }
}
