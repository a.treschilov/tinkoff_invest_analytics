<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\WealthHistoryEntity;
use App\Item\Entities\ItemOperationEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class WealthHistoryStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addEntity(WealthHistoryEntity $wealthHistoryEntity): void
    {
        $this->entityManager->persist($wealthHistoryEntity);
        $this->entityManager->flush();
    }

    /**
     * @param WealthHistoryEntity[] $wealthHistoryEntities
     * @return void
     */
    public function addEntityArray(array $wealthHistoryEntities): void
    {
        foreach ($wealthHistoryEntities as $wealthHistoryEntity) {
            $this->entityManager->persist($wealthHistoryEntity);
        }
        $this->entityManager->flush();
    }

    /**
     * @param int $userId
     * @param \DateTime $from
     * @param \DateTime $to
     * @return WealthHistoryEntity[]
     */
    public function findByDateIntervalAndUser(int $userId, \DateTime $from, \DateTime $to): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId));
        $criteria->andWhere(Criteria::expr()->gte('date', $from));
        $criteria->andWhere(Criteria::expr()->lte('date', $to));

        return $this->entityManager->getRepository(WealthHistoryEntity::class)
            ->matching($criteria)->toArray();
    }

    public function findActiveByDateIntervalAndUser(int $userId, \DateTime $from, \DateTime $to): array
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId));
        $criteria->andWhere(Criteria::expr()->gte('date', $from));
        $criteria->andWhere(Criteria::expr()->lte('date', $to));
        $criteria->andWhere(Criteria::expr()->eq('isActive', 1));

        return $this->entityManager->getRepository(WealthHistoryEntity::class)
            ->matching($criteria)->toArray();
    }

    public function findActiveByUser(int $userId): array
    {
        return $this->entityManager->getRepository(WealthHistoryEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1], ['date' => 'ASC']);
    }

    public function findLastWealth(int $userId): ?WealthHistoryEntity
    {
        return $this->entityManager->getRepository(WealthHistoryEntity::class)
            ->findOneBy(['userId' => $userId, 'isActive' => 1], ['date' => 'Desc']);
    }

    public function findLastWealthByDate(int $userId, \DateTime $date): ?WealthHistoryEntity
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('userId', $userId));
        $criteria->andWhere(Criteria::expr()->lte('date', $date));
        $criteria->orderBy(['date' => Criteria::DESC]);
        $criteria->setMaxResults(1);

        $wealthHistory = $this->entityManager->getRepository(WealthHistoryEntity::class)
            ->matching($criteria)->first();

        return $wealthHistory ? $wealthHistory : null;
    }
}
