<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\InstrumentTypeEntity;
use Doctrine\ORM\EntityManagerInterface;

class InstrumentTypeStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return InstrumentTypeEntity[]
     */
    public function findActive(): array
    {
        return $this->entityManager->getRepository(InstrumentTypeEntity::class)
            ->findBy(['isActive' => 1]);
    }

    public function findByExternalId(string $externalId): InstrumentTypeEntity|null
    {
        return $this->entityManager->getRepository(InstrumentTypeEntity::class)
            ->findOneBy(['externalId' => $externalId]);
    }
}
