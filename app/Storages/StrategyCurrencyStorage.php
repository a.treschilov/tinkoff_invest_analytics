<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\StrategyCurrencyEntity;
use Doctrine\ORM\EntityManagerInterface;

class StrategyCurrencyStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $userId
     * @return StrategyCurrencyEntity[]
     */
    public function findActiveByUserId(int $userId): array
    {
        return $this->entityManager->getRepository(StrategyCurrencyEntity::class)
            ->findBy(['userId' => $userId, 'isActive' => 1]);
    }

    /**
     * @param StrategyCurrencyEntity[] $strategyCurrencyEntityArray
     * @return void
     */
    public function updateArrayEntities(array $strategyCurrencyEntityArray)
    {
        foreach ($strategyCurrencyEntityArray as $entity) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }
}
