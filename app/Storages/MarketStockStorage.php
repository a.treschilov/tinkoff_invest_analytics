<?php

declare(strict_types=1);

namespace App\Storages;

use App\Market\Entities\MarketInstrumentEntity;
use App\Entities\MarketStockEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketStockStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function addEntity(MarketStockEntity $marketStockEntity): void
    {
        $this->entityManager->persist($marketStockEntity);
        $this->entityManager->flush();
    }
}
