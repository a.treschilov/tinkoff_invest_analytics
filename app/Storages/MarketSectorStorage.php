<?php

declare(strict_types=1);

namespace App\Storages;

use App\Entities\MarketSectorEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketSectorStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return \App\Entities\MarketSectorEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(MarketSectorEntity::class)
            ->findBy([], ['name' => 'ASC']);
    }

    public function findById(int $marketSectorId): MarketSectorEntity|null
    {
        return $this->entityManager->getRepository(MarketSectorEntity::class)
            ->findOneBy(['id' => $marketSectorId]);
    }

    /**
     * @param int[] $marketSectorId
     * @return MarketSectorEntity[]
     */
    public function findByIds(array $marketSectorId): array
    {
        return $this->entityManager->getRepository(MarketSectorEntity::class)
            ->findBy(['id' => $marketSectorId]);
    }

    public function findOneByExternalId(string $externalId): MarketSectorEntity|null
    {
        return $this->entityManager->getRepository(MarketSectorEntity::class)
            ->findOneBy(['externalId' => $externalId]);
    }
}
