<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Loan\Models\LoanModel;

class LoanFormatter implements FormatterInterface
{
    private LoanModel $loan;

    public function __construct(LoanModel $loan)
    {
        $this->loan = $loan;
    }

    public function format(): array
    {
        return [
            'loanId' => $this->loan->getLoanId(),
            'itemId' => $this->loan->getItemId(),
            'name' => $this->loan->getName(),
            'amount' => $this->loan->getAmount(),
            'currency' => $this->loan->getCurrency(),
            'date' => $this->loan->getDate(),
            'isActive' => $this->loan->getIsActive(),
            'closedDate' => $this->loan->getClosedDate(),
            'paymentAmount' => $this->loan->getPaymentAmount(),
            'paymentCurrency' => $this->loan->getPaymentCurrency(),
            'payoutFrequency' => $this->loan->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $this->loan->getPayoutFrequencyPeriod(),
            'interestPercent' => $this->loan->getInterestPercent()
        ];
    }
}
