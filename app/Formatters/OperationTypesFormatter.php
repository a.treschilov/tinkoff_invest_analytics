<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\OperationTypeCollection;
use App\Item\Models\OperationTypeModel;

class OperationTypesFormatter implements FormatterInterface
{
    private OperationTypeCollection $operationTypeCollection;

    public function __construct(OperationTypeCollection $operationTypeCollection)
    {
        $this->operationTypeCollection = $operationTypeCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var OperationTypeModel $operationType */
        foreach ($this->operationTypeCollection->getIterator() as $operationType) {
            $formattedData[] = [
                'id' => $operationType->getExternalId(),
                'name' => $operationType->getName()
            ];
        }

        return $formattedData;
    }
}
