<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\PortfolioItemModel;

class PortfolioFullItemFormatter implements FormatterInterface
{
    private PortfolioItemModel $item;

    public function __construct(PortfolioItemModel $item)
    {
        $this->item = $item;
    }

    public function format(): array
    {
        $price = $this->item->getPrice() === null ? null : (new PriceFormatter($this->item->getPrice()))->format();
        $itemTypeShare = $this->item->getItemTypeShare() === null
            ? null
            : $this->item->getItemTypeShare();
        return [
            'itemId' => $this->item->getItemId(),
            'quantity' => $this->item->getQuantity(),
            'amount' => $this->item->getAmount(),
            'currencyIso' => $this->item->getCurrencyIso(),
            'item' => (new ItemFormatter($this->item->getItem()))->format(),
            'price' => $price,
            'baseAmount' => (new PriceFormatter($this->item->getBaseAmount()))->format(),
            'itemTypeShare' => $itemTypeShare,
        ];
    }
}
