<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Entities\CountryEntity;
use App\Intl\Entities\CurrencyEntity;

class CurrencyFormatter implements FormatterInterface
{
    private CurrencyCollection $collection;

    public function __construct(CurrencyCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];

        /** @var CurrencyEntity $country */
        foreach ($this->collection as $country) {
            $formattedData[] = [
                'id' => $country->getId(),
                'iso' => $country->getIso(),
                'name' => $country->getName(),
                'symbol' => $country->getSymbol()
            ];
        }

        return $formattedData;
    }
}
