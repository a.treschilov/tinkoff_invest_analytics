<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\PortfolioItemModel;

class PortfolioItemFormatter implements FormatterInterface
{
    private PortfolioItemModel $item;

    public function __construct(PortfolioItemModel $item)
    {
        $this->item = $item;
    }

    public function format(): array
    {
        return [
            'itemId' => $this->item->getItemId(),
            'quantity' => $this->item->getQuantity(),
            'amount' => $this->item->getAmount(),
            'currencyIso' => $this->item->getCurrencyIso(),
            'item' => (new ItemFormatter($this->item->getItem()))->format()
        ];
    }
}
