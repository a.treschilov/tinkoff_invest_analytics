<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Models\MarketInstrumentModel;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

readonly class MarketStockFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentModel $instrument
    ) {
    }

    #[ArrayShape([
        'id' => "int",
        'itemId' => "int",
        'ticker' => "string",
        'isin' => "null|string",
        'name' => "string",
        'currency' => "string",
        'country' => "null|string",
        'sector' => "null|string",
        'industry' => "null|string"
    ])]
    #[Pure]
    public function format(): array
    {
        return [
            'id' => $this->instrument->getMarketInstrumentId(),
            'itemId' => $this->instrument->getItemId(),
            'ticker' => $this->instrument->getTicker(),
            'isin' => $this->instrument->getIsin(),
            'name' => $this->instrument->getName(),
            'currency' => $this->instrument->getCurrency(),
            'country' => $this->instrument?->getStock()?->getCountry(),
            'sector' => $this->instrument->getStock()?->getSector(),
            'industry' => $this->instrument->getStock()?->getIndustry()
        ];
    }
}
