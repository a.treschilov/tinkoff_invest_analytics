<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Models\WealthHistoryModel;

class WealthFormatter implements FormatterInterface
{
    private WealthHistoryModel $wealth;

    public function __construct(WealthHistoryModel $wealthHistoryModel)
    {
        $this->wealth = $wealthHistoryModel;
    }
    public function format(): array
    {
        return [
            'currency' => $this->wealth->getCurrencyIso(),
            'date' => $this->wealth->getDate(),
            'stock' => $this->wealth->getStock(),
            'assets' => $this->wealth->getAssets(),
            'realEstate' => $this->wealth->getRealEstate(),
            'deposit' => $this->wealth->getDeposit(),
            'loan' => $this->wealth->getLoan(),
            'expenses' => $this->wealth->getExpenses(),
            'wealthRate' => $this->wealth->getWealthRate()
        ];
    }
}
