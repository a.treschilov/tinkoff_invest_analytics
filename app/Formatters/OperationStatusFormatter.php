<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\OperationStatusCollection;
use App\Item\Models\OperationStatusModel;

class OperationStatusFormatter implements FormatterInterface
{
    private OperationStatusCollection $operationStatusCollection;

    public function __construct(OperationStatusCollection $operationStatusCollection)
    {
        $this->operationStatusCollection = $operationStatusCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var OperationStatusModel $operationStatus */
        foreach ($this->operationStatusCollection->getIterator() as $operationStatus) {
            $formattedData[] = [
                'id' => $operationStatus->getExternalId(),
                'name' => $operationStatus->getName(),
            ];
        }
        return $formattedData;
    }
}
