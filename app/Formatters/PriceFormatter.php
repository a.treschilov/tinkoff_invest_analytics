<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Models\PriceModel;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class PriceFormatter implements FormatterInterface
{
    private PriceModel $price;

    public function __construct(PriceModel $price)
    {
        $this->price = $price;
    }

    #[Pure]
    #[ArrayShape(['amount' => "float", 'currency' => "string"])]
    public function format(): array
    {
        return [
            'amount' => $this->price->getAmount(),
            'currency' => $this->price->getCurrency()
        ];
    }
}
