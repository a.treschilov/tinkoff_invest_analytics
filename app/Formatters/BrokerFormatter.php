<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Broker\Models\BrokerModel;

class BrokerFormatter implements FormatterInterface
{
    public function __construct(
        private readonly BrokerModel $broker
    ) {
    }

    public function format(): array
    {
        return [
            'id' => $this->broker->getBrokerId(),
            'name' => $this->broker->getName(),
            'logo' => $this->broker->getLogo(),
            'isApi' => $this->broker->isApi(),
            'isManual' => $this->broker->isManual()
        ];
    }
}
