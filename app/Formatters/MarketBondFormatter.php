<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Models\MarketInstrumentModel;

readonly class MarketBondFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentModel $instrument
    ) {
    }

    public function format(): array
    {
        $maturityDate = $this->instrument->getBond()->getMaturityDate();

        return [
            'id' => $this->instrument->getBond()->getMarketBondId(),
            'isin' => $this->instrument->getIsin(),
            'itemId' => $this->instrument->getItemId(),
            'name' => $this->instrument->getName(),
            'initialNominal' => (new PriceFormatter($this->instrument->getBond()->getInitialNominal()))->format(),
            'nominal' => (new PriceFormatter($this->instrument->getBond()->getNominal()))->format(),
            'maturityDate' => $maturityDate !== null ? (new DateFormatter($maturityDate))->format() : null,
        ];
    }
}
