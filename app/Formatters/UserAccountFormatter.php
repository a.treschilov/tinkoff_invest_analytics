<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Collections\UserAccountCollection;
use App\User\Models\UserAccountModel;

class UserAccountFormatter implements FormatterInterface
{
    public function __construct(
        private readonly UserAccountCollection $collection
    ) {
    }

    public function format(): array
    {
        $result = [];
        /** @var UserAccountModel $userAccount */
        foreach ($this->collection as $userAccount) {
            $result[] = [
                'code' => $userAccount->getAuthType()->value,
            ];
        }

        return $result;
    }
}
