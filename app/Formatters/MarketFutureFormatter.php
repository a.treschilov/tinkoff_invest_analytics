<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Models\MarketInstrumentModel;

readonly class MarketFutureFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentModel $instrument
    ) {
    }

    public function format(): array
    {
        return [
            'itemId' => $this->instrument?->getItemId(),
            'isin' => $this->instrument?->getIsin(),
            'name' => $this->instrument->getName(),
            'type' => $this->instrument?->getFuture()?->getFutureType()?->value,
            'assetType' => $this->instrument?->getFuture()?->getFutureAssetType()?->value,
            'expirationDate' => $this->instrument?->getFuture()?->getExpirationDate() === null
                ? null
                : (new DateFormatter($this->instrument->getFuture()->getExpirationDate()))->format()
        ];
    }
}
