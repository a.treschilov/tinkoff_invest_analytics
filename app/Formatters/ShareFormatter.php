<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Collections\CompareCollection;
use App\Interfaces\FormatterInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ShareFormatter implements FormatterInterface
{
    private CompareCollection $shareCollection;

    public function __construct(CompareCollection $shareCollection)
    {
        $this->shareCollection = $shareCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        foreach ($this->shareCollection->getIterator() as $key => $share) {
            $share['name'] = $key;
            $formattedData[] = $share;
        }
        return $formattedData;
    }
}
