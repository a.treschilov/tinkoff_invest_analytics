<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\OperationCollection;
use App\Item\Models\OperationModel;

class OperationListFormatter implements FormatterInterface
{
    private OperationCollection $operationCollection;

    public function __construct(OperationCollection $operationCollection)
    {
        $this->operationCollection = $operationCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var OperationModel $operation */
        foreach ($this->operationCollection->getIterator() as $operation) {
            $formattedData[] = (new OperationFormatter($operation))->format();
        }
        return $formattedData;
    }
}
