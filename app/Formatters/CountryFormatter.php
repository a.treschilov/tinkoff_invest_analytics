<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Intl\Collections\CountryCollection;
use App\Intl\Entities\CountryEntity;

class CountryFormatter implements FormatterInterface
{
    private CountryCollection $collection;

    public function __construct(CountryCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];

        /** @var CountryEntity $country */
        foreach ($this->collection as $country) {
            $formattedData[] = [
                'iso' => $country->getIso(),
                'name' => $country->getName(),
                'continent' => $country->getContinent()
            ];
        }

        return $formattedData;
    }
}
