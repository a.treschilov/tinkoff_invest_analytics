<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;

class DashboardFormatter implements FormatterInterface
{
    public function __construct(private array $dashboard)
    {
        $this->dashboard = [
            "targetCapital" => $dashboard['targetCapital'],
            "actives" => $dashboard['actives'],
            "passives" => $dashboard['passives'],
            "shouldIncrease" => $dashboard['shouldIncrease'],
            "saveSpeed" => $dashboard['saveSpeed'],
            "pension" => $dashboard['pension'],
            "futurePension" => $dashboard['futurePension'],
            "capital" => $dashboard['capital'],
            "predictInterest" => $dashboard['predictInterest'],
            "lastYearInterest" => $dashboard['lastYearInterest'],
            "wealthRate" => $dashboard['wealthRate'],
            "safeRatio" => $dashboard['safeRatio'],
            "capitalBefore" => [
                "day" => $dashboard['capitalBefore']['day'],
                "month" => $dashboard['capitalBefore']['month'],
                "year" => $dashboard['capitalBefore']['year']
            ]
        ];
    }

    public function format(): array
    {
        return [
            "targetCapital" => $this->dashboard['targetCapital'],
            "actives" => $this->dashboard['actives'],
            "passives" => $this->dashboard['passives'],
            "shouldIncrease" => $this->dashboard['shouldIncrease'],
            "saveSpeed" => $this->dashboard['saveSpeed'],
            "pension" => $this->dashboard['pension'],
            "futurePension" => $this->dashboard['futurePension'],
            "capital" => $this->dashboard['capital'],
            "predictInterest" => $this->dashboard['predictInterest'],
            "lastYearInterest" => $this->dashboard['lastYearInterest'],
            "wealthRate" => $this->dashboard['wealthRate'],
            "safeRatio" => $this->dashboard['safeRatio'],
            "capitalBefore" => [
                "day" => $this->dashboard['capitalBefore']['day'] === null
                    ? null
                    : (new PriceFormatter($this->dashboard['capitalBefore']['day']))->format(),
                "month" => $this->dashboard['capitalBefore']['month'] === null
                    ? null
                    : (new PriceFormatter($this->dashboard['capitalBefore']['month']))->format(),
                "year" => $this->dashboard['capitalBefore']['year'] === null
                    ? null
                    : (new PriceFormatter($this->dashboard['capitalBefore']['year']))->format()
            ]
        ];
    }
}
