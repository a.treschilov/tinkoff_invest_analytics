<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\ItemCollection;
use App\Item\Models\ItemModel;

class AssetListFormatter implements FormatterInterface
{
    private ItemCollection $collection;

    public function __construct(ItemCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];

        /** @var ItemModel $asset */
        foreach ($this->collection as $asset) {
            $formattedData[] = (new AssetItemFormatter($asset))->format();
        }

        return $formattedData;
    }
}
