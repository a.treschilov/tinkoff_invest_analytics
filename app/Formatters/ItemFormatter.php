<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\ItemModel;

class ItemFormatter implements FormatterInterface
{
    public function __construct(
        private ItemModel $item
    ) {
    }
    public function format(): array
    {
        $stock = null;
        $loan = null;
        $deposit = null;
        $realEstate = null;
        if ($this->item->getStock() !== null) {
            $stock = (new MarketInstrumentFormatter($this->item->getStock()))->format();
        };

        if ($this->item->getLoan() !== null) {
            $loan = (new LoanFormatter($this->item->getLoan()))->format();
        }

        if ($this->item->getDeposit() !== null) {
            $deposit = (new DepositFormatter($this->item->getDeposit()))->format();
        }

        if ($this->item->getRealEstate() !== null) {
            $realEstate = (new RealEstateFormatter($this->item->getRealEstate()))->format();
        }

        return [
            'itemId' => $this->item->getItemId(),
            'externalId' => $this->item->getExternalId(),
            'type' => $this->item->getType(),
            'name' => $this->item->getName(),
            'logo' => $this->item->getLogo(),
            'isOutdated' => $this->item->isOutdated(),
            'stock' => $stock,
            'loan' => $loan,
            'deposit' => $deposit,
            'realEstate' => $realEstate
        ];
    }
}
