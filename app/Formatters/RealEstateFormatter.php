<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\RealEstate\Models\RealEstateModel;

class RealEstateFormatter implements FormatterInterface
{
    private RealEstateModel $realEstate;

    public function __construct(RealEstateModel $realEstate)
    {
        $this->realEstate = $realEstate;
    }

    public function format(): array
    {
        $prices = [];
        if ($this->realEstate->getPrices() !== null) {
            foreach ($this->realEstate->getPrices()->getIterator() as $price) {
                $prices[] = (new RealEstatePriceFormatter($price))->format();
            }
        }

        return [
            'realEstateId' => $this->realEstate->getRealEstateId(),
            'isActive' => (bool)$this->realEstate->getIsActive(),
            'itemId' => $this->realEstate->getItemId(),
            'payoutFrequency' => $this->realEstate->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $this->realEstate->getPayoutFrequencyPeriod(),
            'interestAmount' => $this->realEstate->getInterestAmount(),
            'interestCurrency' => $this->realEstate->getInterestCurrency(),
            'currency' => $this->realEstate->getCurrency(),
            'prices' => $prices,
            'name' => $this->realEstate->getName(),
            'purchaseDate' => $this->realEstate->getPurchaseDate(),
            'purchaseAmount' => $this->realEstate->getPurchaseAmount(),
            'saleOperation' => $this->realEstate->getSaleOperation() !== null
                ? [
                    'price' => (new PriceFormatter($this->realEstate->getSaleOperation()->getPrice()))->format(),
                    'date' => $this->realEstate->getSaleOperation()->getDate()
                ]
                : null
        ];
    }
}
