<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Models\UserCredentialModel;

class UserCredentialFormatter implements FormatterInterface
{
    private UserCredentialModel $userCredential;

    public function __construct(UserCredentialModel $userCredential)
    {
        $this->userCredential = $userCredential;
    }
    public function format(): array
    {
        return [
            'userCredentialId' => $this->userCredential->getUserCredentialId(),
            'brokerId' => $this->userCredential->getBrokerId(),
            'brokerName' => $this->userCredential->getBroker()->getName(),
            'apiKey' => $this->userCredential->getApiKey(),
            'isActive' => $this->userCredential->getIsActive(),
            'activeAccountNumber' => $this->userCredential->getBrokerAccounts()->filterActiveAccounts()->count(),
            'accountNumber' => $this->userCredential->getBrokerAccounts()->count()
        ];
    }
}
