<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Models\MarketInstrumentModel;

readonly class MarketInstrumentFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentModel $marketInstrument
    ) {
    }

    public function format(): array
    {
        return [
            'marketInstrumentId' => $this->marketInstrument->getMarketInstrumentId(),
            'type' => $this->marketInstrument->getType(),
            'ticker' => $this->marketInstrument->getTicker(),
            'isin' => $this->marketInstrument->getIsin(),
            'currency' => $this->marketInstrument->getCurrency(),
            'name' => $this->marketInstrument->getName(),
        ];
    }
}
