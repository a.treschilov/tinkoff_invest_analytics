<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Models\MarketInstrumentModel;

readonly class MarketBondListFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentCollection $collection
    ) {
    }

    public function format(): array
    {
        $result = [];
        /** @var MarketInstrumentModel $bond */
        foreach ($this->collection->getIterator() as $bond) {
            $result[] = (new MarketBondFormatter($bond))->format();
        }

        return $result;
    }
}
