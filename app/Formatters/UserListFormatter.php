<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Collections\UserModelCollection;
use App\User\Models\UserModel;

class UserListFormatter implements FormatterInterface
{
    private UserModelCollection $userCollection;

    public function __construct(UserModelCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }
    public function format(): array
    {
        $result = [];
        /** @var UserModel $user */
        foreach ($this->userCollection->getIterator() as $user) {
            $result[] = (new UserFormatter($user))->format();
        }

        return $result;
    }
}
