<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Loan\Models\LoanPortfolioModel;

class LoanPortfolioItemFormatter
{
    private LoanPortfolioModel $loan;

    public function __construct(LoanPortfolioModel $loan)
    {
        $this->loan = $loan;
    }

    public function format(): array
    {
        return [
            'amount' => $this->loan->getAmount(),
            'currency' => $this->loan->getCurrency(),
            'overPayment' => [
                'price' => (new PriceFormatter($this->loan->getOverpayment()->getPrice()))->format(),
                'percent' => $this->loan->getOverpayment()->getPercent()
            ],
            'loan' => (new LoanFormatter($this->loan->getLoan()))->format()
        ];
    }
}
