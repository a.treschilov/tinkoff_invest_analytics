<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Deposit\Models\DepositModel;
use App\Interfaces\FormatterInterface;

class DepositFormatter implements FormatterInterface
{
    private DepositModel $deposit;

    public function __construct(DepositModel $deposit)
    {
        $this->deposit = $deposit;
    }

    public function format(): array
    {
        return [
            'depositId' => $this->deposit->getDepositId(),
            'itemId' => $this->deposit->getItemId(),
            'name' => $this->deposit->getName(),
            'isActive' => $this->deposit->getIsActive(),
            'dealDate' => $this->deposit->getDealDate(),
            'closedDate' => $this->deposit->getClosedDate(),
            'interestPercent' => $this->deposit->getInterestPercent(),
            'duration' => $this->deposit->getDuration(),
            'durationPeriod' => $this->deposit->getDurationPeriod(),
            'payoutFrequency' => $this->deposit->getPayoutFrequency(),
            'payoutFrequencyPeriod' => $this->deposit->getPayoutFrequencyPeriod(),
            'amount' => $this->deposit->getAmount(),
            'currency' => $this->deposit->getCurrency()
        ];
    }
}
