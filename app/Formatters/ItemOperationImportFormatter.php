<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Models\ImportStatisticModel;

class ItemOperationImportFormatter implements FormatterInterface
{
    private ImportStatisticModel $statistic;

    public function __construct(ImportStatisticModel $statistic)
    {
        $this->statistic = $statistic;
    }

    public function format(): array
    {
        return [
            'added' => $this->statistic->getAdded(),
            'updated' => $this->statistic->getUpdated(),
            'skipped' => $this->statistic->getSkipped()
        ];
    }
}
