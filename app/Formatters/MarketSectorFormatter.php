<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Entities\MarketSectorEntity;
use App\Interfaces\FormatterInterface;
use App\Intl\Collections\MarketSectorCollection;

class MarketSectorFormatter implements FormatterInterface
{
    private MarketSectorCollection $collection;

    public function __construct(MarketSectorCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];

        /** @var \App\Entities\MarketSectorEntity $country */
        foreach ($this->collection as $country) {
            $formattedData[] = [
                'id' => $country->getId(),
                'name' => $country->getName()
            ];
        }

        return $formattedData;
    }
}
