<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Broker\Models\BrokerCandleModel;
use App\Collections\CandleCollection;
use App\Interfaces\FormatterInterface;

readonly class CandleListFormatter implements FormatterInterface
{
    public function __construct(
        private CandleCollection $collection
    ) {
    }
    public function format(): array
    {
        $result = [];
        /** @var BrokerCandleModel $candle */
        foreach ($this->collection as $candle) {
            $result[] = (new CandleFormatter($candle))->format();
        }
        return $result;
    }
}
