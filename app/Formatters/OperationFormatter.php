<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\OperationModel;

class OperationFormatter implements FormatterInterface
{
    private OperationModel $operation;

    public function __construct(OperationModel $operation)
    {
        $this->operation = $operation;
    }

    public function format(): array
    {
        return [
            'id' => $this->operation->getItemOperationId(),
            'externalId' => $this->operation->getExternalId(),
            'type' => $this->operation->getOperationType(),
            'itemId' => $this->operation->getItemId(),
            'itemName' => $this->operation->getItem()?->getName(),
            'itemType' => $this->operation->getItem()?->getType(),
            'itemLogo' => $this->operation->getItem()?->getLogo(),
            'itemExternalId' => $this->operation->getItem()?->getExternalId(),
            'broker' => $this->operation->getBrokerId(),
            'amount' => $this->operation->getAmount(),
            'quantity' => $this->operation->getQuantity(),
            'currency' => $this->operation->getCurrencyIso(),
            'date' => $this->operation->getDate(),
            'status' => $this->operation->getStatus()
        ];
    }
}
