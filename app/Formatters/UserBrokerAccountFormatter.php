<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Collections\UserBrokerAccountCollection;
use App\User\Entities\UserBrokerAccountEntity;

class UserBrokerAccountFormatter implements FormatterInterface
{
    private UserBrokerAccountCollection $collection;

    public function __construct(UserBrokerAccountCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $result = [];
        /** @var UserBrokerAccountEntity $account */
        foreach ($this->collection->getIterator() as $account) {
            $result[] = [
                'id' => $account->getId(),
                'name' => $account->getName(),
                'externalId' => $account->getExternalId(),
                'isActive' => $account->getIsActive(),
                'type' => $account->getType()
            ];
        }

        return $result;
    }
}
