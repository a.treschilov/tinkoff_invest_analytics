<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Assets\Models\ImportStatisticModel as ImportStatisticModel;
use App\Interfaces\FormatterInterface;

class AssetImportFormatter implements FormatterInterface
{
    private ImportStatisticModel $model;

    public function __construct(ImportStatisticModel $model)
    {
        $this->model = $model;
    }

    public function format(): array
    {
        return [
            'added' => $this->model->getAdded(),
            'updated' => $this->model->getUpdated(),
            'skipped' => $this->model->getSkipped()
        ];
    }
}
