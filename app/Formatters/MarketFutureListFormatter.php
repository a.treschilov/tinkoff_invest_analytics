<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Models\MarketInstrumentModel;

readonly class MarketFutureListFormatter implements FormatterInterface
{
    public function __construct(
        private MarketInstrumentCollection $collection
    ) {
    }

    public function format(): array
    {
        $result = [];
        /** @var MarketInstrumentModel $future */
        foreach ($this->collection->getIterator() as $future) {
            $result[] = (new MarketFutureFormatter($future))->format();
        }

        return $result;
    }
}
