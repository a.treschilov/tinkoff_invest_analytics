<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\PortfolioItemCollection;

class PortfolioItemsFormatter implements FormatterInterface
{
    private PortfolioItemCollection $portfolioItemCollection;

    public function __construct(PortfolioItemCollection $portfolioItemCollection)
    {
        $this->portfolioItemCollection = $portfolioItemCollection;
    }

    public function format(): array
    {
        $formattedData = [];
        foreach ($this->portfolioItemCollection->getIterator() as $item) {
            $formattedData[] = (new PortfolioItemFormatter($item))->format();
        }

        return $formattedData;
    }
}
