<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Models\PriceModel;

class ItemTypeRatioFormatter implements FormatterInterface
{
    /**
     * @param array{
     *     'market': array{'amount': PriceModel, ration: float},
     *     'real_estate': array{'amount': PriceModel, ration: float},
     *     'deposit': array{'amount': PriceModel, ration: float},
     *     'loan': array{'amount': PriceModel, ration: float},
     *     'crowdfunding': array{'amount': PriceModel, ration: float},
     *     } $itemTypeRatio
     */
    public function __construct(private array $itemTypeRatio)
    {
    }
    public function format(): array
    {
        return [
            'market' => [
                'amount' => (new PriceFormatter($this->itemTypeRatio['market']['amount']))->format(),
                'ratio' => $this->itemTypeRatio['market']['ratio']
            ],
            'real_estate' => [
                'amount' => (new PriceFormatter($this->itemTypeRatio['real_estate']['amount']))->format(),
                'ratio' => $this->itemTypeRatio['real_estate']['ratio']
            ],
            'deposit' => [
                'amount' => (new PriceFormatter($this->itemTypeRatio['deposit']['amount']))->format(),
                'ratio' => $this->itemTypeRatio['deposit']['ratio']
            ],
            'loan' => [
                'amount' => (new PriceFormatter($this->itemTypeRatio['loan']['amount']))->format(),
                'ratio' => $this->itemTypeRatio['loan']['ratio']
            ],
            'crowdfunding' => [
                'amount' => (new PriceFormatter($this->itemTypeRatio['crowdfunding']['amount']))->format(),
                'ratio' => $this->itemTypeRatio['crowdfunding']['ratio']
            ],
        ];
    }
}
