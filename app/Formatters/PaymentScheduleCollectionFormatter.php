<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\PaymentScheduleCollection;
use App\Item\Models\PaymentScheduleModel;

class PaymentScheduleCollectionFormatter implements FormatterInterface
{
    public function __construct(
        private readonly PaymentScheduleCollection $scheduleCollection
    ) {
    }

    public function format(): array
    {
        $result = [];
        /** @var PaymentScheduleModel $item */
        foreach ($this->scheduleCollection->getIterator() as $item) {
            $result[] = (new PaymentScheduleFormatter($item))->format();
        }

        return $result;
    }
}
