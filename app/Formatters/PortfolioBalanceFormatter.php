<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Collections\PortfolioBalanceCollection;
use App\Item\Models\PortfolioBalanceModel;

class PortfolioBalanceFormatter implements FormatterInterface
{
    public function __construct(
        private readonly PortfolioBalanceCollection $balanceCollection
    ) {
    }

    /**
     * @return array{
     *     brokerId: string|null,
     *     amount: float,
     *     currency: string
     *     }
     * @throws \Exception
     */
    public function format(): array
    {
        $result = [];
        /** @var PortfolioBalanceModel $balance */
        foreach ($this->balanceCollection->getIterator() as $balance) {
            $result[] = [
                'brokerId' => $balance->getBrokerId(),
                'amount' => $balance->getAmount(),
                'currency' => $balance->getCurrencyIso()
            ];
        }

        return $result;
    }
}
