<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Deposit\Models\DepositPortfolioItemModel;
use App\Interfaces\FormatterInterface;

class DepositPortfolioItemFormatter implements FormatterInterface
{
    private DepositPortfolioItemModel $depositPortfolioItem;

    public function __construct(DepositPortfolioItemModel $depositPortfolioItem)
    {
        $this->depositPortfolioItem = $depositPortfolioItem;
    }

    public function format(): array
    {
        return [
            'amount' => $this->depositPortfolioItem->getAmount(),
            'income' => $this->depositPortfolioItem->getIncome(),
            'inputAmount' => $this->depositPortfolioItem->getInputAmount(),
            'currency' => $this->depositPortfolioItem->getCurrency(),
            'deposit' => (new DepositFormatter($this->depositPortfolioItem->getDeposit()))->format(),
            'operations' => (new OperationListFormatter($this->depositPortfolioItem->getOperations()))->format()
        ];
    }
}
