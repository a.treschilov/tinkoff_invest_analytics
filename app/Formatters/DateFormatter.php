<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;

class DateFormatter implements FormatterInterface
{
    public function __construct(
        private readonly \DateTime $date
    ) {
    }

    public function format(): array
    {
        return [
            'date' => $this->date->format('Y-m-d H:i:s'),
            'timezone' => $this->date->getTimezone()->getName()
        ];
    }
}
