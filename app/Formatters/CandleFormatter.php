<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Broker\Models\BrokerCandleModel;
use App\Interfaces\FormatterInterface;

readonly class CandleFormatter implements FormatterInterface
{
    public function __construct(
        private BrokerCandleModel $candle
    ) {
    }
    public function format(): array
    {
        return [
            'open' => $this->candle->getOpen(),
            'close' => $this->candle->getClose(),
            'date' => (new DateFormatter($this->candle->getTime()))->format(),
            'currency' => $this->candle->getCurrency(),
            'volume' => $this->candle->getVolume(),
        ];
    }
}
