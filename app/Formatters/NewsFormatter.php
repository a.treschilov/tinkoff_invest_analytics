<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\News\Models\NewsModel;

class NewsFormatter implements FormatterInterface
{
    public function __construct(
        private readonly NewsModel $news
    ) {
    }
    public function format(): array
    {
        return [
            'newsId' => $this->news->getNewsId(),
            'title' => $this->news->getTitle(),
            'description' => $this->news->getDescription(),
            'date' => $this->news->getDate(),
            'imageUrl' => $this->news->getImageUrl(),
            'link' => $this->news->getLink(),
            'symbols' => $this->news->getSymbols(),
            'isActive' => $this->news->isActive()
        ];
    }
}
