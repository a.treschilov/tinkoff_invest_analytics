<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\User\Models\UserModel;

class UserFormatter implements FormatterInterface
{
    private UserModel $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }
    public function format(): array
    {
        return [
            'userId' => $this->user->getUserId(),
            'login' => $this->user->getLogin(),
            'email' => $this->user->getEmail(),
            'name' => $this->user->getName(),
            'birthday' => $this->user->getBirthday(),
            'language' => $this->user->getLanguage(),
            'isNewUser' => $this->user->isNewUser(),
            'telegramId' => $this->user->getTelegramId(),
        ];
    }
}
