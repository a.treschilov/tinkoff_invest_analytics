<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Broker\Models\BrokerReportUploadModel;
use App\Interfaces\FormatterInterface;

readonly class BrokerReportUploadFormatter implements FormatterInterface
{
    public function __construct(
        private BrokerReportUploadModel $report
    ) {
    }

    #[\Override] public function format(): array
    {
        return [
            'brokerReportUploadId' => $this->report->getBrokerUploadReportId(),
            'date' => (new DateFormatter($this->report->getDate()))->format(),
            'brokerId' => $this->report->getBrokerId(),
            'broker' => (new BrokerFormatter($this->report->getBroker()))->format(),
            'added' => $this->report->getAdded(),
            'updated' => $this->report->getUpdated(),
            'skipped' => $this->report->getSkipped(),
            'status' => $this->report->getStatus()->value
        ];
    }
}
