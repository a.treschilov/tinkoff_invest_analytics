<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Assets\Entities\AssetsEntity;
use App\Interfaces\FormatterInterface;
use App\Item\Models\ItemModel;

class AssetItemFormatter implements FormatterInterface
{
    private ItemModel $item;

    public function __construct(ItemModel $item)
    {
        $this->item = $item;
    }

    public function format(): array
    {
        return match ($this->item->getType()) {
            default => (new AssetCrowdfundingItemFormatter($this->item))->format()
        };
    }
}
