<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Market\Collections\MarketInstrumentCollection;
use App\Interfaces\FormatterInterface;
use App\Market\Models\MarketInstrumentModel;

class MarketStockListFormatter implements FormatterInterface
{
    private MarketInstrumentCollection $collection;

    public function __construct(MarketInstrumentCollection $collection)
    {
        $this->collection = $collection;
    }

    public function format(): array
    {
        $formattedData = [];
        /** @var MarketInstrumentModel $instrument */
        foreach ($this->collection->getIterator() as $instrument) {
            $formattedData[] = (new MarketStockFormatter($instrument))->format();
        }

        return $formattedData;
    }
}
