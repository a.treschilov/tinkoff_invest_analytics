<?php

declare(strict_types=1);

namespace App\Formatters;

use App\Interfaces\FormatterInterface;
use App\Item\Models\PaymentScheduleModel;
use App\Models\PriceModel;

class PaymentScheduleFormatter implements FormatterInterface
{
    public function __construct(
        private readonly PaymentScheduleModel $paymentSchedule
    ) {
    }

    public function format(): array
    {
        return [
            'itemId' => $this->paymentSchedule->getItemId(),
            'item' => $this->paymentSchedule->getItem()
                ? (new ItemFormatter($this->paymentSchedule->getItem()))->format()
                : null,
            'interest' => (new PriceFormatter(
                new PriceModel([
                    'amount' => $this->paymentSchedule->getInterestAmount(),
                    'currency' => $this->paymentSchedule->getCurrency()
                ])
            ))->format(),
            'debt' => (new PriceFormatter(
                new PriceModel([
                    'amount' => $this->paymentSchedule->getDebtAmount(),
                    'currency' => $this->paymentSchedule->getCurrency()
                ])
            ))->format(),
            'date' => (new DateFormatter($this->paymentSchedule->getDate()))->format(),
        ];
    }
}
