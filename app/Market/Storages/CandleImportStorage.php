<?php

declare(strict_types=1);

namespace App\Market\Storages;

use App\Market\Entities\CandlesImportEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;

class CandleImportStorage
{
    public function __construct(
        private readonly EntityManager $entityManager
    ) {
    }

    /**
     * @param array $instrumentIds
     * @return array{
     *     date: string,
     *     marketInstrumentId: int
     * }
     */
    public function getLastImportDate(array $instrumentIds): array
    {
        $query = 'SELECT MAX(ci.importDateEnd) date, ci.marketInstrumentId 
            FROM App\Market\Entities\CandlesImportEntity ci
            GROUP BY ci.marketInstrumentId
            ';
        return $this->entityManager->createQuery($query)
            ->getResult();
    }

    public function findInstrumentImportsByDate(int $marketInstrumentId, \DateTime $date)
    {
        $importDateFrom = clone $date;
        $importDateFrom->setTime(0, 0, 0);

        $importDateTo = clone $date;
        $importDateTo->setTime(23, 59, 59);

        $criteria = new Criteria();
        $criteria->Where(Criteria::expr()->lt('importDateStart', $importDateFrom))
            ->andWhere(Criteria::expr()->gt('importDateEnd', $importDateFrom))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->setMaxResults(1);
        return $this->entityManager->getRepository(CandlesImportEntity::class)
            ->matching($criteria)->get(0);
    }

    public function addEntity(CandlesImportEntity $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}
