<?php

declare(strict_types=1);

namespace App\Market\Storages;

use App\Market\Entities\CandleEntity;
use App\Market\Types\CandleIntervalType;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Order;
use Doctrine\ORM\EntityManagerInterface;

class CandleStorage
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $marketInstrumentId
     * @param string $candleInterval
     * @param \DateTime $dateTime
     * @param \DateTime $createdDate
     * @return CandleEntity|null
     * @throws \Exception
     */
    public function findOneByInstrumentIdAndIntervalAndAfterDate(
        int $marketInstrumentId,
        string $candleInterval,
        \DateTime $dateTime,
        \DateTime $createdDate
    ): object|null {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('createdDate', $createdDate))
            ->andWhere(Criteria::expr()->gte('time', $dateTime))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->orderBy(['createdDate' => Order::Descending])
            ->setMaxResults(1);
        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->get(0);
    }

    /**
     * @param int $marketInstrumentId
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return CandleEntity[]
     */
    public function findDayCandleByDateInterval(
        int $marketInstrumentId,
        \DateTime $startDate,
        \DateTime $endDate
    ): array {
        $from = clone $startDate;
        $startDate->setTime(0, 0, 0);
        $to = clone $endDate;
        $endDate->setTime(23, 59, 59);

        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', CandleIntervalType::DAY_1->value))
            ->andWhere(Criteria::expr()->eq('isFinal', 1))
            ->andWhere(Criteria::expr()->gte('time', $from))
            ->andWhere(Criteria::expr()->lte('time', $to))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->orderBy(['time' => Order::Ascending]);
        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->toArray();
    }

    public function addEntity(CandleEntity $candleEntity): void
    {
        $this->entityManager->persist($candleEntity);
        $this->entityManager->flush();
    }

    /**
     * @param CandleEntity[] $candles
     * @return void
     */
    public function addEntityArray(array $candles): void
    {
        foreach ($candles as $candle) {
            $this->entityManager->persist($candle);
        }

        $this->entityManager->flush();
    }

    public function findOneFinalByInstrumentIdAndDate(
        int $marketInstrumentId,
        string $candleInterval,
        \DateTime $date
    ): ?CandleEntity {
        $startDate = clone $date;
        $startDate->setTime(0, 0, 0);
        $endDate = clone $date;
        $endDate->setTime(23, 59, 59);

        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isFinal', 1))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval))
            ->andWhere(Criteria::expr()->gte('time', $startDate))
            ->andWhere(Criteria::expr()->lte('time', $endDate))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->setMaxResults(1);

        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->get(0);
    }

    /**
     * @param int $marketInstrumentId
     * @param CandleIntervalType $candleInterval
     * @param \DateTime $time
     * @param int $isFinal
     * @return CandleEntity[]
     */
    public function findCandles(
        int $marketInstrumentId,
        CandleIntervalType $candleInterval,
        \DateTime $time,
        int $isFinal
    ): array {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isFinal', $isFinal))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('candleInterval', $candleInterval->value))
            ->andWhere(Criteria::expr()->eq('time', $time))
            ->andWhere(Criteria::expr()->eq('isActual', 1));

        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->toArray();
    }

    public function getLastCandle(int $marketInstrumentId): ?CandleEntity
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('isFinal', 1))
            ->andWhere(Criteria::expr()->eq('marketInstrumentId', $marketInstrumentId))
            ->andWhere(Criteria::expr()->eq('isActual', 1))
            ->orderBy(['time' => Order::Descending])
            ->setMaxResults(1);

        return $this->entityManager->getRepository(CandleEntity::class)
            ->matching($criteria)->get(0);
    }
}
