<?php

declare(strict_types=1);

namespace App\Market\Storages;

use App\Market\Entities\MarketInstrumentEntity;
use Doctrine\ORM\EntityManagerInterface;

class MarketInstrumentStorage
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function addEntity(MarketInstrumentEntity $entity): int
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param string $isin
     * @return MarketInstrumentEntity[]
     */
    public function findByIsin(string $isin): array
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findBy(['isin' => $isin]);
    }

    /**
     * @param array $isinList
     * @return MarketInstrumentEntity[]
     */
    public function findByIsinList(array $isinList): array
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findBy(['isin' => $isinList]);
    }

    public function findOneByIsin(string $isin): ?MarketInstrumentEntity
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findOneBy(['isin' => $isin]);
    }

    /**
     * @param string|null $currency
     * @return MarketInstrumentEntity[]
     */
    public function findStocks(?string $currency = null): array
    {
        $criteria['type'] = 'Stock';
        if (null !== $currency) {
            $criteria['currency'] = $currency;
        }

        return $this->entityManager->getRepository(MarketInstrumentEntity::class)->findBy($criteria);
    }

    /**
     * @return MarketInstrumentEntity[]
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)->findAll();
    }

    public function findById(int $id): ?MarketInstrumentEntity
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param 'Bond'|'Stock'|'Etf'|'Currency'|'Future' $type
     * @return MarketInstrumentEntity[]
     */
    public function findByType(string $type): array
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findBy(['type' => $type]);
    }

    public function findByRegNumber(string $regNumber): ?MarketInstrumentEntity
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findOneBy(['regNumber' => $regNumber]);
    }

    public function findByTicker(string $ticker): ?MarketInstrumentEntity
    {
        return $this->entityManager->getRepository(MarketInstrumentEntity::class)
            ->findOneBy(['ticker' => $ticker]);
    }
}
