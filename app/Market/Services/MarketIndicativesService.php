<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Models\BrokerInstrumentModel;
use App\Services\FillMarketInstrumentServiceDecorator;

readonly class MarketIndicativesService
{
    public function __construct(
        private readonly FillMarketInstrumentServiceDecorator $fillMarketInstrumentService,
    ) {
    }
    public function import(): array
    {
        $result = 0;
        $indicatives = $this->fillMarketInstrumentService->getIndicatives();

        /** @var BrokerInstrumentModel $indicative */
        foreach ($indicatives->getIterator() as $indicative) {
            $isin = $indicative->getTicker() . '.' . $indicative->getExchange();
            $this->fillMarketInstrumentService->getInstrumentByIsin($isin);
            $result++;
        }

        return [
            'imported' => $result
        ];
    }
}
