<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Common\Amqp\AmqpClient;
use App\Exceptions\MarketDataException;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Types\InstrumentType;
use App\Models\ImportStatisticModel;

readonly class MarketInstrumentImportService
{
    public function __construct(
        private AmqpClient $amqpClient,
        private MarketInstrumentDbService $instrumentDbService,
        private MarketInstrumentBrokerService $instrumentBrokerService,
        private MarketInstrumentDbServiceStoreDecorator $instrumentDbStoreService
    ) {
    }

    public function import(InstrumentType $instrumentType): ImportStatisticModel
    {
        $statistic = [
            'countImportedItems' => 0,
            'countToImportItems' => 0
        ];
        $messages = [];

        $brokerInstruments = $this->instrumentBrokerService->getByType($instrumentType);
        $dbInstruments = $this->instrumentDbService->getByType($instrumentType);

        /** @var MarketInstrumentModel $instrument */
        foreach ($brokerInstruments as $instrument) {
            switch ($instrumentType) {
                case InstrumentType::BOND:
                    $type = 'bondImport';
                    break;
                case InstrumentType::STOCK:
                    $type = 'stockImport';
                    break;
                case InstrumentType::FUTURE:
                    $type = 'futureImport';
                    break;
                default:
                    throw new MarketDataException(
                        'Instrument type ' . InstrumentType::BOND->value . ' is not supported for import'
                    );
            }

            $dbInstrument = $dbInstruments->getByIsin($instrument->getIsin());
            if ($dbInstrument === null || $this->instrumentDbService->isNeedUpdate($instrument->getIsin())) {
                $messages[] = [
                    'type' => $type,
                    'data' => [
                        'isin' => $instrument->getIsin(),
                    ]
                ];
                $statistic['countToImportItems']++;
            } else {
                $statistic['countImportedItems']++;
            }
        }

        /** @var MarketInstrumentModel $bond */
        foreach ($dbInstruments->getIterator() as $instrument) {
            switch ($instrumentType) {
                case InstrumentType::BOND:
                    $type = 'bondImport';
                    break;
                case InstrumentType::STOCK:
                    $type = 'stockImport';
                    break;
                case InstrumentType::FUTURE:
                    $type = 'futureImport';
                    break;
                default:
                    throw new MarketDataException(
                        'Instrument type ' . $instrumentType->value . ' is not supported for import'
                    );
            }

            if ($brokerInstruments->getByIsin($instrument->getIsin()) === null) {
                if ($this->instrumentDbService->isNeedUpdate($instrument->getIsin())) {
                    $messages[] = [
                        'type' => $type,
                        'data' => [
                            'isin' => $instrument->getIsin()
                        ]
                    ];
                    $statistic['countToImportItems']++;
                } else {
                    $statistic['countImportedItems']++;
                }
            }
        }

        foreach ($messages as $message) {
            $this->amqpClient->publishMessage($message);
        }

        return new ImportStatisticModel($statistic);
    }

    public function createOrUpdate(string $isin): int
    {
        $instrument = $this->instrumentDbStoreService->getByIsin($isin);
        $instrument = $this->instrumentDbService->updateData($instrument->getIsin());
        return $instrument->getItemId();
    }
}
