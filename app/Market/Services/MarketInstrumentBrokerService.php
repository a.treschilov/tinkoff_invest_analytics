<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerInstrumentModel;
use App\Exceptions\MarketDataException;
use App\Exceptions\UnsupportedInstrumentException;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Interfaces\MarketBondServiceInterface;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Types\InstrumentType;

readonly class MarketInstrumentBrokerService implements MarketInstrumentServiceInterface
{
    public function __construct(
        private SourceBrokerInterface $broker,
        private MarketBondServiceInterface $marketBondService,
        private StockService $stockService,
        private FutureService $futureService
    ) {
    }

    public function getByType(InstrumentType $instrumentType): MarketInstrumentCollection
    {
        switch ($instrumentType) {
            case InstrumentType::BOND:
                $collection = $this->marketBondService->convertBondToInstrumentCollection($this->broker->getBonds());
                break;
            case InstrumentType::STOCK:
                $collection = $this->stockService->convertBrokerToInstrumentCollection($this->broker->getShares());
                break;
            case InstrumentType::FUTURE:
                $collection = $this->futureService->convertBrokerToInstrumentCollection($this->broker->getFutures());
                break;

            default:
                throw new MarketDataException('Instrument type not supported');
        }
        return $collection;
    }

    public function getByIsin(string $isin): ?MarketInstrumentModel
    {
        $instrumentCollection = $this->broker->getInstrumentByIsin($isin);

        if ($instrumentCollection->count() === 0) {
            throw new UnsupportedInstrumentException('Unknown instrument', 1011);
        }

        /** @var BrokerInstrumentModel $brokerInstrument */
        $brokerInstrument = $instrumentCollection->mergeSame();

        $sku = $brokerInstrument->getIsin()
            ?: $brokerInstrument->getTicker();
        $instrument = new MarketInstrumentModel([
            'ticker' => $brokerInstrument->getTicker(),
            'logo' => $brokerInstrument->getLogo(),
            'isin' => $sku,
            'minPriceIncrement' => $brokerInstrument->getMinPriceIncrement(),
            'lot' => $brokerInstrument->getLot(),
            'currency' => $brokerInstrument->getCurrency(),
            'name' => $brokerInstrument->getName(),
            'type' => $brokerInstrument->getType(),
            'regNumber' => $brokerInstrument->getRegNumber(),
            'bond' => null,
            'currencyMode' => null,
            'future' => null
        ]);

        switch ($instrument->getType()) {
            case InstrumentType::BOND->value:
                $instrument->setBond($this->marketBondService->getBrokerBondByIsin($sku));
                break;
            case InstrumentType::FUTURE->value:
                $instrument->setFuture($this->futureService->getBrokerFutureByIsin($sku));
                break;
        }

        return $instrument;
    }

    public function getByIsinList(array $isinList): MarketInstrumentCollection
    {
        // TODO: Implement getInstrumentByIsinList() method.
    }
}
