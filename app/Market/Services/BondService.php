<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Collections\BondCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerBondCouponModel;
use App\Broker\Models\BrokerBondModel;
use App\Common\Amqp\AmqpClient;
use App\Item\Collections\PaymentScheduleCollection;
use App\Item\Models\PaymentScheduleModel;
use App\Item\Services\PaymentScheduleService;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketBondEntity;
use App\Market\Interfaces\MarketBondServiceInterface;
use App\Market\Interfaces\MarketConcreteInstrumentInterface;
use App\Market\Models\MarketBondModel;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Storages\MarketInstrumentStorage;
use App\Models\PriceModel;

class BondService implements MarketBondServiceInterface, MarketConcreteInstrumentInterface
{
    private const OUTDATED_INTERVAL = 23 * 60 * 60;

    public function __construct(
        private readonly \DateTime $currentDateTime,
        private readonly AmqpClient $amqpClient,
        private readonly PaymentScheduleService $paymentScheduleService,
        private readonly SourceBrokerInterface $sourceBroker,
        private readonly MarketInstrumentStorage $marketInstrumentStorage,
    ) {
    }

    private function convertModelToEntity(MarketBondModel $model): MarketBondEntity
    {
        $entity = new MarketBondEntity();
        if ($model->getMarketBondId() !== null) {
            $entity->setId($model->getMarketBondId());
        }
        $entity->setNominal($model->getNominal()->getAmount());
        $entity->setInitialNominal($model->getInitialNominal()->getAmount());
        $entity->setMaturityDate($model->getMaturityDate());
        $entity->setCurrencyIso($model->getNominal()->getCurrency());

        if ($model->getUpdatedDate() !== null) {
            $entity->setUpdatedDate($model->getUpdatedDate());
        }

        return $entity;
    }

    public function convertBondToInstrumentCollection(BondCollection $bonds): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();
        /** @var BrokerBondModel $bond */
        foreach ($bonds->getIterator() as $bond) {
            $instrument = new MarketInstrumentModel([
                'isin' => $bond->getIsin(),
                'bond' => $this->convertBrokerToMarketModel($bond)
            ]);
            $collection->add($instrument);
        }

        return $collection;
    }

    public function isNeedForUpdate(MarketInstrumentModel $instrument): bool
    {
        $bond = $instrument->getBond();
        if ($bond === null) {
            return true;
        }

        return
            $bond->getUpdatedDate()->getTimestamp() + self::OUTDATED_INTERVAL < $this->currentDateTime->getTimestamp()
        || $bond->getNominal()->getAmount() === 0.0;
    }

    public function getBrokerBondByIsin(string $isin): ?MarketBondModel
    {
        $bond = $this->sourceBroker->getBondByIsin($isin);

        if ($bond === null) {
            return null;
        }

        return $this->convertBrokerToMarketModel($bond);
    }

    public function updateData(int $marketInstrumentId): int
    {
        $instrumentId = $this->updateItemData($marketInstrumentId);
        $this->fillBondPaymentSchedule($marketInstrumentId);

        return $instrumentId;
    }

    private function updateItemData(int $marketInstrumentId): int
    {
        $instrument = $this->marketInstrumentStorage->findById($marketInstrumentId);

        $brokerBond = $this->sourceBroker->getBondByIsin($instrument->getIsin());

        if ($brokerBond !== null) {
            if ($instrument->getMarketBond() === null) {
                $bondModel = $this->convertBrokerToMarketModel($brokerBond);
                $bondEntity = $this->convertModelToEntity($bondModel);
                $bondEntity->setMarketInstrument($instrument);
                $instrument->setMarketBond($bondEntity);
            }

            $instrument->getMarketBond()->setNominal($brokerBond->getNominal()->getAmount());
            $instrument->getMarketBond()->setMaturityDate($brokerBond->getMaturityDate());

            $instrument->getMarketBond()->setUpdatedDate(new \DateTime());

            $this->marketInstrumentStorage->addEntity($instrument);
        }

        if ($instrument->getMarketBond()->getNominal() === 0.0) {
            $message = [
                'type' => 'outDateItem',
                'data' => [
                    'itemId' => $instrument->getItemId()
                ]
            ];
            $this->amqpClient->publishMessage($message);
        }
        return $instrument->getId();
    }

    private function fillBondPaymentSchedule(int $marketInstrumentId): void
    {
        $instrument = $this->marketInstrumentStorage->findById($marketInstrumentId);

        $from = new \DateTime('2000-01-01');
        $to = $instrument->getMarketBond()->getMaturityDate() ?? new \DateTime('2100-01-01');

        $coupons = $this->sourceBroker->getBondCouponsIsin($instrument->getIsin(), $from, $to);

        $collection = new PaymentScheduleCollection();
        /** @var BrokerBondCouponModel $coupon */
        foreach ($coupons as $coupon) {
            $collection->add($this->convertCouponToPaymentSchedule($coupon));
        }

        $this->paymentScheduleService->setPaymentSchedule($instrument->getItem()->getId(), $collection);
    }

    private function convertBrokerToMarketModel(BrokerBondModel $bond): MarketBondModel
    {
        return new MarketBondModel([
            'nominal' => new PriceModel([
                'amount' => $bond->getNominal()->getAmount(),
                'currency' => $bond->getNominal()->getCurrency()
            ]),
            'initialNominal' => new PriceModel([
                'amount' => $bond->getInitialNominal()->getAmount(),
                'currency' => $bond->getInitialNominal()->getCurrency()
            ]),
            'maturityDate' => $bond?->getMaturityDate()
        ]);
    }

    private function convertCouponToPaymentSchedule(BrokerBondCouponModel $coupon): PaymentScheduleModel
    {
        $instrument = $this->marketInstrumentStorage->findOneByIsin($coupon->getIsin());
        return new PaymentScheduleModel([
            'itemId' => $instrument->getItem()->getId(),
            'interestAmount' => $coupon->getAmount(),
            'debtAmount' => $coupon->getAmortization() === null ? 0 : $coupon->getAmortization()->getAmount(),
            'currency' => $coupon->getCurrency(),
            'date' => $coupon->getDate()
        ]);
    }
}
