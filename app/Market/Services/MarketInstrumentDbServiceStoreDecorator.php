<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Item\Entities\ItemEntity;
use App\Item\Storages\ItemStorage;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Types\InstrumentType;

readonly class MarketInstrumentDbServiceStoreDecorator implements MarketInstrumentServiceInterface
{
    public function __construct(
        private MarketInstrumentDbService $instrumentDbService,
        private MarketInstrumentBrokerService $instrumentBrokerService,
        private ItemStorage $itemStorage
    ) {
    }

    public function getByType(InstrumentType $instrumentType): MarketInstrumentCollection
    {
        // TODO: Implement getByType() method.
    }

    public function getByIsin(string $isin): ?MarketInstrumentModel
    {
        $instrument = $this->instrumentDbService->getByIsin($isin);

        if ($instrument === null) {
            $instrument = $this->instrumentBrokerService->getByIsin($isin);
            if ($instrument !== null) {
                $this->save($instrument);
            }
        }

        return $instrument;
    }

    public function getByIsinList(array $isinList): MarketInstrumentCollection
    {
        // TODO: Implement getInstrumentByIsinList() method.
    }

    private function save(MarketInstrumentModel $instrument): void
    {
        $item = new ItemEntity();
        $item->setName($instrument->getName());
        $item->setLogo($instrument->getLogo());
        $item->setType('market');
        $item->setSource('market');
        $item->setExternalId($instrument->getIsin());

        $instrumentEntity = new MarketInstrumentEntity();
        $instrumentEntity->setCurrency($instrument->getCurrency());
        $instrumentEntity->setIsin($instrument->getIsin());
        $instrumentEntity->setLot($instrument->getLot());
        $instrumentEntity->setMinPriceIncrement($instrument->getMinPriceIncrement());
        $instrumentEntity->setName($instrument->getName());
        $instrumentEntity->setTicker($instrument->getTicker());
        $instrumentEntity->setType($instrument->getType());
        $instrumentEntity->setRegNumber($instrument->getRegNumber());

        $instrumentEntity->setItem($item);
        $item->setMarketInstrument($instrumentEntity);

        $this->itemStorage->addEntity($item);
    }
}
