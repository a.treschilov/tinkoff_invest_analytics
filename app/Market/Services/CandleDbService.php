<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Collections\CandleCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Interfaces\CandleServiceInterface;
use App\Market\Storages\CandleStorage;
use App\Market\Storages\MarketInstrumentStorage;
use App\Market\Types\CandleIntervalType;

readonly class CandleDbService implements CandleServiceInterface
{
    private const CACHE_CANDLE_SECONDS = 2 * 60 * 60;

    public function __construct(
        private CandleStorage $candleStorage,
        private CandleHelperService $candleHelperService,
        private MarketInstrumentStorage $marketInstrumentStorage,
        private \DateTime $now
    ) {
    }

    #[\Override] public function getTodayCandle(string $isin): ?BrokerCandleModel
    {
        $tiCandle = null;
        $cacheDate = clone $this->now;
        $cacheDate->sub((new \DateInterval('PT' . self::CACHE_CANDLE_SECONDS . 'S')));

        $marketInstrument = $this->marketInstrumentStorage->findOneByIsin($isin);

        if ($marketInstrument === null) {
            return null;
        }

        $candle = $this->candleStorage->findOneByInstrumentIdAndIntervalAndAfterDate(
            $marketInstrument->getId(),
            CandleIntervalType::HOUR_1->value,
            $cacheDate,
            $cacheDate
        );

        if ($candle !== null) {
            $tiCandle = $this->candleHelperService->convertEntityToModel($candle);
        }

        return $tiCandle;
    }

    #[\Override] public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel {
        // TODO: Implement getDayCandleByMarketInstrument() method.
    }

    #[\Override] public function getDayCandleByIsin(string $isin, \DateTime $date): ?BrokerCandleModel
    {
        // TODO: Implement getDayCandleByIsin() method.
    }

    #[\Override] public function getLastPricesIsin(array $isinList): array
    {
        // TODO: Implement getLastPricesIsin() method.
    }

    #[\Override] public function getPricesIsin(array $isinList, \DateTime $date): array
    {
        // TODO: Implement getPricesIsin() method.
    }

    #[\Override] public function getFirstCandle(int $marketInstrumentId): ?BrokerCandleModel
    {
        // TODO: Implement getFirstCandle() method.
    }

    #[\Override] public function getDayCandleInterval(
        int $marketInstrumentId,
        \DateTime $from,
        \DateTime $to
    ): CandleCollection {
        $collection = new CandleCollection();

        $candles = $this->candleStorage->findDayCandleByDateInterval($marketInstrumentId, $from, $to);

        foreach ($candles as $candle) {
            $collection->add($this->candleHelperService->convertEntityToModel($candle));
        }

        return $collection;
    }

    /**
     * @inheritDoc
     */
    #[\Override] public function getInstrumentsIntervalDayCandles(
        array $marketInstrumentIds,
        \DateTime $dateFrom,
        \DateTime $dateTo
    ): array {
        $result = [];

        foreach ($marketInstrumentIds as $marketInstrumentId) {
            $instrumentCandles = new CandleCollection();
            $candles = $this->candleStorage->findDayCandleByDateInterval($marketInstrumentId, $dateFrom, $dateTo);
            foreach ($candles as $candle) {
                $instrumentCandles->add($this->candleHelperService->convertEntityToModel($candle));
            }
            $result[] = [
                'marketInstrumentId' => $marketInstrumentId,
                'candles' => $instrumentCandles->filterMainCandles(),
            ];
        }

        return $result;
    }
}
