<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Market\Entities\CandleEntity;

class CandleHelperService
{
    public function convertEntityToModel(CandleEntity $entity): BrokerCandleModel
    {
        return new BrokerCandleModel([
            'open' => $entity->getOpen(),
            'close' => $entity->getClose(),
            'high' => $entity->getHigh(),
            'low' => $entity->getLow(),
            'volume' => $entity->getVolume(),
            'time' => $entity->getTime(),
            'isComplete' => (bool)$entity->getIsFinal(),
            'currency' => $entity->getCurrency(),
            'interval' => $entity->getCandleInterval(),
            'marketInstrumentId' => $entity->getMarketInstrumentId(),
        ]);
    }
}
