<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerInstrumentModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\CandleCollection;
use App\Common\Amqp\AmqpClient;
use App\Market\Entities\MarketInstrumentEntity;
use App\Exceptions\AccountException;
use App\Exceptions\BrokerAdapterException;
use App\Exceptions\ManyRequestException;
use App\Market\Interfaces\CandleServiceInterface;
use App\Market\Storages\CandleImportStorage;
use App\Market\Types\CandleIntervalType;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;
use DateInterval;
use Psr\Log\LoggerInterface;

class CandleBrokerService implements CandleServiceInterface
{
    public function __construct(
        private readonly SourceBrokerInterface $sourceBroker,
        private readonly \DateTime $currentDateTime,
        private readonly CandleImportStorage $candleImportStorage,
        private readonly AmqpClient $amqpClient,
        private readonly MarketInstrumentHelperService $marketInstrumentHelperService,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws AccountException
     * @throws ManyRequestException
     */
    public function getTodayCandle(string $isin): ?BrokerCandleModel
    {
        $from = new \DateTime();
        $from->setTime(0, 0, 0);
        $to = new \DateTime();
        $to->setTime(23, 59, 59);

        try {
            $candleCollection = $this->sourceBroker->getHistoryCandles(
                $isin,
                $from,
                $to,
                CandleIntervalType::HOUR_1
            );
        } catch (BrokerAdapterException $exception) {
            $candleCollection = new CandleCollection();
        }

        return $candleCollection?->getLastFinalCandle();
    }

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel {
        $isin = $marketInstrumentEntity->getIsin();
        return $this->getDayCandleByIsin($isin, $date);
    }

    public function getDayCandleByIsin(string $isin, \DateTime $date): ?BrokerCandleModel
    {
        $from = clone $date;
        $from->sub(new \DateInterval("P15D"));
        $from->setTime(0, 0, 0);
        $to = clone $date;
        $to->setTime(23, 59, 59);

        try {
            $candleCollection = $this->sourceBroker->getHistoryCandles(
                $isin,
                $from,
                $to,
                CandleIntervalType::DAY_1
            );
        } catch (BrokerAdapterException $exception) {
            $candleCollection = new CandleCollection();
        }

        return $candleCollection?->getLastCandle();
    }

    /**
     * @param array $isinList
     * @return BrokerMoneyModel[]
     * @throws AccountException
     */
    public function getLastPricesIsin(array $isinList): array
    {
        return $this->sourceBroker->getLastPrices($isinList);
    }

    /**
     * @param array $isinList
     * @param \DateTime $date
     * @return BrokerMoneyModel[]
     * @throws AccountException
     */
    public function getPricesIsin(array $isinList, \DateTime $date): array
    {
        $result = [];

        if ($date->format('Y-m-d') === $this->currentDateTime->format('Y-m-d')) {
            return $this->getLastPricesIsin($isinList);
        } else {
            $needPrices = [];
            foreach ($isinList as $isin) {
                try {
                    $candle = $this->getDayCandleByIsin($isin, $date);
                } catch (TIException $e) {
                    $candle = null;
                }

                $amount = $candle?->getClose() ?? 0;
                if ($amount !== 0) {
                    $result[$isin] = new BrokerMoneyModel([
                        'amount' => $amount,
                        'currency' => $candle->getCurrency()
                    ]);
                } else {
                    $needPrices[] = $isin;
                }
            }

            $prices = $this->getLastPricesIsin($needPrices);
            $result = array_replace($result, $prices);
        }

        return $result;
    }

    public function importCandles(array $instrumentIds): array
    {
        $messages = [];

        $dates = [];
        $importDates = $this->candleImportStorage->getLastImportDate($instrumentIds);
        foreach ($importDates as $date) {
            $dates[$date['marketInstrumentId']] = $date['date'];
        }

        $lastImportDate = (new \DateTime());
        $lastImportDate->setTime(0, 0, 0);

        foreach ($instrumentIds as $instrumentId) {
            $dateStart = null;
            $dateEnd = null;
            if (isset($dates[$instrumentId])) {
                $dateStart = new \DateTime($dates[$instrumentId]);
            } else {
                $candle = $this->getFirstCandle($instrumentId);

                if ($candle !== null) {
                    $dateStart = $candle->getTime();
                }
            }

            if ($dateStart !== null) {
                $dateEnd = clone $dateStart;
                $dateEnd->add(new \DateInterval('P364D'));
                $dateEnd = min($dateEnd, clone $lastImportDate);

                $dateStart->setTime(0, 0, 0);
                $dateEnd->setTime(0, 0, 0);

                while ($dateStart < $lastImportDate) {
                    $message = [
                        'type' => 'candleImport',
                        'data' => [
                            'timeStart' => (clone $dateStart)->getTimestamp(),
                            'timeEnd' => (clone $dateEnd)->getTimeStamp(),
                            'marketInstrumentId' => $instrumentId
                        ]
                    ];
                    $this->amqpClient->publishMessage($message);

                    $messages[] = $message;

                    $dateStart = clone $dateEnd;
                    $dateEnd->add(new \DateInterval('P364D'));
                    $dateEnd = min($dateEnd, clone $lastImportDate);
                }
            }
        }

        return $messages;
    }

    public function getFirstCandle(int $marketInstrumentId): ?BrokerCandleModel
    {
        $isin = $this->marketInstrumentHelperService->getInstrumentById($marketInstrumentId)?->getIsin();

        if ($isin === null) {
            return null;
        }

        $instruments = $this->sourceBroker->getInstrumentByIsin($isin);
        $minDate = null;
        /** @var BrokerInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            if ($instrument->getFirstCandleDate() !== null) {
                $instrumentFirstCandle = clone $instrument->getFirstCandleDate();
            } else {
                $to = new \DateTime();
                $from = clone $to;
                $from->sub(new DateInterval('P9Y'));
                try {
                    $candles = $this->sourceBroker->getHistoryCandles(
                        $isin,
                        $from,
                        $to,
                        CandleIntervalType::MONTH_1
                    );
                } catch (TIException $e) {
                    if ($e->getCode() === 70001) {
                        $this->logger->warning('Get History candle of isin = ' . $isin . ' internal Tinkoff exception');
                        $candles = new CandleCollection();
                    } else {
                        throw new TIException($e->getMessage(), $e->getCode(), $e->getPrevious());
                    }
                }

                $firstCandle = $candles?->getFirstCandle();
                $instrumentFirstCandle = $firstCandle?->getTime();

                if ($instrumentFirstCandle !== null) {
                    $from = clone $instrumentFirstCandle;
                    $to = clone $instrumentFirstCandle;
                    $to->add(new DateInterval('P1M'));
                    $candles = $this->sourceBroker->getHistoryCandles(
                        $isin,
                        $from,
                        $to,
                        CandleIntervalType::DAY_1
                    );
                    $firstCandle = $candles->getFirstCandle();
                    $instrumentFirstCandle = $firstCandle?->getTime();
                }
            }

            if (
                $instrumentFirstCandle !== null
                && ($minDate === null || $minDate > $instrumentFirstCandle)
            ) {
                $minDate = clone $instrumentFirstCandle;
            }
        }

        if ($minDate === null) {
            return null;
        }

        return $this->getDayCandleByIsin($isin, $minDate);
    }

    public function getDayCandleInterval(
        int $marketInstrumentId,
        \DateTime $from,
        \DateTime $to
    ): CandleCollection {
        $collection = new CandleCollection();

        $isin = $this->marketInstrumentHelperService->getInstrumentById($marketInstrumentId)?->getIsin();

        if ($isin === null) {
            return $collection;
        }

        $candles = $this->sourceBroker->getHistoryCandles(
            $isin,
            $from,
            $to,
            CandleIntervalType::DAY_1
        );

        if ($candles !== null) {
            /** @var BrokerCandleModel $candle */
            foreach ($candles->getIterator() as $candle) {
                if ($candle->getIsComplete() === true) {
                    $collection->add($candle);
                }
            }
        }

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function getInstrumentsIntervalDayCandles(
        array $marketInstrumentIds,
        \DateTime $dateFrom,
        \DateTime $dateTo
    ): array {
        // TODO: Implement getInstrumentsIntervalDayCandles() method.
    }
}
