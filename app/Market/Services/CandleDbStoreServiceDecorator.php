<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Collections\CandleCollection;
use App\Market\Entities\CandleEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Exceptions\AccountException;
use App\Market\Interfaces\CandleServiceInterface;
use App\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Storages\CandleStorage;
use App\Market\Types\CandleIntervalType;
use ATreschilov\TinkoffInvestApiSdk\Exceptions\TIException;

class CandleDbStoreServiceDecorator implements CandleServiceInterface
{
    public function __construct(
        private readonly CandleServiceInterface $candleBrokerService,
        private readonly CandleServiceInterface $candleDbService,
        private readonly CandleStorage $candleStorage,
        private readonly MarketInstrumentServiceInterface $marketInstrumentService,
        private readonly \DateTime $now
    ) {
    }

    public function getTodayCandle(string $isin): ?BrokerCandleModel
    {
        $candle = $this->candleDbService->getTodayCandle($isin);

        if (null === $candle) {
            $candle = $this->candleBrokerService->getTodayCandle($isin);

            if (null !== $candle && (int)$candle->getIsComplete() === 1) {
                $marketInstrument = $this->marketInstrumentService->getInstrumentByIsin($isin);
                $this->saveCandle($candle, $marketInstrument->getId());
            }
        }

        return $candle;
    }

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel {
        $candle = $this->candleStorage
            ->findOneFinalByInstrumentIdAndDate(
                $marketInstrumentEntity->getId(),
                CandleIntervalType::DAY_1->value,
                $date
            );

        if (null !== $candle) {
            $candleModel = new BrokerCandleModel([
                'open' => $candle->getOpen(),
                'close' => $candle->getClose(),
                'high' => $candle->getHigh(),
                'low' => $candle->getLow(),
                'time' => $candle->getTime(),
                'volume' => $candle->getVolume(),
                'currency' => $candle->getCurrency()
            ]);
        } else {
            $candleModel = $this->candleBrokerService->getDayCandleByMarketInstrument($marketInstrumentEntity, $date);

            if ($candleModel !== null && (int)$candleModel->getIsComplete() === 1) {
                if ($date->getTimestamp() - $candleModel->getTime()->getTimestamp() < 24 * 60 * 60) {
                    $candle = new CandleEntity();
                    $candle->setMarketInstrumentId($marketInstrumentEntity->getId());
                    $candle->setFinal((int)$candleModel->getIsComplete());
                    $candle->setOpen($candleModel->getOpen());
                    $candle->setClose($candleModel->getClose());
                    $candle->setLow($candleModel->getLow());
                    $candle->setHigh($candleModel->getHigh());
                    $candle->setCurrency($candleModel->getCurrency());
                    $candle->setVolume($candleModel->getVolume());
                    $candle->setTime($candleModel->getTime());
                    $candle->setCandleInterval(CandleIntervalType::DAY_1->value);
                    $candle->setCreatedDate($this->now);

                    $this->candleStorage->addEntity($candle);
                }
            }
        }
        return $candleModel;
    }

    public function getDayCandleByIsin(string $isin, \DateTime $date): ?BrokerCandleModel
    {
        $marketInstrumentEntity = $this->marketInstrumentService->getInstrumentByIsin($isin);
        return $this->getDayCandleByMarketInstrument($marketInstrumentEntity, $date);
    }

    /**
     * @param array $isinList
     * @return BrokerCandleModel[]
     * @throws AccountException
     */
    public function getLastPricesIsin(array $isinList): array
    {
        $instruments = $this->marketInstrumentService->getInstrumentByIsinList($isinList);

        $needPrices = [];
        $prices = [];
        /** @var MarketInstrumentModel $instrument */
        foreach ($instruments->getIterator() as $instrument) {
            $candle = $this->candleStorage->getLastCandle($instrument->getMarketInstrumentId());
            if ($candle !== null) {
                $prices[$instrument->getIsin()] = new BrokerMoneyModel([
                    'amount' => $candle->getClose(),
                    'currency' => $candle->getCurrency()
                ]);
            } else {
                $needPrices[] = $instrument->getIsin();
            }
        }

        if (count($needPrices)) {
            $prices = array_merge($prices, $this->candleBrokerService->getLastPricesIsin($needPrices));
        }
        return $prices;
    }

    /**
     * @param array $isinList
     * @param \DateTime $date
     * @return BrokerCandleModel[]
     * @throws AccountException
     */
    public function getPricesIsin(array $isinList, \DateTime $date): array
    {
        $result = [];

        if ($this->now->format('Y-m-d') === $date->format('Y-m-d')) {
            $result = $this->getLastPricesIsin($isinList);
        } else {
            $needPrices = [];
            foreach ($isinList as $isin) {
                try {
                    $candle = $this->getDayCandleByIsin($isin, $date);
                } catch (TIException $e) {
                    $candle = null;
                }

                $amount = $candle?->getClose() ?? 0;
                if ($amount !== 0) {
                    $result[$isin] = new BrokerMoneyModel([
                        'amount' => $amount,
                        'currency' => $candle->getCurrency()
                    ]);
                } else {
                    $needPrices[] = $isin;
                }
            }

            $prices = $this->getLastPricesIsin($needPrices);
            $result = array_replace($result, $prices);
        }

        return $result;
    }

    public function getFirstCandle(int $marketInstrumentId): ?BrokerCandleModel
    {
        $candle = $this->candleBrokerService->getFirstCandle($marketInstrumentId);

        if ($candle === null || $candle->getIsComplete() === false) {
            return null;
        }

        $instrument = $this->marketInstrumentService->getInstrumentEntityById($marketInstrumentId);
        $this->saveCandle($candle, $instrument->getId());

        return $candle;
    }

    public function getDayCandleInterval(
        int $marketInstrumentId,
        \DateTime $from,
        \DateTime $to
    ): CandleCollection {
        $entities = [];

        $candles = $this->candleBrokerService->getDayCandleInterval($marketInstrumentId, $from, $to);

        /** @var BrokerCandleModel $candle */
        foreach ($candles->getIterator() as $candle) {
            $entities[] = $this->convertCandleModelToEntity($candle, $marketInstrumentId);
        }

        $this->candleStorage->addEntityArray($entities);

        return $candles;
    }

    /**
     * @inheritDoc
     */
    public function getInstrumentsIntervalDayCandles(
        array $marketInstrumentIds,
        \DateTime $dateFrom,
        \DateTime $dateTo
    ): array {
        // TODO: Implement getInstrumentsIntervalDayCandles() method.
    }

    private function saveCandle(BrokerCandleModel $tiCandle, int $marketInstrumentId): void
    {
        $entitiesToSave = [];

        $candleEntity = $this->convertCandleModelToEntity($tiCandle, $marketInstrumentId);
        $candles = $this->candleStorage->findCandles(
            $candleEntity->getMarketInstrumentId(),
            CandleIntervalType::tryFrom($candleEntity->getCandleInterval()),
            $candleEntity->getTime(),
            $candleEntity->getIsFinal()
        );

        foreach ($candles as $candle) {
            $candle->setIsActual(0);
            $entitiesToSave[] = $candle;
        }

        $entitiesToSave[] = $candleEntity;

        $this->candleStorage->addEntityArray($entitiesToSave);
    }

    private function convertCandleModelToEntity(
        BrokerCandleModel $brokerCandle,
        int $marketInstrumentId
    ): CandleEntity {
        $candle = new CandleEntity();
        $candle->setFinal((int)$brokerCandle->getIsComplete());
        $candle->setOpen($brokerCandle->getOpen());
        $candle->setClose($brokerCandle->getClose());
        $candle->setHigh($brokerCandle->getHigh());
        $candle->setLow($brokerCandle->getLow());
        $candle->setVolume($brokerCandle->getVolume());
        $candle->setTime($brokerCandle->getTime());
        $candle->setCurrency($brokerCandle->getCurrency());
        $candle->setCandleInterval($brokerCandle->getInterval());
        $candle->setMarketInstrumentId($marketInstrumentId);
        $candle->setCreatedDate($this->now);

        return $candle;
    }
}
