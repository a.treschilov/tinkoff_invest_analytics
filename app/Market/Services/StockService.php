<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Collections\ShareCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerShareModel;
use App\Entities\MarketStockEntity;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\MarketSectorService;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Interfaces\MarketConcreteInstrumentInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Models\MarketStockFilterModel;
use App\Market\Models\MarketStockModel;
use App\Market\Storages\MarketInstrumentStorage;
use JetBrains\PhpStorm\ArrayShape;

readonly class StockService implements MarketConcreteInstrumentInterface
{
    public function __construct(
        private CountryService $countryService,
        private CurrencyService $currencyService,
        private MarketSectorService $marketSectorService,
        private MarketInstrumentStorage $marketInstrumentStorage,
        private SourceBrokerInterface $broker
    ) {
    }
    public function isNeedForUpdate(MarketInstrumentModel $instrument): bool
    {
        return false;
    }

    public function updateData(int $marketInstrumentId): int
    {
        $instrument = $this->marketInstrumentStorage->findById($marketInstrumentId);

        $shareModel = $this->broker->getShareByIsin($instrument->getIsin());
        if (null !== $shareModel) {
            $stockEntity = $instrument->getMarketStock();
            if ($instrument->getMarketStock() === null) {
                $stockModel = $this->convertBrokerToMarketModel($shareModel);
                $stockEntity = $this->convertModelToEntity($stockModel);
                $stockEntity->setMarketInstrumentId($instrument->getId());
                $stockEntity->setMarketInstrument($instrument);
                $instrument->setMarketStock($stockEntity);
            }

            $stockEntity->setCountryIso($shareModel->getCountry());
            $stockEntity->setCountry($this->countryService->getCountryByIso($shareModel->getCountry()));
            $stockEntity->setIndustry($shareModel->getIndustry());
            $stockEntity->setSector($shareModel->getSector());

            $this->marketInstrumentStorage->addEntity($instrument);
        }

        return $marketInstrumentId;
    }

    public function convertBrokerToInstrumentCollection(ShareCollection $shares): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();
        /** @var BrokerShareModel $share */
        foreach ($shares->getIterator() as $share) {
            $instrument = new MarketInstrumentModel([
                'isin' => $share->getInstrument()->getIsin(),
            ]);
            $collection->add($instrument);
        }

        return $collection;
    }

    #[ArrayShape([
        'country' => "\App\Intl\Collections\CountryCollection",
        'currency' => "\App\Intl\Collections\CurrencyCollection",
        'sector' => "\App\Intl\Collections\MarketSectorCollection"
    ])]
    public function getFilters(): array
    {
        return [
            'country' => $this->countryService->getList(),
            'currency' => $this->currencyService->getList(),
            'sector' => $this->marketSectorService->getList()
        ];
    }

    public function filter(
        MarketInstrumentCollection $instruments,
        MarketStockFilterModel $filter
    ): MarketInstrumentCollection {
        $result = $instruments;
        if ($filter->getCountry() !== null) {
            $result = new MarketInstrumentCollection([]);
            $countriesArray = $filter->getCountry()->toArray();
            /** @var MarketInstrumentModel $instrument */
            foreach ($instruments->getIterator() as $instrument) {
                if (in_array($instrument?->getStock()?->getCountry(), $countriesArray)) {
                    $result->add($instrument);
                }
            }
            $instruments = $result;
        }


        $result = $instruments;
        if ($filter->getCurrency() !== null) {
            $result = new MarketInstrumentCollection([]);
            $currencyArray = $filter->getCurrency()->toArray();
            /** @var MarketInstrumentModel $instrument */
            foreach ($instruments->getIterator() as $instrument) {
                if (in_array($instrument?->getStock()?->getCurrency(), $currencyArray)) {
                    $result->add($instrument);
                }
            }
            $instruments = $result;
        }

        $result = $instruments;
        if ($filter->getSector() !== null) {
            $result = new MarketInstrumentCollection([]);
            $sectorArray = $filter->getSector()->toArray();
            $marketSectorName = $this->marketSectorService->getNameByIds($sectorArray);
            /** @var MarketInstrumentModel $instrument */
            foreach ($instruments->getIterator() as $instrument) {
                if (in_array($instrument?->getStock()?->getSector(), $marketSectorName)) {
                    $result->add($instrument);
                }
            }
        }

        return $result;
    }

    private function convertBrokerToMarketModel(BrokerShareModel $model): MarketStockModel
    {
        return new MarketStockModel([
            'country' => $model->getCountry(),
            'sector' => $model->getSector(),
            'industry' => $model->getIndustry(),
            'currency' => $model->getInstrument()->getCurrency()
        ]);
    }

    private function convertModelToEntity(MarketStockModel $model): MarketStockEntity
    {

        $entity = new MarketStockEntity();
        $entity->setCountryIso($model->getCountry());
        $entity->setIndustry($model->getIndustry());
        $entity->setSector($model->getSector());
        if ($model->getCountry() !== null) {
            $entity->setCountry($this->countryService->getCountryByIso($model->getCountry()));
        }

        return $entity;
    }
}
