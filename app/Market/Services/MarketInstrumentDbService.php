<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Exceptions\MarketDataException;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Interfaces\MarketConcreteInstrumentInterface;
use App\Market\Interfaces\MarketInstrumentServiceInterface;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Storages\MarketInstrumentStorage;
use App\Market\Types\InstrumentType;

readonly class MarketInstrumentDbService implements MarketInstrumentServiceInterface
{
    public function __construct(
        private MarketInstrumentStorage $marketInstrumentStorage,
        private MarketInstrumentHelperService $marketInstrumentHelperService,
        private MarketConcreteInstrumentInterface $marketBondService,
        private MarketConcreteInstrumentInterface $marketStockService,
        private MarketConcreteInstrumentInterface $marketFutureService,
    ) {
    }

    public function getByType(InstrumentType $instrumentType): MarketInstrumentCollection
    {
        $instruments = $this->marketInstrumentStorage->findByType($instrumentType->value);

        $collection = new MarketInstrumentCollection();
        foreach ($instruments as $instrument) {
            $collection->add($this->marketInstrumentHelperService->convertEntityToModel($instrument));
        }

        return $collection;
    }

    public function getByIsin(string $isin): ?MarketInstrumentModel
    {
        $instrument = $this->marketInstrumentStorage->findOneByIsin($isin);

        if ($instrument === null) {
            return null;
        }

        return $this->marketInstrumentHelperService->convertEntityToModel($instrument);
    }

    public function getByIsinList(array $isinList): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();
        $instruments = $this->marketInstrumentStorage->findByIsinList($isinList);
        foreach ($instruments as $instrument) {
            $collection->add($this->marketInstrumentHelperService->convertEntityToModel($instrument));
        }

        return $collection;
    }

    public function isNeedUpdate(string $isin): bool
    {
        $instrument = $this->getByIsin($isin);

        if ($instrument === null) {
            return true;
        }

        return match ($instrument->getType()) {
            InstrumentType::BOND->value => $this->marketBondService->isNeedForUpdate($instrument),
            InstrumentType::STOCK->value => $this->marketStockService->isNeedForUpdate($instrument),
            InstrumentType::FUTURE->value => $this->marketFutureService->isNeedForUpdate($instrument),
            default => throw new MarketDataException(
                'Instrument type ' . $instrument->getType() . ' is not supported for update'
            ),
        };
    }

    public function updateData(string $isin): ?MarketInstrumentModel
    {
        $instrument = $this->getByIsin($isin);
        if ($instrument === null) {
            return null;
        }

        switch ($instrument->getType()) {
            case InstrumentType::BOND->value:
                $this->marketBondService->updateData($instrument->getMarketInstrumentId());
                break;
            case InstrumentType::STOCK->value:
                $this->marketStockService->updateData($instrument->getMarketInstrumentId());
                break;
            case InstrumentType::FUTURE->value:
                $this->marketFutureService->updateData($instrument->getMarketInstrumentId());
                break;
        }

        return $instrument;
    }
}
