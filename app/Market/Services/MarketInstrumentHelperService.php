<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Entities\MarketCurrencyEntity;
use App\Entities\MarketStockEntity;
use App\Market\Entities\MarketBondEntity;
use App\Market\Entities\MarketFutureEntity;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Interfaces\MarketFutureServiceInterface;
use App\Market\Models\MarketBondModel;
use App\Market\Models\MarketCurrencyModel;
use App\Market\Models\MarketFutureModel;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Models\MarketStockModel;
use App\Market\Storages\MarketInstrumentStorage;
use App\Models\PriceModel;

readonly class MarketInstrumentHelperService
{
    public function __construct(
        private MarketInstrumentStorage $marketInstrumentStorage
    ) {
    }

    public function getInstrumentById(int $marketInstrumentId): ?MarketInstrumentModel
    {
        $entity = $this->marketInstrumentStorage->findById($marketInstrumentId);

        if ($entity === null) {
            return null;
        }

        return $this->convertEntityToModel($entity);
    }

    public function getInstrumentEntityById(int $marketInstrumentId): ?MarketInstrumentEntity
    {
        return $this->marketInstrumentStorage->findById($marketInstrumentId);
    }

    public function convertEntityToModel(MarketInstrumentEntity $instrument): MarketInstrumentModel
    {
        return new MarketInstrumentModel([
            'marketInstrumentId' => $instrument->getId(),
            'itemId' => $instrument->getItem()->getId(),
            'ticker' => $instrument->getTicker(),
            'isin' => $instrument->getIsin(),
            'minPriceIncrement' => $instrument->getMinPriceIncrement(),
            'lot' => $instrument->getLot(),
            'currency' => $instrument->getCurrency(),
            'name' => $instrument->getName(),
            'type' => $instrument->getType(),
            'stock' => $this->convertStockEntityToModel($instrument->getMarketStock()),
            'bond' => $this->convertBondEntityToModel($instrument->getMarketBond()),
            'future' => $this->convertFutureEntityToModel($instrument->getMarketFuture()),
            'currencyModel' => $this->convertCurrencyEntityToModel($instrument->getMarketCurrency()),
        ]);
    }

    public function convertBondEntityToModel(?MarketBondEntity $bond): ?MarketBondModel
    {
        if ($bond === null) {
            return null;
        }

        return new MarketBondModel([
            'marketBondId' => $bond->getId(),
            'nominal' => new PriceModel([
                'amount' => $bond->getNominal(),
                'currency' => $bond->getCurrencyIso()
            ]),
            'initialNominal' => new PriceModel([
                'amount' => $bond->getInitialNominal(),
                'currency' => $bond->getCurrencyIso()
            ]),
            'maturityDate' => $bond->getMaturityDate(),
            'updatedDate' => $bond->getUpdatedDate()
        ]);
    }

    public function convertStockEntityToModel(?MarketStockEntity $stock): ?MarketStockModel
    {
        if ($stock === null) {
            return null;
        }

        return new MarketStockModel([
            'sector' => $stock->getSector(),
            'currency' => $stock->getMarketInstrument()->getCurrency(),
            'country' => $stock->getCountry()?->getIso(),
            'industry' => $stock->getIndustry(),
        ]);
    }

    public function convertFutureEntityToModel(?MarketFutureEntity $entity): ?MarketFutureModel
    {
        if ($entity === null) {
            return null;
        }

        return new MarketFutureModel([
            'marketFutureId' => $entity->getId(),
            'futureType' => $entity->getFuturesType(),
            'assetType' => $entity->getAssetType(),
            'expirationDate' => $entity->getExpirationDate(),
            'minPriceIncrementAmount' => $entity->getMinPriceIncrementAmount()
        ]);
    }

    public function convertCurrencyEntityToModel(?MarketCurrencyEntity $entity): ?MarketCurrencyModel
    {
        if ($entity === null) {
            return null;
        }

        return new MarketCurrencyModel([
            'iso' => $entity->getIso()
        ]);
    }
}
