<?php

declare(strict_types=1);

namespace App\Market\Services;

use App\Broker\Collections\FutureCollection;
use App\Broker\Interfaces\SourceBrokerInterface;
use App\Broker\Models\BrokerFutureModel;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Entities\MarketFutureEntity;
use App\Market\Interfaces\MarketConcreteInstrumentInterface;
use App\Market\Interfaces\MarketFutureServiceInterface;
use App\Market\Models\MarketFutureModel;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Storages\MarketInstrumentStorage;
use App\Market\Types\FutureAssetType;
use App\Market\Types\FutureType;

readonly class FutureService implements MarketFutureServiceInterface, MarketConcreteInstrumentInterface
{
    public function __construct(
        private SourceBrokerInterface $broker,
        private MarketInstrumentStorage $marketInstrumentStorage
    ) {
    }

    public function convertEntityToModel(MarketFutureEntity $entity): MarketFutureModel
    {
        return new MarketFutureModel([
            'marketFutureId' => $entity->getId(),
            'futureType' => $entity->getFuturesType(),
            'assetType' => $entity->getAssetType(),
            'expirationDate' => $entity->getExpirationDate()
        ]);
    }

    public function convertBrokerToInstrumentCollection(FutureCollection $futures): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();

        /** @var BrokerFutureModel $future */
        foreach ($futures->getIterator() as $future) {
            $instrument = new MarketInstrumentModel([
                'isin' => $future->getTicker(),
                'ticker' => $future->getTicker(),
                'minPriceIncrement' => $future->getMinPriceIncrement(),
                'future' => new MarketFutureModel([
                    'expirationDate' => $future->getExpirationDate(),
                    'minPriceIncrementAmount' => $future->getMinPriceIncrementAmount(),
                ])
            ]);
            $collection->add($instrument);
        }

        return $collection;
    }

    public function isNeedForUpdate(MarketInstrumentModel $instrument): bool
    {
        $instrumentEntity = $this->marketInstrumentStorage->findOneByIsin($instrument->getIsin());
        if ($instrumentEntity->getItem()->getIsOutdated() === 0) {
            return true;
        }

        return false;
    }

    public function updateData(int $marketInstrumentId): int
    {
        $instrument = $this->marketInstrumentStorage->findById($marketInstrumentId);

        $future = $this->broker->getFutureByIsin($instrument->getIsin());

        if ($future !== null) {
            if ($instrument->getMarketFuture() === null) {
                $futureModel = $this->convertBrokerToMarketModel($future);
                $futureEntity = $this->convertModelToEntity($futureModel);
                $futureEntity->setMarketInstrument($instrument);
                $instrument->setMarketFuture($futureEntity);
            }

            $instrument->getMarketFuture()->setExpirationDate($future->getExpirationDate());
            $instrument->getMarketFuture()->setUpdatedDate(new \DateTime());

            if ($future->getExpirationDate() !== null && $future->getExpirationDate()->getTimestamp() < time()) {
                $instrument->getItem()->setIsOutdated(1);
            }

            $this->marketInstrumentStorage->addEntity($instrument);
        }

        return $instrument->getId();
    }

    public function getBrokerFutureByIsin(string $isin): ?MarketFutureModel
    {
        $future = $this->broker->getFutureByIsin($isin);

        if ($future === null) {
            return null;
        }

        return $this->convertBrokerToMarketModel($future);
    }

    private function convertBrokerToMarketModel(BrokerFutureModel $future): MarketFutureModel
    {
        return new MarketFutureModel([
            'expirationDate' => $future->getExpirationDate(),
            'minPriceIncrementAmount' => $future->getMinPriceIncrementAmount(),
            'futureType' => FutureType::tryFrom($future->getFutureType() ?? ''),
            'futureAssetType' => FutureAssetType::tryFrom($future->getAssetType() ?? ''),
        ]);
    }

    private function convertModelToEntity(MarketFutureModel $model): MarketFutureEntity
    {
        $entity = new MarketFutureEntity();
        $entity->setFuturesType($model->getFutureType());
        $entity->setAssetType($model->getFutureAssetType());
        $entity->setExpirationDate($model->getExpirationDate());
        $entity->setMinPriceIncrementAmount($model->getMinPriceIncrementAmount());

        return $entity;
    }
}
