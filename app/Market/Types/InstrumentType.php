<?php

declare(strict_types=1);

namespace App\Market\Types;

enum InstrumentType: string
{
    case STOCK = 'Stock';
    case BOND = 'Bond';
    case ETF = 'ETF';
    case CURRENCY = 'Currency';
    case FUTURE = 'Futures';
    case INDEX = 'index';
    case COMMODITY = 'commodity';
}
