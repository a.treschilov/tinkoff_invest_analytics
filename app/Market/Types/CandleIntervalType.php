<?php

declare(strict_types=1);

namespace App\Market\Types;

enum CandleIntervalType: string
{
    case MIN_1 = '1min';
    case MIN_2 = '2min';
    case MIN_3 = '3min';
    case MIN_5 = '5min';
    case MIN_10 = '10min';
    case MIN_15 = '15min';
    case MIN_30 = '30min';
    case HOUR_1 = '1hour';
    case HOUR_2 = '2hour';
    case HOUR_4 = '4hour';
    case DAY_1 = '1day';
    case WEEK_1 = '1week';
    case MONTH_1 = '1month';
}
