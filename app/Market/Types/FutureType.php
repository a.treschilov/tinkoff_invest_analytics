<?php

declare(strict_types=1);

namespace App\Market\Types;

enum FutureType: string
{
    case PHYSICAL_DELIVERY = 'physical_delivery';
    case CASH_SETTLEMENT = 'cash_settlement';
}
