<?php

declare(strict_types=1);

namespace App\Market\Types;

enum FutureAssetType: string
{
    case COMMODITY = 'commodity';
    case CURRENCY = 'currency';
    case SECURITY = 'security';
    case INDEX = 'index';
}
