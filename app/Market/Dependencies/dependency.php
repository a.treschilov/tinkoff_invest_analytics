<?php

declare(strict_types=1);

use App\Adapters\Services\BrokerIterator;
use App\Common\Amqp\AmqpClient;
use App\Intl\Services\CountryService;
use App\Intl\Services\CurrencyService;
use App\Intl\Services\MarketSectorService;
use App\Item\Services\PaymentScheduleService;
use App\Item\Storages\ItemStorage;
use App\Market\Services\CandleDbService;
use App\Market\Services\CandleHelperService;
use App\Market\Services\CandleBrokerService;
use App\Market\Services\CandleDbStoreServiceDecorator;
use App\Market\Services\BondService;
use App\Market\Services\FutureService;
use App\Market\Services\MarketIndicativesService;
use App\Market\Services\MarketInstrumentBrokerService;
use App\Market\Services\MarketInstrumentDbService;
use App\Market\Services\MarketInstrumentDbServiceStoreDecorator;
use App\Market\Services\MarketInstrumentHelperService;
use App\Market\Services\MarketInstrumentImportService;
use App\Market\Services\StockService as MarketStockService;
use App\Market\Storages\CandleImportStorage;
use App\Market\Storages\MarketInstrumentStorage;
use App\Services\FillMarketInstrumentServiceDecorator;
use App\Services\MarketInstrumentService;
use App\Market\Storages\CandleStorage;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

/** @var ContainerInterface $container */
$container->set(BondService::class, function (ContainerInterface $c) {
    return new BondService(
        $c->get('Now' . DateTime::class),
        $c->get(AmqpClient::class),
        $c->get(PaymentScheduleService::class),
        $c->get(BrokerIterator::class),
        $c->get(MarketInstrumentStorage::class)
    );
});

$container->set(CandleBrokerService::class, function (ContainerInterface $c) {
    return new CandleBrokerService(
        $c->get(BrokerIterator::class),
        $c->get('Now' . DateTime::class),
        $c->get(CandleImportStorage::class),
        $c->get(AmqpClient::class),
        $c->get(MarketInstrumentHelperService::class),
        $c->get(Logger::class)
    );
});

$container->set(CandleDbStoreServiceDecorator::class, function (ContainerInterface $c) {
    return new CandleDbStoreServiceDecorator(
        $c->get(CandleBrokerService::class),
        $c->get(CandleDbService::class),
        $c->get(CandleStorage::class),
        $c->get(FillMarketInstrumentServiceDecorator::class),
        $c->get('Now' . DateTime::class)
    );
});

$container->set(CandleDbService::class, function (ContainerInterface $c) {
    return new CandleDbService(
        $c->get(CandleStorage::class),
        $c->get(CandleHelperService::class),
        $c->get(MarketInstrumentStorage::class),
        $c->get('Now' . DateTime::class)
    );
});


$container->set(CandleImportStorage::class, function (ContainerInterface $c) {
    return new CandleImportStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(CandleHelperService::class, function () {
    return new CandleHelperService();
});

$container->set(MarketIndicativesService::class, function (ContainerInterface $c) {
    return new MarketIndicativesService(
        $c->get(FillMarketInstrumentServiceDecorator::class)
    );
});

$container->set(MarketInstrumentStorage::class, function (ContainerInterface $c) {
    return new MarketInstrumentStorage(
        $c->get('doctrine.orm.entity_manager')
    );
});

$container->set(MarketInstrumentDbService::class, function (ContainerInterface $c) {
    return new MarketInstrumentDbService(
        $c->get(MarketInstrumentStorage::class),
        $c->get(MarketInstrumentHelperService::class),
        $c->get(BondService::class),
        $c->get(MarketStockService::class),
        $c->get(FutureService::class)
    );
});

$container->set(MarketInstrumentBrokerService::class, function (ContainerInterface $c) {
    return new MarketInstrumentBrokerService(
        $c->get(BrokerIterator::class),
        $c->get(BondService::class),
        $c->get(MarketStockService::class),
        $c->get(FutureService::class),
    );
});

$container->set(MarketInstrumentDbServiceStoreDecorator::class, function (ContainerInterface $c) {
    return new MarketInstrumentDbServiceStoreDecorator(
        $c->get(MarketInstrumentDbService::class),
        $c->get(MarketInstrumentBrokerService::class),
        $c->get(ItemStorage::class)
    );
});

$container->set(FutureService::class, function (ContainerInterface $c) {
    return new FutureService(
        $c->get(BrokerIterator::class),
        $c->get(MarketInstrumentStorage::class),
    );
});

$container->set(MarketStockService::class, function (ContainerInterface $c) {
    return new MarketStockService(
        $c->get(CountryService::class),
        $c->get(CurrencyService::class),
        $c->get(MarketSectorService::class),
        $c->get(MarketInstrumentStorage::class),
        $c->get(BrokerIterator::class)
    );
});

$container->set(MarketInstrumentHelperService::class, function (ContainerInterface $c) {
    return new MarketInstrumentHelperService(
        $c->get(MarketInstrumentStorage::class)
    );
});

$container->set(MarketInstrumentImportService::class, function (ContainerInterface $c) {
    return new MarketInstrumentImportService(
        $c->get(AmqpClient::class),
        $c->get(MarketInstrumentDbService::class),
        $c->get(MarketInstrumentBrokerService::class),
        $c->get(MarketInstrumentDbServiceStoreDecorator::class)
    );
});
