<?php

declare(strict_types=1);

namespace App\Market\Entities;

use App\Market\Types\FutureAssetType;
use App\Market\Types\FutureType;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_future')]
#[ORM\Entity]
class MarketFutureEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'market_future_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected int $marketInstrumentId;

    #[ORM\JoinColumn(name: 'market_instrument_id', referencedColumnName: 'market_instrument_id')]
    #[ORM\OneToOne(targetEntity: MarketInstrumentEntity::class, inversedBy: 'marketFuture')]
    protected MarketInstrumentEntity $marketInstrument;

    #[ORM\Column(name: 'futures_type', type: 'string', enumType: FutureType::class)]
    protected ?FutureType $futuresType = null;

    #[ORM\Column(name: 'asset_type', type: 'string', enumType: FutureAssetType::class)]
    protected ?FutureAssetType $assetType = null;

    #[ORM\Column(name: 'expiration_date', type: 'datetime')]
    protected \DateTime $expirationDate;

    #[ORM\Column(name: 'min_price_increment_amount', type: 'float')]
    protected ?float $minPriceIncrementAmount = null;

    #[ORM\Column(name: 'updated_date', type: 'datetime')]
    protected \DateTime $updatedDate;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    public function getMarketInstrument(): MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    public function setMarketInstrument(MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    public function getFuturesType(): ?FutureType
    {
        return $this->futuresType;
    }

    public function setFuturesType(?FutureType $futuresType): void
    {
        $this->futuresType = $futuresType;
    }

    public function getAssetType(): ?FutureAssetType
    {
        return $this->assetType;
    }

    public function setAssetType(?FutureAssetType $assetType): void
    {
        $this->assetType = $assetType;
    }

    public function getExpirationDate(): \DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getMinPriceIncrementAmount(): ?float
    {
        return $this->minPriceIncrementAmount;
    }

    public function setMinPriceIncrementAmount(?float $minPriceIncrementAmount): void
    {
        $this->minPriceIncrementAmount = $minPriceIncrementAmount;
    }

    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}
