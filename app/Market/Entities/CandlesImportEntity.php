<?php

declare(strict_types=1);

namespace App\Market\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.candles_import')]
#[ORM\Entity]
class CandlesImportEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'candles_import_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected int $marketInstrumentId;


    #[ORM\Column(name: 'import_date_start', type: 'datetime')]
    protected \DateTime $importDateStart;

    #[ORM\Column(name: 'import_date_end', type: 'datetime')]
    protected \DateTime $importDateEnd;

    #[ORM\Column(name: 'created_date', type: 'datetime')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'added', type: 'integer')]
    protected int $added;

    #[ORM\Column(name: 'updated', type: 'integer')]
    protected int $updated;

    #[ORM\Column(name: 'skipped', type: 'integer')]
    protected int $skipped;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return \DateTime
     */
    public function getImportDateStart(): \DateTime
    {
        return $this->importDateStart;
    }

    /**
     * @param \DateTime $importDateStart
     */
    public function setImportDateStart(\DateTime $importDateStart): void
    {
        $this->importDateStart = $importDateStart;
    }

    /**
     * @return \DateTime
     */
    public function getImportDateEnd(): \DateTime
    {
        return $this->importDateEnd;
    }

    /**
     * @param \DateTime $importDateEnd
     */
    public function setImportDateEnd(\DateTime $importDateEnd): void
    {
        $this->importDateEnd = $importDateEnd;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getAdded(): int
    {
        return $this->added;
    }

    /**
     * @param int $added
     */
    public function setAdded(int $added): void
    {
        $this->added = $added;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getSkipped(): int
    {
        return $this->skipped;
    }

    /**
     * @param int $skipped
     */
    public function setSkipped(int $skipped): void
    {
        $this->skipped = $skipped;
    }
}
