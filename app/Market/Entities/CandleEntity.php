<?php

declare(strict_types=1);

namespace App\Market\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.candles')]
#[ORM\Entity]
class CandleEntity
{
    #[ORM\Id]
    #[ORM\Column(name: 'candle_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected int $marketInstrumentId;

    #[ORM\Column(name: 'is_final', type: 'integer')]
    protected int $isFinal;

    #[ORM\Column(name: 'open', type: 'float')]
    protected float $open;

    #[ORM\Column(name: 'close', type: 'float')]
    protected float $close;

    #[ORM\Column(name: 'high', type: 'float')]
    protected float $high;

    #[ORM\Column(name: 'low', type: 'float')]
    protected float $low;

    #[ORM\Column(name: 'currency', type: 'string')]
    protected string $currency;

    #[ORM\Column(name: 'volume', type: 'integer')]
    protected int $volume;

    #[ORM\Column(name: 'candle_interval', type: 'string')]
    protected string $candleInterval;

    #[ORM\Column(name: 'created_date', type: 'datetime')]
    protected \DateTime $createdDate;

    #[ORM\Column(name: 'time', type: 'datetime')]
    protected \DateTime $time;

    #[ORM\Column(name: 'is_actual', type: 'integer')]
    protected int $isActual = 1;

    public function __construct()
    {
        $currentDateTime = new \DateTime();
        $this->createdDate = $currentDateTime;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return int
     */
    public function getIsFinal(): int
    {
        return $this->isFinal;
    }

    /**
     * @param int $isFinal
     */
    public function setFinal(int $isFinal): void
    {
        $this->isFinal = $isFinal;
    }

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     */
    public function setOpen(float $open): void
    {
        $this->open = $open;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     */
    public function setClose(float $close): void
    {
        $this->close = $close;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     */
    public function setHigh(float $high): void
    {
        $this->high = $high;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     */
    public function setLow(float $low): void
    {
        $this->low = $low;
    }

    /**
     * @return int
     */
    public function getVolume(): int
    {
        return $this->volume;
    }

    /**
     * @param int $volume
     */
    public function setVolume(int $volume): void
    {
        $this->volume = $volume;
    }

    /**
     * @return string
     */
    public function getCandleInterval(): string
    {
        return $this->candleInterval;
    }

    /**
     * @param string $candleInterval
     */
    public function setCandleInterval(string $candleInterval): void
    {
        $this->candleInterval = $candleInterval;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getIsActual(): int
    {
        return $this->isActual;
    }

    public function setIsActual(int $isActual): void
    {
        $this->isActual = $isActual;
    }
}
