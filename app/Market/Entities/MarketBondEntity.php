<?php

declare(strict_types=1);

namespace App\Market\Entities;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'tinkoff_invest.market_bond')]
#[ORM\Entity]
class MarketBondEntity
{
    /**
     * @var int
     */
    #[ORM\Id]
    #[ORM\Column(name: 'market_bond_id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'currency_iso', type: 'string')]
    protected string $currencyIso;

    /**
     * @var int
     */
    #[ORM\Column(name: 'market_instrument_id', type: 'integer')]
    protected int $marketInstrumentId;

    /**
     * @var MarketInstrumentEntity
     */
    #[ORM\JoinColumn(name: 'market_instrument_id', referencedColumnName: 'market_instrument_id')]
    #[ORM\OneToOne(targetEntity: MarketInstrumentEntity::class, inversedBy: 'marketBond')]
    protected MarketInstrumentEntity $marketInstrument;

    /**
     * @var float
     */
    #[ORM\Column(name: 'nominal', type: 'float')]
    protected float $nominal;

    /**
     * @var float
     */
    #[ORM\Column(name: 'initial_nominal', type: 'float')]
    protected float $initialNominal;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'maturity_date', type: 'datetime')]
    protected ?\DateTime $maturityDate = null;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'updated_date', type: 'datetime')]
    protected \DateTime $updatedDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return MarketInstrumentEntity
     */
    public function getMarketInstrument(): MarketInstrumentEntity
    {
        return $this->marketInstrument;
    }

    /**
     * @param MarketInstrumentEntity $marketInstrument
     */
    public function setMarketInstrument(MarketInstrumentEntity $marketInstrument): void
    {
        $this->marketInstrument = $marketInstrument;
    }

    /**
     * @return float
     */
    public function getNominal(): float
    {
        return $this->nominal;
    }

    /**
     * @param float $nominal
     */
    public function setNominal(float $nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return float
     */
    public function getInitialNominal(): float
    {
        return $this->initialNominal;
    }

    /**
     * @param float $initialNominal
     */
    public function setInitialNominal(float $initialNominal): void
    {
        $this->initialNominal = $initialNominal;
    }

    /**
     * @return string
     */
    public function getCurrencyIso(): string
    {
        return $this->currencyIso;
    }

    /**
     * @param string $currencyIso
     */
    public function setCurrencyIso(string $currencyIso): void
    {
        $this->currencyIso = $currencyIso;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getMaturityDate(): ?\DateTime
    {
        return $this->maturityDate;
    }

    /**
     * @param \DateTime|null $maturityDate
     */
    public function setMaturityDate(?\DateTime $maturityDate): void
    {
        $this->maturityDate = $maturityDate;
    }
}
