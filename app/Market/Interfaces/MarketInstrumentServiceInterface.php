<?php

declare(strict_types=1);

namespace App\Market\Interfaces;

use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Models\MarketInstrumentModel;
use App\Market\Types\InstrumentType;

interface MarketInstrumentServiceInterface
{
    public function getByType(InstrumentType $instrumentType): MarketInstrumentCollection;
    public function getByIsin(string $isin): ?MarketInstrumentModel;
    public function getByIsinList(array $isinList): MarketInstrumentCollection;
}
