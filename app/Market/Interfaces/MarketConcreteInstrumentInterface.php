<?php

declare(strict_types=1);

namespace App\Market\Interfaces;

use App\Market\Models\MarketInstrumentModel;

interface MarketConcreteInstrumentInterface
{
    public function isNeedForUpdate(MarketInstrumentModel $instrument): bool;

    public function updateData(int $marketInstrumentId): int;
}
