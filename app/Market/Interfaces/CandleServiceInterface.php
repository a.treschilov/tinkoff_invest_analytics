<?php

declare(strict_types=1);

namespace App\Market\Interfaces;

use App\Broker\Models\BrokerCandleModel;
use App\Broker\Models\BrokerMoneyModel;
use App\Broker\Models\BrokerPortfolioPositionModel;
use App\Collections\CandleCollection;
use App\Market\Entities\MarketInstrumentEntity;
use App\Market\Types\InstrumentType;

interface CandleServiceInterface
{
    public function getTodayCandle(string $isin): ?BrokerCandleModel;

    public function getDayCandleByMarketInstrument(
        MarketInstrumentEntity $marketInstrumentEntity,
        \DateTime $date
    ): ?BrokerCandleModel;

    public function getDayCandleByIsin(
        string $isin,
        \DateTime $date
    ): ?BrokerCandleModel;

    /**
     * @param array $isinList
     * @return BrokerMoneyModel[]
     */
    public function getLastPricesIsin(array $isinList): array;

    /**
     * @param array $isinList
     * @param \DateTime $date
     * @return BrokerMoneyModel[]
     */
    public function getPricesIsin(array $isinList, \DateTime $date): array;

    public function getFirstCandle(int $marketInstrumentId): ?BrokerCandleModel;

    public function getDayCandleInterval(
        int $marketInstrumentId,
        \DateTime $from,
        \DateTime $to
    ): CandleCollection;

    /**
     * @param array $marketInstrumentIds
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @return array{
     *      marketInstrumentId: int,
     *      candles: CandleCollection
     *  }
     */
    public function getInstrumentsIntervalDayCandles(
        array $marketInstrumentIds,
        \DateTime $dateFrom,
        \DateTime $dateTo
    ): array;
}
