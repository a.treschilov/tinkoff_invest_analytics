<?php

declare(strict_types=1);

namespace App\Market\Interfaces;

use App\Broker\Collections\BondCollection;
use App\Market\Collections\MarketInstrumentCollection;
use App\Market\Models\MarketBondModel;

interface MarketBondServiceInterface
{
    public function convertBondToInstrumentCollection(BondCollection $bonds): MarketInstrumentCollection;
    public function getBrokerBondByIsin(string $isin): ?MarketBondModel;
}
