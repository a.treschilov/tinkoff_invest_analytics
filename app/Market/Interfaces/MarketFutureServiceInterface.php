<?php

declare(strict_types=1);

namespace App\Market\Interfaces;

use App\Market\Entities\MarketFutureEntity;
use App\Market\Models\MarketFutureModel;

interface MarketFutureServiceInterface
{
    public function convertEntityToModel(MarketFutureEntity $entity): MarketFutureModel;
}
