<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;
use App\Models\PriceModel;

class MarketBondModel extends BaseModel
{
    private ?int $marketBondId = null;
    private PriceModel $nominal;
    private PriceModel $initialNominal;
    private ?\DateTime $maturityDate = null;
    private ?\DateTime $updatedDate = null;

    public function getMarketBondId(): ?int
    {
        return $this->marketBondId;
    }

    public function setMarketBondId(?int $marketBondId): void
    {
        $this->marketBondId = $marketBondId;
    }

    /**
     * @return PriceModel
     */
    public function getNominal(): PriceModel
    {
        return $this->nominal;
    }

    /**
     * @param PriceModel $nominal
     */
    public function setNominal(PriceModel $nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return PriceModel
     */
    public function getInitialNominal(): PriceModel
    {
        return $this->initialNominal;
    }

    /**
     * @param PriceModel $initialNominal
     */
    public function setInitialNominal(PriceModel $initialNominal): void
    {
        $this->initialNominal = $initialNominal;
    }

    /**
     * @return \DateTime|null
     */
    public function getMaturityDate(): ?\DateTime
    {
        return $this->maturityDate;
    }

    /**
     * @param \DateTime|null $maturityDate
     */
    public function setMaturityDate(?\DateTime $maturityDate): void
    {
        $this->maturityDate = $maturityDate;
    }

    public function getUpdatedDate(): ?\DateTime
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(?\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}
