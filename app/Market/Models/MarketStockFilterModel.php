<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;
use App\Intl\Collections\CountryCollection;
use App\Intl\Collections\CurrencyCollection;
use App\Intl\Collections\MarketSectorCollection;

class MarketStockFilterModel extends BaseModel
{
    private ?CountryCollection $country = null;
    private ?CurrencyCollection $currency = null;
    private ?MarketSectorCollection $sector = null;

    public function getCountry(): ?CountryCollection
    {
        return $this->country;
    }

    public function setCountry(?CountryCollection $country): void
    {
        $this->country = $country;
    }

    public function getCurrency(): ?CurrencyCollection
    {
        return $this->currency;
    }

    public function setCurrency(?CurrencyCollection $currency): void
    {
        $this->currency = $currency;
    }

    public function getSector(): ?MarketSectorCollection
    {
        return $this->sector;
    }

    public function setSector(?MarketSectorCollection $sector): void
    {
        $this->sector = $sector;
    }
}
