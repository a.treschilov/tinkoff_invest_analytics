<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;

class MarketStockModel extends BaseModel
{
    private ?string $country = null;
    private ?string $currency = null;
    private ?string $sector = null;
    private ?string $industry = null;

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    public function getSector(): ?string
    {
        return $this->sector;
    }

    public function setSector(?string $sector): void
    {
        $this->sector = $sector;
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(?string $industry): void
    {
        $this->industry = $industry;
    }
}
