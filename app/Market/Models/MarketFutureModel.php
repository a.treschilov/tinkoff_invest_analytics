<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;
use App\Market\Types\FutureAssetType;
use App\Market\Types\FutureType;

class MarketFutureModel extends BaseModel
{
    private int $futureId;
    private ?FutureType $futureType = null;
    private ?FutureAssetType $futureAssetType = null;
    private \DateTime $expirationDate;
    private ?float $minPriceIncrementAmount = null;
    private \DateTime $updatedDate;

    public function getFutureId(): int
    {
        return $this->futureId;
    }

    public function setFutureId(int $futureId): void
    {
        $this->futureId = $futureId;
    }

    public function getFutureType(): ?FutureType
    {
        return $this->futureType;
    }

    public function setFutureType(?FutureType $futureType): void
    {
        $this->futureType = $futureType;
    }

    public function getFutureAssetType(): ?FutureAssetType
    {
        return $this->futureAssetType;
    }

    public function setFutureAssetType(?FutureAssetType $futureAssetType): void
    {
        $this->futureAssetType = $futureAssetType;
    }

    public function getExpirationDate(): \DateTime
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getMinPriceIncrementAmount(): ?float
    {
        return $this->minPriceIncrementAmount;
    }

    public function setMinPriceIncrementAmount(?float $minPriceIncrementAmount): void
    {
        $this->minPriceIncrementAmount = $minPriceIncrementAmount;
    }

    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }
}
