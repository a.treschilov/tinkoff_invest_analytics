<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;

class MarketInstrumentModel extends BaseModel
{
    private int $marketInstrumentId;
    private int $itemId;
    private string $ticker;
    private string $isin;
    private float $minPriceIncrement = 0;
    private string $currency;
    private string $name;
    private ?string $logo = null;
    private string $type;
    private ?string $regNumber = null;
    private int $lot;
    private ?MarketStockModel $stock = null;
    private ?MarketBondModel $bond = null;
    private ?MarketCurrencyModel $currencyModel = null;
    private ?MarketFutureModel $future = null;

    /**
     * @return int
     */
    public function getMarketInstrumentId(): int
    {
        return $this->marketInstrumentId;
    }

    /**
     * @param int $marketInstrumentId
     */
    public function setMarketInstrumentId(int $marketInstrumentId): void
    {
        $this->marketInstrumentId = $marketInstrumentId;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     */
    public function setItemId(int $itemId): void
    {
        $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     */
    public function setTicker(string $ticker): void
    {
        $this->ticker = $ticker;
    }

    /**
     * @return string
     */
    public function getIsin(): string
    {
        return $this->isin;
    }

    /**
     * @param string $isin
     */
    public function setIsin(string $isin): void
    {
        $this->isin = $isin;
    }

    /**
     * @return float
     */
    public function getMinPriceIncrement(): float
    {
        return $this->minPriceIncrement;
    }

    /**
     * @param float $minPriceIncrement
     */
    public function setMinPriceIncrement(float $minPriceIncrement): void
    {
        $this->minPriceIncrement = $minPriceIncrement;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return MarketBondModel|null
     */
    public function getBond(): ?MarketBondModel
    {
        return $this->bond;
    }

    /**
     * @param MarketBondModel|null $bond
     */
    public function setBond(?MarketBondModel $bond): void
    {
        $this->bond = $bond;
    }

    public function getCurrencyModel(): ?MarketCurrencyModel
    {
        return $this->currencyModel;
    }

    public function setCurrencyModel(?MarketCurrencyModel $currencyModel): void
    {
        $this->currencyModel = $currencyModel;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): void
    {
        $this->regNumber = $regNumber;
    }

    public function getFuture(): ?MarketFutureModel
    {
        return $this->future;
    }

    public function setFuture(?MarketFutureModel $future): void
    {
        $this->future = $future;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getLot(): int
    {
        return $this->lot;
    }

    public function setLot(int $lot): void
    {
        $this->lot = $lot;
    }

    public function getStock(): ?MarketStockModel
    {
        return $this->stock;
    }

    public function setStock(?MarketStockModel $stock): void
    {
        $this->stock = $stock;
    }
}
