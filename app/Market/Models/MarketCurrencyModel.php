<?php

declare(strict_types=1);

namespace App\Market\Models;

use App\Common\Models\BaseModel;

class MarketCurrencyModel extends BaseModel
{
    private string $iso;
    private float $nominal;

    public function getIso(): string
    {
        return $this->iso;
    }

    public function setIso(string $iso): void
    {
        $this->iso = $iso;
    }

    public function getNominal(): float
    {
        return $this->nominal;
    }

    public function setNominal(float $nominal): void
    {
        $this->nominal = $nominal;
    }
}
