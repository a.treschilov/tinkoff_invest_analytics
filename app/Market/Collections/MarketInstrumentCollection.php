<?php

declare(strict_types=1);

namespace App\Market\Collections;

use App\Market\Models\MarketInstrumentModel;
use Doctrine\Common\Collections\ArrayCollection;

class MarketInstrumentCollection extends ArrayCollection
{
    /**
     * @return int[]
     * @throws \Exception
     */
    public function getItemIds(): array
    {
        $result = [];
        /** @var MarketInstrumentModel $marketInstrument */
        foreach ($this->getIterator() as $marketInstrument) {
            $result[] = $marketInstrument->getItemId();
        }

        return $result;
    }

    public function getByIsin($isin): ?MarketInstrumentModel
    {
        /** @var MarketInstrumentModel $marketInstrument */
        foreach ($this->getIterator() as $marketInstrument) {
            if ($marketInstrument->getIsin() === $isin) {
                return $marketInstrument;
            }
        }

        return null;
    }

    public function filterNullableBonds(): MarketInstrumentCollection
    {
        $collection = new MarketInstrumentCollection();
        /** @var MarketInstrumentModel $item */
        foreach ($this->getIterator() as $item) {
            if ($item->getBond() !== null) {
                $collection->add($item);
            }
        }

        return $collection;
    }
}
