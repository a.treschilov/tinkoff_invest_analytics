<?php

declare(strict_types=1);

namespace App\Common;

use Throwable;

class BaseException extends \Exception
{
    protected static $CODE;

    public function __construct($message = "", $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code === 0 ? static::$CODE : $code, $previous);
    }
}
