<?php

declare(strict_types=1);

namespace App\Common\Logger\Obfuscator;

abstract class Obfusactor
{
    protected $pathToField;

    /**
     * @return string
     */
    public function getPathToField()
    {
        return $this->pathToField;
    }

    abstract public function obfuscate($value);
}
