<?php

declare(strict_types=1);

namespace App\Common\Logger\Obfuscator;

class AuthObfuscator extends Obfusactor
{
    protected $pathToField = 'headers.HTTP_AUTHORIZATION';

    /**
     * @param array $value
     * @return array
     */
    public function obfuscate($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        $actualValue = $value[0];
        if (strpos($actualValue, 'Bearer ') !== false) {
            $token = substr($actualValue, 7, strlen($actualValue) - 7);
            $tokenParts = explode(".", $token);
            if (count($tokenParts) !== 3) {
                return ['***'];
            }

            list($header, $payload, $signature) = $tokenParts;
            return ['Bearer ' . $header . '.' . $payload . '.***'];
        } else {
            return ['***'];
        }
    }
}
