<?php

declare(strict_types=1);

namespace App\Common\Logger;

class BuilderFactory
{
    public function __construct(private SimpleFactory $loggerFactory)
    {
    }

    public function getNewBuilder(string $name): Builder
    {
        return new Builder($this->loggerFactory->getLogger($name));
    }
}
