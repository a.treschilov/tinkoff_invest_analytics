<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Psr\Log\LoggerInterface;

class Factory
{
    public function __construct(private BuilderFactory $builderFactory)
    {
    }

    /**
     * Создает логгер c записью transaction_id в лог, для удобства поиска логов в одной транзакции пользователя
     * используя builder. Передав секурные параметры будет создан логгер
     * обфусцирующий эти параметры в логе
     *
     * @param string $name
     * @param array $secureParams
     *
     * @return LoggerInterface
     */
    public function create(string $name, array $secureParams = []): LoggerInterface
    {
        $builder = $this->builderFactory->getNewBuilder($name);
        return $builder->withSessionParams()->withSecuringLog($secureParams)->getLogger();
    }
}
