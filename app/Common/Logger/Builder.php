<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Psr\Log\LoggerInterface;

class Builder
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    /**
     * Подключает к логгеру параметры сессии
     *
     * @return Builder
     */
    public function withSessionParams(): Builder
    {
        $this->logger = new SessionLogger($this->logger);
        return $this;
    }

    /**
     * Подключает к логгеру механизм сокрытия параметров
     *
     * @param array $params - массив ключей, значения которых будут сокрыты. Для обозначения вложенности
     * используется символ тчк(.)
     *
     * @return Builder
     */
    public function withSecuringLog(array $params): Builder
    {
        $this->logger = new SecureLogger(
            $this->logger,
            $params
        );
        return $this;
    }

    /**
     * Возвращает построенный сборщиком экземпляр логгера
     *
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
