<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Monolog\Formatter\FormatterInterface;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Log\LoggerInterface;
use App\Common\Logger\Formatter\ArrayNormalizer;
use App\Common\Logger\Formatter\StringCutter;
use App\Common\Logger\Formatter\SyslogJsonFormatter;

class SimpleFactory
{
    private string $logPath;
    private string $defaultName;
    private ?string $rootDirectory;
    private bool $storeByDate;

    private array $loggers;
    private ?FormatterInterface $logFormatter = null;
    private ?ArrayNormalizer $logNormalizer = null;

    public function __construct(array $config)
    {
        $this->logPath = $config['path'];
        $this->defaultName = $config['default_name'];
        $this->rootDirectory = $config['root_directory'] ?? null;
        $this->storeByDate = $config['store_by_date'] ?? false;
        $this->loggers = [];
    }

    /**
     * Возвращает самый простой логгер, осуществляющий запись и форматирование сообщения
     *
     * @param string|null $name
     *
     * @return LoggerInterface
     */
    public function getLogger(?string $name = null): LoggerInterface
    {
        if (null === $name) {
            $name = $this->defaultName;
        }

        if (null !== $this->rootDirectory) {
            $name = "{$this->rootDirectory}/$name";
        }

        $cachedName = $this->storeByDate ? date('Ymd') . $name : $name;
        if (!\array_key_exists($cachedName, $this->loggers)) {
            $logger = new Logger($name);
            $logger->pushProcessor(new PsrLogMessageProcessor(PsrLogMessageProcessor::SIMPLE_DATE));
            $logger->pushHandler($this->getHandler($name));

            $this->loggers[$cachedName] = $logger;
        }

        return $this->loggers[$cachedName];
    }

    private function getLogFormatter(bool $prettyPrint): FormatterInterface
    {
        if (null === $this->logFormatter) {
            $this->logFormatter = new SyslogJsonFormatter($this->getLogNormalizer(), $prettyPrint);
        }

        return $this->logFormatter;
    }

    private function getLogNormalizer(): ArrayNormalizer
    {
        if (null === $this->logNormalizer) {
            $this->logNormalizer = new ArrayNormalizer(new StringCutter());
        }

        return $this->logNormalizer;
    }

    private function getHandler(string $name): HandlerInterface
    {
        $handler = new StreamHandler($this->getLogPath($name));
        $handler->setFormatter($this->getLogFormatter(false));
        return $handler;
    }

    private function getLogPath(string $name): string
    {
        if ($this->storeByDate) {
            $date = date('Y-m-d');
            $logPath = $this->logPath;
            return "{$logPath}/{$date}/{$name}.log";
        }

        return $this->logPath;
    }
}
