<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Doctrine\DBAL\Exception\DriverException;
use Psr\Log\LoggerInterface;
use App\Common\Logger\Obfuscator\Obfusactor;

/**
 * Class SecureLogger
 * @package App\Logger
 */
class SecureLogger implements LoggerInterface
{
    private const LAST_SYMBOLS_COUNT = 4;

    public function __construct(
        private LoggerInterface $logger,
        private array $rules,
        private array $exceptionsWithHiddenTrace = [DriverException::class]
    ) {
    }

    public function emergency($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function alert($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function critical($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function error($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function warning($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function notice($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function info($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function debug($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function log($level, $message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $level, $message, $this->getContext($context));
    }

    private function callLogger(string $functionName, ...$parameters): void
    {
        $this->logger->{$functionName}(...$parameters);
    }

    private function getContext(array $context): array
    {
        $context = $this->hideValues($context);
        $context = $this->removeTrace($context);

        return $context;
    }

    /**
     * Скрывает поля из контекста
     *
     * @param array $context
     * @return array
     */
    private function hideValues(array $context)
    {
        foreach ($this->rules as $rule) {
            $useCustomObfuscator = $rule instanceof Obfusactor;
            $pathToField = $useCustomObfuscator ? $rule->getPathToField() : $rule;
            $keys = explode('.', $pathToField);
            $temp = &$context;
            $keysCount = \count($keys);
            $found = true;
            for ($i = 0; $found && ($i < $keysCount - 1); $i++) {
                $key = $keys[$i];
                if (isset($temp[$key])) {
                    $temp = &$temp[$key];
                } else {
                    $found = false;
                }
            }

            $lastKey = $keys[$keysCount - 1];
            if ($found && isset($temp[$lastKey])) {
                $valueToHide = $temp[$lastKey];
                $temp[$lastKey] =
                    $useCustomObfuscator ? $rule->obfuscate($valueToHide) : $this->getNewValue($valueToHide);
            }
        }

        return $context;
    }

    /**
     * Получает новое значение для отображения в логах. В случае с числовыми и строковыми значениями отображает
     * последние n символов
     *
     * @param mixed $value
     *
     * @return string
     */
    private function getNewValue($value): string
    {
        $newValue = '***';
        if (\is_string($value) || \is_numeric($value)) {
            $value = (string)$value;
            $valueLength = \strlen($value);
            $newValue = $valueLength > self::LAST_SYMBOLS_COUNT
                ? '***' . \substr(
                    $value,
                    -self::LAST_SYMBOLS_COUNT,
                    self::LAST_SYMBOLS_COUNT
                )
                : $newValue;
        }

        return $newValue;
    }

    /**
     * Удаляет трейс исключения, если оно само или его вложенные исключения
     * добавлены в $exceptionsWithHiddenTrace
     *
     * @param array $context
     * @return array
     */
    private function removeTrace(array $context): array
    {
        if (!isset($context['exception'])) {
            return $context;
        }

        /** @var \Throwable $exception */
        $exception = $context['exception'];
        $found = false;

        for ($i = 0; !$found && $i < \count($this->exceptionsWithHiddenTrace); $i++) {
            $currentException = $exception;
            do {
                $found = ($currentException instanceof $this->exceptionsWithHiddenTrace[$i]);
            } while (!$found && $currentException = $currentException->getPrevious());
        }

        if ($found) {
            $context['exception'] = $this->formatException($exception);
        }

        return $context;
    }

    /**
     * @param \Throwable $exception
     * @return array
     */
    private function formatException(\Throwable $exception)
    {
        return [
            'exceptionClass' => \get_class($exception),
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine()
        ];
    }
}
