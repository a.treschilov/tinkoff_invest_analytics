<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Psr\Log\LoggerInterface;

class CompositeLogger implements LoggerInterface
{
    /** @var LoggerInterface[]  */
    private array $loggers;

    public function __construct(...$loggers)
    {
        $this->loggers = $loggers;
    }

    public function emergency($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function alert($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function critical($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function error($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function warning($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function notice($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function info($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function debug($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $context);
    }

    public function log($level, $message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $level, $message, $context);
    }

    public function callLogger(string $functionName, ...$parameters): void
    {
        foreach ($this->loggers as $logger) {
            $logger->{$functionName}(...$parameters);
        }
    }
}
