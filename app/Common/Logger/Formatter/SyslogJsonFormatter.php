<?php

declare(strict_types=1);

namespace App\Common\Logger\Formatter;

class SyslogJsonFormatter extends SyslogJsonBaseFormatter
{
    private $normalizer;

    public function __construct(ArrayNormalizer $normalizer, $prettyPrint = false)
    {
        $this->normalizer = $normalizer;
        parent::__construct($prettyPrint);
    }

    protected function normalizeRecord(array $record): array
    {
        return $this->normalizer->normalizeRecord($record);
    }
}
