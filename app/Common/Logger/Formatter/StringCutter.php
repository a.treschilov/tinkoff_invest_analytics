<?php

declare(strict_types=1);

namespace App\Common\Logger\Formatter;

class StringCutter
{
    private int $prefixAndSuffixLength;

    public function __construct(private int $maxLength = 10240)
    {
        $this->prefixAndSuffixLength = (int)round($this->maxLength / 2);
    }

    /**
     * @param string $string
     * @return string
     */
    public function cutString(string $string): string
    {
        return $this->getPrefix($string) . $this->getStringCenter($string) . $this->getSuffix($string);
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isStringOverflowMaxLength(string $string): bool
    {
        return $this->maxLength < mb_strlen($string, 'UTF-8');
    }

    private function getPrefix(string $string): string
    {
        return mb_substr($string, 0, $this->prefixAndSuffixLength, 'UTF-8');
    }

    private function getStringCenter(string $string): string
    {
        $droppedChars = mb_strlen($string, 'UTF-8') - 2 * $this->prefixAndSuffixLength;
        return "\nDROPPED $droppedChars CHARS\n";
    }

    private function getSuffix(string $string): string
    {
        $stringSuffixStartPosition = mb_strlen($string, 'UTF-8') - $this->prefixAndSuffixLength;
        return mb_substr($string, $stringSuffixStartPosition, null, 'UTF-8');
    }
}
