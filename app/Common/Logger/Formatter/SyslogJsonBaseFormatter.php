<?php

declare(strict_types=1);

namespace App\Common\Logger\Formatter;

use Monolog\Formatter\FormatterInterface;
use Monolog\Logger;
use Monolog\LogRecord;

abstract class SyslogJsonBaseFormatter implements FormatterInterface
{
    private array $logLevels = [
        Logger::DEBUG => LOG_DEBUG,
        Logger::INFO => LOG_INFO,
        Logger::NOTICE => LOG_NOTICE,
        Logger::WARNING => LOG_WARNING,
        Logger::ERROR => LOG_ERR,
        Logger::CRITICAL => LOG_CRIT,
        Logger::ALERT => LOG_ALERT,
        Logger::EMERGENCY => LOG_EMERG,
    ];

    private $record;
    private int $encodeOptions;

    public function __construct(bool $prettyPrint = false)
    {
        $this->setEncodeOptions($prettyPrint);
    }

    private function setEncodeOptions(bool $prettyPrint)
    {
        $this->encodeOptions = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;
        if ($prettyPrint) {
            $this->encodeOptions = JSON_PRETTY_PRINT | $this->encodeOptions;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function formatBatch(array $records): string
    {
        $message = '';
        foreach ($records as $record) {
            $message .= $this->format($record);
        }
        return $message;
    }

    /**
     * {@inheritDoc}
     */
    public function format(LogRecord $record): string
    {
        $this->record = $record->toArray();
        $this->convertRecordToOurSyslogFormat();
        $normalizedRecord = $this->normalizeRecord($this->record);
        $json = json_encode($normalizedRecord, $this->encodeOptions);
        return $json . PHP_EOL;
    }

    private function convertRecordToOurSyslogFormat()
    {
        //put PROGRAM key to first key in json row
        $this->record = ['PROGRAM' => $this->record['channel']] + $this->record;
        $this->record['MESSAGE'] = $this->record['message'];
        $this->record['LEVEL_NUM'] = $this->logLevels[$this->record['level']];
        $this->deleteUnusedRecordKeys();
    }

    private function deleteUnusedRecordKeys()
    {
        unset($this->record['channel'], $this->record['message'], $this->record['level']);

        if (0 === count($this->record['context'])) {
            unset($this->record['context']);
        }
        if (0 === count($this->record['extra'])) {
            unset($this->record['extra']);
        }
    }

    /**
     * @param array $record
     * @return array
     */
    abstract protected function normalizeRecord(array $record): array;
}
