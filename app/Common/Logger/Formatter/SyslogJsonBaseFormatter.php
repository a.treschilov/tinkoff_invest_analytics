<?php

declare(strict_types=1);

namespace App\Common\Logger\Formatter;

use Monolog\Formatter\FormatterInterface;
use Monolog\Level;
use Monolog\LogRecord;

abstract class SyslogJsonBaseFormatter implements FormatterInterface
{
    private array $logLevels = [
        Level::Debug->value => LOG_DEBUG,
        Level::Info->value => LOG_INFO,
        Level::Notice->value => LOG_NOTICE,
        Level::Warning->value => LOG_WARNING,
        Level::Error->value => LOG_ERR,
        Level::Critical->value => LOG_CRIT,
        Level::Alert->value => LOG_ALERT,
        Level::Emergency->value => LOG_EMERG,
    ];

    private $record;
    private int $encodeOptions;

    public function __construct(bool $prettyPrint = false)
    {
        $this->setEncodeOptions($prettyPrint);
    }

    private function setEncodeOptions(bool $prettyPrint): void
    {
        $this->encodeOptions = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;
        if ($prettyPrint) {
            $this->encodeOptions = JSON_PRETTY_PRINT | $this->encodeOptions;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function formatBatch(array $records): string
    {
        $message = '';
        foreach ($records as $record) {
            $message .= $this->format($record);
        }
        return $message;
    }

    /**
     * {@inheritDoc}
     */
    public function format(LogRecord $record): string
    {
        $this->record = $record->toArray();
        $this->convertRecordToOurSyslogFormat();
        $normalizedRecord = $this->normalizeRecord($this->record);
        $json = json_encode($normalizedRecord, $this->encodeOptions);
        return $json . PHP_EOL;
    }

    private function convertRecordToOurSyslogFormat(): void
    {
        //put PROGRAM key to first key in json row
        $this->record = ['PROGRAM' => $this->record['channel']] + $this->record;
        $this->record['MESSAGE'] = $this->record['message'];
        $this->record['LEVEL_NUM'] = $this->logLevels[$this->record['level']];
        $this->deleteUnusedRecordKeys();
    }

    private function deleteUnusedRecordKeys(): void
    {
        unset($this->record['channel'], $this->record['message'], $this->record['level']);

        if (0 === count($this->record['context'])) {
            unset($this->record['context']);
        }
        if (0 === count($this->record['extra'])) {
            unset($this->record['extra']);
        }
    }

    /**
     * @param array $record
     * @return array
     */
    abstract protected function normalizeRecord(array $record): array;
}
