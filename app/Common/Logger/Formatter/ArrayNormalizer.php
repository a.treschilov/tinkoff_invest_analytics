<?php

declare(strict_types=1);

namespace App\Common\Logger\Formatter;

use DateTimeInterface;
use Monolog\DateTimeImmutable;

class ArrayNormalizer
{
    private bool $recordOverflow = false;

    /**
     * @param StringCutter $stringCutter
     */
    public function __construct(private StringCutter $stringCutter)
    {
    }

    /**
     * @param array $record
     * @return array
     */
    public function normalizeRecord(array $record): array
    {
        foreach ($record as $key => $value) {
            $record[$key] = $this->normalize($value);
        }
        if ($this->recordOverflow) {
            $this->recordOverflow = false;
            $record['overflow'] = '1';
        }
        return $record;
    }

    private function normalize($var)
    {
        if (null === $var) {
            return '[null]';
        }
        if (is_bool($var)) {
            return '[' . var_export($var, true) . ']';
        }
        if (is_string($var)) {
            return $this->normalizeString($var);
        }
        if (is_scalar($var)) {
            return (string)$var;
        }
        if ($var instanceof DateTimeInterface) {
            return $var->format('Y-m-d H:i:s');
        }
        if ($this->isTraversable($var)) {
            return $this->normalizeTraversable($var);
        }
        if (is_object($var)) {
            return $this->normalizeObject($var);
        }
        if (is_resource($var)) {
            return '[resource]';
        }
        return '[unknown(' . gettype($var) . ')]';
    }

    private function normalizeString($string)
    {
        $substitute_character = ini_get('mbstring.substitute_character');
        ini_set('mbstring.substitute_character', 'none');
        $string = mb_convert_encoding($string, 'UTF-8', 'UTF-8');
        ini_set('mbstring.substitute_character', $substitute_character);
        $string = $this->cutString($string);
        return $string;
    }

    private function cutString($string)
    {
        if ($this->stringCutter->isStringOverflowMaxLength($string)) {
            $string = $this->stringCutter->cutString($string);
            $this->recordOverflow = true;
        }
        return $string;
    }

    private function isTraversable($var)
    {
        return is_array($var) || $var instanceof \Traversable;
    }

    private function normalizeTraversable($var)
    {
        $normalized = array();
        foreach ($var as $key => $value) {
            $normalized[$key] = $this->normalize($value);
        }
        return $normalized;
    }

    private function normalizeObject($object)
    {
        if ($object instanceof \Exception) {
            return $this->normalizeException($object);
        }
        $normalizedObject['class'] = get_class($object);
        $objectContent = $this->getObjectContent($object);
        if ($objectContent) {
            $normalizedObject['content'] = $objectContent;
        }
        return $normalizedObject;
    }

    private function normalizeException(\Exception $e)
    {
        $exceptionArray = array(
            'class' => get_class($e),
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
        );
        if ($previous = $e->getPrevious()) {
            $exceptionArray['previous'] = $this->normalizeException($previous);
        }
        return $this->normalizeTraversable($exceptionArray);
    }

    private function getObjectContent($object)
    {
        if (method_exists($object, '__toString')) {
            return (string)$object;
        }
        return $this->normalizeTraversable(get_object_vars($object));
    }
}
