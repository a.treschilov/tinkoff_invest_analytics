<?php

declare(strict_types=1);

namespace App\Common\Logger;

use Psr\Log\LoggerInterface;

class SessionLogger implements LoggerInterface
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    public function emergency($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function alert($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function critical($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function error($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function warning($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function notice($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function info($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function debug($message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $message, $this->getContext($context));
    }

    public function log($level, $message, array $context = []): void
    {
        $this->callLogger(__FUNCTION__, $level, $message, $this->getContext($context));
    }

    private function callLogger(string $functionName, ...$parameters): void
    {
        $this->logger->{$functionName}(...$parameters);
    }

    private function getContext(array $context): array
    {
        return $context;
    }
}
