<?php

declare(strict_types=1);

namespace App\Common;

class HttpResponseCode
{
    public const OK = 200;
    public const ACCEPTED = 202;
    public const NO_CONTENT = 204;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const FORBIDDEN = 403;
    public const NOT_FOUND = 404;
    public const UNPROCCESSABLE_ENTITY = 422;
    public const MANY_REQUESTS = 429;
}
