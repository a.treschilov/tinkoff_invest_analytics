<?php

declare(strict_types=1);

namespace App\Common;

class ApplicationMode
{
    public const PROD = 'prod';
    public const DEV = 'dev';

    public static function isProdMode(): bool
    {
        return getenv('mode') === self::PROD;
    }

    public static function isDevMode(): bool
    {
        return getenv('mode') === self::DEV;
    }
}
