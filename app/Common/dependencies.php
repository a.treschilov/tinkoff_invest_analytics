<?php

declare(strict_types=1);

use App\Common\Amqp\AmqpClient;
use App\Common\ApplicationMode;
use App\Common\Logger\BuilderFactory;
use App\Common\Logger\SimpleFactory;
use App\Common\Mailer\Mailer;
use App\Common\Mailer\PhpMailerAdapter;
use App\Common\Mailer\SendgridMailerAdapter;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Monolog\Logger;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Psr\Container\ContainerInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\PhpFilesAdapter;
use App\Common\Logger\Factory as LoggerFactory;

require_once __DIR__ . '/../Dependencies/dependencies.php';
require_once __DIR__ . '/../User/Dependencies/dependencies.php';
require_once __DIR__ . '/../Intl/Dependencies/dependencies.php';
require_once __DIR__ . '/../Assets/Dependencies/dependency.php';
require_once __DIR__ . '/../Item/Dependencies/dependency.php';
require_once __DIR__ . '/../RealEstate/Dependencies/dependency.php';
require_once __DIR__ . '/../Loan/Dependencies/dependency.php';
require_once __DIR__ . '/../Deposit/Dependencies/dependency.php';
require_once __DIR__ . '/../Market/Dependencies/dependency.php';
require_once __DIR__ . '/../Adapters/Dependencies/dependencies.php';
require_once __DIR__ . '/../News/Dependencies/dependencies.php';
require_once __DIR__ . '/../Broker/dependency.php';

/** @var ContainerInterface $container */
$container->set('doctrine.orm.entity_manager', function (ContainerInterface $c): EntityManager {
    $config = ORMSetup::createAttributeMetadataConfiguration(
        [
            __DIR__ . '/../../app/Entities',
            __DIR__ . '/../../app/Adapters/Entities',
            __DIR__ . '/../../app/Assets/Entities',
            __DIR__ . '/../../app/Deposit/Entities',
            __DIR__ . '/../../app/Intl/Entities',
            __DIR__ . '/../../app/Item/Entities',
            __DIR__ . '/../../app/Loan/Entities',
            __DIR__ . '/../../app/Market/Entities',
            __DIR__ . '/../../app/RealEstate/Entities',
            __DIR__ . '/../../app/User/Entities',
        ],
        false,
        __DIR__ . '/../../DoctrineProxies'
    );

    if (ApplicationMode::isProdMode()) {
        $directory = __DIR__ . '/../../cache/Doctrine/';
        $queryCache = new PhpFilesAdapter('doctrine_queries', 0, $directory);
        $metadataCache = new PhpFilesAdapter('doctrine_metadata', 0, $directory);
    } else {
        $queryCache = new ArrayAdapter();
        $metadataCache = new ArrayAdapter();
    }

    $config->setMetadataCache($metadataCache);
    $config->setQueryCache($queryCache);
    $config->setProxyNamespace('Doctrine\Proxies');

    if (ApplicationMode::isProdMode()) {
        $config->setAutoGenerateProxyClasses(true);
    } else {
        $config->setAutoGenerateProxyClasses(true);
    }

    $dbParams = [
        'driver'   => 'pdo_mysql',
        'host' => 'mysql',
        'dbname'   => getenv('MYSQL_DATABASE'),
        'user'     => getenv('MYSQL_USER'),
        'password' => getenv('MYSQL_PASSWORD'),
        'charset' => 'UTF8',
        'wrapperClass' => 'App\Common\Doctrine\Connection\RetryConnection'
    ];

    $connection = DriverManager::getConnection($dbParams, $config);
    return new EntityManager($connection, $config);
});

$container->set('SERVICE_PHPMAILER', function (ContainerInterface $c) {
    $phpMailer = new PHPMailer(true);

    $phpMailer->SMTPDebug = SMTP::DEBUG_OFF;
    $phpMailer->isSMTP();
    $phpMailer->Host = getenv('EMAIL_HOST');
    $phpMailer->SMTPAuth = true;
    $phpMailer->Username = getenv('EMAIL_USERNAME');
    $phpMailer->Password = getenv('EMAIL_PASSWORD');
    $phpMailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $phpMailer->Port = 465;
    $phpMailer->CharSet = $phpMailer::CHARSET_UTF8;

    $phpMailer->setFrom(getenv('EMAIL_USERNAME'), 'Hakkes — онлайн-помощник долгосрочного инвестора');
    $phpMailer->isHTML(true);

    return new PhpMailerAdapter(
        $phpMailer,
        $c->get(Logger::class)
    );
});

$container->set('SERVICE_SENDGRID', function (ContainerInterface $c) {
    $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

    return new SendgridMailerAdapter(
        $sendgrid,
        $c->get(Logger::class)
    );
});

$container->set('SERVICE_' . Mailer::class, function (ContainerInterface $c) {
    return new Mailer(
        $c->get('SERVICE_SENDGRID')
    );
});

$container->set(LoggerFactory::class, function () {
    $configuration = [
        'path' => __DIR__ . '/../../' . getenv('LOGGER_PATH'),
        'default_name' => 'application',
        'root_directory' => '',
        'store_by_date' => true
    ];

    return new LoggerFactory(
        new BuilderFactory(
            new SimpleFactory($configuration)
        )
    );
});

$container->set(Logger::class, function (ContainerInterface $c) {
    /** @var LoggerFactory $factory */
    $factory = $c->get(LoggerFactory::class);
    return $factory->create('api');
});

$container->set('Now' . DateTime::class, function () {
    return new DateTime();
});

$container->set(AMQPStreamConnection::class, function () {
    return new AMQPStreamConnection(
        'rabbitmq',
        5672,
        getenv('RABBITMQ_DEFAULT_USER'),
        getenv('RABBITMQ_DEFAULT_PASS')
    );
});

$container->set(AmqpClient::class, function (ContainerInterface $c) {
    return new AmqpClient(
        $c->get(AMQPStreamConnection::class)
    );
});
