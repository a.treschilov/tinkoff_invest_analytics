<?php

declare(strict_types=1);

namespace App\Common\Models;

abstract class BaseModel
{
    protected array $mapping = [];
    protected array $keyValueModels = [];

    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $mappedKey = $this->mapping[$key] ?? $key;
            $setterName = 'set' . str_replace('_', '', ucwords($mappedKey, '_'));

            if (method_exists($this, $setterName) && $data[$key] !== null) {
                $propertyValue = $value;

                if (isset($this->keyValueModels[$mappedKey]['class']) && !empty($value)) {
                    $innerClass = $this->keyValueModels[$mappedKey]['class'];
                    $propertyValue = [];
                    foreach ($value as $collectionKey => $collectionValue) {
                        $propertyValue[] = new $innerClass([
                            $this->keyValueModels[$mappedKey]['key'] => $collectionKey,
                            $this->keyValueModels[$mappedKey]['value'] => $collectionValue
                        ]);
                    }
                }

                $this->$setterName($propertyValue);
            }
        }
    }
}
