<?php

declare(strict_types=1);

namespace App\Common;

use App\Interfaces\HttpClientInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class HttpClient implements HttpClientInterface
{
    private Client $client;

    public const CONTENT_TYPE_JSON = 'application/json';
    public const METHOD_GET = 'GET';

    private static int $DEFAULT_TIMEOUT = 15;
    private static bool $DEFAULT_HTTP_ERRORS = false;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function doRequest(
        string $method,
        string $url,
        array $body,
        array $headers = [],
        array $options = [] // Full options list https://docs.guzzlephp.org/en/stable/request-options.html
    ): ResponseInterface {
        $options = $this->getOptions($body, $headers, $options);

        return $this->client->request($method, $url, $options);
    }

    private function getOptions(array $body, array $headers, array $options): array
    {
        $defaultHeaders = [
            'Content-Type' => self::CONTENT_TYPE_JSON
        ];
        $headers = array_merge($defaultHeaders, $headers);

        $defaultOptions = [
            'http_errors' => self::$DEFAULT_HTTP_ERRORS,
            'timeout' => self::$DEFAULT_TIMEOUT
        ];

        return array_merge(
            $defaultOptions,
            $options,
            ['query' => $body],
            ['headers' => $headers]
        );
    }
}
