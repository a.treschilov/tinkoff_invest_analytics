<?php

declare(strict_types=1);

namespace App\Common\Mailer;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Log\LoggerInterface;

class PhpMailerAdapter implements MailSenderInterface
{
    public function __construct(
        private readonly PHPMailer $PHPMailer,
        private readonly LoggerInterface $logger
    ) {
    }

    public function send(array $receiverList, string $template, ?array $data = []): void
    {
        foreach ($receiverList as $receiver) {
            $this->PHPMailer->addAddress($receiver);
        }

        $templateData = json_decode(file_get_contents(__DIR__ . '/../../Emails/' . $template . '.json'), true);

        $body = file_get_contents(__DIR__ . '/../../Emails/' . $templateData['template']);
        $this->PHPMailer->Subject = $templateData['subject'];
        $this->PHPMailer->Body = $body;
        try {
            $this->PHPMailer->send();
        } catch (Exception $e) {
            $this->logger->error('Email send', ['exception' => $e]);
        }
    }
}
