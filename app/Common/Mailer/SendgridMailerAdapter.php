<?php

declare(strict_types=1);

namespace App\Common\Mailer;

use Exception;
use Psr\Log\LoggerInterface;
use SendGrid\Mail\Mail;

class SendgridMailerAdapter implements MailSenderInterface
{
    private const TEMPLATES = [
        'registration' => 'd-eac2edabb1b945aa8637ba2d07e2984d',
        'monthlyReport' => 'd-88a6325548e446b4816faea7a255c5a4'
    ];

    public function __construct(
        private readonly \SendGrid $sendgrid,
        private readonly LoggerInterface $logger
    ) {
    }

    public function send(array $receiverList, string $template, ?array $data = []): void
    {
        $email = new Mail();
        $email->setFrom(getenv('EMAIL_USERNAME'), 'Hakkes — онлайн-помощник долгосрочного инвестора');

        if (isset($data["asm"]["group_id"]) && isset($data["asm"]["groups_to_display"])) {
            $email->setAsm($data["asm"]["group_id"], $data["asm"]["groups_to_display"]);
            unset($data["asm"]["group_id"]);
            unset($data["asm"]["groups_to_display"]);
        }

        try {
            foreach ($receiverList as $receiver) {
                $email->addTo($receiver, null, $data);
            }

            $email->setTemplateId(self::TEMPLATES[$template]);

            $result = $this->sendgrid->send($email);

            if ($result->statusCode() >= 300) {
                $this->logger->warning('Email send possible error', ['exception' => new Exception($result->body())]);
            }
        } catch (Exception $e) {
            $this->logger->error('Email send error', ['exception' => $e]);
        }
    }
}
