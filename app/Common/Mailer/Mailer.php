<?php

declare(strict_types=1);

namespace App\Common\Mailer;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Log\LoggerInterface;

class Mailer
{
    public function __construct(
        private readonly MailSenderInterface $mailSender
    ) {
    }

    /**
     * @param string[] $receiverList
     * @param string $template
     * @return void
     */
    public function send(array $receiverList, string $template, ?array $data = []): void
    {
        $this->mailSender->send($receiverList, $template, $data);
    }
}
