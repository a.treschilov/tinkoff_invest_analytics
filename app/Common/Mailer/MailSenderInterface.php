<?php

declare(strict_types=1);

namespace App\Common\Mailer;

interface MailSenderInterface
{
    public function send(array $receiverList, string $template, ?array $data = []): void;
}
