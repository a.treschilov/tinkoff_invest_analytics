<?php

declare(strict_types=1);

namespace App\Common\Doctrine\Connection;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception\DriverException;
use Doctrine\DBAL\Result;

class RetryConnection extends Connection
{
    public const MYSQL_GONE_AWAY_ERROR = 2006;

    public function beginTransaction(): void
    {
        $this->tryExecuteFunction(__FUNCTION__);
    }

    public function commit(): void
    {
        $this->tryExecuteFunction(__FUNCTION__);
    }

    public function createSavepoint($savepoint): void
    {
        $this->tryExecuteFunction(__FUNCTION__, $savepoint);
    }

    public function lastInsertId($name = null): int
    {
        return (int)$this->tryExecuteFunction(__FUNCTION__, $name);
    }

    public function quote($value, $type = null): string
    {
        return $this->tryExecuteFunction(__FUNCTION__, $value, $type);
    }

    public function releaseSavepoint($savepoint): void
    {
        $this->tryExecuteFunction(__FUNCTION__, $savepoint);
    }

    public function rollBack(): void
    {
        $this->tryExecuteFunction(__FUNCTION__);
    }

    public function rollbackSavepoint($savepoint): void
    {
        $this->tryExecuteFunction(__FUNCTION__, $savepoint);
    }

    public function executeQuery(string $sql, array $params = [], $types = [], ?QueryCacheProfile $qcp = null): Result
    {
        return $this->tryExecuteFunction(__FUNCTION__, $sql, $params, $types, $qcp);
    }

    public function update(string $table, array $data, array $criteria = [], array $types = []): int|string
    {
        return $this->tryExecuteFunction(__FUNCTION__, $table, $data, $criteria, $types);
    }

    private function tryExecuteFunction(string $function, ...$args)
    {
        try {
            return parent::{$function}(...$args);
        } catch (ConnectionException $exception) {
            $this->close();
            return parent::{$function}(...$args);
        } catch (DriverException $exception) {
            $errorInfo = $exception->getCode();
            if (self::MYSQL_GONE_AWAY_ERROR === $errorInfo) {
                parent::close();
                return parent::{$function}(...$args);
            }
            throw $exception;
        }
    }
}
