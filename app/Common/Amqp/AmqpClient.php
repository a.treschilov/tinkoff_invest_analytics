<?php

declare(strict_types=1);

namespace App\Common\Amqp;

use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpClient
{
    private const EXCHANGE_NAME = "main_exchange";
    private AbstractChannel|AMQPChannel|null $channel = null;

    public function __construct(
        private readonly AMQPStreamConnection $amqpConnection
    ) {
    }

    public function __destruct()
    {
        $this->channel?->close();
        if (isset($this->amqpConnection)) {
            $this->amqpConnection->close();
        }
    }

    public function publishMessage(array $message): void
    {
        $channel = $this->getChannel();
        $ampqMessage = new AMQPMessage(
            json_encode($message),
            ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]
        );
        $channel->basic_publish($ampqMessage, self::EXCHANGE_NAME);
    }

    public function getChannel(): AbstractChannel|AMQPChannel
    {
        if ($this->channel === null) {
            $this->channel = $this->amqpConnection->channel();
            $this->channel->exchange_declare(
                self::EXCHANGE_NAME,
                'fanout',
                false,
                true,
                false
            );
            $this->setupQueue('main');
            $this->setupQueue('telegram');
        }

        return $this->channel;
    }

    public function reconnect(): void
    {
        $this->amqpConnection->reconnect();
        $this->channel = null;
    }

    public function close(): void
    {
        $this->amqpConnection->close();
    }

    private function setupQueue($name): void
    {
        $this->channel->queue_declare(
            $name,
            false,
            true,
            false,
            false
        );
        $this->channel->queue_bind($name, self::EXCHANGE_NAME);
    }
}
