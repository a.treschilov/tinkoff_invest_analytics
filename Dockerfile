FROM atreschilov/php-grpc:v0.1.7

ADD ./php.ini /usr/local/etc/php
ADD . /var/www
ADD ./crons/* /etc/cron.d

RUN apt-get update && apt-get install cron -y \
    procps -y

RUN chmod 0644 -R /etc/cron.d
CMD cron && tail -f /var/log/cron.log
COPY entrypoint.sh /etc/entrypoint.sh
RUN chmod +x /etc/entrypoint.sh
ENTRYPOINT ["/etc/entrypoint.sh"]
# cron end

WORKDIR /var/www

CMD ["php-fpm"]